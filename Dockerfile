FROM openjdk:8-jdk


WORKDIR /root/.kube
COPY config .

WORKDIR /app

COPY kamel .
COPY faas-user-portal-0.0.1-SNAPSHOT.war .
COPY kubectl .


RUN chmod 777 /app/kamel
RUN chmod 777 /app/kubectl
#RUN /app/kamel install --registry docker.io --organization neinei --registry-secret offsecret3

ENTRYPOINT ["java", "-jar" , "/app/faas-user-portal-0.0.1-SNAPSHOT.war", "--kubectl.path=/app--spring.profiles.active=test"]