import java.util.Map;
import java.util.HashMap;
import com.google.gson.Gson;

def eventDataResult;

from("debezium-mongodb:@@DB_NAME@@?mongodbHosts=@@REPLICASET_NAME@@/@@CON_HOST@@:@@CON_PORT@@&mongodbUser=@@ADMIN_ID@@&mongodbPassword=@@ADMIN_PW@@&mongodbName=@@DB_NAME@@&collectionWhitelist=@@DB_NAME@@.@@COLLECTION_NAME@@&offsetStorageFileName=/tmp/offset-file-1.dat&databaseHistoryFileFilename=/tmp/history-file-1.dat")
  .to("log:info?showAll=true")
  .process(exchange -> {
		def eventDataString = "";

		Map sourceMetadataMap = exchange.getIn().getHeader("CamelDebeziumSourceMetadata", Map.class);

    if(sourceMetadataMap.containsKey("snapshot") && (String.valueOf(sourceMetadataMap.get("snapshot")).toLowerCase().equals("true") || String.valueOf(sourceMetadataMap.get("snapshot")).toLowerCase().equals("last"))) {
      exchange.getIn().setHeader("isSnapshot", "true");
    } else {
      exchange.getIn().setHeader("isSnapshot", "false");

      String operationString = exchange.getIn().getHeader("CamelDebeziumOperation", String.class);
      operationString = String.valueOf(operationString);

      if(operationString.equals("c") || operationString.equals("u") || operationString.equals("d")) {
        exchange.setExchangeId("mongodb");

        Map beforeMap = exchange.getIn().getHeader("CamelDebeziumBefore", Map.class);
        Map keyMap = exchange.getIn().getHeader("CamelDebeziumKey", Map.class);
        String bodyString = exchange.getIn().getBody(String.class);
      	bodyString = String.valueOf(bodyString);

        String beforeString = new Gson().toJson(beforeMap);
        String keyString = new Gson().toJson(keyMap);
        String sourceMetadataString = new Gson().toJson(sourceMetadataMap);

        eventDataString += '"operation":"' + operationString + '"';

        if(!(String.valueOf(beforeString).equals("null") || String.valueOf(beforeString).equals("")))
          eventDataString += ', "before":' + beforeString;
        if(!(String.valueOf(keyString).equals("null") || String.valueOf(keyString).equals("")))
          eventDataString += ', "key":' + keyString;
        if(!(String.valueOf(sourceMetadataString).equals("null") || String.valueOf(sourceMetadataString).equals("")))
          eventDataString += ', "sourceMetadata":' + sourceMetadataString;
        if(!(bodyString.equals("null") || bodyString.equals("")))
          eventDataString += ', "body":' + bodyString;
      }
    }

    eventDataResult = "{" + eventDataString + "}";
	})
	.process {
		it.in.body = eventDataResult;
	}
  .choice()
		.when(header("isSnapshot").isEqualTo("true"))
			.log("This event data is snapshot data.")
	.otherwise()
		.choice()
			.when(header("CamelDebeziumOperation").in('@@EVENT_TYPES@@'))
				.to("knative:channel/@@CHANNEL_NAME@@")
		.otherwise()
			.log("This operation type is not set.")
		.end()
	.end()