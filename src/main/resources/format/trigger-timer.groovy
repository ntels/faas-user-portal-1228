def start_date = new java.text.SimpleDateFormat("yyyyMMdd HHmmss").parse("@@BASE_DATE_STRING@@");
def end_date = new java.text.SimpleDateFormat("yyyyMMdd HHmmss").parse("@@END_DATE_STRING@@");

from("quartz://myGroup/myTimerName?cron=@@CRON_STRING@@")
  .to("log:info?showAll=true")
  .process {
    String start_string = new java.text.SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(start_date);
    String end_string = new java.text.SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(end_date);

    def current_date = new Date();
    String current_string = new java.text.SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(current_date);

    it.in.body = '{"status":"disabled", "current_date":"' + current_string + '", "start_date":"' + start_string + '", "end_date":"' + end_string + '"}';

    if(start_date <= current_date) {
      if(end_date >= current_date) {
        it.in.body = '{"status":"enabled", "current_date":"' + current_string + '", "start_date":"' + start_string + '", "end_date":"' + end_string + '"}';
      }
    }
  }
  .process (exchange -> {
    exchange.setExchangeId("timer");
  })
  .choice()
    .when(body().contains("enabled"))
      .to("knative:channel/@@CHANNEL_NAME@@")
  .otherwise()
    .log("The current_date is not included in the set period.")
  .end()