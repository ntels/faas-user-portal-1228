import java.util.Map;
import java.util.HashMap;
import com.google.gson.Gson;

def eventDataResult;

from("debezium-mysql:@@DB_NAME@@?databaseHostname=@@CON_HOST@@&databasePort=@@CON_PORT@@&databaseUser=@@ADMIN_ID@@&databasePassword=@@ADMIN_PW@@&databaseServerName=@@DB_NAME@@&databaseServerId=@@SERVER_ID@@&tableWhitelist=@@DB_NAME@@.@@TABLE_NAME@@&offsetStorageFileName=/tmp/offset-file-1.dat&databaseHistoryFileFilename=/tmp/history-file-1.dat")
  .to("log:info?showAll=true")
	.process(exchange -> {
    def eventDataString = "";

    Map sourceMetadataMap = exchange.getIn().getHeader("CamelDebeziumSourceMetadata", Map.class);

    if(sourceMetadataMap.containsKey("snapshot") && (String.valueOf(sourceMetadataMap.get("snapshot")).toLowerCase().equals("true") || String.valueOf(sourceMetadataMap.get("snapshot")).toLowerCase().equals("last"))) {
      exchange.getIn().setHeader("isSnapshot", "true");
    } else {
      exchange.getIn().setHeader("isSnapshot", "false");

      String operationString = exchange.getIn().getHeader("CamelDebeziumOperation", String.class);
      operationString = String.valueOf(operationString);

      if(operationString.equals("c") || operationString.equals("u") || operationString.equals("d")) {
        exchange.setExchangeId("mariadb");

        Map beforeMap = exchange.getIn().getHeader("CamelDebeziumBefore", Map.class);
        Map keyMap = exchange.getIn().getHeader("CamelDebeziumKey", Map.class);
        Map bodyMap = exchange.getIn().getBody(Map.class);

        String beforeString = new Gson().toJson(beforeMap);
        String keyString = new Gson().toJson(keyMap);
        String sourceMetadataString = new Gson().toJson(sourceMetadataMap);
        String bodyString = new Gson().toJson(bodyMap);

        eventDataString += '"operation":"' + operationString + '"';

        if(!(String.valueOf(beforeString).equals("null") || String.valueOf(beforeString).equals("")))
          eventDataString += ', "before":' + beforeString;
        if(!(String.valueOf(keyString).equals("null") || String.valueOf(keyString).equals("")))
          eventDataString += ', "key":' + keyString;
        if(!(String.valueOf(sourceMetadataString).equals("null") || String.valueOf(sourceMetadataString).equals("")))
          eventDataString += ', "sourceMetadata":' + sourceMetadataString;
        if(!(String.valueOf(bodyString).equals("null") || String.valueOf(bodyString).equals("")))
          eventDataString += ', "body":' + bodyString;
      }
    }

		eventDataResult = "{" + eventDataString + "}";
	})
	.process {
		it.in.body = eventDataResult;
	}
	.choice()
		.when(header("isSnapshot").isEqualTo("true"))
			.log("This event data is snapshot data.")
	.otherwise()
		.choice()
			.when(header("CamelDebeziumOperation").in('@@EVENT_TYPES@@'))
				.to("knative:channel/@@CHANNEL_NAME@@")
		.otherwise()
			.log("This operation type is not set.")
		.end()
	.end()