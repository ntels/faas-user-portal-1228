/**
 * 
 */


function setTopLink(url){
	$(".btn-refresh").attr("href",url);
}

function goDetail(template_id){
	//switchContent("faas","/faas/?menu=/admin/template/main?action=detailAction&template_id="+template_id);
	$("#contentWrap").hide();
	$("#contentWrap").load("template/main",{action: "detailAction","template_id":template_id});
}
function goModify(){
	$("#contentWrap").load("template/main",{action: "modifyAction","template_id":condition.template_id});
}

function goList() {
	switchContent("${pageContext.request.contextPath}","/faas/?menu=/admin/template/list");
}

//(트리거 추가)삭제
function deleteTrigger() {
	$(document).on('click','[data-btn="delete"]', function(e) {
		e.preventDefault();
		$(this).parents('.trigger-ele').remove();
	});
};

function goNodeJs() {
	$("#nodejsVersion").css("display","block");
	$("#pythonVersion").css("display","none");
}

function goPython() {
	$("#nodejsVersion").css("display","none");
	$("#pythonVersion").css("display","block");
}	

function setEditor(editBox,result,readonly) {
	//debugger;	
	var modelist = ace.require("ace/ext/modelist");
	var modeCheck ="";
	var content = "";
	var editor = ace.edit(editBox);
	if(result != "") {
		modeCheck = modelist.getModeForPath(result.fileName);
		editor.session.setMode(modeCheck.mode);
		content = result.fileContent;
	}
	editor.setReadOnly(readonly);
	editor.setTheme("ace/theme/textmate");
	editor.session.setOption("useWorker", false);
	editor.setOptions({
	    enableBasicAutocompletion: true,
	    enableSnippets: true,
	    enableLiveAutocompletion: true,
	    fontSize: "15pt"
	});
	//editor.setValue(b64_to_utf8(codeByteArray.content));
	
	console.log(content);
	editor.setValue(content);
	editor.clearSelection();
	return editor;
}

function addTrigger(action) {
	var param = {};
	
	if(action =='httpTrigger') {
		var httpCnt = $("#httpCnt").text();
		if(httpCnt =='') {
			httpCnt = 0;
		}else{
			httpCnt = parseInt(httpCnt);
			httpCnt += 1;
		}
		$("#httpCnt").text(httpCnt);
		param = {"httpCnt":httpCnt};
	}
	if(action =='cephTrigger') {
		var cephCnt = $("#cephCnt").text();
		if(cephCnt =='') {
			cephCnt = 0;
		}else{
			cephCnt = parseInt(cephCnt);
			cephCnt += 1;
		}
		$("#cephCnt").text(cephCnt);
		param = {"cephCnt":cephCnt};
	}
	
	if(action =='mongoTrigger') {
		var mongoCnt = $("#mongoCnt").text();
		if(mongoCnt =='') {
			mongoCnt = 0;
		}else{
			mongoCnt = parseInt(mongoCnt);
			mongoCnt += 1;
		}
		$("#mongoCnt").text(mongoCnt);
		param = {"mongoCnt":mongoCnt};
	}	
	
	if(action =='postgreTrigger') {
		var postgreCnt = $("#postgreCnt").text();
		if(postgreCnt =='') {
			postgreCnt = 0;
		}else{
			postgreCnt = parseInt(postgreCnt);
			postgreCnt += 1;
		}
		$("#postgreCnt").text(postgreCnt);
		param = {"postgreCnt":postgreCnt};
	}
	
	if(action =='timerTrigger') {
		var timerCnt = $("#timerCnt").text();
		if(timerCnt =='') {
			timerCnt = 0;
		}else{
			timerCnt = parseInt(timerCnt);
			timerCnt += 1;
		}
		$("#timerCnt").text(timerCnt);
		param = {"timerCnt":timerCnt};
	}		
	
	var url = "template/"+action;
	$.ajax({
        url : url,
        data : param,
        type : 'POST',
        success : function(data) {
           if(action =='httpTrigger') $("#httpArea").append(data);
           if(action =='cephTrigger') $("#cephArea").append(data);
           if(action =='mongoTrigger') $("#mongoArea").append(data);
           if(action =='postgreTrigger') $("#postgreArea").append(data);
           if(action =='timerTrigger') $("#timerArea").append(data);
          // console.log(data);
        },
        error: function(request, status, error){	        	
        	alert("code : "+ request.status + "\r\nmessage : "+request.responseText + "\r\error : "+error);
        },
        complete : function() {
        }
	});
}	

function goDelete() {
	var param = $("#myForm").serialize();
	param = queryStringToJSON(param);
	param = JSON.stringify(param),
	console.log(param);
	formData = param;
	$.ajax({
        url : "template/delete",
        data : param,
        type : 'POST',
        dataType : 'json',
        contentType:"application/json;charset=utf-8",
        traditional : true,
        success : function(data) {
        	console.log(data);
        	if(data.deleteResult =='succsess'){
				alert('삭제하였습니다.');
				$("#deleteCancel").trigger("click");
        		goList();
        	}else {
        		alert('삭제에 실패하였습니다.');
        		return false;
        	}
        },
        error: function(request, status, error){	        	
        	alert("code : "+ request.status + "\r\nmessage : "+request.responseText + "\r\error : "+error);
        },
        complete : function() {
        	//alert('comple');
        }
	}); 	
}   

function goAdd() {
	//$("#contentWrap").hide();
	//$("#contentWrap").load("template/main",{action: "addAction"});
	switchContent("faas","/faas/?menu=/admin/template/addAction");
}




//관리자 지정 클릭시 show
// HTTP 트리거 제어
$(document).on('change', '[data-sel *="httpSel"] input:checked', function() {
 	var parents = $(this).parent().parent().parent();
	var divObj = null;
	for (var i=0;i<parents.length;i++) {
		var parent = parents[i];
		if ($(parent).hasClass("list-inline")) {
		//if ($(parent).is("#httpIndex")) {
			divObj = parent;
			break;
		}
	}
	
	var index = $(divObj).find('input[name="httpIndex"]').val();
	var sel = "httpSel"+index;
	
	if($('[data-sel = '+sel+'] input:checked').val() == "admin") {
		$(this).parent().parent().siblings('[data-lst="httpAdmin"]').show();
	} else {
		$(this).parent().parent().siblings('[data-lst="httpAdmin"]').hide();
	};
});

// Ceph 트리거 제어
$(document).on('change', '[data-sel *="cephSel"] input:checked', function() { 
 	var parents = $(this).parent().parent().parent();
	var divObj = null;
	for (var i=0;i<parents.length;i++) {
		var parent = parents[i];
		if ($(parent).hasClass("list-inline")) {
			divObj = parent;
			break;
		}
	}
	var index = $(divObj).find('input[name="cephIndex"]').val();
	var sel = "cephSel"+index;	
	if($('[data-sel = '+sel+'] input:checked').val() == "admin") {
		$(this).parent().parent().siblings('[data-lst="admin"]').show();
	} else {
		$(this).parent().parent().siblings('[data-lst="admin"]').hide();
	};
});
// MongoDB 트리거​ 제어
$(document).on('change', '[data-sel *="mongoSel"] input:checked', function() {
 	var parents = $(this).parent().parent().parent();
	var divObj = null;
	for (var i=0;i<parents.length;i++) {
		var parent = parents[i];
		if ($(parent).hasClass("list-inline")) {
			divObj = parent;
			break;
		}
	}
	var index = $(divObj).find('input[name="mongoIndex"]').val();
	var sel = "mongoSel"+index;	
	
	if($('[data-sel = '+sel+'] input:checked').val() == "admin") {
		$(this).parent().parent().siblings('[data-lst="admin"]').show();
	} else {
		$(this).parent().parent().siblings('[data-lst="admin"]').hide();
	};
});
// PostgreSQL 트리거​ 제어
$(document).on('change', '[data-sel *="postgreSel"] input:checked', function() {
 	var parents = $(this).parent().parent().parent();
	var divObj = null;
	for (var i=0;i<parents.length;i++) {
		var parent = parents[i];
		if ($(parent).hasClass("list-inline")) {
			divObj = parent;
			break;
		}
	}
	var index = $(divObj).find('input[name="postgreIndex"]').val();
	var sel = "postgreSel"+index;		
	

	if($('[data-sel = '+sel+'] input:checked').val() == "admin") {
		$(this).parent().parent().siblings('[data-lst="admin"]').show();
	} else {
		$(this).parent().parent().siblings('[data-lst="admin"]').hide();
	};
});
// 타이머 트리거 제어
$(document).on('change', '[data-sel *="timerSel"] input:checked', function() {
 	var parents = $(this).parent().parent().parent();
	var divObj = null;
	for (var i=0;i<parents.length;i++) {
		var parent = parents[i];
		if ($(parent).hasClass("list-inline")) {
			divObj = parent;
			break;
		}
	}
	
	var index = $(divObj).find('input[name="timerIndex"]').val();
	var sel = "timerSel"+index;		
	//var admin = "admin"+index
	if($('[data-sel = '+sel+'] input:checked').val() == "admin") {
		$(this).parent().parent().siblings('[data-lst="admin"]').show();
	} else {
		$(this).parent().parent().siblings('[data-lst="admin"]').hide();
	};
});





/* 	function setEditor(editBox) {
	var modelist = ace.require("ace/ext/modelist");
	var editor = ace.edit(editBox);
	editor.setReadOnly(false);
	editor.setTheme("ace/theme/textmate");
	editor.session.setOption("useWorker", false);
	editor.setOptions({
	  enableBasicAutocompletion: true,
	  enableSnippets: true,
	  enableLiveAutocompletion: true,
	  fontSize: "15pt"
	});
	editor.setValue("");
	editor.clearSelection();
	return editor;
} */	



function indexActive(){
	$("#index").addClass("active");
	$("#package").removeClass("active");
	$("#editor1").css("display","block");
	$("#editor2").css("display","none");	
}

function packageActive(){
	$("#index").removeClass("active");
	$("#package").addClass("active");
	$("#editor1").css("display","none");
	$("#editor2").css("display","block");
}	

/**
 * 문자열이 빈 문자열인지 체크하여 결과값을 리턴한다.
 * @param str       : 체크할 문자열
 */
function isEmpty(str){
    if(typeof str == "undefined" || str == null || str == "")
        return true;
    else
        return false ;
}
 
/**
 * 문자열이 빈 문자열인지 체크하여 기본 문자열로 리턴한다.
 * @param str           : 체크할 문자열
 * @param defaultStr    : 문자열이 비어있을경우 리턴할 기본 문자열
 */
function nvl(str, defaultStr){
     
    if(typeof str == "undefined" || str == null || str == "")
        str = defaultStr ;
     
    return str ;
}



