const express = require('express');
const bodyParser = require('body-parser');
const cors = require('cors');
const app = express();
const targetConfigs = {
  // format : 'onSuccess' : 'targetEndpoint#1,targetEndpoint#2,targetEndpoint#3 ...'
  'onSuccess': process.env.TARGET_ENDPOINTS_ONSUCCESS,
  // format : 'onFailure' : 'targetEndpoint#1,targetEndpoint#2,targetEndpoint#3 ...'
  'onFailure': process.env.TARGET_ENDPOINTS_ONFAILURE
}

app.use(cors());

app.use(
  bodyParser.urlencoded({
  extended: false
}));

app.use((req, res, next) => {
  req.setEncoding('utf8');

  var target_type = '';
  var target_data = '';

  var is_success = req.headers["is-success"];

  if(is_success) {
    if(is_success.toLowerCase().includes("true"))
      target_type = "success";
    else
      target_type = "failure";
  } else {
    target_type = "failure";
  }

  req.on('data', function(chunk) {
    target_data += chunk;
  });

  req.on('end', function() {
    req.target_type = target_type;
    req.target_data = target_data;

    app.use(bodyParser.json());

    next();
  });
});

app.use((req, res, next) => {
  console.log("******************************");

  if(req.target_type)
    console.log('target_type is ' + req.target_type);
  else
    console.log('target_type is null');

  if(req.target_data)
    console.log('target_data is ' + req.target_data);
  else
    console.log('target_data is null');

  requestTarget(req.target_type, req.target_data);

  console.log("Target request process has been completed.");
  console.log("******************************");

  res.status(200).send("Target request process has been completed.");
});

app.listen(8080, function() {
  console.log("This application listening on port 8080.");
});

function requestTarget(target_type, target_data) {
  console.log('Request target function! target_type is ' + target_type);

  var targetEndpoints;

  if(target_type == "success")
    targetEndpoints = targetConfigs.onSuccess;
  else
    targetEndpoints = targetConfigs.onFailure;

  if(!targetEndpoints)
    return;

  targetEndpointList = targetEndpoints.split(",");

  if(targetEndpointList.length < 1)
    return;

  for(var i=0; i<targetEndpointList.length; i++) {
    console.log("Request target endpoint " + i + " is " + targetEndpointList[i]);

    var request = require('request');

    var options = {
      uri: '' + targetEndpointList[i],
      method: 'POST',
      timeout: 2000,
      body : target_data
    }

    try {
      request(options, function(error, response, body) {});
    } catch(error) {
      console.log("Request to " + targetEndpointList[i] + " - error");
    }
  }
}
