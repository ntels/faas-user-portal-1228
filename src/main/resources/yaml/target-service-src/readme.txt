-- 함수 대상 (target) 서비스 모듈 소스 --
* 함수 호출시 대상이 지정되면 이 모듈을 호출하고 이 모듈이 다시 대상 서비스를 호출한다.

-- image 생성
docker build --tag 192.168.14.51:5000/faas_public/target-service:latest .
docker push 192.168.14.51:5000/faas_public/target-service:latest

-- properties
target.service.img=192.168.14.51:5000/faas_public/target-service:latest

-- 적용관련 소스는 FunctionService.java 에 구현되어 있음.

