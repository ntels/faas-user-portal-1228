package kr.co.ntels.faas.trigger.vo;

public class BaseTriggerVo {
    private int trigger_id;
    private int function_id;
    private String trigger_tp;
    private String name;
    private String namespace;
    private String channel_name;
    private String subsc_name;
    private String kamel_name;
    private String dsp_name;
    private String trigger_stat_cd;
    private int ord;

    public int getTrigger_id() {
        return trigger_id;
    }

    public void setTrigger_id(int trigger_id) {
        this.trigger_id = trigger_id;
    }

    public int getFunction_id() {
        return function_id;
    }

    public void setFunction_id(int function_id) {
        this.function_id = function_id;
    }

    public String getTrigger_tp() {
        return trigger_tp;
    }

    public void setTrigger_tp(String trigger_tp) {
        this.trigger_tp = trigger_tp;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getNamespace() {
        return namespace;
    }

    public void setNamespace(String namespace) {
        this.namespace = namespace;
    }

    public String getChannel_name() {
        return channel_name;
    }

    public void setChannel_name(String channel_name) {
        this.channel_name = channel_name;
    }

    public String getSubsc_name() {
        return subsc_name;
    }

    public void setSubsc_name(String subsc_name) {
        this.subsc_name = subsc_name;
    }

    public String getKamel_name() {
        return kamel_name;
    }

    public void setKamel_name(String kamel_name) {
        this.kamel_name = kamel_name;
    }

    public String getDsp_name() {
        return dsp_name;
    }

    public void setDsp_name(String dsp_name) {
        this.dsp_name = dsp_name;
    }

    public String getTrigger_stat_cd() {
        return trigger_stat_cd;
    }

    public void setTrigger_stat_cd(String trigger_stat_cd) {
        this.trigger_stat_cd = trigger_stat_cd;
    }

    public int getOrd() {
        return ord;
    }

    public void setOrd(int ord) {
        this.ord = ord;
    }
}
