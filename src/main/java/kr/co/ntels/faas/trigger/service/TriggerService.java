package kr.co.ntels.faas.trigger.service;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.StringUtils;

import com.fasterxml.jackson.databind.ObjectMapper;

import kr.co.ntels.common.utils.CommonUtils;
import kr.co.ntels.faas.common.apiCommonService.model.KCLIData;
import kr.co.ntels.faas.common.utils.CronUtil;
import kr.co.ntels.faas.common.utils.VoConvertUtil;
import kr.co.ntels.faas.constant.FaaSCommCD;
import kr.co.ntels.faas.dao.FaasDao;
import kr.co.ntels.faas.function.model.api.trigger.TriggerInfo;
import kr.co.ntels.faas.function.model.api.trigger.TriggerInfoFactory;
import kr.co.ntels.faas.function.service.DynamicDBService;
import kr.co.ntels.faas.function.service.FunctionService;
import kr.co.ntels.faas.function.service.FunctionServiceBase;
import kr.co.ntels.faas.function.service.KongService;
import kr.co.ntels.faas.function.service.dynamicDB.DynamicDBFactory;
import kr.co.ntels.faas.function.service.dynamicDB.DynamicDBFactory.DBInfo;
import kr.co.ntels.faas.function.vo.FunctionVo;
import kr.co.ntels.faas.trigger.dao.TriggerDao;
import kr.co.ntels.faas.trigger.vo.BaseTriggerVo;
import kr.co.ntels.faas.trigger.vo.DbTriggerVo;
import kr.co.ntels.faas.trigger.vo.HttpTriggerVo;
import kr.co.ntels.faas.trigger.vo.StorageTriggerVo;
import kr.co.ntels.faas.trigger.vo.TimerTriggerVo;
import kr.co.ntels.faas.userException.ExceptionDBConnection;
import kr.co.ntels.faas.userException.ExceptionDBPreProcess;
import kr.co.ntels.faas.userException.ExceptionEx;

/**
 * create*
 *      => knative trigger 생성, user db 셋팅
 * save*
 *		=> DB에 저장. (template)
 */
@Service
public class TriggerService {
	Logger logger = LoggerFactory.getLogger(this.getClass());

	private @Value("${kong.service.url}") String kongServiceUrl;
	
	@Autowired
	private FaasDao dao;
	@Autowired
	private TriggerDao triggerDao;

	@Autowired
	private FunctionService functionService;
	@Autowired
	private FunctionServiceBase functionServiceBase;

	@Autowired
	private DynamicDBService dbService;
	
	@Autowired
	KongService kongService;
	
	@Autowired
	TriggerInfoFactory triggerInfoFactory;
	

	// template 트리거 정보 DB 저장
	@Transactional
	public void saveTemplateHttpTrigger(List<Map<String,Object>> listHttpTrigger, FunctionVo function) throws Exception {
		if(listHttpTrigger == null || listHttpTrigger.size() == 0)
			return;
		
		String apiName = "";
		String function_id = function.getFunction_id()+"";
		String namespace = function.getNamespace();
		String name = function.getName();
		String dsp_name = function.getDsp_name();
		String functionUrl = String.format("%s.%s.svc.cluster.local", name , namespace);
		
		for(Map<String,Object> trigger : listHttpTrigger)
		{
			HttpTriggerVo triggerVo = new HttpTriggerVo();
			
			triggerVo.setFunction_id(function.getFunction_id());
			triggerVo.setName(name);
			triggerVo.setDsp_name(function.getDsp_name());
			triggerVo.setNamespace(namespace);
			triggerVo.setTrigger_tp("http");
			triggerVo.setTrigger_stat_cd(FaaSCommCD.E_TRIGGER_STAT.INIT.toString());
			triggerVo.setKamel_name("");
			triggerVo.setChannel_name("");
			triggerVo.setSubsc_name("");
			
			try
			{
				Map<String,Object> param = new HashMap<>();
				param.put("trigger_id", trigger.get("trigger_id").toString());
				Map<String,Object> templateTriggerInfo = (Map<String,Object>)dao.selectOne("faas.template.loadTriggerHTTPInfo",  param);
				if("Y".equals(templateTriggerInfo.get("adm_set_yn")))
				{
					trigger.put("api_name",templateTriggerInfo.get("api_name").toString());
				}
				
				apiName = trigger.get("api_name").toString();
				
				triggerVo.setApi_name(apiName);
				triggerVo.setAuth_yn("n");
				triggerVo.setCon_url("");
				triggerVo.setApi_key("");
				
				saveHttpTrigger(triggerVo);
			}
			catch(RuntimeException ex)
			{
				if("".equals(apiName) == false)
					kongService.deleteApiGW(namespace, dsp_name, apiName);
				throw ex;
			}
		}
	}
	
	@Transactional
	public void saveTemplateDBTrigger(List<Map<String,Object>> listDBTrigger, FunctionVo function) throws Exception {
		if(listDBTrigger == null || listDBTrigger.size() == 0)
			return;
		
		int function_id = function.getFunction_id();
		String namespace = function.getNamespace();
		String name = function.getName();
		
		for(Map<String,Object> trigger : listDBTrigger)
		{
			String suffix = CommonUtils.getRandomString(6);
			DbTriggerVo triggerVo = new DbTriggerVo();
			
			triggerVo.setFunction_id(function_id);
			triggerVo.setName(name);
			triggerVo.setDsp_name(function.getDsp_name());
			triggerVo.setNamespace(namespace);
			triggerVo.setTrigger_tp("db");
			triggerVo.setTrigger_stat_cd(FaaSCommCD.E_TRIGGER_STAT.INIT.toString());
			triggerVo.setKamel_name(String.format("%s%s-%s", TriggerInfo.kamelPrefix, function.getName() , suffix));
			triggerVo.setChannel_name(String.format("%s%s-%s", TriggerInfo.channelPrefix, function.getName() , suffix));
			triggerVo.setSubsc_name(String.format("%s%s-%s", TriggerInfo.subscriptionPrefix, function.getName() , suffix));
			
			triggerVo.setDb_type(trigger.get("db_type").toString());
			triggerVo.setDb_name(trigger.get("db_name").toString());
			triggerVo.setCon_host(trigger.get("con_host").toString());
			triggerVo.setCon_port(trigger.get("con_port").toString());
			triggerVo.setAdmin_id(trigger.get("admin_id").toString());
			triggerVo.setAdmin_pw(trigger.get("admin_pw").toString());
			triggerVo.setTable_name(trigger.get("table_name").toString());
			triggerVo.setCollection_name(trigger.get("collection_name").toString());

			try
			{
				Map<String,Object> param = new HashMap<>();
				param.put("trigger_id", trigger.get("trigger_id").toString());
				Map<String,Object> templateTriggerInfo = (Map<String,Object>)dao.selectOne("faas.template.loadTriggerDBInfo",  param);
				if("Y".equals(templateTriggerInfo.get("adm_set_yn")))
				{
					trigger.put("insert_yn",templateTriggerInfo.get("insert_yn").toString());
					trigger.put("update_yn",templateTriggerInfo.get("update_yn").toString());
					trigger.put("delete_yn",templateTriggerInfo.get("delete_yn").toString());
				}
				
				triggerVo.setInsert_yn(trigger.get("insert_yn").toString());
				triggerVo.setUpdate_yn(trigger.get("update_yn").toString());
				triggerVo.setDelete_yn(trigger.get("delete_yn").toString());
						
				saveDbTrigger(triggerVo);
			}
			catch(RuntimeException ex)
			{
				throw ex;
			}
		}
	}
	
	@Transactional
	public void saveTemplateTimerTrigger(List<Map<String,Object>> listTimerTrigger, FunctionVo function) throws Exception {
		if(listTimerTrigger == null || listTimerTrigger.size() == 0)
			return;
		
		int function_id = function.getFunction_id();
		String namespace = function.getNamespace();
		String name = function.getName();
		
		for(Map<String,Object> trigger : listTimerTrigger)
		{
			String suffix = CommonUtils.getRandomString(6);
			TimerTriggerVo triggerVo = new TimerTriggerVo();
			
			triggerVo.setFunction_id(function_id);
			triggerVo.setName(name);
			triggerVo.setDsp_name(function.getDsp_name());
			triggerVo.setNamespace(namespace);
			triggerVo.setTrigger_tp("timer");
			triggerVo.setTrigger_stat_cd(FaaSCommCD.E_TRIGGER_STAT.INIT.toString());
			triggerVo.setKamel_name(String.format("%s%s-%s", TriggerInfo.kamelPrefix, function.getName() , suffix));
			triggerVo.setChannel_name(String.format("%s%s-%s", TriggerInfo.channelPrefix, function.getName() , suffix));
			triggerVo.setSubsc_name(String.format("%s%s-%s", TriggerInfo.subscriptionPrefix, function.getName() , suffix));
			
			triggerVo.setBas_date_hms(trigger.get("bas_date_hms").toString());
			triggerVo.setEnd_use_yn(trigger.get("end_use_yn").toString());
			triggerVo.setEnd_date_hms(trigger.get("end_date_hms").toString());
			
			try
			{	
				Map<String,Object> param = new HashMap<>();
				param.put("trigger_id", trigger.get("trigger_id").toString());
				Map<String,Object> templateTriggerInfo = (Map<String,Object>)dao.selectOne("faas.template.loadTriggerTimerInfo",  param);
				if("Y".equals(templateTriggerInfo.get("adm_set_yn")))
				{
					trigger.put("repeat_tp",templateTriggerInfo.get("repeat_tp").toString());
					trigger.put("repeat_val",new ObjectMapper().readValue(templateTriggerInfo.get("repeat_val").toString(),Map.class));
					trigger.put("repeat_des",templateTriggerInfo.get("repeat_des").toString());
				}
				triggerVo.setRepeat_tp(trigger.get("repeat_tp").toString());
				triggerVo.setRepeat_val(new ObjectMapper().writeValueAsString(trigger.get("repeat_val")));
				triggerVo.setRepeat_des(trigger.get("repeat_des").toString());
				triggerVo.setTimer_val(CronUtil.getCronExpression(new JSONObject(trigger)));
				
				saveTimerTrigger(triggerVo);
			}
			catch(RuntimeException ex)
			{
				throw ex;
			}
		}
	}

	// HTTP
	@Transactional
	public void createAndSaveHttpTrigger(Map dataMap, Map functionMap) throws Exception {
		FunctionVo function = VoConvertUtil.convertMapToObj(functionMap, FunctionVo.class);

		String trigger_config = dataMap.get("trigger_config").toString();
		JSONObject config_obj = new JSONObject(trigger_config);

		dataMap.put("api_name", config_obj.get("api_name"));

		HttpTriggerVo httpTrigger = VoConvertUtil.convertMapToObj(dataMap, HttpTriggerVo.class);

		createAndSaveHttpTrigger(httpTrigger, function);
	}

	@Transactional
	public void createAndSaveHttpTrigger(HttpTriggerVo httpTrigger, FunctionVo function) throws Exception {
		if(httpTrigger == null)
			return;
		
		String function_id = function.getFunction_id()+"";
		String namespace = function.getNamespace();
		String name = function.getName();
		String dsp_name = function.getDsp_name();
		String apiName = httpTrigger.getApi_name();
		String functionUrl = String.format("%s.%s.svc.cluster.local", name , namespace);
		String api_key = kongService.registerApiGW(function_id , namespace, dsp_name ,apiName, functionUrl);

		httpTrigger.setFunction_id(function.getFunction_id());
		httpTrigger.setKamel_name("");
		httpTrigger.setChannel_name("");
		httpTrigger.setSubsc_name("");
		httpTrigger.setTrigger_stat_cd(FaaSCommCD.E_TRIGGER_STAT.REG.toString());
		httpTrigger.setCon_url(kongService.getConUrl(namespace , dsp_name , apiName));
		httpTrigger.setApi_key(api_key);

		try
		{
			saveHttpTrigger(httpTrigger);
		}
		catch(RuntimeException ex)
		{
			kongService.deleteApiGW(namespace, dsp_name, apiName);
			throw ex;
		}
	}
	
	
	// Http 트리거
	
	
	// Timer 트리거
	@Transactional
	public void createAndSaveTimerTrigger(Map dataMap, Map functionMap) throws Exception {
		FunctionVo function = VoConvertUtil.convertMapToObj(functionMap, FunctionVo.class);

		String trigger_config = dataMap.get("trigger_config").toString();
		JSONObject config_obj = new JSONObject(trigger_config);

		dataMap.put("bas_date_hms", config_obj.get("bas_date_hms"));
		dataMap.put("repeat_tp", config_obj.get("repeat_tp"));
		dataMap.put("repeat_val", config_obj.get("repeat_val").toString());
		dataMap.put("repeat_des", config_obj.get("repeat_des"));
		dataMap.put("end_use_yn", config_obj.get("end_use_yn"));
		dataMap.put("end_date_hms", config_obj.get("end_date_hms"));
		dataMap.put("timer_val", CronUtil.getCronExpression(config_obj));

		TimerTriggerVo timerTrigger = VoConvertUtil.convertMapToObj(dataMap, TimerTriggerVo.class);

		createAndSaveTimerTrigger(timerTrigger, function);
	}

	@Transactional
	public void createAndSaveTimerTrigger(TimerTriggerVo timerTrigger, FunctionVo function) throws Exception {
		if(timerTrigger == null)
			return;
		
		String suffix = CommonUtils.getRandomString(6);
		timerTrigger.setKamel_name(String.format("%s%s-%s", TriggerInfo.kamelPrefix, function.getName() , suffix));
		timerTrigger.setChannel_name(String.format("%s%s-%s", TriggerInfo.channelPrefix, function.getName() , suffix));
		timerTrigger.setSubsc_name(String.format("%s%s-%s", TriggerInfo.subscriptionPrefix, function.getName() , suffix));
		timerTrigger.setTrigger_stat_cd(FaaSCommCD.E_TRIGGER_STAT.NORMAL.toString());

		try
		{
			createTimerTrigger(timerTrigger, function);
		}
		catch(ExceptionEx ee)
		{
			throw ee;
		}
		
		try
		{
			timerTrigger.setTrigger_tp("timer");
			timerTrigger.setFunction_id(function.getFunction_id());
			saveTimerTrigger(timerTrigger);
		}
		catch(RuntimeException ex)
		{
			Map<String,Object> param = VoConvertUtil.convertObjToMap(timerTrigger);
			TriggerInfo info = triggerInfoFactory.createTriggerInfo(TriggerInfoFactory.E_TRRIGGER_TYPE.TIMER, function.getName(), function.getNamespace(), param);
			functionService.deleteTriger(info);
			throw ex;
		}
	}

	@Transactional
	public void createAndSaveDbTrigger(Map<String, Object> dataMap, Map<String, Object> functionMap) throws Exception {
		FunctionVo function = VoConvertUtil.convertMapToObj(functionMap, FunctionVo.class);
		
		String trigger_config = dataMap.get("trigger_config").toString();
		JSONObject config_obj = new JSONObject(trigger_config);
		
		dataMap.put("db_type", config_obj.get("db_type"));
		dataMap.put("db_name", config_obj.get("db_name"));
		dataMap.put("con_host", config_obj.get("con_host"));
		dataMap.put("con_port", config_obj.get("con_port"));
		dataMap.put("admin_id", config_obj.get("admin_id"));
		dataMap.put("admin_pw", config_obj.get("admin_pw"));
		dataMap.put("insert_yn", config_obj.get("insert_event_yn"));
		dataMap.put("update_yn", config_obj.get("update_event_yn"));
		dataMap.put("delete_yn", config_obj.get("delete_event_yn"));
		dataMap.put("collection_name", "");
		dataMap.put("table_name", "");
		
		if(config_obj.has("collection_name") && !config_obj.get("collection_name").toString().equals("")) {
			dataMap.put("collection_name", config_obj.get("collection_name"));
		} else if(config_obj.has("table_name") && !config_obj.get("table_name").toString().equals("")) {
			dataMap.put("table_name", config_obj.get("table_name"));
		} else {
			throw new ExceptionEx("DB 정보가 부족합니다.");
		}
		
		DbTriggerVo dbTrigger = VoConvertUtil.convertMapToObj(dataMap, DbTriggerVo.class);
		
		createAndSaveDbTrigger(dbTrigger, function);
	}

	@Transactional
	public void createAndSaveDbTrigger(DbTriggerVo dbTrigger, FunctionVo function) throws Exception {
		if(dbTrigger == null)
			return;
		
		String suffix = CommonUtils.getRandomString(6);
		dbTrigger.setKamel_name(String.format("%s%s-%s", TriggerInfo.kamelPrefix, function.getName(), suffix));
		dbTrigger.setChannel_name(String.format("%s%s-%s", TriggerInfo.channelPrefix, function.getName(), suffix));
		dbTrigger.setSubsc_name(String.format("%s%s-%s", TriggerInfo.subscriptionPrefix, function.getName(), suffix));
		dbTrigger.setTrigger_stat_cd(FaaSCommCD.E_TRIGGER_STAT.REG.toString());
		
		try {
			createDbTrigger(dbTrigger, function);
		}
		catch(ExceptionDBPreProcess e) {
			throw e;
		}
		catch(ExceptionEx e) {
			throw e;
		}
		
		try {
			saveDbTrigger(dbTrigger);
		}
		catch(RuntimeException ex) {
			Map<String,Object> param = VoConvertUtil.convertObjToMap(dbTrigger);
			TriggerInfo info = triggerInfoFactory.createTriggerInfo(TriggerInfoFactory.E_TRRIGGER_TYPE.POSTGRESQL, function.getName(), function.getNamespace(), param);
			functionService.deleteTriger(info);
			throw ex;
		}
	}

	@Transactional
	public void createDbTrigger(DbTriggerVo dbTrigger, FunctionVo function) throws Exception {
		DynamicDBFactory.DBInfo info = new DynamicDBFactory.DBInfo();
		
		String selected_dbType = dbTrigger.getDb_type(); 
		
		if(selected_dbType.toLowerCase().equals(TriggerInfoFactory.E_TRRIGGER_TYPE.MONGODB.toString().toLowerCase())) {
			info.setDbType(TriggerInfoFactory.E_TRRIGGER_TYPE.MONGODB);
		} else if(selected_dbType.toLowerCase().equals(TriggerInfoFactory.E_TRRIGGER_TYPE.POSTGRESQL.toString().toLowerCase())) {
			info.setDbType(TriggerInfoFactory.E_TRRIGGER_TYPE.POSTGRESQL);
		} else if(selected_dbType.toLowerCase().equals(TriggerInfoFactory.E_TRRIGGER_TYPE.MARIADB.toString().toLowerCase())) {
			info.setDbType(TriggerInfoFactory.E_TRRIGGER_TYPE.MARIADB);
		}
		
		info.setHost(dbTrigger.getCon_host());
		info.setPort(Integer.valueOf(dbTrigger.getCon_port()));
		info.setUsr(dbTrigger.getAdmin_id());
		info.setPwd(dbTrigger.getAdmin_pw());
		info.setDatabase(dbTrigger.getDb_name());
		
		List<String> events = new ArrayList<>();
		
		if("Y".equals(dbTrigger.getInsert_yn().toUpperCase()))
			events.add("INSERT");
		if("Y".equals(dbTrigger.getUpdate_yn().toUpperCase()))
			events.add("UPDATE");
		if("Y".equals(dbTrigger.getDelete_yn().toUpperCase()))
			events.add("DELETE");
		
		ObjectMapper objectMapper = new ObjectMapper();
		Map<String, Object> preProcessMap = objectMapper.convertValue(dbService.triggerPreProcess(info), Map.class);
		
		Map<String,Object> dbTriggerMap = VoConvertUtil.convertObjToMap(dbTrigger);
		KCLIData result = new KCLIData();
		
		if(info.getDbType().equals(TriggerInfoFactory.E_TRRIGGER_TYPE.MONGODB)) {
			// TODO : camel-k debezium mongodb groovy 작성 시 replicaset_name이 필요합니다. 현재는 caas 카탈로그앱의 mongodb-replicaset 차트의 기본값인 "rs0"으로 고정입니다.
			result = functionServiceBase.createTrigger(
					function.getNamespace(),
					function.getName(),
					TriggerInfoFactory.E_TRRIGGER_TYPE.MONGODB,
					dbTriggerMap);
		} else if(info.getDbType().equals(TriggerInfoFactory.E_TRRIGGER_TYPE.POSTGRESQL)) {
			result = functionServiceBase.createTrigger(
					function.getNamespace(),
					function.getName(),
					TriggerInfoFactory.E_TRRIGGER_TYPE.POSTGRESQL,
					dbTriggerMap);
		} else if(info.getDbType().equals(TriggerInfoFactory.E_TRRIGGER_TYPE.MARIADB)) {
			dbTriggerMap.put("server_id", preProcessMap.get("server_id"));
			
			result = functionServiceBase.createTrigger(
			        function.getNamespace(),
					function.getName(),
					TriggerInfoFactory.E_TRRIGGER_TYPE.MARIADB,
					dbTriggerMap);
		}
		
		if(!result.result) {
			throw new ExceptionEx("db trigger creating fail : " + result.msg);
		}
	}
	
	@Transactional
	private void saveHttpTrigger(HttpTriggerVo httpTrigger) throws SQLException
	{
		if(httpTrigger == null)
			return;
		
		triggerDao.insertBaseTrigger(httpTrigger);
		triggerDao.insertHttpTrigger(httpTrigger);
	}
	
	@Transactional
	private void saveDbTrigger (DbTriggerVo dbTrigger) throws Exception {
		if(dbTrigger == null)
			return;
		
		triggerDao.insertBaseTrigger(dbTrigger);
		triggerDao.insertDbTrigger(dbTrigger);
	}
	

	@Transactional
	private void saveStorageTrigger(StorageTriggerVo storageTrigger, FunctionVo function) {
		if(storageTrigger == null)
			return;

		storageTrigger.setTrigger_tp("storage");
		storageTrigger.setFunction_id(function.getFunction_id());
	}

	@Transactional
	private void saveTimerTrigger(TimerTriggerVo timerTrigger) throws Exception {
		if(timerTrigger == null)
			return;

		triggerDao.insertBaseTrigger(timerTrigger);
		triggerDao.insertTimerTrigger(timerTrigger);
	}

	@Transactional
	public void createTimerTrigger(TimerTriggerVo timerTrigger, FunctionVo function) throws Exception {
		
		Map<String,Object> timerTriggerMap = VoConvertUtil.convertObjToMap(timerTrigger);
		
		KCLIData result = functionService.createTrigger(
				function.getNamespace(),
				function.getName(),
				TriggerInfoFactory.E_TRRIGGER_TYPE.TIMER,
				timerTriggerMap);

		if(!result.result){
			throw new ExceptionEx("timer trigger creating fail !! - " + result.msg);
		}
	}
	
	public DbTriggerVo selectDbTrigger(int trigger_id) {
		return triggerDao.selectDbTrigger(trigger_id);
	}
	
	public Map isValidAndGetTableList(Map dataMap) throws Exception {
		Map<String, Object> resultMap = new HashMap<>();
		boolean isValid = false;
		List<String> tableList = new ArrayList<>();
		
		String db_type = dataMap.get("db_type").toString();
		TriggerInfoFactory.E_TRRIGGER_TYPE e_db_type;
		try {
			e_db_type = TriggerInfoFactory.E_TRRIGGER_TYPE.valueOf(db_type.toUpperCase());
		} catch(Exception e) {
			throw new Exception("is not supported db type");
		}
		
		DBInfo info = new  DBInfo();
		info.setDbType(e_db_type);
		info.setHost(dataMap.get("con_host").toString());
		info.setPort(Integer.parseInt(dataMap.get("con_port").toString()));
		info.setDatabase(dataMap.get("db_name").toString());
		info.setUsr(dataMap.get("admin_id").toString());
		info.setPwd(dataMap.get("admin_pw").toString());
		
		try {
			tableList = dbService.getListTableName(info);
			isValid = true;
		} catch(ExceptionDBConnection e) {
			isValid = false;
		}
		
		resultMap.put("isValid", isValid);
		resultMap.put("tableList", tableList);
		
		return resultMap;
	}

	public void deployInitTriggers(String namespace, String serviceName) throws Exception {
		FunctionVo param = new FunctionVo();
		param.setNamespace(namespace);
		param.setName(serviceName);
		FunctionVo functionInfo = functionService.functionInfoById(param);
		if(functionInfo == null) {
			logger.debug("deployInitTriggers() - no function.");
			return;
		}

		List<BaseTriggerVo> list = triggerDao.selectBaseTriggerByFunctionId(functionInfo.getFunction_id());
		if(list == null || list.size() == 0) {
			logger.debug("deployInitTriggers() - no triggers.");
			return;
		}

		for(BaseTriggerVo trigger : list){
			if(!FaaSCommCD.E_TRIGGER_STAT.INIT.toString().equals(trigger.getTrigger_stat_cd())){
				logger.debug("deployInitTriggers() - skip({})", trigger.getName());
				continue;
			}

			String triggerTp = trigger.getTrigger_tp() != null ? trigger.getTrigger_tp().toUpperCase() : "";

			if(FaaSCommCD.E_TRIGGER_TYPE.TIMER.toString().equals(triggerTp)){
			    TimerTriggerVo timer = triggerDao.selectTimerTrigger(trigger.getTrigger_id());

			    createTimerTrigger(timer, functionInfo);

			}else if(FaaSCommCD.E_TRIGGER_TYPE.DB.toString().equals(triggerTp)) {
			    DbTriggerVo db = triggerDao.selectDbTrigger(trigger.getTrigger_id());
			    createDbTrigger(db, functionInfo);
			    
            }else if(FaaSCommCD.E_TRIGGER_TYPE.HTTP.toString().equals(triggerTp)) {
            	
            	String api_key;
            	String apiName;
            	String con_url;
            	String function_id = trigger.getFunction_id()+"";
            	String dsp_name = functionInfo.getDsp_name();
            	String name = functionInfo.getName();
            	String functionUrl = String.format("%s.%s.svc.cluster.local", name , namespace);
            	
            	Map<String,Object> httpInfo = (Map<String,Object>)dao.selectOne("faas.trigger.selectHttpTrigger", trigger.getTrigger_id()+"");
            	if(httpInfo != null)
            	{
            		apiName = httpInfo.get("api_name").toString();
            		con_url = kongService.getConUrl(namespace , dsp_name , apiName);
            		api_key = kongService.registerApiGW(function_id , namespace, dsp_name ,apiName, functionUrl);
            		
            		Map<String,Object> map = new HashMap<>();
            		map.put("trigger_id", trigger.getTrigger_id()+"");
            		map.put("api_name", apiName);
            		map.put("api_key", api_key);
            		map.put("con_url", con_url);
            		
            		dao.update("faas.trigger.setHttpTrigger", map);
            	}
            }
			
			trigger.setTrigger_stat_cd(FaaSCommCD.E_TRIGGER_STAT.NORMAL.toString());
		    dao.update("faas.trigger.setTriggerStat", trigger);
		}

	}
}