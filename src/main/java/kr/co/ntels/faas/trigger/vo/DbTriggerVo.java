package kr.co.ntels.faas.trigger.vo;

public class DbTriggerVo extends BaseTriggerVo {
    private String db_type;
    private String db_name;
    private String con_host;
    private String con_port;
    private String admin_id;
    private String admin_pw;
    private String table_name;
    private String collection_name;
    private String insert_yn;
    private String update_yn;
    private String delete_yn;

    public String getDb_type() {
        return db_type;
    }

    public void setDb_type(String db_type) {
        this.db_type = db_type;
    }

    public String getDb_name() {
        return db_name;
    }

    public void setDb_name(String db_name) {
        this.db_name = db_name;
    }

    public String getCon_host() {
        return con_host;
    }

    public void setCon_host(String con_host) {
        this.con_host = con_host;
    }

    public String getCon_port() {
        return con_port;
    }

    public void setCon_port(String con_port) {
        this.con_port = con_port;
    }

    public String getAdmin_id() {
        return admin_id;
    }

    public void setAdmin_id(String admin_id) {
        this.admin_id = admin_id;
    }

    public String getAdmin_pw() {
        return admin_pw;
    }

    public void setAdmin_pw(String admin_pw) {
        this.admin_pw = admin_pw;
    }

    public String getTable_name() {
        return table_name;
    }

    public void setTable_name(String table_name) {
        this.table_name = table_name;
    }

    public String getCollection_name() {
        return collection_name;
    }

    public void setCollection_name(String collection_name) {
        this.collection_name = collection_name;
    }

    public String getInsert_yn() {
        return insert_yn;
    }

    public void setInsert_yn(String insert_yn) {
        this.insert_yn = insert_yn;
    }

    public String getUpdate_yn() {
        return update_yn;
    }

    public void setUpdate_yn(String update_yn) {
        this.update_yn = update_yn;
    }

    public String getDelete_yn() {
        return delete_yn;
    }

    public void setDelete_yn(String delete_yn) {
        this.delete_yn = delete_yn;
    }
}
