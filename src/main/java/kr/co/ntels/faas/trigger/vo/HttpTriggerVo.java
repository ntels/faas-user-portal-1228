package kr.co.ntels.faas.trigger.vo;

public class HttpTriggerVo extends BaseTriggerVo {
    private String api_name;
    private String auth_yn;
    private String con_url;
    private String api_key;

    public String getApi_name() {
        return api_name;
    }

    public void setApi_name(String api_name) {
        this.api_name = api_name;
    }

    public String getAuth_yn() {
        return auth_yn;
    }

    public void setAuth_yn(String auth_yn) {
        this.auth_yn = auth_yn;
    }

    public String getCon_url() {
        return con_url;
    }

    public void setCon_url(String con_url) {
        this.con_url = con_url;
    }

	public String getApi_key() {
		return api_key;
	}

	public void setApi_key(String api_key) {
		this.api_key = api_key;
	}
}
