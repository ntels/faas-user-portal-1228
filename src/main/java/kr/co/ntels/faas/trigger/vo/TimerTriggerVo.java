package kr.co.ntels.faas.trigger.vo;

public class TimerTriggerVo extends BaseTriggerVo {
    private String bas_date_hms;
    private String repeat_tp;
    private String repeat_val;
    private String repeat_des;
    private String end_use_yn;
    private String end_date_hms;
    private String timer_val;

    public String getBas_date_hms() {
        return bas_date_hms;
    }

    public void setBas_date_hms(String bas_date_hms) {
        this.bas_date_hms = bas_date_hms;
    }

    public String getRepeat_tp() {
        return repeat_tp;
    }

    public void setRepeat_tp(String repeat_tp) {
        this.repeat_tp = repeat_tp;
    }

    public String getRepeat_val() {
        return repeat_val;
    }

    public void setRepeat_val(String repeat_val) {
        this.repeat_val = repeat_val;
    }
    
    public String getRepeat_des() {
        return repeat_des;
    }

    public void setRepeat_des(String repeat_des) {
        this.repeat_des = repeat_des;
    }

    public String getEnd_use_yn() {
        return end_use_yn;
    }

    public void setEnd_use_yn(String end_use_yn) {
        this.end_use_yn = end_use_yn;
    }

    public String getEnd_date_hms() {
        return end_date_hms;
    }

    public void setEnd_date_hms(String end_date_hms) {
        this.end_date_hms = end_date_hms;
    }

    public String getTimer_val() {
        return timer_val;
    }

    public void setTimer_val(String timer_val) {
        this.timer_val = timer_val;
    }
}
