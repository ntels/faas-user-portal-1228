package kr.co.ntels.faas.trigger.dao;

import java.util.List;
import java.util.Map;

import org.mybatis.spring.SqlSessionTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import kr.co.ntels.faas.function.vo.FunctionVo;
import kr.co.ntels.faas.trigger.vo.BaseTriggerVo;
import kr.co.ntels.faas.trigger.vo.DbTriggerVo;
import kr.co.ntels.faas.trigger.vo.HttpTriggerVo;
import kr.co.ntels.faas.trigger.vo.TimerTriggerVo;

@Repository
public class TriggerDao {
	
    @Autowired
    private SqlSessionTemplate sqlSessionTemplate;

    public Object insertBaseTrigger(BaseTriggerVo param) {
        return sqlSessionTemplate.insert("faas.trigger.insertBaseTrigger", param);
    }

    public Object insertHttpTrigger(HttpTriggerVo param) {
        return sqlSessionTemplate.insert("faas.trigger.insertHttpTriggerVo", param);
    }

    public Object insertDbTrigger(DbTriggerVo param) {
        return sqlSessionTemplate.insert("faas.trigger.insertDbTrigger", param);
    }

    public Object insertTimerTrigger(TimerTriggerVo param) {
        return sqlSessionTemplate.insert("faas.trigger.insertTimerTriggerVo", param);
    }

    public Object insertTimerTrigger(Map<String, Object> param) {
        return sqlSessionTemplate.insert("faas.trigger.insertTimerTrigger", param);
    }

    public DbTriggerVo selectDbTrigger(int trigger_id) {
        return sqlSessionTemplate.selectOne("faas.trigger.selectDbTrigger", trigger_id);
    }

    public List<BaseTriggerVo> selectBaseTriggerByFunctionId(int functionId) {
        return sqlSessionTemplate.selectList("faas.trigger.selectBaseTriggerByFunctionId", functionId);
    }

    public TimerTriggerVo selectTimerTrigger(int trigger_id) {
        return sqlSessionTemplate.selectOne("faas.trigger.selectTimerTrigger", trigger_id);
    }
    
    public Object insertFunctionVo(FunctionVo param) {
        return sqlSessionTemplate.insert("faas.function.insertFunctionVo", param);
    }
}
