package kr.co.ntels.faas.userInterface;

public interface ICallbackFunction {
	
	public void callFunction(String type, Object obj) throws Exception;

}
