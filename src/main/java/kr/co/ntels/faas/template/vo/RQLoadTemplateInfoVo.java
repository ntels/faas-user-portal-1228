package kr.co.ntels.faas.template.vo;

public class RQLoadTemplateInfoVo {
    private int template_id;

    public int getTemplate_id() {
        return template_id;
    }

    public void setTemplate_id(int template_id) {
        this.template_id = template_id;
    }
}
