package kr.co.ntels.faas.template.vo;

public class TemplateVo {
    private int template_id;
    private String name;
    private String runtime;
    private String version;
    private String git_url;
    private String des;
    private String reg_usr;
    private String reg_date_hms;
    private String mod_usr;
    private String mod_date_hms;
    private String top_yn;
    private String use_yn;

    public int getTemplate_id() {
        return template_id;
    }

    public void setTemplate_id(int template_id) {
        this.template_id = template_id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getRuntime() {
        return runtime;
    }

    public void setRuntime(String runtime) {
        this.runtime = runtime;
    }

    public String getVersion() {
        return version;
    }

    public void setVersion(String version) {
        this.version = version;
    }

    public String getGit_url() {
        return git_url;
    }

    public void seGit_url(String git_url) {
        this.git_url = git_url;
    }

    public String getDes() {
        return des;
    }

    public void setDes(String des) {
        this.des = des;
    }
    
    public String getReg_usr() {
        return reg_usr;
    }

    public void setReg_usr(String reg_usr) {
        this.reg_usr = reg_usr;
    }

    public String getReg_date_hms() {
        return reg_date_hms;
    }

    public void setReg_date_hms(String reg_date_hms) {
        this.reg_date_hms = reg_date_hms;
    }

    public String getMod_usr() {
        return mod_usr;
    }

    public void setMod_usr(String mod_usr) {
        this.mod_usr = mod_usr;
    }

    public String getMod_date_hms() {
        return mod_date_hms;
    }

    public void setMod_date_hms(String mod_date_hms) {
        this.mod_date_hms = mod_date_hms;
    }

    public String getTop_yn() {
        return top_yn;
    }

    public void setTop_yn(String top_yn) {
        this.top_yn = top_yn;
    }

    public String getUse_yn() {
        return use_yn;
    }

    public void setUse_yn(String use_yn) {
        this.use_yn = use_yn;
    }

}
