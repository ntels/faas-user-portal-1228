package kr.co.ntels.faas.template.web;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import kr.co.ntels.faas.common.vo.ResultVo;
import kr.co.ntels.faas.constant.FaaSCommCD;
import kr.co.ntels.faas.function.service.FunctionService;
import kr.co.ntels.faas.gitlab.service.GitLabService;
import kr.co.ntels.faas.template.service.TemplateService;
import kr.co.ntels.faas.trigger.service.TriggerService;

@Controller
@RequestMapping("template")
public class TemplateController {
	Logger logger = LoggerFactory.getLogger(this.getClass());

	@Autowired FunctionService functionService;

	@Autowired TemplateService templateService;

	@Autowired TriggerService triggerService;

	@Autowired GitLabService gitlabService;

	@ResponseBody
	@RequestMapping("loadTemplateList.json")
	public ModelMap loadTemplateList(@RequestBody Map<String,Object> param) {
		ModelMap model = new ModelMap();

		try{
			
			int curpage = Integer.parseInt(param.get("curpage").toString());
			int limit = 12;
			int offset = functionService.getOffsetInPage(curpage , limit);
			
			param.put("limit", limit);
			param.put("offset", offset);
			
			List<Map<String,Object>> list = templateService.loadTemplateList(param);
			int totalCount = templateService.loadTemplateListCount(param);
			
			model.addAttribute("list", list);
			model.addAttribute("totalCount", totalCount);
			model.addAttribute("curpage", curpage + "");
			model.addAttribute("lastpage", functionService.getLastPage(totalCount, limit));
			model.addAttribute("result", new ResultVo(true));

		} catch (Exception e) {
			logger.error(null, e);
			model.addAttribute("result", new ResultVo(false, e.getMessage()));
		}

		return model;
	}


	@ResponseBody
	@RequestMapping("loadTemplateInfo.json")  
	public ModelMap loadTemplateInfo(@RequestBody Map<String,Object> param) {
		ModelMap model = new ModelMap();

		try {
			
			Map<String,Object> tempInfo = (Map<String,Object>)templateService.loadTemplateInfo(param);
			String templateGitpath = tempInfo.get("git_url").toString();
			List<Map<String, Object>> gitfiles = gitlabService.checkOutFromProject(templateGitpath);

			if(gitfiles != null && gitfiles.size() > 0){
				model.addAttribute("templateSource", gitfiles.get(0));
			}else{
				Map<String, String> source = new HashMap<>();
				source.put("fileName", "no file");
				source.put("fileContent", "no content");
			    model.addAttribute("templateSource", source);
			}
			
			Map<String,Object> triggerInfo = templateService.loadTriggerInfo(param);
			tempInfo.put("triggerinfo", triggerInfo);

			model.addAttribute("info", tempInfo);
			model.addAttribute("result", new ResultVo(true));

		}catch (Exception e){
			logger.error(null, e);
			model.addAttribute("result", new ResultVo(false, e.getMessage()));
		}

		return model;
    }


	@ResponseBody
	@RequestMapping("saveTemplate.json")
	public ModelMap saveTemplate(HttpServletRequest request, @RequestBody Map<String,Object> param) {
		ModelMap model = new ModelMap();

		try {
			int functionId = templateService.saveTemplate(param);
			model.addAttribute("functionId", functionId);
			model.addAttribute("result", new ResultVo(true));

		} catch (Exception e) {
			logger.error(null, e);
			model.addAttribute("result", new ResultVo(false, e.getMessage()));
		}

		return model;
	}

	@ResponseBody
	@RequestMapping("existFunction.json")
	public ModelMap existFunction(HttpServletRequest request,@RequestBody Map<String,Object> param) {
		ModelMap model = new ModelMap();

		if(StringUtils.isEmpty(param.get("functionDspName").toString())){
			model.addAttribute("result", new ResultVo(false, "no function name."));
			return model;
		}

		try {
			String namespace = FaaSCommCD.getNamespace(request);
			if(gitlabService.projectExists(namespace, param.get("functionDspName").toString())) {
				model.addAttribute("exist", true);
			}else{
				model.addAttribute("exist", false);
			}
			model.addAttribute("result", new ResultVo(true));

		} catch (Exception e) {
			logger.error(null, e);
			model.addAttribute("result", new ResultVo(false, e.getMessage()));
		}

		return model;
	}

}