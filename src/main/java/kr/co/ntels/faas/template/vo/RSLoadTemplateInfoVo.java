package kr.co.ntels.faas.template.vo;

public class RSLoadTemplateInfoVo {
    private int template_id;
    private String name;
    private String runtime;
    private String version;
    private String des;
    private String top_yn;

    public RSLoadTemplateInfoVo() {}
    // 오바? 안오바?
    public RSLoadTemplateInfoVo(TemplateVo info) {
        setTemplate_id(info.getTemplate_id());
        setName(info.getName());
        setRuntime(info.getRuntime());
        setVersion(info.getVersion());
        setDes(info.getDes());
        setTop_yn(info.getTop_yn());
    }

    public int getTemplate_id() {
        return template_id;
    }

    public void setTemplate_id(int template_id) {
        this.template_id = template_id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getRuntime() {
        return runtime;
    }

    public void setRuntime(String runtime) {
        this.runtime = runtime;
    }

    public String getVersion() {
        return version;
    }

    public void setVersion(String version) {
        this.version = version;
    }


    public String getDes() {
        return des;
    }

    public void setDes(String des) {
        this.des = des;
    }

   
    public String getTop_yn() {
        return top_yn;
    }

    public void setTop_yn(String top_yn) {
        this.top_yn = top_yn;
    }
}
