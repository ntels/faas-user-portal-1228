package kr.co.ntels.faas.template.vo;

import kr.co.ntels.faas.trigger.vo.*;

public class RQSaveTemplateVo {
    // 함수이름 입력값
    private String functionDspName;

    private int templateId;

    private HttpTriggerVo httpTrigger;
    private DbTriggerVo dbTrigger;
    private StorageTriggerVo storageTrigger;
    private TimerTriggerVo timerTrigger;

    public int getTemplateId() {
        return templateId;
    }

    public void setTemplateId(int templateId) {
        this.templateId = templateId;
    }


    public String getFunctionDspName() {
        return functionDspName;
    }

    public void setFunctionDspName(String functionDspName) {
        this.functionDspName = functionDspName;
    }

    public HttpTriggerVo getHttpTrigger() {
        return httpTrigger;
    }

    public void setHttpTrigger(HttpTriggerVo httpTrigger) {
        this.httpTrigger = httpTrigger;
    }

    public DbTriggerVo getDbTrigger() {
        return dbTrigger;
    }

    public void setDbTrigger(DbTriggerVo dbTrigger) {
        this.dbTrigger = dbTrigger;
    }

    public StorageTriggerVo getStorageTrigger() {
        return storageTrigger;
    }

    public void setStorageTrigger(StorageTriggerVo storageTrigger) {
        this.storageTrigger = storageTrigger;
    }

    public TimerTriggerVo getTimerTrigger() {
        return timerTrigger;
    }

    public void setTimerTrigger(TimerTriggerVo timerTrigger) {
        this.timerTrigger = timerTrigger;
    }
}
