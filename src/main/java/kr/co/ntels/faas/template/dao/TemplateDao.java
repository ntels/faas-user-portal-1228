package kr.co.ntels.faas.template.dao;

import kr.co.ntels.faas.template.vo.RQLoadTemplateInfoVo;
import kr.co.ntels.faas.template.vo.RQLoadTemplateListVo;
import kr.co.ntels.faas.template.vo.TemplateVo;
import org.mybatis.spring.SqlSessionTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public class TemplateDao {

    @Autowired
    private SqlSessionTemplate sqlSessionTemplate;

    public List<TemplateVo> loadTemplateList(RQLoadTemplateListVo param) {
        return sqlSessionTemplate.selectList("faas.template.loadTemplateList", param);
    }

    public int loadTemplateListCount(RQLoadTemplateListVo param) {
        return sqlSessionTemplate.selectOne("faas.template.loadTemplateListCount", param);
    }

    public TemplateVo loadTemplateInfo(int template_id) {
        return sqlSessionTemplate.selectOne("faas.template.loadTemplateInfo", template_id);
    }
}
