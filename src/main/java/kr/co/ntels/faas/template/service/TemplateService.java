package kr.co.ntels.faas.template.service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.fasterxml.jackson.core.JsonProcessingException;

import kr.co.ntels.common.utils.CommonUtils;
import kr.co.ntels.common.utils.StringUtil;
import kr.co.ntels.common.utils.UserDetailsHelper;
import kr.co.ntels.common.vo.User;
import kr.co.ntels.faas.common.utils.CommCode;
import kr.co.ntels.faas.common.utils.CommCons;
import kr.co.ntels.faas.common.utils.CookieUtil;
import kr.co.ntels.faas.dao.FaasDao;
import kr.co.ntels.faas.function.service.FunctionService;
import kr.co.ntels.faas.function.vo.FunctionVo;
import kr.co.ntels.faas.gitlab.service.GitLabService;
import kr.co.ntels.faas.template.dao.TemplateDao;
import kr.co.ntels.faas.template.vo.TemplateVo;
import kr.co.ntels.faas.trigger.dao.TriggerDao;
import kr.co.ntels.faas.trigger.service.TriggerService;
import kr.co.ntels.faas.userException.ExceptionEx;

@Service
public class TemplateService {

	private @Value("${gitlab.url}") String GITLAB_URL;
	private @Value("${gitlab.username.admin}") String GITLAB_USERNAME_ADMIN;
	private @Value("${gitlab.password.admin}") String GITLAB_PASSWORD_ADMIN;

	Logger logger = LoggerFactory.getLogger(this.getClass());

	@Autowired
	private FunctionService functionService;

	@Autowired
	private TriggerService triggerService;

	@Autowired
	private GitLabService gitlabService;

	@Autowired
	private TemplateDao templateDao;
	
	@Autowired
	private TriggerDao triggerDao;

	@Autowired
	private FaasDao faasDao;

	public List<Map<String,Object>> loadTemplateList(Map<String,Object> param) {
		return faasDao.select("faas.template.loadTemplateList", param);
	}

	public int loadTemplateListCount(Map<String,Object> param) {
		return (int)faasDao.selectOne("faas.template.loadTemplateListCount", param);
	}

    public Object loadTemplateInfo(Map<String,Object> param) {
		return faasDao.selectOne("faas.template.loadTemplateInfo", param);
    }
    
    private Object loadTemplateInfo(int template_id) {
    	Map<String,Object> param = new HashMap<>();
    	param.put("template_id", template_id+"");
		return loadTemplateInfo(param);
    }
    
    public Map<String,Object> loadTriggerInfo(Map<String,Object> param) {
    	
    	Map<String,Object> MapTriggerInfo = new HashMap<>();
    	
    	MapTriggerInfo.put("DB", faasDao.select("faas.template.loadTriggerDBInfo", param));
    	MapTriggerInfo.put("HTTP", faasDao.select("faas.template.loadTriggerHTTPInfo", param));
    	MapTriggerInfo.put("Timer", faasDao.select("faas.template.loadTriggerTimerInfo", param));
    	MapTriggerInfo.put("Storage", faasDao.select("faas.template.loadTriggerStorageInfo", param));
		
		return MapTriggerInfo;
    }

	/**
     * git 생성. DB에 정보만 저장.
	 * 실제 knative service, trigger 생성은 구성화면에서 진행한다.
	 *
	 * @param param
	 * @throws JsonProcessingException
	 */
	@Transactional
	public int saveTemplate(Map<String,Object> param) throws Exception {
		User user = UserDetailsHelper.getAuthenticatedUser();

		int templateId = Integer.parseInt(param.get("templateId").toString());

		String faasUserId = user.getId();
		String namespace = CookieUtil.faasProjectId();
		String faasDisplayName = param.get("functionDspName").toString();
		String faasFunctionName = faasDisplayName + CommonUtils.getRandomString(10);

		String gitGroupPath = namespace;
		String gitProjectName = faasFunctionName;
		String gitFullPath = gitGroupPath + "/" + gitProjectName;
		String gitUserId = faasUserId;
		String gitUserRole = StringUtil.isNullToString(user.getRole());

		// git
		createGitGround(gitGroupPath, gitProjectName, gitUserId, gitUserRole);
		commitTemplateCopy(templateId, gitFullPath);

		Map<String,Object> tp = (Map<String,Object>)loadTemplateInfo(templateId);
		if(tp == null){
			logger.error("** no template info. templateId({})", templateId);
			throw new Exception("no template info");
		}

		FunctionVo function = saveFunction(faasUserId, namespace, faasFunctionName,
				faasDisplayName, gitFullPath, tp);
		
		Map<String,Object> mapTriggerInfo = (Map<String,Object>)param.get("triggerInfo");
		if(mapTriggerInfo != null)
		{
			triggerService.saveTemplateHttpTrigger((List<Map<String,Object>>)mapTriggerInfo.get("Http"), function);
			triggerService.saveTemplateDBTrigger((List<Map<String,Object>>)mapTriggerInfo.get("DB"), function);
			triggerService.saveTemplateTimerTrigger((List<Map<String,Object>>)mapTriggerInfo.get("Timer"), function );
			//triggerService.saveStorageTrigger((List<Map<String,Object>>)mapTriggerInfo.get("DB"), function);
		}
		return function.getFunction_id();
	}

	private void commitTemplateCopy(int templateId, String gitFullPath) throws Exception {
		
		Map<String,Object> template = (Map<String,Object>)loadTemplateInfo(templateId);
		if(template != null)
		{
			List<Map<String, Object>> gitfiles = gitlabService.checkOutFromProject(template.get("git_url").toString());
	
			if(gitlabService.commitToProject(gitfiles, gitFullPath, GITLAB_USERNAME_ADMIN, GITLAB_PASSWORD_ADMIN) == null){
			    throw new ExceptionEx("template source copy fail.");
			}
		}
		else
			throw new ExceptionEx("template not found");
	}

	private FunctionVo saveFunction(String faasUserId, String namespace, String faasFunctionName,
									String faasDisplayName, String gitFullPath, Map<String,Object> tp) {
		FunctionVo function = new FunctionVo();
		function.setName(faasFunctionName);
		function.setRuntime(tp.get("runtime").toString());
		function.setVersion(tp.get("version").toString());
		function.setNamespace(namespace);
		function.setDsp_name(faasDisplayName);
		function.setDes("");
		function.setGit_url(gitFullPath);
		function.setGit_usr(faasUserId);
		function.setStat_cd(CommCode.Stat.INIT);
		function.setReg_step_cd(CommCode.RegStep.STEP1);
		function.setReg_usr(faasUserId);
		function.setMod_usr(faasUserId);

		triggerDao.insertFunctionVo(function);

		return function;
	}

	private void createGitGround(String gitGroupPath, String gitProjectName, String gitUserId, String gitUserRole) throws Exception {
		if(gitlabService.projectExists(gitGroupPath, gitProjectName)){
			logger.error("** git exist. - gitGroupPath({}), gitProjectName({})", gitGroupPath, gitProjectName);
			throw new Exception("already git exists");
		}

		if(!gitlabService.groupExists(gitGroupPath)){
			gitlabService.createGroup(gitGroupPath);
		}

		if(!gitlabService.userExists(gitUserId)){
			String gitUserPassword = CommonUtils.getRandomString(12);
			gitlabService.createUser(gitUserId, gitUserPassword);
			Map<String, Object> gp = new HashMap<>();
			gp.put("git_usr", gitUserId);
			gp.put("git_pwd", gitUserPassword);
			gp.put("hubpop_userName", gitUserId);
			gp.put("hubpop_password", gitUserPassword);
			gp.put("hubpop_userRole", gitUserRole);
			functionService.insertGitLabUser(gp);
		}

		if(!gitlabService.userExistsinGroup(gitGroupPath, gitUserId)){
			gitlabService.addUserToGroup(gitGroupPath, gitUserId);
		}else{
			// TODO createProject 할때 깃유저 password가 필요하다면 가져옴. 현재는 깃어드민으로 생성.
		}

		if(!gitlabService.createProject(gitGroupPath, gitProjectName)) {
			logger.error("git creating fail - gitGroupPath({}), gitProjectName({})", gitGroupPath, gitProjectName);
			throw new Exception("git creating fail");
		}
	}


}
