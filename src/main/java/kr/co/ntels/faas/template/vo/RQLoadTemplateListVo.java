package kr.co.ntels.faas.template.vo;

public class RQLoadTemplateListVo extends BaseListVo {
    private int curpage;
    private String search_name;

    public int getCurpage() {
        return curpage;
    }

    public void setCurpage(int curpage) {
        this.curpage = curpage;
    }

    public String getSearch_name() {
        return search_name;
    }

    public void setSearch_name(String search_name) {
        this.search_name = search_name;
    }
}
