package kr.co.ntels.faas.template.vo;

public class TemplatePagingVo extends BasePagingVo {
    private int curpage;
    private int lastpage;

    private int totalCount;

    private int limit = 12;
    private int offset;

    public void settingLastPage(int totalCount) {
        int lastPage = totalCount / limit;

        if (totalCount % limit > 0) {
            lastPage++;
        }

        setTotalCount(totalCount);
        setLastpage(lastPage);
    }

    public int getCurpage() {
        return curpage;
    }

    public void setCurpage(int curpage) {
        this.curpage = curpage;
    }

    public int getLastpage() {
        return lastpage;
    }

    public void setLastpage(int lastpage) {
        this.lastpage = lastpage;
    }

    public int getTotalCount() {
        return totalCount;
    }

    public void setTotalCount(int totalCount) {
        this.totalCount = totalCount;
    }

    public int getLimit() {
        return limit;
    }

    public void setLimit(int limit) {
        this.limit = limit;
    }

    public int getOffset() {
        return offset;
    }

    public void setOffset(int offset) {
        this.offset = offset;
    }
}
