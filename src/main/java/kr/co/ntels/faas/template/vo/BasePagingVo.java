package kr.co.ntels.faas.template.vo;

public class BasePagingVo {
    private int curpage;
    private int limit = 12;

    public int getCurpage() {
        return curpage;
    }

    public void setCurpage(int curpage) {
        this.curpage = curpage;
    }

    public int getLimit() {
        return limit;
    }

    public void setLimit(int limit) {
        this.limit = limit;
    }
}
