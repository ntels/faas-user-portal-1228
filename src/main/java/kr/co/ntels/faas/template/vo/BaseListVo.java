package kr.co.ntels.faas.template.vo;

public class BaseListVo {
    private int limit = 12;
    private int offset;

    public void settingOffsetInPage(BasePagingVo paging) {
        limit = paging.getLimit();
        offset = (paging.getCurpage() - 1) * paging.getLimit();
    }

    public int getLimit() {
        return limit;
    }

    public int getOffset() {
        return offset;
    }
}
