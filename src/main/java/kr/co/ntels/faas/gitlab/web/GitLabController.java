package kr.co.ntels.faas.gitlab.web;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Random;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import kr.co.ntels.common.utils.CommonUtils;
import kr.co.ntels.common.web.HubpopController;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.fasterxml.jackson.databind.ObjectMapper;

import kr.co.ntels.faas.constant.FaaSCommCD;
import kr.co.ntels.faas.function.service.FunctionService;
import kr.co.ntels.faas.gitlab.service.GitLabService;

@Controller
public class GitLabController {
	
	@Autowired FunctionService service;
	@Autowired GitLabService gitLabService;
	private @Value("${gitlab.sample.group.path}") String GITLAB_SAMPLE_GROUP_PATH;
	private @Value("${gitlab.admin.group.path}") String GITLAB_ADMIN_GROUP_PATH;
	
	@ResponseBody
	@RequestMapping("/service/regFunction.json")
	public String regFunction(HttpServletRequest request, HttpServletResponse response, @RequestParam String param) throws Exception {
		Map<String, Object> paramMap = new ObjectMapper().readValue(param, Map.class);
		
		String groupPath = FaaSCommCD.getNamespace(request);
		String userName = paramMap.get("user_id").toString();
		String password = CommonUtils.getRandomString(12);
		String dsp_name = paramMap.get("function_name").toString();
		String projectName = dsp_name + CommonUtils.getRandomString(10);
		
		Map<String, Object> functionMap = new HashMap<String, Object>();
		functionMap.put("namespace", groupPath);
		functionMap.put("dsp_name", dsp_name);
		
		List<Map<String, Object>> functionList = service.getFunction(functionMap);
		boolean isExists = functionList.size() > 0 ? true : false;
		
		Map<String, Object> dataMap = new HashMap<String, Object>();
		dataMap.put("hubpop_userName", paramMap.get("user_name").toString());
		dataMap.put("hubpop_password", "hubpop!123");
		dataMap.put("hubpop_userRole", paramMap.get("user_role").toString());
		dataMap.put("name", projectName);
		dataMap.put("runtime", paramMap.get("runtime_type").toString());
		dataMap.put("version", paramMap.get("runtime_version").toString());
		dataMap.put("namespace", groupPath);
		dataMap.put("dsp_name", dsp_name);
		dataMap.put("des", paramMap.get("function_des").toString());
		dataMap.put("git_url", groupPath + "/" + projectName);
		dataMap.put("git_usr", userName);
		dataMap.put("stat_cd", "INIT");
		dataMap.put("git_pwd", password);
		dataMap.put("reg_usr", userName);
		dataMap.put("mod_usr", userName);
		
		Map<String, Object> returnMap = new HashMap<String, Object>();
		
		if(isExists) {
			returnMap.put("projectExists", "true");
		} else {
			createGroup(groupPath);
			
			Map<String, Object> tempMap = createUser(userName, password, dataMap);
			dataMap.put("git_usr", tempMap.get("git_usr").toString());
			dataMap.put("git_pwd", tempMap.get("git_pwd").toString());
			
			addUserToGroup(groupPath, userName);
			createProject(groupPath, projectName, dataMap);
			
			// List<Map<String, Object>> tempList = service.getFunctionId(dataMap);
			// returnMap.put("functionId", tempList.get(0).get("function_id").toString());
			returnMap.put("functionId", dataMap.get("function_id").toString()); 
		}
		
		ObjectMapper mapper = new ObjectMapper();
		
		return mapper.writeValueAsString(returnMap);
	}
	
	public void createGroup(String groupPath) throws Exception {
		if(gitLabService.groupExists(groupPath)) {}
		else {
			gitLabService.createGroup(groupPath);
		}
	}
	
	public Map<String, Object> createUser(String userName, String password, Map<String, Object> dataMap) throws Exception {
		Map<String, Object> returnMap = new HashMap<String, Object>();
		
		if(gitLabService.userExists(userName)) {
			List<Map<String, Object>> tempList = service.getGitLabUser(dataMap);
			
			returnMap.put("git_usr", tempList.get(0).get("git_usr").toString());
			returnMap.put("git_pwd", tempList.get(0).get("git_pwd").toString());
		} else {
			gitLabService.createUser(userName, password);
			service.insertGitLabUser(dataMap);
			
			returnMap.put("git_usr", userName);
			returnMap.put("git_pwd", password);
		}
		
		return returnMap;
	}
	
	public void addUserToGroup(String groupPath, String userName) throws Exception {
		if(gitLabService.userExistsinGroup(groupPath, userName)) {}
		else {
			gitLabService.addUserToGroup(groupPath, userName);
		}
	}
	
	public void createProject(String groupPath, String projectName, Map<String, Object> dataMap) throws Exception {
		if(gitLabService.createProject(groupPath, projectName)) {
			service.insertRegFunction(dataMap);
		}
	}
	
	public List<Map<String, Object>> checkOutFromProject(String projectPath, String runtime) throws Exception {
		List<Map<String, Object>> returnList = gitLabService.checkOutFromProject(projectPath);
		
		if(returnList.size() > 0) {}
		else {
			returnList = gitLabService.checkOutFromProject(GITLAB_SAMPLE_GROUP_PATH, runtime);
		}
		
		return returnList;
	}
	
	public void commitToProject(List<Map<String, Object>> fileList, String projectPath, String userName, String password) throws Exception {
		gitLabService.commitToProject(fileList, projectPath, userName, password);
	}
}