package kr.co.ntels.faas.gitlab.service;

import java.io.InputStream;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.io.IOUtils;
import org.gitlab4j.api.GitLabApi;
import org.gitlab4j.api.GitLabApiException;
import org.gitlab4j.api.models.AccessLevel;
import org.gitlab4j.api.models.Commit;
import org.gitlab4j.api.models.CommitAction;
import org.gitlab4j.api.models.CommitAction.Action;
import org.gitlab4j.api.models.Diff;
import org.gitlab4j.api.models.Group;
import org.gitlab4j.api.models.Member;
import org.gitlab4j.api.models.Project;
import org.gitlab4j.api.models.Repository;
import org.gitlab4j.api.models.RepositoryFile;
import org.gitlab4j.api.models.TreeItem;
import org.gitlab4j.api.models.User;
import org.gitlab4j.api.models.Visibility;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import kr.co.ntels.faas.userException.ExceptionEx;

@Service
public class GitLabService {

	private @Value("${gitlab.url}") String GITLAB_URL;
	private @Value("${gitlab.username.admin}") String GITLAB_USERNAME_ADMIN;
	private @Value("${gitlab.password.admin}") String GITLAB_PASSWORD_ADMIN;
	
	// 관리자 계정으로 로그인
	public GitLabApi loginGitLabApiWithAdmin() throws Exception {
		
		GitLabApi gitLabApi;
		
		try {
			String gitLabUrl = GITLAB_URL;
			String gitLabUserName = GITLAB_USERNAME_ADMIN;
			String gitLabPassword = GITLAB_PASSWORD_ADMIN;
			
			gitLabApi = GitLabApi.login(gitLabUrl, gitLabUserName, gitLabPassword);
		} catch (Exception e) {
			throw e;
		}
		
		return gitLabApi;
	}
	
	// 유저 계정으로 로그인
	public GitLabApi loginGitLabApiWithUser(String userName, String password) throws Exception {
		
		GitLabApi gitLabApi;
		
		try {
			String gitLabUrl = GITLAB_URL;
			String gitLabUserName = userName;
			String gitLabPassword = password;
			
			gitLabApi = GitLabApi.login(gitLabUrl, gitLabUserName, gitLabPassword);
		} catch (Exception e) {
			throw e;
		}
		
		return gitLabApi;
	}
	
	// 그룹 생성
	public boolean createGroup(String groupPath) throws Exception {
		
		try {
			String gitLabUrl = GITLAB_URL;
			String gitLabUserName = GITLAB_USERNAME_ADMIN;
			String gitLabPassword = GITLAB_PASSWORD_ADMIN;
			
			GitLabApi gitLabApi = GitLabApi.login(gitLabUrl, gitLabUserName, gitLabPassword);
			
			Group group = new Group();
			group.setName(groupPath);
			group.setPath(groupPath);
			group.setDescription("");
			group.setVisibility(Visibility.PRIVATE);
			group.setLfsEnabled(false);
			group.setRequestAccessEnabled(false);
			group.setParentId(null);
			
			gitLabApi.getGroupApi().addGroup(group);
			
			return true;
		} catch (Exception e) {
			throw e;
		}
	}
	
	// 그룹 삭제
	public boolean deleteGroup(String groupPath) throws Exception {
		
		try {
			String gitLabUrl = GITLAB_URL;
			String gitLabUserName = GITLAB_USERNAME_ADMIN;
			String gitLabPassword = GITLAB_PASSWORD_ADMIN;
			
			GitLabApi gitLabApi = GitLabApi.login(gitLabUrl, gitLabUserName, gitLabPassword);
			
			gitLabApi.getGroupApi().deleteGroup(groupPath);
			
			return true;
		} catch (Exception e) {
			throw e;
		}
	}
	
	// 그룹 존재 확인
	public boolean groupExists(String groupPath) throws Exception {
		
		try {
			String gitLabUrl = GITLAB_URL;
			String gitLabUserName = GITLAB_USERNAME_ADMIN;
			String gitLabPassword = GITLAB_PASSWORD_ADMIN;
			
			GitLabApi gitLabApi = GitLabApi.login(gitLabUrl, gitLabUserName, gitLabPassword);
			
			Group group = gitLabApi.getGroupApi().getGroup(groupPath);
			
			if(group != null) {
				return true;
			}
		} catch (GitLabApiException e) {
			// 그룹이 존재하지 않은 경우 처리
			if(e.getHttpStatus() == 404 || e.getMessage().toLowerCase().contains("group not found")) {
				return false;
			}
			
			throw e;
		} catch (Exception e) {
			throw e;
		}
		
		return false;
	}
	
	// 그룹에 유저 추가
	public boolean addUserToGroup(String groupPath, String userName) throws Exception {
		
		try {
			String gitLabUrl = GITLAB_URL;
			String gitLabUserName = GITLAB_USERNAME_ADMIN;
			String gitLabPassword = GITLAB_PASSWORD_ADMIN;
			
			GitLabApi gitLabApi = GitLabApi.login(gitLabUrl, gitLabUserName, gitLabPassword);
			
			User user = gitLabApi.getUserApi().getUser(userName);
			
			Member member = gitLabApi.getGroupApi().addMember(groupPath, user.getId(), AccessLevel.MAINTAINER);
			
			if(member != null) {
				return true;
			}
		} catch (Exception e) {
			throw e;
		}
		
		return false;
	}
	
	// 그룹에서 유저 삭제
	public boolean deleteUserFromGroup(String groupPath, String userName) throws Exception {
		
		try {
			String gitLabUrl = GITLAB_URL;
			String gitLabUserName = GITLAB_USERNAME_ADMIN;
			String gitLabPassword = GITLAB_PASSWORD_ADMIN;
			
			GitLabApi gitLabApi = GitLabApi.login(gitLabUrl, gitLabUserName, gitLabPassword);
			
			User user = gitLabApi.getUserApi().getUser(userName);
			
			gitLabApi.getGroupApi().removeMember(groupPath, user.getId());
			
			return true;
		} catch (Exception e) {
			throw e;
		}
	}
	
	// 그룹에서 유저 존재 확인
	public boolean userExistsinGroup(String groupPath, String userName) throws Exception {
		
		try {
			String gitLabUrl = GITLAB_URL;
			String gitLabUserName = GITLAB_USERNAME_ADMIN;
			String gitLabPassword = GITLAB_PASSWORD_ADMIN;
			
			GitLabApi gitLabApi = GitLabApi.login(gitLabUrl, gitLabUserName, gitLabPassword);
			
			User user = gitLabApi.getUserApi().getUser(userName);
			
			Member member = gitLabApi.getGroupApi().getMember(groupPath, user.getId());
			
			if(member != null) {
				return true;
			}
		} catch (GitLabApiException e) {
			// 그룹에 유저가 존재하지 않은 경우 처리
			if(e.getHttpStatus() == 404 || e.getMessage().toLowerCase().contains("not found")) {
				return false;
			}
			
			throw e;
		} catch (Exception e) {
			throw e;
		}
		
		return false;
	}
	
	// 유저 생성
	public boolean createUser(String userName, String password) throws Exception {
		
		try {
			String gitLabUrl = GITLAB_URL;
			String gitLabUserName = GITLAB_USERNAME_ADMIN;
			String gitLabPassword = GITLAB_PASSWORD_ADMIN;
			
			GitLabApi gitLabApi = GitLabApi.login(gitLabUrl, gitLabUserName, gitLabPassword);
			
			String email = userName + "@hubpop.com";
			Date date = new Date();
			
			User user = new User();
			user.setName(userName);
			user.setUsername(userName);
			user.setEmail(email);
			user.setConfirmedAt(date);
			user.setSkipConfirmation(true);
			
			gitLabApi.getUserApi().createUser(user, password, false);
			
			return true;
		} catch (Exception e) {
			throw e;
		}
	}
	
	// 유저 삭제
	public boolean deleteUser(String userName) throws Exception {
		
		try {
			String gitLabUrl = GITLAB_URL;
			String gitLabUserName = GITLAB_USERNAME_ADMIN;
			String gitLabPassword = GITLAB_PASSWORD_ADMIN;
			
			GitLabApi gitLabApi = GitLabApi.login(gitLabUrl, gitLabUserName, gitLabPassword);
			
			User user = gitLabApi.getUserApi().getUser(userName);
			
			gitLabApi.getUserApi().deleteUser(user.getId());
			
			return true;
		} catch (Exception e) {
			throw e;
		}
	}
	
	// 유저 존재 확인
	public boolean userExists(String userName) throws Exception {
		
		try {
			String gitLabUrl = GITLAB_URL;
			String gitLabUserName = GITLAB_USERNAME_ADMIN;
			String gitLabPassword = GITLAB_PASSWORD_ADMIN;
			
			GitLabApi gitLabApi = GitLabApi.login(gitLabUrl, gitLabUserName, gitLabPassword);
			
			User user = gitLabApi.getUserApi().getUser(userName);
			
			if(user != null) {
				return true;
			}
		} catch (GitLabApiException e) {
			throw e;
		} catch (Exception e) {
			throw e;
		}
		
		return false;
	}
	
	// 프로젝트 생성
	public boolean createProject(String groupPath, String projectName) throws Exception {
		
		try {
			String gitLabUrl = GITLAB_URL;
			String gitLabUserName = GITLAB_USERNAME_ADMIN;
			String gitLabPassword = GITLAB_PASSWORD_ADMIN;
			
			GitLabApi gitLabApi = GitLabApi.login(gitLabUrl, gitLabUserName, gitLabPassword);
			
			Group group = gitLabApi.getGroupApi().getGroup(groupPath);
			
			gitLabApi.getProjectApi().createProject(group.getId(), projectName);
			
			return true;
		} catch (Exception e) {
			throw e;
		}
	}
	
	// 프로젝트 삭제
	public boolean deleteProject(String groupPath, String projectName) throws Exception {
		
		try {
			String gitLabUrl = GITLAB_URL;
			String gitLabUserName = GITLAB_USERNAME_ADMIN;
			String gitLabPassword = GITLAB_PASSWORD_ADMIN;
			
			GitLabApi gitLabApi = GitLabApi.login(gitLabUrl, gitLabUserName, gitLabPassword);
			
			gitLabApi.getProjectApi().deleteProject(groupPath + "/" + projectName);
			
			return true;
		} catch(GitLabApiException e) {
			// 프로젝트가 존재하지 않은 경우 처리
			if(e.getHttpStatus() == 404 || e.getMessage().toLowerCase().contains("project not found")) {
				return true;
			}
			
			throw e;
		} catch (Exception e) {
			throw new ExceptionEx(e.getMessage());
		}
	}
	
	// 프로젝트 존재 확인
	public boolean projectExists(String groupPath, String projectName) throws Exception {
		
		try {
			String gitLabUrl = GITLAB_URL;
			String gitLabUserName = GITLAB_USERNAME_ADMIN;
			String gitLabPassword = GITLAB_PASSWORD_ADMIN;
			
			GitLabApi gitLabApi = GitLabApi.login(gitLabUrl, gitLabUserName, gitLabPassword);
			
			Project project = gitLabApi.getProjectApi().getProject(groupPath + "/" + projectName);
			
			if(project != null) {
				return true;
			}
		} catch (GitLabApiException e) {
			// 프로젝트가 존재하지 않은 경우 처리
			if(e.getHttpStatus() == 404 || e.getMessage().toLowerCase().contains("project not found")) {
				return false;
			}
			
			throw e;
		} catch (Exception e) {
			throw e;
		}
		
		return false;
	}
	
	// 프로젝트에서 파일 체크 아웃 #1
	// fileList = [{fileName=<파일_이름1>, fileContent=<파일_내용1>}, {fileName=<파일_이름2>, fileContent=<파일_내용2>}, ...]
	public List<Map<String, Object>> checkOutFromProject(String groupPath, String projectName) throws Exception {
		
		List<Map<String, Object>> fileList = new ArrayList<Map<String, Object>>();
		
		try {
			String gitLabUrl = GITLAB_URL;
			String gitLabUserName = GITLAB_USERNAME_ADMIN;
			String gitLabPassword = GITLAB_PASSWORD_ADMIN;
			
			String projectPath = groupPath + "/" + projectName;
			
			GitLabApi gitLabApi = GitLabApi.login(gitLabUrl, gitLabUserName, gitLabPassword);
			
			List<String> filePathList = getFilePathList(projectPath, "");
			
			for(int i=0; i<filePathList.size(); i++) {
				RepositoryFile repositoryFile = gitLabApi.getRepositoryFileApi().getFile(projectPath, filePathList.get(i), "master");
				
				HashMap<String, Object> fileMap = new HashMap<String, Object>();
				fileMap.put("fileName", repositoryFile.getFilePath());
				fileMap.put("fileContent", repositoryFile.getDecodedContentAsString());
				
				fileList.add(fileMap);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		return fileList;
	}
	
	// 프로젝트에서 파일 체크 아웃 #2
	// fileList = [{fileName=<파일_이름1>, fileContent=<파일_내용1>}, {fileName=<파일_이름2>, fileContent=<파일_내용2>}, ...]
	public List<Map<String, Object>> checkOutFromProject(String projectPath) throws Exception {
		
		List<Map<String, Object>> fileList = new ArrayList<Map<String, Object>>();
		
		try {
			String gitLabUrl = GITLAB_URL;
			String gitLabUserName = GITLAB_USERNAME_ADMIN;
			String gitLabPassword = GITLAB_PASSWORD_ADMIN;
			
			GitLabApi gitLabApi = GitLabApi.login(gitLabUrl, gitLabUserName, gitLabPassword);
			
			List<String> filePathList = getFilePathList(projectPath, "");
			
			for(int i=0; i<filePathList.size(); i++) {
				RepositoryFile repositoryFile = gitLabApi.getRepositoryFileApi().getFile(projectPath, filePathList.get(i), "master");
				
				HashMap<String, Object> fileMap = new HashMap<String, Object>();
				fileMap.put("fileName", repositoryFile.getFilePath());
				fileMap.put("fileContent", repositoryFile.getDecodedContentAsString());
				
				fileList.add(fileMap);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		return fileList;
	}
	
	// 프로젝트에 파일 커밋
	// fileList = [{fileName=<파일_이름1>, fileContent=<파일_내용1>}, {fileName=<파일_이름2>, fileContent=<파일_내용2>}, ...]
	public boolean commitToProject(List<HashMap<String, Object>> fileList, String groupPath, String projectName, String userName, String password) throws Exception {
		
		try {
			String gitLabUrl = GITLAB_URL;
			String gitLabUserName = GITLAB_USERNAME_ADMIN;
			String gitLabPassword = GITLAB_PASSWORD_ADMIN;
			
			String projectPath = groupPath + "/" + projectName;
			
			GitLabApi gitLabApi = GitLabApi.login(gitLabUrl, userName, password);
			
			List<String> filePathList = getFilePathList(projectPath, "");
			
			List<String> fileNameList = new ArrayList<>();
			
			for(Map map : fileList) {
				fileNameList.add(map.get("fileName").toString());
			}
			
			List<CommitAction> commitActionList = new ArrayList<>();
			
			for(int i=0; i<filePathList.size(); i++) {
				if(!fileNameList.contains(filePathList.get(i).toString())) {
					CommitAction commitAction = new CommitAction();
					
					commitAction.setAction(Action.DELETE);
					commitAction.setFilePath(filePathList.get(i).toString());
					
					commitActionList.add(commitAction);
				}
			}
			
			for(int i=0; i<fileList.size(); i++) {
				CommitAction commitAction = new CommitAction();
				
				if(filePathList.contains(fileList.get(i).get("fileName").toString()))
					commitAction.setAction(Action.UPDATE);
				else
					commitAction.setAction(Action.CREATE);
				
				commitAction.setFilePath(fileList.get(i).get("fileName").toString());
				commitAction.setContent(fileList.get(i).get("fileContent").toString());
				
				commitActionList.add(commitAction);
			}
			
			gitLabApi.getCommitsApi().createCommit(projectPath, "master", "commit by " + userName, null, userName, userName, commitActionList);
			
			return true;
		} catch (Exception e) {
			throw e;
		}
	}
	
	// 프로젝트에 파일 커밋
	// fileList = [{fileName=<파일_이름1>, fileContent=<파일_내용1>}, {fileName=<파일_이름2>, fileContent=<파일_내용2>}, ...]
	public Commit commitToProject(List<Map<String, Object>> fileList, String projectPath, String userName, String password) throws Exception {
		
		try {
			String gitLabUrl = GITLAB_URL;
			String gitLabUserName = GITLAB_USERNAME_ADMIN;
			String gitLabPassword = GITLAB_PASSWORD_ADMIN;
			
			GitLabApi gitLabApi = GitLabApi.login(gitLabUrl, userName, password);
			
			List<String> filePathList = getFilePathList(projectPath, "");
			
			List<String> fileNameList = new ArrayList<>();
			
			for(Map map : fileList) {
				fileNameList.add(map.get("fileName").toString());
			}
			
			List<CommitAction> commitActionList = new ArrayList<>();
			
			for(int i=0; i<filePathList.size(); i++) {
				if(!fileNameList.contains(filePathList.get(i).toString())) {
					CommitAction commitAction = new CommitAction();
					
					commitAction.setAction(Action.DELETE);
					commitAction.setFilePath(filePathList.get(i).toString());
					
					commitActionList.add(commitAction);
				}
			}
			
			for(int i=0; i<fileList.size(); i++) {
				CommitAction commitAction = new CommitAction();
				
				if(filePathList.contains(fileList.get(i).get("fileName").toString()))
					commitAction.setAction(Action.UPDATE);
				else
					commitAction.setAction(Action.CREATE);
				
				commitAction.setFilePath(fileList.get(i).get("fileName").toString());
				commitAction.setContent(fileList.get(i).get("fileContent").toString());
				
				commitActionList.add(commitAction);
			}
			
			Commit commit = gitLabApi.getCommitsApi().createCommit(projectPath, "master", "commit by " + userName, null, userName, userName, commitActionList);
			
			return commit;
		} catch (Exception e) {
			throw e;
		}
	}
	
	// project에 파일 경로 목록 가져오기("."으로 시작하는 숨김 파일은 제외)
	// filePathList = [<파일_경로1>, <파일_경로2>, ...]
	public List<String> getFilePathList(String projectPath, String filePath) throws Exception {
		
		List<String> filePathList = new ArrayList<String>();
		
		try {
			String gitLabUrl = GITLAB_URL;
			String gitLabUserName = GITLAB_USERNAME_ADMIN;
			String gitLabPassword = GITLAB_PASSWORD_ADMIN;
			
			GitLabApi gitLabApi = GitLabApi.login(gitLabUrl, gitLabUserName, gitLabPassword);
			
			List<TreeItem> treeItemList = gitLabApi.getRepositoryApi().getTree(projectPath, filePath, null);
			
			for(int i=0; i<treeItemList.size(); i++) {
				if(String.valueOf(treeItemList.get(i).getType()).toLowerCase().equals("blob") && !(String.valueOf(treeItemList.get(i).getName()).startsWith("."))) {
					filePathList.add(treeItemList.get(i).getPath().toString());
				} else if(String.valueOf(treeItemList.get(i).getType()).toLowerCase().equals("tree") && !(String.valueOf(treeItemList.get(i).getName()).startsWith("."))) {
					List<String> tempFilePathList = getFilePathList(projectPath, treeItemList.get(i).getPath().toString());
					
					filePathList.addAll(tempFilePathList);
				}
			}
		} catch (GitLabApiException e) {
			// tree가 존재하지 않은 경우 처리
			if(e.getHttpStatus() == 404 || e.getMessage().toLowerCase().contains("tree not found")) {
				return filePathList;
			}
			
			throw e;
		} catch (Exception e) {
			throw e;
		}
		
		return filePathList;
	}
	
	public List<Diff> syncDiffGitlab(String projectId , String revisionId) throws Exception {

		String gitLabUrl = GITLAB_URL;
		String gitLabUserName = GITLAB_USERNAME_ADMIN;
		String gitLabPassword = GITLAB_PASSWORD_ADMIN;
		
		HashMap<String, Object> resultMap = new HashMap<String, Object>();
		
		GitLabApi gitLabApi = GitLabApi.login(gitLabUrl, gitLabUserName, gitLabPassword);
		List<Diff> diffList = gitLabApi.getCommitsApi().getDiff(projectId, revisionId);
		
		return diffList;
	}

	public String getRawContent(String projectId, String filePath, String revisionId) throws Exception {
		GitLabApi gitLabApi = GitLabApi.login(GITLAB_URL, GITLAB_USERNAME_ADMIN, GITLAB_PASSWORD_ADMIN);
		RepositoryFile repositoryFile = gitLabApi.getRepositoryFileApi().getFileInfo(projectId, filePath, revisionId);
		String rawContent = IOUtils.toString(gitLabApi.getRepositoryApi().getRawBlobContent(projectId, repositoryFile.getBlobId()));
		
		return rawContent;
	}

	public List<Map<String, Object>> getDiffAndRawContent(String projectId, String revisionId) throws Exception{
		List<Map<String, Object>> resultList = new ArrayList<>();
		List<Diff> diffList = this.syncDiffGitlab(projectId, revisionId);
		diffList.forEach((Diff diff) -> {
				try {
					Map map = new HashMap<>();
					
					map.put("diff", diff.getDiff());
					map.put("oldPath", diff.getOldPath());
					map.put("newPath", diff.getNewPath());
					map.put("content", this.getRawContent(projectId, diff.getNewPath(), revisionId));

					resultList.add(map);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		);
		return resultList;
	}
}
