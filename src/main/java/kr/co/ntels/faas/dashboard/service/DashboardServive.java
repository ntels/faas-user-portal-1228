package kr.co.ntels.faas.dashboard.service;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import kr.co.ntels.faas.dao.FaasDao;

/**
  * @FileName : DashboardServive.java
  * @Project : faas-user-portal
  * @Date : 2020. 6. 9. 
  * @작성자 : sig61 - 백광욱
  * @변경이력 :
  * @프로그램 설명 :
  */
@Service
public class DashboardServive {
	
	@Autowired 
	FaasDao dao;
	
	public List<Map<String, Object>> getFunctionList(Map<String,Object> param)
	{
		return dao.select("faas.dashboard.getFunctionTop5", param);
	}
	
	public List<Map<String, Object>> getCallList(Map<String,Object> param)
	{
		return dao.select("faas.dashboard.getCallList", param);
	}
	
	public Map<String, Object> getRuntimeInfo(Map<String,Object> param)
	{
		Map<String, Object> ret = new HashMap<>();
		List<Map<String, Object>> list = dao.select("faas.dashboard.getRuntimeInfo", param);
		
		int total = 0;
		ret.put("nodejs", "0");
		ret.put("python", "0");
		for(Map<String, Object> map : list)
		{
			String runtime = map.get("runtime").toString();
			int cnt = Integer.parseInt(map.get("cnt").toString());
			
			if("nodejs".equals(runtime) || "python".equals(runtime))
			{
				ret.put(runtime, cnt+"");
				total += cnt;
			}
		}
		ret.put("total", total+"");
		
		return ret;
	}
	
	public Map<String, Object> getTriggerData(Map<String,Object> param)
	{
		Map<String, Object> ret = new HashMap<>();
		Map<String, Object> triggerInfo = getTriggerInfo(param);
		List<Map<String, Object>> chartData = getChartData(triggerInfo);
		ret.put("triggerInfo", triggerInfo);
		ret.put("chartData", chartData);
		
		return ret;
	}
	
	
	private Map<String, Object> getTriggerInfo(Map<String,Object> param)
	{
		Map<String, Object> ret = new HashMap<>();
		List<Map<String, Object>> list = dao.select("faas.dashboard.getTriggerInfo", param);
		
		int total = 0;
		ret.put("http", 0);
		ret.put("ceph", 0);
		ret.put("mongodb", 0);
		ret.put("postgresql", 0);
		ret.put("timer", 0);
		for(Map<String, Object> map : list)
		{
			String trigger_tp = map.get("trigger_tp").toString();
			int cnt = Integer.parseInt(map.get("cnt").toString());
			
			ret.put(trigger_tp, cnt);
			total += cnt;
		}
		ret.put("total", total);
		
		return ret;
	} 
	
	private List<Map<String, Object>> getChartData(Map<String,Object> param)
	{
		List<String> items = new ArrayList<>(Arrays.asList("HTTP", "Ceph", "MongoDB", "PostgreSQL", "timer"));
		List<Map<String, Object>> list = new ArrayList<>();
		for(String item : items)
		{
			Map<String, Object> map = new HashMap<>();
			map.put("title", item.equals("timer") ? "타이머" : item) ;
			map.put("value", param.get(item.toLowerCase()));
			list.add(map);
		}
		
		return list;
	}
	
	public static String getMonthList()
	{
		return getMonthList(3);
	}
	
	public static String getMonth()
	{
		return getMonthList(1);
	}
	
	private static String getMonthList(int cnt)
	{
		List<String> list = new ArrayList<>();
		long t = getUnixTime();
		int iYear = Integer.parseInt(getUnixTimeToString(t,"yyyy"));
		int iMonth = Integer.parseInt(getUnixTimeToString(t,"MM"));
		for(int i=0;i<cnt;i++)
		{
			iMonth = iMonth - 1;
			if(iMonth == 0) { 
				iMonth = 12;
				iYear--;
			}
			
			list.add(String.format("'%d.%02d'", iYear,iMonth));
		}
		if(cnt == 1)
			return list.get(0);
		return list.toString();
	}
	
	private static long getUnixTime()
	{
		 return System.currentTimeMillis()/1000;// - 9*3600 ;
	}
	
	private static String getUnixTimeToString( long time, String format)
	{
		 Date date = new Date(time*1000);
		 return new SimpleDateFormat(format).format(date);
	}	

}
