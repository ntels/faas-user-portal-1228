package kr.co.ntels.faas.dashboard.web;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.fasterxml.jackson.databind.ObjectMapper;

import kr.co.ntels.common.web.HubpopController;
import kr.co.ntels.faas.constant.FaaSCommCD;
import kr.co.ntels.faas.dashboard.service.DashboardServive;

/**
  * @FileName : DashboardController.java
  * @Project : faas-user-portal
  * @Date : 2020. 6. 9. 
  * @작성자 : sig61 - 백광욱
  * @변경이력 :
  * @프로그램 설명 :
  */
@Controller
public class DashboardController {
	
	@Autowired
	DashboardServive dashboardServive;
	
	@RequestMapping(value="/dashboard/query.json")
	@ResponseBody
	public String dashboard(HttpServletRequest request,@RequestParam String param) throws Exception {
		
		Map<String,Object> paramMap = new ObjectMapper().readValue(param, Map.class);
		Map<String,Object> result = new HashMap<>();
		String namespace = FaaSCommCD.getNamespace(request);
		paramMap.put("namespace", namespace);
		
		
		List<Map<String, Object>> list = dashboardServive.getFunctionList(paramMap);
		Map<String, Object> runtime = dashboardServive.getRuntimeInfo(paramMap);
		Map<String, Object> triggerData = dashboardServive.getTriggerData(paramMap);
		List<Map<String, Object>> callList = dashboardServive.getCallList(paramMap);
		
		List<Map<String, Object>> empty = new ArrayList<>();
		
		result.put("list", list);
		result.put("runtime", runtime);
		result.put("triggerInfo", triggerData.get("triggerInfo"));
		result.put("chartData", triggerData.get("chartData"));
		result.put("callList", callList);
		
		
		return new ObjectMapper().writeValueAsString(result);
	}
	
	@RequestMapping(value="/dashboard/callList.json")
	@ResponseBody
	public String callList(HttpServletRequest request,@RequestParam String param) throws Exception {
		
		Map<String,Object> paramMap = new ObjectMapper().readValue(param, Map.class);
		Map<String,Object> result = new HashMap<>();
		String namespace = FaaSCommCD.getNamespace(request);
		paramMap.put("namespace", namespace);
		
		List<Map<String, Object>> callList = dashboardServive.getCallList(paramMap);
		
		result.put("callList", callList);
		
		
		return new ObjectMapper().writeValueAsString(result);
	}

}
