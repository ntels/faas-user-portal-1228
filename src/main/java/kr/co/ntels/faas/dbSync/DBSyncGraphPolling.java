package kr.co.ntels.faas.dbSync;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import kr.co.ntels.faas.common.apiCommon.model.APIJsonData;
import kr.co.ntels.faas.common.apiCommon.util.JsonUtil;
import kr.co.ntels.faas.common.apiCommonService.baseService.ServiceBase;
import kr.co.ntels.faas.dao.FaasDao;
import kr.co.ntels.faas.dbSync.model.GraphData;
import kr.co.ntels.faas.dbSync.model.GraphData.Data;

/**
  * @FileName : DBSyncGraphPolling.java
  * @Project : faas-user-portal
  * @Date : 2020. 5. 20. 
  * @작성자 : sig61 - 백광욱
  * @변경이력 :
  * @프로그램 설명 :
  */
@Component
public class DBSyncGraphPolling extends ServiceBase {
	
	private static final Log Log = LogFactory.getLog(DBSyncGraphPolling.class );
	
	@Value("${test.graphpolling.enable}")
	String graphpollingEnable;
	
	@Value("${prometheus.api.url}")
	protected String baseUrl;
	
	@Autowired
	FaasDao dao;	
	
	int pollingTimeSec = 60 * 10;
	boolean _stopPolling = false;
	Thread _thread = null;
	int step = GraphData.getStepInit();
	List<GraphData> listGraphData = new ArrayList<>();
	
	
	
	@PostConstruct
	public void initConstruct() throws Exception 
	{
		GraphData graphData = null;
		// String filter = "reporter=%22source%22,destination_app=%22activator%22,source_workload=%22istio-ingressgateway%22";
		String filter = "reporter=%22source%22,destination_app=%22activator%22";
		
		graphData = new GraphData(GraphData.E_DATA_TYPE.CNT, "cnt");
		graphData.setQuery(String.format("%s", "istio_requests_total{"+filter+"}"));
		listGraphData.add(graphData);
		
		graphData = new GraphData(GraphData.E_DATA_TYPE.RATE, "rate");
		graphData.setQuery(String.format("%s/%s"
				, "istio_requests_total{"+filter+",response_code=~%222.*%22}"
				, "istio_requests_total{"+filter+"}"
				));
		listGraphData.add(graphData);
		
		graphData = new GraphData(GraphData.E_DATA_TYPE.MAX, "p99");
		graphData.setQuery(String.format("%s"
				, "histogram_quantile(0.99,sum(irate(istio_request_duration_seconds_bucket{"+filter+"}[5m]))by(destination_service_namespace,destination_service_name,le))"
				));
		listGraphData.add(graphData);
		
		graphData = new GraphData(GraphData.E_DATA_TYPE.MAX, "p90");
		graphData.setQuery(String.format("%s"
				, "histogram_quantile(0.90,sum(irate(istio_request_duration_seconds_bucket{"+filter+"}[5m]))by(destination_service_namespace,destination_service_name,le))"
				));
		listGraphData.add(graphData);
		
		graphData = new GraphData(GraphData.E_DATA_TYPE.MAX, "p50");
		graphData.setQuery(String.format("%s"
				, "histogram_quantile(0.50,sum(irate(istio_request_duration_seconds_bucket{"+filter+"}[5m]))by(destination_service_namespace,destination_service_name,le))"
				));
		listGraphData.add(graphData);
		
		
		if("1".equals(graphpollingEnable))
			startThread();
	}
	
	@PreDestroy
	public void preDestory()
	{
		_stopPolling = true;
	}
	
	private void startThread()
	{
		_thread = new Thread(()->{
		      
		    while(true)
		    {
			    try 
			    {
			    	polling();
				} catch (Exception e) {
					Log.error("DBSyncGraphPolling : 데이타 조회중 문제가 있습니다." + e.getMessage());
				}
			    
			    try 
			    {
			    	Thread.sleep(pollingTimeSec * 1000);
				} catch (Exception e) {
				}
			    
		    }
		});
		
		_thread.setName("Thread : DBSyncGraphPolling");
		_thread.start();
	}
	
	private void polling() throws Exception
	{
		Map<String,Object> param = new HashMap<>();
		
		for(int ia=0;ia<listGraphData.size();ia++)
		{
			String url = "";
			GraphData graphData = listGraphData.get(ia);
			url = String.format("%s/query_range?query=%s&start=%s&end=%s&step=%ss", baseUrl, graphData.getQuery(), graphData.getStart(), graphData.getEnd(), graphData.getStep());
			APIJsonData apiJsonData = getAPIGetData(url,"$..result.[*]");
			List<APIJsonData> listJsonData = JsonUtil.getJsonArrayUnit(apiJsonData,"$");
			for(int ib=0;ib<listJsonData.size();ib++)
			{
				List<Data> listData = graphData.getDataList(listJsonData.get(ib));
				String namespace = JsonUtil.getFilterJson(listJsonData.get(ib), "$..destination_service_namespace").toString();
				String name = JsonUtil.getFilterJson(listJsonData.get(ib), "$..destination_service_name").toString();
				name = name.substring(0, name.length() - 6);
				
				param = new HashMap<>();
				param.put("namespace", namespace);
				param.put("name", name);
				
				if(!"1".equals(dao.selectOne("faas.dbSyncGraph.cntFunctionId", param).toString()))
					continue;

				
				for(int ic=0 ; ic< listData.size() ; ic++)
				{
					param = new HashMap<>();
					Data data = listData.get(ic);
					param.put("namespace", namespace);
					param.put("name", name);
					param.put("date_hms", graphData.getDateString(data._time));
					param.put("time",data._time);
					param.put(graphData.getDbFieldName(),data.getValue());
					
					if(ia == 0)
						param.put("url",url);
					try
					{
						dao.update("faas.dbSyncGraph.setGraphTempValue", param);
					}
					catch(Exception ee)
					{
					}
				}
			}
		}

		// 함수 등록수 , 데이보드 시간기준 데이타 통계처리
		try
		{
			dao.update("faas.dbSyncGraph.genGraphFunctionValue", null); //등록 함수 통계처리
			dao.update("faas.dbSyncGraph.genGraphValue", null);// 분단위 데이타 시간단위 테이블로 합계 이관
		}
		catch(Exception ee)
		{
		}
		
	}
}
