package kr.co.ntels.faas.dbSync;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;

import org.mybatis.spring.SqlSessionTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import kr.co.ntels.faas.common.apiCommon.model.APIJsonData;
import kr.co.ntels.faas.common.apiCommon.util.JsonUtil;
import kr.co.ntels.faas.common.apiCommonService.KubeAPIService;
import kr.co.ntels.faas.constant.FaaSCommCD;
import kr.co.ntels.faas.function.service.FunctionService;
import kr.co.ntels.faas.function.service.JenkinsService;
import kr.co.ntels.faas.webSocketHandler.FunctionWebSocketHandler;

/**
  * @FileName : DBSyncManager.java
  * @Project : faas-user-portal
  * @Date : 2020. 4. 28. 
  * @작성자 : sig61 - 백광욱
  * @변경이력 :
  * @프로그램 설명 :
  */
@Component
public class DBSyncManager {
	
	Thread _thread = null;
	boolean _bStop = false;
	
	@Value("${test.dbsync.enable}")	String dbSyncEnable;
	
	@Autowired SqlSessionTemplate sqlSessionTemplate;
	@Autowired KubeAPIService kubeAPIService;
	@Autowired FunctionService service;
	@Autowired JenkinsService jenkinsService;
	@Autowired FunctionWebSocketHandler functionWebSocketHandler;
	
	
	@PostConstruct
	public void init() throws Exception 
	{
		if("1".equals(dbSyncEnable))
		startThread();
	}
	
	@PreDestroy
	public void destroy()
	{
		endThread();
	}
	
	protected void startThread() throws Exception
	{
		_thread = new Thread(()->{
		    
			Map<String,Object> param = new HashMap<>();
			List<Map<String, Object>> list = sqlSessionTemplate.selectList("faas.dbSync.listFunction", param);
			
			for(Map<String, Object> function : list)
			{
				String stat = "NORMAL" , buf="";
				StringBuffer des = new StringBuffer();
				String function_id = function.get("function_id").toString();
				String name = function.get("name").toString();
				String namespace = function.get("namespace").toString();
				String stat_cd = function.get("stat_cd").toString();
				String reg_step_cd = function.get("reg_step_cd").toString();
				
				// 기존 작업중이던 작업 다시 시작
				if(FaaSCommCD.E_STAT.REG == FaaSCommCD.E_STAT.valueOf(stat_cd))
				{
					Map<String,Object> param2 = new HashMap<>();
					param2.put("project_id",namespace.replace("faas-p", ""));
					Map<String, Object> JenkinsAccessInfo = jenkinsService.getJenkinsAccessInfo(param2);
					
					Map<String,Object> param1 = new HashMap<>();
					param1.put("namespace", namespace);
					param1.put("name", name);
					param1.put("stat", FaaSCommCD.E_STAT.REG.toString());
					param1.put("jenkins_url", JenkinsAccessInfo.get("jenkins_url"));
					param1.put("jenkins_admin_id", JenkinsAccessInfo.get("jenkins_admin_id"));
					param1.put("jenkins_admin_pw", JenkinsAccessInfo.get("jenkins_admin_pw"));
					
					try {
						functionWebSocketHandler.restartJenkinsLogPolling(param1, service);
					} catch (Exception e) {
					}					
				}
				else if(FaaSCommCD.E_STAT.INIT == FaaSCommCD.E_STAT.valueOf(stat_cd))
				{
					
				}
				else if(FaaSCommCD.E_STAT.NORMAL == FaaSCommCD.E_STAT.valueOf(stat_cd))
				{
					if("NONE".equals(reg_step_cd))
					{
						try 
						{
							for(Map<String, Object> trigger : getListTrigger(function_id))
							{
								String namespaceT = function.get("namespace").toString();
								String channel_name = trigger.get("channel_name").toString();
								String subsc_name = trigger.get("subsc_name").toString();
								String kamel_name = trigger.get("kamel_name").toString();
								String trigger_stat_cd = trigger.get("trigger_stat_cd").toString();
								
								if(FaaSCommCD.E_TRIGGER_STAT.INIT == FaaSCommCD.E_TRIGGER_STAT.valueOf(trigger_stat_cd))
									continue;
			
								if(!"".equals(channel_name) && isExistCannel(namespaceT,channel_name) == false)
								{
									stat = "NOTEXIST";
									buf = String.format("체널 %s 오류", channel_name);
									des.append(buf);
								}
								
								if(!"".equals(subsc_name) && isExistSubsc(namespaceT,subsc_name) == false)
								{
									stat = "NOTEXIST";
									buf = String.format("구독 %s 오류", subsc_name);
									des.append(buf);
								}
								
								if(!"".equals(kamel_name) && isExistKamel(namespaceT,kamel_name) == false)
								{
									stat = "NOTEXIST";
									buf = String.format("카멜 %s 오류", kamel_name);
									des.append(buf);
								}
								
							}
						
							Map<String,Object> retMap = new HashMap<>();
							if(isExistService(namespace,name,retMap) == false)
							{
								stat = "NOTEXIST";
								buf = String.format("서비스 %s %s 오류", namespace , name);
								des.append(buf);
							}
							
						} catch (Exception e) {
							stat = "ERROR";
							buf = String.format("작업오류 : %s" , e.getMessage());
							des.append(buf);
						}
						
						updateSyncInfo(function_id,stat, des.toString());
					}
				}
				
				if(_bStop) return;
			}
		    
		});
		
		_thread.setName("Thread : "+getClass().getSimpleName());
		_thread.start();
	}
	
	protected void endThread()
	{
		_bStop = true;		
	}
	
	protected boolean isExistService(String namespace, String name , Map<String,Object> retMap) throws Exception
	{
		String url = String.format("apis/serving.knative.dev/v1/namespaces/%s/services/%s", namespace, name);
		APIJsonData apiJsonData = kubeAPIService.getServiceJson(url, "$");
		String value = JsonUtil.getFilterJson(apiJsonData, "$..kind").toString();
		
		retMap.put("a", "");
		
		return "Service".equals(value);
	}
	
	protected boolean isExistCannel(String namespace, String name) throws Exception
	{
		String url = String.format("apis/serving.knative.dev/v1/namespaces/%s/services/%s", namespace, name);
		String value = kubeAPIService.getServiceJson(url, "$..kind").toString();
		
		return "Service".equals(value);
	}
	
	protected boolean isExistSubsc(String namespace, String name) throws Exception
	{
		String url = String.format("apis/serving.knative.dev/v1/namespaces/%s/services/%s", namespace, name);
		String value = kubeAPIService.getServiceJson(url, "$..kind").toString();
		
		return "Service".equals(value);
	}
	
	protected boolean isExistKamel(String namespace, String name) throws Exception
	{
		String url = String.format("apis/serving.knative.dev/v1/namespaces/%s/services/%s", namespace, name);
		String value = kubeAPIService.getServiceJson(url, "$..kind").toString();
		
		return "Service".equals(value);
	}
	
	protected List<Map<String, Object>> getListTrigger(String function_id)
	{
		Map<String,Object> param = new HashMap<>();
		param.put("function_id", function_id);
		
		List<Map<String, Object>> list = sqlSessionTemplate.selectList("faas.dbSync.listTrigger", param);
		
		return list;
	}
	
	protected void updateSyncInfo(String function_id , String stat , String des)
	{
		Map<String,Object> param = new HashMap<>();
		param.put("function_id", function_id);
		param.put("stat", stat);
		param.put("des", des.getBytes().length < 200 ? des : des.substring(0, 199) );
		
		sqlSessionTemplate.update("faas.dbSync.syncFunction", param);
	}

}
