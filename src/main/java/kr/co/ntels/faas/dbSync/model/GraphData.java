package kr.co.ntels.faas.dbSync.model;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import kr.co.ntels.faas.common.apiCommon.model.APIJsonData;
import kr.co.ntels.faas.common.apiCommon.util.JsonUtil;
import kr.co.ntels.faas.userException.ExceptionEx;
import net.minidev.json.JSONArray;

/**
  * @FileName : GraphData.java
  * @Project : faas-user-portal
  * @Date : 2020. 5. 20. 
  * @작성자 : sig61 - 백광욱
  * @변경이력 :
  * @프로그램 설명 :
  */
public class GraphData {
	
	public enum E_DATA_TYPE {CNT,RATE,MAX}
	public class Data{
		public int ivalue = 0;
		public double dvalue = 0.0;
		public long _time = 0L;
		private String value="0";
		public void setValue(int value) { ivalue = value; this.value = String.format("%d", value); }
		public void setValue(double value) { dvalue = value; this.value = String.format("%10.2f", value); }
		public String getValue() { return value.trim(); }
	}
	
	int step = GraphData.getStepInit();   // 수집기준 10분씩
	int term = 6 * 24; 
	long start = 0L;
	long end = 0;
	String query = "";
	String dateString = "";
	E_DATA_TYPE type;
	String dbFieldName="";
	
	public static int getStepInit()
	{
		return 600;
	}
	
	public GraphData(E_DATA_TYPE type,String dbFieldName)
	{
		this.type = type;
		this.dbFieldName = dbFieldName;
	}
	
	public int getStep() {
		return step;
	}

	public void setStep(int step) {
		this.step = step;
	}

	public long getStart() {
		long t = getEnd() - this.step * term;
		return t;
	}

	public long getEnd() {
		long t = ((int)(getUnixTime()/this.step) ) * this.step;
		
		return t;
	}

	public String getQuery() {
		return query;
	}

	public void setQuery(String query) {
		this.query = query;
	}

	public String getDateString() {
		long t = ((int)(getUnixTime()/this.step)) * this.step;
		return getUnixTimeToString(t, "yyyy-MM-dd HH:mm:ss");
	}
	
	public String getDateString(long time) {
		long t = ((int)(getUnixTime()/time)) * time;
		return getUnixTimeToString(t, "yyyy-MM-dd HH:mm:ss");
	}
	
	public List<Data> getDataList(APIJsonData apiJsonData) throws Exception
	{
		List<APIJsonData> listJsonData = JsonUtil.getJsonArrayUnit(apiJsonData , "$..values.[*]");
		
		if(E_DATA_TYPE.CNT == type)
			return getCntDataList(listJsonData);
		else if(E_DATA_TYPE.RATE == type)
			return getRateDataList(listJsonData);
		else if(E_DATA_TYPE.MAX == type)
			return getMaxDataList(listJsonData);
		else
			throw new ExceptionEx("afa");
	}
	
	private List<Data> getCntDataList(List<APIJsonData> listJsonData) throws Exception
	{
		List<Data> list = new ArrayList<>();
		int size = listJsonData.size();
		if(size <= 1) return list;
		
		GraphData.Data bufData = getData(listJsonData.get(listJsonData.size()-1));
		for(int ia=2 ; ia<=size ; ia++)
		{
			GraphData.Data data = getData(listJsonData.get(size-ia));
			if(data._time + this.step == bufData._time)
			{
				if(bufData.ivalue >= data.ivalue)
					bufData.setValue( bufData.ivalue - data.ivalue);
			}
			list.add(bufData);
			
			bufData = data;
		}
		
		return list;
	}
	
	private List<Data> getRateDataList(List<APIJsonData> listJsonData) throws Exception
	{
		List<Data> list = new ArrayList<>();
		int size = listJsonData.size();

		for(int ia=0 ; ia<size ; ia++)
		{
			GraphData.Data data = getData(listJsonData.get(ia));
			list.add(data);
		}
		
		return list;
	}
	
	private List<Data> getMaxDataList(List<APIJsonData> listJsonData) throws Exception
	{
		List<Data> list = new ArrayList<>();
		int size = listJsonData.size();

		for(int ia=0 ; ia<size ; ia++)
		{
			GraphData.Data data = getData(listJsonData.get(ia));
			list.add(data);
		}
		
		return list;
	}
	
		
	private GraphData.Data getData(APIJsonData data)
	{
		GraphData.Data dd = new GraphData.Data();
		JSONArray array = data.getJsonArray();
		if(array.size() == 2)
		{
			dd._time = Long.parseLong(array.get(0).toString());
			
			try
			{
				if(this.type == E_DATA_TYPE.CNT)
				{
					dd.setValue(Integer.parseInt(array.get(1).toString()));
				}
				else
				{
					dd.setValue(Double.parseDouble(array.get(1).toString()));
				}
			}
			catch(NumberFormatException ee)
			{
				dd.ivalue = 0;
				dd.dvalue = 0.0;
			}
			return dd;
		}

		dd._time = this.getStart();
		dd.ivalue = 0;
		dd.dvalue = 0.0;
	
		return dd;
	}
	
	private long getUnixTime()
	{
		 return System.currentTimeMillis()/1000;// - 9*3600 ;
	}
	
	private String getUnixTimeToString( long time, String format)
	{
		 Date date = new Date(time*1000);
		 return new SimpleDateFormat(format).format(date);
	}

	public E_DATA_TYPE getType() {
		return type;
	}

	public void setType(E_DATA_TYPE type) {
		this.type = type;
	}

	public String getDbFieldName() {
		return dbFieldName;
	}
	
}
