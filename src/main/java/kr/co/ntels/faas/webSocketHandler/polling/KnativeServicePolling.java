package kr.co.ntels.faas.webSocketHandler.polling;

import java.util.Map;

import kr.co.ntels.faas.common.apiCommon.model.QueueData;
import kr.co.ntels.faas.common.apiCommon.util.polling.HttpPolling;
import kr.co.ntels.faas.common.apiCommon.util.polling.queue.PollingQueue;
import kr.co.ntels.faas.function.service.FunctionServiceBase;

public class KnativeServicePolling extends HttpPolling{
	
	FunctionServiceBase _functionServiceBase;
	
	public KnativeServicePolling(FunctionServiceBase functionServiceBase,String urlString, String method, String postData,Map<String,Object> mapHeader, PollingQueue queue, int pollingTimeSec)
	{
		super(urlString,method,postData, mapHeader,queue,pollingTimeSec);
		_functionServiceBase = functionServiceBase; 
	}
	
	@Override
	public void push(QueueData data) throws Exception
	{
		queue.push(data);
	}

}
