/**
 * 
 */
package kr.co.ntels.faas.webSocketHandler;

import java.util.concurrent.BlockingQueue;

import javax.annotation.PreDestroy;

import org.springframework.stereotype.Component;

import kr.co.ntels.faas.common.apiCommon.model.QueueData;
import kr.co.ntels.faas.common.apiCommon.util.QueuedWebSocketHandler;
import kr.co.ntels.faas.common.apiCommon.util.polling.queue.PollingQueue;

/**
  * @FileName : TemplateWebSocketHandler.java
  * @Project : faas-user-portal
  * @Date : 2020. 4. 20. 
  * @작성자 : sig61 - 백광욱
  * @변경이력 :
  * @프로그램 설명 :
  */
@Component
public class TemplateWebSocketHandler extends QueuedWebSocketHandler  {
	
	@PreDestroy
	public void preDestory()
	{

	}

	@Override
	protected void doProducer() throws Exception {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void pushProcess(BlockingQueue<QueueData> queue, QueueData data) throws Exception {
		// TODO Auto-generated method stub
		
	}

	@Override
	public QueueData popProcess(BlockingQueue<QueueData> queue) throws Exception {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	protected void doConsumer(PollingQueue queue) throws Exception {
		// TODO Auto-generated method stub
		
	}
	
	
}