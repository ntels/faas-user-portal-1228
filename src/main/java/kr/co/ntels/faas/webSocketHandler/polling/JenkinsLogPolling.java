package kr.co.ntels.faas.webSocketHandler.polling;

import java.net.ConnectException;
import java.net.URI;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.offbytwo.jenkins.JenkinsServer;
import com.offbytwo.jenkins.model.JobWithDetails;

import kr.co.ntels.faas.common.apiCommon.model.APIJsonData;
import kr.co.ntels.faas.common.apiCommon.model.APIResponseData;
import kr.co.ntels.faas.common.apiCommon.model.QueueData;
import kr.co.ntels.faas.common.apiCommon.util.HttpUtil;
import kr.co.ntels.faas.common.apiCommon.util.JsonUtil;
import kr.co.ntels.faas.common.apiCommon.util.polling.queue.PollingQueue;
import kr.co.ntels.faas.function.model.JenkinsLog;
import kr.co.ntels.faas.userException.ExceptionEx;
import kr.co.ntels.faas.userException.ExceptionNotExist;

public class JenkinsLogPolling {
	
	public static class JenkinsInfo{
		public String JENKINS_URL;
		public String JENKINS_USERNAME;
		public String JENKINS_PASSWORD;
		
		public JenkinsInfo() {}
		
		public JenkinsInfo(String url, String user, String pwd)
		{
			if("/".equals(url.substring(url.length()-1)))
				url = url.substring(0,url.length()-1);
			JENKINS_URL = url + "/";
			JENKINS_USERNAME = user;
			JENKINS_PASSWORD = pwd;
		}
	}
	
	private String JENKINS_URL;
	private String JENKINS_USERNAME;
	private String JENKINS_PASSWORD;
	

	private static final Log Log = LogFactory.getLog(JenkinsLogPolling.class );
	
	int pollingTimeSec = 3;
	PollingQueue queue = null;
	boolean _stopPolling = false;
	Thread _thread = null;
	String namespace, name; 
	JenkinsLog lastJenkinsLog = null;
	
	public String getName() {return name;}
	public String getNamespace() {return namespace;}
	
	public JenkinsLogPolling() {}
	
	public JenkinsLogPolling(String namespace,String name,int pollingTimeSec, PollingQueue queue , JenkinsInfo info) throws Exception
	{
		this.namespace = namespace;
		this.name = name;
		this.queue = queue;
		this.pollingTimeSec = pollingTimeSec;
		
		this.JENKINS_URL = info.JENKINS_URL;
		this.JENKINS_USERNAME = info.JENKINS_USERNAME;
		this.JENKINS_PASSWORD = info.JENKINS_PASSWORD;
		
		startThread();
	}
	
	public void stopPolling()
	{
		_stopPolling = true;
	}
	
	private void startThread()
	{
		_thread = new Thread(()->{
		      
			try 
		    {
				Thread.sleep(2000);
		    } catch (Exception e) {}
			
		    while(true)
		    {
			    try 
			    {
			    	polling();
			    } catch (ExceptionNotExist e){
			    	_stopPolling = true;
				} catch (Exception e) {
					Log.error("HttpPolling : 데이타 조회중 문제가 있습니다." + e.getMessage());
					e.printStackTrace();
				}
			    
			    try 
			    {
			    	Thread.sleep(pollingTimeSec * 1000);
				} catch (Exception e) {}
			    
			    if(_stopPolling)
			    {
			    	System.out.println("========================================== JenkinsLogPolling thread stop ====================");
			    	break;
			    }
		    }
		});
		
		_thread.setName("Thread : JenkinsLogPolling");
		_thread.start();
	}
	
	private void polling() throws Exception
	{
		boolean isSuccess = false;
		JenkinsLog jenkinsLog = null;
		List<JenkinsLog> list = new ArrayList<>();
		int lastBuildNumber = getLastBuildNumber();
		String url = String.format("%sjob/%s/%d/wfapi/describe", JENKINS_URL, getBuildJobName(), lastBuildNumber);
		APIJsonData apiJsonData = getAPIGetData(url,"$..stages.[*]");
		
		List<APIJsonData> listJsonData = JsonUtil.getJsonArrayUnit(apiJsonData,"$");
		
		if(listJsonData.size() == 0) return;
		
		for(int i=0;i<listJsonData.size();i++)
		{
			apiJsonData = listJsonData.get(listJsonData.size()-i-1);
			jenkinsLog = JsonUtil.jsonDataMapper(apiJsonData, JenkinsLog.class);
			jenkinsLog.setNamespace(namespace);
			jenkinsLog.setName(name);
			
			if("FAILED".equals(jenkinsLog.getStatus()) || "SUCCESS".equals(jenkinsLog.getStatus()) || "IN_PROGRESS".equals(jenkinsLog.getStatus()))
			{
				isSuccess = true;
				break;
			}
		}
		if(isSuccess == false || (lastJenkinsLog != null && lastJenkinsLog.isSame(jenkinsLog)))
		
		if("FAILED".equals(jenkinsLog.getStatus()))
		{
			stopPolling();
			
			StringBuffer log = new StringBuffer();
			String statgeUrl = String.format("%s%s", JENKINS_URL , jenkinsLog.getHref());
			APIJsonData stageFlowNodes = getAPIGetData(statgeUrl,"$..stageFlowNodes.[*]");
			List<APIJsonData> listLog = JsonUtil.getJsonArrayUnit(stageFlowNodes,"$");
			for(int ia=0;ia<listLog.size();ia++)
			{
				JenkinsLog jenkinsErrorLog = JsonUtil.jsonDataMapper(listLog.get(ia), JenkinsLog.class);
				String logHref = jenkinsErrorLog.getLogHref();
				if(logHref != null && !"".equals(logHref)) 
				{
					String logUrl = String.format("%s%s", JENKINS_URL , logHref);
					String logBuf = getAPIGetData(logUrl,"$..text").toString();
					log.append(logBuf);
				}
			}
			jenkinsLog.setLog(log.toString());
		}
		
		if("FAILED".equals(jenkinsLog.getStatus()) || "SUCCESS".equals(jenkinsLog.getStatus()) || "IN_PROGRESS".equals(jenkinsLog.getStatus()))
		{
			QueueData data = new QueueData();
			data.setSourceObj(this);
			data.setTime(LocalDateTime.now());
			data.setMessage(new ObjectMapper().writeValueAsString(jenkinsLog));
			try
			{
				queue.push(data);
			}
			catch(Exception ex)
			{
				Log.error(ex.getMessage(),ex);
			}	
		}

		lastJenkinsLog = jenkinsLog;
	}
	
	private int getLastBuildNumber() throws Exception
	{
		JenkinsServer jenkinsServer = null;
		JobWithDetails buildJob = null;
		String buildJobName = String.format("%s-%s", this.namespace, this.name);
		
		try
		{
			jenkinsServer = new JenkinsServer(new URI(JENKINS_URL), JENKINS_USERNAME, JENKINS_PASSWORD);
			buildJob = jenkinsServer.getJob(buildJobName);
			
			if(buildJob == null)
				throw new ExceptionNotExist("Jenkins job을 찾을 수 없습니다.");
			
		}
		catch(ConnectException conex)
		{
			Log.error(conex.getMessage());
			throw conex;
		}
		catch(ExceptionNotExist e)
		{
			throw e;
		}
		catch(Exception e)
		{
			Log.error(e.getMessage());
			throw new ExceptionEx(e.getMessage());
		}
		finally
		{
			if(jenkinsServer != null) jenkinsServer.close();
		}
		
		return buildJob.getLastBuild().getNumber();
	}
	
	private String getBuildJobName()
	{
		return String.format("%s-%s", this.namespace, this.name);
	}
	
	private APIJsonData getAPIGetData(String url , String jsonPath ) throws Exception 
	{
		APIResponseData apiResponseData;
		try
		{
			apiResponseData = HttpUtil.getAPIData(url, "GET", null, "");
			if(apiResponseData.getException() != null)
				throw apiResponseData.getException();
			
			APIJsonData apiJsonData = JsonUtil.getFilterJson(apiResponseData, jsonPath);
			
			return apiJsonData;
		}
		catch(Exception e)
		{
			throw e;
		}
	}
	
	private List<String> getAPIGetListData(String url,String jsonPath) throws Exception
	{
		List<String> retList = new ArrayList<>();
		try {
			
			APIJsonData apiJsonData = getAPIGetData(url,jsonPath);
			if(apiJsonData.getJsonArray() != null)
			{
				for(Object obj : apiJsonData.getJsonArray())
				{
					retList.add(obj.toString());
				}
			}
		} catch (Exception e) {
			return new ArrayList<>();
		}
		return retList;
	}
	

}
