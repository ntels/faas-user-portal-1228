/**
 * 
 */
package kr.co.ntels.faas.webSocketHandler;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.BlockingQueue;

import javax.annotation.PreDestroy;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import kr.co.ntels.faas.common.apiCommon.model.QueueData;
import kr.co.ntels.faas.common.apiCommon.util.JsonUtil;
import kr.co.ntels.faas.common.apiCommon.util.QueuedWebSocketHandler;
import kr.co.ntels.faas.common.apiCommon.util.polling.HttpLongPolling;
import kr.co.ntels.faas.common.apiCommon.util.polling.HttpPolling;
import kr.co.ntels.faas.common.apiCommon.util.polling.queue.PollingQueue;
import kr.co.ntels.faas.constant.FaaSCommCD;
import kr.co.ntels.faas.dao.FaasDao;
import kr.co.ntels.faas.function.model.JenkinsLog;
import kr.co.ntels.faas.function.model.ServiceWatch;
import kr.co.ntels.faas.function.model.TriggerWatch;
import kr.co.ntels.faas.function.service.FunctionServiceBase;
import kr.co.ntels.faas.function.service.KongService;
import kr.co.ntels.faas.log.service.LogService;
import kr.co.ntels.faas.userInterface.ICallbackFunction;
import kr.co.ntels.faas.webSocketHandler.polling.JenkinsLogPolling;
import kr.co.ntels.faas.webSocketHandler.polling.KnativeServicePolling;



/**
  * @FileName : FunctionWebSocketHandler.java
  * @Project : faas-user-portal
  * @Date : 2020. 4. 20. 
  * @작성자 : sig61 - 백광욱
  * @변경이력 :
  * @프로그램 설명 :
  */
@Component
public class FunctionWebSocketHandler extends QueuedWebSocketHandler  {
	
	private static final Log Log = LogFactory.getLog(FunctionWebSocketHandler.class );
	
	HttpLongPolling httpLongPollingFunction; // 쿠버네티스 watch
	HttpLongPolling httpLongPollingTrigger; // camel watch
	List<JenkinsLogPolling> listJenkinsLogPolling = new ArrayList<>();
	List<HttpPolling> listKanativeServicePolling = new ArrayList<>();
	
	@Value("${test.longpolling.enable}")
	String longpollingEnable;
/*
	@Value("${jenkins.url}")
	private String JENKINS_URL;
	
	@Value("${jenkins.username}")
	private String JENKINS_USERNAME;
	
	@Value("${jenkins.password}")
	private String JENKINS_PASSWORD;
*/
	@Value("${docker.registry}")
	private String DOCKER_REGISTRY;
	
	
	@Value("${kubernetes.api.authorization.type}")
	String authorizationType;
	
	@Value("${kubernetes.api.authorization.token}")
	String authorizationToken;
	
	@Value("${kubernetes.api.base.url}")
	protected String baseUrl;
	
	@Autowired
	FaasDao dao;
	
	@Autowired
	FunctionServiceBase functionServiceBase;
	
	@Autowired
	LogService logService;
	
	@Autowired
	KongService kongService;
	
	QueueData lastQueueData = null;
	ICallbackFunction _callback = null;
	
	@PreDestroy
	public void preDestory()
	{
		if(httpLongPollingFunction != null)
			httpLongPollingFunction.stopPolling();
		
		if(httpLongPollingTrigger != null)
			httpLongPollingTrigger.stopPolling();
		
		for(int i=0;i<listJenkinsLogPolling.size();i++)
		{
			JenkinsLogPolling obj = listJenkinsLogPolling.get(i);
			if(obj != null)
				obj.stopPolling();
		}
		for(int i=0;i<listKanativeServicePolling.size();i++)
		{
			HttpPolling obj = listKanativeServicePolling.get(i);
			if(obj != null)
				obj.stopPolling();
		}
		
	}

	@Override  //Queue에 데이타를 넣기 위한 작업 (HttpLongPolling , HttpPolling , 혹은 DBPolling )
	protected void doProducer() throws Exception {
		
		Map<String , Object> mapHeader = getHeader();
		
		String watchUrl = String.format("%s%s", baseUrl, "apis/serving.knative.dev/v1/services?watch=1");
		httpLongPollingFunction = new HttpLongPolling( watchUrl, "GET", "", mapHeader, this.queue);

		String watchTriggerUrl = String.format("%s/%s", baseUrl, "apis/camel.apache.org/v1/integrations?watch=1");
		httpLongPollingTrigger = new HttpLongPolling( watchTriggerUrl, "GET", "", mapHeader, this.queue);
	}
	
	public void restartJenkinsLogPolling(Map<String,Object> param , ICallbackFunction callback) throws Exception 
	{
		String name = param.get("name").toString();
		String namespace = param.get("namespace").toString();
		String jenkins_url = param.get("jenkins_url").toString();
		String jenkins_admin_id = param.get("jenkins_admin_id").toString();
		String jenkins_admin_pw = param.get("jenkins_admin_pw").toString();
		
		for(JenkinsLogPolling item : listJenkinsLogPolling)
		{
			if(item.getName().equals(name) && item.getNamespace().equals(namespace))
			{
				return;
			}
		}
		
		createJenkinsLogPolling(namespace,name,jenkins_url,jenkins_admin_id,jenkins_admin_pw,2,callback);
	}
	
	// JenkinsLogPolling 객체를 생성함.
	public void createJenkinsLogPolling(String namespace,String name, String jenkins_url, String jenkins_admin_id, String jenkins_admin_pw, 
										int pollingTimeSec, ICallbackFunction callback) throws Exception
	{
		JenkinsLogPolling.JenkinsInfo info = new JenkinsLogPolling.JenkinsInfo(jenkins_url, jenkins_admin_id, jenkins_admin_pw); 
		JenkinsLogPolling jenkinsLogPolling = new JenkinsLogPolling(namespace, name, pollingTimeSec, this.queue, info);
		listJenkinsLogPolling.add(jenkinsLogPolling);
		
		_callback = callback;
	}
	
	public void createKanativeServicePolling(String namespace,String name, int pollingTimeSec)
	{
		Map<String , Object> mapHeader = getHeader();
		String url = String.format("%s/apis/serving.knative.dev/v1/namespaces/%s/services/%s", baseUrl, namespace, name);
		KnativeServicePolling httpPolling = new KnativeServicePolling(functionServiceBase,url, "GET", "", mapHeader, this.queue, pollingTimeSec);
		
		listKanativeServicePolling.add(httpPolling);
	}

	@Override  //Queue 작업을 위해 쓰래드에서 주기적으로 호출된다.
	protected void doConsumer(PollingQueue queue) throws Exception {

		//Log.info("\n ==>WebSocket session(+) cnt "+ getCountActiveSession());
		
		QueueData data = queue.pop();
		if(data != null)
		{
			this.sendAllSession(data.getMessage());
		}
		else
		{ //sig61 Test
			//Map<String,String> map = new HashMap<>();
			//map.put("ECHO", LocalDateTime.now().toString());
			//this.sendAllSession(new ObjectMapper().writeValueAsString(map));
		}
	}

	@Override  // Queue 에 push 되면 호출된다. 
	public void pushProcess(BlockingQueue<QueueData> queue, QueueData data) throws Exception {

		ObjectMapper mapper = new ObjectMapper();
		if(httpLongPollingFunction.equals(data.getSourceObj()) && "1".equals(longpollingEnable))
		{
			
			String orgMessage = data.getMessage();
			ServiceWatch serviceWatch = JsonUtil.jsonDataMapper(data.getMessage(), ServiceWatch.class);
			String kubeStatus = serviceWatch.getKubeStatus();
			String type = serviceWatch.getType();
			data.setMessage(mapper.writeValueAsString(serviceWatch));
			
			//정상적인 코드가 아니면 리턴하다.
			if("collection".equals(type)) 
				return;
			//정상적인 코드가 아니면 리턴하다.
			
			Map<String,String> param = new HashMap<>();
			param.put("namespace", serviceWatch.getNamespace());
			param.put("name", serviceWatch.getName());
			param.put("ref_name", serviceWatch.getLatestReadyRevisionName());
			dao.update("faas.dbSync.setFunctionValue", param);
			
			if("ADDED".equals(serviceWatch.getType()))
			{
				System.out.println("[ADDED] : "+mapper.writeValueAsString(serviceWatch));
				
				if(FaaSCommCD.E_STAT.NORMAL.toString().equals(kubeStatus) || FaaSCommCD.E_STAT.ERROR.toString().equals(kubeStatus))
				{
					dao.update("faas.dbSync.syncFunctionStat",serviceWatch);
					
					if(FaaSCommCD.E_STAT.ERROR.toString().equals(kubeStatus))
					{
						addFunctionErrorLog(serviceWatch.getNamespace() , serviceWatch.getName(), serviceWatch.getLog());
					}
				}
				
				return; 
			}
			
			if("DELETE".equals(serviceWatch.getType()))
			{
				System.out.println("[DELETE] : "+mapper.writeValueAsString(serviceWatch));
				return;
			}
				
			if("MODIFIED".equals(serviceWatch.getType()))
			{
				System.out.println("[MODIFIED] : "+mapper.writeValueAsString(serviceWatch));
				
				if(FaaSCommCD.E_STAT.NORMAL.toString().equals(kubeStatus))
				{
					dao.update("faas.dbSync.syncFunctionStat",serviceWatch);
				}	
				if(FaaSCommCD.E_STAT.ERROR.toString().equals(kubeStatus))
				{
					param.put("stat", FaaSCommCD.E_STAT.ERROR.toString());
					
					dao.update("faas.dbSync.setFunctionStat",param);
					addFunctionErrorLog(serviceWatch.getNamespace() , serviceWatch.getName(), serviceWatch.getLog());
				}
				
				if(lastQueueData != null && lastQueueData.getMessage().equals(data.getMessage()))
					return;
			}
			
			if(this.getCountActiveSession() > 0)
			{
				queue.put(data);
				lastQueueData = data;
			}
		}
		else if(httpLongPollingTrigger.equals(data.getSourceObj()) && "1".equals(longpollingEnable))
		{
			
			String orgMessage = data.getMessage();
			TriggerWatch triggerWatch = JsonUtil.jsonDataMapper(data.getMessage(), TriggerWatch.class);
			String type = triggerWatch.getType();
			data.setMessage(mapper.writeValueAsString(triggerWatch));
			
			//정상적인 코드가 아니면 리턴하다.
			if("collection".equals(type)) 
				return;
			//정상적인 코드가 아니면 리턴하다.
			
			Map<String,String> param = new HashMap<>();
			param.put("namespace", triggerWatch.getNamespace());
			param.put("name", triggerWatch.getName());
			param.put("stat_cd", FaaSCommCD.E_TRIGGER_STAT.NORMAL.toString());

			
			
			if("ADDED".equals(triggerWatch.getType()))
			{
				System.out.println("[TRIGGER-ADDED] : "+mapper.writeValueAsString(triggerWatch));
				return; 
			}
			
			if("DELETE".equals(triggerWatch.getType()))
			{
				System.out.println("[TRIGGER-DELETE] : "+mapper.writeValueAsString(triggerWatch));
				return;
			}
			
				
			if("MODIFIED".equals(triggerWatch.getType()))
			{
				System.out.println("[TRIGGER-MODIFIED] : "+mapper.writeValueAsString(triggerWatch));
				return;
			}
			
			//dao.update("faas.dbSync.setFunctionValue", param);
			
			/*if(this.getCountActiveSession() > 0)
			{
				queue.put(data);
				lastQueueData = data;
			}*/
		}
		else if(KnativeServicePolling.class.getName().equals(data.getSourceObj().getClass().getName()))
		{
			System.out.println(data.getMessage());
			
			ServiceWatch serviceWatch = JsonUtil.jsonDataMapper(data.getMessage(), ServiceWatch.class);
			serviceWatch.setSource(ServiceWatch.E_SERVICE_SOURCE.KUBE_REG.toString());
			
			data.setMessage(mapper.writeValueAsString(serviceWatch));
			String kubeStatus = serviceWatch.getKubeStatus();
			
			System.out.println(data.getMessage());
			
			Map<String,String> param = new HashMap<>();
			param.put("namespace", serviceWatch.getNamespace());
			param.put("name", serviceWatch.getName());
			param.put("stat", kubeStatus);
			
			if(serviceWatch.getNamespace() == null)
			{
				((HttpPolling)data.getSourceObj()).stopPolling();
				listKanativeServicePolling.remove(data.getSourceObj());
				return;
			}
			
			if(FaaSCommCD.E_STAT.REG.toString().equals(kubeStatus))
			{
				param.put("step", FaaSCommCD.E_REG_STEP.STEP5.toString());
				dao.update("faas.dbSync.setFunctionStat", param);
			}
			
			if(FaaSCommCD.E_STAT.NORMAL.toString().equals(kubeStatus) || FaaSCommCD.E_STAT.ERROR.toString().equals(kubeStatus))
			{
				((HttpPolling)data.getSourceObj()).stopPolling();
				
				param.put("step", FaaSCommCD.E_REG_STEP.NONE.toString());
				dao.update("faas.dbSync.setFunctionStat", param);
				
				if(FaaSCommCD.E_STAT.ERROR.toString().equals(kubeStatus))
				{
					addFunctionErrorLog(serviceWatch.getNamespace() , serviceWatch.getName(), serviceWatch.getLog());
				}
				else
				{
					Map<String,Object> functionInfo = (Map<String,Object>)dao.selectOne("faas.function.functionId", param);
					if(functionInfo != null)
					{
						kongService.updateFunctionTraffics(functionInfo.get("function_id").toString());
					}
				}
				
				addFunctionInfoLog(serviceWatch.getNamespace(), serviceWatch.getName(), serviceWatch);
				listKanativeServicePolling.remove(data.getSourceObj());
			}
			
			if(this.getCountActiveSession() > 0)
			{
				queue.put(data);
				lastQueueData = data;
			}
			
			
		}
		else if(JenkinsLogPolling.class.getName().equals(data.getSourceObj().getClass().getName()))
		{
			JenkinsLog jenkinsLog = mapper.readValue(data.getMessage(), JenkinsLog.class);
			
			System.out.println(data.getMessage());
		
			dao.update("faas.dbSync.syncFunctionStep", jenkinsLog);
			
			if("FAILED".equals(jenkinsLog.getStatus()))
			{
				Map<String,String> param = new HashMap<>();
				param.put("namespace", jenkinsLog.getNamespace());
				param.put("name", jenkinsLog.getName());
				param.put("stat", FaaSCommCD.E_STAT.ERROR.toString());
				param.put("log", jenkinsLog.getLog());
				
				dao.update("faas.dbSync.setFunctionStat", param);
				addFunctionErrorLog(jenkinsLog.getNamespace() , jenkinsLog.getName(), jenkinsLog.getLog());
				listJenkinsLogPolling.remove(data.getSourceObj());
			}
			
			if(this.getCountActiveSession() > 0)
			{
				queue.put(data);
			}
			
			if("SUCCESS".equals(jenkinsLog.getStatus()) && "DEPLOY".equals(jenkinsLog.getStage()))
			{
				JenkinsLogPolling polling = (JenkinsLogPolling)data.getSourceObj();
				polling.stopPolling();
				
				addFunctionInfoLog(jenkinsLog.getNamespace(), jenkinsLog.getName(), jenkinsLog);
				listJenkinsLogPolling.remove(data.getSourceObj());
				
				if(_callback != null)
					_callback.callFunction("JenkinsLog", jenkinsLog);
			}
		}
	}

	@Override // Queue 에  pop 되면 호출된다.
	public QueueData popProcess(BlockingQueue<QueueData> queue) throws Exception {
		
		return queue.poll();
	}
	
	private void addFunctionErrorLog(String namespace, String name,Object obj) throws JsonProcessingException
	{
		String log = new ObjectMapper().writeValueAsString(obj);
		log = log.replaceAll("\\\\r", "");
		log = log.replaceAll("\\\\n", "\n");
		log = log.replaceAll("\\\\\"", "\"");
		
		if(log.length()>1 && "\"".equals(log.substring(0, 1))) log = log.substring(1);  /// 첫글자 " 제거
		if(log.length()>2 && "\"".equals(log.substring(log.length()-1, log.length()))) log = log.substring(0,log.length()-1); /// 마지막 글짜 " 제거
		
		logService.saveErrorLog(namespace, name, log);
	}
	
	private void addFunctionInfoLog(String namespace, String name,Object obj) throws JsonProcessingException
	{
		String log =  new ObjectMapper().writeValueAsString(obj);
		logService.saveInfoLog(namespace, name, log);
	}
	
	private Map<String , Object> getHeader() 
	{
		Map<String , Object> mapHeader = new HashMap<>();
		mapHeader.put("Authorization", String.format("%s %s", authorizationType,authorizationToken));
		mapHeader.put("Content-Type", "application/json");
		mapHeader.put("Accept", "application/json");
		
		return mapHeader;
	}
	
}