package kr.co.ntels.faas.monitoring.web;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.fasterxml.jackson.databind.ObjectMapper;

import kr.co.ntels.common.web.HubpopController;
import kr.co.ntels.faas.constant.FaaSCommCD;
import kr.co.ntels.faas.monitoring.service.MonitoringService;

/**
  * @FileName : MonitoringController.java
  * @Project : faas-user-portal
  * @Date : 2020. 5. 18. 
  * @작성자 : sig61 - 백광욱
  * @변경이력 :
  * @프로그램 설명 :
  */
@Controller
public class MonitoringController {
	
	@Autowired
	MonitoringService monitoringService;
	
	@RequestMapping(value="/queryGraph.json")
	@ResponseBody
	public String monitoring(HttpServletRequest request,@RequestParam String param) throws Exception {
		
		Map<String,Object> paramMap = new ObjectMapper().readValue(param, Map.class);
		Map<String,Object> result = new HashMap<>();
		String namespace = FaaSCommCD.getNamespace(request);
		paramMap.put("namespace", namespace);

		String unitType = paramMap.get("unitType").toString();
		int stepCnt = Integer.parseInt(paramMap.get("stepCnt").toString());
		
		List<Map<String,String>> list = monitoringService.getMonitoringData(unitType,stepCnt,paramMap);
		
		result.put("listResult", list);
		result.put("mapC3Result", monitoringService.getMonitoringC3Data(list));
		
		return new ObjectMapper().writeValueAsString(result);
	}

}
