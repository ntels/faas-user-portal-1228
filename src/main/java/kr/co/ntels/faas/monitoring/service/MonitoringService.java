/**
 * 
 */
package kr.co.ntels.faas.monitoring.service;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import kr.co.ntels.faas.common.apiCommonService.baseService.ServiceBase;
import kr.co.ntels.faas.dao.FaasDao;

/**
  * @FileName : MonitoringService.java
  * @Project : faas-user-portal
  * @Date : 2020. 6. 5. 
  * @작성자 : sig61 - 백광욱
  * @변경이력 :
  * @프로그램 설명 :
  */
@Service
public class MonitoringService extends ServiceBase{
	private Logger logger = LoggerFactory.getLogger(this.getClass());

	@Autowired
	FaasDao dao;
	
	public List<Map<String,String>> getMonitoringData(String unitType , int stepCnt , Map<String,Object> paramMap)
	{
		List<Map<String,String>> list = getTargetList(unitType,stepCnt);
		
		List<Map<String, Object>> ret = dao.select("faas.monitoring.getDayGroupByFunction", paramMap);
		
		for(Map<String,String> map : list)
		{
			boolean isMatch = false;
			for(Map<String, Object> obj : ret)
			{
				String time = map.get("checktime").toString();
				String date = obj.get("date_hms").toString();
				if(time.equals(date))
				{
					map.put("cnt", obj.get("cnt").toString());
					map.put("rate", obj.get("rate").toString());
					map.put("rate1", obj.get("rate1").toString());
					map.put("p99", obj.get("p99").toString());
					map.put("p90", obj.get("p90").toString());
					map.put("p50", obj.get("p50").toString());
					isMatch = true;
				}
			}
			
			if(!isMatch) 
			{
				map.put("cnt", "0");
				map.put("rate", "0");
				map.put("rate1", "0");
				map.put("p99", "0");
				map.put("p90", "0");
				map.put("p50", "0");
			}
		}
		
		return list;
	}
	
	public Map<String,Object> getMonitoringC3Data(List<Map<String,String>> list)
	{
		List<String> listColumn = new ArrayList<>(Arrays.asList("time","cnt","rate","rate1","p99","p90","p50"));
		Map<String,Object> retMap = new HashMap<>();
		
		for(String key : listColumn)
		{
			
			List<String> listData = new ArrayList<>();
			retMap.put(key, listData);
			
			for(Map<String,String> map : list)
			{
				String val = map.get(key).toString();
				listData.add(val);
			}
			
		}
		
		return retMap;
	}
	
	
	private List<Map<String,String>> getTargetList(String unitType , int cnt)
	{
		List<Map<String,String>> list = new ArrayList<>();
		
		switch(unitType)
		{
		case "H": getStepTimeList(list,3600,cnt , "MM-dd HH","MM-dd HH"); break;
		case "D": getStepTimeList(list,3600*24,cnt, "MM/dd","yyyy-MM-dd"); break;
		case "W": getStepYearWeekList(list,cnt); break;
		case "M": getStepMonthList(list,cnt); break;
		default: getStepTimeList(list,3600*24,cnt, "MM-dd HH","MM-dd HH"); break;
		}
		
		return list;
	}
	
	private List<Map<String,String>> getStepTimeList(List<Map<String,String>> list , int step, int cnt,String format , String checkformat)
	{
		long t = ((int)(getUnixTime()/step)) * step;
		for(int i=0;i<cnt;i++)
		{
			Map<String,String> map = new HashMap<>();
			map.put("time", getUnixTimeToString(t-(cnt-i-1)*step, format));
			map.put("checktime", getUnixTimeToString(t-(cnt-i-1)*step, checkformat));
			list.add(map);
		}
		return list;
	}
	
	private List<Map<String,String>> getStepYearWeekList(List<Map<String,String>> list , int cnt)
	{
		int iWeek = (int)dao.selectOne("faas.monitoring.getYearweekNum", new HashMap<String,Object>());
		for(int i=0;i<cnt;i++)
		{
			Map<String,String> map = new HashMap<>();
			map.put("time", String.format("%d/52", iWeek));
			list.add(map);
			
			iWeek--;
			if(iWeek == 0) iWeek = 52;
		}
		return list;
	}
	
	private List<Map<String,String>> getStepMonthList(List<Map<String,String>> list , int cnt)
	{
		long t = getUnixTime();
		int iYear = Integer.parseInt(getUnixTimeToString(t,"yyyy"));
		int iMonth = Integer.parseInt(getUnixTimeToString(t,"MM"));
		for(int i=0;i<cnt;i++)
		{
			Map<String,String> map = new HashMap<>();
			map.put("time", String.format("%d-%02d", iYear,iMonth));
			list.add(map);
			
			iMonth = iMonth - 1;
			if(iMonth == 0) { 
				iMonth = 12;
				iYear--;
			}
		}
		return list;
	}
	
	private long getUnixTime()
	{
		 return System.currentTimeMillis()/1000;// - 9*3600 ;
	}
	
	private String getUnixTimeToString( long time, String format)
	{
		 Date date = new Date(time*1000);
		 return new SimpleDateFormat(format).format(date);
	}
	
	private int string2num(String s , int defaultNum)
	{
		int iRet = 0;
		try
		{
			iRet = Integer.parseInt(s);
			if(iRet < defaultNum) iRet = defaultNum;
		}
		catch(NumberFormatException ee)
		{
			iRet = defaultNum;
		}
		return iRet;
	}

}
