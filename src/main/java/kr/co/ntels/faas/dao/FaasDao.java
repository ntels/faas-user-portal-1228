package kr.co.ntels.faas.dao;

import java.util.List;
import java.util.Map;

import org.mybatis.spring.SqlSessionTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import kr.co.ntels.faas.function.model.FunctionLogVO;
import kr.co.ntels.faas.function.vo.FunctionVo;

@Repository
public class FaasDao {
	
	@Autowired
	private SqlSessionTemplate sqlSessionTemplate;
	

	public int update(String mapper, Map<String,Object> param){
		
		return sqlSessionTemplate.update(mapper, param);
	}
	
	public int update(String mapper, Object vo){
		
		return sqlSessionTemplate.update(mapper, vo);
	}
	
	public List<Map<String, Object>> select(String mapper, Map<String,Object> param){
		
		return sqlSessionTemplate.selectList(mapper, param);
	}
	
	public List<Map<String, Object>> select(String mapper,Object param){
		
		return sqlSessionTemplate.selectList(mapper, param);
	}

	public Object selectOne(String mapper, Map<String,Object> param){
		return sqlSessionTemplate.selectOne(mapper, param);
	}

	public Object selectOne(String mapper, Object param){
		return sqlSessionTemplate.selectOne(mapper, param);
	}

	public int insert(String mapper, Map<String,Object> param){
		return sqlSessionTemplate.insert(mapper, param);
	}

}
