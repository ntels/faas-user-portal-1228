package kr.co.ntels.faas.dao;

import java.util.List;
import java.util.Map;

import org.mybatis.spring.SqlSessionTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

@Repository
public class DPLDao {
	
	@Autowired
	private SqlSessionTemplate sqlSessionTemplate2;
	

	public int update(String mapper, Map<String,Object> param){
		
		return sqlSessionTemplate2.update(mapper, param);
	}
	
	public int update(String mapper, Object vo){
		
		return sqlSessionTemplate2.update(mapper, vo);
	}
	
	public List<Map<String, Object>> select(String mapper, Map<String,Object> param){
		
		return sqlSessionTemplate2.selectList(mapper, param);
	}
	
}
