/**
 * 
 */
package kr.co.ntels.faas.userException;

/**
  * @FileName : ExceptionEx.java
  * @Project : faas-portal
  * @Date : 2020. 2. 24. 
  * @작성자 : sig61 - 백광욱
  * @변경이력 :
  * @프로그램 설명 :
  */

public class ExceptionNotExist extends Exception{

	/**
	 * 
	 */
	private static final long serialVersionUID = 2L;
	
	public ExceptionNotExist(String msg)
	{
		super(msg);
		
	}

}
