package kr.co.ntels.faas.userException;

public class ExceptionDBPreProcess extends Exception {
	
	private static final long serialVersionUID = 1L;
	
	public ExceptionDBPreProcess(String message)
	{
		super(message);
	}
	
}