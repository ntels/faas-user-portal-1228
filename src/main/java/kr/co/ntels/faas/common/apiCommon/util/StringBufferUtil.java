/**
 * 
 */
package kr.co.ntels.faas.common.apiCommon.util;

/**
  * @FileName : StringBufferUtil.java
  * @Project : faas-portal
  * @Date : 2020. 2. 24. 
  * @작성자 : sig61 - 백광욱
  * @변경이력 :
  * @프로그램 설명 :
  */

public class StringBufferUtil{
	StringBuffer _sb = new StringBuffer();
	
	public void append(String str)
	{
		_sb.append(str).append("\n");
	}
	
	public String toString()
	{
		return _sb.toString();
	}
}
