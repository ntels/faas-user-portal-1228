/**
 * 
 */
package kr.co.ntels.faas.common.apiCommonService.baseService;

import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileOutputStream;

import kr.co.ntels.faas.common.apiCommon.util.ExecShell;
import kr.co.ntels.faas.common.apiCommonService.model.KCLIData;
import kr.co.ntels.faas.common.apiCommonService.model.KCLIData.E_FILE_TYPE;

/**
  * @FileName : KCLIServiceBase.java
  * @Project : FaasKubeAPI
  * @Date : 2020. 1. 30. 
  * @작성자 : sig61 - 백광욱
  * @변경이력 :
  * @프로그램 설명 :
  */

public class KCLIServiceBase {
	
	protected KCLIData execShell(KCLIData data) throws Exception
	{
		ExecShell shell = new ExecShell();
		boolean ret = shell.exec(data.cmd);
		data.msg = shell.getMsg();
		data.result = ret;
		return data;
	}
	
	protected KCLIData getKCLIData(String content , E_FILE_TYPE type) throws Exception
	{
		BufferedOutputStream bs = null;
		
		try
		{
			String filePath = "";
			File temp = null;
			if(type == E_FILE_TYPE.JSON)
				temp = File.createTempFile("jsonfile", ".json");
			if(type == E_FILE_TYPE.YMAL)
				temp = File.createTempFile("ymalfile", ".ymal");
			if(type == E_FILE_TYPE.GROOVY)
				temp = File.createTempFile("groovyfile", ".groovy");
			
			filePath = temp.getAbsolutePath();
			bs = new BufferedOutputStream(new FileOutputStream(filePath));
			bs.write(content.getBytes());
			
			KCLIData kcliData = new KCLIData(filePath,type);
			kcliData.fnameOnly = getFileNameOnly(temp , type);
			kcliData.content = content;
			return kcliData;
		
		}
		catch(Exception e)
		{
			e.printStackTrace();
			return new KCLIData();
		}
		finally
		{
			if(bs != null) bs.close();
		}
	}
	
	private String getFileNameOnly(File file , E_FILE_TYPE type)
	{
		String fname = file.getName();
		if(type == E_FILE_TYPE.JSON)
			fname = fname.replaceAll(".json","");
		if(type == E_FILE_TYPE.YMAL)
			fname = fname.replaceAll(".ymal",""); 
		if(type == E_FILE_TYPE.GROOVY)
			fname = fname.replaceAll(".groovy","");
		
		return fname;
	}

}
