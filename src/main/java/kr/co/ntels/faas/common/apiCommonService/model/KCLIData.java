/**
 * 
 */
package kr.co.ntels.faas.common.apiCommonService.model;

import java.io.File;

import com.fasterxml.jackson.databind.ObjectMapper;

/**
  * @FileName : KCLIData.java
  * @Project : FaasKubeAPI
  * @Date : 2020. 1. 30. 
  * @작성자 : sig61 - 백광욱
  * @변경이력 :
  * @프로그램 설명 :
  */

public class KCLIData {
	
	public enum E_FILE_TYPE {JSON , YMAL , GROOVY };
	
	public String cmd;
	public String msg;
	public boolean result = false;
	
	public String fnameOnly;
	public String fname;
	public String content;
	E_FILE_TYPE type = E_FILE_TYPE.JSON;
	
	public KCLIData() {}
	public KCLIData(String fname , E_FILE_TYPE type) 
	{
		this.fname = fname;
		this.type = type;
	}
	
	@Override
	 protected void finalize() throws Throwable { 
		if(!"".equals(this.fname))
		{
			File f = new File(this.fname);
			if(f != null) f.delete();
		}
	 }
	
	public String toJsonString() throws Exception  
	{
		return new ObjectMapper().writeValueAsString(this);
	}

}
