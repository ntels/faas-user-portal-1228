package kr.co.ntels.faas.common.utils;

import kr.co.ntels.faas.template.service.TemplateService;
import kr.co.ntels.faas.trigger.vo.TimerTriggerVo;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Map;

public class CronUtil {
    private static SimpleDateFormat date_format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

    public static String getCronExpression(TimerTriggerVo timer) throws Exception {
        String repeat_tp = timer.getRepeat_tp();
        JSONObject repeat_val = new JSONObject(timer.getRepeat_val());

        Date bas_date_hms = date_format.parse(timer.getBas_date_hms());

        String ret = convertCronExpression(repeat_tp, repeat_val, bas_date_hms);

        return ret;
    }

    public static String getCronExpression(JSONObject config_obj) throws Exception {
        String repeat_tp = config_obj.get("repeat_tp").toString();
        JSONObject repeat_val = (JSONObject) config_obj.get("repeat_val");

        Date bas_date_hms = date_format.parse(config_obj.get("bas_date_hms").toString());

        String ret = convertCronExpression(repeat_tp, repeat_val, bas_date_hms);

        return ret;
    }

    private static String convertCronExpression(String repeat_tp, JSONObject repeat_val, Date bas_date_hms) throws Exception {
        String ret = "";

        int cal;
        String exp;

        switch(repeat_tp) {
            case "NO":
                Calendar cd = Calendar.getInstance();
                cd.setTime(bas_date_hms);

                ret = String.format("%s %s %s %s %s ? *", "0", cd.get(Calendar.MINUTE), cd.get(Calendar.HOUR), cd.get(Calendar.DATE), cd.get(Calendar.MONTH)+1);
                break;
            case "MN":
                cal = bas_date_hms.getMinutes() % Integer.parseInt(repeat_val.get("value").toString());
                exp = cal + "/" + Integer.parseInt(repeat_val.get("value").toString());

                ret = String.format("%s %s %s %s %s %s", "0", exp, "*", "*", "*", "?");
                break;
            case "HR":
                cal = bas_date_hms.getHours() % Integer.parseInt(repeat_val.get("value").toString());
                exp = cal + "/" + Integer.parseInt(repeat_val.get("value").toString());

                ret = String.format("%s %s %s %s %s %s", "0", bas_date_hms.getMinutes(), exp, "*", "*", "?");
                break;
            case "DY":
                if(repeat_val.get("radio").toString().equals("day")) {
                    cal = bas_date_hms.getDay() % Integer.parseInt(repeat_val.get("value").toString());
                    exp = cal + "/" + Integer.parseInt(repeat_val.get("value").toString());

                    ret = String.format("%s %s %s %s %s %s %s", "0", bas_date_hms.getMinutes(), bas_date_hms.getHours(), exp, "*", "?", "*");
                } else if(repeat_val.get("radio").toString().equals("week")) {
                    exp = "MON-FRI";

                    ret = String.format("%s %s %s %s %s %s %s", "0", bas_date_hms.getMinutes(), bas_date_hms.getHours(), "?", "*", exp, "*");
                }
                break;
            case "WK":
                exp = repeat_val.get("value").toString().replace(" ", "");

                ret = String.format("%s %s %s %s %s %s %s", "0", bas_date_hms.getMinutes(), bas_date_hms.getHours(), "?", "*", exp, "*");
                break;
            case "MO":
                if(repeat_val.get("radio").toString().equals("day")) {
                    exp = repeat_val.get("value").toString();

                    ret = String.format("%s %s %s %s %s %s %s", "0", bas_date_hms.getMinutes(), bas_date_hms.getHours(), exp, "*", "?", "*");
                } else if(repeat_val.get("radio").toString().equals("week")) {
                    exp = repeat_val.get("value_week").toString() + "#" + repeat_val.get("value_th").toString();

                    ret = String.format("%s %s %s %s %s %s %s", "0", bas_date_hms.getMinutes(), bas_date_hms.getHours(), "?", "*", exp, "*");
                }
                break;
            case "YR":
                if(repeat_val.get("radio").toString().equals("month")) {
                    exp = repeat_val.get("value").toString();

                    ret = String.format("%s %s %s %s %s %s %s", "0", bas_date_hms.getMinutes(), bas_date_hms.getHours(), bas_date_hms.getDate(), exp, "?", "*");
                } else if(repeat_val.get("radio").toString().equals("week")) {
                    exp = repeat_val.get("value_week").toString() + "#" + repeat_val.get("value_th").toString();

                    ret = String.format("%s %s %s %s %s %s %s", "0", bas_date_hms.getMinutes(), bas_date_hms.getHours(), "?", bas_date_hms.getMonth(), exp, "*");
                }
                break;
            default:
                throw new Exception("undefined timer trigger's repeat type");
        }
        
        return ret;
    }


    public static void main(String[] args) throws Exception {
        TimerTriggerVo timer = new TimerTriggerVo();
        timer.setBas_date_hms("2020-05-01 112233");

        timer.setRepeat_tp("NO");
        timer.setRepeat_val("{}");
        System.out.println(timer.getRepeat_tp() + CronUtil.getCronExpression(timer));

        timer.setRepeat_tp("MN");
        timer.setRepeat_val("{'value':'3'}");
        System.out.println(timer.getRepeat_tp() + CronUtil.getCronExpression(timer));

        timer.setRepeat_tp("HR");
        timer.setRepeat_val("{'value':'3'}");
        System.out.println(timer.getRepeat_tp() + CronUtil.getCronExpression(timer));

        timer.setRepeat_tp("DY");
        timer.setRepeat_val("{'radio':'day', 'value':'3'}");
        System.out.println(timer.getRepeat_tp() + CronUtil.getCronExpression(timer));

        timer.setRepeat_tp("DY");
        timer.setRepeat_val("{'radio':'week'}");
        System.out.println(timer.getRepeat_tp() + CronUtil.getCronExpression(timer));

        timer.setRepeat_tp("WK");
        timer.setRepeat_val("{'value':'WED,THU,FRI'}");
        System.out.println(timer.getRepeat_tp() + CronUtil.getCronExpression(timer));

        timer.setRepeat_tp("MO");
        timer.setRepeat_val("{'radio':'day', 'value':'3'}");
        System.out.println(timer.getRepeat_tp() + CronUtil.getCronExpression(timer));

        timer.setRepeat_tp("MO");
        timer.setRepeat_val("{'radio':'week', 'value_th':'3', 'value_week':'WED'}");
        System.out.println(timer.getRepeat_tp() + CronUtil.getCronExpression(timer));

        timer.setRepeat_tp("YR");
        timer.setRepeat_val("{'radio':'month', 'value':'3'}");
        System.out.println(timer.getRepeat_tp() + CronUtil.getCronExpression(timer));

        timer.setRepeat_tp("YR");
        timer.setRepeat_val("{'radio':'week', 'value_th':'3', 'value_week':'WED'}");
        System.out.println(timer.getRepeat_tp() + CronUtil.getCronExpression(timer));
    }


}
