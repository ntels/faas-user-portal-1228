package kr.co.ntels.faas.common.apiCommon.util.polling;

import java.time.LocalDateTime;
import java.util.List;
import java.util.Map;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.mybatis.spring.SqlSessionTemplate;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.fasterxml.jackson.databind.ObjectMapper;

import kr.co.ntels.faas.common.apiCommon.model.QueueData;
import kr.co.ntels.faas.common.apiCommon.util.polling.queue.PollingQueue;

public class DBPolling {
	
	private static final Logger Logger = LoggerFactory.getLogger(DBPolling.class );
	private static final Log Log = LogFactory.getLog(DBPolling.class );

	SqlSessionTemplate template;
	String mapper;
	Map<String,Object> param;
	PollingQueue queue = null;
	int pollingTimeSec = 3;
	
	boolean _stopPolling = false;
	Thread _thread = null;
	
	public DBPolling()
	{
		
	}
	
	public DBPolling(SqlSessionTemplate template, String mapper, Map<String,Object> param, PollingQueue queue, int pollingTimeSec)
	{
		this.template = template;
		this.mapper = mapper;
		this.param = param;
		this.queue = queue;
		this.pollingTimeSec = pollingTimeSec;
		
		startThread();
	}
	
	public void stopPolling()
	{
		_stopPolling = true;
	}
	
	private void startThread()
	{
		_thread = new Thread(()->{
		      
		    while(true)
		    {
			    try 
			    {
			    	polling();
				} catch (Exception e) {
					Log.error("DBPolling : 데이타 조회중 문제가 있습니다." + e.getMessage());
				}
			    
			    try 
			    {
			    	Thread.sleep(pollingTimeSec * 1000);
				} catch (Exception e) {
				}
			    
			    if(_stopPolling)
			    	break;
		    }
		});
		
		_thread.setName("DBPolling");
		_thread.start();
	}
	
	private void polling() throws Exception
	{
		if(this.template != null)
		{
			ObjectMapper objectMapper = new ObjectMapper();
			List<Map<String, Object>> list = this.template.selectList(mapper, param);
			String json = objectMapper.writeValueAsString(list);
			
			QueueData data = new QueueData();
			data.setSourceObj(this);
			data.setTime(LocalDateTime.now());
			data.setMessage(json);
			queue.push(data);
			
		}
	}
}
