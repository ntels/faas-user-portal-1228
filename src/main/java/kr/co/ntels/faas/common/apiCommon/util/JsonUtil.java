package kr.co.ntels.faas.common.apiCommon.util;

import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.jayway.jsonpath.JsonPath;
import com.jayway.jsonpath.PathNotFoundException;

import kr.co.ntels.faas.common.apiCommon.model.APIJsonData;
import kr.co.ntels.faas.common.apiCommon.model.APIResponseData;
import kr.co.ntels.faas.common.apiCommon.model.Constant;
import kr.co.ntels.faas.common.apiCommon.model.Constant.E_ORDERBY;
import net.minidev.json.JSONArray;

/**
  * @FileName : JsonUtil.java
  * @Project : api-portal
  * @Date : 2018. 12. 7. 
  * @작성자 : sig61 - 백광욱
  * @변경이력 :
  * @프로그램 설명 :
  */
public class JsonUtil {
	
	private static final Logger Logger = LoggerFactory.getLogger(JsonUtil.class );
	
	public static APIJsonData getFilterJson(APIJsonData apiJsonDataOrg , String jsonPath) throws Exception
	{
		JSONArray j = new JSONArray();
		APIJsonData apiJsonData = new APIJsonData();
		
		try
		{
			Object obj = JsonPath.read(apiJsonDataOrg.getJsonString(),jsonPath);
			if(obj instanceof JSONArray)
				j = (JSONArray)obj;
			else if(obj instanceof LinkedHashMap || obj instanceof String)
			{
				j.add(obj);
			}
			else
			{
				j.add(obj);
			}
			
			Logger.debug(obj.toString());
		}
		catch(PathNotFoundException ex)
		{
			Logger.debug("PathNotFoundException jsonPath={}", jsonPath);
		}
		
		apiJsonData.setHttpStatus(apiJsonDataOrg.getHttpStatus());
		apiJsonData.setJsonArray(j);
		apiJsonData.setJsonString(j.toJSONString());
		
		return apiJsonData;
		
	}
	
		
	public static APIJsonData getFilterJson(APIResponseData apiResponseData , String jsonPath) throws Exception
	{
		APIJsonData apiJsonData = new APIJsonData();
		APIJsonData apiJsonDataOrg = new APIJsonData(apiResponseData);
		
		JSONArray j = new JSONArray();
		try
		{
			if(StringUtil.isEmpty(jsonPath))
			{
				Object obj = JsonPath.read(apiJsonDataOrg.getJsonString(),"$");
				if(obj instanceof JSONArray)
					j = (JSONArray)obj;
				else if(obj instanceof LinkedHashMap || obj instanceof String)
				{
					j.add(obj);
				}
				else
				{
					j.add(obj);
				}
				
				Logger.debug(obj.toString());
			}
			else
			{
				Object obj = JsonPath.read(apiJsonDataOrg.getJsonString(),jsonPath);
				if(obj instanceof JSONArray)
					j = (JSONArray)obj;
				else if(obj instanceof LinkedHashMap || obj instanceof String)
				{
					j.add(obj);
				}
				else
				{
					j.add(obj);
				}
				
				Logger.debug(obj.toString());
			}
		}
		catch(PathNotFoundException ex)
		{
			Logger.debug("PathNotFoundException jsonPath={}", jsonPath);
		}
		
		apiJsonData.setHttpStatus(apiJsonDataOrg.getHttpStatus());
		apiJsonData.setJsonArray(j);
		apiJsonData.setJsonString(j.toJSONString());
		
		return apiJsonData;
		
	}
	
	public static int getJsonArrayCount(APIJsonData apiJsonData)
	{
		return apiJsonData.getJsonArray().size();
	}
	
	public static List<APIJsonData> getJsonArrayUnit(APIJsonData apiJsonData , String jsonPath ) throws Exception
	{
		return getJsonArrayUnit(apiJsonData,jsonPath,Constant.E_ORDERBY.ASC,-1);
	}
	
	public static List<APIJsonData> getJsonArrayUnit(APIJsonData apiJsonData , String jsonPath , Constant.E_ORDERBY orderBy , int unitCnt) throws Exception
	{
		APIJsonData jsonData = getFilterJson(apiJsonData,jsonPath);
		return getJsonArrayUnit(jsonData,orderBy,unitCnt);
	}
	
	public static List<APIJsonData> getJsonArrayUnit(APIJsonData apiJsonData , Constant.E_ORDERBY orderBy , int unitCnt) throws Exception
	{
		List<APIJsonData> list = new ArrayList<>();
		int icnt = getJsonArrayCount(apiJsonData);
		
		for(int i=0;i<icnt;i++)
		{
			int index = i;
			if(orderBy == E_ORDERBY.DESC)
				index = icnt - i - 1;
				
			String jsonPath = String.format("$.[%d]", index);
			APIJsonData apiJsonDataTag = getFilterJson(apiJsonData,jsonPath);
			list.add(apiJsonDataTag);
			
			if(unitCnt > 0) // 0 보다 작으면 전체 조회다.
			{
				if(i >= (unitCnt-1))
					break;
			}
		}
		
		return list;
	}
	
	//==
	public static <T> void jsonDataMapKeyMapper(Map<String,Object> map , String keys,T t)
	{
		Logger.debug("================jsonDataMapKeyMapper==========");
		
		List<Object> list = null;
		boolean isLast = false;
		String[] arrKey = keys.split("[:]");
		
		try
		{
			for(int i = 0; i<arrKey.length ; i++)
			{
				String key = arrKey[i];
				if(i == arrKey.length-1) isLast = true;
				if(StringUtil.isEmpty(key)) continue;
				
				Object obj = null;
				
				if(list != null)
				{
					obj = jsonDataMapKeyMapper(list,key);
					if(obj == null) return;
					list = null;
					map = (Map<String,Object>)obj;
				}
	
				obj = jsonDataMapKeyMapper(map,key);
				
				if(obj == null)
					return;
				else	
				{
					if(isLast)
					{
						map.put(key, t);
						return;
					}
					
					if(obj instanceof LinkedHashMap)
					{
						map = (Map<String,Object>)obj;
						continue;
					}
					else if(obj instanceof ArrayList)
					{
						list = (List<Object>)obj;
					}
				}
			}
		}
		catch(Exception e)
		{
			Logger.debug("================jsonDataMapKeyMapper ERROR==========");
			throw e;
		}
	}
	
	private static Object jsonDataMapKeyMapper(Map<String,Object> map , String key)
	{
		if(map.containsKey(key))
			return map.get(key);
		else
			return null;
	}
	
	private static Map<String,Object> jsonDataMapKeyMapper(List<Object> list , String key)
	{
		for(Object obj : list)
		{
			if(obj instanceof LinkedHashMap)
			{
				Object mapObj = jsonDataMapKeyMapper((Map<String,Object>)obj , key);
				if(mapObj != null)
					return (Map<String,Object>)obj;
			}
		}
		return null;
	}
	//==
	
	
	public static <T> T jsonDataMapper(String jsonString , Class<T> t) throws Exception
	{
		APIResponseData response = new APIResponseData(200,jsonString);
		APIJsonData apiJsonData = new APIJsonData(response);
		
		return jsonDataMapper(apiJsonData , t);
	}
			
	public static <T> T jsonDataMapper(Object object , Class<T> t) throws Exception
	{
		String jsonString = new ObjectMapper().writeValueAsString(object);
		return jsonDataMapper(jsonString,t);
	}
	
	//==
	public static <T> T jsonDataMapper(APIJsonData apiJsonDataOrg , Class<T> t) throws Exception
	{
		T obj = t.newInstance();
		List<Field> listField = new ArrayList<>();
		List<Method> listMethod = new ArrayList<>();
		
		listMethod = getDeclaredMethodsAll(t, listMethod);
		listField = getDeclaredFieldsAll(t , listField);
		for(Field field : listField)
		{
			JsonSearch jsonSearch = field.getDeclaredAnnotation(JsonSearch.class);
			if(jsonSearch != null)
			{
				String path = jsonSearch.path();
				String scope = jsonSearch.scope();
				
				String[] jsonPaths = path.split(",");
				
				for(String jsonPath : jsonPaths)
				{
					String val = getSearchData(apiJsonDataOrg,jsonPath.trim()).toString();
					if(val == null || "".equals(val)) continue;
					
					if("function".equals(scope))
					{
						Method method = findMethod(listMethod,field.getName());
						if(method != null)
						{
							method.setAccessible(true);
							method.invoke(obj, val);
						}
					}
					else
					{
						field.setAccessible(true);
						field.set(obj, val);
					}
				}
			}
		}
		
		
		for(Method method : listMethod)
		{
			JsonSearch jsonSearch = method.getDeclaredAnnotation(JsonSearch.class);
			if(jsonSearch != null)
			{
				String jsonPath = jsonSearch.path();
				String val = getSearchData(apiJsonDataOrg,jsonPath).toString();
				
				method.setAccessible(true);
				method.invoke(obj, val);
			}
		}
		
		return obj;
	}
	
	private static Method findMethod(List<Method> listMethod , String fieldName)
	{
		for(Method method : listMethod)
		{
			String name = String.format("set%s%s", fieldName.substring(0, 1).toUpperCase(),fieldName.substring(1));
			if(method.getName().equals(name))
				return method;
		}
		return null;
	}
	
	private static <T> List<Field> getDeclaredFieldsAll(Class<T> t , List<Field> listField )
	{
		listField.addAll(Arrays.asList(t.getDeclaredFields()));

		Class superClazz = t.getSuperclass();
		if(superClazz != null)
			getDeclaredFieldsAll(superClazz, listField);
		
		return listField;
	}
	
	private static <T> List<Method> getDeclaredMethodsAll(Class<T> t , List<Method> listMethod )
	{
		listMethod.addAll(Arrays.asList(t.getDeclaredMethods()));

		Class superClazz = t.getSuperclass();
		if(superClazz != null)
			getDeclaredMethodsAll(superClazz, listMethod);
		
		return listMethod;
	}
	
	private static Object getSearchData(APIJsonData apiJsonDataOrg ,String jsonPath)
	{
		String val = "";
		if(!StringUtil.isEmpty(jsonPath))
		{
			try
			{
				APIJsonData apiJsonData = getFilterJson(apiJsonDataOrg , jsonPath);
				JSONArray j = apiJsonData.getJsonArray();
				if(j != null && j.size() > 0)
				{
					if(j.size() == 1)
					{
						Object o = j.get(0);
						if(o != null)// && o instanceof String)
						{
							val = o.toString();
						}
					}
					else
					{
						val = j.toJSONString();
					}
				}
			}
			catch(Exception e)
			{
				val = "?";
			}
		}
		return val;
	}
	//==
	
}
