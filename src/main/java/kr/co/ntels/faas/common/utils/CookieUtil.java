package kr.co.ntels.faas.common.utils;

import org.springframework.util.StringUtils;
import org.springframework.web.context.request.RequestAttributes;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;

public class CookieUtil {
    public static String hubpopProjectId(){
        return getCookie("PROJECT-ID");
    }

    public static String faasProjectId(){
        String id = getCookie("PROJECT-ID");

        return CommCons.faasProjectId(id);
    }

    public static String getCookie(String name){
        return getCookieValue(name);
    }


    private static String getCookieValue(String name) {
        if(StringUtils.isEmpty(name))
            return null;

        HttpServletRequest request = getCurrentHttpRequest();

        Cookie[] cookies = request.getCookies();
        if (cookies != null) {
            for (Cookie cookie : cookies) {
                if (name.equals(cookie.getName())) {
                    return cookie.getValue();
                }
            }
        }

        return null;
    }

    private static HttpServletRequest getCurrentHttpRequest(){
        RequestAttributes requestAttributes = RequestContextHolder.getRequestAttributes();
        if (requestAttributes instanceof ServletRequestAttributes) {
            HttpServletRequest request = ((ServletRequestAttributes)requestAttributes).getRequest();
            return request;
        }

        return null;
    }
}
