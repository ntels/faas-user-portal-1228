package kr.co.ntels.faas.common.vo;

public class ResultVo {
    private boolean isSuccess = false;
    private String message;

    public ResultVo() {}
    public ResultVo(boolean success) {
        this.isSuccess = success;
    }

    public ResultVo(boolean success, String message) {
        this.isSuccess = success;
        this.message = message;
    }

    public boolean isSuccess() {
        return isSuccess;
    }

    public void setSuccess(boolean success) {
        isSuccess = success;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
