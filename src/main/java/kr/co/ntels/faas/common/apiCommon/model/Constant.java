/**
 * 
 */
package kr.co.ntels.faas.common.apiCommon.model;

/**
  * @FileName : Constant.java
  * @Project : faas-portal
  * @Date : 2020. 2. 19. 
  * @작성자 : sig61 - 백광욱
  * @변경이력 :
  * @프로그램 설명 :
  */

	
public class Constant {
	public enum E_YN {Y , N};
	public enum E_ORDERBY {ASC , DESC};
	public enum E_ORDER_TYPE {NONE ,CREATE_DATE,MODIFY_DATE,NAME};
	public enum E_SERVICE_FILTER { NONE, NODEJS , PYTHON };
	
	

}
