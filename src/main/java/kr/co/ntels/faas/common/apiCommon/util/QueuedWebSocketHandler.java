package kr.co.ntels.faas.common.apiCommon.util;

import java.io.IOException;
import java.util.List;
import java.util.concurrent.CopyOnWriteArrayList;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.socket.CloseStatus;
import org.springframework.web.socket.TextMessage;
import org.springframework.web.socket.WebSocketSession;
import org.springframework.web.socket.handler.TextWebSocketHandler;

import kr.co.ntels.faas.common.apiCommon.util.polling.HttpLongPolling;
import kr.co.ntels.faas.common.apiCommon.util.polling.queue.IQueuePop;
import kr.co.ntels.faas.common.apiCommon.util.polling.queue.IQueuePush;
import kr.co.ntels.faas.common.apiCommon.util.polling.queue.PollingQueue;

public abstract class QueuedWebSocketHandler extends TextWebSocketHandler implements IQueuePush , IQueuePop {
	
	Thread _thread = null;
	boolean _bStop = false;
	protected List<WebSocketSession> sessions = new CopyOnWriteArrayList<>();
	protected PollingQueue queue = new PollingQueue();
	protected int pollingTime = 1000;
	
	private static final Logger Logger = LoggerFactory.getLogger(QueuedWebSocketHandler.class );
	private static final Log Log = LogFactory.getLog(QueuedWebSocketHandler.class );
	
	
	@PostConstruct
	public void init() throws Exception 
	{
		queue.setQueueProcess(this, this);
		
		doProducer();
		startThread();
	}
	
	@PreDestroy
	public void destroy()
	{
		endThread();
	}

	@Override
	public void handleTextMessage(WebSocketSession session, TextMessage message)
			throws InterruptedException, IOException {

	}

	@Override
	public void afterConnectionEstablished(WebSocketSession session) throws Exception {
		sessions.add(session);
		
		Log.info("\n ==>WebSocket session(+) cnt "+ getCountActiveSession());
	}
	
	@Override
	public void afterConnectionClosed(WebSocketSession session, CloseStatus status) throws Exception {
		sessions.remove(session);
		Log.info("\n ==>WebSocket session(-) cnt "+ getCountActiveSession());
	}
	
	public void sendAllSession(String content) throws IOException
	{
		Log.info("\n ==>WebSocket send : client("+ getCountActiveSession()+")");
		
		for(Object obj : sessions) 
		{
			WebSocketSession webSocketSession = (WebSocketSession)obj;
			if(webSocketSession != null && webSocketSession.isOpen())
			{
				webSocketSession.sendMessage(new TextMessage(content));
			}
		}
	}
	
	protected int getCountActiveSession()
	{
		int cnt = 0;
		for(Object obj : sessions) 
		{
			WebSocketSession webSocketSession = (WebSocketSession)obj;
			if(webSocketSession != null && webSocketSession.isOpen())
			{
				cnt++;
			}
		}
		
		return cnt;
	}
	
	protected abstract void doProducer() throws Exception;  // Queus에 데이타를 넣기 위한 작업
	
	protected abstract void doConsumer(PollingQueue queue) throws Exception; // Queus에 데이타를 가져오기 위한 작업
	
	protected void startThread() throws Exception
	{
		_thread = new Thread(()->{
		      
		    while(true)
		    {
			    try 
			    {
			    	doConsumer(queue);
			    	
				} catch (Exception e) {
				}
			      
			    if(_bStop)
			    	return;

			    try 
			    {
			    	Thread.sleep(pollingTime);			    	
				} catch (Exception e) {
				}
			   
		    }
		});
		
		_thread.setName("Thread : "+getClass().getSimpleName());
		_thread.start();
	}
	
	public void endThread()
	{
		_bStop = true;		
	}

}
