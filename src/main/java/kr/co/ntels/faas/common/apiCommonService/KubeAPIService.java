/**
 * 
 */
package kr.co.ntels.faas.common.apiCommonService;

import java.util.ArrayList;
import java.util.List;

import org.springframework.stereotype.Service;

import kr.co.ntels.faas.common.apiCommon.model.APIJsonData;
import kr.co.ntels.faas.common.apiCommonService.baseService.ServiceBase;

/**
  * @FileName : KubeAPIService.java
  * @Project : FaasSimpleAPI
  * @Date : 2020. 1. 15. 
  * @작성자 : sig61 - 백광욱
  * @변경이력 :
  * @프로그램 설명 :
  */

@Service
public class KubeAPIService extends ServiceBase{

	public APIJsonData getServiceJson(String apiPath , String jsonPath) throws Exception
	{
 		String url = String.format("%s/%s", baseUrl , apiPath);
		APIJsonData data = getAPIGetData(url,jsonPath);
		
		return data;
	}
	
	public APIJsonData getObjectJson(String groupVersion , String namespace, String resourceName , String name) throws Exception
	{
 		String url = String.format("%s/%s/namespaces/%s/", baseUrl , groupVersion , namespace , resourceName , name );
		APIJsonData data = getAPIGetData(url,"");
		
		return data;
	}
	
	public List<List<String>> getServicListJson(String apiPath , String filterJson, List<String> listFiedlJsonPath) throws Exception
	{
 		String url = String.format("%s/%s", baseUrl , apiPath);
		APIJsonData data = getAPIGetData(url,filterJson);
		
		List<List<String>> retList = new ArrayList<>();
		for(int i=0;i<data.getJsonArray().size();i++)
		{
			List<String> list = new ArrayList<>();
			APIJsonData apiJsonData = getAPIJsonData(data,"$.["+i+"]");
			for(String jsonPath :listFiedlJsonPath)
			{
				if("".equals(jsonPath))
					list.add("");
				else
				{
					APIJsonData fieldData = getAPIJsonData(apiJsonData,jsonPath);
					list.add(fieldData.toString());
				}
			}
			retList.add(list);
		}
		
		return retList;
		
	}

}
