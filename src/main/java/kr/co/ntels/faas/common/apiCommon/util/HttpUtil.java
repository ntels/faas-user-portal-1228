package kr.co.ntels.faas.common.apiCommon.util;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.security.cert.X509Certificate;
import java.util.Map;
import java.util.Queue;

import javax.net.ssl.HttpsURLConnection;
import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLException;
import javax.net.ssl.TrustManager;
import javax.net.ssl.X509TrustManager;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import kr.co.ntels.faas.common.apiCommon.model.APIResponseData;
import kr.co.ntels.faas.common.apiCommon.model.ApiResourceInfo;
import kr.co.ntels.faas.common.apiCommon.model.QueueData;

/**
  * @FileName : HttpUtil.java
  * @Project : api-portal
  * @Date : 2018. 12. 7. 
  * @작성자 : sig61 - 백광욱
  * @변경이력 :
  * @프로그램 설명 :
  */
public class HttpUtil {
	
	@SuppressWarnings("serial")
	public static class APIKeyException extends Exception{
		public APIKeyException()
		{
			super("서버 설정에 문제가 있습니다. 관리자에게 문의하세요. (API 접근권한이 없습니다.)");
		}
	}
	
	private static final Logger Logger = LoggerFactory.getLogger(HttpUtil.class );
	private static final Log Log = LogFactory.getLog(HttpUtil.class );
	
	public static APIResponseData getAPIData(String urlString , String method ,Map<String,Object> mapHeader, String postData) throws Exception
	{
		
		HttpURLConnection conn = null;
		BufferedReader reader = null;
		StringBuilder builder = new StringBuilder(); //문자열을 담기 위한 객체
		InputStream is = null;
		APIResponseData apiResponseData = null;
		
		try {
			
			URL url = new URL(urlString);
			conn = (HttpURLConnection) url.openConnection();
			conn.setRequestMethod(method);

			//헤더 정보 셋팅
			if(mapHeader != null)
			{
				for(String key : mapHeader.keySet())
				{
					conn.setRequestProperty(key, mapHeader.get(key).toString());
				}
			}
			
			//post 데이타 전송 정의
			if(postData != null && !"".equals(postData))
			{
				byte[] postDataBytes = postData.getBytes("utf8");
				conn.setDoOutput(true);
		        conn.getOutputStream().write(postDataBytes);
			}
			
			int statusCode = conn.getResponseCode();
			is = ((int)statusCode/200) == 1 ? conn.getInputStream() : conn.getErrorStream(); //input스트림 개방
			if(statusCode == 401)
				throw new APIKeyException();
				

			if(is != null)
			{
				builder = new StringBuilder(); //문자열을 담기 위한 객체
				reader = new BufferedReader(new InputStreamReader(is,"UTF-8")); //문자열 셋 세팅
				String line;
	
				while ((line = reader.readLine()) != null) {
					builder.append(line);
				}
				
				Logger.debug(builder.toString());
			}
			
			apiResponseData = new APIResponseData(statusCode,builder.toString());
			return apiResponseData;

		} catch (SSLException e) {
			
			Log.warn( "\n=========================================="
					+ "\n|                  WARN                  |"
					+ "\n------------------------------------------"
					+ "\n| 서버SSL 인증에 문제가 있습니다.                |"
					+ "\n| SSL인증을 무시하고 진행합니다.                 |"
					+ "\n| 보안에 취약할 수 있으니 서버 SSL 인증서 점검하세요.  |"
					+ "\n==================message================="
					+ "\n"
					+ e.getMessage()
					+ "\n==========================================", null);
			
			try
			{
				return getAPIDataIgnoreSSL(urlString , method , mapHeader , postData);
			}
			catch(Exception ee)
			{
				Log.error(ee.getMessage(),ee);
				throw ee;
			}
		}
		finally
		{
			if(conn != null) conn.disconnect();
			if(is != null) is.close();
			if(reader != null) reader.close();
			
		}
	}
	
	private static APIResponseData getAPIDataIgnoreSSL(String urlString , String method ,Map<String,Object> mapHeader, String postData) throws Exception
	{
		HttpURLConnection conn = null;
		BufferedReader reader = null;
		StringBuilder builder = new StringBuilder(); //문자열을 담기 위한 객체
		InputStream is = null;
		APIResponseData apiResponseData = null;
		
		try {
			
			if(urlString.startsWith("https://"))
			{
				//ssl 접속에 대해서 인증서 무시하도록 하는 코드
				TrustManager[] trustAllCerts = new TrustManager[] { new X509TrustManager() {
					public java.security.cert.X509Certificate[] getAcceptedIssuers() {
						return null;
					}
	
					public void checkClientTrusted(X509Certificate[] certs,
							String authType) {
					}
	
					public void checkServerTrusted(X509Certificate[] certs,
							String authType) {
					}
				} };
	
				SSLContext sc = SSLContext.getInstance("SSL");
				sc.init(null, trustAllCerts, new java.security.SecureRandom());
				HttpsURLConnection.setDefaultSSLSocketFactory(sc.getSocketFactory());
				//ssl 접속에 대해서 인증서 무시하도록 하는 코드
			}

			URL url = new URL(urlString);
			conn = (HttpURLConnection) url.openConnection();
			conn.setRequestMethod(method);

			//헤더 정보 셋팅
			if(mapHeader != null)
			{
				for(String key : mapHeader.keySet())
				{
					conn.setRequestProperty(key, mapHeader.get(key).toString());
				}
			}
			
			//post 데이타 전송 정의
			if(postData != null && !"".equals(postData))
			{
				byte[] postDataBytes = postData.getBytes("utf8");
				conn.setDoOutput(true);
		        conn.getOutputStream().write(postDataBytes);
			}
			
			int statusCode = conn.getResponseCode();
			is = ((int)statusCode/200) == 1 ? conn.getInputStream() : conn.getErrorStream(); //input스트림 개방
			
			if(statusCode == 401)
				throw new APIKeyException();

			if(is != null)
			{
				builder = new StringBuilder(); //문자열을 담기 위한 객체
				reader = new BufferedReader(new InputStreamReader(is,"UTF-8")); //문자열 셋 세팅
				String line;
	
				while ((line = reader.readLine()) != null) {
					builder.append(line);
				}
				
				Logger.debug(builder.toString());
			}
			apiResponseData = new APIResponseData(statusCode,builder.toString());
			return apiResponseData;

		} catch (Exception e) {
			Log.error(e.getMessage(),e);
			throw e;
		}
		finally
		{
			if(conn != null) conn.disconnect();
			if(is != null) is.close();
			if(reader != null) reader.close();
			
		}
	}
	
	public static ApiResourceInfo getApiResourceData(String urlString , String method ,Map<String,Object> mapHeader) throws Exception
	{
		HttpURLConnection conn = null;
		BufferedReader reader = null;
		InputStream is = null;
		byte content[] = new byte[0];
		ApiResourceInfo apiResourceInfo = new ApiResourceInfo();
		
		try {
			
			URL url = new URL(urlString);
			conn = (HttpURLConnection) url.openConnection();
			conn.setRequestMethod(method);
			// conn.setChunkedStreamingMode(-1);

			//헤더 정보 셋팅
			if(mapHeader != null)
			{
				for(String key : mapHeader.keySet())
				{
					conn.setRequestProperty(key, mapHeader.get(key).toString());
				}
			}
			
			int statusCode = conn.getResponseCode();
			int size =0;
			is = ((int)statusCode/200) == 1 ? conn.getInputStream() : conn.getErrorStream(); //input스트림 개방
			apiResourceInfo.setStatusCode(statusCode);

			if(is != null)
			{
				//if(size > 0)
				//{
					
					
					int read = 0,total = 0;
					byte[] buf = new byte[1024*8];
					while((read=is.read(buf)) != -1)
					{
						long aaa = conn.getContentLengthLong();
						System.out.println(aaa+" total= " + total + " read="+read);
						System.out.println(conn.getHeaderFields().toString());
						
						
						if(size < (read+total))
						{
							byte[] contentBuf = new byte[read+total];
							size = read+total;
							if(total > 0)
								System.arraycopy(content, 0 , contentBuf, 0 , total);
							content = contentBuf;
						}
						
						System.arraycopy(buf, 0 , content, total , read);
						total += read;
					}
					
					apiResourceInfo.setContent(content);
					apiResourceInfo.setContentType(conn.getContentType());
					
				/*
				}
				else
				{
					StringBuilder builder = new StringBuilder(); //문자열을 담기 위한 객체
					reader = new BufferedReader(new InputStreamReader(is,"UTF-8")); //문자열 셋 세팅
					String line;
		
					while ((line = reader.readLine()) != null) {
						builder.append(line);
					}
					
					apiResourceInfo.setContent(builder.toString().getBytes());
					apiResourceInfo.setContentType(conn.getContentType());
				}
				*/
			}
			else
			{
				String msg = String.format("%s 해당 정보가 없습니다.", urlString);
				throw new Exception(msg);
			}
		} catch (Exception e) {
			throw e;
		}
		finally
		{
			if(conn != null) conn.disconnect();
			if(is != null) is.close();
			
		}
		return apiResourceInfo;
	}
}
