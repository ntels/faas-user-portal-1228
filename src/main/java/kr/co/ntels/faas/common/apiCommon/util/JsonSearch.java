package kr.co.ntels.faas.common.apiCommon.util;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
  * @FileName : JsonSearch.java
  * @Project : api-portal
  * @Date : 2018. 12. 7. 
  * @작성자 : sig61 - 백광욱
  * @변경이력 :
  * @프로그램 설명 :
  */
@Target({ElementType.METHOD, ElementType.FIELD})
@Retention(RetentionPolicy.RUNTIME)
public @interface JsonSearch {
	
	String path() default "";
	String scope() default ""; // function 인 경우 set 함수 호출함.
}
