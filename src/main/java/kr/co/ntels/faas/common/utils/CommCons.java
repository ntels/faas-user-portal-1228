package kr.co.ntels.faas.common.utils;

import org.springframework.util.StringUtils;


/**
 * 공통코드 외에 고정값이나 규칙에 맞게 생성되는 값들 모음.
 */
public class CommCons {

    // db에 따로 관리하지 않기로 함. 규칙에 맞게 깃 만들어 씁시다.
    public static String templateGitpath(int templateId) {
        return String.format("faas-template/template%d", templateId);
    }

    public static String faasProjectId(String projectId){
        return StringUtils.isEmpty(projectId) == false ? "faas-p" + projectId : null;
    }

}
