package kr.co.ntels.faas.common.apiCommonService.baseService;

import java.util.ArrayList;
import java.util.Base64;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.PostConstruct;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;

import kr.co.ntels.faas.common.apiCommon.model.APIJsonData;
import kr.co.ntels.faas.common.apiCommon.model.APIResponseData;
import kr.co.ntels.faas.common.apiCommon.util.HttpUtil;
import kr.co.ntels.faas.common.apiCommon.util.JsonUtil;
import kr.co.ntels.faas.common.apiCommon.util.StringUtil;
import net.minidev.json.JSONArray;




/**
  * @FileName : ServiceBase.java
  * @Project : kubernetes_dashboards
  * @Date : 2020. 1. 10. 
  * @작성자 : sig61 - 백광욱
  * @변경이력 :
  * @프로그램 설명 :
  */

public class ServiceBase {
	
	private static final Logger Logger = LoggerFactory.getLogger(ServiceBase.class );

	public enum E_WORKLOAD_TYPE { deployment , statefulSet , none }	
	
	@Value("${kubernetes.api.authorization.type}")
	String authorizationType;
	
	@Value("${kubernetes.api.authorization.token}")
	String authorizationToken;
	
	@Value("${kubernetes.api.base.url}")
	protected String baseUrl;

	
	
	@PostConstruct
	public void init()
	{
		if("/".equals(baseUrl.substring(baseUrl.length()-1)))
			baseUrl = baseUrl.substring(0,baseUrl.length()-1);
	}

	public String getAPIGetOneDataString(String url , String jsonPath ) throws Exception 
	{
		APIJsonData apiJsonData = getAPIGetData(url,jsonPath);
		
		if(apiJsonData.IsSuccess())
		{
			if( apiJsonData.getJsonArray().size() == 1)
				return apiJsonData.getJsonArray().get(0).toString();
			else
				return "";
		}
		else
		{
			throw new Exception("데이타를 정상적으로 가져오지 못했습니다.");
		}
	}
	
	public APIJsonData getAPIJsonData(APIJsonData apiJsonDataParam , String jsonPath ) throws Exception 
	{
		APIJsonData apiJsonData = new APIJsonData();
		try
		{
			apiJsonData = JsonUtil.getFilterJson(apiJsonDataParam, jsonPath);
			
			return apiJsonData;
		}
		catch(Exception e)
		{
			throw e;
		}
	}
	
	public APIJsonData getAPIGetData(String url , String jsonPath ) throws Exception 
	{
		APIResponseData apiResponseData;
		try
		{
			Logger.debug("API GET call : " + url);
			System.out.println("API GET call : " + url);
			
			apiResponseData = HttpUtil.getAPIData(url, "GET", getHeader(), "");
			if(apiResponseData.getException() != null)
				throw apiResponseData.getException();
			
			APIJsonData apiJsonData = JsonUtil.getFilterJson(apiResponseData, jsonPath);
			
			return apiJsonData;
		}
		catch(Exception e)
		{
			throw e;
		}
	}
	
	public List<String> getAPIGetListData(String url,String jsonPath) throws Exception
	{
		List<String> retList = new ArrayList<>();
		try {
			
			APIJsonData apiJsonData = getAPIGetData(url,jsonPath);
			if(apiJsonData.getJsonArray() != null)
			{
				for(Object obj : apiJsonData.getJsonArray())
				{
					retList.add(obj.toString());
				}
			}
		} catch (Exception e) {
			return new ArrayList<>();
		}
		return retList;
	}
	
	public APIJsonData getAPIPost(String url , String postData , String jsonPath) throws Exception
	{
		APIResponseData apiResponseData;
		try
		{
			Logger.debug("API POST call : " + url);
			
			apiResponseData = HttpUtil.getAPIData(url, "POST", getHeader(), postData);
			if(apiResponseData.getException() != null)
				throw apiResponseData.getException();
			
			APIJsonData apiJsonData = new APIJsonData(apiResponseData);
			if(!StringUtil.isEmpty(jsonPath))
				apiJsonData = JsonUtil.getFilterJson(apiJsonData, jsonPath);
			
			return apiJsonData;
		}
		catch(Exception e)
		{
			throw e;
		}
	}
	
	public boolean getAPIPut(String url  , String jsonPath) throws Exception
	{
		APIResponseData apiResponseData;
		JSONArray j = new JSONArray();
		try
		{
			Logger.debug("API PUT call : " + url);
			
			apiResponseData = HttpUtil.getAPIData(url, "PUT", getHeader(), "");
			if(apiResponseData.getException() != null)
				throw apiResponseData.getException();
			
			int httpStatus = apiResponseData.getHttpStatus();
			
			if(((int)httpStatus/200) == 1)
				return true;
			else
				return false;
		}
		catch(Exception e)
		{
			throw e;
		}
	}
	
	public boolean getAPIDelete(String url  , String jsonPath) throws Exception
	{
		APIResponseData apiResponseData;
		JSONArray j = new JSONArray();
		try
		{
			Logger.debug("API DELETE call : " + url);
			
			apiResponseData = HttpUtil.getAPIData(url, "DELETE", getHeader(), "");
			if(apiResponseData.getException() != null)
				throw apiResponseData.getException();
			
			int httpStatus = apiResponseData.getHttpStatus();
			
			if(((int)httpStatus/200) == 1)
				return true;
			else
				return false;
		}
		catch(Exception e)
		{
			throw e;
		}
	}
	
	public Map<String , Object> getHeader() 
	{
		Map<String , Object> mapHeader = new HashMap<>();
		mapHeader.put("Authorization", String.format("%s %s", authorizationType,authorizationToken));
		mapHeader.put("Content-Type", "application/json");
		mapHeader.put("Accept", "application/json");
		
		return mapHeader;
	}
	
	protected String base64Encode(String str)
	{
		return Base64.getEncoder().encodeToString(str.getBytes());
	}
	
	protected String base64Decode(String str)
	{
		byte[] decodedBytes = Base64.getDecoder().decode(str);
		return new String(decodedBytes);
	}

}
