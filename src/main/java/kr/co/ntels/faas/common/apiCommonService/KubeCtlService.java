/**
 * 
 */
package kr.co.ntels.faas.common.apiCommonService;

import java.io.File;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import kr.co.ntels.faas.common.apiCommonService.baseService.KCLIServiceBase;
import kr.co.ntels.faas.common.apiCommonService.model.KCLIData;
import kr.co.ntels.faas.common.apiCommonService.model.KCLIData.E_FILE_TYPE;

/**
  * @FileName : KubeCtlService.java
  * @Project : FaasSimpleAPI
  * @Date : 2020. 1. 15. 
  * @작성자 : sig61 - 백광욱
  * @변경이력 :
  * @프로그램 설명 :
  */

@Service
public class KubeCtlService extends KCLIServiceBase{
	
	@Value("${kubectl.path}")
	String kubectlPath;
	
	
	public KCLIData exec(String cmd) throws Exception
	{
		KCLIData data = new KCLIData();
		if(cmd.indexOf("kubectl") == 0)
		{
			data.cmd = cmd.replace("kubectl",getKubectlPath());
			data = execShell(data);
		}
		else
		{
			data.msg = "kubectl 로 시작하는 명령이 아닙니다.";
		}

		return data;
	}
	
	public KCLIData get(String cmd , String jsonPath) throws Exception
	{
		KCLIData data = new KCLIData();
		if("".equals(jsonPath))
		{
			cmd = String.format("%s get %s -o jsonpath='{%s}'", getKubectlPath() , cmd , jsonPath);
		}
		else
		{
			cmd = String.format("%s get %s -o json", getKubectlPath() , cmd );
		}
		data.cmd = cmd;
		
		return execShell(data);
	}
	
	public KCLIData apply_f(String content , E_FILE_TYPE type) throws Exception
	{
		KCLIData data = getKCLIData(content,type);
		String cmd = String.format("%s apply -f %s", getKubectlPath() , data.fname);
		data.cmd = cmd;

		data = execShell(data);
		
		return data;
	}
	
	public KCLIData apply_k(String path) throws Exception
	{
		KCLIData data = new KCLIData();
		data.cmd = String.format("%s apply -k %s", getKubectlPath() , path);
		data = execShell(data);
		return data;
	}
	
	public KCLIData delete_f(String content , E_FILE_TYPE type) throws Exception 
	{
		KCLIData data = getKCLIData(content,type);
		
		try {
			String cmd = String.format("%s delete -f %s", getKubectlPath() , data.fname);
			data.cmd = cmd;
			
			data = execShell(data);
		}catch (Exception e) {
			
		}
		
		return data;
	}
	
	
	private String getKubectlPath()
	{
		String path = kubectlPath + File.separator + "kubectl";
		path = path.replace(File.separator+File.separator, File.separator);
		return path;
	}

}
