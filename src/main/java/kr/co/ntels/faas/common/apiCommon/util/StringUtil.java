/**
 * @Class Name  : StringUtil.java
 * @Description : 문자열 데이터 처리 관련 유틸리티
 * @Modification Information
 *
 *     수정일         	수정자                   수정내용
 *     -------  --------   ---------------------------
 *   2018.01.31 twkim      최초 생성
 *
 * @author twkim
 * @since 2018.01.31
 * @version 1.0
 * @see
 *
 */
package kr.co.ntels.faas.common.apiCommon.util;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class StringUtil {

	private static final Logger LOGGER = LoggerFactory.getLogger(StringUtil.class);

	/**
	 * 빈 문자열 <code>""</code>.
	 */
	public static final String EMPTY = "";



	/**
	 * <p>
	 * String이 비었거나("") 혹은 null 인지 검증한다.
	 * </p>
	 *
	 * <pre>
	 *  StringUtil.isEmpty(null)      = true
	 *  StringUtil.isEmpty("")        = true
	 *  StringUtil.isEmpty(" ")       = false
	 *  StringUtil.isEmpty("bob")     = false
	 *  StringUtil.isEmpty("  bob  ") = false
	 * </pre>
	 *
	 * @param str - 체크 대상 스트링오브젝트이며 null을 허용함
	 * @return <code>true</code> - 입력받은 String 이 빈 문자열 또는 null인 경우
	 */
	public static boolean isEmpty(String str) {
		return str == null || str.length() == 0;
	}

	
}
