/**
 * 
 */
package kr.co.ntels.faas.common.apiCommonService;

import java.io.File;
import java.util.Iterator;
import java.util.Map;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import kr.co.ntels.faas.common.apiCommonService.baseService.KCLIServiceBase;
import kr.co.ntels.faas.common.apiCommonService.model.KCLIData;
import kr.co.ntels.faas.common.apiCommonService.model.KCLIData.E_FILE_TYPE;

/**
  * @FileName : KamelService.java
  * @Project : FaasKubeAPI
  * @Date : 2020. 1. 30. 
  * @작성자 : sig61 - 백광욱
  * @변경이력 :
  * @프로그램 설명 :
  */
@Service
public class KamelService extends KCLIServiceBase {
	
	@Value("${kubectl.path}")
	String kamelPath;
	
	
	public KCLIData exec(String cmd) throws Exception
	{
		KCLIData data = new KCLIData();
		if(cmd.indexOf("kamel") == 0)
		{
			data.cmd = cmd.replace("kamel",getKamelPath());
			data = execShell(data);
		}
		else
		{
			data.msg = "kamel 로 시작하는 명령이 아닙니다.";
		}

		return data;
	}
	
	public KCLIData run(String namespace, String name, String content, E_FILE_TYPE type) throws Exception
	{
		return run(namespace, name, content, type, "", "");
	}
	
	public KCLIData run(String namespace, String name,String content, E_FILE_TYPE type, Map<String,String> dependencies, Map<String,String> traits) throws Exception
	{
		return run(namespace, name, content, type, makeDependency(dependencies), makeTrait(traits));
	}
	
	private KCLIData run(String namespace, String name, String content, E_FILE_TYPE type, String dependencies, String traits) throws Exception
	{
		KCLIData data = getKCLIData(content, type);
		String cmd = String.format("%s run %s -n %s --name %s %s %s", getKamelPath(), data.fname, namespace, name, dependencies, traits);
		data.cmd = cmd;
		
		data = execShell(data);
		
		return data;
	}
	
	private String makeTrait(Map<String, String> traits)
	{
		StringBuffer ret = new StringBuffer();
		Iterator<String> iterator = traits.keySet().iterator();
		
		while(iterator.hasNext())
		{
			String key = iterator.next();
			String val = traits.get(key);
			String str = String.format("-t %s=%s", key, val);
			ret.append(str);
			ret.append(" ");
		}
		
		return ret.toString();
	}
	
	private String makeDependency(Map<String,String> dependencies)
	{
		StringBuffer ret = new StringBuffer();
		Iterator<String> iterator = dependencies.keySet().iterator();
		
		while(iterator.hasNext())
		{
			String item = iterator.next();
			String version = dependencies.get(item);
			String str = String.format("-d %s:%s", item, version);
			ret.append(str);
			ret.append(" ");
		}
		
		return ret.toString();
	}
	
	public KCLIData delete(String name , String namespace) throws Exception
	{
		KCLIData data = new KCLIData();
		String cmd = String.format("%s delete -n %s %s", getKamelPath() ,namespace ,name);
		data.cmd = cmd;
		
		data = execShell(data);
		
		return data;
	}
	
	private String getKamelPath()
	{
		String path = kamelPath + File.separator + "kamel";
		path = path.replace(File.separator+File.separator, File.separator);
		return path;
	}

}
