package kr.co.ntels.faas.common.apiCommon.util.polling.queue;

import java.util.concurrent.BlockingQueue;
import java.util.concurrent.LinkedBlockingQueue;

import kr.co.ntels.faas.common.apiCommon.model.QueueData;



public class PollingQueue {
	
	BlockingQueue<QueueData> queue = new LinkedBlockingQueue<>();
	IQueuePop queuePop = null;
	IQueuePush queuePush = null;
	
	public PollingQueue()
	{
		
	}
	
	public void setQueueProcess(IQueuePush push, IQueuePop pop )
	{
		queuePush = push;
		queuePop = pop;
	}
	
	public void push(QueueData data) throws Exception
	{
		if(this.queuePush != null)
		{
			this.queuePush.pushProcess(queue, data);
		}
		else
			queue.put(data);
	}
	
	public QueueData pop() throws Exception
	{
		if(this.queuePop != null)
		{
			return this.queuePop.popProcess(queue);
		}
		else
			return queue.poll();
	}

}
