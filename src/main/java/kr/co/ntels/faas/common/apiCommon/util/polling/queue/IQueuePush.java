package kr.co.ntels.faas.common.apiCommon.util.polling.queue;

import java.util.concurrent.BlockingQueue;

import kr.co.ntels.faas.common.apiCommon.model.QueueData;

public interface IQueuePush {
	
	public void pushProcess(BlockingQueue<QueueData> queue,QueueData data) throws Exception;

}
