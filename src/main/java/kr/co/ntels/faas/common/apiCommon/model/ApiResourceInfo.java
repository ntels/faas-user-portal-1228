/**
 * 
 */
package kr.co.ntels.faas.common.apiCommon.model;

/**
  * @FileName : ApiResourceInfo.java
  * @Project : kubernetes_dashboards
  * @Date : 2019. 6. 20. 
  * @작성자 : sig61 - 백광욱
  * @변경이력 :
  * @프로그램 설명 :
  */
public class ApiResourceInfo {
	
	int statusCode;
	String contentType;
	byte[] content;
	
	
	public int getStatusCode() {
		return statusCode;
	}
	public void setStatusCode(int statusCode) {
		this.statusCode = statusCode;
	}
	public String getContentType() {
		return contentType;
	}
	public void setContentType(String contentType) {
		this.contentType = contentType;
	}
	public byte[] getContent() {
		return content;
	}
	public void setContent(byte[] content) {
		this.content = content;
	}

}
