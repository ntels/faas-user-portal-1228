package kr.co.ntels.faas.common.utils;

public class CommCode {
    public class RegStep {
        public static final String NONE = "NONE";
        public static final String STEP1 = "STEP1";
        public static final String STEP2 = "STEP2";
        public static final String STEP3 = "STEP3";
        public static final String STEP4 = "STEP4";
        public static final String STEP5 = "STEP5";
    }

    public class Stat {
        public static final String ABNORMAL = "ABNORMAL";
        public static final String ERROR = "ERROR";
        public static final String INIT = "INIT";
        public static final String NORMAL = "NORMAL";
        public static final String REG = "REG";
        public static final String SYNC_ERROR = "SYNC_ERROR";
    }

    public class TriggerStatCd {
        public static final String ERROR = "ERROR";
        /**
         * DB에만 저장이 되어있는 상태. (template)
         */
        public static final String INIT = "INIT";
        public static final String NORMAL = "NORMAL";
        public static final String REG = "REG";
    }

    public class DBType {
        public static final String POSTGRESQL = "POSTGRESQL";
        public static final String MONGODB = "MONGODB";
    }

}
