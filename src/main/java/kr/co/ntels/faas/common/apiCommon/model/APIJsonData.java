package kr.co.ntels.faas.common.apiCommon.model;

import java.util.Map;

import com.jayway.jsonpath.JsonPath;

import net.minidev.json.JSONArray;

/**
  * @FileName : APIJsonData.java
  * @Project : api-portal
  * @Date : 2018. 12. 7. 
  * @작성자 : sig61 - 백광욱
  * @변경이력 :
  * @프로그램 설명 :
  */
public class APIJsonData {
	
	int httpStatus = 0;
	boolean _isSuccess = false;
	String jsonString = "{}";
	JSONArray jsonArray = new JSONArray();
	APIError apiError = new APIError();
	
	public APIJsonData() {}
	
	public static boolean isEmpty(String str) {
		return str == null || str.length() == 0;
	}
	
	public APIJsonData(APIResponseData apiResponseData) 
	{
		setHttpStatus(apiResponseData.getHttpStatus());
		setJsonString(apiResponseData.getJsonString());
		if(!isEmpty(jsonString))
		{
			Map<String,Object> dataMap = JsonPath.read(jsonString,"$");
			jsonArray = new JSONArray();
			jsonArray.add(dataMap);
		}
		
		apiError = apiResponseData.getApiError();
	}
	
	public boolean IsSuccess()
	{
		return _isSuccess;
	}
	
	public int getHttpStatus() {
		return httpStatus;
	}

	public void setHttpStatus(int httpStatus) {
		this.httpStatus = httpStatus;
		if(1 == ((int)httpStatus/200) )
			_isSuccess = true;
	}

	public String getJsonString() {
		return jsonString;
	}

	
	public void setJsonString(String jsonString) {
		this.jsonString = jsonString;
	}

	public JSONArray getJsonArray() {
		return jsonArray;
	}

	
	public void setJsonArray(JSONArray jsonArray) {
		this.jsonArray = jsonArray;
	}
	
	public APIError getApiError() {
		return apiError;
	}

	public void setApiError(APIError apiError) {
		this.apiError = apiError;
	}
	
	public String toString() {
		if(jsonArray.size() == 0 ) return "";
		else if(jsonArray.size() == 1 ) return jsonArray.get(0)==null ? "" : jsonArray.get(0).toString();
		else {
			return jsonString;
		}
	}
	
}
