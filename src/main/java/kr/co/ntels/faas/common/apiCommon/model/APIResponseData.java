package kr.co.ntels.faas.common.apiCommon.model;

import java.util.Map;

import com.fasterxml.jackson.databind.ObjectMapper;

/**
  * @FileName : APIResponseData.java
  * @Project : api-portal
  * @Date : 2018. 12. 7. 
  * @작성자 : sig61 - 백광욱
  * @변경이력 :
  * @프로그램 설명 :
  */
public class APIResponseData {
	
	int httpStatus = 0;
	
	String jsonString="{}";
	
	APIError apiError = new APIError();
	
	Exception exception;
	
	/**
	 * @param httpStatus
	 * @param jsonString
	 */
	public APIResponseData(int httpStatus , String jsonString) 
	{
		this.httpStatus = httpStatus;
		if(((int)httpStatus/200) == 1)
		{
			this.jsonString = jsonString;
		}
		else
		{
			try {
				Map<String,Object> map  = new ObjectMapper().readValue(jsonString, Map.class) ;
				
				apiError = new APIError();
				if(map.containsKey("code")) apiError.setCode(map.get("code").toString());
				if(map.containsKey("message")) apiError.setMessage(map.get("message").toString());
				apiError.setStatus(httpStatus);
				
			} catch (Exception e) {
				exception = e;
			}
		}
	}
	
	public APIResponseData(Exception e)
	{
		this.exception = e;
	}

	public int getHttpStatus() {
		return httpStatus;
	}

	public void setHttpStatus(int httpStatus) {
		this.httpStatus = httpStatus;
	}

	public String getJsonString() {
		return jsonString;
	}

	public void setJsonString(String jsonString) {
		this.jsonString = jsonString;
	}

	public APIError getApiError() {
		return apiError;
	}

	public void setApiError(APIError apiError) {
		this.apiError = apiError;
	}

	public Exception getException() {
		return exception;
	}

	public void setException(Exception exception) {
		this.exception = exception;
	}

	public String toString()
	{
		if(exception == null)
		{
			if(httpStatus == 200)
				return jsonString;
			else
				return apiError.getMessage();
		}
		else
			return exception.getMessage();
	}

}
