package kr.co.ntels.faas.common.utils;

import org.apache.commons.dbcp.BasicDataSource;

import javax.sql.DataSource;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

public class DbConnectHelper {
    private String host;
    private String port;
    private String db;
    private String user;
    private String password;

    private List<String> queries;

    public DbConnectHelper(String host, String port, String db, String user, String password){
        this.host = host;
        this.port = port;
        this.db = db;
        this.user = user;
        this.password = password;

        this.queries = new ArrayList<String>();
    }

    public void executePostgresql() throws SQLException {
        DataSource ds = getDataSourcePostgresql(host, port, db, user, password);

        Connection con = null;
        Statement stmt = null;
        ResultSet rs = null;

        try {
            con = ds.getConnection();
            stmt = con.createStatement();

            for(String query : queries){
                stmt.execute(query);
            }

        } catch (SQLException e) {
            e.printStackTrace();
            throw e;
        } finally{
            try {
                if(rs != null) rs.close();
                if(stmt != null) stmt.close();
                if(con != null) con.close();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
    }

    public void addQuery(String query) {
        queries.add(query);
    }

    public static DataSource getDataSourcePostgresql(String host, String port, String dbname, String username, String password){
        BasicDataSource ds = new BasicDataSource();

        ds.setDriverClassName("org.postgresql.Driver");
        ds.setUrl(String.format("jdbc:postgresql://%s:%s/%s", host, port, dbname));
        ds.setUsername(username);
        ds.setPassword(password);

        return ds;
    }

    public String getHost() {
        return host;
    }

    public String getPort() {
        return port;
    }

    public String getDb() {
        return db;
    }

    public String getUser() {
        return user;
    }

    public String getPassword() {
        return password;
    }

    public List<String> getQueries() {
        return queries;
    }

    public void setQueries(List<String> queries) {
        this.queries = queries;
    }
}
