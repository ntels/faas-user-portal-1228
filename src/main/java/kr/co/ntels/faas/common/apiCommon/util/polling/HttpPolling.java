package kr.co.ntels.faas.common.apiCommon.util.polling;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.security.cert.X509Certificate;
import java.time.LocalDateTime;
import java.util.Map;

import javax.net.ssl.HttpsURLConnection;
import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLException;
import javax.net.ssl.TrustManager;
import javax.net.ssl.X509TrustManager;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import kr.co.ntels.faas.common.apiCommon.model.QueueData;
import kr.co.ntels.faas.common.apiCommon.util.HttpUtil;
import kr.co.ntels.faas.common.apiCommon.util.polling.queue.PollingQueue;


/**
  * @FileName : HttpPolling.java
  * @Project : faas-user-portal
  * @Date : 2020. 4. 20. 
  * @작성자 : sig61 - 백광욱
  * @변경이력 :
  * @프로그램 설명 :
  */
public class HttpPolling {
	
	@SuppressWarnings("serial")
	public static class APIKeyException extends Exception{
		public APIKeyException()
		{
			super("서버 설정에 문제가 있습니다. 관리자에게 문의하세요. (API 접근권한이 없습니다.)");
		}
	}
	
	private static final Logger Logger = LoggerFactory.getLogger(HttpPolling.class );
	private static final Log Log = LogFactory.getLog(HttpPolling.class );
	
	protected String urlString;
	protected String method;
	protected String postData;
	protected int pollingTimeSec = 3;
	protected Map<String,Object> mapHeader;
	protected PollingQueue queue = null;
	boolean _stopPolling = false;
	Thread _thread = null;
	
	public HttpPolling(String urlString, String method, String postData,Map<String,Object> mapHeader, PollingQueue queue, int pollingTimeSec)
	{
		this.urlString = urlString;
		this.method = method;
		this.postData = postData;
		this.mapHeader = mapHeader;
		this.queue = queue;
		this.pollingTimeSec = pollingTimeSec;
		
		startThread();
	}
	
	public void stopPolling()
	{
		_stopPolling = true;
	}
	
	private void startThread()
	{
		_thread = new Thread(()->{
		      
		    while(true)
		    {
		    	if(_stopPolling)
			    {
			    	System.out.println("========================================== HttpPolling thread stop ====================");
			    	break;
			    }
		    	
			    try 
			    {
			    	polling();
				} catch (Exception e) {
					Log.error("HttpPolling : 데이타 조회중 문제가 있습니다." + e.getMessage());
				}
			    
			    try 
			    {
			    	Thread.sleep(pollingTimeSec * 1000);
				} catch (Exception e) {
				}
			    
		    }
		});
		
		_thread.setName("Thread : HttpPolling");
		_thread.start();
	}
	
	public void push(QueueData data) throws Exception
	{
		queue.push(data);
	}
	
	private void polling() throws Exception
	{
		
		HttpURLConnection conn = null;
		BufferedReader reader = null;
		InputStream is = null;
		StringBuffer sb = new StringBuffer();
		
		try {
			
			URL url = new URL(urlString);
			conn = (HttpURLConnection) url.openConnection();
			conn.setRequestMethod(method);

			//헤더 정보 셋팅
			if(mapHeader != null)
			{
				for(String key : mapHeader.keySet())
				{
					conn.setRequestProperty(key, mapHeader.get(key).toString());
				}
			}
			
			//post 데이타 전송 정의
			if(postData != null && !"".equals(postData))
			{
				byte[] postDataBytes = postData.getBytes("utf8");
				conn.setDoOutput(true);
		        conn.getOutputStream().write(postDataBytes);
			}
			
			int statusCode = conn.getResponseCode();
			is = ((int)statusCode/200) == 1 ? conn.getInputStream() : conn.getErrorStream(); //input스트림 개방
			if(statusCode == 401)
				throw new APIKeyException();
				

			if(is != null)
			{
				reader = new BufferedReader(new InputStreamReader(is,"UTF-8")); //문자열 셋 세팅
				String line = "";
				while ((line = reader.readLine()) != null) {
					sb.append(line);
				}
				
				QueueData data = new QueueData();
				data.setSourceObj(this);
				data.setTime(LocalDateTime.now());
				data.setMessage(sb.toString());
				try
				{
					push(data);
				}
				catch(Exception ex)
				{
					Log.error(ex.getMessage(),ex);
				}
			}
		} catch (SSLException e) {
			try
			{
				pollingSSL();
			}
			catch(Exception ee)
			{
				Log.error(ee.getMessage(),ee);
				throw ee;
			}
		}
		finally
		{
			if(conn != null) conn.disconnect();
			if(is != null) is.close();
			if(reader != null) reader.close();
		}
	}
	
	private void pollingSSL() throws Exception
	{
		HttpURLConnection conn = null;
		BufferedReader reader = null;
		InputStream is = null;
		StringBuffer sb = new StringBuffer();
		
		try {
			
			if(urlString.startsWith("https://"))
			{
				//ssl 접속에 대해서 인증서 무시하도록 하는 코드
				TrustManager[] trustAllCerts = new TrustManager[] { new X509TrustManager() {
					public java.security.cert.X509Certificate[] getAcceptedIssuers() {
						return null;
					}
	
					public void checkClientTrusted(X509Certificate[] certs,
							String authType) {
					}
	
					public void checkServerTrusted(X509Certificate[] certs,
							String authType) {
					}
				} };
	
				SSLContext sc = SSLContext.getInstance("SSL");
				sc.init(null, trustAllCerts, new java.security.SecureRandom());
				HttpsURLConnection.setDefaultSSLSocketFactory(sc.getSocketFactory());
				//ssl 접속에 대해서 인증서 무시하도록 하는 코드
			}

			URL url = new URL(urlString);
			conn = (HttpURLConnection) url.openConnection();
			conn.setRequestMethod(method);

			//헤더 정보 셋팅
			if(mapHeader != null)
			{
				for(String key : mapHeader.keySet())
				{
					conn.setRequestProperty(key, mapHeader.get(key).toString());
				}
			}
			
			//post 데이타 전송 정의
			if(postData != null && !"".equals(postData))
			{
				byte[] postDataBytes = postData.getBytes("utf8");
				conn.setDoOutput(true);
		        conn.getOutputStream().write(postDataBytes);
			}
			
			int statusCode = conn.getResponseCode();
			is = ((int)statusCode/200) == 1 ? conn.getInputStream() : conn.getErrorStream(); //input스트림 개방
			
			if(statusCode == 401)
				throw new APIKeyException();

			if(is != null)
			{
				reader = new BufferedReader(new InputStreamReader(is,"UTF-8")); //문자열 셋 세팅
				String line = "";
				while ((line = reader.readLine()) != null) {
					sb.append(line);
				}
				
				QueueData data = new QueueData();
				data.setSourceObj(this);
				data.setTime(LocalDateTime.now());
				data.setMessage(sb.toString());
				try
				{
					push(data);
				}
				catch(Exception ex)
				{
					Log.error(ex.getMessage(),ex);
				}
			}

		} catch (Exception e) {
			Log.error(e.getMessage(),e);
			throw e;
		}
		finally
		{
			if(conn != null) conn.disconnect();
			if(is != null) is.close();
			if(reader != null) reader.close();
		}
	}
	
}
