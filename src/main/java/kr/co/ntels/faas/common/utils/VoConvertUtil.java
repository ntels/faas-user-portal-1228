package kr.co.ntels.faas.common.utils;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.DeserializationConfig;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import kr.co.ntels.faas.trigger.vo.DbTriggerVo;

import java.util.Map;

public class VoConvertUtil {
    public static Map<String, Object> convertObjToMap(Object obj){
        ObjectMapper om = new ObjectMapper();
        om.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
        Map<String, Object> map = om.convertValue(obj, new TypeReference<Map<String, Object>>() {});
        return map;
    }
    public static <T> T convertMapToObj(Map<String, Object> map, Class<T> klass) {
        ObjectMapper om = new ObjectMapper();
        om.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
        T obj = om.convertValue(map, klass);

        return obj;
    }

    public static void main(String[] args){
        DbTriggerVo db = new DbTriggerVo();
        db.setFunction_id(1);
        db.setAdmin_id("admin");
        db.setName("nnnnnnnnnname");
        Map<String, Object> map = VoConvertUtil.convertObjToMap(db);
        map.put("lalalal", "nanana");

        System.out.println(map);

        DbTriggerVo obj = VoConvertUtil.convertMapToObj(map, DbTriggerVo.class);

        System.out.println(obj.getAdmin_id());
        System.out.println(obj.getName());
    }
}
