/**
 * 
 */
package kr.co.ntels.faas.common.apiCommon.util;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


/**
  * @FileName : ExecShell.java
  * @Project : FaasSimpleAPI
  * @Date : 2020. 1. 15. 
  * @작성자 : sig61 - 백광욱
  * @변경이력 :
  * @프로그램 설명 :
  */

public class ExecShell {

	private static final Logger Logger = LoggerFactory.getLogger(ExecShell.class );
	String msg = "";
	
	public String getMsg()
	{
		return msg;
	}
	
	public boolean exec(String cmd) throws Exception
	{
		Process process = null;
        Runtime runtime = Runtime.getRuntime();
        StringBuffer successOutput = new StringBuffer();
        StringBuffer errorOutput = new StringBuffer();
        BufferedReader successBufferReader = null;
        BufferedReader errorBufferReader = null;
        String msg = "";
        boolean result = false;
 
        List<String> cmdList = new ArrayList<String>();
        
        if (System.getProperty("os.name").indexOf("Windows") > -1) {
            cmdList.add("cmd");
            cmdList.add("/c");
        } else {
            cmdList.add("/bin/sh");
            cmdList.add("-c");
        }
        
        cmdList.add(cmd);
        String[] array = cmdList.toArray(new String[cmdList.size()]);
 
        try {

            process = runtime.exec(array);
 
            successBufferReader = new BufferedReader(new InputStreamReader(process.getInputStream(), "EUC-KR"));
 
            while ((msg = successBufferReader.readLine()) != null) {
                successOutput.append(msg + System.getProperty("line.separator"));
            }
 
            errorBufferReader = new BufferedReader(new InputStreamReader(process.getErrorStream(), "EUC-KR"));
            while ((msg = errorBufferReader.readLine()) != null) {
                errorOutput.append(msg + System.getProperty("line.separator"));
            }
 
            process.waitFor();
 
            if (process.exitValue() == 0) {
                result = true;
                this.msg = successOutput.toString();
                Logger.debug("success : " + successOutput.toString());
            } else {
            	Logger.error("abnormal finish error : " + errorOutput.toString());
            }
 
            if (!"".equals(errorOutput.toString())) {
            	Logger.error( "error : " + errorOutput.toString());
            	this.msg = errorOutput.toString();
            }
 
        } catch (IOException e) {
            Logger.error(e.getMessage());
            this.msg = e.getMessage();
        } catch (InterruptedException e) {
            Logger.error( e.getMessage());
            this.msg = e.getMessage();
        } finally {
            try {
                process.destroy();
                if (successBufferReader != null) successBufferReader.close();
                if (errorBufferReader != null) errorBufferReader.close();

            } catch (IOException e1) {
                Logger.error( e1.getMessage());
                this.msg = e1.getMessage();
            }
        }
        return result;
	}

}
