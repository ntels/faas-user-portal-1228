package kr.co.ntels.faas.common.apiCommon.model;

import java.time.LocalDateTime;

public class QueueData{
	
	LocalDateTime time;
	String message;
	String type;
	Object sourceObj;
	
	public QueueData()
	{
		this.time = LocalDateTime.now();
	}
	
	public LocalDateTime getTime() {
		return time;
	}
	public void setTime(LocalDateTime time) {
		this.time = time;
	}
	public String getMessage() {
		return message;
	}
	public void setMessage(String message) {
		this.message = message;
	}
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}
	public Object getSourceObj() {
		return sourceObj;
	}
	public void setSourceObj(Object sourceObj) {
		this.sourceObj = sourceObj;
	}		
}