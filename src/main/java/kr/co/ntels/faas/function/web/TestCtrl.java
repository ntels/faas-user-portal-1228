package kr.co.ntels.faas.function.web;

import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.nio.file.Paths;
import java.security.KeyManagementException;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.cert.X509Certificate;
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.PostConstruct;
import javax.net.ssl.SSLContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.http.conn.ssl.SSLConnectionSocketFactory;
import org.apache.http.conn.ssl.SSLContexts;
import org.apache.http.conn.ssl.TrustStrategy;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.http.client.HttpComponentsClientHttpRequestFactory;
import org.springframework.stereotype.Controller;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.multipart.MultipartHttpServletRequest;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.github.jknack.handlebars.Handlebars;
import com.github.jknack.handlebars.Template;

import kr.co.ntels.faas.common.apiCommon.model.APIResponseData;
import kr.co.ntels.faas.common.apiCommon.model.ApiResourceInfo;
import kr.co.ntels.faas.common.apiCommon.util.HttpUtil;
import kr.co.ntels.faas.dao.FaasDao;
import kr.co.ntels.faas.function.service.DynamicDBService;
import kr.co.ntels.faas.function.service.FunctionService;
import kr.co.ntels.faas.function.service.KongService;
import kr.co.ntels.faas.gitlab.service.GitLabService;

/**
  * @FileName : TestCtrl.java
  * @Project : faas-user-portal
  * @Date : 2020. 4. 29. 
  * @작성자 : sig61 - 백광욱
  * @변경이력 :
  * @프로그램 설명 :
  */
@Controller
public class TestCtrl {
	
	@Autowired FunctionService functionServiceBase;
	@Autowired DynamicDBService dynamicDBService;
	@Autowired KongService kongService;
	@Autowired GitLabService gitLabService;
	@Autowired FaasDao dao;
	@Value("${kubernetes.api.base.url}") protected String baseUrl;
	
	@PostConstruct
	public void init()
	{
		System.setProperty("Tomcat.util.http.parser.HttpParser.requestTargetAllow", "[]");
	}
	
	@ResponseBody
	@RequestMapping("/api/proxy.do")
	public String proxy(@RequestParam String path) throws Exception
	{
		String ret = "";
		String url = String.format("%s%s", baseUrl ,path);
		Map<String , Object> mapHeader = functionServiceBase.getHeader();
		
		ApiResourceInfo data = HttpUtil.getApiResourceData(url, "GET", mapHeader);
		if(data.getStatusCode()/200 == 1)
		{
			byte by[] = data.getContent();
			ret = new String(by);
		}
		return ret;
		
	}
	
	@ResponseBody
	@RequestMapping("/handlebar.do")
	public String handlebar() throws Exception
	{
		
		Handlebars hb = new Handlebars();

		Template template = hb.compileInline("Hello {{#if d}} {{a}}! {{/if}} {{b.bbb}}  {{c}}");

		Map<String,Object> map = new HashMap<>();
		Map<String,Object> map1 = new HashMap<>();
		map1.put("bbb", "bbbbbbbb"); 
		map.put("a", "aaa");
		map.put("b", map1);
		map.put("c", "ccc");
		System.out.println(template.apply(map));
		
		
		return "OK";
	}
	 
	@ResponseBody
	@RequestMapping("/upload1.do")
	public Object uploadMain(HttpServletRequest request, HttpServletResponse response , @RequestParam String function_id) throws Exception {
		
		String path = Paths.get("D:\\java\\FREE_Project\\nTels_Faas_new\\upload\\extract\\").toString();
		
		List<String> listExtractFile = new ArrayList<>();
		List<Map<String,Object>> list = functionServiceBase.setUpload((MultipartHttpServletRequest)request, function_id);
		
		for(Map<String,Object> map : list)
		{
			String zipfile = map.get("upload_file_path").toString();
			functionServiceBase.unzipFile(zipfile, path , listExtractFile);
			
			
		}
        
		return listExtractFile;
	}
	
	@ResponseBody
	@RequestMapping("/checkout.do")
	public Object checkout(HttpServletRequest request, HttpServletResponse response) throws Exception {

		List<Map<String, Object>> fileList = new ArrayList<>();
		Map<String, Object> map1 = new HashMap<>();
		Map<String, Object> map2 = new HashMap<>();
		Map<String, Object> map3 = new HashMap<>();
		Map<String, Object> map4 = new HashMap<>();
		fileList.add(map1);
		fileList.add(map2);
		fileList.add(map3);
		fileList.add(map4);
		
		
		
		map1.put("fileName", "aaa/aaa.txt");
		map1.put("fileContent", "aaaaaaaaaaaaaaaaaaaaaaaaaaaaa");
		map2.put("fileName", "aaa/bbb.txt");
		map2.put("fileContent", "bbbbbbbbbbbbbbbbbbbbbbbbbbbbb");
		map3.put("fileName", "index.js");
		map3.put("fileContent", "module.exports = function(app)\r\n" + 
				"{\r\n" + 
				"  app.post(\"/\", function(req, res) {\r\n" + 
				"    console.log(req.event_type);\r\n" + 
				"    console.log(req.event_data);\r\n" + 
				"\r\n" + 
				"    res.status(200).json(req.event_data);\r\n" + 
				"  });\r\n" + 
				"}\r\n" + 
				"");
		map4.put("fileName", "package.json");
		map4.put("fileContent", "{\r\n" + 
				"  \"name\": \"knative-serving\",\r\n" + 
				"  \"version\": \"1.0.0\",\r\n" + 
				"  \"description\": \"knative serving application\",\r\n" + 
				"  \"main\": \"app.js\",\r\n" + 
				"  \"scripts\": {\r\n" + 
				"    \"start\": \"node app.js\"\r\n" + 
				"  },\r\n" + 
				"  \"license\": \"Apache-2.0\",\r\n" + 
				"  \"dependencies\": {\r\n" + 
				"    \"express\": \"4.16.0\",\r\n" + 
				"    \"body-parser\": \"1.19.0\",\r\n" + 
				"    \"cors\": \"2.8.5\"\r\n" + 
				"  }\r\n" + 
				"}\r\n" + 
				"");
		
		gitLabService.commitToProject(fileList, "faas-p310/xml-testnu0cxyn6e1", "root", "gitlab#1$");
		
		List<String> list = gitLabService.getFilePathList("faas-p310/xml-testnu0cxyn6e1","");
		
		return list;
	}
	
	@ResponseBody
	@RequestMapping("/api/grafana.do")
	public String grafana(@RequestParam String path) throws Exception
	{
		String sss = "api/v1/namespaces/cattle-prometheus/services/http:access-grafana:80/proxy/d/2V2BA2vMz/deployment111?orgId=1&from=now-5m&to=now&refresh=5s";
		String ret = "";
		String bbb = "https://caas.kepri-demo.crossent.com:8000/k8s/clusters/c-m7h2z/";
		String url = String.format("%s%s", bbb ,sss);
		Map<String , Object> mapHeader = functionServiceBase.getHeader();
		
		APIResponseData data = HttpUtil.getAPIData(url, "GET", mapHeader , "");
		
		return data.getJsonString();
		
	}
	
	
	//@GetMapping("/k8s/**/d/**")
    public ResponseEntity<String> proxy(HttpServletRequest request, HttpServletResponse response, @RequestBody(required = false) byte[] body) throws IOException, URISyntaxException, KeyManagementException, KeyStoreException, NoSuchAlgorithmException 
	{
     
     HttpComponentsClientHttpRequestFactory httpRequestFactory = new HttpComponentsClientHttpRequestFactory();
        httpRequestFactory.setConnectionRequestTimeout(360000); //6m
        httpRequestFactory.setConnectTimeout(360000); //6m
        httpRequestFactory.setReadTimeout(360000); //6m
     
     // restTempate tobe bean
     RestTemplate restTemplate = restTemplate();//new RestTemplate(httpRequestFactory);
     
     // url
     //String originReqURL = request.getRequestURI().replaceAll("^/faas/proxy/", "");
     String originReqURL = request.getRequestURI().replaceAll("^/faas/", "");
     String originQueryString = request.getQueryString();
     String urlStr = "https://caas.kepri-demo.crossent.com:8000/" + originReqURL + (StringUtils.isEmpty(originQueryString) ? "" : "?"+originQueryString);
     
     URI url = new URI(urlStr);
     
     // method
     String originMethod = request.getHeader("x-origin-method");
     HttpMethod method = HttpMethod.valueOf("GET");//originMethod.toUpperCase());
     
     
     // header
     Enumeration<String> headerNames = request.getHeaderNames();
     MultiValueMap<String, String> headers = new LinkedMultiValueMap<>();
     while(headerNames.hasMoreElements()) {
      String headerName = headerNames.nextElement();
      String headerValue = request.getHeader(headerName);
   
      headers.add(headerName, headerValue);
     }
     
     headers.add("Authorization", "Bearer token-vbj7g:ms5ccr5nnpqzwdqmjdfx24m97fjmsg75pqnsl484c6dcfs88gg8x88");
     
     
     
     // http entity (body, header)
     HttpEntity<byte[]> httpEntity = new HttpEntity<>(body, headers);
     
     ResponseEntity<String> aaa = restTemplate.exchange(url, method, httpEntity, String.class);
     String result = (String)aaa.getBody();
     
     String res = result.replaceAll("/k8s/", "/faas/k8s/");
     HttpHeaders orgHeaders = aaa.getHeaders();
     HttpHeaders tarHeaders = new HttpHeaders();
     if(orgHeaders.containsKey("Content-Type")) 
     { 
    	 List<String> list = (List<String>)orgHeaders.get("Content-Type");
    	 tarHeaders.addAll("Content-Type", list);
     }
     
     return ResponseEntity.ok().headers(tarHeaders).body(res);
    }
    
    public RestTemplate restTemplate()    throws KeyStoreException, NoSuchAlgorithmException, KeyManagementException {
		TrustStrategy acceptingTrustStrategy = (X509Certificate[] chain, String authType) -> true;
		
		SSLContext sslContext = SSLContexts.custom()
		                .loadTrustMaterial(null, acceptingTrustStrategy)
		                .build();
		
		SSLConnectionSocketFactory csf = new SSLConnectionSocketFactory(sslContext);
		
		CloseableHttpClient httpClient = HttpClients.custom()
		                .setSSLSocketFactory(csf)
		                .build();
		
		HttpComponentsClientHttpRequestFactory requestFactory =
		                new HttpComponentsClientHttpRequestFactory();
		
		requestFactory.setHttpClient(httpClient);
		RestTemplate restTemplate = new RestTemplate(requestFactory);
		return restTemplate;
	}
	
    
    //@GetMapping("/k8s/**")
    public ResponseEntity<byte[]> proxy111(HttpServletRequest request, HttpServletResponse response, @RequestBody(required = false) byte[] body) throws IOException, URISyntaxException, KeyManagementException, KeyStoreException, NoSuchAlgorithmException 
	{
     
     HttpComponentsClientHttpRequestFactory httpRequestFactory = new HttpComponentsClientHttpRequestFactory();
        httpRequestFactory.setConnectionRequestTimeout(360000); //6m
        httpRequestFactory.setConnectTimeout(360000); //6m
        httpRequestFactory.setReadTimeout(360000); //6m
     
     // restTempate tobe bean
     RestTemplate restTemplate = restTemplate();//new RestTemplate(httpRequestFactory);
     
     // url
     String originReqURL = request.getRequestURI().replaceAll("^/faas/k8s/", "");
	 String originQueryString = request.getQueryString();
	 String urlStr = "https://caas.kepri-demo.crossent.com:8000/k8s/" + originReqURL + (StringUtils.isEmpty(originQueryString) ? "" : "?"+originQueryString);
     
     URI url = new URI(urlStr);
     
     // method
     String originMethod = request.getHeader("x-origin-method");
     HttpMethod method = HttpMethod.valueOf("GET");//originMethod.toUpperCase());
     
     
     // header
     Enumeration<String> headerNames = request.getHeaderNames();
     MultiValueMap<String, String> headers = new LinkedMultiValueMap<>();
     while(headerNames.hasMoreElements()) {
      String headerName = headerNames.nextElement();
      String headerValue = request.getHeader(headerName);
   
      headers.add(headerName, headerValue);
     }
     
     headers.add("Authorization", "Bearer token-vbj7g:ms5ccr5nnpqzwdqmjdfx24m97fjmsg75pqnsl484c6dcfs88gg8x88");
     
     
     
     // http entity (body, header)
     HttpEntity<byte[]> httpEntity = new HttpEntity<>(body, headers);
     
     ResponseEntity<byte[]> aaa = restTemplate.exchange(url, method, httpEntity, byte[].class);
     String result = aaa.getBody().toString();
     HttpHeaders orgHeaders = aaa.getHeaders();
     HttpHeaders tarHeaders = new HttpHeaders();
     if(orgHeaders.containsKey("Content-Type")) 
     { 
    	 List<String> list = (List<String>)orgHeaders.get("Content-Type");
    	 tarHeaders.addAll("Content-Type", list);
     }
     
     //  return ResponseEntity.ok().body(result);
     
     return ResponseEntity.ok().headers(tarHeaders).body(aaa.getBody());
    }
    
    //@ResponseBody
	//@RequestMapping("test/{path}.do")
	public Object delete(HttpServletRequest request, HttpServletResponse response , @PathVariable String path ) throws Exception 
    {
    
    	String url = String.format("https://caas.kepri-demo.crossent.com/k8s/clusters/c-m7h2z/apis/camel.apache.org/v1/namespaces/faas-p310/integrations/%s",path);
    	return functionServiceBase.getAPIDelete(url, "");
    	
    	
    }
    
	// test gitlab api
	// http://local.kepri2-demo.crossent.com:8085/faas/test/gitlab.do?group=test-gitlab&project=test-gitlab&user=test-gitlab
	@ResponseBody
	@RequestMapping("test/gitlab.do")
	public Object testGitLabGroupApi(HttpServletRequest request, HttpServletResponse response, @RequestParam String group, @RequestParam String project, @RequestParam String user) throws Exception 
    {
		String groupName = String.valueOf(group);
		String projectName = String.valueOf(project);
		String userName = String.valueOf(user);
		
		boolean returnBoolean = gitLabService.createGroup(groupName);
		
    	return returnBoolean;
    }
}
