package kr.co.ntels.faas.function.service;

import java.net.URL;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.github.jknack.handlebars.Handlebars;
import com.github.jknack.handlebars.Template;
import com.google.common.io.Resources;

import kr.co.ntels.faas.common.apiCommon.model.APIJsonData;
import kr.co.ntels.faas.common.apiCommon.model.Constant;
import kr.co.ntels.faas.common.apiCommon.model.Constant.E_ORDERBY;
import kr.co.ntels.faas.common.apiCommon.util.JsonUtil;
import kr.co.ntels.faas.common.apiCommon.util.StringUtil;
import kr.co.ntels.faas.common.apiCommonService.KamelService;
import kr.co.ntels.faas.common.apiCommonService.KubeAPIService;
import kr.co.ntels.faas.common.apiCommonService.KubeCtlService;
import kr.co.ntels.faas.common.apiCommonService.model.KCLIData;
import kr.co.ntels.faas.common.apiCommonService.model.KCLIData.E_FILE_TYPE;
import kr.co.ntels.faas.function.model.api.FunctionInfo;
import kr.co.ntels.faas.function.model.api.trigger.TriggerInfo;
import kr.co.ntels.faas.function.model.api.trigger.TriggerInfoFactory;
import kr.co.ntels.faas.userException.ExceptionCreateService;
import kr.co.ntels.faas.userException.ExceptionDeleteService;
import kr.co.ntels.faas.userException.ExceptionEx;

/**
  * @FileName : FunctionServiceBase.java
  * @Project : faas-user-portal
  * @Date : 2020. 4. 28. 
  * @작성자 : sig61 - 백광욱
  * @변경이력 :
  * @프로그램 설명 :
  */
@Service
public class FunctionServiceBase extends KubeAPIService {
	
	private Logger logger = LoggerFactory.getLogger(this.getClass());
	
	@Value("${kamel.install.option}") String kamelOption;
	@Value("${kamel.install.maven.option}") String kamelMavenOption;
	
	@Value("${docker.registry}")
	String dockerRegistry;
	
	@Value("${docker.registry.secret}")
	String dockerRegistrySecret;
	
	@Value("${docker.username}")
	String dockerUsr;
	
	@Value("${docker.password}")
	String dockerPWD;
	
	@Value("${maven.private.repository.url}") String MAVEN_PRIVATE_REPOSITORY_URL;
	@Value("${maven.private.repository.isused}") String MAVEN_PRIVATE_REPOSITORY_ISUSED;

	@Autowired
	KubeCtlService kubeCtlService;
	
	@Autowired
	KamelService kamelService;
	
	@Autowired
	TriggerInfoFactory triggerInfoFactory;
	
	//namespace 등록 -- 등록된 네임스페이스가 없으면 프로젝트등록하고 네임스페이스 등록한다.
	public String getNamespace(String name) throws Exception
	{
		String rancherBaseUrl = getRancherBaseUrl();
		String clusterId = getClusterId();
		String url = String.format("%s/clusters/%s/namespaces/%s", rancherBaseUrl,clusterId,name);
		APIJsonData apiJsonData = getAPIGetData(url,"");
		
		try
		{
			if(apiJsonData.IsSuccess())
			{
				APIJsonData dataId = JsonUtil.getFilterJson(apiJsonData, "$..id");
				APIJsonData dataProjectId = JsonUtil.getFilterJson(apiJsonData, "$..projectId");
				String id = dataId.toString();
				String projectId = dataProjectId.toString();
				boolean projectExist = false;
				
				if(!"".equals(projectId))
				{
					if(name.equals(getProjectName(projectId)))
						projectExist = true;
						
				}
				
				if(projectExist == false)
				{
					projectId =  createProject(name);
					
					if(!moveProject(name,projectId))
						throw new ExceptionEx("프로젝트에 네임스페이스 등록에 실패했습니다.");
				}
				
				if(name.equals(id))
				{
					if(Boolean.parseBoolean(MAVEN_PRIVATE_REPOSITORY_ISUSED))
					{
						// camel-k maven settings.xml configMap 생성
						if(!applyKamelMavenConfigMap(name))
						{
							throw new ExceptionEx("해당 네임스페이스에 ConfigMap(Kamel용 maven settings.xml) 생성을 실패했습니다.");
						};
						
						// camel-k-operator 삭제 후 재설치
						kamelService.exec(String.format("kamel uninstall --all -n %s", name));
						kamelService.exec(String.format("kamel install --force -n %s %s %s", name, kamelOption, kamelMavenOption));
					}
					else
					{
						// camel-k-operator 삭제 후 재설치
						kamelService.exec(String.format("kamel uninstall --all -n %s", name));
						kamelService.exec(String.format("kamel install --force -n %s %s", name, kamelOption));
					}
					
					return name;
				}
				else
					throw new ExceptionEx("namespace 조회 정보가 정상적이지 않습니다.");
			}
			else
			{
				if(apiJsonData.getHttpStatus() == 404)
				{
					String projectId =  createProject(name);
					
					if(createNamespace(name, projectId))
					{
						if(Boolean.parseBoolean(MAVEN_PRIVATE_REPOSITORY_ISUSED))
						{
							// camel-k maven settings.xml configMap 생성
							if(!applyKamelMavenConfigMap(name))
							{
								throw new ExceptionEx("해당 네임스페이스에 ConfigMap(Kamel용 maven settings.xml) 생성을 실패했습니다.");
							};
							
							// camel-k-operator 삭제 후 재설치
							kamelService.exec(String.format("kamel uninstall --all -n %s", name));
							kamelService.exec(String.format("kamel install --force -n %s %s %s", name, kamelOption, kamelMavenOption));
						}
						else
						{
							// camel-k-operator 삭제 후 재설치
							kamelService.exec(String.format("kamel uninstall --all -n %s", name));
							kamelService.exec(String.format("kamel install --force -n %s %s", name, kamelOption));
						}
						
						return name;
					}
				}
				throw new ExceptionEx("namespace 조회에 실패했습니다.");
			}
		}
		catch(ExceptionEx ee)
		{
			throw ee;
		}
	}
	
	
	// ============ Namespace 관련 함수 ============================================================
	private String createProject(String name) throws Exception
	{
		String id = getProjectId(name);
		if(!"".equals(id))  // 기존 프로젝트가 있우면 체크
		{
			String projectId = getProjectName(id);
			if(name.equals(projectId))
			{
				createProjectDockerCredential(id);
				return id;
			}
		}
		
		String clusterId = getClusterId();
		String rancherBaseUrl = getRancherBaseUrl();
		String post = String.format("{\"clusterId\": \"%s\",\"enableProjectMonitoring\": false, \"labels\": {}, \"name\": \"%s\", \"type\": \"project\"}", clusterId , name);
		String url = String.format("%s/project?_replace=true",rancherBaseUrl);
		APIJsonData apiJsonData = getAPIPost(url, post , "$..id");
		
		if(apiJsonData.IsSuccess())
		{
			id = apiJsonData.toString();
			
			post = String.format("{\"podSecurityPolicyTemplateId\": null}");
			url = String.format("%s/projects/%s?action=setpodsecuritypolicytemplate",rancherBaseUrl , id);
			apiJsonData = getAPIPost(url, post , "");
			
			if(apiJsonData.IsSuccess())
			{
				createProjectDockerCredential(id);
				return id;
			}
		}
		
		// secret
		// https://caas.kepri-demo.crossent.com:8000/v3/projects/c-hwmqp:p-9cbgl/dockercredential
		// name: "harbor-ntels"
		// registries: {192.168.14.51:5000: {username: "ntels", password: "Ntels123!@#"}}
		// type: "dockerCredential"
		
		throw new ExceptionEx("프로젝트 생성에 실패했습니다.");
	}
	
	private boolean createProjectDockerCredential(String projectId) throws Exception
	{
		String registryUrl = getBaseUrl("HTTP://"+dockerRegistry);
		String rancherBaseUrl = getRancherBaseUrl();
		String post = String.format("{\"name\": \"harbor-ntels\",\"type\": \"dockerCredential\", \"registries\": { \"%s\": {\"username\": \"%s\",\"password\": \"%s\"}}}", registryUrl,dockerUsr,dockerPWD);
		String url = String.format("%s/projects/%s/dockercredential",rancherBaseUrl,projectId);
		APIJsonData apiJsonData = getAPIGetData(url, "$.data[*].name");
		
		if("".equals(apiJsonData.toString()))
		{
			getAPIPost(url, post , "");
		}
		return true;
	}
	
	private boolean createNamespace(String name, String projectId) throws Exception
	{
		String clusterId = getClusterId();
		String rancherBaseUrl = getRancherBaseUrl();
		String post = String.format("{\"clusterId\": \"%s\" , \"labels\": {}, \"name\": \"%s\",\"projectId\": \"%s\",\"resourceQuota\": null,\"type\": \"namespace\"}", clusterId ,name, projectId);
		String url = String.format("%s/clusters/%s/namespace",rancherBaseUrl,clusterId);
		APIJsonData apiJsonData = getAPIPost(url, post , "");
		
		if(apiJsonData.IsSuccess())
		{
			return true;
		}
		else
		{
			return false;
		}
	}
	
	private boolean moveProject(String name, String projectId) throws Exception
	{
		String clusterId = getClusterId();
		String rancherBaseUrl = getRancherBaseUrl();
		String post = String.format("{\"projectId\": \"%s\"}", projectId);
		String url = String.format("%s/clusters/%s/namespaces/%s?action=move",rancherBaseUrl,clusterId,name);
		APIJsonData apiJsonData = getAPIPost(url, post , "");
		
		return apiJsonData.IsSuccess();		
	}
	
	private String getProjectId(String name) throws Exception
	{
		String clusterId = getClusterId();
		String rancherBaseUrl = getRancherBaseUrl();
		String url = String.format("%s/projects?clusters=%s&name=%s", rancherBaseUrl,clusterId,name);
		APIJsonData apiJsonData = getAPIGetData(url, "$.data[*].id");
		
		if(apiJsonData.IsSuccess() && apiJsonData.getJsonArray().size() >= 1)
		{
			return apiJsonData.getJsonArray().get(0).toString();
		}
		
		return "";
	}
	
	private String getProjectName(String projectId) throws Exception
	{
		String clusterId = getClusterId();
		String rancherBaseUrl = getRancherBaseUrl();
		String url = String.format("%s/projects?clusters=%s&id=%s", rancherBaseUrl,clusterId,projectId);
		APIJsonData apiJsonData = getAPIGetData(url, "$.data[*].name");
		
		if(apiJsonData.IsSuccess() && apiJsonData.getJsonArray().size() >= 1)
		{
			return apiJsonData.getJsonArray().get(0).toString();
		}
		
		return "";
	}
	
	private String getRancherBaseUrl() throws Exception
	{
		URL urlObj = new URL(this.baseUrl);
		int port = urlObj.getPort();
		String sPort = port == -1 ? "" : String.format(":%d", port);
		String url = String.format("%s://%s%s/v3", urlObj.getProtocol(),urlObj.getHost(),sPort);
		return url;
	}
	
	private String getBaseUrl(String str) throws Exception
	{
		URL urlObj = new URL(str);
		int port = urlObj.getPort();
		String sPort = port == -1 ? "" : String.format(":%d", port);
		String url = String.format("%s%s", urlObj.getHost(),sPort);
		return url;
	}
	
	private String getClusterId()
	{
		String[] arr = this.baseUrl.split("/");
		if(arr.length > 1)
		{
			String item = arr[arr.length -1];
			String item2 = arr[arr.length -2];
			if(item != null && !"".equals(item))
				return item;
			else
				return item2;
		}
		return "";
	}
	// ============ Namespace 관련 함수 ============================================================	
	
	
	// 등록
	public KCLIData createService(String projectId , FunctionInfo functionInfo ) throws Exception
	{
		KCLIData cliData = new KCLIData();
		try
		{
			String content;
			functionInfo.setNamespace(getNamespace(projectId));
			functionInfo.setDockerRegistrySecret(dockerRegistrySecret);
			content = functionInfo.toYamelString();
			cliData = kubeCtlService.apply_f(content, E_FILE_TYPE.YMAL);
			if(cliData.result == false) throw new ExceptionCreateService("");
		}
		catch(ExceptionCreateService ee)
		{
		    logger.error(null, ee);
		    throw ee;
		}
		return cliData;
	}


	public KCLIData createTrigger(String projectId, String serviceName, TriggerInfoFactory.E_TRRIGGER_TYPE type, Map<String, Object> param) throws Exception
	{
		KCLIData cliData = new KCLIData();
		try
		{
			if(getExistService(projectId, serviceName))
			{
				FunctionInfo functionInfo = getService(projectId, serviceName);
				TriggerInfo info = triggerInfoFactory.createTriggerInfo(type, serviceName, projectId, param);
				cliData = createTrigger(functionInfo, info);
			}
			else
			{
				cliData.msg = String.format("%s:%s 는 존재하지 않는 함수명입니다.", projectId, serviceName);
			}
		}
		catch(ExceptionEx e)
		{
			e.printStackTrace();
		}
		
		return cliData;
	}
	
	protected KCLIData createTrigger(FunctionInfo functionInfo, TriggerInfo info) throws Exception
	{
		KCLIData cliData = new KCLIData();
		
		try
		{
			cliData = setTrigger(functionInfo, info);
		}
		catch(ExceptionEx e)
		{
			e.printStackTrace();
		}
		
		return cliData;
	}
	
	// 수정
	public KCLIData updateService(String projectId , FunctionInfo functionInfo ) throws Exception
	{
		String content;
		functionInfo.setNamespace(projectId);
		content = functionInfo.toYamelString();
		KCLIData cliData = kubeCtlService.apply_f(content, E_FILE_TYPE.YMAL);
		return cliData;
	}
	
	public KCLIData updateTrigger(String projectId , String serviceName, TriggerInfoFactory.E_TRRIGGER_TYPE type , Map<String, Object> param) throws Exception
	{
		KCLIData cliData = new KCLIData();
		
		try
		{
			if(getExistService(projectId ,serviceName))
			{
				FunctionInfo functionInfo = getService(projectId , serviceName);
				TriggerInfo info = triggerInfoFactory.createTriggerInfo(type, serviceName, projectId, param);
				cliData = createTrigger(functionInfo,info);
			}
			else
			{
				cliData.msg = String.format("%s:%s 는 존재하지 않는 함수명입니다.", projectId,serviceName);
			}
		}
		catch(ExceptionEx e)
		{
			e.printStackTrace();
		}
		
		return cliData;
	}
	
	protected KCLIData setTrigger(FunctionInfo functionInfo, TriggerInfo info) throws Exception
	{
		KCLIData cliData = new KCLIData();
		
		if(functionInfo != null && info != null)
		{
			try
			{
				String yaml = "";
				yaml = info.toChannelYaml();
				if(!StringUtil.isEmpty(yaml))
				{
					cliData = kubeCtlService.apply_f(yaml, E_FILE_TYPE.YMAL);
					if(cliData.result == false) throw new ExceptionEx("체널 등록에 실패 했습니다.");
				}
				else
				{
					throw new ExceptionEx("체널 등록정보가 없습니다.");
				}
				
				//yaml = info.toKamelSourceYaml();
				yaml = info.toKamelSource();
				if(!StringUtil.isEmpty(yaml))
				{
					Map dependencies = getTriggerDependencies(info);
					Map traits = getTriggerTraits(info);
					
					cliData = kamelService.run(functionInfo.getNamespace(), info.getKamelName(), yaml, E_FILE_TYPE.GROOVY, dependencies, traits);
					//cliData = kubeCtlService.apply_f(yaml, E_FILE_TYPE.YMAL);
					if(cliData.result == false) throw new ExceptionEx("카멜 등록에 실패 했습니다.");
					//info.setKamelName(cliData.fnameOnly);
				}
				else
				{
					System.out.println("DEBUG : 개발전 - 카멜 등록정보가 없습니다.");
					//throw new ExceptionEx("카멜 등록정보가 없습니다.");
				}
				
				yaml = info.toSubscriptionYaml();
				if(!StringUtil.isEmpty(yaml))
				{
					cliData = kubeCtlService.apply_f(yaml, E_FILE_TYPE.YMAL);
					if(cliData.result == false) throw new ExceptionEx("서브스크립션 등록에 실패 했습니다.");
				}
				else
				{
					throw new ExceptionEx("서브스크립션 등록정보가 없습니다.");
				}
				
				functionInfo.getListTrigger().add(info);
				cliData = updateService(functionInfo.getNamespace() , functionInfo);
				if(cliData.result == false) throw new ExceptionEx("서비스 정보 갱신에 실패했습니다.");
			}
			catch(ExceptionEx e)
			{
				deleteTriger(functionInfo,info);
				cliData.msg = e.getMessage();
			}
			
		}
		return cliData;
	}

	//조회 - 서비스
	public FunctionInfo getService(String projectId , String serviceName) throws Exception
	{
		String url = String.format("/apis/serving.knative.dev/v1/namespaces/%s/services/%s", projectId , serviceName);
		APIJsonData apiJsonData = getServiceJson(url,"");
		
		FunctionInfo info = null;
		if(apiJsonData.IsSuccess())
		{
			List<APIJsonData> listJsonData = JsonUtil.getJsonArrayUnit(apiJsonData, E_ORDERBY.ASC , -1);
			if(listJsonData.size() == 1)
			{
				APIJsonData data = listJsonData.get(0);
				info = JsonUtil.jsonDataMapper(data, FunctionInfo.class);
				getListTrigger(projectId , info);
			}
		}
		
		return info;
	}
	
	//임시
	public List<FunctionInfo> getListService(String projectId,Constant.E_SERVICE_FILTER filter,Constant.E_ORDER_TYPE type) throws Exception
	{
		String url = String.format("/apis/serving.knative.dev/v1/namespaces/%s/services", projectId);
		APIJsonData apiJsonData = getServiceJson(url,"items[*]");
		
		List<FunctionInfo> list = new ArrayList<>();
		if(apiJsonData.IsSuccess())
		{
			List<APIJsonData> listJsonData = JsonUtil.getJsonArrayUnit(apiJsonData, E_ORDERBY.ASC , -1);
			for(int i=0;i<listJsonData.size();i++)
			{
				APIJsonData data = listJsonData.get(i);
				FunctionInfo functionInfo = JsonUtil.jsonDataMapper(data, FunctionInfo.class);
				
				if(functionInfo.isFilterMatch(filter))
					list.add(functionInfo);
			}
		}
		
		list = sortFunctionInfo(list , type , Constant.E_ORDERBY.ASC);
		
		return list;
	}
	
	
	
	protected List<FunctionInfo> getListService(String projectId) throws Exception
	{
		String url = String.format("/apis/serving.knative.dev/v1/namespaces/%s/services", projectId);
		APIJsonData apiJsonData = getServiceJson(url,"items[*]");
		
		List<FunctionInfo> list = new ArrayList<>();
		if(apiJsonData.IsSuccess())
		{
			List<APIJsonData> listJsonData = JsonUtil.getJsonArrayUnit(apiJsonData, E_ORDERBY.ASC , -1);
			for(int i=0;i<listJsonData.size();i++)
			{
				APIJsonData data = listJsonData.get(i);
				FunctionInfo functionInfo = JsonUtil.jsonDataMapper(data, FunctionInfo.class);
				getListTrigger(projectId , functionInfo);
				list.add(functionInfo);
			}
		}
		
		return list;
	}
	
	// 조회 - trigger 관련
	public List<TriggerInfo> getListTrigger(String projectId , String serviceName) throws Exception
	{
		List<TriggerInfo> list = new ArrayList<>();
		KCLIData cliData = new KCLIData();
		
		FunctionInfo functionInfo = getService(projectId , serviceName);
		if(functionInfo != null)
		{
			list = getListTrigger(projectId , functionInfo);
		}
		else 
		{
			cliData.msg = String.format("%s:%s 는 존재하지 않는 함수명입니다.", projectId,serviceName);
		}
		return list;
	}
	
	public List<TriggerInfo> getListTrigger(String projectId , FunctionInfo functionInfo) throws Exception
	{
		List<TriggerInfo> list = new ArrayList<>();
		
		KCLIData cliData = new KCLIData();
		try
		{
			if(functionInfo != null)
			{
				String url = String.format("/apis/serving.knative.dev/v1/namespaces/%s/services/%s", projectId , functionInfo.getName());
				APIJsonData apiJsonData = getServiceJson(url,"metadata.annotations.ext_trigger");
				
				if(apiJsonData.IsSuccess())
				{
					String json = apiJsonData.getJsonArray().get(0).toString();
					List<String> listName = new ObjectMapper().readValue(json, List.class) ;
					for(String name : listName)
					{
						TriggerInfo info = getTrigger(projectId,name);
						if(info != null)
							functionInfo.getListTrigger().add(info);
					}
				}
			}
			else 
			{
				cliData.msg = String.format("%s:%s 는 존재하지 않는 함수명입니다.", projectId,functionInfo.getName());
			}
		}
		catch(ExceptionEx e)
		{
			
		}
		return null;
	}
	
	public TriggerInfo getTrigger(String projectId , String name) 
	{
		TriggerInfo info = null;
		try
		{
			String url = String.format("/apis/messaging.knative.dev/v1alpha1/namespaces/%s/subscriptions/%s", projectId , name);
			APIJsonData apiJsonData = getServiceJson(url,"");
			if(apiJsonData.IsSuccess())
			{
				String ext_trigger_type = JsonUtil.getFilterJson(apiJsonData, "$.[*].metadata.annotations.ext_trigger_type").toString();
				TriggerInfoFactory.E_TRRIGGER_TYPE type = triggerInfoFactory.getTiggerType(ext_trigger_type);
				info = triggerInfoFactory.createTriggerInfoByJson(type, apiJsonData);
			}
		}
		catch(Exception e)
		{
			
		}
		return info;
	}
	
	// 삭제 - service
	public KCLIData deleteService(String projectId , String serviceName) throws Exception
	{
		return deleteService(projectId, serviceName, true);
	}
	
	public KCLIData deleteServiceOnly(String projectId , String serviceName) throws Exception
	{
		return deleteService(projectId, serviceName, false);
	}
	
	protected KCLIData deleteService(String projectId , String serviceName , boolean bDeleteTrigger) throws Exception
	{
		KCLIData cliData = new KCLIData();
		try
		{
			
			if(getExistService(projectId , serviceName))
			{
				FunctionInfo functionInfo = getService(projectId , serviceName);
				functionInfo.setName(serviceName);
				if(bDeleteTrigger == true)
				{
					for(TriggerInfo info : functionInfo.getListTrigger())
					{
						cliData = deleteTriger(functionInfo , info);
						if(cliData.result == false) throw new ExceptionDeleteService("");
					}
				}
				
				String yaml = "";
				yaml = functionInfo.toYamelString();
				if(!StringUtil.isEmpty(yaml))
				{
					cliData = kubeCtlService.delete_f(yaml, E_FILE_TYPE.YMAL);
					if(cliData.result == false) throw new ExceptionDeleteService("서비스 삭제에 실패 했습니다.");
				}
				else
				{
					throw new ExceptionDeleteService("서비스 삭제정보가 없습니다.");
				}
			}
			else 
			{
				cliData.msg = String.format("%s:%s 는 존재하지 않는 함수명입니다.", projectId,serviceName);
			}
		}
		catch(ExceptionDeleteService e)
		{
			
		}
		
		return cliData;
	}
	
	
	// 삭제 - trigger
	public KCLIData deleteTriger(String projectId , String serviceName , String triggerName) throws Exception
	{
		KCLIData cliData = new KCLIData();
		try
		{
			if(getExistService(projectId , serviceName))
			{
				int deleteCnt = 0;
				FunctionInfo functionInfo = getService(projectId , serviceName);
				
				List<TriggerInfo> list = functionInfo.getListTrigger();
				
				for(int ia=0;ia<list.size();ia++)
				{
					TriggerInfo info = list.get(ia);
					System.err.println("채널 이름:::" + info.getChannelName() + "트리거 이름:::" + info.getName());
					if(triggerName.equals(info.getName()))
					{
						cliData = deleteTriger(functionInfo , info);
						deleteCnt++;
						if(cliData.result == false) throw new ExceptionDeleteService("");
					}
				}
				
				if(deleteCnt == 0)
					cliData.msg = String.format("%s 는 존재하지 않는 트리거명입니다.", triggerName);
			}
			else 
			{
				cliData.msg = String.format("%s:%s 는 존재하지 않는 함수명입니다.", projectId,serviceName);
			}
		}
		catch(ExceptionEx e)
		{
			
		}
		
		return cliData;
	}
	
	public KCLIData deleteAllTriger(String projectId , String serviceName ) throws Exception
	{
		KCLIData cliData = new KCLIData();
		
		try
		{
			if(getExistService(projectId , serviceName))
			{
				FunctionInfo functionInfo = getService(projectId , serviceName);
				for(TriggerInfo info : functionInfo.getListTrigger())
				{
					cliData = deleteTriger(functionInfo , info);
					if(cliData.result == false) throw new ExceptionDeleteService("");
				}
			}
			else 
			{
				cliData.msg = String.format("%s:%s 는 존재하지 않는 함수명입니다.", projectId,serviceName);
			}
		}
		catch(ExceptionDeleteService e)
		{
			
		}
		
		return cliData;
	}
	
	protected KCLIData deleteTriger(FunctionInfo functionInfo , TriggerInfo info) throws Exception
	{
		KCLIData cliData = new KCLIData();
		if(functionInfo != null && info != null)
		{
			try
			{
				String yaml = "";
				yaml = info.toChannelYaml();
				System.err.println("채널 yaml::: " + yaml);
				if(!StringUtil.isEmpty(yaml))
				{
					cliData = kubeCtlService.delete_f(yaml, E_FILE_TYPE.YMAL);
					//if(cliData.result == false) throw new ExceptionEx("체널 삭제에 실패 했습니다.");
				}
				else
				{
					throw new ExceptionEx("체널 삭제정보가 없습니다.");
				}
				
				yaml = info.getKamelName();
				System.err.println("카멜 이름::: " + yaml);
				if(!StringUtil.isEmpty(yaml))
				{
					String namesapce = info.getNamespace();
					cliData = kamelService.delete(yaml,namesapce);
					//if(cliData.result == false) throw new ExceptionEx("카멜 삭제에 실패 했습니다.");
				}
				else
				{
					System.out.println("DEBUG : 개발전 - 카멜 삭제정보가 없습니다.");
					//throw new ExceptionEx("카멜 등록정보가 없습니다.");
				}
				
				yaml = info.toSubscriptionYaml();
				System.err.println("섭스크립션 yaml::: " + yaml);
				if(!StringUtil.isEmpty(yaml))
				{
					cliData = kubeCtlService.delete_f(yaml, E_FILE_TYPE.YMAL);
					//if(cliData.result == false) throw new ExceptionEx("서브스크립션 삭제에 실패 했습니다.");
				}
				else
				{
					throw new ExceptionEx("서브스크립션 삭제정보가 없습니다.");
				}
				
				
				
				functionInfo.getListTrigger().remove(info);
				cliData = updateService(functionInfo.getNamespace() , functionInfo);
				if(cliData.result == false) throw new ExceptionEx("서비스 정보 갱신에 실패했습니다.");
			}
			catch(ExceptionEx e)
			{
				cliData.msg = e.getMessage();
			}
			
		}
		return cliData;
		
	}
	
	// private
	private boolean getExistService(String projectId , String serviceName) throws ExceptionEx
	{
		try
		{
			String url = String.format("/apis/serving.knative.dev/v1/namespaces/%s/services/%s", projectId , serviceName);
			APIJsonData apiJsonData = getServiceJson(url,"metadata.name");
			
			if(apiJsonData.IsSuccess() && apiJsonData.getJsonArray().size() == 1)
			{
				return true;
			}
			
			return false;
		}
		catch(Exception e)
		{
			throw new ExceptionEx("서비스명 조회를 실패했습니다.");
		}
	}
	
	private List<FunctionInfo> sortFunctionInfo(List<FunctionInfo> list ,Constant.E_ORDER_TYPE type, Constant.E_ORDERBY e_orderby)
	{
		list.sort(new Comparator<FunctionInfo>() {
			@Override
            public int compare(FunctionInfo arg0, FunctionInfo arg1) {
                   String age0 = arg0.getCompartor(type);
                   String age1 = arg1.getCompartor(type);
                   
                   if(e_orderby == E_ORDERBY.ASC)
                	   return age0.compareTo(age1);
                   
                   else
                	   return age1.compareTo(age0);
                	   
            }
		});
		
		return list;
	}
	
	private Map<String, Object> getTriggerDependencies(TriggerInfo info) {
		Map<String, Object> returnMap = new HashMap<>();
		
		String trigger_type = info.getType();
		
		if(trigger_type.equals(TriggerInfoFactory.E_TRRIGGER_TYPE.MARIADB.toString())) {
			returnMap.put("mvn:io.debezium/debezium-connector-mysql", "1.1.0.Final");
			returnMap.put("mvn:org.apache.camel/camel-gson", "3.3.0");
		} else if(trigger_type.equals(TriggerInfoFactory.E_TRRIGGER_TYPE.MONGODB.toString())) {
			returnMap.put("mvn:org.apache.camel/camel-gson", "3.3.0");
		} else if(trigger_type.equals(TriggerInfoFactory.E_TRRIGGER_TYPE.POSTGRESQL.toString())) {
			returnMap.put("mvn:io.debezium/debezium-connector-postgres", "1.1.0.Final");
			returnMap.put("mvn:org.apache.camel/camel-gson", "3.3.0");
		}
		
		return returnMap;
	}
	
	private Map<String, Object> getTriggerTraits(TriggerInfo info) {
		Map<String, Object> returnMap = new HashMap<>();
		
		String trigger_type = info.getType();
		
		if(trigger_type.equals(TriggerInfoFactory.E_TRRIGGER_TYPE.TIMER.toString())) {
			returnMap.put("cron.enabled", "false");
		}
		
		return returnMap;
	}
	
	// camel-k maven settings.xml configMap 생성
	private boolean applyKamelMavenConfigMap(String namespace) throws Exception {
		KCLIData cliData = new KCLIData();
		
		try {
			String content = "";
			
			URL url = Resources.getResource("yaml/kamel-maven-configmap.yaml");
			String templateString = Resources.toString(url, StandardCharsets.UTF_8);

			Map<String, Object> dataMap = new HashMap<>();
			dataMap.put("name", "maven-settings");
			dataMap.put("namespace", namespace);
			dataMap.put("MAVEN_PRIVATE_REPOSITORY_URL", MAVEN_PRIVATE_REPOSITORY_URL);

			Handlebars hb = new Handlebars();
			Template template = hb.compileInline(templateString);
			content = template.apply(dataMap);
			
			deleteKamelMavenConfigMap(content);
			
			cliData = kubeCtlService.apply_f(content, E_FILE_TYPE.YMAL);
			
			if(cliData.result == false)
				throw new Exception();
		} catch(Exception e) {
			return false;
		}
		
		return true;
	}

	private void deleteKamelMavenConfigMap(String content) throws Exception {
		KCLIData cliData = new KCLIData();

		try {
			cliData = kubeCtlService.delete_f(content, E_FILE_TYPE.YMAL);
			
			if(cliData.result == false)
				throw new Exception();
		} catch(Exception e) {
			// Ignore exceptions.
		}
	}
}