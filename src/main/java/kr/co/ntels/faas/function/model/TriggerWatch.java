package kr.co.ntels.faas.function.model;

import kr.co.ntels.faas.common.apiCommon.util.JsonSearch;
import kr.co.ntels.faas.function.model.ServiceWatch.E_SERVICE_SOURCE;

public class TriggerWatch {
	
	String source = "CAMEL_WATCH";
	
	@JsonSearch(path="$..metadata.name")
	String name;
	
	@JsonSearch(path="$..metadata.namespace")
	String namespace;
	
	@JsonSearch(path="$.type")
	String type;
	
	@JsonSearch(path="$..status.conditions[0].status",scope="function")
	String status_integrationPlatformAvailable;
	
	@JsonSearch(path="$..status.conditions[1].status",scope="function")
	String status_integrationKitAvailable;
	
	@JsonSearch(path="$..status.conditions[2].status",scope="function")
	String status_deploymentAvailable;
	
	@JsonSearch(path="$..status.conditions[3].status",scope="function")
	String status_knativeServiceAvailable;
	
	@JsonSearch(path="$..status.conditions[0].message",scope="function")
	String message_integrationPlatformAvailable;
	
	@JsonSearch(path="$..status.conditions[1].message",scope="function")
	String message_integrationKitAvailable;
	
	@JsonSearch(path="$..status.conditions[2].message",scope="function")
	String message_deploymentAvailable;
	
	@JsonSearch(path="$..status.conditions[2].message",scope="function")
	String message_knativeServiceAvailable;
	
	@JsonSearch(path="$..status.conditions[0].reason")
	String reason_integrationPlatformAvailable;
	
	@JsonSearch(path="$..status.conditions[1].reason")
	String reason_integrationKitAvailable;
	
	@JsonSearch(path="$..status.conditions[2].reason")
	String reason_deploymentAvailable;
	
	@JsonSearch(path="$..status.conditions[3].reason")
	String reason_knativeServiceAvailable;
	
	String status;
	
	String log;

	public String getSource() {
		return source;
	}

	public void setSource(String source) {
		this.source = source;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getNamespace() {
		return namespace;
	}

	public void setNamespace(String namespace) {
		this.namespace = namespace;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getStatus_integrationPlatformAvailable() {
		return status_integrationPlatformAvailable;
	}

	public void setStatus_integrationPlatformAvailable(String status_integrationPlatformAvailable) {
		this.status_integrationPlatformAvailable = status_integrationPlatformAvailable;
	}

	public String getStatus_integrationKitAvailable() {
		return status_integrationKitAvailable;
	}

	public void setStatus_integrationKitAvailable(String status_integrationKitAvailable) {
		this.status_integrationKitAvailable = status_integrationKitAvailable;
	}

	public String getStatus_deploymentAvailable() {
		return status_deploymentAvailable;
	}

	public void setStatus_deploymentAvailable(String status_deploymentAvailable) {
		this.status_deploymentAvailable = status_deploymentAvailable;
	}

	public String getStatus_knativeServiceAvailable() {
		return status_knativeServiceAvailable;
	}

	public void setStatus_knativeServiceAvailable(String status_knativeServiceAvailable) {
		this.status_knativeServiceAvailable = status_knativeServiceAvailable;
	}

	public String getMessage_integrationPlatformAvailable() {
		return message_integrationPlatformAvailable;
	}

	public void setMessage_integrationPlatformAvailable(String message_integrationPlatformAvailable) {
		this.message_integrationPlatformAvailable = message_integrationPlatformAvailable;
	}

	public String getMessage_integrationKitAvailable() {
		return message_integrationKitAvailable;
	}

	public void setMessage_integrationKitAvailable(String message_integrationKitAvailable) {
		this.message_integrationKitAvailable = message_integrationKitAvailable;
	}

	public String getMessage_deploymentAvailable() {
		return message_deploymentAvailable;
	}

	public void setMessage_deploymentAvailable(String message_deploymentAvailable) {
		this.message_deploymentAvailable = message_deploymentAvailable;
	}

	public String getMessage_knativeServiceAvailable() {
		return message_knativeServiceAvailable;
	}

	public void setMessage_knativeServiceAvailable(String message_knativeServiceAvailable) {
		this.message_knativeServiceAvailable = message_knativeServiceAvailable;
	}

	public String getReason_integrationPlatformAvailable() {
		return reason_integrationPlatformAvailable;
	}

	public void setReason_integrationPlatformAvailable(String reason_integrationPlatformAvailable) {
		this.reason_integrationPlatformAvailable = reason_integrationPlatformAvailable;
	}

	public String getReason_integrationKitAvailable() {
		return reason_integrationKitAvailable;
	}

	public void setReason_integrationKitAvailable(String reason_integrationKitAvailable) {
		this.reason_integrationKitAvailable = reason_integrationKitAvailable;
	}

	public String getReason_deploymentAvailable() {
		return reason_deploymentAvailable;
	}

	public void setReason_deploymentAvailable(String reason_deploymentAvailable) {
		this.reason_deploymentAvailable = reason_deploymentAvailable;
	}

	public String getReason_knativeServiceAvailable() {
		return reason_knativeServiceAvailable;
	}

	public void setReason_knativeServiceAvailable(String reason_knativeServiceAvailable) {
		this.reason_knativeServiceAvailable = reason_knativeServiceAvailable;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getLog() {
		return log;
	}

	public void setLog(String log) {
		this.log = log;
	}

}
