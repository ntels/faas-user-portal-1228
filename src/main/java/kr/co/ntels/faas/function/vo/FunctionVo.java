package kr.co.ntels.faas.function.vo;

public class FunctionVo {
    private int function_id;
    private String name;
    private String runtime;
    private String version;
    private String namespace;
    private String dsp_name;
    private String des;
    private String git_url;
    private String git_usr;
    private String stat_cd;
    private String reg_step_cd;
    private String reg_usr;
    private String reg_date_hms;
    private String mod_usr;
    private String mod_date_hms;

    public int getFunction_id() {
        return function_id;
    }

    public void setFunction_id(int function_id) {
        this.function_id = function_id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getRuntime() {
        return runtime;
    }

    public void setRuntime(String runtime) {
        this.runtime = runtime;
    }

    public String getVersion() {
        return version;
    }

    public void setVersion(String version) {
        this.version = version;
    }

    public String getNamespace() {
        return namespace;
    }

    public void setNamespace(String namespace) {
        this.namespace = namespace;
    }

    public String getDsp_name() {
        return dsp_name;
    }

    public void setDsp_name(String dsp_name) {
        this.dsp_name = dsp_name;
    }

    public String getDes() {
        return des;
    }

    public void setDes(String des) {
        this.des = des;
    }

    public String getGit_url() {
        return git_url;
    }

    public void setGit_url(String git_url) {
        this.git_url = git_url;
    }

    public String getGit_usr() {
        return git_usr;
    }

    public void setGit_usr(String git_usr) {
        this.git_usr = git_usr;
    }

    public String getStat_cd() {
        return stat_cd;
    }

    public void setStat_cd(String stat_cd) {
        this.stat_cd = stat_cd;
    }

    public String getReg_step_cd() {
        return reg_step_cd;
    }

    public void setReg_step_cd(String reg_step_cd) {
        this.reg_step_cd = reg_step_cd;
    }

    public String getReg_usr() {
        return reg_usr;
    }

    public void setReg_usr(String reg_usr) {
        this.reg_usr = reg_usr;
    }

    public String getReg_date_hms() {
        return reg_date_hms;
    }

    public void setReg_date_hms(String reg_date_hms) {
        this.reg_date_hms = reg_date_hms;
    }

    public String getMod_usr() {
        return mod_usr;
    }

    public void setMod_usr(String mod_usr) {
        this.mod_usr = mod_usr;
    }

    public String getMod_date_hms() {
        return mod_date_hms;
    }

    public void setMod_date_hms(String mod_date_hms) {
        this.mod_date_hms = mod_date_hms;
    }
}
