package kr.co.ntels.faas.function.service.dynamicDB;

import java.util.ArrayList;
import java.util.List;

import javax.sql.DataSource;

import org.mybatis.spring.SqlSessionFactoryBean;
import org.mybatis.spring.SqlSessionTemplate;
import org.springframework.core.io.Resource;
import org.springframework.core.io.support.PathMatchingResourcePatternResolver;

import com.zaxxer.hikari.HikariConfig;
import com.zaxxer.hikari.HikariDataSource;

import kr.co.ntels.faas.userException.ExceptionDBConnection;

public abstract class DynamicDB implements IDynamicDB{
	
	protected DynamicDBFactory.DBInfo info;
	
	public DynamicDB() {}
	
	public DynamicDB(DynamicDBFactory.DBInfo info)
	{
		this.info = info;
	}
	
	public List<String> getListTableName() throws ExceptionDBConnection
	{
		List<String> list = new ArrayList<>();
		try
		{
			list = getListTableNameDB();
		}
		catch(Exception e)
		{
			throw new ExceptionDBConnection("db connection fail");
		}
		return list;
	}
	
	protected abstract List<String> getListTableNameDB() throws Exception;
}