package kr.co.ntels.faas.function.service;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.net.URL;
import java.nio.charset.StandardCharsets;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.zip.ZipEntry;
import java.util.zip.ZipInputStream;

import javax.annotation.PostConstruct;

import org.json.JSONArray;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.MultipartHttpServletRequest;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.github.jknack.handlebars.Handlebars;
import com.github.jknack.handlebars.Template;
import com.google.common.io.Resources;

import kr.co.ntels.common.utils.CommonUtils;
import kr.co.ntels.faas.common.apiCommon.util.JsonUtil;
import kr.co.ntels.faas.common.apiCommon.util.StringUtil;
import kr.co.ntels.faas.common.apiCommonService.model.KCLIData;
import kr.co.ntels.faas.common.apiCommonService.model.KCLIData.E_FILE_TYPE;
import kr.co.ntels.faas.constant.FaaSCommCD;
import kr.co.ntels.faas.dao.DPLDao;
import kr.co.ntels.faas.dao.FaasDao;
import kr.co.ntels.faas.function.model.JenkinsLog;
import kr.co.ntels.faas.function.model.api.FunctionInfo;
import kr.co.ntels.faas.function.model.api.trigger.TriggerInfo;
import kr.co.ntels.faas.function.model.api.trigger.TriggerInfoBase;
import kr.co.ntels.faas.function.vo.FunctionVo;
import kr.co.ntels.faas.gitlab.service.GitLabService;
import kr.co.ntels.faas.trigger.service.TriggerService;
import kr.co.ntels.faas.userException.ExceptionCreateService;
import kr.co.ntels.faas.userException.ExceptionDeleteService;
import kr.co.ntels.faas.userException.ExceptionEx;
import kr.co.ntels.faas.userException.ExceptionNotExist;
import kr.co.ntels.faas.userInterface.ICallbackFunction;
import kr.co.ntels.faas.webSocketHandler.FunctionWebSocketHandler;

@Service
public class FunctionService extends FunctionServiceBase implements ICallbackFunction{
	
	final String targetServicePrefix = "target-service-";
	final String targetServiceNamespace = "target";
	
	@Value("${file.path}") String uploadFilePath;
	@Value("${target.service.img}") String targetServiceImg;
	
	@Autowired
	FaasDao dao;
	
	@Autowired
	DPLDao dplDao;
	
	@Autowired
	KongService kongService;
	
	@Autowired
	FunctionWebSocketHandler functionWebSocketHandler;

	@Autowired
	private TriggerService triggerService;
	
	@PostConstruct
	public void init()
	{
		if("/".equals(uploadFilePath.substring(uploadFilePath.length()-1)))
			uploadFilePath = uploadFilePath.substring(0,uploadFilePath.length()-1);
		
		// 함수 target(end-point) 서비용 네임스페이스를 생성해 준다.
		try
		{
			this.getNamespace(targetServiceNamespace);
		}
		catch(Exception e)
		{
			System.out.println("target 서비스용 네임스페이스 생성에 실패했습니다.");
			e.printStackTrace();
		}
	}
	
	public List<Map<String, Object>> getFunctionList(Map<String, Object> map) throws Exception {
		List<Map<String, Object>> list = dao.select("faas.function.functionList", map);
		list = convertFunction(list);
		return list;
	}
	
	public List<Map<String, Object>> getFunction(Map<String, Object> map) throws Exception {
		List<Map<String, Object>> list = dao.select("faas.function.function", map);
		list = convertFunction(list);
		return list;
	}
	
	public Map<String, Object> getFunction(String function_id) throws Exception {
		Map<String, Object> map = new HashMap<>();
		map.put("function_id", function_id);
		Map<String, Object> retMap = (Map<String, Object>)dao.selectOne("faas.function.function", map);
		return convertFunction(retMap);
	}
	
	public Map<String, Object> getFunction(String namespace , String function_id) throws Exception {
		Map<String, Object> map = new HashMap<>();
		map.put("namespace", namespace);
		map.put("function_id", function_id);
		Map<String, Object> retMap = (Map<String, Object>)dao.selectOne("faas.function.function", map);
		return convertFunction(retMap);
	}
	
	public List<Map<String, Object>> getFunctionId(Map<String, Object> map) throws Exception {
		List<Map<String, Object>> list = dao.select("faas.function.functionId", map);
		list = convertFunction(list);
		return list;
	}
	
	private List<Map<String, Object>> convertFunction(List<Map<String, Object>> list) throws Exception
	{
		ObjectMapper mapper = new ObjectMapper();
		for(int i=0;i<list.size();i++)
		{
			Map<String, Object> map = list.get(i);
			
			Object obj = map.get("env");
			if(obj != null && !"".equals(obj.toString()))
			{
				map.put("env", mapper.readValue(obj.toString(),List.class));
			}

			obj = map.get("trf");
			if(obj != null && !"".equals(obj.toString()))
			{
				map.put("trf", mapper.readValue(obj.toString(),Map.class));
			}
		}
		return list;
	}
	
	private Map<String, Object> convertFunction(Map<String, Object> map) throws Exception{
		ObjectMapper mapper = new ObjectMapper();
		
		Object obj = map.get("env");
		if(obj != null && !"".equals(obj.toString())) {
			map.put("env", mapper.readValue(obj.toString(), List.class));
		}
		
		obj = map.get("trf");
		if(obj != null && !"".equals(obj.toString()))
		{
			map.put("trf", mapper.readValue(obj.toString(), Map.class));
		}
		
		return map;
	}
	
	public int insertRegFunction(Map<String, Object> map) {

		int result = dao.update("faas.function.regFunction", map);
		
		return result;
	}
	
	public int insertRegTrigger(Map<String, Object> map) {
		int result = dao.update("faas.function.regTrigger", map);
		
		return result;
	}
	
	public List<Map<String, Object>> getTriggerList(Map map) {
		List<Map<String, Object>> list = dao.select("faas.function.triggerList", map);
		
		return list;
	}
	
	public List<Map<String, Object>> getGitLabUserList(Map map) {
		List<Map<String, Object>> list = dplDao.select("faas.function.gitLabUserList", map);
		
		return list;
	}
	
	public List<Map<String, Object>> getGitLabUser(Map map) {
		List<Map<String, Object>> list = dplDao.select("faas.function.gitLabUser", map);
		
		return list;
	}
	
	public int insertGitLabUser(Map map) {
		int result = dplDao.update("faas.function.insertGitLabUser", map);
		
		return result;
	}
	
	@Transactional
	public void setServiceMod(Map<String,Object> dataMap) throws Exception
	{
		ObjectMapper mapper = new ObjectMapper();
		Map<String,Object> param = new HashMap<>();
		Map<String,Object> config = (Map<String,Object>)dataMap.get("config");
		
		String function_id = dataMap.get("function_id").toString();
		String env = mapper.writeValueAsString(config.get("env"));
		String trf = mapper.writeValueAsString(config.get("trf"));
		String mem = config.get("mem").toString();
		String cpu = config.get("cpu").toString();
		String concur = config.get("concur").toString();
		int min = Integer.parseInt(((Map<String,Object>)config.get("timeout")).get("min").toString());
		int sec = Integer.parseInt(((Map<String,Object>)config.get("timeout")).get("sec").toString());
		
		param.put("function_id", function_id);
		param.put("env", env);
		param.put("trf", trf);
		param.put("mem", mem);
		param.put("cpu", cpu);
		param.put("concur", concur);
		param.put("timeout", (min*60+sec)+"");
		
		dao.update("faas.function.setFunction", param);
	}
	
	public void startJenkinsLogPolling(Map<String,Object> dataMap) throws Exception
	{
		Map<String,Object> param = new HashMap<>();
		String namespace = dataMap.get("namespace").toString();
		String name = dataMap.get("name").toString();
		String jenkins_url = dataMap.get("jenkins_url").toString();
		String jenkins_admin_id = dataMap.get("jenkins_admin_id").toString();
		String jenkins_admin_pw = dataMap.get("jenkins_admin_pw").toString();
		
		try
		{
			KCLIData data1 = this.deleteServiceOnly(namespace , name);
			if(data1.result == true)
			{
			}
		}
		catch(Exception ex)
		{
			ex.printStackTrace();
		}
		
		param.put("namespace", namespace);
		param.put("name", name);
		param.put("stat", FaaSCommCD.E_STAT.REG.toString());
		
		dao.update("faas.dbSync.setFunctionStat", param);
		
		functionWebSocketHandler.createJenkinsLogPolling(namespace, name, jenkins_url, jenkins_admin_id, jenkins_admin_pw, 1, this);
	}
	
	@Override
	public void callFunction(String type, Object obj) throws Exception {
		
		Map<String, Object> param = new HashMap<>();
		
		if("JenkinsLog".equals(type))
		{
			JenkinsLog jenkinsLog = (JenkinsLog)obj;
			String namespace = jenkinsLog.getNamespace();
			String name = jenkinsLog.getName();
			
			param.put("namespace", namespace);
			param.put("name", name);
			
			List<Map<String, Object>> list = dao.select("faas.function.function", param);
			if(list.size() == 1)
			{
				Map<String, Object> funcmap = list.get(0);
				String function_id = funcmap.get("function_id").toString();
				String dspName = funcmap.get("dsp_name").toString();
				
				//target 서비스 호출 정보 ENV에 추가
				Object targetServiceEnvObject = getTargetServiceEnvObject(namespace, dspName);
				String liststr = funcmap.get("env").toString();
				List envList = new ObjectMapper().readValue(liststr, List.class);
				envList.add(targetServiceEnvObject);
				funcmap.put("env", new ObjectMapper().writeValueAsString(envList));
				// target 정보 ENV에 추가
				
				String sss = new ObjectMapper().writeValueAsString(funcmap);
				FunctionInfo functionInfo = (FunctionInfo)JsonUtil.jsonDataMapper(sss , FunctionInfo.class);
				
				try
				{
					KCLIData data = this.createService(namespace , functionInfo);
					if(data.result == true)
					{
						param.put("namespace", namespace);
						param.put("name", name);
						param.put("step", FaaSCommCD.E_REG_STEP.STEP5.toString());
						
						dao.update("faas.dbSync.setFunctionStat", param);
						functionWebSocketHandler.createKanativeServicePolling(namespace,name,3);

						// template - DB 정보 중 배포하지 않은 trigger를 배포함.
						triggerService.deployInitTriggers(namespace, functionInfo.getName());
						
						// target (대상) service apply
						targetServiceApply(function_id, namespace, dspName);
					}
				}
				catch(ExceptionCreateService ex)
				{
					param.put("namespace", namespace);
					param.put("name", name);
					param.put("step", FaaSCommCD.E_REG_STEP.STEP5.toString());
					param.put("stat", FaaSCommCD.E_STAT.ERROR.toString());
					
					dao.update("faas.dbSync.setFunctionStep", param);
					dao.update("faas.dbSync.setFunctionStat", param);
				}
			}
		}
		
	}
	
	public int getLastPage(int totalCount, int listCount) throws Exception {
		int lastPage = totalCount / listCount;
		 
		if (totalCount % listCount > 0) {
			lastPage++;
		}
		
		return lastPage;
	}
	
	public int getOffsetInPage(int curpage, int listCount) throws Exception {
		return (curpage - 1) * listCount;
	}
	
	public List<Map<String, Object>> getFileListFromJson(String sourceListJson) throws Exception {
		List<Map<String, Object>> returnList = new ArrayList<Map<String, Object>>();
		
		JSONArray jsonFileList = new JSONArray(sourceListJson);
		
		for(int i=0; i<jsonFileList.length(); i++) {
			HashMap<String, Object> tempMap = new HashMap<String, Object>();
			
			JSONObject jsonObject = jsonFileList.getJSONObject(i);
			
			tempMap.put("fileName", jsonObject.get("fileName").toString());
			tempMap.put("fileContent", jsonObject.get("fileContent").toString());
			
			returnList.add(tempMap);
		}
		
		return returnList;
	}
	
	public TriggerInfoBase getTriggereInfoBase(String namespace , String trigger_id) throws Exception
	{
		Map<String, Object> param = new HashMap<>();
		param.put("namespace", namespace);
		param.put("trigger_id", trigger_id);
		
		List<Map<String,Object>> list = dao.select("faas.function.getTriggerBase", param);
		if(list.size() == 1)
		{
			TriggerInfoBase base = JsonUtil.jsonDataMapper(list.get(0), TriggerInfoBase.class);
			return base;
		}
		else
			return null;
	}
	
	@Transactional
	public int deleteFunction(GitLabService gitLabService, String namespace, String function_id) throws Exception
	{
		ObjectMapper mapper = new ObjectMapper();
		Map<String,Object> param = new HashMap<>();
		param.put("namespace", namespace);
		param.put("function_id", function_id);
		
		try
		{
			List<Map<String, Object>> list = getFunction(param);
			
			if(list.size() == 1)
			{
				List<Map<String, Object>> listTrigger = dao.select("faas.function.getTriggerBase", param);
				
				for(int i=0;i<listTrigger.size();i++)
				{
					Map<String, Object> mapTrigger = listTrigger.get(i);
					String trigger_id = mapTrigger.get("trigger_id").toString();
					deleteTrigger(namespace , trigger_id);
				}
				
				Map<String, Object> mapFunction = list.get(0);
				FunctionInfo info = JsonUtil.jsonDataMapper(mapper.writeValueAsString(mapFunction) , FunctionInfo.class);
				if(deleteFunction(info) == 1)
				{
					String dspName = mapFunction.get("dsp_name").toString();
					
					targetServiceDelete(namespace, dspName);
					
					gitLabService.deleteProject(namespace, mapFunction.get("name").toString());
					
					dao.update("faas.function.deleteFunction", param);
					
					return 1;
				}
			}
		}
		catch(ExceptionEx ee)
		{
			
		}
		
		throw new RuntimeException("함수 삭제 실패");
	}
	
	protected int deleteFunction(FunctionInfo info) throws Exception
	{
		KCLIData cliData = new KCLIData();
		String yaml = "";
		yaml = info.toYamelString();
		if(!StringUtil.isEmpty(yaml))
		{
			cliData = kubeCtlService.delete_f(yaml, E_FILE_TYPE.YMAL);
			// if(cliData.result == false) throw new ExceptionDeleteService("서비스 삭제에 실패 했습니다.");
			return 1;
		}
		else
		{
			throw new ExceptionDeleteService("서비스 삭제정보가 없습니다.");
		}
	}
	
	@Transactional
	public int deleteTrigger(String namespace ,String trigger_id) throws Exception
	{
		ObjectMapper mapper = new ObjectMapper();
		Map<String,Object> param = new HashMap<>();
		param.put("namespace",namespace);
		param.put("trigger_id", trigger_id);
		
		try
		{
			List<Map<String, Object>> list =  dao.select("faas.function.getTriggerBase", param);
			if(list.size() == 1)
			{
				Map<String, Object> map = list.get(0);
				if("http".equals(map.get("trigger_tp")))
				{
					String apiName = map.get("api_name").toString();
					String dsp_name = map.get("dsp_name").toString();
					if(	kongService.deleteApiGW(namespace , dsp_name, apiName))
					{
						dao.update("faas.function.deleteTrigger", param);
						return 1;
					}
				}
				else
				{
					TriggerInfoBase info = JsonUtil.jsonDataMapper(mapper.writeValueAsString(map) , TriggerInfoBase.class);
					KCLIData ret = deleteTriger(info);
					if(ret.result)
					{
						dao.update("faas.function.deleteTrigger", param);
						return 1;
					}
				}
			}
		}
		catch(ExceptionEx ee)
		{
			
		}
		throw new RuntimeException("트리거 삭제 실패");
	}
	
	public KCLIData deleteTriger(TriggerInfo info) throws Exception
	{
		KCLIData cliData = new KCLIData();
		
		if(info != null)
		{
			try
			{
				String yaml = "";
				yaml = info.toChannelYaml();
				if(!StringUtil.isEmpty(yaml))
				{
					cliData = kubeCtlService.delete_f(yaml, E_FILE_TYPE.YMAL);
					//if(cliData.result == false) throw new ExceptionEx("체널 삭제에 실패 했습니다.");
				}
				else
				{
					throw new ExceptionEx("체널 삭제정보가 없습니다.");
				}
				
				yaml = info.getKamelName();
				if(!StringUtil.isEmpty(yaml))
				{
					String namespace = info.getNamespace();
					cliData = kamelService.delete(yaml , namespace);
					//if(cliData.result == false) throw new ExceptionEx("카멜 삭제에 실패 했습니다.");
				}
				else
				{
					throw new ExceptionEx("카멜 등록정보가 없습니다.");
				}
				
				yaml = info.toSubscriptionYaml();
				if(!StringUtil.isEmpty(yaml))
				{
					cliData = kubeCtlService.delete_f(yaml, E_FILE_TYPE.YMAL);
					//if(cliData.result == false) throw new ExceptionEx("서브스크립션 삭제에 실패 했습니다.");
				}
				else
				{
					throw new ExceptionEx("서브스크립션 삭제정보가 없습니다.");
				}
			}
			catch(ExceptionEx e)
			{
				cliData.msg = e.getMessage();
			}
			
		}
		return cliData;
		
	}

	public FunctionVo functionInfoById(FunctionVo param) {
		return (FunctionVo) dao.selectOne("faas.function.functionInfoByName", param);
	}
	
	public void setFunctionStat(Map<String,Object> param)
	{
		dao.update("faas.function.setFunctionStat", param);
	}
	
	public List<Map<String, Object>> getFunctionListForAdmin(Map<String, Object> map) throws Exception {
		List<Map<String, Object>> list = dao.select("faas.function.listForAdmin", map);
		list = convertFunction(list);
		return list;
	}
	
	public int getFunctionCount(Map<String, Object> map) throws Exception {
		return (Integer) dao.selectOne("faas.function.totalCount", map);
	}

	public Map<String, Object> getFunctionInfo(Map<String, Object> map) throws Exception {
		Map<String, Object> resultMap = (Map<String, Object>) dao.selectOne("faas.function.infoForAdmin", map);
		resultMap = convertFunction(resultMap);
		return resultMap;
	}
	
	
	/* 
	 * 비활성은 kubenates 에서 service와 trigger를 제거한다.
	 * STAT_CD 는 DISABLE 로 셋한다.
	 */
	public boolean disableFunction(String namespace ,String function_id) throws Exception
	{
		ObjectMapper mapper = new ObjectMapper();
		Map<String,Object> param = new HashMap<>();
		param.put("namespace",namespace);
		param.put("function_id", function_id);
		
		try
		{
			List<Map<String, Object>> list =  getFunction(param);
			if(list.size() == 1)
			{
				String functionName = list.get(0).get("dsp_name").toString();
				List<Map<String, Object>> listTrigger = dao.select("faas.function.getTriggerBase", param);
				for(int i=0;i<listTrigger.size();i++)
				{
					Map<String, Object> mapTrigger = listTrigger.get(i);
					mapTrigger.put("trigger_stat_cd", FaaSCommCD.E_TRIGGER_STAT.INIT.toString());
					if(FaaSCommCD.E_TRIGGER_TYPE.HTTP.toString().toLowerCase().equals(mapTrigger.get("trigger_tp").toString()))
					{
						if(kongService.deleteApiGW(namespace, functionName, mapTrigger.get("api_name").toString()))
						{
							Map<String,Object> map = new HashMap<>();
		            		map.put("trigger_id", mapTrigger.get("trigger_id").toString());
		            		map.put("api_name", mapTrigger.get("api_name").toString());
		            		map.put("api_key", "");
		            		map.put("con_url", "");
		            		
		            		dao.update("faas.trigger.setHttpTrigger", map);
							dao.update("faas.function.setTriggerStat", mapTrigger);
						}
					}
					else
					{
						TriggerInfoBase info = JsonUtil.jsonDataMapper(mapper.writeValueAsString(mapTrigger) , TriggerInfoBase.class);
						KCLIData ret = deleteTriger(info);
						dao.update("faas.function.setTriggerStat", mapTrigger);
					}
				}
				
				Map<String, Object> mapFunction = list.get(0);
				FunctionInfo info = JsonUtil.jsonDataMapper(mapper.writeValueAsString(mapFunction) , FunctionInfo.class);
				if(deleteFunction(info) == 1)
				{
					String dspName = mapFunction.get("dsp_name").toString();
					targetServiceDelete(namespace, dspName);
					
					param.put("stat_cd", FaaSCommCD.E_STAT.DISABLE.toString());
					param.put("reg_step_cd", FaaSCommCD.E_REG_STEP.NONE.toString());
					setFunctionStat(param);
					return true;
				}
			}
		}
		catch(ExceptionEx ee)
		{
			return false;
		}
		throw new RuntimeException("함수 삭제 실패");
	}
	
	public List<Map<String,Object>> setUpload(MultipartHttpServletRequest multipartHttpServletRequest , String function_id) throws RuntimeException, IOException 
	{
    	Iterator<String> iterator = multipartHttpServletRequest.getFileNames();
    	MultipartFile multipartFile = null;
    	String originalFileName = null;
    	String originalFileExtension = null;
    	String storedFileName = null;
    	String targetFileName = null;
    	
    	File file = new File(uploadFilePath);
        if(file.exists() == false){
        	file.mkdirs();
        }
    	
    	List<Map<String,Object>> list = new ArrayList<Map<String,Object>>();
        Map<String, Object> listMap = null; 
        
        while(iterator.hasNext())
        {
        	multipartFile = multipartHttpServletRequest.getFile(iterator.next());
        	if(multipartFile.isEmpty() == false){
        		originalFileName = multipartFile.getOriginalFilename();
        		originalFileExtension = originalFileName.substring(originalFileName.lastIndexOf("."));
        		storedFileName = CommonUtils.getRandomString() + originalFileExtension;
        		targetFileName = uploadFilePath + storedFileName;
        		
        		file = new File(targetFileName);
        		multipartFile.transferTo(file);
        		
        		listMap = new HashMap<String,Object>();
        		listMap.put("function_id", function_id);
        		listMap.put("file_name", originalFileName);
        		listMap.put("upload_file_path", targetFileName);
        		list.add(listMap);
        	}
        }
        
        try
        {
	        Map<String, Object> param = new HashMap<>();
	        param.put("function_id", function_id);
	        List<Map<String,Object>> ret = dao.select("faas.function.selectUploadFile", param);
	        for(Map<String,Object> map : ret)
	        {
	        	String upload_file_path = map.get("upload_file_path").toString();
	        	File upload_file = new File(upload_file_path);
	        	if(upload_file.exists()) upload_file.delete();
	        }
	        dao.update("faas.function.deleteUploadFile", param);
	        
	        for(Map<String,Object> map : list)
	        {
	        	dao.update("faas.function.insertUploadFile", map);
	        }
        }
        catch(RuntimeException e)
        {
        	for(Map<String,Object> map : list)
	        {
        		String upload_file_path = map.get("upload_file_path").toString();
	        	File upload_file = new File(upload_file_path);
	        	if(upload_file.exists()) upload_file.delete();
	        }
        	
        	throw e;
        }
        
        return list;
	}
	
	public List<Map<String, Object>> getAttachFile(String function_id) throws Exception
	{
		Map<String, Object> param = new HashMap<>();
	    param.put("function_id", function_id);
	    List<Map<String,Object>> list = dao.select("faas.function.selectUploadFile", param);
	    
		return list;
	}
	
	public List<Map<String, Object>> getUploadFile(List<Map<String, Object>> fileList , String function_id) throws Exception
	{
		List<String> listExtractFile = new ArrayList<>();
		Map<String, Object> param = new HashMap<>();
	    param.put("function_id", function_id);
	    List<Map<String,Object>> list = dao.select("faas.function.selectUploadFile", param);
	    
	    for(Map<String,Object> map : list)
	    {
	    	String tempPath = uploadFilePath + CommonUtils.getRandomString(10);
	    	File tempfile = new File(tempPath);
	        if(tempfile.exists() == false){
	        	tempfile.mkdirs();
	        }
	        
	    	String zipfile = map.get("upload_file_path").toString();
	    	readEntryFromZipFile(zipfile,fileList);
	    }
	    
		return fileList;
	}
	
	public void readEntryFromZipFile(String zipfile ,List<Map<String, Object>> fileList) throws Exception
	{
		FileInputStream fileInputStream = null;
        FileOutputStream fileOutputStream = null;
        ZipInputStream zipInputStream = null;
        try {
            fileInputStream = new FileInputStream(zipfile);
            zipInputStream = new ZipInputStream(fileInputStream);
            ZipEntry zipEntry = null;

            while ((zipEntry = zipInputStream.getNextEntry()) != null) {
            	if(zipEntry.isDirectory()) continue;
            	
                int length = 0;
                int size = (int)zipEntry.getSize();
                if(size == -1) continue;
                
                byte[] buffer = new byte[size];
                zipInputStream.read(buffer);

                Map<String, Object> item = new HashMap<>();
                String fileName = zipEntry.getName();
                String fileContent = new String(buffer);
                
                item.put("fileName", fileName);
                item.put("fileContent", fileContent);
                fileList.add(item);

                zipInputStream.closeEntry();
                if(fileOutputStream != null)
                {
	                fileOutputStream.flush();
	                fileOutputStream.close();
                }
            }
        } catch (IOException e) {
            throw e;
        } catch (Exception e) {
        	throw e;
        }
        finally {
            try {
                if(zipInputStream != null)zipInputStream.close();
            } catch (IOException e) {
            	throw e;
            }
        }
	}
	
	public boolean unzipFile(String zipfile ,String extractPath, List<String> listExtractFile) throws Exception
	{
		byte[] buffer = new byte[1024*5];
		FileInputStream fileInputStream = null;
        FileOutputStream fileOutputStream = null;
        ZipInputStream zipInputStream = null;
        try {
            fileInputStream = new FileInputStream(zipfile);
            zipInputStream = new ZipInputStream(fileInputStream);
            ZipEntry zipEntry = null;

            while ((zipEntry = zipInputStream.getNextEntry()) != null) {
            	if(zipEntry.isDirectory()) continue;
            	
            	Path path = Paths.get(extractPath, zipEntry.getName());
            	String name = path.toString();
            	File file = new File(path.getParent().toString());
            	if(file.exists() == false){
                	file.mkdirs();
                }

            	fileOutputStream = new FileOutputStream(name);

                int length = 0;
                while ((length = zipInputStream.read(buffer)) != -1) {
                    fileOutputStream.write(buffer,0,length);
                }
                
                listExtractFile.add(name);

                zipInputStream.closeEntry();
                if(fileOutputStream != null)
                {
	                fileOutputStream.flush();
	                fileOutputStream.close();
                }
            }
            zipInputStream.close();
        } catch (IOException e) {
            throw e;
        } catch (Exception e) {
        	throw e;
        }
        finally {
            try {
                zipInputStream.closeEntry();
                fileOutputStream.flush();
                fileOutputStream.close();
                zipInputStream.close();
            } catch (IOException e) {
            	throw e;
            }
        }
        
		return true;
	}
	
	public List<Map<String, Object>> getEditableSource(String runtime , List<Map<String, Object>> sourceList) throws ExceptionNotExist
	{
		List<Map<String, Object>> targetList = new ArrayList<>();
		List<String> list = new ArrayList<>();
		switch(runtime)
		{
		case "nodejs":
			list.add("index.js");
			list.add("package.json");
			break;
		case "python":
			list.add("index.py");
			list.add("requirements.txt");
			break;
		default:
			throw new ExceptionNotExist("지원하지 않는 런타임입니다.");
		}
		
		for(Map<String, Object> map : sourceList)
		{
			if(map.containsKey("fileName"))
			{
				String fileName = map.get("fileName").toString();
				if(list.indexOf(fileName) > -1)
				{
					targetList.add(map);
				}
			}
		}
		
		return targetList;
	}
	
	//target 관련
	// 대상 DB 처리
	public Map<String,Object> saveTarget(Map<String,Object> param) throws Exception
	{
		Map<String,Object> paramMap = new HashMap<>();
		
		String referenceNamespace = param.get("namespace").toString();
		paramMap.put("namespace", referenceNamespace);
		paramMap.put("function_id", param.get("target_function_id"));
		
		Map<String,Object> map = (Map<String,Object>)dao.selectOne("faas.function.function", paramMap);
		
		if(map != null)
		{
			String target_url = String.format("http://%s.%s.svc.cluster.local", map.get("name"),param.get("namespace"));
			param.put("target_url", target_url);
			param.put("success", "N");
			
			if(dao.insert("faas.function.insertTarget", param) > 0)
			{
				Map<String,Object> functionMap = this.getFunction(param.get("function_id").toString());
				String function_id = functionMap.get("function_id").toString();
				String dsp_name = functionMap.get("dsp_name").toString();
				if(targetServiceApply(function_id, referenceNamespace, dsp_name))
					param.put("success", "Y");
			}
		}
		else
		{
			throw new ExceptionEx("함수를 찾지 못했습니다.");
		}
		return param;
	}
	
	public List<Map<String, Object>> getTargetList(Map<String,Object> param)
	{
		return dao.select("faas.function.selectTarget", param);
	}
	
	public List<Map<String, Object>> getCandidateTargetList(Map<String,Object> param)
	{
		return dao.select("faas.function.candidateTargetList", param);
	}
	
	public int deleteTarget(Map<String,Object> param) throws Exception
	{
		String referenceNamespace = param.get("namespace").toString();
		
		if(dao.update("faas.function.deleteTarget", param) > 0)
		{
			Map<String,Object> functionMap = this.getFunction(param.get("function_id").toString());
			String function_id = functionMap.get("function_id").toString();
			String dsp_name = functionMap.get("dsp_name").toString();
			if(targetServiceApply(function_id, referenceNamespace, dsp_name))
			{
				return 1;
			}
		}
		return 0;
	}
	// 대상 DB 처리
	
	private String getTargetServiceName(String referenceNamespace, String dspName)
	{
		return String.format("%s%s-%s", targetServicePrefix, referenceNamespace, dspName);
	}
	
	private String getTargetServiceUrl(String referenceNamespace, String dspName)
	{
		return String.format("http://%s.%s.svc.cluster.local", getTargetServiceName(referenceNamespace, dspName) , targetServiceNamespace);
	}
	
	private Object getTargetServiceEnvObject(String referenceNamespace, String dspName)
	{
		String targetServiceUrl = getTargetServiceUrl(referenceNamespace, dspName);
		Map<String,Object> param = new HashMap<>();
		
		param.put("name", "TARGET_SERVICE_ENDPOINT");
		param.put("value", targetServiceUrl);
		
		return param;
	}
	
	private boolean targetServiceApply(String function_id, String referenceNamespace, String dspName) 
	{
		KCLIData cliData = new KCLIData();
		try
		{
			String content;
			Map<String,Object> param = new HashMap<>();
			
			param.put("name", getTargetServiceName(referenceNamespace, dspName));
			param.put("namespace", targetServiceNamespace);
			param.put("image", targetServiceImg);
			param.put("success_target_endpoins", getTargetEndpointEnvString(function_id,FaaSCommCD.E_TARGET_RESULT.success));
			param.put("fail_target_endpoins", getTargetEndpointEnvString(function_id,FaaSCommCD.E_TARGET_RESULT.fail));
			param.put("dockerRegistrySecret", dockerRegistrySecret);
			
			targetServiceDelete(referenceNamespace, dspName);
			
			content = getTargetServiceYaml(param);
			cliData = kubeCtlService.apply_f(content, E_FILE_TYPE.YMAL);
			if(cliData.result == false) throw new ExceptionCreateService("");
		}
		catch(Exception ee)
		{
		    return false;
		}
		return true;
	}
	
	private void targetServiceDelete(String referenceNamespace, String dspName) throws Exception
	{
		KCLIData cliData = new KCLIData();
		try
		{
			String content;
			Map<String,Object> param = new HashMap<>();
			param.put("name", getTargetServiceName(referenceNamespace, dspName));
			param.put("namespace", targetServiceNamespace);
			param.put("image", targetServiceImg);
			param.put("dockerRegistrySecret", dockerRegistrySecret);

			content = getTargetServiceYaml(param);
			cliData = kubeCtlService.delete_f(content, E_FILE_TYPE.YMAL);
			if(cliData.result == false) throw new ExceptionCreateService("");
		}
		catch( Exception ee)
		{
		    //throw ee;
		}
	}
	
	private String getTargetServiceYaml(Map<String,Object> param) throws IOException
	{
		URL source = Resources.getResource("yaml/target_service.yaml");
		String yaml = Resources.toString(source, StandardCharsets.UTF_8);
		
		Handlebars hb = new Handlebars();
		Template template = hb.compileInline(yaml);
		return template.apply(param);
	}
	
	private String getTargetEndpointEnvString(String function_id , FaaSCommCD.E_TARGET_RESULT result)
	{
		StringBuffer sb = new StringBuffer();
		Map<String,Object> param = new HashMap<>();
		param.put("function_id", function_id);
		param.put("target_result_cd", result.toString());
		
		List<Map<String,Object>> list = dao.select("faas.function.selectTargetEnv", param);
		for(Map<String,Object> map : list)
		{
			String str = map.get("target_url").toString();
			if(!"".equals(sb.toString())) sb.append(",");
			sb.append(str);
		}
		
		return sb.toString();		
	}
	//target 관련
	
	public List<Map<String, Object>> getProjectList(Map<String,Object> params)
	{
		return dao.select("faas.function.selectProjects", params);
	}
	
}
