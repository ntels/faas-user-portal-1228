package kr.co.ntels.faas.function.service.dynamicDB;

import java.util.HashMap;
import java.util.Map;

import org.springframework.stereotype.Component;

import kr.co.ntels.faas.function.model.api.trigger.TriggerInfoFactory;
import kr.co.ntels.faas.userException.ExceptionEx;

/**
  * @FileName : DynamicDBFactory.java
  * @Project : faas-user-portal
  * @Date : 2020. 5. 28. 
  * @작성자 : sig61 - 백광욱
  * @변경이력 :
  * @프로그램 설명 :
  */
@Component
public class DynamicDBFactory {
	
	public static class DBInfo {
		TriggerInfoFactory.E_TRRIGGER_TYPE dbType;
		String host;
		int port;
		String usr;
		String pwd;
		String database;
		Map<String,Object> paramMap = new HashMap<>();
 		
		public DBInfo() {}
		
		public TriggerInfoFactory.E_TRRIGGER_TYPE getDbType() {
			return dbType;
		}
		public void setDbType(TriggerInfoFactory.E_TRRIGGER_TYPE dbType) {
			this.dbType = dbType;
		}
		public String getHost() {
			return host;
		}
		public void setHost(String host) {
			this.host = host;
		}
		public int getPort() {
			return port;
		}
		public void setPort(int port) {
			this.port = port;
		}
		public String getUsr() {
			return usr;
		}
		public void setUsr(String usr) {
			this.usr = usr;
		}
		public String getPwd() {
			return pwd;
		}
		public void setPwd(String pwd) {
			this.pwd = pwd;
		}
		public String getDatabase() {
			return database;
		}
		public void setDatabase(String database) {
			this.database = database;
		}
		
		public void addParam(String key, Object value)
		{
			paramMap.put(key, value);
		}
		
		public Object getParamValue(String key)
		{
			if(paramMap.containsKey(key))
			{
				return paramMap.get(key);
			}
			
			return "";
		}
	}
	

	public IDynamicDB getDynamicDB(DBInfo info) throws Exception
	{
		switch(info.getDbType())
		{
		case MONGODB:	return new DynamicDBMongo(info);
		case POSTGRESQL: return new DynamicDBPostgresJdbc(info);
		case MARIADB: return new DynamicDBMariaJdbc(info);
		default: throw new ExceptionEx("[개발참고] : 지원하지 않는 DB 유형입니다.");
		}
	}
}
