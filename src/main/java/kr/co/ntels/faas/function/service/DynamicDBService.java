package kr.co.ntels.faas.function.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import kr.co.ntels.faas.function.service.dynamicDB.DynamicDBFactory;
import kr.co.ntels.faas.function.service.dynamicDB.IDynamicDB;

/**
  * @FileName : DynamicDBService.java
  * @Project : faas-user-portal
  * @Date : 2020. 5. 27. 
  * @작성자 : sig61 - 백광욱
  * @변경이력 :
  * @프로그램 설명 :
  */
@Service
public class DynamicDBService {
	
	@Autowired
	DynamicDBFactory dynamicDBFactory;
	
	public List<String> getListTableName(DynamicDBFactory.DBInfo info) throws Exception
	{
		IDynamicDB dynamicDB = dynamicDBFactory.getDynamicDB(info);
		return dynamicDB.getListTableName();
	}
	
	public Object triggerPreProcess(DynamicDBFactory.DBInfo info) throws Exception
	{
		IDynamicDB dynamicDB = dynamicDBFactory.getDynamicDB(info);
		return dynamicDB.triggerPreProcess();
	}

}
