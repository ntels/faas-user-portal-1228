/**
 * 
 */
package kr.co.ntels.faas.function.model.api.trigger.http;

import java.util.Map;

import kr.co.ntels.faas.common.apiCommon.util.JsonSearch;
import kr.co.ntels.faas.function.model.api.trigger.TriggerInfo;
import kr.co.ntels.faas.function.model.api.trigger.TriggerInfoFactory;

/**
  * @FileName : TriggerHTTPInfo.java
  * @Project : faas-portal
  * @Date : 2020. 2. 21. 
  * @작성자 : sig61 - 백광욱
  * @변경이력 :
  * @프로그램 설명 :
  */

public class TriggerHTTPInfo extends  TriggerInfo{

	@JsonSearch(path="$.api_name")
	String apiName;
	
	@JsonSearch(path="$.api_key")
	String apiKey;
	
	@JsonSearch(path="$.auth_yn")
	String authYN;
	
	@JsonSearch(path="$.con_url")
	String conUrl;
	
	public TriggerHTTPInfo() {}
	/**
	 * @param name
	 * @param namespace
	 */
	public TriggerHTTPInfo(String name, String namespace , Map<String, Object> param) {
		super(name, namespace,param);
		// TODO Auto-generated constructor stub
	}

	

	public String getApiName() {
		return apiName;
	}
	public void setApiName(String apiName) {
		this.apiName = apiName;
	}
	public String getApiKey() {
		return apiKey;
	}
	public void setApiKey(String apiKey) {
		this.apiKey = apiKey;
	}
	public String getAuthYN() {
		return authYN;
	}
	public void setAuthYN(String authYN) {
		this.authYN = authYN;
	}
	public String getConUrl() {
		return conUrl;
	}
	public void setConUrl(String conUrl) {
		this.conUrl = conUrl;
	}
	
	@Override
	public String getType() {
		return TriggerInfoFactory.E_TRRIGGER_TYPE.HTTP.toString();
	}

	@Override
	public String toChannelYaml() {
		return "";
	}
	
	@Override
	public String toSubscriptionYaml() {
		return "";
	}

	@Override
	public String toKamelSource() {
		return "";
	}

}
