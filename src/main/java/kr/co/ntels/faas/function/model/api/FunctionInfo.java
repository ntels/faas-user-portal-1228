/**
 * 
 */
package kr.co.ntels.faas.function.model.api;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Value;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import kr.co.ntels.faas.common.apiCommon.model.Constant;
import kr.co.ntels.faas.common.apiCommon.model.Constant.E_SERVICE_FILTER;
import kr.co.ntels.faas.common.apiCommon.util.JsonSearch;
import kr.co.ntels.faas.common.apiCommon.util.StringBufferUtil;
import kr.co.ntels.faas.function.model.api.trigger.TriggerInfo;
import kr.co.ntels.faas.userException.ExceptionEx;

/**
  * @FileName : FunctionInfo.java
  * @Project : faas-portal
  * @Date : 2020. 2. 18. 
  * @작성자 : sig61 - 백광욱
  * @변경이력 :
  * @프로그램 설명 :
  */

public class FunctionInfo {

	final String apiVersion = "serving.knative.dev/v1";
	final String kind = "Service";
	
	@Value("${docker.registry.secret}")
	private String DOCKER_REGISTRY_SECRET;
	
	@JsonSearch(path="$..metadata.name, $.name")
	String name="";
	
	@JsonSearch(path="$..metadata.annotations.ext_dsp_name , $.dsp_name")	 		
	String dspName="";
	
	@JsonSearch(path="$..metadata.namespace, $.namespace")	 	
	String namespace="";
	
	@JsonSearch(path="$..metadata.annotations.ext_runtime, $.runtime")
	String runtime="";
	
	@JsonSearch(path="$..metadata.annotations.ext_version, $.version")	 
	String version="";
	
	@JsonSearch(path="$..metadata.annotations.ext_creator")	 
	String creator="";
	
	@JsonSearch(path="$..metadata.annotations.ext_modifier")	 
	String modifier="";
	
	@JsonSearch(path="$..spec.template.spec.containers[*].image, $.img")	 
	String image="";
	
	@JsonSearch(path="$..status.url")	 
	String url="";
	
	@JsonSearch(path="$.env")	 
	String env="[]";
	
	@JsonSearch(path="$..metadata.annotations.ext_create_date")	 
	String create_date="";
	
	@JsonSearch(path="$..metadata.annotations.ext_modify_date")	 
	String modify_date="";
	
	@JsonSearch(path="$..latestReadyRevisionName , $.ref_name")	 
	String latestReadyRevisionName="";
	
	@JsonSearch(path="$..spec.template.spec.containerConcurrency , $.concur" , scope="function")
	String containerConcurrency;
	
	@JsonSearch(path="$..spec.template.spec.timeoutSeconds , $.timeout" , scope="function")
	String timeoutSeconds;
	
	@JsonSearch(path="$..spec.template.spec.containers[*].resources.limits.memory , $.mem" , scope="function")
	String limitsMemory;
	
	@JsonSearch(path="$..spec.template.spec.containers[*].resources.limits.cpu , $.cpu" , scope="function")
	String limitsCPU="100";
	
	List<TriggerInfo> listTrigger = new ArrayList<>();
	List<Map<String,String>> envList = new ArrayList<>();
	
	int triggerCnt;
	
	String dockerRegistrySecret = "";

	public String getEnv() {
		return env;
	}

	public void setEnv(String env) {
		this.env = env;
	}

	// getter,setter
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}
	
	public String getDspName() {
		return dspName;
	}

	public void setDspName(String dspName) {
		this.dspName = dspName;
	}

	public String getNamespace() {
		return namespace;
	}

	public void setNamespace(String namespace) {
		this.namespace = namespace;
	}

	public String getRuntime() {
		return runtime;
	}

	public void setRuntime(String runtime) {
		this.runtime = runtime;
	}

	public String getVersion() {
		return version;
	}

	public void setVersion(String version) {
		this.version = version;
	}

	public String getCreator() {
		return creator;
	}

	public void setCreator(String creator) {
		this.creator = creator;
	}

	public String getModifier() {
		return modifier;
	}

	public void setModifier(String modifier) {
		this.modifier = modifier;
	}

	public String getImage() {
		return image;
	}

	public void setImage(String image) {
		this.image = image;
	}

	public String getUrl() {
		return url;
	}

	public void setUrl(String url) {
		this.url = url;
	}

	public String getCreate_date() {
		return create_date;
	}

	public void setCreate_date(String create_date) {
		this.create_date = create_date;
	}

	public String getModify_date() {
		return modify_date;
	}

	public void setModify_date(String modify_date) {
		this.modify_date = modify_date;
	}
	
	public String getLatestReadyRevisionName() {
		return latestReadyRevisionName;
	}

	public void setLatestReadyRevisionName(String latestReadyRevisionName) {
		this.latestReadyRevisionName = latestReadyRevisionName;
	}

	
	public List<TriggerInfo> getListTrigger() {
		return listTrigger;
	}

	

	public void setListTrigger(List<TriggerInfo> listTrigger) {
		this.listTrigger = listTrigger;
	}
	
	public String getContainerConcurrency() {
		int ival = parseInt(containerConcurrency);
		if(ival < 0) ival = 0;
		return ival+"";
	}

	public void setContainerConcurrency(String containerConcurrency) {
		int ival = parseInt(containerConcurrency);
		if(ival < 0) ival = 0;
		this.containerConcurrency = ival+"";
	}

	public String getTimeoutSeconds() {
		int ival = parseInt(timeoutSeconds);
		if(ival < 300) ival = 300;
		return ival+"";
	}

	public void setTimeoutSeconds(String timeoutSeconds) {
		int ival = parseInt(timeoutSeconds);
		if(ival < 300) ival = 300;
		this.timeoutSeconds = ival+"";
	}

	public String getLimitsMemory() {
		return limitsMemory;
	}

	public void setLimitsMemory(String limitsMemory) {
		limitsMemory = limitsMemory.replaceAll("Mi", "");
		int ival = parseInt(limitsMemory);
		this.limitsMemory = ival+"";
	}

	public String getLimitsCPU() {
		int ival = parseInt(limitsMemory);
		this.limitsCPU = ival+"Mi";
		return limitsCPU;
	}

	public void setLimitsCPU(String limitsCPU) {
		this.limitsCPU = limitsCPU;
	}
	// getter,setter
	
	public void setDockerRegistrySecret(String dockerRegistrySecret) {
		this.dockerRegistrySecret = dockerRegistrySecret;
	}

	public String getDockerRegistrySecret() {
		return dockerRegistrySecret;
	}
	
	public String getCompartor(Constant.E_ORDER_TYPE type) // 정렬기준 
	{
		switch(type)
		{
		case CREATE_DATE:
			return create_date;
		case MODIFY_DATE:
			return modify_date;
		default:
			return name;
		}
	}
	
	public boolean isFilterMatch(Constant.E_SERVICE_FILTER filter) throws Exception
	{
		if(filter == E_SERVICE_FILTER.NONE) return true;
		if(runtime == null || runtime == "") return false;
		
		return filter.toString().toLowerCase().equals(runtime.toLowerCase());
	}
	
	public String toYamelString() throws Exception
	{
		StringBufferUtil sb = new StringBufferUtil();
		sb.append("apiVersion: @apiVersion@");
		sb.append("kind: @kind@");
		sb.append("metadata: ");
		sb.append("	name: @name@");
		sb.append("	namespace: @namespace@");
		sb.append("	annotations: ");
		//sb.append("			knative.ntels.ext.service: @yamel@");
		sb.append("		ext_dsp_name: @ext_dsp_name@");
		sb.append("		ext_runtime: @ext_runtime@");
		sb.append("		ext_version: @ext_version@");
		sb.append("		ext_creator: @ext_creator@");
		sb.append("		ext_create_date: @ext_create_date@");
		sb.append("		ext_modifier: @ext_modifier@");
		sb.append("		ext_modify_date: @ext_modify_date@");
		sb.append("		ext_trigger_index: @ext_trigger_index@");
		sb.append("		ext_trigger: @ext_trigger@");
		sb.append("spec:");
		sb.append("	template:");
		sb.append("		spec:");
		sb.append("			containerConcurrency: @containerConcurrency@");
		sb.append("			timeoutSeconds: @timeoutSeconds@");		
		sb.append("			containers:");
		sb.append("				- image: @image@");
		sb.append("				  imagePullPolicy: Always");
		sb.append("				  resources:");
		sb.append("				  	limits:");
		sb.append("				  		memory: @limitsMemory@");
		sb.append("				  		cpu: @limitsCPU@");
		sb.append("				  env: @env@");
		sb.append("			imagePullSecrets:");
		sb.append("				- name: @dockerRegistrySecret@");
		
		String str = sb.toString();
		str = str.replaceAll("@","@@");
		str = str.replaceAll("@apiVersion@", apiVersion)
				.replaceAll("@kind@", kind)
				.replaceAll("@name@", name)
				.replaceAll("@namespace@", namespace)
				.replaceAll("@ext_dsp_name@", dspName)
				.replaceAll("@ext_runtime@", runtime)
				.replaceAll("@ext_version@", version)
				.replaceAll("@ext_creator@", creator)
				.replaceAll("@ext_create_date@", create_date)
				.replaceAll("@ext_modifier@", modifier)
				.replaceAll("@ext_modify_date@", modify_date)
				.replaceAll("@ext_trigger_index@", getTriggerCnt())
				.replaceAll("@ext_trigger@", getTriggerNamesJson())
				.replaceAll("@image@", image)
				.replaceAll("@@containerConcurrency@@", containerConcurrency)  // int @@ @@ 로 처리
				.replaceAll("@@timeoutSeconds@@", timeoutSeconds)				// int 
				.replaceAll("@limitsMemory@", limitsMemory+"Mi")
				.replaceAll("@limitsCPU@", limitsCPU+"m")
				.replaceAll("@@env@@", env)
				.replaceAll("@dockerRegistrySecret@", dockerRegistrySecret);
		
		str = str.replaceAll("\t", " ");
		str = str.replaceAll("@", "\"");
		
		return str;
	}

	public String getTriggerCnt()
	{
		return String.format("%d", listTrigger.size());
	}
	
	public String getTriggerNamesJson() throws Exception
	{
		String str = "";
		try
		{		
			List<String> list = new ArrayList<>();
			for(TriggerInfo info : listTrigger)
			{
				list.add(info.getSubscriptionName());
			}
			str = new ObjectMapper().writeValueAsString(list);
			str = str.replaceAll("\"", "\\\\\\\\\""); // replace 할때 \\ 가 하나 사라진다. 이유를 모르겠다. 일단 \\" 으로 처리.
		}
		catch(JsonProcessingException e)
		{
			throw new ExceptionEx(e.getMessage());
		}
		return str;
	}
	
	public void addEnv(String key , String val)
	{
		Map<String,String> map = new HashMap<>();
		map.put("name", key);
		map.put("value", val);
		envList.add(map);
	}
	
	private int parseInt(String str)
	{
		return parseInt(str,0);
	}

	private int parseInt(String str,int iDefault)
	{
		int ret = iDefault;
		try
		{
			ret = Integer.parseInt(str);
		}
		catch(Exception e)
		{
			ret = iDefault;
		}
		return ret;
	}
	
}