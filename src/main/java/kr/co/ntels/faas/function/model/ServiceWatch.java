package kr.co.ntels.faas.function.model;

import kr.co.ntels.faas.common.apiCommon.util.JsonSearch;
import kr.co.ntels.faas.constant.FaaSCommCD;


public class ServiceWatch {
	
	public enum E_SERVICE_SOURCE {KUBE_WATCH , KUBE_REG}
	
	String source = E_SERVICE_SOURCE.KUBE_WATCH.toString();
	
	@JsonSearch(path="$..metadata.name")
	String name;
	
	@JsonSearch(path="$..metadata.namespace")
	String namespace;
	
	//Kube
	@JsonSearch(path="$.type")
	String type;
	
	@JsonSearch(path="$..status.conditions[0].status",scope="function")
	String status_configurationsReady;
	
	@JsonSearch(path="$..status.conditions[1].status",scope="function")
	String status_ready;
	
	@JsonSearch(path="$..status.conditions[2].status",scope="function")
	String status_routesReady;
	
	@JsonSearch(path="$..status.conditions[0].message",scope="function")
	String message_configurationsReady;
	
	@JsonSearch(path="$..status.conditions[1].message",scope="function")
	String message_ready;
	
	@JsonSearch(path="$..status.conditions[2].message",scope="function")
	String message_routesReady;
	
	@JsonSearch(path="$..status.conditions[0].reason")
	String reason_configurationsReady;
	
	@JsonSearch(path="$..status.conditions[1].reason")
	String reason_ready;
	
	@JsonSearch(path="$..status.conditions[2].reason")
	String reason_routesReady;
	
	
	
	
	String kubeStatus;
	
	String log;
	
	 @JsonSearch(path="$..latestReadyRevisionName")	 
	 String latestReadyRevisionName="";

	public String getLatestReadyRevisionName() {
		return latestReadyRevisionName;
	}

	public void setLatestReadyRevisionName(String latestReadyRevisionName) {
		this.latestReadyRevisionName = latestReadyRevisionName;
	}

	public String getSource() {
		return source;
	}

	public void setSource(String source) {
		this.source = source;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getNamespace() {
		return namespace;
	}

	public void setNamespace(String namespace) {
		this.namespace = namespace;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public void setStatus_configurationsReady(String status_configurationsReady) {
		this.status_configurationsReady = status_configurationsReady.toLowerCase();
		getKubeStatus();
	}

	public void setStatus_ready(String status_ready) {
		this.status_ready = status_ready.toLowerCase();
		getKubeStatus();
	}

	public void setStatus_routesReady(String status_routesReady) {
		this.status_routesReady = status_routesReady.toLowerCase();
		getKubeStatus();
	}

	public String getKubeStatus() {
		FaaSCommCD.E_STAT eStat = FaaSCommCD.E_STAT.REG;
		
		if("true".equals(status_configurationsReady) && "true".equals(status_ready) && "true".equals(status_routesReady))
			eStat = FaaSCommCD.E_STAT.NORMAL;
		else if ("false".equals(status_configurationsReady) && "false".equals(status_ready) && "false".equals(status_routesReady))
			eStat = FaaSCommCD.E_STAT.ERROR;
		//else if ("false".equals(status_configurationsReady) && "false".equals(status_ready))
			//eStat = FaaSCommCD.E_STAT.ERROR;

		this.kubeStatus = eStat.toString();
		return this.kubeStatus;
	}

	public void setKubeStatus(String kubeStatus) {
		this.kubeStatus = kubeStatus;
	}

	public String getStatus_configurationsReady() {
		return status_configurationsReady;
	}

	public String getStatus_ready() {
		return status_ready;
	}

	public String getStatus_routesReady() {
		return status_routesReady;
	}

	public void setMessage_configurationsReady(String message_configurationsReady) {
		this.message_configurationsReady = message_configurationsReady==null ? "":message_configurationsReady;
		getLog();
	}

	public void setMessage_ready(String message_ready) {
		this.message_ready = message_ready==null ? "":message_ready;
		getLog();
	}

	public void setMessage_routesReady(String message_routesReady) {
		this.message_routesReady = message_routesReady==null ? "":message_routesReady;
		getLog();
	}

	public String getLog() {
		String message_configurationsReady = this.message_configurationsReady==null ? "":this.message_configurationsReady;
		String message_ready = this.message_ready==null ? "":this.message_ready;
		String message_routesReady = this.message_routesReady==null ? "":this.message_routesReady;
		
		log = String.format("%s", message_configurationsReady);
		return FaaSCommCD.E_STAT.ERROR.toString().equals(kubeStatus)  ? log : "";
	}

	public String getReason_configurationsReady() {
		return reason_configurationsReady;
	}

	public void setReason_configurationsReady(String reason_configurationsReady) {
		this.reason_configurationsReady = reason_configurationsReady;
	}

	public String getReason_ready() {
		return reason_ready;
	}

	public void setReason_ready(String reason_ready) {
		this.reason_ready = reason_ready;
	}

	public String getReason_routesReady() {
		return reason_routesReady;
	}

	public void setReason_routesReady(String reason_routesReady) {
		this.reason_routesReady = reason_routesReady;
	}
	

}
