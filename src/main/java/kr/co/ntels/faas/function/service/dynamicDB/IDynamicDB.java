package kr.co.ntels.faas.function.service.dynamicDB;

import java.util.List;

/**
  * @FileName : IDynamicDB.java
  * @Project : faas-user-portal
  * @Date : 2020. 5. 27. 
  * @작성자 : sig61 - 백광욱
  * @변경이력 :
  * @프로그램 설명 :
  */
public interface IDynamicDB {
	
	public List<String> getListTableName() throws Exception;  // table 정보 혹은 Mongo는 collection 정보, 각 DB 혹은 모듈마다 필요한 요소 구현
	public Object triggerPreProcess() throws Exception; // 트리거 생성을 위한 초기화작업 또는 필요한 작업을 구현
	
}