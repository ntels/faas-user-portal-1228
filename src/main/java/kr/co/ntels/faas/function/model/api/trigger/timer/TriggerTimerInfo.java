/**
 * 
 */
package kr.co.ntels.faas.function.model.api.trigger.timer;

import java.io.IOException;
import java.net.URL;
import java.nio.charset.StandardCharsets;
import java.util.Map;

import com.google.common.io.Resources;

import kr.co.ntels.faas.common.apiCommon.util.JsonSearch;
import kr.co.ntels.faas.common.utils.CronUtil;
import kr.co.ntels.faas.function.model.api.trigger.TriggerInfo;
import kr.co.ntels.faas.function.model.api.trigger.TriggerInfoFactory;

/**
  * @FileName : TriggerTimerInfo.java
  * @Project : faas-portal
  * @Date : 2020. 2. 21. 
  * @작성자 : sig61 - 백광욱
  * @변경이력 :
  * @프로그램 설명 :
  */

public class TriggerTimerInfo extends  TriggerInfo{
	
	@JsonSearch(path="$.bas_date_hms")
	private String bas_date_hms;
	
	@JsonSearch(path="$.repeat_tp")
    private String repeat_tp;
	
	@JsonSearch(path="$.repeat_val")
    private String repeat_val;
	
	@JsonSearch(path="$.repeat_des")
    private String repeat_des;
	
	@JsonSearch(path="$.end_use_yn")
    private String end_use_yn;
	
	@JsonSearch(path="$.end_date_hms")
    private String end_date_hms;
	
	@JsonSearch(path="$.timer_val")
    private String timer_val;
	
	
	public TriggerTimerInfo() {}
	/**
	 * @param name
	 * @param namespace
	 */
	public TriggerTimerInfo(String name, String namespace , Map<String, Object> param) {
		super(name, namespace ,param);
		
		bas_date_hms = param.get("bas_date_hms").toString();
		repeat_tp = param.get("repeat_tp").toString();
		repeat_val = param.get("repeat_val").toString();
		repeat_des = param.get("repeat_des").toString();
		end_use_yn = param.get("end_use_yn").toString();
		end_date_hms = param.get("end_date_hms").toString();
		timer_val = param.get("timer_val").toString();
		
		bas_date_hms = bas_date_hms.replaceAll("-|:", "");
		end_date_hms = end_date_hms.replaceAll("-|:", "");
		timer_val = timer_val.replaceAll(" ", "+");
	}

	public String getType() {
		return TriggerInfoFactory.E_TRRIGGER_TYPE.TIMER.toString();
	}


	public String getBas_date_hms() {
		return bas_date_hms;
	}
	public void setBas_date_hms(String bas_date_hms) {
		this.bas_date_hms = bas_date_hms;
	}
	public String getRepeat_tp() {
		return repeat_tp;
	}
	public void setRepeat_tp(String repeat_tp) {
		this.repeat_tp = repeat_tp;
	}
	public String getRepeat_val() {
		return repeat_val;
	}
	public void setRepeat_val(String repeat_val) {
		this.repeat_val = repeat_val;
	}
	public String getRepeat_des() {
		return repeat_des;
	}
	public void setRepeat_des(String repeat_des) {
		this.repeat_des = repeat_des;
	}
	public String getEnd_use_yn() {
		return end_use_yn;
	}
	public void setEnd_use_yn(String end_use_yn) {
		this.end_use_yn = end_use_yn;
	}
	public String getEnd_date_hms() {
		return end_date_hms;
	}
	public void setEnd_date_hms(String end_date_hms) {
		this.end_date_hms = end_date_hms;
	}
	public String getTimer_val() {
		return timer_val;
	}
	public void setTimer_val(String timer_val) {
		this.timer_val = timer_val;
	}
	@Override
	public String toChannelYaml() {
		String str = super.toChannelYaml();
		return str;
	}

	@Override
	public String toSubscriptionYaml() {
		String str = super.toSubscriptionYaml();
		return str;
	}

	@Override
	public String toKamelSource() {

		String result = null;

		URL url = Resources.getResource("format/trigger-timer.groovy");
		try {
			result = Resources.toString(url, StandardCharsets.UTF_8);

			String baseDateString = bas_date_hms;
			String endDateString = end_date_hms;
			String cronString = timer_val;

			result = result.replaceAll("@@BASE_DATE_STRING@@", baseDateString)
					.replaceAll("@@END_DATE_STRING@@", endDateString)
					.replaceAll("@@CRON_STRING@@", cronString)
					.replaceAll("@@CHANNEL_NAME@@", this.getChannelName());


		} catch (IOException e) {
			e.printStackTrace();
		}

		return result;
	}

}
