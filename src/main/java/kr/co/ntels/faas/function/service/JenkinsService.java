package kr.co.ntels.faas.function.service;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URI;
import java.net.URL;
import java.nio.charset.StandardCharsets;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import kr.co.ntels.common.utils.CommonUtils;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import com.github.jknack.handlebars.Handlebars;
import com.github.jknack.handlebars.Template;
import com.google.common.io.Resources;
import com.offbytwo.jenkins.JenkinsServer;
import com.offbytwo.jenkins.model.JobWithDetails;

import kr.co.ntels.faas.dao.DPLDao;
import kr.co.ntels.faas.dao.FaasDao;
import kr.co.ntels.faas.function.model.api.trigger.TriggerInfo;
import kr.co.ntels.faas.gitlab.web.GitLabController;

@Service
public class JenkinsService {
	
	private @Value("${gitlab.url}") String GITLAB_URL;
	private @Value("${gitlab.sample.group.path}") String GITLAB_SAMPLE_GROUP_PATH;
	private @Value("${gitlab.admin.group.path}") String GITLAB_ADMIN_GROUP_PATH;
	
	private @Value("${docker.registry}") String DOCKER_REGISTRY;
	private @Value("${docker.registry.public}") String DOCKER_REGISTRY_PUBLIC;
	private @Value("${docker.username}") String DOCKER_USERNAME;
	private @Value("${docker.password}") String DOCKER_PASSWORD;
	
	private @Value("${private.repository.host}") String PRIVATE_REPOSITORY_HOST;
	private @Value("${npm.private.repository.url}") String NPM_PRIVATE_REPOSITORY_URL;
	private @Value("${npm.private.repository.isused}") String NPM_PRIVATE_REPOSITORY_ISUSED;
	private @Value("${pip.private.repository.url}") String PIP_PRIVATE_REPOSITORY_URL;
	private @Value("${pip.private.repository.isused}") String PIP_PRIVATE_REPOSITORY_ISUSED;
	
	@Value("${jenkins.info.static}") String jenkinsInfoStatic;
	@Value("${jenkins.url}") String jenkinsUrl;
	@Value("${jenkins.username}") String jenkinsUserName;
	@Value("${jenkins.password}") String jenkinsPassword;
	
	@Autowired
	FaasDao faasDao;
	@Autowired
	DPLDao dplDao;
	
	private final String USER_AGENT = "Mozilla/5.0";
	
	public Map<String, Object> getJenkinsAccessInfo(Map<String, Object> map) {
		Map<String, Object> returnMap = new HashMap<>();
		List<Map<String, Object>> resultList;
		
		if("Y".equals(jenkinsInfoStatic)) {
			returnMap.put("jenkins_url", jenkinsUrl);
			returnMap.put("jenkins_admin_id", jenkinsUserName);
			returnMap.put("jenkins_admin_pw", jenkinsPassword);
		} else {
			resultList = dplDao.select("faas.function.jenkinsAccessInfo", map);
			
			if(resultList.size() > 0) {
				returnMap = resultList.get(0);
			} else {
				returnMap.clear();
			}
		}
		
		return returnMap;
	}
	
	// Jenkins 빌드잡 존재 확인
	// successYn : 성공 여부, existenceYn : 빌드잡 존재 여부, existenceBuildJobName : 빌드잡 이름(빌드잡 존재 시), existenceBuildJobUrl : 빌드잡 네임(빌드잡 존재 시)
	public Map<String, Object> checkBuildJobExistence(Map<String, Object> dataMap) throws Exception {
		
		Map<String, Object> resultMap = new HashMap<String, Object>();
		String successYn = "n";
		String existenceYn = "n";
		String existenceBuildJobName = "";
		String existenceBuildJobUrl = "";
		
		String groupName;
		String projectName;
		String jenkins_url;
		String jenkins_admin_id;
		String jenkins_admin_pw;
		
		try {
			
			groupName = dataMap.get("namespace").toString();
			projectName = dataMap.get("name").toString();
			jenkins_url = dataMap.get("jenkins_url").toString();
			jenkins_admin_id = dataMap.get("jenkins_admin_id").toString();
			jenkins_admin_pw = dataMap.get("jenkins_admin_pw").toString();
			
			String buildJobName = groupName + "-" + projectName;
			String buildJobUrl = jenkins_url + "/job/" + buildJobName + "/api/json";
			
			HashMap<String, Object> responseMap = getHttpResponse(buildJobUrl);
			
			if(Integer.parseInt(responseMap.get("responseCode").toString()) == 200) {
				JSONObject json = new JSONObject(responseMap.get("responseBody").toString());
								
				existenceBuildJobName = json.getString("name").toString();
				existenceBuildJobUrl = json.getString("url").toString();
				
				existenceYn = "y";
			} else {
				existenceYn = "n";
			}
			
			successYn = "y";
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		resultMap.put("successYn", successYn);
		resultMap.put("existenceYn", existenceYn);
		resultMap.put("existenceBuildJobName", existenceBuildJobName);
		resultMap.put("existenceBuildJobUrl", existenceBuildJobUrl);
		
		return resultMap;
	}	
	
	// Jenkins 빌드잡 생성
	// successYn : 생성 성공 여부
	public Map<String, Object> createBuildJob(Map<String, Object> dataMap) throws Exception {
		
		Map<String, Object> resultMap = new HashMap<String, Object>();
		String successYn = "n";
		
		String groupName;
		String projectName;
		String jenkins_url;
		String jenkins_admin_id;
		String jenkins_admin_pw;
		
		try {
			
			groupName = dataMap.get("namespace").toString();
			projectName = dataMap.get("name").toString();
			jenkins_url = dataMap.get("jenkins_url").toString();
			jenkins_admin_id = dataMap.get("jenkins_admin_id").toString();
			jenkins_admin_pw = dataMap.get("jenkins_admin_pw").toString();
			
			String buildJobName = groupName + "-" + projectName;
			
			JenkinsServer jenkinsServer = new JenkinsServer(new URI(jenkins_url), jenkins_admin_id, jenkins_admin_pw);
			
			String configXml = createConfigXml(dataMap);
			
			Map<String, Object> checkMap = checkBuildJobExistence(dataMap);
			
			if(checkMap.get("successYn").toString().equals("y")) {
				if(checkMap.get("existenceYn").toString().equals("y")) {
					jenkinsServer.updateJob(buildJobName, configXml.toString(), true);
				} else {
					jenkinsServer.createJob(buildJobName, configXml.toString(), true);
				}
			}
			
			jenkinsServer.close();
			
			successYn = "y";
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		resultMap.put("successYn", successYn);
		
		return resultMap;
	}
	
	// Jenkins 빌드잡 실행
	// successYn : 실행 성공 여부
	public HashMap<String, Object> runBuildJob(Map<String, Object> dataMap) throws Exception {
		
		HashMap<String, Object> resultMap = new HashMap<String, Object>();
		String successYn = "n";
		int lastBuildNumber = 0;
		
		String groupName;
		String projectName;
		String jenkins_url;
		String jenkins_admin_id;
		String jenkins_admin_pw;
		
		try {
			
			groupName = dataMap.get("namespace").toString();
			projectName = dataMap.get("name").toString();
			jenkins_url = dataMap.get("jenkins_url").toString();
			jenkins_admin_id = dataMap.get("jenkins_admin_id").toString();
			jenkins_admin_pw = dataMap.get("jenkins_admin_pw").toString();
			
			String buildJobName = groupName + "-" + projectName;
			
			JenkinsServer jenkinsServer = new JenkinsServer(new URI(jenkins_url), jenkins_admin_id, jenkins_admin_pw);
			
			JobWithDetails buildJob = jenkinsServer.getJob(buildJobName);
			
			lastBuildNumber = buildJob.getLastBuild().getNumber();
			
			buildJob.build(true);
			
			int maxLoop = 100;
			while(maxLoop-- > 0)
			{
				buildJob = jenkinsServer.getJob(buildJobName);
				int buildNumber = buildJob.getLastBuild().getNumber();
				if(lastBuildNumber < buildNumber)
				{
					lastBuildNumber = buildNumber;
					break;
				}
				Thread.sleep(100);
			}
			
			jenkinsServer.close();
			
			successYn = "y";
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		resultMap.put("lastBuildNumber", lastBuildNumber);
		resultMap.put("successYn", successYn);
		
		return resultMap;
	}

	// 빌드잡 구성 파일 생성
	public String createConfigXml(Map<String, Object> dataMap) throws Exception {
		
		String resultXml = "";
		
		String groupName = dataMap.get("namespace").toString();
		String projectName = dataMap.get("name").toString();
		String configSourceName = dataMap.get("runtime").toString() + ".xml";
		String appSourceName = String.format("%s-%s-%s", dataMap.get("runtime").toString(), dataMap.get("version").toString(), "app.txt");
		String dockerImageName = String.format("%s-%s-%s", groupName , projectName , CommonUtils.getRandomString(5));
		String dockerImageTag = "latest";
		
		String baseImage = "";
		
		switch (dataMap.get("runtime").toString().toLowerCase()) {
			case "nodejs":
				baseImage = String.format("%s/%s:%s", DOCKER_REGISTRY_PUBLIC, "node", dataMap.get("version").toString());
				break;
			case "python":
				baseImage = String.format("%s/%s:%s", DOCKER_REGISTRY_PUBLIC, "python", dataMap.get("version").toString());
				break;
			default:
				throw new Exception("is not supported runtime type");
		}
		
		//sig61 Test 하드코딩 다시작업요함.
		Map<String, Object> param = new HashMap<>();
		param.put("namespace", groupName);
		param.put("name", projectName);
		param.put("img", String.format("%s/%s:%s", DOCKER_REGISTRY, dockerImageName, dockerImageTag));
		faasDao.update("faas.dbSync.setFunctionValue", param);
		//sig61 Test 하드코딩 다시작업요함.
		
		URL appSourceUrl = Resources.getResource("jenkinsConfigXml/" + appSourceName);
		String appSource = Resources.toString(appSourceUrl, StandardCharsets.UTF_8);
		
		Map<String, Object> xmlDataMap = new HashMap<>();
		xmlDataMap.put("GITLAB_URL", GITLAB_URL);
		xmlDataMap.put("groupName", groupName);
		xmlDataMap.put("projectName", projectName);
		xmlDataMap.put("baseImage", baseImage);
		xmlDataMap.put("GITLAB_ADMIN_GROUP_PATH", GITLAB_ADMIN_GROUP_PATH);
		xmlDataMap.put("DOCKER_REGISTRY", DOCKER_REGISTRY);
		xmlDataMap.put("DOCKER_USERNAME", DOCKER_USERNAME);
		xmlDataMap.put("DOCKER_PASSWORD", DOCKER_PASSWORD);
		xmlDataMap.put("dockerImageName", dockerImageName);
		xmlDataMap.put("dockerImageTag", dockerImageTag);
		xmlDataMap.put("NPM_PRIVATE_REPOSITORY_ISUSED", Boolean.valueOf(NPM_PRIVATE_REPOSITORY_ISUSED.toLowerCase()));			
		xmlDataMap.put("PIP_PRIVATE_REPOSITORY_ISUSED", Boolean.valueOf(PIP_PRIVATE_REPOSITORY_ISUSED.toLowerCase()));
		xmlDataMap.put("NPM_PRIVATE_REPOSITORY_URL", NPM_PRIVATE_REPOSITORY_URL);
		xmlDataMap.put("PIP_PRIVATE_REPOSITORY_URL", PIP_PRIVATE_REPOSITORY_URL);
		xmlDataMap.put("PRIVATE_REPOSITORY_HOST", PRIVATE_REPOSITORY_HOST);
		xmlDataMap.put("appSource", appSource);
		
		URL url = Resources.getResource("jenkinsConfigXml/" + configSourceName);
		String xmlConfig = Resources.toString(url, StandardCharsets.UTF_8);
		
		Handlebars hb = new Handlebars();
		Template template = hb.compileInline(xmlConfig);
		resultXml = template.apply(xmlDataMap);
		
		return resultXml;
	}
	
	// Http GET 요청 및 응답 가져오기
	// responseCode : 응답 코드, responseBody : 응답 바디
	private HashMap<String, Object> getHttpResponse(String targetUrl) throws Exception {
		
		HashMap<String, Object> resultMap = new HashMap<String, Object>();
		
		URL url = new URL(targetUrl);
		
		HttpURLConnection urlConnection = (HttpURLConnection)url.openConnection();
		
		urlConnection.setRequestMethod("GET"); // optional default is GET
		urlConnection.setRequestProperty("User-Agent", USER_AGENT); // add request header
		
		int responseCode = urlConnection.getResponseCode();
		StringBuffer responseBody = new StringBuffer();
		
		if(responseCode == 200) {
			BufferedReader input = new BufferedReader(new InputStreamReader(urlConnection.getInputStream()));
			String inputLine;
			
			while ((inputLine = input.readLine()) != null) {
				responseBody.append(inputLine + "\n");
			}
			
			input.close(); // print result
		}
		
		resultMap.put("responseCode", responseCode);
		resultMap.put("responseBody", responseBody);
		
		return resultMap;
	}

}