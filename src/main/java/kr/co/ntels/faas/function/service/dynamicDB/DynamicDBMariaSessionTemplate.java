package kr.co.ntels.faas.function.service.dynamicDB;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.sql.DataSource;

import org.json.JSONObject;
import org.mybatis.spring.SqlSessionFactoryBean;
import org.mybatis.spring.SqlSessionTemplate;
import org.springframework.core.io.Resource;
import org.springframework.core.io.support.PathMatchingResourcePatternResolver;

import com.zaxxer.hikari.HikariConfig;
import com.zaxxer.hikari.HikariDataSource;

import kr.co.ntels.common.utils.CommonUtils;
import kr.co.ntels.faas.userException.ExceptionDBPreProcess;


/*
 * SqlSessionTemplate 으로 연결해서 테스트시 DB 연결이 해결되지 않는 문제가 있어 DynamicDBMariaJdbc.java로 코딩 다시함. 
 * 연결 해제가 해결되면 사용가능함.
 */

public class DynamicDBMariaSessionTemplate extends DynamicDB {

	public DynamicDBMariaSessionTemplate(DynamicDBFactory.DBInfo info)
	{
		this.info = info;
	}

	@Override
	protected List<String> getListTableNameDB() throws Exception 
	{
		Map<String,String> param = new HashMap<>();
		List<String> list = new ArrayList<>();
		
		SqlSessionTemplate sqlSessionTemplate = getSqlSessionTemplate();

		param.put("database", info.getDatabase());
		sqlSessionTemplate.update("faas.dynamicDBMaria.flush");
		List<Map<String, Object>> retList = sqlSessionTemplate.selectList("faas.dynamicDBMaria.getTableList", param);
		
		for(Map<String, Object> map : retList)
		{
			String tableName = map.get("tablename").toString();
			list.add(tableName);
		}
		
		sqlSessionTemplate = null;
		
		return list;
	}
	
	@Override
	public Object triggerPreProcess() throws Exception {
		Map<String, Object> returnMap = new HashMap<>();
		
		try {
			SqlSessionTemplate sqlSessionTemplate = getSqlSessionTemplate();
			
			String server_id = sqlSessionTemplate.selectOne("faas.dynamicDBMaria.getServerId").toString();
			
			if(!String.valueOf(server_id).toLowerCase().equals("null") && !String.valueOf(server_id).equals("")) {
				returnMap.put("server_id", server_id);
			} else {
				throw new ExceptionDBPreProcess("failed to get server_id");
			}
		} catch(Exception e) {
			throw new ExceptionDBPreProcess("db trigger pre process fail");
		}
		
		return returnMap;
	}
	
	private SqlSessionTemplate getSqlSessionTemplate() throws Exception
	{
		DataSource dataSource = new HikariDataSource(getHikariConfig());
		SqlSessionFactoryBean sqlSessionFactoryBean = new SqlSessionFactoryBean();
		sqlSessionFactoryBean.setDataSource(dataSource);
		sqlSessionFactoryBean.setMapperLocations(new PathMatchingResourcePatternResolver().getResources("classpath:/mapper/dynamicDBMaria.xml"));
		
		Resource myBatisConfig = new PathMatchingResourcePatternResolver().getResource("classpath:mybatis-config.xml");
		sqlSessionFactoryBean.setConfigLocation(myBatisConfig);
		
		return new SqlSessionTemplate(sqlSessionFactoryBean.getObject());
	}
	
	private HikariConfig getHikariConfig() throws Exception
	{
		HikariConfig config = new HikariConfig();
		config.setDriverClassName("org.mariadb.jdbc.Driver");
		config.setJdbcUrl(String.format("jdbc:mariadb://%s:%s/%s" , info.getHost() , info.getPort() , info.getDatabase()));
		config.setUsername(info.getUsr());
		config.setPassword(info.getPwd());
		config.setMaximumPoolSize(1);
		
		return config;
	}
	
	private String getDbServerId() throws Exception {
		return "";
	}
}