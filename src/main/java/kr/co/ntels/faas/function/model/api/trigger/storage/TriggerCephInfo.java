/**
 * 
 */
package kr.co.ntels.faas.function.model.api.trigger.storage;

import java.util.Map;

import kr.co.ntels.faas.function.model.api.trigger.TriggerInfo;
import kr.co.ntels.faas.function.model.api.trigger.TriggerInfoFactory;

/**
  * @FileName : TriggerCephInfo.java
  * @Project : faas-portal
  * @Date : 2020. 2. 21. 
  * @작성자 : sig61 - 백광욱
  * @변경이력 :
  * @프로그램 설명 :
  */

public class TriggerCephInfo extends  TriggerInfo{

	public TriggerCephInfo() {}
	/**
	 * @param name
	 * @param namespace
	 */
	public TriggerCephInfo(String name, String namespace , Map<String, Object> param) {
		super(name, namespace,param);
		// TODO Auto-generated constructor stub
	}

	@Override
	public String getType() {
		// TODO Auto-generated method stub
		return TriggerInfoFactory.E_TRRIGGER_TYPE.CEPH.toString();
	}

	@Override
	public String toChannelYaml() {
		String str = super.toChannelYaml();
		return str;
	}

	@Override
	public String toSubscriptionYaml() {
		String str = super.toSubscriptionYaml();
		return str;
	}

	@Override
	public String toKamelSource() {
		// TODO Auto-generated method stub
		return null;
	}

}
