package kr.co.ntels.faas.function.model;

import kr.co.ntels.faas.common.apiCommon.util.JsonSearch;
import kr.co.ntels.faas.constant.FaaSCommCD;

/**
  * @FileName : JenkinsLog.java
  * @Project : faas-user-portal
  * @Date : 2020. 5. 8. 
  * @작성자 : sig61 - 백광욱
  * @변경이력 :
  * @프로그램 설명 :
  */
public class JenkinsLog {
	
	String source="JENKINS";
	
	String namespace;
	String name;
	
	@JsonSearch(path="$.._links.self.href")
	String href;
	
	@JsonSearch(path="$..id")
	String id;
	
	@JsonSearch(path="$..name",scope="function")
	String stage;
	
	@JsonSearch(path="$..status")
	String status;
	
	@JsonSearch(path="$..error")
	String error;
	
	@JsonSearch(path="$..log.href")
	String logHref;
	
	String log;
	String step;
	
	public String getSource() {
		return source;
	}

	public void setSource(String source) {
		this.source = source;
	}
	
	public String getNamespace() {
		return namespace;
	}
	public void setNamespace(String namespace) {
		this.namespace = namespace;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getHref() {
		return href;
	}
	public void setHref(String href) {
		this.href = href;
	}
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getStage() {
		return stage;
	}
	public void setStage(String stage) {
		this.stage = stage;
		setStep(stage);
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public String getError() {
		return error;
	}
	public void setError(String error) {
		this.error = error;
	}
	public String getLogHref() {
		return logHref;
	}

	public void setLogHref(String logHref) {
		this.logHref = logHref;
	}

	public String getLog() {
		return log;
	}
	public void setLog(String log) {
		this.log = log;
	}
	public String getStep() {
		
		if("CHECK OUT".equals(step) || FaaSCommCD.E_REG_STEP.STEP2.toString().equals(step)) this.step = FaaSCommCD.E_REG_STEP.STEP2.toString();
		else if("BUILD".equals(step) || FaaSCommCD.E_REG_STEP.STEP3.toString().equals(step)) this.step = FaaSCommCD.E_REG_STEP.STEP3.toString();
		else if("DEPLOY".equals(step) || FaaSCommCD.E_REG_STEP.STEP4.toString().equals(step)) this.step = FaaSCommCD.E_REG_STEP.STEP4.toString();
		else this.step = FaaSCommCD.E_REG_STEP.NONE.toString();
		
		return step;
	}
	public void setStep(String step) {
		if("CHECK OUT".equals(step) || FaaSCommCD.E_REG_STEP.STEP2.toString().equals(step)) this.step = FaaSCommCD.E_REG_STEP.STEP2.toString();
		else if("BUILD".equals(step) || FaaSCommCD.E_REG_STEP.STEP3.toString().equals(step)) this.step = FaaSCommCD.E_REG_STEP.STEP3.toString();
		else if("DEPLOY".equals(step) || FaaSCommCD.E_REG_STEP.STEP4.toString().equals(step)) this.step = FaaSCommCD.E_REG_STEP.STEP4.toString();
		else this.step = FaaSCommCD.E_REG_STEP.NONE.toString();
	}
	
	public boolean isSame(JenkinsLog other)
	{
		return this.stage.equals(other.stage)
				&& this.status.equals(other.status)
				;
	}

}
