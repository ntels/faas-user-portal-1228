/**
 * 
 */
package kr.co.ntels.faas.function.model.api.trigger;

import java.util.Map;

import org.springframework.stereotype.Component;

import kr.co.ntels.faas.common.apiCommon.model.APIJsonData;
import kr.co.ntels.faas.common.apiCommon.util.JsonUtil;
import kr.co.ntels.faas.function.model.api.trigger.db.TriggerDBMariadbInfo;
import kr.co.ntels.faas.function.model.api.trigger.db.TriggerDBMongodbInfo;
import kr.co.ntels.faas.function.model.api.trigger.db.TriggerDBPostgresInfo;
import kr.co.ntels.faas.function.model.api.trigger.storage.TriggerCephInfo;
import kr.co.ntels.faas.function.model.api.trigger.timer.TriggerTimerInfo;
import kr.co.ntels.faas.userException.ExceptionEx;

/**
  * @FileName : TriggerInfoFactory.java
  * @Project : faas-portal
  * @Date : 2020. 2. 24. 
  * @작성자 : sig61 - 백광욱
  * @변경이력 :
  * @프로그램 설명 :
  */

@Component
public class TriggerInfoFactory {
	
	public enum E_TRRIGGER_TYPE { HTTP , POSTGRESQL , MONGODB , MARIADB , CEPH , TIMER };
	
	public E_TRRIGGER_TYPE getTiggerType(String type) throws Exception
	{
		for(E_TRRIGGER_TYPE item : E_TRRIGGER_TYPE.values())
		{
			String s = item.toString();
			if(s.equals(type))
				return item;
		}
		
		throw new ExceptionEx("지원하지 않는 트리거입니다.");
	}
	
	public TriggerInfo createTriggerInfo(TriggerInfoFactory.E_TRRIGGER_TYPE type, String name, String namespace, Map<String, Object> param) throws Exception
	{
		switch(type)
		{
		// case HTTP: 			return new TriggerHTTPInfo(name,namespace,param);
		case POSTGRESQL: 	return new TriggerDBPostgresInfo(name,namespace,param);
		case MONGODB: 		return new TriggerDBMongodbInfo(name,namespace,param);
		case MARIADB: 		return new TriggerDBMariadbInfo(name,namespace,param);
		case CEPH: 			return new TriggerCephInfo(name,namespace,param);
		case TIMER: 		return new TriggerTimerInfo(name,namespace,param);
		}
		
		throw new ExceptionEx("지원하지 않는 트리거입니다.");
	}
	
	public TriggerInfo createTriggerInfoByJson(TriggerInfoFactory.E_TRRIGGER_TYPE type , APIJsonData json) throws Exception
	{
		TriggerInfo info;
		
		switch(type)
		{
		// case HTTP: 			return JsonUtil.jsonDataMapper(json, TriggerHTTPInfo.class); 
		case POSTGRESQL: 	return JsonUtil.jsonDataMapper(json, TriggerDBPostgresInfo.class);
		case MONGODB: 		return JsonUtil.jsonDataMapper(json, TriggerDBMongodbInfo.class);
		case CEPH: 			return JsonUtil.jsonDataMapper(json, TriggerCephInfo.class);
		case TIMER: 		return JsonUtil.jsonDataMapper(json, TriggerTimerInfo.class); 
		case MARIADB: 		return JsonUtil.jsonDataMapper(json, TriggerDBMariadbInfo.class); 
		}

		throw new ExceptionEx("지원하지 않는 트리거입니다.");
	}
	
}
