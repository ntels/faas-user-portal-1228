package kr.co.ntels.faas.function.web;

import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.security.KeyManagementException;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.cert.X509Certificate;
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.PostConstruct;
import javax.net.ssl.SSLContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.http.conn.ssl.SSLConnectionSocketFactory;
import org.apache.http.conn.ssl.SSLContexts;
import org.apache.http.conn.ssl.TrustStrategy;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.tomcat.util.http.fileupload.impl.FileSizeLimitExceededException;
import org.gitlab4j.api.models.Commit;
import org.gitlab4j.api.models.Diff;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.http.client.HttpComponentsClientHttpRequestFactory;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.multipart.MultipartHttpServletRequest;

import com.fasterxml.jackson.databind.ObjectMapper;

import kr.co.ntels.common.web.HubpopController;
import kr.co.ntels.faas.constant.FaaSCommCD;
import kr.co.ntels.faas.function.model.api.trigger.TriggerInfoBase;
import kr.co.ntels.faas.function.service.FunctionService;
import kr.co.ntels.faas.function.service.JenkinsService;
import kr.co.ntels.faas.function.service.KongService;
import kr.co.ntels.faas.gitlab.service.GitLabService;
import kr.co.ntels.faas.gitlab.web.GitLabController;
import kr.co.ntels.faas.log.service.LogService;
import kr.co.ntels.faas.trigger.service.TriggerService;
import kr.co.ntels.faas.userException.ExceptionDBPreProcess;
import kr.co.ntels.faas.userException.ExceptionEx;

@Controller
public class FunctionController {
	
	@Value("${kubernetes.grafana.home}")	String grafanaKubernetesHome;
	@Value("${kubernetes.api.authorization.type}")	String authorizationType;
	@Value("${kubernetes.api.authorization.token}")	String authorizationToken;
	@Autowired FunctionService service;
	@Autowired GitLabController gitLabController;
	@Autowired GitLabService gitLabService;
	@Autowired JenkinsService jenkinsService;
	@Autowired TriggerService triggerService;
	@Autowired LogService logService;
	@Autowired KongService kongService;
	
	@PostConstruct
	public void init()
	{
		if("/".equals(grafanaKubernetesHome.substring(grafanaKubernetesHome.length()-1)))
			grafanaKubernetesHome = grafanaKubernetesHome.substring(0,grafanaKubernetesHome.length()-1);
	}
	
	@ResponseBody
	@RequestMapping("/service/getFunctionList.json")
	public String getFunctionList(HttpServletRequest request, HttpServletResponse response, @RequestParam String param) throws Exception
	{
		Map<String,Object> paramMap = new ObjectMapper().readValue(param, Map.class);
		
		String namespace = FaaSCommCD.getNamespace(request);
		
		int curpage = Integer.parseInt(paramMap.get("curpage").toString());
		int limit = Integer.parseInt(paramMap.get("limit").toString());
		int offset = service.getOffsetInPage(curpage , limit);
		
		String sch_runtime = paramMap.get("sch_runtime").toString();
		String order_by = paramMap.get("order_by").toString();
		
		paramMap.put("namespace", namespace);
		paramMap.put("limit", limit);
		paramMap.put("offset", offset);
		paramMap.put("sch_runtime", sch_runtime);
		paramMap.put("order_by", order_by);
		
		List<Map<String, Object>> functionList = service.getFunctionList(paramMap);
		
		Map<String,Object> returnMap = new HashMap<>();
		
		int totalCount = 0;
		if(functionList.size() > 0) totalCount = Integer.parseInt(functionList.get(0).get("total_count").toString());
		
		returnMap.put("list", functionList);
		returnMap.put("totalCount", totalCount);
		returnMap.put("curpage", curpage + "");
		returnMap.put("lastpage", service.getLastPage(totalCount, limit));
		
		ObjectMapper mapper = new ObjectMapper();
		
		return mapper.writeValueAsString(returnMap);
	}
	
	@ResponseBody
	@RequestMapping("/service/getFunction.json")
	public String getFunction(HttpServletRequest request, HttpServletResponse response, @RequestParam String param) throws Exception {
		Map<String,Object> paramMap = new ObjectMapper().readValue(param, Map.class);
		Map<String,Object> dataMap = new HashMap<String, Object>();
		Map<String,Object> returnMap = new HashMap<String, Object>();
		Map<String,Object> config = new HashMap<String, Object>();
		String namespace = FaaSCommCD.getNamespace(request);
		String function_id = paramMap.get("function_id").toString();
		
		dataMap.put("namespace", namespace);
		dataMap.put("function_id", function_id);
		List<Map<String, Object>> functionList = service.getFunction(dataMap);
		
		String projectPath = functionList.get(0).get("git_url").toString();
		String runtime = functionList.get(0).get("runtime").toString();
		List<Map<String, Object>> sourceList = gitLabController.checkOutFromProject(projectPath, runtime);
		List<Map<String, Object>> attachList = service.getAttachFile(function_id);
		List<Map<String, Object>> targetList = service.getTargetList(dataMap);
		List<Map<String, Object>> triggerList = new ArrayList<>();
		triggerList.addAll(service.getTriggerList(dataMap));
		
		// attach 파일 이외 수정가능한 모듈만 필터링한다.
		sourceList = service.getEditableSource(runtime , sourceList);
		
		returnMap.put("list", functionList);
		returnMap.put("sourceList", sourceList);
		if(attachList.size() > 0)	returnMap.put("attachList", attachList);
		returnMap.put("triggerList", triggerList);
		returnMap.put("targetList", targetList);
		
		ObjectMapper mapper = new ObjectMapper();
		
		return mapper.writeValueAsString(returnMap);
	}
	
	@ResponseBody
	@RequestMapping("/service/checkJenkinsExists.json")
	public ModelMap checkJenkinsExists(HttpServletRequest request, HttpServletResponse response, @RequestBody String param) throws Exception {
		Map<String,Object> paramMap = new ObjectMapper().readValue(param, Map.class);
		Map<String,Object> dataMap = new HashMap<String, Object>();
		ModelMap model = new ModelMap();
		
		dataMap.put("project_id", HubpopController.getHubpopProjectIdCookie(request));
		
		Map<String, Object> JenkinsAccessInfo = jenkinsService.getJenkinsAccessInfo(dataMap);
		
		if(JenkinsAccessInfo.containsKey("jenkins_url") && JenkinsAccessInfo.containsKey("jenkins_admin_id") && JenkinsAccessInfo.containsKey("jenkins_admin_pw")) {
			model.addAttribute("jenkinsExists", true);
		} else {
			model.addAttribute("jenkinsExists", false);
		}
		
		return model;
	}
	
	@ResponseBody
	@RequestMapping("/service/saveSource.json")
	public String saveFunction(HttpServletRequest request, HttpServletResponse response, @RequestBody String param) throws Exception {
		Map<String,Object> paramMap = new ObjectMapper().readValue(param, Map.class);
		Map<String,Object> dataMap = new HashMap<String, Object>();
		Map<String,Object> returnMap = new HashMap<String, Object>();
		String namespace = FaaSCommCD.getNamespace(request);
		
		dataMap.put("function_id", paramMap.get("function_id"));
		dataMap.put("namespace", namespace);
		dataMap.put("stat_cd", FaaSCommCD.E_STAT.REG.toString());
		dataMap.put("reg_step_cd", FaaSCommCD.E_REG_STEP.STEP1.toString());
		
		service.setFunctionStat(dataMap);
		List<Map<String, Object>> functionList = service.getFunction(dataMap);
		
		dataMap.clear();
		dataMap.putAll(functionList.get(0));
		
		String projectPath = dataMap.get("git_url").toString();
		String userName = dataMap.get("git_usr").toString();
		
		List<Map<String, Object>> gitLabUserList = service.getGitLabUser(dataMap);
		String password = gitLabUserList.get(0).get("git_pwd").toString();
		
		List<Map<String, Object>> fileList = service.getFileListFromJson(paramMap.get("source_list").toString());
		// upload 파일 처리
		fileList = service.getUploadFile(fileList , paramMap.get("function_id").toString());
		// upload 파일 처리
		Commit commit = gitLabService.commitToProject(fileList, projectPath, userName, password);
		
		dataMap.put("project_id", HubpopController.getHubpopProjectIdCookie(request));
		
		Map<String, Object> JenkinsAccessInfo = jenkinsService.getJenkinsAccessInfo(dataMap);
		dataMap.put("jenkins_url", JenkinsAccessInfo.get("jenkins_url"));
		dataMap.put("jenkins_admin_id", JenkinsAccessInfo.get("jenkins_admin_id"));
		dataMap.put("jenkins_admin_pw", JenkinsAccessInfo.get("jenkins_admin_pw"));
		
		returnMap = jenkinsService.createBuildJob(dataMap);
		
		if(returnMap.get("successYn").toString().equals("y")) {
			returnMap = jenkinsService.runBuildJob(dataMap);
		} else {
			throw new Exception("jenkins buildJob create fail");
		}
		
		service.setServiceMod(paramMap);
		service.startJenkinsLogPolling(dataMap);
		
		//[추가개발필요함] source , config 변경여부 서버에서 체크해야 하나 일단 클라이언트에서 체크함. 별도 구현 필요함.
 		if(((Map)paramMap.get("isModify")).get("source").toString().equals("true"))
		{
			//로그 추가
			logService.saveModifyLog(dataMap.get("function_id").toString(), FaaSCommCD.E_LOG_DTL.REVISION , "", commit.getId());
		}
		if(((Map)paramMap.get("isModify")).get("config").toString().equals("true"))
		{
			//로그 추가
			logService.saveModifyLog(dataMap.get("function_id").toString(), FaaSCommCD.E_LOG_DTL.MODIFY , "", "");
		}
		
		
		ObjectMapper mapper = new ObjectMapper();
		
		return mapper.writeValueAsString(returnMap);
	}
	
	@ResponseBody
	@RequestMapping("/service/regTrigger.json")
	public String regTrigger(HttpServletRequest request, HttpServletResponse response, @RequestParam String param) throws Exception {
		Map<String,Object> paramMap = new ObjectMapper().readValue(param, Map.class);
		Map<String,Object> dataMap = new HashMap<String, Object>();
		Map<String,Object> returnMap = new HashMap<String, Object>();
		String namespace = FaaSCommCD.getNamespace(request);
		String triggerType = "NONE";
		
		try {
			dataMap.put("namespace", namespace);
			dataMap.put("function_id", String.valueOf(paramMap.get("function_id")));
			dataMap.put("trigger_tp", String.valueOf(paramMap.get("trigger_type")));
			dataMap.put("trigger_config", String.valueOf(paramMap.get("trigger_config")));
			
			Map<String,Object> functionMap = service.getFunction(dataMap).get(0);
			dataMap.put("name", functionMap.get("name"));
			dataMap.put("dsp_name", functionMap.get("dsp_name"));
			
			switch(dataMap.get("trigger_tp").toString()) {
				case "http":
					triggerService.createAndSaveHttpTrigger(dataMap, functionMap);
					triggerType = "HTTP";
					break;
				case "ceph":
					triggerType = "CEPH";
					break;
				case "db":
					triggerService.createAndSaveDbTrigger(dataMap, functionMap);
					triggerType = "DB";
					break;
				case "timer":
					triggerService.createAndSaveTimerTrigger(dataMap, functionMap);
					triggerType = "TIMER";
					break;
				default:
					break;
			}
			
			returnMap.put("isSuccess", true);
		} catch(ExceptionDBPreProcess e) {
			returnMap.put("isSuccess", false);
			returnMap.put("errorMessage", e.getMessage());
		} catch(Exception e) {
			e.printStackTrace();
			throw e;
		}
		
		/*
		//로그 추가
		logService.saveModifyLog(dataMap.get("function_id").toString(), FaaSCommCD.E_LOG_DTL.ADD_TRIGGER , triggerType, "");
		*/
		
		ObjectMapper mapper = new ObjectMapper();
		
		return mapper.writeValueAsString(returnMap);
	}
	
	@ResponseBody
	@RequestMapping("/service/isValidAndGetTableList.json")
	public String isValidAndGetTableList(HttpServletRequest request, HttpServletResponse response, @RequestParam String param) throws Exception {
		Map<String,Object> paramMap = new ObjectMapper().readValue(param, Map.class);
		Map<String,Object> dataMap = new HashMap<String, Object>();
		Map<String,Object> returnMap = new HashMap<String, Object>();
		
		try {
			dataMap.put("function_id", paramMap.get("function_id").toString());
			dataMap.put("db_type", paramMap.get("db_type").toString());
			dataMap.put("con_host", paramMap.get("con_host").toString());
			dataMap.put("con_port", paramMap.get("con_port").toString());
			dataMap.put("db_name", paramMap.get("db_name").toString());
			dataMap.put("admin_id", paramMap.get("admin_id").toString());
			dataMap.put("admin_pw", paramMap.get("admin_pw").toString());
		} catch(Exception e) {
			returnMap.put("errorMessage", e.getMessage());
			returnMap.put("isSuccess", false);
		}
		
		try {
			Map resultMap = triggerService.isValidAndGetTableList(dataMap);
			
			if(resultMap.get("isValid").toString().equals("true")) {
				returnMap.put("isValid", true);
				returnMap.put("tableList", resultMap.get("tableList"));
			} else {
				returnMap.put("isValid", false);
				returnMap.put("tableList", new ArrayList<>());
			}
			
			returnMap.put("isSuccess", true);
		} catch(Exception e) {
			returnMap.put("errorMessage", e.getMessage());
			returnMap.put("isSuccess", false);
		}
		
		
		
		ObjectMapper mapper = new ObjectMapper();
		
		return mapper.writeValueAsString(returnMap);
	}
	
	@ResponseBody
	@RequestMapping("/service/deleteFunction.json")
	public String deleteFunction(HttpServletRequest request, HttpServletResponse response, @RequestParam String param) throws Exception
	{
		ObjectMapper mapper = new ObjectMapper();
		Map<String,Object> paramMap = new ObjectMapper().readValue(param, Map.class);
		Map<String,Object> returnMap = new HashMap<String, Object>();
		String namespace = FaaSCommCD.getNamespace(request);
		String function_id = paramMap.get("function_id").toString();
		
		try
		{
			if(service.deleteFunction(gitLabService, namespace, function_id) == 1)
			{
				returnMap.put("success", "Y");
			}
			else
			{
				returnMap.put("success", "N");
				returnMap.put("message", "");
			}
		}
		catch(Exception e)
		{
			returnMap.put("success", "N");
			returnMap.put("message", e.getMessage());
		}
		
		return mapper.writeValueAsString(returnMap);
	}
	
	@ResponseBody
	@RequestMapping("/trigger/deleteTrigger.json")
	public String deleteTrigger(HttpServletRequest request, HttpServletResponse response, @RequestParam String param) throws Exception
	{
		ObjectMapper mapper = new ObjectMapper();
		Map<String,Object> paramMap = new ObjectMapper().readValue(param, Map.class);
		Map<String,Object> returnMap = new HashMap<String, Object>();
		String namespace = FaaSCommCD.getNamespace(request);
		String trigger_id = paramMap.get("trigger_id").toString();
		
		try
		{
			if(service.deleteTrigger(namespace, trigger_id) == 1)
			{
				returnMap.put("success", "Y");
				
				//로그 추가
				TriggerInfoBase base = service.getTriggereInfoBase(namespace, trigger_id);
				if(base != null)
					logService.saveModifyLog(base.getFunction_id(), FaaSCommCD.E_LOG_DTL.DEL_TRIGGER , base.getTrigger_tp().toUpperCase(), "");
			}
			else
			{
				returnMap.put("success", "N");
				returnMap.put("message", "");
			}
		}
		catch(ExceptionEx e)
		{
			returnMap.put("success", "N");
			returnMap.put("message", e.getMessage());
		}
		
		return mapper.writeValueAsString(returnMap);
	}
	
	@ResponseBody
	@RequestMapping("/service/sourceCommitDiff.json")
	public String sourceCommitDiff(HttpServletRequest request, HttpServletResponse response, @RequestParam String param) throws Exception
	{
		ObjectMapper mapper = new ObjectMapper();
		Map<String,Object> paramMap = mapper.readValue(param, Map.class);
		HashMap<String, Object> resultMap = new HashMap<String, Object>();
		
		String namespace = FaaSCommCD.getNamespace(request);
		String projectId = HubpopController.getHubpopProjectIdCookie(request);
		
		String function_id = paramMap.get("function_id").toString();
		String revisionId = paramMap.get("revision_id").toString();
		
		paramMap.put("namespace", namespace);
		
		List<Map<String, Object>> functionList = service.getFunction(paramMap);
		if(functionList.size() == 1)
		{
			String projectPath = functionList.get(0).get("git_url").toString();
			String runtime = functionList.get(0).get("runtime").toString();
			List<Map<String, Object>> sourceList = gitLabController.checkOutFromProject(projectPath, runtime);
			List<Diff> listDiff = gitLabService.syncDiffGitlab(projectPath,revisionId);
			//List<Diff> listDiff = gitLabService.syncDiffGitlab("425","4cc290a4a1ac3aad7e86d1e7972b572a04fa15e7");
		
			resultMap.put("success", "Y");
			resultMap.put("sourceList", sourceList);
			resultMap.put("diff", listDiff);
		}
		else
		{
			resultMap.put("success", "N");
		}
		return mapper.writeValueAsString(resultMap);
	}
	
	@ResponseBody
	@RequestMapping("/trigger/httpAPIExist.json")
	public String httpAPIExist(HttpServletRequest request, HttpServletResponse response, @RequestParam String param) throws Exception
	{
		ObjectMapper mapper = new ObjectMapper();
		Map<String,Object> paramMap = mapper.readValue(param, Map.class);
		HashMap<String, Object> resultMap = new HashMap<String, Object>();
		String namespace = FaaSCommCD.getNamespace(request);
		String apiName = paramMap.get("apiName").toString();
		paramMap.put("namespace", namespace);
		
		List<Map<String, Object>> functionList = service.getFunction(paramMap);
		if(functionList.size() == 1)
		{
			String functionName = functionList.get(0).get("dsp_name").toString();
			resultMap.put("success", "Y");
			resultMap.put("api_name", apiName);
			
			resultMap.put("isExist", "N");
			if(kongService.isExistService(namespace, functionName, apiName))
				resultMap.put("isExist", "Y");
			
			return mapper.writeValueAsString(resultMap);
		}
		throw new ExceptionEx("함수 정보를 조회하지 못했습니다.");
	}
	
	@ResponseBody
	@RequestMapping("/service/enableFunction.json")
	public String enableFunction(HttpServletRequest request, HttpServletResponse response, @RequestParam String param) throws Exception
	{
		ObjectMapper mapper = new ObjectMapper();
		Map<String,Object> paramMap = new ObjectMapper().readValue(param, Map.class);
		Map<String,Object> dataMap = new HashMap<String, Object>();
		Map<String,Object> returnMap = new HashMap<String, Object>();
		String namespace = FaaSCommCD.getNamespace(request);
		String function_id = paramMap.get("function_id").toString();
		
		dataMap.put("namespace", namespace);
		dataMap.put("function_id", function_id);
		
		List<Map<String, Object>> functionList = service.getFunction(dataMap);
		if(functionList.size() == 1)
		{
			Map<String, Object> function = (Map<String, Object>)functionList.get(0);
			if(FaaSCommCD.E_STAT.DISABLE.toString().equals(function.get("stat_cd").toString()))
			{
				dataMap.put("stat_cd", FaaSCommCD.E_STAT.REG.toString());
				dataMap.put("reg_step_cd", FaaSCommCD.E_REG_STEP.STEP1.toString());
				service.setFunctionStat(dataMap);
				
				dataMap.put("project_id", HubpopController.getHubpopProjectIdCookie(request));
				dataMap.putAll(function);
				
				Map<String, Object> JenkinsAccessInfo = jenkinsService.getJenkinsAccessInfo(dataMap);
				dataMap.put("jenkins_url", JenkinsAccessInfo.get("jenkins_url"));
				dataMap.put("jenkins_admin_id", JenkinsAccessInfo.get("jenkins_admin_id"));
				dataMap.put("jenkins_admin_pw", JenkinsAccessInfo.get("jenkins_admin_pw"));
				
				returnMap = jenkinsService.createBuildJob(dataMap);
				
				if(returnMap.get("successYn").toString().equals("y")) {
					returnMap = jenkinsService.runBuildJob(dataMap);
				} else {
					throw new Exception("jenkins buildJob create fail");
				}
				
				service.startJenkinsLogPolling(dataMap);
				
				logService.saveModifyLog(dataMap.get("function_id").toString(), FaaSCommCD.E_LOG_DTL.ENABLE , "", "");
				
				returnMap.put("success", "Y");
				return 	mapper.writeValueAsString(returnMap);
			}
		}
		
		returnMap.put("success", "N");
		return 	mapper.writeValueAsString(returnMap);	
	}
	
	@ResponseBody
	@RequestMapping("/service/disableFunction.json")
	public String disableFunction(HttpServletRequest request, HttpServletResponse response, @RequestParam String param) throws Exception
	{
		ObjectMapper mapper = new ObjectMapper();
		Map<String,Object> paramMap = new ObjectMapper().readValue(param, Map.class);
		Map<String,Object> dataMap = new HashMap<String, Object>();
		Map<String,Object> returnMap = new HashMap<String, Object>();
		String namespace = FaaSCommCD.getNamespace(request);
		String function_id = paramMap.get("function_id").toString();
		
		boolean bRet = service.disableFunction(namespace,function_id);
		
		if(bRet)
		{
			returnMap.put("success", "Y");
			logService.saveModifyLog(function_id, FaaSCommCD.E_LOG_DTL.DISABLE , "", "");
		}
		else returnMap.put("success", "N");
		
		return mapper.writeValueAsString(returnMap);
	}
	
	
	@ExceptionHandler(FileSizeLimitExceededException.class) 
	public Object nullex(FileSizeLimitExceededException e) 
	{ 
		System.err.println(e.getClass()); return "myService"; 
	}

	
	/*
	 * 업로드 파일을 저장하고 파일 정보를 DB에 저장한다.
	 */
	@ResponseBody
	@RequestMapping("/service/upload.json")
	public Object upload(HttpServletRequest request, HttpServletResponse response ,  @RequestParam String function_id) throws Exception
	{
		Map<String,Object> returnMap = new HashMap<String, Object>();
		returnMap.put("success", "N");
		try
		{
			service.setUpload((MultipartHttpServletRequest)request, function_id);
			returnMap.put("success", "Y");
		}
		catch(RuntimeException e)
		{
			returnMap.put("msg", "DB 저장중 에러가 발생했습니다.");
		}
		catch(IOException e)
		{
			returnMap.put("msg", "업로드파일 처리중 에러가 발생했습니다.");
		}
		return returnMap;
	}
	
	@ResponseBody
	@RequestMapping("/service/regTarget.json")
	public Object regTarget(HttpServletRequest request, HttpServletResponse response ,  @RequestParam String param) throws Exception
	{
		ObjectMapper mapper = new ObjectMapper();
		Map<String,Object> paramMap = new ObjectMapper().readValue(param, Map.class);
		String namespace = FaaSCommCD.getNamespace(request);
		paramMap.put("namespace", namespace);

		paramMap = service.saveTarget(paramMap);
		return paramMap;
	}
	
	@ResponseBody
	@RequestMapping("/service/candidateTargetList.json")
	public Object candidateTargetList(HttpServletRequest request, HttpServletResponse response,  @RequestParam String param) throws Exception
	{
		ObjectMapper mapper = new ObjectMapper();
		Map<String, Object> paramMap = new ObjectMapper().readValue(param, Map.class);
		Map<String, Object> returnMap = new HashMap<>();
		
		String namespace = FaaSCommCD.getNamespace(request);
		paramMap.put("namespace", namespace);
		
		paramMap.put("target_result_cd", "success");
		List<Map<String, Object>> onSuccess = service.getCandidateTargetList(paramMap);
		
		paramMap.put("target_result_cd", "fail");
		List<Map<String, Object>> onFailure = service.getCandidateTargetList(paramMap);
		
		returnMap.put("onSuccess", onSuccess);
		returnMap.put("onFailure", onFailure);
		
		return returnMap;
	}
	
	@ResponseBody
	@RequestMapping("/service/deleteTarget.json")
	public Object deleteTarget(HttpServletRequest request, HttpServletResponse response ,  @RequestParam String param) throws Exception
	{
		ObjectMapper mapper = new ObjectMapper();
		Map<String,Object> paramMap = new ObjectMapper().readValue(param, Map.class);
		Map<String,Object> retMap = new HashMap<>();
		String namespace = FaaSCommCD.getNamespace(request);
		paramMap.put("namespace", namespace);
		
		if(service.deleteTarget(paramMap) > 0)
			retMap.put("success", "Y");
		else
			retMap.put("success", "N");
		
		return retMap;
	}
	
	// grafana 관련
	@GetMapping("/grafana/main.do")
	public String grafanaMain(HttpServletRequest request, HttpServletResponse response, @RequestParam Map<String,String> paramMap , Model model) throws Exception 
	{
 		 String function_id = paramMap.get("function_id").toString();
	     String namespace = FaaSCommCD.getNamespace(request);
	     Map<String,Object> functionMap = service.getFunction(namespace,function_id);
	     if(functionMap == null) throw new ExceptionEx("함수 정보를 찾지 못했습니다.");
	     String serviceUrl = String.format("%s.%s.svc.cluster.local", functionMap.get("name") ,namespace );
	     // 함수 조회
	     String targetUrl = String.format("/faas/k8s/clusters/c-m7h2z/api/v1/namespaces/cattle-prometheus/services/http:access-grafana:80"
	     		+ "/proxy/d/%s?orgId=2&refresh=5s&var-service=%s&kiosk"
	    		 , "rgSDu_vGk/hubpop-service" , serviceUrl ) ;
	     paramMap.put("targetUrl", targetUrl);
		
		 model.addAttribute("paramMap", paramMap);
		 return "layout/function/grafanaFrame";
	}
	
	
	@GetMapping("/k8s/**/d/**")
    public ResponseEntity<String> proxy(HttpServletRequest request, HttpServletResponse response) throws Exception 
	{
	     RestTemplate restTemplate = restTemplate();
	     
	     
	     String originReqURL = request.getRequestURI().replaceAll("^/faas", "");
	     String originQueryString = request.getQueryString();
	     String urlStr = grafanaKubernetesHome + originReqURL + (StringUtils.isEmpty(originQueryString) ? "" : "?"+originQueryString);
	     
	     URI url = new URI(urlStr);
	     System.out.println("k8s*d* : " + urlStr);
	     
	     // method
	     String originMethod = request.getHeader("x-origin-method");
	     HttpMethod method = HttpMethod.valueOf("GET");
	     
	     // header
	     Enumeration<String> headerNames = request.getHeaderNames();
	     MultiValueMap<String, String> headers = new LinkedMultiValueMap<>();
	     headers.add("Authorization", String.format("%s %s",authorizationType , authorizationToken));
	     
	     // http entity (body, header)
	     HttpEntity<byte[]> httpEntity = new HttpEntity<>(null, headers);
	     
	     ResponseEntity<String> responseEntity = restTemplate.exchange(url, method, httpEntity, String.class);
	     String result = (String)responseEntity.getBody();
	     String res = result.replaceAll(grafanaKubernetesHome,"").replaceAll("/k8s/", "/faas/k8s/");
	     HttpHeaders orgHeaders = responseEntity.getHeaders();
	     HttpHeaders tarHeaders = new HttpHeaders();
	     if(orgHeaders.containsKey("Content-Type")) 
	     { 
	    	 List<String> list = (List<String>)orgHeaders.get("Content-Type");
	    	 tarHeaders.addAll("Content-Type", list);
	     }
	     return ResponseEntity.ok().headers(tarHeaders).body(res);
    }
    
    @GetMapping("/k8s/**")
    public ResponseEntity<byte[]> proxy111(HttpServletRequest request, HttpServletResponse response, @RequestBody(required = false) byte[] body) throws IOException, URISyntaxException, KeyManagementException, KeyStoreException, NoSuchAlgorithmException 
	{
		 RestTemplate restTemplate = restTemplate();
		 
		 // url
		 String originReqURL = request.getRequestURI().replaceAll("^/faas", "");
		 String originQueryString = request.getQueryString();
		 String urlStr = grafanaKubernetesHome + originReqURL + (StringUtils.isEmpty(originQueryString) ? "" : "?"+originQueryString);
		 
		 URI url = new URI(urlStr);
		 
		 System.out.println("k8s : " + urlStr);
		 
		 // method
		 String originMethod = request.getHeader("x-origin-method");
		 HttpMethod method = HttpMethod.valueOf("GET");
		 
		 // header
	     Enumeration<String> headerNames = request.getHeaderNames();
	     MultiValueMap<String, String> headers = new LinkedMultiValueMap<>();
	     while(headerNames.hasMoreElements()) {
	      String headerName = headerNames.nextElement();
	      String headerValue = request.getHeader(headerName);
	   
	      headers.add(headerName, headerValue);
	     }
	     headers.add("Authorization", String.format("%s %s",authorizationType , authorizationToken));
		 
		 // http entity (body, header)
		 HttpEntity<byte[]> httpEntity = new HttpEntity<>(body, headers);
		 
		 ResponseEntity<byte[]> responseEntity = restTemplate.exchange(url, method, httpEntity, byte[].class);
		 String result = responseEntity.getBody().toString();
		 HttpHeaders orgHeaders = responseEntity.getHeaders();
		 HttpHeaders tarHeaders = new HttpHeaders();
		 if(orgHeaders.containsKey("Content-Type")) 
		 { 
			 List<String> list = (List<String>)orgHeaders.get("Content-Type");
			 tarHeaders.addAll("Content-Type", list);
		 }
		 
		 return ResponseEntity.ok().headers(tarHeaders).body(responseEntity.getBody());
    }
    
    private RestTemplate restTemplate()    throws KeyStoreException, NoSuchAlgorithmException, KeyManagementException 
    {
		TrustStrategy acceptingTrustStrategy = (X509Certificate[] chain, String authType) -> true;
		
		SSLContext sslContext = SSLContexts.custom()
		                .loadTrustMaterial(null, acceptingTrustStrategy)
		                .build();
		
		SSLConnectionSocketFactory csf = new SSLConnectionSocketFactory(sslContext);
		
		CloseableHttpClient httpClient = HttpClients.custom()
		                .setSSLSocketFactory(csf)
		                .build();
		
		HttpComponentsClientHttpRequestFactory requestFactory =
		                new HttpComponentsClientHttpRequestFactory();
		
		requestFactory.setHttpClient(httpClient);
		RestTemplate restTemplate = new RestTemplate(requestFactory);
		return restTemplate;
	}
    // grafana 관련
	
}