package kr.co.ntels.faas.function.service.dynamicDB;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.mybatis.spring.SqlSessionTemplate;

import kr.co.ntels.faas.userException.ExceptionDBPreProcess;



public class DynamicDBMariaJdbc extends DynamicDB {
	
	String sqlTableList = "SELECT TABLE_NAME tablename FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_SCHEMA = ? ;";
	String sqlServerId = "SELECT @@server_id;";
	

	public DynamicDBMariaJdbc(DynamicDBFactory.DBInfo info)
	{
		this.info = info;
	}

	@Override
	protected List<String> getListTableNameDB() throws Exception 
	{
		List<String> list = new ArrayList<>();
		
		Connection con = null;
		PreparedStatement preparedStatement = null;
		ResultSet rs = null;
		try
		{
			con = getConnection();
			preparedStatement = con.prepareStatement(sqlTableList);
			preparedStatement.setString(1, info.getDatabase());
			rs = preparedStatement.executeQuery();
			
			while(rs.next())
			{
				String tableName = rs.getString(1);
				list.add(tableName);
			}
		}
		catch(SQLException e)
		{
			throw e;
		}
		finally
		{
			if(rs != null) rs.close();
			if(preparedStatement != null) preparedStatement.close();
			if(con != null) con.close();
		}
		
		return list;
	}
	
	@Override
	public Object triggerPreProcess() throws Exception {
		Map<String, Object> returnMap = new HashMap<>();
		
		Connection con = null;
		PreparedStatement preparedStatement = null;
		ResultSet rs = null;
		try 
		{
			String server_id = "";
			con = getConnection();
			preparedStatement = con.prepareStatement(sqlServerId);
			rs = preparedStatement.executeQuery();
			
			if(rs.next())
			{
				server_id = rs.getString(1);
			}
			
			if(!String.valueOf(server_id).toLowerCase().equals("null") && !String.valueOf(server_id).equals("")) {
				returnMap.put("server_id", server_id);
			} else {
				throw new ExceptionDBPreProcess("failed to get server_id");
			}
		} catch(Exception e) {
			throw new ExceptionDBPreProcess("db trigger pre process fail");
		}
		
		return returnMap;
	}
	
	private Connection  getConnection() throws Exception
	{
		Connection con = null;
		String driver = "org.mariadb.jdbc.Driver";
		String url = String.format("jdbc:mariadb://%s:%s/%s" , info.getHost() , info.getPort() , info.getDatabase());
		String user = info.getUsr(); 
		String pw = info.getPwd();
		
		try 
		{ 
			Class.forName(driver); 
			con = DriverManager.getConnection(url, user, pw); 
		} 
		catch (SQLException e) 
		{ 
			System.out.println("[SQL Error : " + e.getMessage() +"]");
			throw e;
		} 
		catch (ClassNotFoundException e1) 
		{ 
			System.out.println("[JDBC Connector Driver Error : " + e1.getMessage() + "]");
			throw e1;	
		}
		
		return con;
	}
	
}