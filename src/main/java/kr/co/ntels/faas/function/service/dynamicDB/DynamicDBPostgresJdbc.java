package kr.co.ntels.faas.function.service.dynamicDB;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.sql.DataSource;

import org.mybatis.spring.SqlSessionFactoryBean;
import org.mybatis.spring.SqlSessionTemplate;
import org.springframework.core.io.Resource;
import org.springframework.core.io.support.PathMatchingResourcePatternResolver;

import com.zaxxer.hikari.HikariConfig;
import com.zaxxer.hikari.HikariDataSource;

import kr.co.ntels.faas.userException.ExceptionDBPreProcess;

/**
  * @FileName : DynamicDBPostgres.java
  * @Project : faas-user-portal
  * @Date : 2020. 5. 28. 
  * @작성자 : sig61 - 백광욱
  * @변경이력 :
  * @프로그램 설명 :
  */
public class DynamicDBPostgresJdbc extends DynamicDB{
	
	String sqlTableList = "SELECT schemaname,tablename FROM PG_TABLES WHERE SCHEMANAME NOT IN ('pg_catalog', 'information_schema');";
	String sqlWalLevel = "SHOW wal_level";
	String sqlMaxReplicationSlots = "SHOW max_replication_slots";
	String sqlReplicationSlotList = "SELECT Count(*) cnt FROM PG_REPLICATION_SLOTS WHERE 1=1; ";
	
	public DynamicDBPostgresJdbc(DynamicDBFactory.DBInfo info)
	{
		super(info);
	}

	@Override
	protected List<String> getListTableNameDB() throws Exception 
	{
		List<String> list = new ArrayList<>();
		
		Connection con = null;
		PreparedStatement preparedStatement = null;
		ResultSet rs = null;
		try
		{
			con = getConnection();
			preparedStatement = con.prepareStatement(sqlTableList);
			rs = preparedStatement.executeQuery();
			
			while(rs.next())
			{
				String tableName =String.format("%s.%s", rs.getString(1), rs.getString(2));
				list.add(tableName);
			}
		}
		catch(SQLException e)
		{
			throw e;
		}
		finally
		{
			if(rs != null) rs.close();
			if(preparedStatement != null) preparedStatement.close();
			if(con != null) con.close();
		}
		
		return list;

	}

	@Override
	public Object triggerPreProcess() throws Exception {
		Map<String, Object> returnMap = new HashMap<>();
		
		Connection con = null;
		try
		{
			String wal_level = "";
			con = getConnection();
			wal_level = getScalarString(con , sqlWalLevel);

			if(!String.valueOf(wal_level).toLowerCase().equals("logical")) {
				throw new ExceptionDBPreProcess("wal_level is not logical");
			}
			
			String max_replication_slots = getScalarString(con, sqlMaxReplicationSlots);
			String replicationSlotList = getScalarString(con,sqlReplicationSlotList);
			
			if(!(Integer.parseInt(max_replication_slots) > Integer.parseInt(replicationSlotList))) {
				throw new ExceptionDBPreProcess("replication_slot is full");
			}
		} catch(Exception e) {
			throw new ExceptionDBPreProcess("db trigger pre process fail");
		}
		finally
		{
			if(con != null) con.close();
		}
		
		return returnMap;
	}
	
	private String getScalarString(Connection con , String sql) throws Exception
	{
		PreparedStatement preparedStatement = null;
		ResultSet rs = null;
		try
		{
			con = getConnection();
			preparedStatement = con.prepareStatement(sql);
			rs = preparedStatement.executeQuery();
			
			if(rs.next())
			{
				return rs.getString(1);
			}
		}
		catch(SQLException e)
		{
			throw e;
		}
		finally
		{
			if(rs != null) rs.close();
			if(preparedStatement != null) preparedStatement.close();
		}
		return "";
	}

	private Connection  getConnection() throws Exception
	{
		Connection con = null;
		String driver = "org.postgresql.Driver";
		String url = String.format("jdbc:postgresql://%s:%d/%s" , info.getHost() , info.getPort() , info.getDatabase());
		String user = info.getUsr(); 
		String pw = info.getPwd();
		
		try 
		{ 
			Class.forName(driver); 
			con = DriverManager.getConnection(url, user, pw); 
		} 
		catch (SQLException e) 
		{ 
			System.out.println("[SQL Error : " + e.getMessage() +"]");
			throw e;
		} 
		catch (ClassNotFoundException e1) 
		{ 
			System.out.println("[JDBC Connector Driver Error : " + e1.getMessage() + "]");
			throw e1;	
		}
		
		return con;
	}
	

}
