package kr.co.ntels.faas.function.service.kong;

import java.util.HashMap;
import java.util.Map;

import org.springframework.stereotype.Service;

import kr.co.ntels.faas.common.apiCommonService.baseService.ServiceBase;

public class KongAPIBase extends ServiceBase{
	
	Map<String , Object> _mapHeader = new HashMap<>();
	
	
	public void addHeader(String key, String val)
	{
		_mapHeader.put(key, val);
	}
	
	@Override
	public Map<String , Object> getHeader() 
	{
		return _mapHeader;
	}
	
}
