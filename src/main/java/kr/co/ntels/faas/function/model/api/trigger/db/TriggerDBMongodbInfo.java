/**
 * 
 */
package kr.co.ntels.faas.function.model.api.trigger.db;

import java.io.IOException;
import java.net.URL;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang.StringUtils;

import com.google.common.io.Resources;

import kr.co.ntels.faas.function.model.api.trigger.TriggerInfoFactory;

/**
  * @FileName : TriggerDBPostgresInfo.java
  * @Project : faas-portal
  * @Date : 2020. 2. 24. 
  * @작성자 : sig61 - 백광욱
  * @변경이력 :
  * @프로그램 설명 :
  */

public class TriggerDBMongodbInfo extends TriggerDBInfo {

	public TriggerDBMongodbInfo() {}

	/**
	 * @param name
	 * @param namespace
	 * @param param
	 */
	Map<String, Object> param = new HashMap<>();
	
	public TriggerDBMongodbInfo(String name, String namespace, Map<String, Object> param) {
		super(name, namespace, param);
		this.param = param;
	}
	
	@Override
	public  String getType() {
		return TriggerInfoFactory.E_TRRIGGER_TYPE.MONGODB.toString();
	}

	@Override
	public String toChannelYaml() {
		String str = super.toChannelYaml();
		return str;
	}

	@Override
	public String toSubscriptionYaml() {
		String str = super.toSubscriptionYaml();
		return str;
	}

	@Override
	public String toKamelSource() {
		String str = "";
		
		URL url = Resources.getResource("format/trigger-db-mongodb.groovy");
		
		try {
			str = Resources.toString(url, StandardCharsets.UTF_8);
			
			String con_host = param.get("con_host").toString();
			String con_port = param.get("con_port").toString();
			String db_name = param.get("db_name").toString();
			String admin_id = param.get("admin_id").toString();
			String admin_pw = param.get("admin_pw").toString();
			admin_pw = admin_pw.replace("$", "\\$");
			String replicaset_name = "rs0";
			String collection_name = param.get("collection_name").toString();
			String channel_name = this.getChannelName();
			
			String insert_yn = param.get("insert_yn").toString();
			String update_yn = param.get("update_yn").toString();
			String delete_yn = param.get("delete_yn").toString();
			
			List<String> event_list = new ArrayList<String>();
			
			if(insert_yn.toLowerCase().equals("y"))
				event_list.add("c");
			if(update_yn.toLowerCase().equals("y"))
				event_list.add("u");
			if(delete_yn.toLowerCase().equals("y"))
				event_list.add("d");
			
			String event_types = StringUtils.join(event_list, ", ");
			
			str = str.replace("@@CON_HOST@@", con_host);
			str = str.replace("@@CON_PORT@@", con_port);
			str = str.replace("@@DB_NAME@@", db_name);
			str = str.replace("@@ADMIN_ID@@", admin_id);
			str = str.replace("@@ADMIN_PW@@", admin_pw);
			str = str.replace("@@REPLICASET_NAME@@", replicaset_name);
			str = str.replace("@@COLLECTION_NAME@@", collection_name);
			str = str.replace("@@EVENT_TYPES@@", event_types);
			str = str.replace("@@CHANNEL_NAME@@", channel_name);
			
			System.out.println("MongoDB Kamel Groovy: " + str);
		} catch (IOException e) {
			e.printStackTrace();
		}
		
		return str;
	}
}