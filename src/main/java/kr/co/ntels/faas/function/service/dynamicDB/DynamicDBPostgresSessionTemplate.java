package kr.co.ntels.faas.function.service.dynamicDB;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.sql.DataSource;

import org.mybatis.spring.SqlSessionFactoryBean;
import org.mybatis.spring.SqlSessionTemplate;
import org.springframework.core.io.Resource;
import org.springframework.core.io.support.PathMatchingResourcePatternResolver;

import com.zaxxer.hikari.HikariConfig;
import com.zaxxer.hikari.HikariDataSource;

import kr.co.ntels.faas.userException.ExceptionDBPreProcess;

/**
  * @FileName : DynamicDBPostgres.java
  * @Project : faas-user-portal
  * @Date : 2020. 5. 28. 
  * @작성자 : sig61 - 백광욱
  * @변경이력 :
  * @프로그램 설명 :
  */

/*
 * SqlSessionTemplate 으로 연결해서 테스트시 DB 연결이 해결되지 않는 문제가 있어 DynamicDBPostgresJdbc.java로 코딩 다시함. 
 * 연결 해제가 해결되면 사용가능함.
 */

public class DynamicDBPostgresSessionTemplate extends DynamicDB{

	
	public DynamicDBPostgresSessionTemplate(DynamicDBFactory.DBInfo info)
	{
		super(info);
	}

	@Override
	protected List<String> getListTableNameDB() throws Exception 
	{
		List<String> list = new ArrayList<>();
		
		SqlSessionTemplate sqlSessionTemplate = getSqlSessionTemplate();
		
		List<Map<String, Object>> retList = sqlSessionTemplate.selectList("faas.dynamicDBPostgresql.getTableList", new HashMap<String,Object>());
		
		for(Map<String, Object> map : retList)
		{
			String tableName = String.format("%s.%s", map.get("schemaname").toString(), map.get("tablename").toString());
			
			list.add(tableName);
		}
		
		return list;
	}

	@Override
	public Object triggerPreProcess() throws Exception {
		Map<String, Object> returnMap = new HashMap<>();
		
		try {
			SqlSessionTemplate sqlSessionTemplate = getSqlSessionTemplate();
			
			String wal_level = sqlSessionTemplate.selectOne("faas.dynamicDBPostgresql.getWalLevel", new HashMap<String,Object>());
			
			if(!String.valueOf(wal_level).toLowerCase().equals("logical")) {
				throw new ExceptionDBPreProcess("wal_level is not logical");
			}
			
			String max_replication_slots = sqlSessionTemplate.selectOne("faas.dynamicDBPostgresql.getMaxReplicationSlots", new HashMap<String,Object>());
			List<Map<String, Object>> replicationSlotList = sqlSessionTemplate.selectList("faas.dynamicDBPostgresql.getReplicationSlotList", new HashMap<String,Object>());
			
			if(!(Integer.parseInt(max_replication_slots) > replicationSlotList.size())) {
				throw new ExceptionDBPreProcess("replication_slot is full");
			}
		} catch(Exception e) {
			throw new ExceptionDBPreProcess("db trigger pre process fail");
		}
		
		return returnMap;
	}

	private SqlSessionTemplate getSqlSessionTemplate() throws Exception
	{
		DataSource dataSource = new HikariDataSource(getHikariConfig());
		SqlSessionFactoryBean sqlSessionFactoryBean = new SqlSessionFactoryBean();
		sqlSessionFactoryBean.setDataSource(dataSource);
		sqlSessionFactoryBean.setMapperLocations(new PathMatchingResourcePatternResolver().getResources("classpath:/mapper/dynamicDBPostgresql.xml"));
		
		Resource myBatisConfig = new PathMatchingResourcePatternResolver().getResource("classpath:mybatis-config.xml");
		sqlSessionFactoryBean.setConfigLocation(myBatisConfig);
		
		return new SqlSessionTemplate(sqlSessionFactoryBean.getObject());
	}
	
	private HikariConfig getHikariConfig() throws Exception
	{
		HikariConfig config = new HikariConfig();
		config.setDriverClassName("org.postgresql.Driver");
		config.setJdbcUrl(String.format("jdbc:postgresql://%s:%d/%s" , info.getHost() , info.getPort() , info.getDatabase()));
		config.setUsername(info.getUsr());
		config.setPassword(info.getPwd());
		
		return config;
	}

}
