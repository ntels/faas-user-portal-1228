package kr.co.ntels.faas.function.model;

import kr.co.ntels.faas.constant.FaaSCommCD;

/**
  * @FileName : FunctionLog.java
  * @Project : faas-user-portal
  * @Date : 2020. 6. 18. 
  * @작성자 : sig61 - 백광욱
  * @변경이력 :
  * @프로그램 설명 :
  */
public class FunctionLogVO {
	
	String functionLogId;
	String functionId;
	String triggerTp;
	String revisionId;
	String logCd;
	String logDtlCd;
	String regUsr;
	String regDateHms;
	String log;
	String namespace;
	String name; 

	
	public String getFunctionLogId() {
		return functionLogId;
	}
	public void setFunctionLogId(String functionLogId) {
		this.functionLogId = functionLogId;
	}
	public String getFunctionId() {
		return functionId;
	}
	public void setFunctionId(String functionId) {
		this.functionId = functionId;
	}
	public String getTriggerTp() {
		return triggerTp;
	}
	public void setTriggerTp(String triggerTp) {
		this.triggerTp = triggerTp;
	}
	public String getRevisionId() {
		return revisionId;
	}
	public void setRevisionId(String revisionId) {
		this.revisionId = revisionId;
	}
	public String getLogCd() {
		return logCd;
	}
	public void setLogCd(FaaSCommCD.E_LOG e_log) {
		this.logCd = e_log.toString();
	}
	public String getLogDtlCd() {
		return logDtlCd;
	}
	public void setLogDtlCd(FaaSCommCD.E_LOG_DTL e_log_dtl) {
		this.logDtlCd = e_log_dtl.toString();
	}
	public String getRegUsr() {
		return regUsr;
	}
	public void setRegUsr(String regUsr) {
		this.regUsr = regUsr;
	}
	public String getRegDateHms() {
		return regDateHms;
	}
	public void setRegDateHms(String regDateHms) {
		this.regDateHms = regDateHms;
	}
	public String getLog() {
		return log;
	}
	public void setLog(String log) {
		this.log = log;
	}
	public String getNamespace() {
		return namespace;
	}
	public void setNamespace(String namespace) {
		this.namespace = namespace;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}

}
