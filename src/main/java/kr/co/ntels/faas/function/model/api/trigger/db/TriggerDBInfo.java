/**
 * 
 */
package kr.co.ntels.faas.function.model.api.trigger.db;

import java.util.Map;

import kr.co.ntels.faas.common.apiCommon.util.JsonSearch;
import kr.co.ntels.faas.common.utils.CommCode;
import kr.co.ntels.faas.function.model.api.trigger.TriggerInfo;

/**
  * @FileName : TriggerDBInfo.java
  * @Project : faas-portal
  * @Date : 2020. 2. 21. 
  * @작성자 : sig61 - 백광욱
  * @변경이력 :
  * @프로그램 설명 :
  */

public class TriggerDBInfo extends TriggerInfo {
	
	@JsonSearch(path="$.db_type")
	String dbType;
	
	@JsonSearch(path="$.db_name")
	String dbName;
	
	@JsonSearch(path="$.con_host")
	String conHost;
	
	@JsonSearch(path="$.con_port")
	String conPort;
	
	@JsonSearch(path="$.admin_id")
	String adminId;
	
	@JsonSearch(path="$.admin_pw")
	String adminPwd;
	
	@JsonSearch(path="$.table_name")
	String tableName;
	
	@JsonSearch(path="$.collection_name")
	String collectionName;
	
	@JsonSearch(path="$.insert_yn")
	String insertYN;
	
	@JsonSearch(path="$.update_yn")
	String updateYN;
	
	@JsonSearch(path="$.delete_yn")
	String deleteYN;
	
	
	public TriggerDBInfo() {}
	/**
	 * @param name
	 * @param namespace
	 */
	public TriggerDBInfo(String name, String namespace , Map<String, Object> param) {
		super(name, namespace,param);
		
		dbType = param.get("db_type").toString();
		dbName = param.get("db_name").toString();
		conHost = param.get("con_host").toString();
		conPort = param.get("con_port").toString();
		adminId = param.get("admin_id").toString();
		adminPwd = param.get("admin_pw").toString();
		tableName = param.get("table_name").toString();
		collectionName = param.get("collection_name").toString();
		insertYN = param.get("insert_yn").toString();
		updateYN = param.get("update_yn").toString();
		deleteYN = param.get("delete_yn").toString();
	}

	@Override
	public  String getType() {
		return "DB";
	}
	
	public String getDbType() {
		return dbType;
	}
	public void setDbType(String dbType) {
		this.dbType = dbType;
	}
	public String getDbName() {
		return dbName;
	}
	public void setDbName(String dbName) {
		this.dbName = dbName;
	}
	public String getConHost() {
		return conHost;
	}
	public void setConHost(String conHost) {
		this.conHost = conHost;
	}
	public String getConPort() {
		return conPort;
	}
	public void setConPort(String conPort) {
		this.conPort = conPort;
	}
	public String getAdminId() {
		return adminId;
	}
	public void setAdminId(String adminId) {
		this.adminId = adminId;
	}
	public String getAdminPwd() {
		return adminPwd;
	}
	public void setAdminPwd(String adminPwd) {
		this.adminPwd = adminPwd;
	}
	public String getTableName() {
		return tableName;
	}
	public void setTableName(String tableName) {
		this.tableName = tableName;
	}
	public String getCollectionName() {
		return collectionName;
	}
	public void setCollectionName(String collectionName) {
		this.collectionName = collectionName;
	}
	public String getInsertYN() {
		return insertYN;
	}
	public void setInsertYN(String insertYN) {
		this.insertYN = insertYN;
	}
	public String getUpdateYN() {
		return updateYN;
	}
	public void setUpdateYN(String updateYN) {
		this.updateYN = updateYN;
	}
	public String getDeleteYN() {
		return deleteYN;
	}
	public void setDeleteYN(String deleteYN) {
		this.deleteYN = deleteYN;
	}
	
	@Override
	public String toChannelYaml() {
		String str = super.toChannelYaml();
		return str;
	}

	@Override
	public String toSubscriptionYaml() {
		String str = super.toSubscriptionYaml();
		return str;
	}

	@Override
	public String toKamelSource() {
		// TODO Auto-generated method stub
		return null;
	}

}
