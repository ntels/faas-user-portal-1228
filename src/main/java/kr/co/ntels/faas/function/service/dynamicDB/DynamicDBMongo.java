package kr.co.ntels.faas.function.service.dynamicDB;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.mongodb.MongoClient;
import com.mongodb.MongoClientURI;
import com.mongodb.MongoSocketOpenException;
import com.mongodb.client.MongoDatabase;
import com.mongodb.client.MongoIterable;

import kr.co.ntels.faas.userException.ExceptionDBPreProcess;

/**
  * @FileName : DynamicDBMongo.java
  * @Project : faas-user-portal
  * @Date : 2020. 5. 28. 
  * @작성자 : sig61 - 백광욱
  * @변경이력 :
  * @프로그램 설명 :
  */
public class DynamicDBMongo extends DynamicDB {
	
	public DynamicDBMongo(DynamicDBFactory.DBInfo info)
	{
		super(info);
	}

	@Override
	protected List<String> getListTableNameDB() throws Exception {
		List<String> list = new ArrayList<>();
		
		MongoClient mongoClient = getMongoClient();
		
		if(mongoClient != null)
		{
			MongoDatabase db = mongoClient.getDatabase(info.getDatabase());
	        MongoIterable<String> collections = db.listCollectionNames();
	        
	        for (String colName : collections) {
	        	list.add(colName);
	        }
		}
		
		return list;
	}

	@Override
	public Object triggerPreProcess() throws Exception {
		Map<String, Object> returnMap = new HashMap<>();
		
		try {
			
		} catch(Exception e) {
			throw new ExceptionDBPreProcess("db trigger pre process fail");
		}
		
		return returnMap;
	}
	
	private MongoClient getMongoClient() throws Exception
	{
		MongoClient mongoClient = null;
		
		try {
			MongoClientURI uri = new MongoClientURI(String.format("mongodb://%s:%s@%s:%d", info.getUsr(), info.getPwd(), info.getHost(), info.getPort()));
			// MongoClientURI uri = new MongoClientURI(String.format("mongodb://%s:%s@%s:%d/?authSource=%s", info.getUsr(), info.getPwd(), info.getHost(), info.getPort(), info.getDatabase()));
			mongoClient = new MongoClient(uri);
			
		} catch(MongoSocketOpenException e) {
			throw new Exception("mongodb connection fail");
		}
		catch(RuntimeException e) {
			throw new Exception("mongodb connection fail");
		}
		catch(Exception e) {
			throw new Exception("mongodb connection fail");
		}
		
        return mongoClient;
	}
}