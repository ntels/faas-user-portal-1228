package kr.co.ntels.faas.function.service;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.PostConstruct;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import com.fasterxml.jackson.databind.ObjectMapper;

import kr.co.ntels.faas.common.apiCommon.model.APIJsonData;
import kr.co.ntels.faas.common.apiCommon.model.Constant.E_ORDERBY;
import kr.co.ntels.faas.common.apiCommon.util.JsonUtil;
import kr.co.ntels.faas.constant.FaaSCommCD;
import kr.co.ntels.faas.dao.FaasDao;
import kr.co.ntels.faas.function.service.kong.KongAPIBase;
import kr.co.ntels.faas.userException.ExceptionEx;

@Service
public class KongService {
	
	public class KongRoute {
		
		List<Object> protocols = new ArrayList<>();
		Map<String,Object> service = new HashMap<>();
		String name;
		boolean preserve_host = false;
		int regex_priority = 0;
		boolean strip_path = true;
		List<Object> paths = new ArrayList<>();
		List<Object> hosts = new ArrayList<>();
		List<Object> methods = new ArrayList<>();
		
		public KongRoute()
		{
			protocols.add("http");
			methods.add("GET");
			methods.add("POST");
		}
		
		public void putService(String key, String val)
		{
			service.put(key, val);
		}
		
		public void addHosts(String host)
		{
			hosts.add(host);
		}
		
		public void addPaths(String path)
		{
			paths.add(path);
		}

		
		public List<Object> getProtocols() {
			return protocols;
		}

		public void setProtocols(List<Object> protocols) {
			this.protocols = protocols;
		}

		public Map<String, Object> getService() {
			return service;
		}

		public void setService(Map<String, Object> service) {
			this.service = service;
		}

		public String getName() {
			return name;
		}

		public void setName(String name) {
			this.name = name;
		}

		public boolean isPreserve_host() {
			return preserve_host;
		}

		public void setPreserve_host(boolean preserve_host) {
			this.preserve_host = preserve_host;
		}

		public int getRegex_priority() {
			return regex_priority;
		}

		public void setRegex_priority(int regex_priority) {
			this.regex_priority = regex_priority;
		}

		public boolean isStrip_path() {
			return strip_path;
		}

		public void setStrip_path(boolean strip_path) {
			this.strip_path = strip_path;
		}

		public List<Object> getPaths() {
			return paths;
		}

		public void setPaths(List<Object> paths) {
			this.paths = paths;
		}

		public List<Object> getHosts() {
			return hosts;
		}

		public void setHosts(List<Object> hosts) {
			this.hosts = hosts;
		}

		public List<Object> getMethods() {
			return methods;
		}

		public void setMethods(List<Object> methods) {
			this.methods = methods;
		}
		
	}
	
	
	@Value("${kong.api.url}")	String kongAPIUrl;
	
	@Value("${kong.service.url}") String kongServiceUrl;
	
	@Autowired
	FaasDao dao;
	
	@PostConstruct
	public void init()
	{
		if("/".equals(kongAPIUrl.substring(kongAPIUrl.length()-1)))
			kongAPIUrl = kongAPIUrl.substring(0,kongAPIUrl.length()-1);
		
		if("/".equals(kongServiceUrl.substring(kongServiceUrl.length()-1)))
			kongServiceUrl = kongServiceUrl.substring(0,kongServiceUrl.length()-1);
	}
	
	private String getServiceName(String namespace ,String functionName, String apiName)
	{
		return String.format("%s_%s_%s", namespace , functionName, apiName);
	}
	
	private String getRouteName(String namespace ,String functionName, String apiName)
	{
		return String.format("route_%s_%s_%s", namespace , functionName , apiName);
	}
	
	private String getRoutePath(String namespace ,String functionName, String apiName)
	{
		return String.format("/%s/%s/%s", namespace , functionName , apiName);
	}
	
	public String getConUrl(String namespace ,String functionName, String apiName)
	{
		return String.format("%s/%s/%s/%s", kongServiceUrl ,namespace , functionName , apiName);
	}
	
	public boolean isExistService(String namespace ,String functionName, String apiName) throws Exception
	{
		KongAPIBase apiBase = new KongAPIBase();
		String srviceName = getServiceName(namespace,functionName,apiName);
		String url = String.format("%s/services/%s", kongAPIUrl , srviceName);
		
		APIJsonData  data = apiBase.getAPIGetData(url, "$.name");
		if(data.IsSuccess())
		{
			return srviceName.equals(data.toString());
		}
		else
		{
			if(data.getHttpStatus() == 404)
				return false;
			
			throw new ExceptionEx("[isExistService 조회실패] : ");
		}
	}
	
	public String registerApiGW(String function_id, String namespace ,String functionName, String apiName, String functionUrl  ) throws Exception
	{
		String apiKey = "";
		Map<String,Object> param = new HashMap<>();
		param.put("function_id", function_id);
		param.put("apiName", apiName); // kong service name
		param.put("serviceName", getServiceName(namespace,functionName,apiName)); // kong service name
		param.put("routeName", getRouteName(namespace,functionName,apiName)); 
		param.put("routePath", getRoutePath(namespace,functionName,apiName)); 
		param.put("functinUrl",functionUrl);
		param.put("namespace", namespace);
		boolean bTraffic = readTrafficSetting(param);
		
		try
		{
			setService(param);
			setRoute(param);
			setConsumer(param);
			setGroupAddConsumer(param);
			setRouteACL(param);
			apiKey = setConsumerApiKey(param);
			if(bTraffic)
				setRateLimiting(param);
		}
		catch(ExceptionEx e)
		{
			deleteApiGW(namespace,functionName,apiName);
			throw e;
		}
		return apiKey;
	}
	
	public void updateFunctionTraffics(String function_id) throws Exception
	{
		Map<String,Object> param = new HashMap<>();
		param.put("function_id", function_id);
		readTrafficSetting(param);
		
		List<Map<String,Object>> listTrigger =  dao.select("faas.trigger.selectFunctionHttpTrigger", param);
		for(int i=0; i<listTrigger.size(); i++)
		{
			Map<String,Object> ret = listTrigger.get(i);
			
			if(FaaSCommCD.E_TRIGGER_STAT.INIT.toString().equals(ret.get("trigger_stat_cd").toString()))
				continue;
			
			String namespace = ret.get("namespace").toString();
			String functionName = ret.get("dsp_name").toString();
			String apiName = ret.get("api_name").toString();
			
			param.put("routeName", getRouteName(namespace,functionName,apiName)); 
			
			setRateLimiting(param);
		}
	}
	
	public boolean deleteApiGW(String namespace ,String functionName, String apiName) throws Exception
	{
		String url = "";
		KongAPIBase apiBase = new KongAPIBase();
		String serviceName = getServiceName(namespace,functionName, apiName);
		String routeName = getRouteName(namespace,functionName, apiName);
		
		//route
		url = String.format("%s/routes/%s", kongAPIUrl , routeName);
		if(!apiBase.getAPIDelete(url, ""))
			throw new ExceptionEx("[route 삭제실패] : ");
		//service
		url = String.format("%s/services/%s", kongAPIUrl , serviceName);
		if(!apiBase.getAPIDelete(url, ""))
			throw new ExceptionEx("[route 삭제실패] : ");
		//consumer
		url = String.format("%s/consumers/%s", kongAPIUrl , serviceName);
		if(!apiBase.getAPIDelete(url, ""))
			throw new ExceptionEx("[route 삭제실패] : ");
		
		
		return true;
	}
	
	protected void setService(Map<String,Object> param) throws Exception
	{
		KongAPIBase apiBase = new KongAPIBase();
		String url = String.format("%s/services", kongAPIUrl);
		String postData = "";
		
		String serviceName = param.get("serviceName").toString();
		String functinUrl = param.get("functinUrl").toString();
		
		postData = String.format("name=%s&url=%s", serviceName, String.format("http://%s", functinUrl));
		
		
		APIJsonData  data = apiBase.getAPIPost(url, postData, "");
		if(!data.IsSuccess())
		{
			throw new ExceptionEx("[service 등록실패] : ");
		}
	}
	
	protected void setRoute(Map<String,Object> param) throws Exception
	{
		ObjectMapper mapper = new ObjectMapper();
		KongAPIBase apiBase = new KongAPIBase();
		String url = String.format("%s/routes", kongAPIUrl);
		KongRoute route = new KongRoute();
		String postData = "";
		String serviceName = param.get("serviceName").toString();
		String routeName = param.get("routeName").toString();
		String routePath = param.get("routePath").toString();
		
		apiBase.addHeader("Content-Type", "application/json");
		
		route.putService("name", serviceName);
		route.setName(routeName);
		route.addPaths(routePath);

		postData = mapper.writeValueAsString(route);
		APIJsonData  data = apiBase.getAPIPost(url, postData, "");
		if(!data.IsSuccess())
		{
			throw new ExceptionEx("[route 등록실패] : ");
		}
	}
	
	protected void setConsumer(Map<String,Object> param) throws Exception
	{
		KongAPIBase apiBase = new KongAPIBase();
		String url = String.format("%s/consumers/", kongAPIUrl);
		String postData = "";
		String serviceName = param.get("serviceName").toString();
		
		postData = String.format("username=%s", serviceName);
		
		APIJsonData  data = apiBase.getAPIPost(url, postData, "");
		if(!data.IsSuccess())
		{
			throw new ExceptionEx("[consumer 등록실패] : ");
		}
	}
	
	protected void setGroupAddConsumer(Map<String,Object> param) throws Exception
	{
		ObjectMapper mapper = new ObjectMapper();
		Map<String,Object> jsonMap = new HashMap<>();
		KongAPIBase apiBase = new KongAPIBase();
		String serviceName = param.get("serviceName").toString();
		String url = String.format("%s/consumers/%s/acls", kongAPIUrl,serviceName);
		String postData = "";
		apiBase.addHeader("Content-Type", "application/json");
		
		
		jsonMap.put("group", serviceName);
		postData = mapper.writeValueAsString(jsonMap);
		APIJsonData  data = apiBase.getAPIPost(url, postData, "");
		if(!data.IsSuccess())
		{
			throw new ExceptionEx("[consumer 등록실패] : ");
		}
	}
	
	protected void setRouteACL(Map<String,Object> param) throws Exception
	{
		ObjectMapper mapper = new ObjectMapper();
		Map<String,Object> jsonMap = new HashMap<>();
		KongAPIBase apiBase = new KongAPIBase();
		String routeName = param.get("routeName").toString();
		String serviceName = param.get("serviceName").toString();
		String url = String.format("%s/routes/%s/plugins", kongAPIUrl,routeName);
		String postData = "";
		apiBase.addHeader("Content-Type", "application/json");
		
		
		jsonMap.put("name", "key-auth");
		postData = mapper.writeValueAsString(jsonMap);
		APIJsonData  data1 = apiBase.getAPIPost(url, postData, "");
		if(!data1.IsSuccess())
		{
			throw new ExceptionEx("[consumer 등록실패] : ");
		}
		
		postData = String.format("{\"name\":\"acl\", \"config\":{\"whitelist\":[\"%s\"]}}", serviceName);
		APIJsonData  data = apiBase.getAPIPost(url, postData, "");
		if(!data.IsSuccess())
		{
			throw new ExceptionEx("[consumer 등록실패] : ");
		}
	}
	
	protected String setConsumerApiKey(Map<String,Object> param) throws Exception
	{
		KongAPIBase apiBase = new KongAPIBase();
		String serviceName = param.get("serviceName").toString();
		String url = String.format("%s/consumers/%s/key-auth", kongAPIUrl,serviceName);
		String postData = "{}";
		
		
		apiBase.addHeader("Content-Type", "application/json");
		APIJsonData  data = apiBase.getAPIPost(url, postData, "$.key");
		if(data.IsSuccess())
		{
			String key = data.toString();
			System.out.println(key);
			return key;
		}
		else
		{
			throw new ExceptionEx("[consumer 등록실패] : ");
		}
	}
	
	protected void setRateLimiting(Map<String,Object> param) throws Exception
	{
		KongAPIBase apiBase = new KongAPIBase();
		String pluginsName = "rate-limiting";
		String policy = "cluster";
		String limit_by = "service";
		String routeName = param.get("routeName").toString();
		String burst = param.get("burst").toString();
		String quotaLimit = param.get("quotaLimit").toString();
		String url = String.format("%s/routes/%s/plugins", kongAPIUrl,routeName);
		String postData = "";
		
		// 기존 설정 삭제
		APIJsonData  data = apiBase.getAPIGetData(url, "$.data[*]");
		if(data.IsSuccess())
		{
			List<APIJsonData> listJsonData = JsonUtil.getJsonArrayUnit(data, E_ORDERBY.ASC , -1);
			for(int i=0;i<listJsonData.size();i++)
			{
				APIJsonData  dataName = JsonUtil.getFilterJson(listJsonData.get(i), "$.[*].name");
				APIJsonData  dataId = JsonUtil.getFilterJson(listJsonData.get(i), "$.[*].id");
				
				String name = dataName.toString();
				String id = dataId.toString();
				
				if(pluginsName.equals(name))
				{
					String deleteUrl = String.format("%s/routes/%s/plugins/%s", kongAPIUrl,routeName,id);
					apiBase.getAPIDelete(deleteUrl, "");
				}
			}
		}
		// 기존 설정 삭제
		
		postData = String.format("name=%s&config.limit_by=%s&config.policy=%s&config.second=%s&%s", pluginsName,limit_by, policy, burst , quotaLimit);
		data = apiBase.getAPIPost(url, postData, "");
		if(!data.IsSuccess())
		{
			throw new ExceptionEx("[트래픽설정 등록실패] : ");
		}
		
	}
	
	protected boolean readTrafficSetting(Map<String,Object> param) throws Exception
	{
		String function_id  = param.get("function_id").toString();
		Map<String,Object> paramMap = new HashMap<>();
		paramMap.put("function_id", function_id);
		
		Map<String,Object> function = (Map<String,Object>)dao.selectOne("faas.function.function", paramMap);
		
		if(function.get("trf") != null)
		{
			Map<String,Object> setMap = new ObjectMapper().readValue(function.get("trf").toString(), Map.class);
			
			String burst = setMap.get("burst").toString();
			String type = ((Map<String,Object>)setMap.get("quota")).get("type").toString();
			String val = ((Map<String,Object>)setMap.get("quota")).get("cnt").toString();
			
			switch(type)
			{
			case "H": type = String.format("config.hour=%s", val); break;
			case "D": type = String.format("config.day=%s", val); break;
			case "M": type = String.format("config.month=%s", val); break;
			case "Y": type = String.format("config.year=%s", val); break;
			default: throw new ExceptionEx("기간별 제한 설정이 지원하지 않는 속성입니다.");
			}
			
			param.put("burst", burst);
			param.put("quotaLimit", type);
		
			return true;
		}
		else
			return false;
		
	}

}
