/**
 * 
 */
package kr.co.ntels.faas.function.model.api.trigger;

import java.util.HashMap;
import java.util.Map;
import java.util.Random;

import com.fasterxml.jackson.databind.ObjectMapper;

import kr.co.ntels.faas.common.apiCommon.util.JsonSearch;
import kr.co.ntels.faas.common.apiCommon.util.StringBufferUtil;

/**
  * @FileName : TriggerInfo.java
  * @Project : faas-portal
  * @Date : 2020. 2. 18. 
  * @작성자 : sig61 - 백광욱
  * @변경이력 :
  * @프로그램 설명 :
  */

public abstract class TriggerInfo {
	
	@JsonSearch(path="$.function_id")
	String function_id;
	
	@JsonSearch(path="$.trigger_id")
	String trigger_id;
	
	@JsonSearch(path="$.trigger_tp")
	String trigger_tp;

	@JsonSearch(path="$.[*].metadata.name , $.name")
	String name;
	
	@JsonSearch(path="$.[*].metadata.annotations.ext_service_name")
	String serviceName="";
	
	@JsonSearch(path="$.[*].metadata.namespace , $.namespace")
	String namespace;
	
	@JsonSearch(path="$.[*].spec.channel.name , $.channel_name")
	String channelName;
	
	@JsonSearch(path="$.[*].metadata.name , $.subsc_name")
	String subscriptingName;
	
	@JsonSearch(path="$.[*].metadata.annotations.ext_kamel_name , $.kamel_name")
	String kamelName;
	
	@JsonSearch(path="$.dsp_name")
	String dspName;
	
	@JsonSearch(path="$.trigger_stat_cd")
	String trigger_stat_cd;
	
	@JsonSearch(path="$.del_yn")
	String delYN;
	
	
	//@JsonSearch(path="$.[*].metadata.annotations.ext_param" , scope="function")
	//Map<String, Object> param = new HashMap<>();
	
	
	public final static String channelPrefix = "faas-channel-";
	public final static String subscriptionPrefix = "faas-subscription-";
	public final static String kamelPrefix = "faas-kamel-";
	
	
	public TriggerInfo() {

	}
	
	public TriggerInfo(String serviceName , String namespace , Map<String, Object> param)
	{
		this.serviceName = serviceName;
		this.namespace = namespace;

		channelName = param.get("channel_name").toString();
		subscriptingName = param.get("subsc_name").toString();
		kamelName = param.get("kamel_name").toString();
	}
	

	
	public abstract String getType();
	public abstract String toKamelSource();

	public String getFunction_id() {
		return function_id;
	}

	public void setFunction_id(String function_id) {
		this.function_id = function_id;
	}

	public String getTrigger_id() {
		return trigger_id;
	}

	public void setTrigger_id(String trigger_id) {
		this.trigger_id = trigger_id;
	}

	public String getTrigger_tp() {
		return trigger_tp;
	}

	public void setTrigger_tp(String trigger_tp) {
		this.trigger_tp = trigger_tp;
	}

	public String getName() { return name;}
	public String getServiceName() { return serviceName;}
	public String getNamespace() {return namespace;}
	public String getChannelName() { return channelName;}
	public String getSubscriptionName()	{ return subscriptingName;}
	public String getKamelName() { return kamelName;}
	
	public void setKamelName(String name)
	{
		kamelName = name;
	}
	
	public String getDspName() {
		return dspName;
	}

	public void setDspName(String dspName) {
		this.dspName = dspName;
	}

	public String getTrigger_stat_cd() {
		return trigger_stat_cd;
	}

	public void setTrigger_stat_cd(String trigger_stat_cd) {
		this.trigger_stat_cd = trigger_stat_cd;
	}

	public String getDelYN() {
		return delYN;
	}

	public void setDelYN(String delYN) {
		this.delYN = delYN;
	}

	public String toChannelYaml()
	{
		StringBufferUtil sb = new StringBufferUtil();
		sb.append("apiVersion: messaging.knative.dev/v1alpha1");
		sb.append("kind: InMemoryChannel");
		sb.append("metadata:");
		sb.append("  name: " + getChannelName());
		sb.append("  namespace: @namespace@");
		sb.append("  annotations: ");
		sb.append("    ext_service_name: @serviceName@");
		
		String str = sb.toString();
		str = str.replace("@namespace@",  getNamespace())
				.replace("@serviceName@", getServiceName())
		;

		str = str.replaceAll("\t", " ");
		
		return str;
	}
	
	public String toSubscriptionYaml()
	{
		StringBufferUtil sb = new StringBufferUtil();
		sb.append("apiVersion: messaging.knative.dev/v1alpha1");
		sb.append("kind: Subscription");
		sb.append("metadata:");
		sb.append("	name: " + getSubscriptionName());
		sb.append("	namespace: @namespace@");
		sb.append("	annotations: ");
		sb.append("		ext_service_name: @serviceName@");
		sb.append("		ext_trigger_type: " + getType());
		sb.append("		ext_kamel_name: " + getKamelName());
		sb.append("spec:");
		sb.append("	channel:");
		sb.append("		apiVersion: messaging.knative.dev/v1alpha1");
		sb.append("		kind: InMemoryChannel");
		sb.append("		name: " + getChannelName());
		sb.append("	subscriber:");
		sb.append("		ref:");
		sb.append("			apiVersion: serving.knative.dev/v1");
		sb.append("			kind: Service");
		sb.append("			name: @serviceName@");
		sb.append("			namespace: @namespace@");
		
		String str = sb.toString();
		str = str.replaceAll("@","@@");
		str = str.replace("@namespace@", getNamespace())
				.replace("@serviceName@", getServiceName())
		;
		str = str.replaceAll("\t", " ");
		str = str.replaceAll("@", "\"");
		
		return str;
	}
	
	public String toKamelSourceYaml()
	{
		StringBufferUtil sb = new StringBufferUtil();
		sb.append("apiVersion: camel.apache.org/v1alpha1");
		sb.append("kind: Integration");
		sb.append("metadata:");
		sb.append("	name: " + getKamelName());
		sb.append("	namespace: @namespace@");
		sb.append("	annotations: ");
		sb.append("		ext_service_name: @serviceName@");
		sb.append("spec:");
		sb.append("	sources:");
		sb.append("		- content: " + toKamelSource().replaceAll("\r", "").replaceAll("\n", ""));
		sb.append("		  name: groovy-" + getKamelName());
		
		String str = sb.toString();
		str = str.replaceAll("@","@@");
		str = str.replace("@namespace@", getNamespace())
				.replace("@serviceName@", getServiceName())
		;
		str = str.replaceAll("\t", " ");
		str = str.replaceAll("@", "\"");
		
		return str;
	}
	
}
