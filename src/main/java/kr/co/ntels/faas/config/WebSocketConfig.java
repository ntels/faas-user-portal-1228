/**
 * 
 */
package kr.co.ntels.faas.config;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.socket.config.annotation.EnableWebSocket;
import org.springframework.web.socket.config.annotation.WebSocketConfigurer;
import org.springframework.web.socket.config.annotation.WebSocketHandlerRegistry;

import kr.co.ntels.faas.webSocketHandler.FunctionWebSocketHandler;
import kr.co.ntels.faas.webSocketHandler.TemplateWebSocketHandler;

/**
  * @FileName : WebSocketConfig.java
  * @Project : test
  * @Date : 2020. 3. 20. 
  * @작성자 : sig61 - 백광욱
  * @변경이력 :
  * @프로그램 설명 :
  */

@Configuration
@EnableWebSocket
public class WebSocketConfig implements WebSocketConfigurer {
	
	
	@Autowired
	FunctionWebSocketHandler functionWebSocketHandler;
	
	@Autowired
	TemplateWebSocketHandler templateWebSocketHandler;
	
	
	public void registerWebSocketHandlers(WebSocketHandlerRegistry registry) {
		
		registry.addHandler(functionWebSocketHandler, "/service.do").setAllowedOrigins("*");
		
		registry.addHandler(templateWebSocketHandler, "/template.do").setAllowedOrigins("*");
		
	}
	
	
}
