package kr.co.ntels.faas.log.service;

import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import kr.co.ntels.faas.constant.FaaSCommCD;
import kr.co.ntels.faas.dao.FaasDao;
import kr.co.ntels.faas.function.model.FunctionLogVO;
import kr.co.ntels.faas.function.model.api.trigger.TriggerInfoFactory;

/**
  * @FileName : LogService.java
  * @Project : faas-user-portal
  * @Date : 2020. 6. 4. 
  * @작성자 : sig61 - 백광욱
  * @변경이력 :
  * @프로그램 설명 :
  */
@Service
public class LogService {
	
	@Autowired	FaasDao dao;
	
	
	public List<Map<String, Object>> getLogList(Map<String, Object> param)
	{
		return dao.select("faas.log.logList", param);
	}
	
	public void saveInfoLog(String namespace , String functionName , String log)
	{
		FunctionLogVO vo = new FunctionLogVO();
		vo.setNamespace(namespace);
		vo.setName(functionName);
		vo.setLog(log);
		
		dao.update("faas.log.insertInfoFunctionLog", vo);
	}
	
	public void saveErrorLog(String namespace , String functionName , String log)
	{
		FunctionLogVO vo = new FunctionLogVO();
		vo.setNamespace(namespace);
		vo.setName(functionName);
		vo.setLog(log);
		
		dao.update("faas.log.insertErrorFunctionLog", vo);
	}
	
	public void saveModifyLog(String function_id , FaaSCommCD.E_LOG_DTL e_log_dtl, String triggerType , String revisionId)
	{
		String log;
		FunctionLogVO vo = new FunctionLogVO();
		
		switch(e_log_dtl)
		{
			case ENABLE: 		log = "비활성 -> 활성";	break;
			case DISABLE:		log = "활성 -> 비활성";	break;
			case MODIFY:		log = String.format("함수수정%s", "(설정변경)");	break;
			case REVISION:		log = String.format("함수수정(Revision : %s)", revisionId);	
								vo.setRevisionId(revisionId);
								break;								
			case ADD_TRIGGER:	log = String.format("트리거 추가(%s)", triggerType.toString());
								vo.setTriggerTp(triggerType);
								break;
			case DEL_TRIGGER:	log = String.format("트리거 삭제(%s)", triggerType.toString());	
								vo.setTriggerTp(triggerType);
								break;
			case ADD_APP:		log = String.format("백엔드 추가(%s)", "");	break;
			case DEL_APP:		log = String.format("백엔드 삭제(%s)", "");	break;
			default: return;
		}
		
		vo.setFunctionId(function_id);
		vo.setLogCd(FaaSCommCD.E_LOG.MODIFY);
		vo.setLogDtlCd(e_log_dtl);
		vo.setLog(log);
		
		dao.update("faas.log.insertFunctionLogVO", vo);
	}
	
	public void saveLog(FunctionLogVO vo)
	{
		dao.update("faas.log.insertFunctionLogVO", vo);
	}

}
