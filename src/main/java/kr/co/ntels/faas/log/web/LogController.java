package kr.co.ntels.faas.log.web;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.fasterxml.jackson.databind.ObjectMapper;

import kr.co.ntels.common.web.HubpopController;
import kr.co.ntels.faas.constant.FaaSCommCD;
import kr.co.ntels.faas.function.service.FunctionService;
import kr.co.ntels.faas.log.service.LogService;

/**
  * @FileName : LogController.java
  * @Project : faas-user-portal
  * @Date : 2020. 6. 4. 
  * @작성자 : sig61 - 백광욱
  * @변경이력 :
  * @프로그램 설명 :
  */
@Controller
public class LogController {
	
	@Autowired LogService logService;
	@Autowired FunctionService functionService;
	
	@RequestMapping(value="/log/getLogList.json")
	@ResponseBody
	public String getLogList(HttpServletRequest request , @RequestParam(name="param") String param1) throws Exception {
		
		Map<String,Object> paramMap = new ObjectMapper().readValue(param1, Map.class);
		
		int totalCount = 0;
		int curpage = Integer.parseInt(paramMap.get("curpage").toString());
		int limit = 12;
		int offset = functionService.getOffsetInPage(curpage , limit);
		
		String namespace = FaaSCommCD.getNamespace(request);
		String function_id = paramMap.get("function_id").toString();
		String sch_log_cd = paramMap.get("sch_log_cd").toString();
		Map<String,Object> param = new HashMap<>();
		Map<String,Object> result = new HashMap<>();
		
		param.put("function_id", function_id);
		param.put("namespace", namespace);
		param.put("sch_log_cd", sch_log_cd);
		param.put("limit", limit);
		param.put("offset", offset);
		
		List<Map<String,Object>> functionList = functionService.getFunction(param);
		List<Map<String,Object>> list = logService.getLogList(param);
		
		if(list.size() > 0) totalCount = Integer.parseInt(list.get(0).get("total_count").toString());
		
		result.put("functionlist", functionList);
		result.put("list", list);
		result.put("totalCount", totalCount+"");
		result.put("curpage", curpage);
		result.put("lastpage", functionService.getLastPage(totalCount, limit));

		
		return new ObjectMapper().writeValueAsString(result);
	}
	


}
