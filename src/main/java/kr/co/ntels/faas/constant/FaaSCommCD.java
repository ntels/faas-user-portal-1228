package kr.co.ntels.faas.constant;

import javax.servlet.http.HttpServletRequest;

import kr.co.ntels.common.web.HubpopController;

/**
  * @FileName : FaaSCommCD.java
  * @Project : faas-user-portal
  * @Date : 2020. 5. 8. 
  * @작성자 : sig61 - 백광욱
  * @변경이력 :
  * @프로그램 설명 :
  */
public class FaaSCommCD {
	
	public enum E_REG_STEP { NONE, STEP1, STEP2, STEP3, STEP4, STEP5}
	public enum E_STAT { ERROR, INIT, DISABLE, NORMAL, REG ,SYNC_ERROR }
	public enum E_TRIGGER_STAT { REG, NORMAL, ERROR, INIT }
	public enum E_LOG {ERROR, MODIFY , INFO}
	public enum E_LOG_DTL {ENABLE,DISABLE,MODIFY,REVISION,ADD_TRIGGER,DEL_TRIGGER,ADD_APP,DEL_APP,INFO}
	public enum E_DB_TYPE {MONGODB, POSTGRESQL}
	public enum E_TRIGGER_TYPE {HTTP, DB, STORAGE, TIMER}
	public enum E_TARGET_RESULT {success, fail}

	final public static String projectPrefix = "faas-p";
	public static String getNamespace(HttpServletRequest request)
	{
		return String.format("%s%s", projectPrefix , HubpopController.getHubpopProjectIdCookie(request));
	}

	public static String getNamespace(String projectId)
	{
		return String.format("%s%s", projectPrefix , projectId);
	}

	public static String getProjectPrefix() {
		return projectPrefix;
	}
}
