package kr.co.ntels.admin.common.service;

import org.springframework.stereotype.Service;

@Service
public class AdminCommonService {
    public int getOffsetInPage(int curpage, int listCount) throws Exception {
		return (curpage - 1) * listCount;
	}

	public int getLastPage(int totalCount, int listCount) throws Exception {
		int lastPage = totalCount / listCount;
		 
		if (totalCount % listCount > 0) {
			lastPage++;
		}
		
		return lastPage;
	}
}

