package kr.co.ntels.admin.common.domain;

import java.util.List;
import java.util.Map;

public class Paging {
	 private List<?> lists;
	  
	  private int totalCount = 0;
	  
	  private int page = 1;
	  
	  private int perPage = 10;
	  
	  private int perSize = 10;
	  
	  private Map<String, Object> map;
	  
	  public Paging(List<?> lists, int totalCount, int perPage, int page) {
	    this.lists = lists;
	    this.totalCount = totalCount;
	    this.perPage = perPage;
	    this.page = page;
	  }
	  
	  public List<?> getLists() {
	    return this.lists;
	  }
	  
	  public void setLists(List<?> lists) {
	    this.lists = lists;
	  }
	  
	  public int getTotalCount() {
	    return this.totalCount;
	  }
	  
	  public void setTotalCount(int totalCount) {
	    this.totalCount = totalCount;
	  }
	  
	  public int getPage() {
	    return this.page;
	  }
	  
	  public void setPage(int page) {
	    this.page = page;
	  }
	  
	  public int getPerPage() {
	    return this.perPage;
	  }
	  
	  public void setPerPage(int perPage) {
	    this.perPage = perPage;
	  }
	  
	  public int getPerSize() {
	    return this.perSize;
	  }
	  
	  public void setPerSize(int perSize) {
	    this.perSize = perSize;
	  }
	  
	  public Map<String, Object> getMap() {
	    return this.map;
	  }
	  
	  public void setMap(Map<String, Object> map) {
	    this.map = map;
	  }
	  
	  public String toString() {
	    return "Paging [lists=" + this.lists + ", totalCount=" + this.totalCount + 
	      ", page=" + this.page + ", perPage=" + this.perPage + ", perSize=" + 
	      this.perSize + ", map=" + this.map + "]";
	  }
	  
	  public static int convertPage(int totalCount, int perPage, int page) {
	    if (totalCount > 0) {
	      int overridePage = (int)Math.ceil((totalCount / perPage));
	      if (page > overridePage)
	        return overridePage; 
	    } 
	    return page;
	  }
}
