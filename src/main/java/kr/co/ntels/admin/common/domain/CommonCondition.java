package kr.co.ntels.admin.common.domain;

public class CommonCondition {
	private Integer page = 1;
	private Integer perPage=10;
	private Integer start;
	private Integer end;
	private String orderBy;
	private String language;
	
	private String dbDatePattern;
	

	public Integer getPage() {
		return page;
	}
	public void setPage(Integer page) {
		this.page = page;
	}
	public Integer getPerPage() {
		return perPage;
	}
	public void setPerPage(Integer perPage) {
		this.perPage = perPage;
	}
	public Integer getStart() {
		return start;
	}
	public void setStart(Integer start) {
		this.start = start;
	}
	public Integer getEnd() {
		return end;
	}
	public void setEnd(Integer end) {
		this.end = end;
	}
	public String getOrderBy() {
		return orderBy;
	}
	public void setOrderBy(String orderBy) {
		this.orderBy = orderBy;
	}
	public String getLanguage() {
		return language;
	}
	public void setLanguage(String language) {
		this.language = language;
	}	
	public String getDbDatePattern() {
		return dbDatePattern;
	}
	public void setDbDatePattern(String dbDatePattern) {
		this.dbDatePattern = dbDatePattern;
	}
	
	@Override
	public String toString() {
		return "CommonCondition [page=" + page + ", perPage=" + perPage
				+ ", start=" + start + ", end=" + end
				+ ", orderBy=" + orderBy + ", language="
				+ language + "]";
	}
}
