package kr.co.ntels.admin.common.dao;

import java.sql.SQLException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.mybatis.spring.SqlSessionTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

@Repository
public class AdminCommonDAO {
	
	@Autowired
	private SqlSessionTemplate sqlSessionTemplate;

	public int update(String mapper, Map<String,Object> param){
		
		return sqlSessionTemplate.update(mapper, param);
	}
	
	public List<Map<String, Object>> select(String mapper, Map<String,Object> param){
		
		return sqlSessionTemplate.selectList(mapper, param);
	}
	

	
	/**
	 * @param statement Mybatis Statement
	 * 공통 INSERT
	 * @param parameter Parameter Object
	 * @return insert 성공 횟수
	 */
	public int insert(String statement, Object parameter) throws SQLException {
		return sqlSessionTemplate.insert(statement, parameter);
	}

	/**
	 * 공통 SELECT(LIST)
	 * @param statement Mybatis Statement
	 * @param parameter 파라미터 Object
	 * @return SELECT한 List
	 */
	@SuppressWarnings("rawtypes")
	public List selectList(String statement, Object parameter) throws SQLException {
		return sqlSessionTemplate.selectList(statement, parameter);
	}
	
	/**
	 * 공통 SELECT(LIST)
	 * @param statement Mybatis Statement
	 * @param parameter 파라미터 Object
	 * @return SELECT한 List
	 */
	@SuppressWarnings("rawtypes")
	public List selectList(String statement) throws SQLException {
		return sqlSessionTemplate.selectList(statement);
	}	
	
	/**
	 * 공통 SELECT(Object)
	 * @param statement Mybatis Statement
	 * @param parameter 파라미터 Object
	 * @return SELECT한 Object
	 */
	public int selectCount(String statement, Object parameter) throws SQLException {
		return sqlSessionTemplate.selectOne(statement, parameter);
	}

	/**
	 * 공통 SELECT(Object)
	 * @param statement Mybatis Statement
	 * @param parameter 파라미터 Object
	 * @return SELECT한 Object
	 */
	public HashMap<String,Object> selectOne(String statement, Object parameter) throws SQLException {
		return sqlSessionTemplate.selectOne(statement, parameter);
	}
	
	/**
	 * 공통 SELECT(Object)
	 * @param statement Mybatis Statement
	 * @param parameter 파라미터 Object
	 * @return SELECT한 int
	 */
	public int selectInt(String statement, Object parameter) throws SQLException {
		return (int) sqlSessionTemplate.selectOne(statement, parameter);
	}	
	
	/**
	 * 공통 SELECT(Object)
	 * @param statement Mybatis Statement
	 * @param parameter 파라미터 Object
	 * @return SELECT한 Object
	 */
	public HashMap<String,Object> selectOne(String statement) throws SQLException {
		return sqlSessionTemplate.selectOne(statement);
	}

	/**
	 * 공통 UPDATE
	 * @param statement Mybatis Statement
	 * @param parameter 파라미터 Object
	 * @return UPDATE 성공한 횟수
	 */
	public int update(String statement, Object parameter) throws SQLException {
		return sqlSessionTemplate.update(statement, parameter);
	}

	/**
	 * 공통 DELETE
	 * @param statement Mybatis Statement
	 * @param parameter 파라미터 Object
	 * @return DELETE 성공한 횟수
	 */
	public int delete(String statement, Object parameter) throws SQLException {
		return sqlSessionTemplate.delete(statement, parameter);
	}
	

}
