package kr.co.ntels.admin.function.web;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import kr.co.ntels.admin.trigger.service.AdminTriggerService;
import kr.co.ntels.faas.constant.FaaSCommCD;
import kr.co.ntels.faas.function.service.FunctionService;
import kr.co.ntels.faas.gitlab.service.GitLabService;
import kr.co.ntels.faas.gitlab.web.GitLabController;
import kr.co.ntels.faas.monitoring.service.MonitoringService;




@RestController
@RequestMapping(value = "/service/admin")
public class AdminFunctionController {

    @Autowired FunctionService userFunctionService;
	@Autowired AdminTriggerService triggerService;
	@Autowired GitLabController gitLabController;
	@Autowired GitLabService gitLabService;
    @Autowired MonitoringService userMonitoringService;

	@GetMapping("/getFunctionList.json")
	public Map<String, Object> index(HttpServletRequest request, HttpServletResponse response, @RequestParam Map<String, Object> params) throws Exception {
		int limit = params.get("limit") != null ? Integer.parseInt(String.valueOf(params.get("limit"))) : 12;
		int offset = userFunctionService.getOffsetInPage(Integer.parseInt(String.valueOf(params.get("curpage"))), limit);
		params.put("limit", limit);
		params.put("offset", offset);
		params.put("projectPrefix", FaaSCommCD.getProjectPrefix());
		
		String namespace = params.get("projectId") != null && !params.get("projectId").equals("") 
			? FaaSCommCD.getNamespace(params.get("projectId").toString()) : "";
		params.put("namespace", namespace);

		List<Map<String, Object>> results = userFunctionService.getFunctionListForAdmin(params);
		int totalCount = userFunctionService.getFunctionCount(params);

		Map<String,Object> returnMap = new HashMap<>();
		returnMap.put("list", results);
		returnMap.put("totalCount", totalCount);
		returnMap.put("curpage", params.get("curpage"));
		returnMap.put("lastpage", userFunctionService.getLastPage(totalCount, limit));

		return returnMap;
	}

	@GetMapping("/getFunction.json")
	public Map<String, Object> show(HttpServletRequest request, HttpServletResponse response, @RequestParam Map<String, Object> params) throws Exception {
		Map<String, Object> resultMap = new HashMap<>();
		params.put("projectPrefix", FaaSCommCD.getProjectPrefix());
		
		resultMap.put("function", userFunctionService.getFunctionInfo(params));
		resultMap.put("triggers", triggerService.getList(params));
		resultMap.put("targets", userFunctionService.getTargetList(params));

		return resultMap;
	}

	@PostMapping("/deleteFunction.json")
	Map<String, Object> delete(HttpServletRequest request, HttpServletResponse response, @RequestParam Map<String, Object> params) throws Exception {
		Map<String, Object> returnMap = new HashMap<>();

		Map<String, Object> function = userFunctionService.getFunctionInfo(params);
		try {
			if (userFunctionService.deleteFunction(gitLabService, function.get("namespace").toString(), function.get("function_id").toString()) == 1) {
				returnMap.put("isSuccess", true);
			} else {
				returnMap.put("isSuccess", false);
				returnMap.put("message", "확인되지 않은 오류가 발생하였습니다.");
			}
		} catch (Exception e) {
			e.printStackTrace();
			returnMap.put("isSuccess", false);
			returnMap.put("message", "확인되지 않은 오류가 발생하였습니다.");
		}
		
		return returnMap;
	}	

	@GetMapping("/getFunctionSourceList.json")
	public List<Map<String, Object>> sourceList(HttpServletRequest request, HttpServletResponse response, @RequestParam Map<String, Object> params) throws Exception{
		Map<String, Object> function = userFunctionService.getFunctionInfo(params);
		List<Map<String, Object>> resultList = gitLabController.checkOutFromProject(function.get("git_url") + "", function.get("runtime") + "");
		
		return resultList;
	}

	@GetMapping("/getMonitoring.json")
    public Map<String, Object> requestMethodName(HttpServletRequest request, HttpServletResponse response, @RequestParam Map<String, Object> params) throws Exception {
        Map<String, Object> resultMap = new HashMap<>();
        
		String unitType = params.get("unitType").toString();
		int stepCnt = Integer.parseInt(params.get("stepCnt").toString());
		List<Map<String,String>> list = userMonitoringService.getMonitoringData(unitType, stepCnt, params);
		
		resultMap.put("list", list);
		resultMap.put("c3", userMonitoringService.getMonitoringC3Data(list));
        return resultMap;
	}
	
	@GetMapping("/getProjectList.json")
	public List<Map<String, Object>> indexOfProject(@RequestParam Map<String, Object> params) {
		return userFunctionService.getProjectList(params);
	}
	
}






