package kr.co.ntels.admin.template.service;

import java.io.IOException;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.gitlab4j.api.models.Commit;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.google.gson.Gson;

import kong.unirest.HttpResponse;
import kong.unirest.Unirest;
import kr.co.ntels.admin.common.dao.AdminCommonDAO;
import kr.co.ntels.admin.common.domain.Paging;
import kr.co.ntels.admin.dashboard.domain.FunctionVO;
import kr.co.ntels.admin.template.domain.CephVO;
import kr.co.ntels.admin.template.domain.DatabaseVO;
import kr.co.ntels.admin.template.domain.GitFile;
import kr.co.ntels.admin.template.domain.HttpVO;
import kr.co.ntels.admin.template.domain.TemplateTriggerVO;
import kr.co.ntels.admin.template.domain.TemplateVO;
import kr.co.ntels.admin.template.domain.TimerResultVO;
import kr.co.ntels.admin.template.domain.TimerVO;
import kr.co.ntels.common.utils.CommonUtils;
import kr.co.ntels.common.utils.UserDetailsHelper;
import kr.co.ntels.common.vo.User;
import kr.co.ntels.faas.function.service.FunctionService;
import kr.co.ntels.faas.gitlab.service.GitLabService;


@Service
public class AdminTemplateService {
	private @Value("${gitlab.private_token}") String private_token;
	
	@Autowired 
	AdminCommonDAO dao;
	
	@Autowired
	GitLabService gitLabService;
	
	@Autowired FunctionService service;
	
	
	@SuppressWarnings("unchecked")
	public Paging listAction(TemplateVO condition) throws SQLException
	{
		
		if (condition.getPage() == 0) {
			condition.setPage(1);
		}

		//condition.setStart(((condition.getPage() - 1) * condition.getPerPage()) + 1);
		condition.setStart(((condition.getPage() - 1) * condition.getPerPage()));
		condition.setEnd(condition.getPerPage());
		
		int count = dao.selectInt("faas.admin.template.count", condition);
		List<FunctionVO> list = null;
		if (count > 0) {
			list = dao.selectList("faas.admin.template.list", condition);
		}
			
		return new Paging(list, count, condition.getPerPage(), condition.getPage());
	}	
	
	public GitFile getGitFile(TemplateVO condition) throws IOException {
		//String restUrl = gitLabUrl+gitLabRestPath+"/projects/"+projectId+"/repository/files/"+fileName+"?ref="+ref;
		String restUrl = condition.getGit_url();
		System.out.println(restUrl);
		HttpResponse<String> response = Unirest.get(restUrl)
				.header("private-token", private_token)
				.header("cache-control", "no-cache")
				.header("gitreposervice-token", "40c3f23b-7138-519d-1a14-1d11ca2cd06f")
				.asString();

		System.out.println(response.getBody().indexOf("file_name"));
		GitFile file = new GitFile();
		if(response.getBody().indexOf("file_name") > -1) {
			file =  kr.co.ntels.admin.template.domain.Converter.fromJsonString(response.getBody());	
			file.setContent(	new String(	java.util.Base64.getDecoder().decode(	file.getContent()	)	)	);
		}
		return file;
	}	
	
	@SuppressWarnings("unchecked")
	public TemplateVO  getTempate(TemplateVO condition) throws SQLException
	{
		condition.setStart(0);
		condition.setEnd(1);
		List<TemplateVO> list = dao.selectList("faas.admin.template.list",condition);
		TemplateVO vo = new TemplateVO();
		if(list != null && list.size() > 0) {
			vo = list.get(0);	
		}
		 
		return vo;
	}	
	
	@SuppressWarnings("unchecked")
	public TemplateVO insertTemplate(TemplateVO condition) throws SQLException
	{
		dao.insert("faas.admin.template.insertTemplate", condition);
		return condition;
	}
	
	@Transactional
	public HashMap<String,Object> update(HashMap<String,Object> paramMap) throws Exception {
		Gson gson = new Gson();
		dao.delete("faas.admin.template.deleteHttpTemplate",paramMap);
		dao.delete("faas.admin.template.deleteCephTemplate",paramMap);
		dao.delete("faas.admin.template.deleteDatabaseTemplate",paramMap);
		dao.delete("faas.admin.template.deleteTimerTemplate",paramMap);
		dao.delete("faas.admin.template.deleteTemplateTrigger",paramMap);
		dao.update("faas.admin.template.updateTemplate",paramMap);
		
		//화면으로 부터받는값.
		TemplateVO templateVO = gson.fromJson(gson.toJson(paramMap), TemplateVO.class);
		paramMap.put("templateVO", templateVO);
		saveTrigger(paramMap);
		
		//소스 깃저장.
		ArrayList<Map<String,Object>> fileList = new ArrayList<Map<String,Object>>();
		HashMap<String,Object> fileMap = new HashMap<String, Object>();
		fileMap.put("fileName", "index.js");
		fileMap.put("fileContent", paramMap.get("fileContent1"));
		fileList.add(fileMap);
		
		fileMap = new HashMap<String, Object>();
		fileMap.put("fileName", "package.json");
		fileMap.put("fileContent", paramMap.get("fileContent2"));
		fileList.add(fileMap);
		
		User user = UserDetailsHelper.getAuthenticatedUser();
		paramMap.put("git_usr",user.getId());
		List<Map<String, Object>> gitLabUserList = service.getGitLabUser(paramMap);
		String password = gitLabUserList.get(0).get("git_pwd").toString();
		String projectPath = templateVO.getGit_url();
		Commit commit = gitLabService.commitToProject(fileList,projectPath,user.getId(),password);		
		return paramMap;
	}
	
	@Transactional
	public HashMap<String,Object> delete(HashMap<String,Object> paramMap) throws Exception {
		Gson gson = new Gson();
		dao.delete("faas.admin.template.deleteHttpTemplate",paramMap);
		dao.delete("faas.admin.template.deleteCephTemplate",paramMap);
		dao.delete("faas.admin.template.deleteDatabaseTemplate",paramMap);
		dao.delete("faas.admin.template.deleteTimerTemplate",paramMap);
		dao.delete("faas.admin.template.deleteTemplateTrigger",paramMap);
		dao.delete("faas.admin.template.deleteTemplate",paramMap);
		
/*		//화면으로 부터받는값.
		TemplateVO templateVO = gson.fromJson(gson.toJson(paramMap), TemplateVO.class);
		paramMap.put("templateVO", templateVO);
		saveTrigger(paramMap);
		
		//소스 깃저장.
		ArrayList<Map<String,Object>> fileList = new ArrayList<Map<String,Object>>();
		HashMap<String,Object> fileMap = new HashMap<String, Object>();
		fileMap.put("fileName", "index.js");
		fileMap.put("fileContent", paramMap.get("fileContent1"));
		fileList.add(fileMap);
		
		fileMap = new HashMap<String, Object>();
		fileMap.put("fileName", "package.json");
		fileMap.put("fileContent", paramMap.get("fileContent2"));
		fileList.add(fileMap);
		
		User user = UserDetailsHelper.getAuthenticatedUser();
		paramMap.put("git_usr",user.getId());
		List<Map<String, Object>> gitLabUserList = service.getGitLabUser(paramMap);
		String password = gitLabUserList.get(0).get("git_pwd").toString();
		String projectPath = templateVO.getGit_url();
		Commit commit = gitLabService.commitToProject(fileList,projectPath,user.getId(),password);		*/
		return paramMap;
	}	
	
	@Transactional
	public HashMap<String,Object> saveTrigger(HashMap<String,Object> paramMap) throws Exception {
		Gson gson = new Gson();
		TemplateVO templateVO = (TemplateVO) paramMap.get("templateVO");
		//HTTP API데이터 설정
				if(paramMap.get("httpIndex") != null) {
					String httpIndex = (String) paramMap.get("httpIndex");
					String[] httpIndexArr = org.apache.commons.lang.StringUtils.split(httpIndex,",");
					for(int i=0;i<httpIndexArr.length;i++) {
						//TODO : 반복되는 부분이니 함수로 만들어서 처리할것.
						paramMap.put("trigger_tp","http");
						paramMap.put("template_id",templateVO.getTemplate_id());
						String adminYN = "radioHttp"+httpIndexArr[i];
						adminYN = (String) paramMap.get(adminYN);
						adminYN = ("admin".equals(adminYN) ? "Y" : "N");
						paramMap.put("adm_set_yn",adminYN);
						TemplateTriggerVO templateTriggerVO = gson.fromJson(gson.toJson(paramMap), TemplateTriggerVO.class);
						dao.insert("faas.admin.template.insertTemplateTrigger",templateTriggerVO);
						if("Y".equals(adminYN)) {
							String httpApi = "httpApi"+httpIndexArr[i];
							httpApi = (String) paramMap.get(httpApi);
							System.out.println(httpApi);
							paramMap.put("api_name",httpApi);
							int trigger_id = templateTriggerVO.getTrigger_id();
							paramMap.put("trigger_id",trigger_id);
							HttpVO httpVO = gson.fromJson(gson.toJson(paramMap), HttpVO.class);
							System.out.println(paramMap.toString());
							dao.insert("faas.admin.template.insertHttpTemplate",httpVO);
						}
						
					}
				}

				//CEPH 데이터 설정
				if(paramMap.get("cephIndex") != null) {
					String cephIndex = (String) paramMap.get("cephIndex");
					String[] cephIndexArr = org.apache.commons.lang.StringUtils.split(cephIndex,",");
					for(int i=0;i<cephIndexArr.length;i++) {
						//TODO : 반복되는 부분이니 함수로 만들어서 처리할것.
						paramMap.put("trigger_tp","storage");
						String adminYN = "radioCeph"+cephIndexArr[i];
						adminYN = (String) paramMap.get(adminYN);
						adminYN = ("admin".equals(adminYN) ? "Y" : "N");
						paramMap.put("adm_set_yn",adminYN);
						paramMap.put("template_id",templateVO.getTemplate_id());
						TemplateTriggerVO templateTriggerVO = gson.fromJson(gson.toJson(paramMap), TemplateTriggerVO.class);
						dao.insert("faas.admin.template.insertTemplateTrigger",templateTriggerVO);
						if("Y".equals(adminYN)) {
							String chkFile = "chkFile"+cephIndexArr[i];
							String create = ("create".equals(paramMap.get(chkFile)) ? "Y" : "N" );
							String delete = ("Y".equals(create) ? "N" : "Y");
							//chkFile    = ("delete".equals(paramMap.get(chkFile)) ? "Y" : "N" );
							paramMap.put("create_yn", create);
							paramMap.put("delete_yn", delete);
							
							int trigger_id = templateTriggerVO.getTrigger_id();
							paramMap.put("trigger_id",trigger_id);
							CephVO cephVO = gson.fromJson(gson.toJson(paramMap), CephVO.class);
							System.out.println(paramMap.toString());
							dao.insert("faas.admin.template.insertCephTemplate",cephVO);
						}
					}
				}
				
				
				/*
				 * DB타입일 경우에는 mongo와 postgres의 구분이 FT_TEMPLATE_DB테이블의 
				    DB_TYPE필드에 존재하기 때문에 사용자 선택 일경우에도 FT_TEMPLATE_DB에 인서트한다.
				 */
				//Mongodb 데이터 설정
 				if(paramMap.get("mongoIndex") != null) {
					String mongoIndex = (String) paramMap.get("mongoIndex");
					String[] mongoIndexArr = org.apache.commons.lang.StringUtils.split(mongoIndex,",");
					for(int i=0;i<mongoIndexArr.length;i++) {
						//TODO : 반복되는 부분이니 함수로 만들어서 처리할것.
						paramMap.put("trigger_tp","db");
						String adminYN = "radioMongo"+mongoIndexArr[i];
						adminYN = (String) paramMap.get(adminYN);
						adminYN = ("admin".equals(adminYN) ? "Y" : "N");
						paramMap.put("adm_set_yn",adminYN);
						paramMap.put("template_id",templateVO.getTemplate_id());
						TemplateTriggerVO templateTriggerVO = gson.fromJson(gson.toJson(paramMap), TemplateTriggerVO.class);
						dao.insert("faas.admin.template.insertTemplateTrigger",templateTriggerVO);
						
							String chkmongo = "chkMongo"+mongoIndexArr[i];
							String insert =  ("insert".equals(paramMap.get(chkmongo)) ? "Y" : "N" );
							String update =  ("update".equals(paramMap.get(chkmongo)) ? "Y" : "N" );
							String delete =  ("delete".equals(paramMap.get(chkmongo)) ? "Y" : "N" );
							
							paramMap.put("insert_yn", insert);
							paramMap.put("update_yn", update);
							paramMap.put("delete_yn", delete);
							paramMap.put("db_type", "mongodb");
							System.out.println(paramMap.toString());
							int trigger_id = templateTriggerVO.getTrigger_id();
							paramMap.put("trigger_id",trigger_id);
							DatabaseVO databaseVO= gson.fromJson(gson.toJson(paramMap), DatabaseVO.class);
							System.out.println(paramMap.toString());
							dao.insert("faas.admin.template.insertDatabaseTemplate",databaseVO);	
					}
				}
				
				//Postgre 데이터 설정
				if(paramMap.get("postgreIndex") != null) {
					String postgreIndex = (String) paramMap.get("postgreIndex");
					String[] postgreIndexArr = org.apache.commons.lang.StringUtils.split(postgreIndex,",");
					for(int i=0;i<postgreIndexArr.length;i++) {
						//TODO : 반복되는 부분이니 함수로 만들어서 처리할것.
						paramMap.put("trigger_tp","db");
						String adminYN = "radioPostgre"+postgreIndexArr[i];
						adminYN = (String) paramMap.get(adminYN);
						adminYN = ("admin".equals(adminYN) ? "Y" : "N");
						paramMap.put("adm_set_yn",adminYN);
						paramMap.put("template_id",templateVO.getTemplate_id());
						TemplateTriggerVO templateTriggerVO = gson.fromJson(gson.toJson(paramMap), TemplateTriggerVO.class);
						dao.insert("faas.admin.template.insertTemplateTrigger",templateTriggerVO);
							String chkPostgre = "chkPostgre"+postgreIndexArr[i];
							String insert =  ("insert".equals(paramMap.get(chkPostgre)) ? "Y" : "N" );
							String update =  ("update".equals(paramMap.get(chkPostgre)) ? "Y" : "N" );
							String delete =  ("delete".equals(paramMap.get(chkPostgre)) ? "Y" : "N" );
							
							paramMap.put("insert_yn", insert);
							paramMap.put("update_yn", update);
							paramMap.put("delete_yn", delete);
							paramMap.put("db_type", "postgre");
							System.out.println(paramMap.toString());
							
							int trigger_id = templateTriggerVO.getTrigger_id();
							paramMap.put("trigger_id",trigger_id);
							DatabaseVO databaseVO= gson.fromJson(gson.toJson(paramMap), DatabaseVO.class);
							System.out.println(paramMap.toString());
							dao.insert("faas.admin.template.insertDatabaseTemplate",databaseVO);			
					}
				}		
				
				//timer 데이터 설정
				if(paramMap.get("timerIndex") != null) {
					String timerIndex = (String) paramMap.get("timerIndex");
					String[] timerIndexArr = org.apache.commons.lang.StringUtils.split(timerIndex,",");
					for(int i=0;i<timerIndexArr.length;i++) {
						//TODO : 반복되는 부분이니 함수로 만들어서 처리할것.
						paramMap.put("trigger_tp","timer");
						String adminYN = "radioTimer"+timerIndexArr[i];
						adminYN = (String) paramMap.get(adminYN);
						adminYN = ("admin".equals(adminYN) ? "Y" : "N");
						paramMap.put("adm_set_yn",adminYN);
						paramMap.put("template_id",templateVO.getTemplate_id());
						TemplateTriggerVO templateTriggerVO = gson.fromJson(gson.toJson(paramMap), TemplateTriggerVO.class);
						dao.insert("faas.admin.template.insertTemplateTrigger",templateTriggerVO);
						int trigger_id = templateTriggerVO.getTrigger_id();
						
			            String repeatSet = "repeatSet"+	timerIndexArr[i];
			            repeatSet = (paramMap.get(repeatSet) != null ? (String)paramMap.get(repeatSet) : null );
			            String repeat_des = null;
			            TimerVO timerVO = new TimerVO();
			            TimerResultVO timerResultVO = new TimerResultVO(); 
			            if("perMinute".equals(repeatSet))  {  //분 또는 시간 마다.
			            	timerVO.setRepeat_tp("MN");
				            String repeatVal = repeatSet+"Val"+ timerIndexArr[i];
				            repeatVal = (paramMap.get(repeatVal) != null ? (String)paramMap.get(repeatVal) : null );
				            System.out.println(repeatVal);
				            timerResultVO.setValue(repeatVal);
				            repeat_des="매 "+ repeatVal+"분 마다 반복";
			            }
			            
			            if("perHour".equals(repeatSet))  {  //분 또는 시간 마다.
			            	timerVO.setRepeat_tp("HR");
				            String repeatVal = repeatSet+"Val"+ timerIndexArr[i];
				            repeatVal = (paramMap.get(repeatVal) != null ? (String)paramMap.get(repeatVal) : null );
				            System.out.println(repeatVal);
				            timerResultVO.setValue(repeatVal);
				            repeat_des = "매 "+repeatVal+"시간 마다 반복";
			            }	            

			            if("perDay".equals(repeatSet))  {   
			            	timerVO.setRepeat_tp("DY");
			            	String weekChk = repeatSet+timerIndexArr[i];
			            	if(	"per".equals(paramMap.get(weekChk)) ) {   //매 # 일마다 반복. 
					            String repeatVal = repeatSet+"Val"+ timerIndexArr[i];
					            repeatVal = (paramMap.get(repeatVal) != null ? (String)paramMap.get(repeatVal) : null );
					            System.out.println(repeatVal);
					            repeat_des="매 "+ repeatVal+"일 마다 반복";
					            timerResultVO.setRadio("day");
					            timerResultVO.setValue(repeatVal);
			            	}else {                                       //평일 라디오 버튼 선택.
			            		repeat_des="평일 마다 반복";
			            		timerResultVO.setRadio("week");
			            	}
			            }
			            
			            String weekDesc = null;
			            if("perWeek".equals(repeatSet))  {  
			            	timerVO.setRepeat_tp("WK");
				            String repeatVal = repeatSet+"Val"+ timerIndexArr[i];
				            repeatVal = (paramMap.get(repeatVal) != null ? (String)paramMap.get(repeatVal) : null );
				            timerResultVO.setRadio("week");
				            timerResultVO.setValue(repeatVal);
				            weekDesc = repeatVal;
				            weekDesc = org.apache.commons.lang3.StringUtils.replacePattern(weekDesc, "MON", "월요일");
				            weekDesc = org.apache.commons.lang3.StringUtils.replacePattern(weekDesc, "TUE", "화요일");
				            weekDesc = org.apache.commons.lang3.StringUtils.replacePattern(weekDesc, "WED", "수요일");
				            weekDesc = org.apache.commons.lang3.StringUtils.replacePattern(weekDesc, "THU", "목요일");
				            weekDesc = org.apache.commons.lang3.StringUtils.replacePattern(weekDesc, "FRI", "금요일");
				            weekDesc = org.apache.commons.lang3.StringUtils.replacePattern(weekDesc, "SAT", "토요일");
				            weekDesc = org.apache.commons.lang3.StringUtils.replacePattern(weekDesc, "SUN", "일요일");

				            
				            repeat_des = weekDesc+" 마다 반복";
			            }
			            
			            String valDesc = null;
			         
			            if("perMonth".equals(repeatSet))  {
			            	timerVO.setRepeat_tp("MO");
			            	String type ="type"+timerIndexArr[i]+"_month";
			            	type = (String) paramMap.get(type);
			            	if("eachDay".equals(type)) {  // 매월 #일마다.
			            		String val = "perMonthVal"+timerIndexArr[i];
			            		val = (String)paramMap.get(val);
			            		timerResultVO.setRadio("month");
			            		timerResultVO.setValue(val);
			            		repeat_des = "매월 "+val+"일 마다 반복";
			            	}else {                       //#번째 #요일마다.
			            		String val = "selMonthWeek_1_Val"+timerIndexArr[i];
			            		val = (String)paramMap.get(val);
			            		if("1".equals(val)) valDesc ="첫 번째";
			            		if("2".equals(val)) valDesc ="두 번째";
			            		if("3".equals(val)) valDesc ="세 번째";
			            		if("4".equals(val)) valDesc ="네 번째";
			            		if("5".equals(val)) valDesc ="다섯번째";
			            		
			            		String week = "selMonthWeek_2_Val"+timerIndexArr[i];
			            		week = (String)paramMap.get(week);
			            		if("MON".equals(week)) weekDesc = "월요일";
			            		if("TUE".equals(week)) weekDesc = "화요일";
			            		if("WED".equals(week)) weekDesc = "수요일";
			            		if("THU".equals(week)) weekDesc = "목요일";
			            		if("FRI".equals(week)) weekDesc = "금요일";
			            		if("SAT".equals(week)) weekDesc = "토요일";
			            		if("SUN".equals(week)) weekDesc = "일요일";
			            		
			            		repeat_des=valDesc+ " "+weekDesc+ " 마다 반복";
			            		
			            		timerResultVO.setRadio("week");
			            		timerResultVO.setValue_th(val);
			            		timerResultVO.setValue_week(week);
			            	}
			            }
			            
			            if("perYear".equals(repeatSet))  {
			            	timerVO.setRepeat_tp("YR");
			            	String type ="type"+timerIndexArr[i]+"_year";
			            	type = (String) paramMap.get(type);
			            	if("eachMonth".equals(type)) {  // 매년 #월마다.
			            		String val = "perYearVal"+timerIndexArr[i];
			            		val = (String)paramMap.get(val);
			            		timerResultVO.setRadio("month");
			            		timerResultVO.setValue(val);
			            		repeat_des = "매년 "+val+"월 마다 반복";
			            		
			            	}else {                         //#번째 #요일마다.
			            		String val = "selYearWeek_1_Val"+timerIndexArr[i];
			            		val = (String)paramMap.get(val);
			            		
			            		if("1".equals(val)) valDesc ="첫 번째";
			            		if("2".equals(val)) valDesc ="두 번째";
			            		if("3".equals(val)) valDesc ="세 번째";
			            		if("4".equals(val)) valDesc ="네 번째";
			            		if("5".equals(val)) valDesc ="다섯번째";
			            		
			            		String week = "selYearWeek_2_Val"+timerIndexArr[i];
			            		week = (String)paramMap.get(week);
			            		if("MON".equals(week)) weekDesc = "월요일";
			            		if("TUE".equals(week)) weekDesc = "화요일";
			            		if("WED".equals(week)) weekDesc = "수요일";
			            		if("THU".equals(week)) weekDesc = "목요일";
			            		if("FRI".equals(week)) weekDesc = "금요일";
			            		if("SAT".equals(week)) weekDesc = "토요일";
			            		if("SUN".equals(week)) weekDesc = "일요일";
			            		
			            		repeat_des=valDesc+ " "+weekDesc+ " 마다 반복";
			            		
			            		timerResultVO.setRadio("week");
			            		timerResultVO.setValue_th(val);
			            		timerResultVO.setValue_week(week);       		
			            	}
			            }
			            
			            timerVO.setTrigger_id(trigger_id);
			            timerVO.setRepeat_des(repeat_des);
			            String repeat_val = gson.toJson(timerResultVO);
			            System.out.println(repeat_val);
			            timerVO.setRepeat_val(repeat_val);
			            
			            if(timerVO.getRepeat_tp() != null) {  //null이면 '없음 선택'
			            	dao.insert("faas.admin.template.insertTimerTemplate",timerVO);
			            }
						System.out.println(timerVO.toString());
						
					}
				}				

		return paramMap;
	}
	
	@Transactional
	public HashMap<String,Object> save(HashMap<String,Object> paramMap) throws Exception {

		Gson gson = new Gson();
		System.err.println(paramMap.toString());
		TemplateVO templateVO = gson.fromJson(gson.toJson(paramMap), TemplateVO.class);
		
		//ft_template 데이터 세팅.
		User user = UserDetailsHelper.getAuthenticatedUser();
		String faasUserId = user.getId();
		String faasDisplayName = templateVO.getName();

		templateVO.setReg_usr(faasUserId);
		
		String top_yn = (	paramMap.get("top_yn") != null ? "Y" : "N");
		templateVO.setTop_yn(top_yn);
		templateVO.setUse_yn("Y");
		
		//깃저장소 경로
		String projectName = faasDisplayName + CommonUtils.getRandomString(10);
		String groupPath = (String) paramMap.get("groupPath");
		
		boolean bl = gitLabService.createProject(groupPath,projectName);
		if(!bl) {
			paramMap.put("result", "fail");
			return paramMap;
		}
		
		//소스 깃저장.
		ArrayList<Map<String,Object>> fileList = new ArrayList<Map<String,Object>>();
		HashMap<String,Object> fileMap = new HashMap<String, Object>();
		fileMap.put("fileName", "index.js");
		fileMap.put("fileContent", paramMap.get("fileContent1"));
		fileList.add(fileMap);
		
		fileMap = new HashMap<String, Object>();
		fileMap.put("fileName", "package.json");
		fileMap.put("fileContent", paramMap.get("fileContent2"));
		fileList.add(fileMap);
		paramMap.put("git_usr",user.getId());
		List<Map<String, Object>> gitLabUserList = service.getGitLabUser(paramMap);
		String password = gitLabUserList.get(0).get("git_pwd").toString();
		String projectPath = groupPath + "/" + projectName;
		//20190908 수정
		templateVO.setName(projectName);
		templateVO.setDsp_name(faasDisplayName);
		
		Commit commit = gitLabService.commitToProject(fileList,projectPath,user.getId(),password);
		
		templateVO.setGit_url(projectPath);
		
		dao.insert("faas.admin.template.insertTemplate",templateVO);
		
		paramMap.put("templateVO", templateVO);
		paramMap = saveTrigger(paramMap);
			
		paramMap.put("result", "success");
		return paramMap;
	}
	
	@SuppressWarnings("unchecked")
	public HashMap<String,Object> getModifyData(HashMap<String,Object> paramMap) throws Exception {
		ArrayList<HashMap<String,Object>> resultList = (ArrayList<HashMap<String, Object>>) dao.selectList("faas.admin.template.getModifyData",paramMap);
		if(resultList == null ||  resultList.size() ==0) {
			return null;	
		}
		Gson gson = new Gson();
		//ft_template 데이터 가져오기
		System.out.println(resultList.get(0).toString());
		TemplateVO templateVO = new TemplateVO();
		System.out.println(resultList.get(0).get("TEMPLATE_ID"));
		templateVO.setTemplate_id((int) resultList.get(0).get("TEMPLATE_ID"));
		templateVO.setName((String) resultList.get(0).get("NAME"));
		templateVO.setDsp_name((String) resultList.get(0).get("DSP_NAME"));
		templateVO.setRuntime((String) resultList.get(0).get("RUNTIME"));
		templateVO.setVersion((String) resultList.get(0).get("VERSION"));
		templateVO.setGit_url((String) resultList.get(0).get("GIT_URL"));
		templateVO.setDes((String) resultList.get(0).get("DES"));
		templateVO.setReg_usr((String) resultList.get(0).get("REG_USR"));
		templateVO.setReg_date_hms((String) resultList.get(0).get("REG_DATE_HMS"));
		templateVO.setMod_usr((String) resultList.get(0).get("MOD_USR"));
		templateVO.setMod_date_hms((String) resultList.get(0).get("MOD_DATE_HMS"));
		templateVO.setTop_yn((String) resultList.get(0).get("TOP_YN"));
		templateVO.setUse_yn((String) resultList.get(0).get("USE_YN"));
		
		//각각의 트리거 테이블 데이터 가져오기.
		ArrayList<HttpVO> httpTriggerList = new ArrayList<HttpVO>();
		ArrayList<CephVO> cephTriggerList = new ArrayList<CephVO>();
		ArrayList<DatabaseVO> mongoTriggerList = new ArrayList<DatabaseVO>();
		ArrayList<DatabaseVO> postgreTriggerList = new ArrayList<DatabaseVO>();
		ArrayList<TimerVO> timerTriggerList = new ArrayList<TimerVO>();
		ArrayList<TimerResultVO>  timerTriggerResultList = new ArrayList<TimerResultVO>();
		
		
		for(int i=0;i<resultList.size();i++) {
			if(resultList.get(i).get("TRIGGER_TP") != null) {
				String trigger_tp = (String) resultList.get(i).get("TRIGGER_TP");
				switch(trigger_tp) {
					case "http" : 
						int httpCnt = 0;
						HttpVO httpVO = new HttpVO();
					    httpVO.setTrigger_id((int) resultList.get(i).get("TRIGGER_ID"));
					    httpVO.setApi_name((String) resultList.get(i).get("API_NAME"));
					    httpVO.setAdm_set_yn((String) resultList.get(i).get("ADM_SET_YN"));
					    httpTriggerList.add(httpVO);
					    httpCnt += httpCnt;
					    break;
					case "storage" : 
						int cephCnt =0;
						CephVO cephVO = new CephVO();
						cephVO.setTrigger_id((int) resultList.get(i).get("TRIGGER_ID"));
						cephVO.setCreate_yn((String) resultList.get(i).get("CREATE_YN"));
						cephVO.setDelete_yn((String) resultList.get(i).get("CEPH_DELETE_YN"));
						cephVO.setAdm_set_yn((String) resultList.get(i).get("ADM_SET_YN"));
						cephTriggerList.add(cephVO);
						cephCnt +=cephCnt;
						break;
					case "db" :
						String type = (String) resultList.get(i).get("DB_TYPE");
						if("mongodb".equals(type)) {
							int mongoCnt =0;
							DatabaseVO mongodbVO= new DatabaseVO();
							mongodbVO.setTrigger_id((int) resultList.get(i).get("TRIGGER_ID"));
							mongodbVO.setInsert_yn((String) resultList.get(i).get("INSERT_YN"));
							mongodbVO.setUpdate_yn((String) resultList.get(i).get("UPDATE_YN"));
							mongodbVO.setDelete_yn((String) resultList.get(i).get("DB_DELETE_YN"));
							mongodbVO.setAdm_set_yn((String) resultList.get(i).get("ADM_SET_YN"));
							mongodbVO.setDb_type(type);
							mongoTriggerList.add(mongodbVO);
							mongoCnt +=mongoCnt; 
						}
						if("postgre".equals(type)) {
							int postgreCnt =0;
							DatabaseVO postgreVO= new DatabaseVO();
							System.out.println(resultList.get(i).toString());
							postgreVO.setTrigger_id((int) resultList.get(i).get("TRIGGER_ID"));
							postgreVO.setInsert_yn((String) resultList.get(i).get("INSERT_YN"));
							postgreVO.setUpdate_yn((String) resultList.get(i).get("UPDATE_YN"));
							postgreVO.setDelete_yn((String) resultList.get(i).get("DB_DELETE_YN"));
							postgreVO.setAdm_set_yn((String) resultList.get(i).get("ADM_SET_YN"));
							postgreVO.setDb_type(type);
							postgreTriggerList.add(postgreVO);
							postgreCnt +=postgreCnt;
						}					
						break;
					case "timer" :
						type = (String) resultList.get(i).get("REPEAT_TP");
						int timerCnt =0;
						TimerVO timerVO = new TimerVO();
						timerVO.setTrigger_id((int) resultList.get(i).get("TRIGGER_ID"));
						timerVO.setRepeat_tp((String) resultList.get(i).get("REPEAT_TP"));
						timerVO.setRepeat_val((String) resultList.get(i).get("REPEAT_VAL"));
						timerVO.setRepeat_des((String) resultList.get(i).get("REPEAT_DES"));
						timerVO.setAdm_set_yn((String) resultList.get(i).get("ADM_SET_YN"));
						timerTriggerList.add(timerVO);
						
						TimerResultVO timerResultVO = gson.fromJson(timerVO.getRepeat_val(), TimerResultVO.class);
						timerTriggerResultList.add(timerResultVO);
						timerCnt++;
						paramMap.put("timerCnt", timerCnt);
						break;					
						
					default : break;
			   }
		   }
		}
		
		paramMap.put("templateVO", templateVO);
		paramMap.put("httpTriggerList", httpTriggerList);
		paramMap.put("cephTriggerList", cephTriggerList);
		paramMap.put("mongoTriggerList", mongoTriggerList);
		paramMap.put("postgreTriggerList", postgreTriggerList);

		paramMap.put("timerTriggerList", timerTriggerList);
		paramMap.put("timerTriggerResultList", timerTriggerResultList);
		
		System.out.println(paramMap.toString());
		paramMap.put("resultList", resultList);
		
		return paramMap;
	}
}


/*  미사용 method		
	ArrayList<TimerVO> timerMinTriggerList = new ArrayList<TimerVO>();
	ArrayList<TimerVO> timerHourTriggerList = new ArrayList<TimerVO>();
	ArrayList<TimerVO> timerDayTriggerList = new ArrayList<TimerVO>();
	ArrayList<TimerVO> timerWeekTriggerList = new ArrayList<TimerVO>();
	ArrayList<TimerVO> timerMonTriggerList = new ArrayList<TimerVO>();
	ArrayList<TimerVO> timerYearTriggerList = new ArrayList<TimerVO>();
	if("MN".equals(type)) {
		int minCnt =0;
		TimerVO minVO= new TimerVO();
		minVO.setTrigger_id((int) resultList.get(i).get("TRIGGER_ID"));
		minVO.setRepeat_tp((String) resultList.get(i).get("REPEAT_TP"));
		minVO.setRepeat_val((String) resultList.get(i).get("REPEAT_VAL"));
		minVO.setRepeat_des((String) resultList.get(i).get("REPEAT_DES"));
		minVO.setAdm_set_yn((String) resultList.get(i).get("ADM_SET_YN"));
		timerMinTriggerList.add(minVO);
		minCnt+=minCnt;
	}
	if("HR".equals(type)) {
		int hourCnt =0;
		TimerVO hourVO= new TimerVO();
		hourVO.setTrigger_id((int) resultList.get(i).get("TRIGGER_ID"));
		hourVO.setRepeat_tp((String) resultList.get(i).get("REPEAT_TP"));
		hourVO.setRepeat_val((String) resultList.get(i).get("REPEAT_VAL"));
		hourVO.setRepeat_des((String) resultList.get(i).get("REPEAT_DES"));	
		hourVO.setAdm_set_yn((String) resultList.get(i).get("ADM_SET_YN"));
		timerHourTriggerList.add(hourVO);
		hourCnt+=hourCnt;
	}
	if("DY".equals(type)) {
		int dayCnt =0;
		TimerVO dayVO= new TimerVO();
		dayVO.setTrigger_id((int) resultList.get(i).get("TRIGGER_ID"));
		dayVO.setRepeat_tp((String) resultList.get(i).get("REPEAT_TP"));
		dayVO.setRepeat_val((String) resultList.get(i).get("REPEAT_VAL"));
		dayVO.setRepeat_des((String) resultList.get(i).get("REPEAT_DES"));	
		dayVO.setAdm_set_yn((String) resultList.get(i).get("ADM_SET_YN"));
		timerDayTriggerList.add(dayVO);
		dayCnt+=dayCnt;
	}
	if("WK".equals(type)) {
		int weekCnt =0;
		TimerVO weekVO= new TimerVO();
		weekVO.setTrigger_id((int) resultList.get(i).get("TRIGGER_ID"));
		weekVO.setRepeat_tp((String) resultList.get(i).get("REPEAT_TP"));
		weekVO.setRepeat_val((String) resultList.get(i).get("REPEAT_VAL"));
		weekVO.setRepeat_des((String) resultList.get(i).get("REPEAT_DES"));		
		weekVO.setAdm_set_yn((String) resultList.get(i).get("ADM_SET_YN"));
		timerWeekTriggerList.add(weekVO);
		weekCnt+=weekCnt;
	}
	if("MO".equals(type)) {
		int monthCnt =0;
		TimerVO monthVO= new TimerVO();
		monthVO.setTrigger_id((int) resultList.get(i).get("TRIGGER_ID"));
		monthVO.setRepeat_tp((String) resultList.get(i).get("REPEAT_TP"));
		monthVO.setRepeat_val((String) resultList.get(i).get("REPEAT_VAL"));
		monthVO.setRepeat_des((String) resultList.get(i).get("REPEAT_DES"));
		monthVO.setAdm_set_yn((String) resultList.get(i).get("ADM_SET_YN"));
		timerMonTriggerList.add(monthVO);
		monthCnt+=monthCnt;
	}
	if("YR".equals(type)) {
		int yearCnt =0;
		TimerVO yearVO= new TimerVO();
		yearVO.setTrigger_id((int) resultList.get(i).get("TRIGGER_ID"));
		yearVO.setRepeat_tp((String) resultList.get(i).get("REPEAT_TP"));
		yearVO.setRepeat_val((String) resultList.get(i).get("REPEAT_VAL"));
		yearVO.setRepeat_des((String) resultList.get(i).get("REPEAT_DES"));
		yearVO.setAdm_set_yn((String) resultList.get(i).get("ADM_SET_YN"));
		timerYearTriggerList.add(yearVO);
		yearCnt+=yearCnt;
	}
	
		paramMap.put("timerMinTriggerList", timerMinTriggerList);
		paramMap.put("timerHourTriggerList", timerHourTriggerList);
		paramMap.put("timerDayTriggerList", timerDayTriggerList);
		paramMap.put("timerWeekTriggerList", timerWeekTriggerList);
		paramMap.put("timerMonTriggerList", timerMonTriggerList);
		paramMap.put("timerYearTriggerList", timerYearTriggerList);	
*/


