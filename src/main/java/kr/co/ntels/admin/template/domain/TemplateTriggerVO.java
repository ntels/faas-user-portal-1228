package kr.co.ntels.admin.template.domain;

public class TemplateTriggerVO {
	int trigger_id;
	int template_id;
	String trigger_tp;
	String adm_set_yn;
	
	public int getTrigger_id() {
		return trigger_id;
	}
	public void setTrigger_id(int trigger_id) {
		this.trigger_id = trigger_id;
	}
	public int getTemplate_id() {
		return template_id;
	}
	public void setTemplate_id(int template_id) {
		this.template_id = template_id;
	}
	public String getTrigger_tp() {
		return trigger_tp;
	}
	public void setTrigger_tp(String trigger_tp) {
		this.trigger_tp = trigger_tp;
	}
	public String getAdm_set_yn() {
		return adm_set_yn;
	}
	public void setAdm_set_yn(String adm_set_yn) {
		this.adm_set_yn = adm_set_yn;
	}
	
	

}
