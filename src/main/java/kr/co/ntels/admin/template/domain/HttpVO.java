package kr.co.ntels.admin.template.domain;

public class HttpVO {
	int trigger_id;
	String api_name;
	String adm_set_yn;
	
	
	public String getAdm_set_yn() {
		return adm_set_yn;
	}
	public void setAdm_set_yn(String adm_set_yn) {
		this.adm_set_yn = adm_set_yn;
	}
	public int getTrigger_id() {
		return trigger_id;
	}
	public void setTrigger_id(int trigger_id) {
		this.trigger_id = trigger_id;
	}
	public String getApi_name() {
		return api_name;
	}
	public void setApi_name(String api_name) {
		this.api_name = api_name;
	}
	
	
}
