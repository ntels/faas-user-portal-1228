package kr.co.ntels.admin.template.domain;

public class CephVO {
	int trigger_id;
	String create_yn;
	String delete_yn;
	String adm_set_yn;
	
	
	public String getAdm_set_yn() {
		return adm_set_yn;
	}
	public void setAdm_set_yn(String adm_set_yn) {
		this.adm_set_yn = adm_set_yn;
	}
	public int getTrigger_id() {
		return trigger_id;
	}
	public void setTrigger_id(int trigger_id) {
		this.trigger_id = trigger_id;
	}
	public String getCreate_yn() {
		return create_yn;
	}
	public void setCreate_yn(String create_yn) {
		this.create_yn = create_yn;
	}
	public String getDelete_yn() {
		return delete_yn;
	}
	public void setDelete_yn(String delete_yn) {
		this.delete_yn = delete_yn;
	}
	
	
}
