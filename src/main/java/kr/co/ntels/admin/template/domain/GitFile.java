package kr.co.ntels.admin.template.domain;

import com.fasterxml.jackson.annotation.JsonProperty;

public class GitFile {
    private String fileName;
    private String filePath;
    private long size;
    private String encoding;
    private String contentSha256;
    private String ref;
    private String blobID;
    private String commitID;
    private String lastCommitID;
    private String content;

    @JsonProperty("file_name")
    public String getFileName() { return fileName; }
    @JsonProperty("file_name")
    public void setFileName(String value) { this.fileName = value; }

    @JsonProperty("file_path")
    public String getFilePath() { return filePath; }
    @JsonProperty("file_path")
    public void setFilePath(String value) { this.filePath = value; }

    @JsonProperty("size")
    public long getSize() { return size; }
    @JsonProperty("size")
    public void setSize(long value) { this.size = value; }

    @JsonProperty("encoding")
    public String getEncoding() { return encoding; }
    @JsonProperty("encoding")
    public void setEncoding(String value) { this.encoding = value; }

    @JsonProperty("content_sha256")
    public String getContentSha256() { return contentSha256; }
    @JsonProperty("content_sha256")
    public void setContentSha256(String value) { this.contentSha256 = value; }

    @JsonProperty("ref")
    public String getRef() { return ref; }
    @JsonProperty("ref")
    public void setRef(String value) { this.ref = value; }

    @JsonProperty("blob_id")
    public String getBlobID() { return blobID; }
    @JsonProperty("blob_id")
    public void setBlobID(String value) { this.blobID = value; }

    @JsonProperty("commit_id")
    public String getCommitID() { return commitID; }
    @JsonProperty("commit_id")
    public void setCommitID(String value) { this.commitID = value; }

    @JsonProperty("last_commit_id")
    public String getLastCommitID() { return lastCommitID; }
    @JsonProperty("last_commit_id")
    public void setLastCommitID(String value) { this.lastCommitID = value; }

    @JsonProperty("content")
    public String getContent() { return content; }
    @JsonProperty("content")
    public void setContent(String value) { this.content = value; }
}
