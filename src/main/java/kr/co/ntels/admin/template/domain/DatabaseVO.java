package kr.co.ntels.admin.template.domain;

public class DatabaseVO {
	int trigger_id;
	String insert_yn;
	String update_yn;
	String delete_yn;
	String db_type;
	String adm_set_yn;
	
	
	public String getDb_type() {
		return db_type;
	}
	public void setDb_type(String db_type) {
		this.db_type = db_type;
	}
	public String getAdm_set_yn() {
		return adm_set_yn;
	}
	public void setAdm_set_yn(String adm_set_yn) {
		this.adm_set_yn = adm_set_yn;
	}
	public int getTrigger_id() {
		return trigger_id;
	}
	public void setTrigger_id(int trigger_id) {
		this.trigger_id = trigger_id;
	}
	public String getInsert_yn() {
		return insert_yn;
	}
	public void setInsert_yn(String insert_yn) {
		this.insert_yn = insert_yn;
	}
	public String getUpdate_yn() {
		return update_yn;
	}
	public void setUpdate_yn(String update_yn) {
		this.update_yn = update_yn;
	}
	public String getDelete_yn() {
		return delete_yn;
	}
	public void setDelete_yn(String delete_yn) {
		this.delete_yn = delete_yn;
	}
	
}
