package kr.co.ntels.admin.template.web;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.google.gson.Gson;

import kr.co.ntels.admin.template.domain.TemplateVO;
import kr.co.ntels.admin.template.service.AdminTemplateService;
import kr.co.ntels.faas.constant.FaaSCommCD;
import kr.co.ntels.faas.gitlab.web.GitLabController;


@Controller
@RequestMapping(value = "/admin/template")
public class AdminTemplateController {
	private static final Logger logger = LoggerFactory.getLogger(AdminTemplateController.class);
	@Autowired
	AdminTemplateService adminTemplateService ;
	
	@Autowired GitLabController gitLabController;
	
	/**
	 * @throws Exception 
	 *  @작성일 : 2020. 6. 12.
	 *  @작성자 : minkb
	 * @Method 설명 : 
	 * @etc : 
	 */
	@RequestMapping(value = "list", method = { RequestMethod.GET, RequestMethod.POST }, produces = "application/json; charset=utf8")
	public String list(TemplateVO condition,@RequestParam HashMap<String,Object> paramMap, Model model, HttpServletRequest request, HttpServletResponse response)
			throws Exception {
		return "layout/admin/template/list";
	}
	
	/**
	 * @throws Exception 
	 *  @작성일 : 2020. 6. 12.
	 *  @작성자 : minkb
	 * @Method 설명 : 
	 * @etc : 
	 */
	@RequestMapping(value = "listAction", method = { RequestMethod.GET, RequestMethod.POST }, produces = "application/json; charset=utf8")
	public String listAction(TemplateVO condition,@RequestParam HashMap<String,Object> paramMap, Model model, HttpServletRequest request, HttpServletResponse response)
			throws Exception {
		Gson gson = new Gson();
		String strUrl="layout/admin/template/listAction";
		String runtimeVersion = (paramMap.get("runtimeVersion") !=null && !"".equals(paramMap.get("runtimeVersion")) ? (String)paramMap.get("runtimeVersion") : null);
		String runtime= (runtimeVersion !=null ? org.apache.commons.lang.StringUtils.split(runtimeVersion)[0] : null);
		String version = (runtimeVersion !=null ? org.apache.commons.lang.StringUtils.split(runtimeVersion)[1] : null);
		condition.setRuntime(runtime);
		condition.setVersion(version);
		
		String trigger_tp = (paramMap.get("trigger_tp") !=null && !"".equals(paramMap.get("trigger_tp")) ? (String)paramMap.get("trigger_tp") : null);
		
		if("mongodb".equals(trigger_tp) || "postgre".equals(trigger_tp) ) {
			condition.setTrigger_tp("db");
			condition.setDb_type(trigger_tp);
		}
		
		model.addAttribute("resultList"  , adminTemplateService.listAction(condition));
		model.addAttribute("condition"  ,  gson.toJson(condition));
		model.addAttribute("paramMap"  ,  paramMap);
		return strUrl;
	}	
	
	
	/**
	 * @throws Exception 
	 *  @작성일 : 2020. 6. 12.
	 *  @작성자 : minkb
	 * @Method 설명 : 
	 * @etc : 
	 */
	@RequestMapping(value = "main", method = { RequestMethod.GET, RequestMethod.POST }, produces = "application/json; charset=utf8")
	public String add(TemplateVO condition, @RequestParam HashMap<String,Object> paramMap, Model model, HttpServletRequest request, HttpServletResponse response)
			throws Exception {
		Gson gson = new Gson();
		model.addAttribute("condition"  ,  gson.toJson(condition));
		return "layout/admin/template/main";
	}	
	
	/**
	 * @throws Exception 
	 *  @작성일 : 2020. 6. 12.
	 *  @작성자 : minkb
	 * @Method 설명 : 
	 * @etc : 
	 */
	@RequestMapping(value = "addAction", method = { RequestMethod.GET, RequestMethod.POST }, produces = "application/json; charset=utf8")
	public String addAction(TemplateVO condition, @RequestParam HashMap<String,Object> paramMap, Model model, HttpServletRequest request, HttpServletResponse response)
			throws Exception {
		Gson gson = new Gson();
		model.addAttribute("condition",  gson.toJson(paramMap));
		return "layout/admin/template/addAction";
	}
	
	/**
	 * @throws Exception 
	 *  @작성일 : 2020. 6. 12.
	 *  @작성자 : minkb
	 * @Method 설명 : 
	 * @etc : 
	 */
	@RequestMapping(value = "detailAction", method = { RequestMethod.GET, RequestMethod.POST }, produces = "application/json; charset=utf8")
	public String detailAction(TemplateVO condition, @RequestParam HashMap<String,Object> paramMap, Model model, HttpServletRequest request, HttpServletResponse response)
			throws Exception {
		Gson gson = new Gson();
		condition.setStart(0);
		condition.setEnd(1);
		TemplateVO templateVO = adminTemplateService.getTempate(condition);
		
		
		//TemplateVO vo = adminTemplateService.getTempate(condition);
		
		String projectPath = templateVO.getGit_url();
		String runtime = templateVO.getRuntime();
		List<Map<String, Object>> sourceList = gitLabController.checkOutFromProject(projectPath, runtime);
		
		paramMap = adminTemplateService.getModifyData(paramMap);
		
		model.addAttribute("paramMap",paramMap);
		model.addAttribute("sourceList", gson.toJson(sourceList));
		model.addAttribute("condition",  gson.toJson(condition));
		model.addAttribute("templateVO",  templateVO);
		return "layout/admin/template/detailAction";
	}	
	
	/**
	 * @throws Exception 
	 *  @작성일 : 2020. 6. 12.
	 *  @작성자 : minkb
	 * @Method 설명 : 
	 * @etc : 
	 */
	@RequestMapping(value = "modifyAction", method = { RequestMethod.GET, RequestMethod.POST }, produces = "application/json; charset=utf8")
	public String modifyAction(TemplateVO condition, @RequestParam HashMap<String,Object> paramMap, Model model, HttpServletRequest request, HttpServletResponse response)
			throws Exception {
		Gson gson = new Gson();
		condition.setTemplate_id(Integer.parseInt((String) paramMap.get("template_id")));
		TemplateVO vo = adminTemplateService.getTempate(condition);
		
		String projectPath = vo.getGit_url();
		String runtime = vo.getRuntime();
		List<Map<String, Object>> sourceList = gitLabController.checkOutFromProject(projectPath, runtime);
		
		
		
		paramMap = adminTemplateService.getModifyData(paramMap);
		
		model.addAttribute("sourceList", gson.toJson(sourceList));
		model.addAttribute("condition",gson.toJson(paramMap));
		model.addAttribute("paramMap",paramMap);
		return "layout/admin/template/modifyAction";
	}	
	
	/**
	 * @throws Exception 
	 *  @작성일 : 2020. 6. 12.
	 *  @작성자 : minkb
	 * @Method 설명 : 
	 * @etc : 
	 */
	@RequestMapping(value = "httpTrigger", method = { RequestMethod.GET, RequestMethod.POST }, produces = "application/json; charset=utf8")
	public String httpTrigger(TemplateVO condition, @RequestParam HashMap<String,Object> paramMap, Model model, HttpServletRequest request, HttpServletResponse response)
			throws Exception {
		Gson gson = new Gson();
		model.addAttribute("condition",gson.toJson(paramMap));
		model.addAttribute("paramMap",paramMap);
		return "layout/admin/template/httpTrigger";
	}	
	
	/**
	 * @throws Exception 
	 *  @작성일 : 2020. 6. 12.
	 *  @작성자 : minkb
	 * @Method 설명 : 
	 * @etc : 
	 */
	@RequestMapping(value = "cephTrigger", method = { RequestMethod.GET, RequestMethod.POST }, produces = "application/json; charset=utf8")
	public String cephTrigger(TemplateVO condition, @RequestParam HashMap<String,Object> paramMap, Model model, HttpServletRequest request, HttpServletResponse response)
			throws Exception {
		Gson gson = new Gson();
		model.addAttribute("condition",gson.toJson(paramMap));
		model.addAttribute("paramMap",paramMap);		
		return "layout/admin/template/cephTrigger";
	}	
	
	/**
	 * @throws Exception 
	 *  @작성일 : 2020. 6. 12.
	 *  @작성자 : minkb
	 * @Method 설명 : 
	 * @etc : 
	 */
	@RequestMapping(value = "mongoTrigger", method = { RequestMethod.GET, RequestMethod.POST }, produces = "application/json; charset=utf8")
	public String mongoTrigger(TemplateVO condition, @RequestParam HashMap<String,Object> paramMap, Model model, HttpServletRequest request, HttpServletResponse response)
			throws Exception {
		Gson gson = new Gson();
		model.addAttribute("condition",gson.toJson(paramMap));
		model.addAttribute("paramMap",paramMap);
		return "layout/admin/template/mongoTrigger";
	}		
	
	/**
	 * @throws Exception 
	 *  @작성일 : 2020. 6. 12.
	 *  @작성자 : minkb
	 * @Method 설명 : 
	 * @etc : 
	 */
	@RequestMapping(value = "postgreTrigger", method = { RequestMethod.GET, RequestMethod.POST }, produces = "application/json; charset=utf8")
	public String postgreTrigger(TemplateVO condition, @RequestParam HashMap<String,Object> paramMap, Model model, HttpServletRequest request, HttpServletResponse response)
			throws Exception {
		Gson gson = new Gson();
		model.addAttribute("condition",gson.toJson(paramMap));
		model.addAttribute("paramMap",paramMap);
		return "layout/admin/template/postgreTrigger";
	}
	
	/**
	 * @throws Exception 
	 *  @작성일 : 2020. 6. 12.
	 *  @작성자 : minkb
	 * @Method 설명 : 
	 * @etc : 
	 */
	@RequestMapping(value = "timerTrigger", method = { RequestMethod.GET, RequestMethod.POST }, produces = "application/json; charset=utf8")
	public String timerTrigger(TemplateVO condition, @RequestParam HashMap<String,Object> paramMap, Model model, HttpServletRequest request, HttpServletResponse response)
			throws Exception {
		Gson gson = new Gson();
		model.addAttribute("condition",gson.toJson(paramMap));
		model.addAttribute("paramMap",paramMap);
		return "layout/admin/template/timerTrigger";
	}	
	
	
	@RequestMapping(value = "save", method = RequestMethod.POST)
	public @ResponseBody String save(Model model, @RequestBody HashMap<String, Object> paramMap,HttpServletRequest request, HttpServletResponse res) throws Exception {
		Gson gson = new Gson();
		String groupPath = FaaSCommCD.getNamespace(request);
		paramMap.put("groupPath", groupPath);
		paramMap = adminTemplateService.save(paramMap);
		paramMap.put("saveResult", "sucsess");
		return gson.toJson(paramMap);
	}
	
	@RequestMapping(value = "update", method = RequestMethod.POST)
	public @ResponseBody String update(Model model, @RequestBody HashMap<String, Object> paramMap,HttpServletRequest request, HttpServletResponse res) throws Exception {
		Gson gson = new Gson();
		String top_yn = (	paramMap.get("top_yn") != null ? "Y" : "N");
		paramMap.put("top_yn",top_yn);
		String groupPath = FaaSCommCD.getNamespace(request);
		paramMap.put("groupPath", groupPath);
		paramMap = adminTemplateService.update(paramMap);
		paramMap.put("saveResult", "sucsess");
		return gson.toJson(paramMap);
	}		
	
	@RequestMapping(value = "delete", method = RequestMethod.POST)
	public @ResponseBody String delete(Model model, @RequestBody HashMap<String, Object> paramMap,HttpServletRequest request, HttpServletResponse res) throws Exception {
		Gson gson = new Gson();
		paramMap = adminTemplateService.delete(paramMap);
		paramMap.put("deleteResult", "succsess");
		return gson.toJson(paramMap);
	}			
	
}






