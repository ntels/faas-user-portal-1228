package kr.co.ntels.admin.template.domain;

import com.fasterxml.jackson.annotation.JsonProperty;

public class TimerResultVO {
    private String value;
    private String radio;
    private String value_th;
    private String value_week;

    @JsonProperty("value")
    public String getValue() { return value; }
    @JsonProperty("value")
    public void setValue(String value) { this.value = value; }

    @JsonProperty("radio")
    public String getRadio() { return radio; }
    @JsonProperty("radio")
    public void setRadio(String value) { this.radio = value; }

    @JsonProperty("value_th")
    public String getValue_th() { return value_th; }
    @JsonProperty("value_th")
    public void setValue_th(String value) { this.value_th = value; }

    @JsonProperty("value_week")
    public String getValue_week() { return value_week; }
    @JsonProperty("value_week")
    public void setValue_week(String value) { this.value_week = value; }
}
