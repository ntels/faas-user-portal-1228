package kr.co.ntels.admin.template.domain;

public class TimerVO {
	int trigger_id;
	String repeat_tp;
	String repeat_val;
	String repeat_des;
	String adm_set_yn;
	
	
	public String getAdm_set_yn() {
		return adm_set_yn;
	}
	public void setAdm_set_yn(String adm_set_yn) {
		this.adm_set_yn = adm_set_yn;
	}
	public int getTrigger_id() {
		return trigger_id;
	}
	public void setTrigger_id(int trigger_id) {
		this.trigger_id = trigger_id;
	}
	public String getRepeat_tp() {
		return repeat_tp;
	}
	public void setRepeat_tp(String repeat_tp) {
		this.repeat_tp = repeat_tp;
	}
	public String getRepeat_val() {
		return repeat_val;
	}
	public void setRepeat_val(String repeat_val) {
		this.repeat_val = repeat_val;
	}
	public String getRepeat_des() {
		return repeat_des;
	}
	public void setRepeat_des(String repeat_des) {
		this.repeat_des = repeat_des;
	}
	
	
	
}
