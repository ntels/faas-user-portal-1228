package kr.co.ntels.admin.dashboard.web;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import com.google.gson.Gson;

import kr.co.ntels.admin.common.domain.Paging;
import kr.co.ntels.admin.dashboard.domain.FunctionVO;
import kr.co.ntels.admin.dashboard.service.AdminDashBoardService;
import kr.co.ntels.faas.dashboard.service.DashboardServive;



@Controller
@RequestMapping(value = "/admin/dashboard")
public class AdminDashBoardController {
	
	
	private static final Logger logger = LoggerFactory.getLogger(AdminDashBoardController.class);

	@Autowired
	AdminDashBoardService functionService;	
	
	@Autowired
	DashboardServive dashboardServive;	

	
	/**
	 * @throws Exception 
	 *  @작성일 : 2020. 6. 12.
	 *  @작성자 : minkb
	 * @Method 설명 :  관리자 대시보드 화면 컨트롤러
	 * @etc : 
	 */
	@RequestMapping(value = "dashboard", method = { RequestMethod.GET, RequestMethod.POST }, produces = "application/json; charset=utf8")
	public String dashboard(@RequestParam Map<String,Object> paramMap, Model model, HttpServletRequest request, HttpServletResponse response)
			throws Exception {
		Gson gson = new Gson();
		List<Map<String, Object>> functionTop10List = functionService.getFunctionTop10(paramMap);
		FunctionVO functionVO = new FunctionVO(); 
//		functionVO.setOrderBy("reg_date_hms");
		functionVO.setOrderBy("reg_date_hms DESC");
		Paging temp = functionService.listAction(functionVO);
		List<FunctionVO> recentFunctionList = (List<FunctionVO>)temp.getLists();
		
		List<Map<String, Object>> getRuntimeFunction = functionService.getRuntimeFunction();
		
		//Map<String, Object> test = dashboardServive.getTriggerData(paramMap);
		List<Map<String, Object>> getTriggerInfo = functionService.getTriggerInfo(paramMap);
		
		model.addAttribute("getFunctionTop10List" , functionTop10List);
		model.addAttribute("getFunctionTop10Json" , gson.toJson(functionTop10List));
		
		model.addAttribute("recentFunctionList" , recentFunctionList);
		model.addAttribute("runtimeFunctionList" , getRuntimeFunction);
		model.addAttribute("getTemplateList" , functionService.getTemplate(paramMap));
		
		//트리거 등록 현황
		model.addAttribute("getTriggerInfoList" , getTriggerInfo);
		model.addAttribute("getTriggerInfoJson" , gson.toJson(getTriggerInfo));
		
		List<Map<String, Object>> regFunctionPerMonth = functionService.regFunctionPerMonth(paramMap);
		//월별 함수 등록 현황​
		model.addAttribute("regFunctionPerMonthList" , regFunctionPerMonth);
		model.addAttribute("regFunctionPerMonthJson" , gson.toJson(regFunctionPerMonth));
		//라인차트 
		List<Map<String, Object>> lineChart = functionService.lineChart(paramMap);
		model.addAttribute("lineChart" , lineChart);
		model.addAttribute("lineChartJson" , gson.toJson(lineChart));		
		
		//라인차트(등록현황)
		List<Map<String, Object>> lineChartRegCnt = functionService.lineChartRegCnt(paramMap);
		model.addAttribute("lineChartRegCnt" , lineChartRegCnt);
		model.addAttribute("lineChartRegCnt" , gson.toJson(lineChartRegCnt));
		
		return "layout/admin/dashboard/dashboard";
	}
	
	
	/**
	 * @throws Exception 
	 *  @작성일 : 2020. 6. 12.
	 *  @작성자 : minkb
	 * @Method 설명 : 
	 * @etc : 
	 */
	@RequestMapping(value = "list", method = { RequestMethod.GET, RequestMethod.POST }, produces = "application/json; charset=utf8")
	public String functionList(@RequestParam Map<String,Object> paramMap, Model model, HttpServletRequest request, HttpServletResponse response)
			throws Exception {
		Gson gson = new Gson();
		model.addAttribute("comboProjectList", functionService.comboProject());
		model.addAttribute("condition", gson.toJson(paramMap));
		return "layout/admin/dashboard/list";
	}	
	/**
	 * @throws Exception 
	 *  @작성일 : 2020. 6. 12.
	 *  @작성자 : minkb
	 * @Method 설명 : 
	 * @etc : 
	 */
	@RequestMapping(value = "listAction", method = { RequestMethod.GET, RequestMethod.POST }, produces = "application/json; charset=utf8")
	public String listAction(@RequestParam Map<String,Object> paramMap,FunctionVO condition, Model model, HttpServletRequest request, HttpServletResponse response)
			throws Exception {
		Gson gson = new Gson();
		/*List<Map<String, Object>> functionTop10List = functionService.getFunctionTop10(paramMap);
		model.addAttribute("getFunctionTop10List" , functionTop10List);
		model.addAttribute("getFunctionTop10Json" , gson.toJson(functionTop10List));*/
		model.addAttribute("resultList", functionService.listAction(condition));
		model.addAttribute("condition", gson.toJson(condition));
		return "layout/admin/dashboard/listAction";
	}	
}

/*미사용함수

	@RequestMapping(value = "mainStructureAction", method = { RequestMethod.GET, RequestMethod.POST }, produces = "application/json; charset=utf8")
	public String mainStructureAction(@RequestParam HashMap<String,Object> paramMap, Model model, HttpServletRequest request, HttpServletResponse response)
			throws Exception {
		Gson gson = new Gson();
		List<Map<String,Object>> getTriggerStructureList = functionService.getTriggerStructure(paramMap);
		model.addAttribute("getTriggerStructureList" , getTriggerStructureList);
		model.addAttribute("getTriggerStructureListJson" , gson.toJson(getTriggerStructureList));
		model.addAttribute("paramMap" , paramMap);
		model.addAttribute("condition" , gson.toJson(paramMap));
		return "layout/admin/function/mainStructureAction";
	}	
	
	@RequestMapping(value = "main", method = { RequestMethod.GET, RequestMethod.POST }, produces = "application/json; charset=utf8")
	public String main(@RequestParam Map<String,Object> paramMap, Model model, HttpServletRequest request, HttpServletResponse response)
			throws Exception {
		Gson gson = new Gson();
		model.addAttribute("condition" , gson.toJson(paramMap));
		return "layout/admin/function/main";
	}	
	
	@RequestMapping(value = "mainMonitoringAction", method = { RequestMethod.GET, RequestMethod.POST }, produces = "application/json; charset=utf8")
	public String mainMonitoringAction(@RequestParam HashMap<String,Object> paramMap, Model model, HttpServletRequest request, HttpServletResponse response)
			throws Exception {

		return "layout/admin/function/mainMonitoringAction";
	}		
 
 */






