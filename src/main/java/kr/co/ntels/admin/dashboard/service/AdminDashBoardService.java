package kr.co.ntels.admin.dashboard.service;

import java.sql.SQLException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import kr.co.ntels.admin.common.dao.AdminCommonDAO;
import kr.co.ntels.admin.common.domain.Paging;
import kr.co.ntels.admin.dashboard.domain.FunctionVO;


@Service
public class AdminDashBoardService {
	
	@Autowired 
	AdminCommonDAO dao;
	
	public List<Map<String, Object>> getFunctionTop10(Map<String,Object> paramMap) throws SQLException
	{
		return dao.select("faas.admin.dashboard.getFunctionTop10", paramMap);
	}
	
	@SuppressWarnings("unchecked")
	public Paging listAction(FunctionVO condition) throws SQLException
	{
		
		if (condition.getPage() == 0) {
			condition.setPage(1);
		}

		//condition.setStart(((condition.getPage() - 1) * condition.getPerPage()) + 1);
		condition.setStart(((condition.getPage() - 1) * condition.getPerPage()));
		condition.setEnd(condition.getPerPage());
		
		int count = dao.selectInt("faas.admin.dashboard.count", condition);
		List<FunctionVO> list = null;
		if (count > 0) {
			list = dao.selectList("faas.admin.dashboard.list", condition);
		}
			
		return new Paging(list, count, condition.getPerPage(), condition.getPage());
	}	
	
	@SuppressWarnings("unchecked")
	public List<Map<String, Object>> comboProject() throws SQLException
	{
		return dao.selectList("faas.admin.dashboard.comboProject");
	}	
	
	@SuppressWarnings("unchecked")
	public List<Map<String, Object>> getRuntimeFunction() throws SQLException
	{
		return dao.selectList("faas.admin.dashboard.getRuntimeFunction");
	}		
	
	
	@SuppressWarnings("unchecked")
	public List<Map<String, Object>> getTemplate(Map<String,Object> paramMap) throws SQLException
	{
		return dao.selectList("faas.admin.dashboard.getTemplate",paramMap);
	}
	
	@SuppressWarnings("unchecked")
	public List<Map<String, Object>> getTriggerInfo(Map<String,Object> paramMap) throws SQLException
	{
		return dao.selectList("faas.admin.dashboard.getTriggerInfo",paramMap);
	}		

	@SuppressWarnings("unchecked")
	public List<Map<String, Object>> regFunctionPerMonth(Map<String,Object> paramMap) throws SQLException
	{
		return dao.selectList("faas.admin.dashboard.regFunctionPerMonth",paramMap);
	}		
	
	@SuppressWarnings("unchecked")
	public List<Map<String, Object>> lineChart(Map<String,Object> paramMap) throws SQLException
	{
		return dao.selectList("faas.admin.dashboard.lineChart",paramMap);
	}		
	
	@SuppressWarnings("unchecked")
	public List<Map<String, Object>> lineChartRegCnt(Map<String,Object> paramMap) throws SQLException
	{
		return dao.selectList("faas.admin.dashboard.lineChartRegCnt",paramMap);
	}			
}


/* 미사용함수
  
 	@SuppressWarnings("unchecked")
	public List<Map<String, Object>> getTriggerStructure(HashMap<String,Object> paramMap) throws SQLException
	{
		return dao.selectList("faas.admin.dashboard.getTriggerStructure",paramMap);
	}	
 */



