package kr.co.ntels.admin.dashboard.domain;

import kr.co.ntels.admin.common.domain.CommonCondition;

public class FunctionVO extends CommonCondition{
	int function_id;
	String name;
	String runtime;
	String version;	
	String    namespace;
	String dsp_name;
	String img;
	String des;
	String git_url;
	String git_usr;
	String stat_cd;
	String reg_step_cd;
	String ref_name;
	String env;
	int    mem;
	int    concur;
	int    timeout;
	String trf;
	String reg_usr;
	String reg_date_hms;
	String mod_usr;
	String mod_date_hms;
	String last_sync_date_hms;
	String last_sync_cd;
	String last_sync_des;
	String del_yn;
	
	int cnt;
	int triggerCnt;
	int objCnt;
	
	String keyword;
	
	
	public String getKeyword() {
		return keyword;
	}
	public void setKeyword(String keyword) {
		this.keyword = keyword;
	}
	public int getObjCnt() {
		return objCnt;
	}
	public void setObjCnt(int objCnt) {
		this.objCnt = objCnt;
	}
	public int getTriggerCnt() {
		return triggerCnt;
	}
	public void setTriggerCnt(int triggerCnt) {
		this.triggerCnt = triggerCnt;
	}
	public int getFunction_id() {
		return function_id;
	}
	public void setFunction_id(int function_id) {
		this.function_id = function_id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getRuntime() {
		return runtime;
	}
	public void setRuntime(String runtime) {
		this.runtime = runtime;
	}
	public String getVersion() {
		return version;
	}
	public void setVersion(String version) {
		this.version = version;
	}
	public int getCnt() {
		return cnt;
	}
	public void setCnt(int cnt) {
		this.cnt = cnt;
	}
	
	public String getNamespace() {
		return namespace;
	}
	public void setNamespace(String namespace) {
		this.namespace = namespace;
	}
	public String getDsp_name() {
		return dsp_name;
	}
	public void setDsp_name(String dsp_name) {
		this.dsp_name = dsp_name;
	}
	public String getImg() {
		return img;
	}
	public void setImg(String img) {
		this.img = img;
	}
	public String getDes() {
		return des;
	}
	public void setDes(String des) {
		this.des = des;
	}
	public String getGit_url() {
		return git_url;
	}
	public void setGit_url(String git_url) {
		this.git_url = git_url;
	}
	public String getGit_usr() {
		return git_usr;
	}
	public void setGit_usr(String git_usr) {
		this.git_usr = git_usr;
	}
	public String getStat_cd() {
		return stat_cd;
	}
	public void setStat_cd(String stat_cd) {
		this.stat_cd = stat_cd;
	}
	public String getReg_step_cd() {
		return reg_step_cd;
	}
	public void setReg_step_cd(String reg_step_cd) {
		this.reg_step_cd = reg_step_cd;
	}
	public String getRef_name() {
		return ref_name;
	}
	public void setRef_name(String ref_name) {
		this.ref_name = ref_name;
	}
	public String getEnv() {
		return env;
	}
	public void setEnv(String env) {
		this.env = env;
	}
	public int getMem() {
		return mem;
	}
	public void setMem(int mem) {
		this.mem = mem;
	}
	public int getConcur() {
		return concur;
	}
	public void setConcur(int concur) {
		this.concur = concur;
	}
	public int getTimeout() {
		return timeout;
	}
	public void setTimeout(int timeout) {
		this.timeout = timeout;
	}
	public String getTrf() {
		return trf;
	}
	public void setTrf(String trf) {
		this.trf = trf;
	}
	public String getReg_usr() {
		return reg_usr;
	}
	public void setReg_usr(String reg_usr) {
		this.reg_usr = reg_usr;
	}
	public String getReg_date_hms() {
		return reg_date_hms;
	}
	public void setReg_date_hms(String reg_date_hms) {
		this.reg_date_hms = reg_date_hms;
	}
	public String getMod_usr() {
		return mod_usr;
	}
	public void setMod_usr(String mod_usr) {
		this.mod_usr = mod_usr;
	}
	public String getMod_date_hms() {
		return mod_date_hms;
	}
	public void setMod_date_hms(String mod_date_hms) {
		this.mod_date_hms = mod_date_hms;
	}
	public String getLast_sync_date_hms() {
		return last_sync_date_hms;
	}
	public void setLast_sync_date_hms(String last_sync_date_hms) {
		this.last_sync_date_hms = last_sync_date_hms;
	}
	public String getLast_sync_cd() {
		return last_sync_cd;
	}
	public void setLast_sync_cd(String last_sync_cd) {
		this.last_sync_cd = last_sync_cd;
	}
	public String getLast_sync_des() {
		return last_sync_des;
	}
	public void setLast_sync_des(String last_sync_des) {
		this.last_sync_des = last_sync_des;
	}
	public String getDel_yn() {
		return del_yn;
	}
	public void setDel_yn(String del_yn) {
		this.del_yn = del_yn;
	}
	
}
