package kr.co.ntels.admin.trigger.web;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import kr.co.ntels.admin.trigger.service.AdminTriggerService;

@RestController
@RequestMapping(value = "/service/admin")
public class AdminTriggerController {
    @Autowired AdminTriggerService triggerService;

    @GetMapping("/getTriggerList.json")
    public Map<String, Object> index(HttpServletRequest request, HttpServletResponse response, @RequestParam Map<String, Object> params) throws Exception {
        Map<String, Object> resultMap = new HashMap<>();
        List<Map<String, Object>> list = triggerService.getList(params);
        resultMap.put("list", list);
        return resultMap;
    }

    @GetMapping("/getTrigger.json")
    public String showTrigger(HttpServletRequest request, HttpServletResponse response, @RequestParam Map<String, Object> params) throws Exception {
        return "";
    }

    @GetMapping("/getTriggerHttp.json")
    public Map<String, Object> showTriggerHttp(HttpServletRequest request, HttpServletResponse response, @RequestParam Map<String, Object> params) throws Exception {
        return triggerService.getHttpInfo(params);
    }

    @GetMapping("/getTriggerCeph.json")
    public Map<String, Object> showTriggerCeph(HttpServletRequest request, HttpServletResponse response, @RequestParam Map<String, Object> params) throws Exception {
        return triggerService.getCephInfo(params);
    }

    @GetMapping("/getTriggerDatabase.json")
    public Map<String, Object> showTriggerDatabase(HttpServletRequest request, HttpServletResponse response, @RequestParam Map<String, Object> params) throws Exception {
        return triggerService.getDatabaseInfo(params);
    }

    @GetMapping("/getTriggerTimer.json")
    public Map<String, Object> showTriggerTimer(HttpServletRequest request, HttpServletResponse response, @RequestParam Map<String, Object> params) throws Exception {
        return triggerService.getTimerInfo(params);
    }
}