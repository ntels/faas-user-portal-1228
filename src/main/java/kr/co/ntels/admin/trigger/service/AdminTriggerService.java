package kr.co.ntels.admin.trigger.service;

import java.util.*;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import kr.co.ntels.faas.dao.FaasDao;

@Service
public class AdminTriggerService {
    
	@Autowired private FaasDao dao;

    public List<Map<String, Object>> getList(Map<String, Object> params) {
		return dao.select("faas.trigger.listForAdmin", params);
    }
    
    public Map<String, Object> getHttpInfo(Map<String, Object> params) {
        return (Map<String, Object>) dao.selectOne("faas.trigger.httpInfoForAdmin", params);
    }

    public Map<String, Object> getCephInfo(Map<String, Object> params) {
        return (Map<String, Object>) dao.selectOne("faas.trigger.cephInfoForAdmin", params);
    }

    public Map<String, Object> getDatabaseInfo(Map<String, Object> params) {
        return (Map<String, Object>) dao.selectOne("faas.trigger.databaseInfoForAdmin", params);
    }

    public Map<String, Object> getTimerInfo(Map<String, Object> params) {
        return (Map<String, Object>) dao.selectOne("faas.trigger.timerInfoForAdmin", params);
    }
}