package kr.co.ntels.admin.log.service;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.gitlab4j.api.models.Diff;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import kr.co.ntels.faas.dao.FaasDao;
import kr.co.ntels.faas.gitlab.service.GitLabService;
import kr.co.ntels.faas.gitlab.web.GitLabController;

@Service
public class AdminFunctionLogService {
    @Autowired FaasDao dao;
	@Autowired GitLabService gitLabService;
	@Autowired GitLabController gitLabController;

    public List<Map<String, Object>> getList(Map<String, Object> params) {
        return dao.select("faas.log.listForAdmin", params);
    }

    public int getTotalCount(Map<String, Object> params) throws Exception {
		return (Integer) dao.selectOne("faas.log.totalCountForAdmin", params);
    }
    
    public Map<String, Object> getInfo(Map<String, Object> params) {
        Map<String, Object> functionLog = (Map<String, Object>) dao.selectOne("faas.log.infoForAdmin", params);
        if(functionLog == null) return new HashMap<String, Object>();
        
        if ("REVISION".equals(functionLog.get("log_dtl_cd"))) {
            Map<String, Object> function = (Map<String, Object>) dao.selectOne("faas.function.infoForAdmin", params);
            String projectPath = function.get("git_url") + "";
            String revisionId = functionLog.get("revision_id") + "";
            try {
                functionLog.put("diff_list", gitLabService.getDiffAndRawContent(projectPath, revisionId));
            } catch(Exception e) {
                e.printStackTrace();
                functionLog.put("diff_list", new ArrayList<>());
            }
        }
        return functionLog;
    }
}