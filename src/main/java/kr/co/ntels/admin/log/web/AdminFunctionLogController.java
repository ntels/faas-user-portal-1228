package kr.co.ntels.admin.log.web;

import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import kr.co.ntels.admin.common.service.AdminCommonService;
import kr.co.ntels.admin.log.service.AdminFunctionLogService;

import org.springframework.web.bind.annotation.RequestParam;


@RestController
@RequestMapping(value="/service/admin")
public class AdminFunctionLogController {
    @Autowired AdminFunctionLogService logService;
    @Autowired AdminCommonService commonService;

    @GetMapping("/getFunctionLogList.json")
    public Map<String, Object> index(HttpServletRequest request, HttpServletResponse response, @RequestParam Map<String, Object> params) throws Exception{
        Map<String, Object> resultMap = new HashMap<>();
        
		int limit = params.get("limit") != null ? Integer.parseInt(params.get("limit") + "") : 12;
		int offset = commonService.getOffsetInPage(Integer.parseInt(params.get("curpage") + ""), limit);
		params.put("limit", limit);
		params.put("offset", offset);
        
        int totalCount = logService.getTotalCount(params);

        resultMap.put("list", logService.getList(params));
        resultMap.put("totalCount", totalCount);
		resultMap.put("curpage", params.get("curpage"));
        resultMap.put("lastpage", commonService.getLastPage(totalCount, limit));
        
        return resultMap;
    }

    @GetMapping("/getFunctionLog.json")
    public Map<String, Object> show(HttpServletRequest request, HttpServletResponse response, @RequestParam Map<String, Object> params) throws Exception {
        return logService.getInfo(params);
    }
}