<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>

<div class="col-md-6">
    <div class="card card-type1">
        <div class="card-header">
            <div class="card-info-area">
                <div class="text-box">
                    <div class="top">
                        <div class="row">
                            <div class="col-xs-12">
                                <div class="title inline-block">
                                    <b>
                                        함수 스케일 최대값 설정
                                    </b>
                                    <span class="info-icon-tooltip" data-toggle="tooltip" title="" data-placement="right" data-original-title="함수가 동시에 실행될 수 있는 건 수를 설정합니다.​">
                                        i
                                    </span>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="card-body">
            <div class="row">
                <div class="col-sm-12 img-content-type2">
                    <div class="img-zone img-concurrency img-size70">
                        <span class="sr-only">제한 시간​​</span>
                    </div>
                    <div class="name">
                        함수 스케일 최대값
                    </div>
                    <span class="font-size-20">
                        <b>{{comma function.detail.concur}}</b>
                    </span>
                </div>
            </div>
        </div>
    </div>
</div>