<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>

{{#if (eq functionLog._selectedKind 'LIST')}}
    {{component "logList"}}
{{else}}
    {{component "logDetail"}}
{{/if}}

<script>
    function reloadLog(type, function_log_id, revision_id) {
        var functionLog = NtelsHB.Store.get("functionLog");
        functionLog._selectedKind = type || 'LIST';
        functionLog.function_log_id = function_log_id || 0;
        NtelsHB.Store.set("functionLog", functionLog, false);
        NtelsHB.Store.get("_reload")("LOG");
    }
</script>