<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
    
<div class="panel-heading">
    <h3 class="panel-title">
        HTTP
    </h3>
</div> <!-- // panel-heading -->
<div class="panel-body">
    <div class="row padding-left-10">
        <div class="col-md-1">
            <div class="write-sub-title  blt-dot">
                API
            </div>
        </div>
        <div class="col-md-11">
            <div class="grey-600 font-size-13">
                {{trigger.http.api_name}}
            </div>
        </div>
    </div>
    <div class="row padding-left-10">
        <div class="col-md-1">
            <div class="write-sub-title  blt-dot">
                앤드포인트
            </div>
        </div>
        <div class="col-md-11">
            <div class="grey-600 font-size-13">
                <a href='{{trigger.http.con_url}}' target="_blank">{{trigger.http.con_url}}</a>
            </div>
        </div>
    </div>
    <div class="row padding-left-10">
        <div class="col-md-1">
            <div class="write-sub-title  blt-dot">
                API 키
            </div>
        </div>
        <div class="col-md-11">
            <div class="grey-600 font-size-13">
                <span>
                    <a href="javascript:void(0)" title="클립보드에 복사" target="_blank" data-role='btn' data-toggle='modal' data-target='#modalCopy' onclick="javascript: copyToClipboard('{{trigger.http.api_key}}');">
                        {{trigger.http.api_key}}
                        <span class="feather-icon">
                            <i data-feather="copy"></i>
                        </span>
                    </a>
                </span>
            </div>
        </div>
    </div>
</div> <!-- // panel-body -->

<script>
    function copyToClipboard(text) {
        /* Get the text field */
        var copyText = document.createElement("input");
        document.body.appendChild(copyText);
        copyText.value = text;

        /* Select the text field */
        copyText.select();
        copyText.setSelectionRange(0, 99999); /*For mobile devices*/

        /* Copy the text inside the text field */
        document.execCommand("copy");
        alert("복사하였습니다.");
    }

</script>