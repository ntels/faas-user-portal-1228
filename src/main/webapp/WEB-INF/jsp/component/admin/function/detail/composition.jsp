<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<section class="panel panel-bordered">
    <div class="panel-heading">
        <h3 class="panel-title">
            함수 구성
        </h3>
    </div> <!-- // panel-heading -->
    <div class="panel-body">
        <div class="item_wrap">
            <div>
                <div>
                    <p class="tit">트리거</p>
                    {{#if (eq trigger.list.length 0)}}
                    <div class="dot_box" style="display: ;"><p>트리거가 등록되지 않았습니다.​</p></div><!-- 트리거가 추가되면 해당부분은 삭제 또는 display:none -->
                    {{else}}
                    <ul class="tri_item">
                        {{#each trigger.list}}
                            <li>
                            {{#if ( eq trigger_tp "http" )}}
                                <button class="btn btn-default" data-btn="HTTP{{trigger_id}}" onclick="javascript:triggerDataLoad({{trigger_id}}, 'HTTP');">
                                    <div class="feather-icon"><i data-feather="globe"></i></div>
                                    HTTP
                                {{#if (or (eq trigger_stat_cd "REG") (eq trigger_stat_cd "INIT"))}}
                                    <div class="ico_area">
                                        <span class="unicode xi-spin"></span>
                                    </div>
                                {{/if}}
                                </button>
                            {{else if ( eq trigger_tp "timer")}}
                                <button class="btn btn-default" data-btn="TIMER{{trigger_id}}" onclick="javascript:triggerDataLoad({{trigger_id}}, 'TIMER');">
                                    <div class="feather-icon"><i data-feather="clock"></i></div>
                                    타이머
                                {{#if (or (eq trigger_stat_cd "REG") (eq trigger_stat_cd "INIT"))}}
                                    <div class="ico_area">
                                        <span class="unicode xi-spin"></span>
                                    </div>
                                {{/if}}
                                </button>
                            {{else if ( eq trigger_tp "db" )}}
                                <button class="btn btn-default" data-btn="DATABASE{{trigger_id}}" onclick="javascript:triggerDataLoad({{trigger_id}}, 'DATABASE');">
                                    <div class="feather-icon"><i data-feather="database"></i></div>
                                    DB
                                {{#if (or (eq trigger_stat_cd "REG") (eq trigger_stat_cd "INIT"))}}
                                    <div class="ico_area">
                                        <span class="unicode xi-spin"></span>
                                    </div>
                                {{/if}}
                                </button>
                            {{else }}
                                <button class="btn btn-default" data-btn="CEPH{{trigger_id}}" onclick="javascript:triggerDataLoad({{trigger_id}}, 'CEPH');">
                                    <div class="feather-icon"><i class="img-size16 img-ceph"></i></div>
                                    Ceph
                                {{#if (or (eq trigger_stat_cd "REG") (eq trigger_stat_cd "INIT"))}}
                                    <div class="ico_area">
                                        <span class="unicode xi-spin"></span>
                                    </div>
                                {{/if}}
                                </button>
                            {{/if}}
                            </li>
                        {{/each}}
                    </ul>
                    {{/if}}
                </div>
            </div>
            <div>
                <div>
                    <p class="tit">함수</p>
                    <ul class="tri_item">
                        <li>
                            <button class="btn btn-default" data-btn="FUNCTION" id="btnFunction" onclick="javascript: functionDataLoad();">
                                <div class="feather-icon">
                                    <i data-feather="box"></i>
                                </div>
                                {{ function.detail.dsp_name }}
                            </button>
                        </li>
                    </ul>
                </div>
            </div>
            <div>
                <div>
                    <p class="tit">대상​​</p>
                    {{#if (eq target.list.length 0) }}
                    <div class="dot_box"><p>결과 값을 전송할 대상을 추가해 주세요.​​​</p></div>
                    {{else}}
                    <ul class="tri_item">
                        {{#each target.list}}
                        <li>
                            <button class="btn btn-default" onclick='javascript: switchContent("/faas", "/faas/?menu=/layout/admin/function/detail.do?function_id={{target_function_id}}");'>
                                <div class="feather-icon">
                                    <i data-feather="file"></i>
                                </div>
                                {{dsp_name}}
                                <%-- <div class="ico_area">
                                    <span class="unicode xi-spin"></span>
                                </div> --%>
                            </button>
                        </li>
                        {{/each}}
                    </ul>
                    {{/if}}
                </div>
            </div>
        </div>
    </div> <!-- // panel-body -->
</section>
{{#if (eq _selectedCompositionKind 'HTTP')}}
    <!-- HTTP 상세 -->
    <div  data-lst="edit-trigger" data-tab="HTTP" style="display: none;">
        <section class="panel panel-bordered">
            {{ component "http" }}
        </section>
    </div>
{{else if (eq _selectedCompositionKind 'CEPH')}}
    <!-- Ceph 상세 -->
    <div data-lst="edit-trigger" data-tab="CEPH" style="display: none;">
        <section class="panel panel-bordered">
            {{ component "ceph" }}
        </section>
    </div>
{{else if (eq _selectedCompositionKind 'DATABASE')}}
    <!-- DB 상세 -->
    <div data-lst="edit-trigger" data-tab="DATABASE" style="display: none;">
        <section class="panel panel-bordered">
            {{ component "database" }}
        </section>
    </div>
{{else if (eq _selectedCompositionKind 'TIMER')}}
    <!-- 타이머 상세 -->
    <div data-lst="edit-trigger" data-tab="TIMER" style="display: none;">
        <section class="panel panel-bordered">
            {{ component "timer" }}
        </section>
    </div>
{{else if (eq _selectedCompositionKind 'FUNCTION')}}
    <!-- 함수 상세 -->
    <div data-lst="edit-trigger" data-tab="FUNCTION" style="display: none;">
        {{ component "source" }}

        <%-- {{#if (neq function.stat_cd 'REG')}} --%>
        <section class="row">
            {{ component "environment" }}
            {{ component "traffic" }}
        </section>

        <section class="row">
            {{component "defaultConfig"}}
            {{component "concurrency"}}
        </section>
        <%-- {{/if}} --%>

        <%-- {{#if (neq function.detail.reg_step_cd 'NONE')}}
        <section class="panel panel-bordered">
            {{component "regStep"}}
        </section>
        {{/if}} --%>

        {{#if (eq function.detail.stat_cd 'ERROR')}}
        <section class="panel panel-bordered">
            <div class="panel-heading">
                <h3 class="panel-title">
                    오류 내역
                </h3>
            </div> <!-- // panel-heading -->
            <div class="panel-body">
                <div id="errorEditor" class="editorBox" style="height: ;"></div>
            </div> <!-- // panel-body -->
        </section>
        {{/if}}
    </div>
{{/if}}
<script>

    function functionDataLoad() {
        NtelsHB.Store.get("_reload")();
    }

    function triggerDataLoad(triggerId, kind) {
        var params = {
            trigger_id: triggerId
        };
        var apiUrl = '${pageContext.request.contextPath}/service/admin';
        if(kind === 'HTTP') apiUrl += '/getTriggerHttp.json';
        else if (kind === 'DATABASE') apiUrl += '/getTriggerDatabase.json';
        else if(kind === 'CEPH') apiUrl += '/getTriggerCeph.json';
        else if(kind === 'TIMER') apiUrl += '/getTriggerTimer.json';

        prom.get(apiUrl, params)
        .then(function(result) {
            NtelsHB.Store.set("_selectedCompositionKind", kind, false);
            NtelsHB.Store.set("_selectedTriggerId", triggerId, false);
            
            var trigger = NtelsHB.Store.get("trigger");
            if(kind === 'HTTP') trigger.http = result;
            else if (kind === 'DATABASE') trigger.database = result;
            else if(kind === 'CEPH') trigger.ceph = result;
            else if(kind === 'TIMER') trigger.timer = result;
            NtelsHB.Store.set("trigger", trigger);
        });
    }
</script>