<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>

<div class="card card-type2">
    <div class="card-header">
        <div class="card-info-area">
            <div class="text-box">
                <div class="top">
                    <div class="row">
                        <div class="col-xs-10">
                            <b>
                                {{function.detail.dsp_name}}
                            </b>
                        </div>
                        <div class="col-xs-2">
                            <div class="dropdown pull-right">
                                <button type="button" class="btn btn-xs btn-nostyle dropdown-toggle" data-toggle="dropdown">
                                    <div class="feather-icon">
                                        <i data-feather="more-horizontal"></i>
                                    </div>
                                </button>
                                <ul class="dropdown-menu" role="menu">
                                    <li>
                                        <a href="#" data-toggle="modal" data-target="#modalDelete">
                                            <i data-feather="trash-2" class="dropdown-icon"></i>
                                            삭제
                                        </a>
                                    </li>
                                </ul>
                            </div>	
                        </div>
                    </div>	
                    <div class="row">
                        <div class="col-sm-6">
                            <span class="lagn">
                                {{#if (eq function.detail.runtime 'nodejs')}}
                                <span class="img-size15 img-nodejs"></span>
                                {{else}}
                                <span class="img-size15 img-python"></span>
                                {{/if}}
                                {{ function.detail.runtime }} {{ function.detail.version }}
                            </span>
                        </div>
                        <div class="col-sm-6">
                            <div class="text-right">
                                <span class="state">
                                    {{#if (eq function.detail.stat_cd "REG")}}
                                    <div class="state-round reg"></div>
                                    {{else if (eq function.detail.stat_cd "INIT")}}
                                    <div class="state-round ready"></div>
                                    {{else if (eq function.detail.stat_cd "NORMAL")}}
                                    <div class="state-round normal"></div>
                                    {{else}}
                                    <div class="state-round error"></div>
                                    {{/if}}
                                    {{ function.detail.stat_name }}
                                </span>
                                <span class="left-bar">
                                    <b>등록 일시</b>
                                    <span>{{formatDate function.detail.reg_date_hms 'YYYY-MM-DD HH:mm'}}</span>
                                </span>
                                <span class="left-bar">
                                    <b>등록자</b>
                                    <span>{{ function.detail.reg_usr }}​ ({{function.detail.project_name}})</span>
                                </span>
                            </div>
                        </div>
                    </div>
                    <!-- modal -->
                    <div class="modal" id="modalDelete">
                        <div class="modal-dialog">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                        <span aria-hidden="true">×</span>
                                    </button>
                                    <h4 class="modal-title">함수 삭제</h4>
                                </div>
                                <div class="modal-body">
                                    <div class="font-size-13">
                                        선택한 함수를 삭제하면, 모든 데이터가 삭제되어 복구할 수 없습니다.​<br>
                                        안전을 위해 선택한 함수 이름을 직접 입력해 주세요.​
                                    </div>
                                    <div class="form-group default-label margin-bottom-20 margin-top-30">
                                        <input type="text" class="form-control" id="functionName" placeholder="삭제할 함수 이름을 입력해 주세요.​">
                                        <label for="formId">함수 이름</label>
                                    </div>
                                </div>
                                <div class="modal-footer">
                                    <button type="button" class="btn btn-primary" onclick="javascript:validate();">삭제</button>
                                    <button type="button" class="btn btn-default" data-dismiss="modal">취소​</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<script>
    function validate() {
        var _function = NtelsHB.Store.get("function");
        var functionName = $("#functionName").val().trim();

        if (functionName === '') { 
            alert("선택한 함수 이름을 직접 입력해 주세요."); 
            return;
        }

        if (functionName !== _function.detail.dsp_name) {
            alert("선택한 함수 이름과 일치하지 않습니다. 다시 확인해 주세요."); 
            return;
        }

        if (confirm("선택한 함수를 삭제하시겠습니까?")) {
            var params = {
                function_id: _function.detail.function_id
            };
            var url = '${pageContext.request.contextPath}/service/admin/deleteFunction.json';
            prom.post(url, params)
            .then(function(result) {
                if (result.isSuccess) {
                    loadPage('layout/admin/function/list.do');
                } else {
                    alert("삭제를 실패했습니다.");
                }
                $('#modalDelete').modal('hide');
            })
            .catch(function(reason) {
                $('#modalDelete').modal('hide');
            });
        }
    }

    $('#modalDelete').on('hidden.bs.modal', function (e) {
        $("#functionName").val("");
    })
</script>