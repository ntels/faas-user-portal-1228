<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<div class="table-input-wrap">
    <div class="row">
        <div class="col-sm-3">
            <div class="datatable-length">
                <span>페이지당 행 수 : </span>
                <select name="limit" id="limit" class="form-control form-control-sm margin-left-5">
                    <option value="10">10</option>
                    <option value="25">25</option>
                    <option value="50">50</option>
                    <option value="100">100</option>
                </select>
            </div>
        </div>
    </div>
</div>

<div class="table-wrap">
    <table class="table table-hover" cellspacing="0" width="100%">
        <colgroup>
        {{#each tableOptions.widths}}
			{{#if this}}
			<col style="width: {{this}}px">
			{{else}}
			<col>
			{{/if}}
		{{/each}}
        </colgroup>
        <thead>
            <tr>
			{{#each tableOptions.headers}}
                <th class="text-center">{{this}}</th>
			{{/each}}
            </tr>
        </thead>
        <tbody>
            {{#each list}}
                {{component 'tableItem'}}
            {{/each}}
        </tbody>
    </table>
</div>