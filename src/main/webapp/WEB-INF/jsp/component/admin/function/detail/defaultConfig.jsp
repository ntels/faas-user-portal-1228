<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
    
<div class="col-md-6">
    <div class="card card-type1">
        <div class="card-header">
            <div class="card-info-area">
                <div class="text-box">
                    <div class="top">
                        <div class="row">
                            <div class="col-xs-12">
                                <div class="title inline-block">
                                    <b>
                                        기본 설정
                                    </b>
                                    <span class="info-icon-tooltip" data-toggle="tooltip" title="" data-placement="right" data-original-title="함수에 사용할 메모리 및 제한 시간을 설정합니다.​">
                                        i
                                    </span>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="card-body">
            <div class="row">
                <div class="col-sm-4 img-content-type2">
                    <div class="img-zone img-memory img-size70">
                        <span class="sr-only">메모리(MB)​</span>
                    </div>
                    <div class="name">
                        메모리(MB)​
                    </div>
                    <span class="font-size-20">
                        <b>{{comma function.detail.mem}}</b>
                    </span>
                </div>
                <div class="col-sm-4 img-content-type2">
                    <div class="img-zone img-cpu img-size70">
                        <span class="sr-only">CPU​</span>
                    </div>
                    <div class="name">
                        CPU​
                    </div>
                    <span class="font-size-20">
                        <b>{{comma function.detail.cpu}}</b>
                    </span>
                </div>
                <div class="col-sm-4 img-content-type2">
                    <div class="img-zone img-limit img-size70">
                        <span class="sr-only">제한 시간​​</span>
                    </div>
                    <div class="name">
                        제한 시간​
                    </div>
                    <span class="font-size-20">
                        <b>{{function.detail.timeout.min}}분 {{function.detail.timeout.sec}}초</b>
                    </span>
                </div>
            </div>
        </div>
    </div>
</div>