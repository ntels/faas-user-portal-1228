<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
    
<section class="panel panel-bordered">
    {{#if (and (eq functionLog.params.log_cd "") (eq functionLog.totalCount 0))}}
        <div class="nodata-box type2">
            <div class="img-div">
                <p class="text-center">등록된 데이터가 없습니다. ​</p>
            </div>
        </div>
    {{else}}
        <div class="panel-heading">
            <div class="row">
                <div class="col-md-7">
                    <h3 class="panel-title">
                        로그
                    </h3>
                </div>
                <div class="col-md-5 text-right">
                    <div class="form-inline panel-heading-right">
                        <div class="form-group">
                            <select name="" id="logCode" class="form-control form-control-sm">
                                <option value="">전체</option>
                                <option value="MODIFY">수정</option>
                                <option value="ERROR">오류​</option>
                            </select>
                        </div>
                    </div>
                </div>
            </div>
        </div> <!-- // panel-heading -->
        {{#if (neq functionLog.totalCount 0)}}
            <div class="panel-body">
                <div class="table-wrap">
                    <table class="table table-outline">
                        <colgroup>
                            <col style="width: 200px">
                            <col style="width: 150px">
                            <col>
                            <col style="width: 100px">
                        </colgroup>
                        <thead class="bg-blue-grey-100">
                            <tr class="text-center">
                                <th scope="col">로그 발생 일시</th>
                                <th scope="col">구분​</th>
                                <th scope="col">로그 내역​</th>
                                <th scope="col">수정자​</th>
                            </tr>
                        </thead>
                        <tbody>
                            {{#each functionLog.list}}
                            <tr>
                                <td class="text-center">
                                    {{reg_date_hms}}
                                </td>
                                <td class="text-center">
                                    {{log_cd_nm}}
                                </td>
                                <td class="ellipsis-2">
                                    {{#if (eq log_cd "ERROR")}}
                                        <a href="#" onclick="javascript: reloadLog('DETAIL', '{{function_log_id}}'); return false;">{{logtext}}</a>
                                    {{else if (eq log_cd "MODIFY")}}
                                        {{#if (eq log_dtl_cd "REVISION")}}
                                            <a href="#" onclick="javascript: reloadLog('DETAIL', '{{function_log_id}}');">함수수정</a>
                                        {{else if (or (eq log_dtl_cd "DISABLE") (eq log_dtl_cd "ENABLE"))}}
                                            <span>상태변경({{log_dtl_cd_nm}})</span>
                                        {{else if (or (eq log_dtl_cd "DEL_TRIGGER") (eq log_dtl_cd "DEL_APP"))}}
                                            <span>{{log_dtl_cd_nm}}({{trigger_tp}})</span>
                                        {{else if (or (eq log_dtl_cd "MODIFY") (eq log_dtl_cd "ADD_TRIGGER") (eq log_dtl_cd "ADD_APP"))}}
                                            <span>{{log_dtl_cd_nm}}</span>
                                        {{/if}}
                                    {{/if}}
                                </td>
                                <td class="text-center">
                                    {{#if (eq log_cd 'ERROR')}}
                                        -
                                    {{else}}
                                        {{reg_usr}}
                                    {{/if}}
                                </td>
                            </tr>
                            {{/each}}
                        </tbody>
                    </table>
                </div>
                
                {{ component "pagination"}}
            </div> <!-- // panel-body -->
        {{else}}
            <!-- 검색결과가 없습니다. -->
            <div class="nodata-box type2">
                <div class="nodata">
                    <div class="img-div"></div>
                    <p>검색된 데이터가 없습니다. <span class="blue bold">다른 조건으로 검색해 보세요.​</span></p>
                </div>
            </div>
        {{/if}}
    {{/if}}
</section>