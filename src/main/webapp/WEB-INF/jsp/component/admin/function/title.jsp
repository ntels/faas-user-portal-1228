<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
    <div class="page-title">
        <div id="spot">
            <div class="page-info">
                함수 관리
            </div>
            <div class="page-location">
                <span class="home">
                    <i class="xi-home-o"></i>
                    HUBPoP
                </span>
                <span>함수 관리</span>
            </div>

            {{#if (eq isShowListButton true)}}
            <a href="javascript:void(0);" onclick='javascript:switchContent("/faas", "/faas/?menu=/layout/admin/function/list.do");' class="btn-list-all" title="함수 관리 목록">
                <i class="xi-bars"></i>
                <p class="sr-only">함수 관리 목록</p>
            </a>
            {{/if}}

            <a href="javascript:void(0);" onclick='javascript:reload();' class="btn-refresh">
                <i class="xi-renew"></i>
                <p class="sr-only">새로고침</p>
            </a>
        </div>
    </div>
    <script>
        function reload() {
            var _reload = NtelsHB.Store.get('_reload');
            _reload();
        }
    </script>