<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<tr>
    <!-- 프로젝트 -->
    <td class="text-center">{{project_name}}​</td>
    <!-- 함수명/설명 -->
    <td>
        <a href="javascript:void(0);" onclick='javascript:switchContent("/faas", "/faas/?menu=/layout/admin/function/detail.do?function_id={{function_id}}");'>{{ dsp_name }}</a>
        <div class="ellipsis-2">{{ des }}</div>
    </td>
    <!-- 상태 -->
    <td class="text-left">
        <span class="state">
            {{#if (eq stat_cd "REG")}}
            <div class="state-round reg"></div>
            {{else if (eq stat_cd "INIT")}}
            <div class="state-round ready"></div>
            {{else if (eq stat_cd "NORMAL")}}
            <div class="state-round normal"></div>
            {{else}}
            <div class="state-round error"></div>
            {{/if}}
            {{ stat_name }}
        </span>
    </td>
    <!-- 런타임 버전 -->
    <td class="text-center">{{ runtime }} {{ version }}​</td>
    <!-- 트리거 -->
    <td class="text-center">{{ trigger_count }}건​</td>
    <!-- 대상 -->
    <td class="text-center">{{ target_count }}건​</td>
    <!-- 등록일시 -->
    <td class="text-center">{{formatDate reg_date_hms 'YYYY-MM-DD HH:mm'}}​</td>
    <!-- 등록자 -->
    <td class="text-center">{{ reg_usr }}</td>
</tr>