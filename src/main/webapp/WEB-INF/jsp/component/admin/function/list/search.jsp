<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>

<div class="card-input-wrap">
    <div class="row">
        <div class="col-md-4">
            <div class="font-size-18 padding-top-5">
                <b>함수 <span class="color-primary">({{totalCount}})</span></b>
            </div>
        </div>
        <div class="col-md-8">
            <div class="text-right">
                <!-- TODO: 프로젝트 조회 정보 받은 후 작업 필요 urjo -->
                <div class="form-group inline-block margin-bottom-0">
                    <select name="project" id="project" class="form-control">
                        <option value="">프로젝트</option>
                        {{#each projects}}
                        <option value="{{id}}">{{org_name}}​</option>
                        {{/each}}
                    </select>
                </div>
                <div class="form-group inline-block margin-bottom-0">
                    <select name="sch_runtime" id="sch_runtime" class="form-control">
                        <option value="">런타임 유형​</option>
                        <option value="nodejs" selected>Note.js</option>
                        <option value="python">Python​</option>
                    </select>
                </div>
                <div class="form-group inline-block margin-bottom-0">
                    <select name="order_by" id="order_by" class="form-control">
                        <option value="create">등록일 순​</option>
                        <option value="modify">수정일 순</option>
                        <option value="name">이름 순​​</option>
                    </select>
                </div>
                <div class="form-group inline-block margin-bottom-0">
                    <input type="text" class="form-control" id="keyword" placeholder="키워드 검색​">
                </div>
                <button type="button" class="btn btn-primary" id="btnSearch">
                    <div class="feather-icon">
                        <i data-feather="search"></i>
                    </div>
                    <span class="sr-only">search</span>
                </button>
            </div>
        </div>
    </div>
</div>