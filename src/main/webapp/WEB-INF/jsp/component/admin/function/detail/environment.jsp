<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>

<div class="col-md-6">
    <div class="card card-type1">
        <div class="card-header">
            <div class="card-info-area">
                <div class="text-box">
                    <div class="top">
                        <div class="row">
                            <div class="col-xs-12">
                                <div class="title inline-block">
                                    <b>
                                        환경 변수
                                    </b>
                                    <span class="info-icon-tooltip" data-toggle="tooltip" title="" data-placement="right" data-original-title="함수 코드에서 액세스할 수 있는 키/값을 설정하면, 함수 코드 변경 없이 간편하게 이용할 수 있습니다.​">
                                        i
                                    </span>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="card-body">
            {{#if (or (eq function.detail.env.length 0) (eq function.detail.env null))}}
            <div class="nodata-box" style="height: 100px;">
                <div class="img-div">
                    <p class="text-center">등록된 데이터가 없습니다. ​</p>
                </div>
            </div>
            {{else}}
            <div class="scroll-content scroll-line" style="height: 205px">
                <div class="table-wrap">
                    <table class="table table-outline text-center">
                        <colgroup>
                            <col span="2">
                        </colgroup>
                        <thead class="bg-blue-grey-100">
                            <tr>
                                <th scope="col">키</th>
                                <th scope="col">값​</th>
                            </tr>
                        </thead>
                        <tbody>
                            {{#each function.detail.env}}
                            <tr>
                                <td>
                                    {{name}}
                                </td>
                                <td>
                                    {{value}}
                                </td>
                            </tr>
                            {{/each}}
                        </tbody>
                    </table>
                </div>
            </div>
            {{/if}}
        </div>
    </div>
</div>