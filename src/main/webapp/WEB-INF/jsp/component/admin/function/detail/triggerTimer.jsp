<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
    
<div class="panel-heading">
    <h3 class="panel-title">
        타이머
    </h3>
</div> <!-- // panel-heading -->
<div class="panel-body">
    <div class="row padding-left-10">
        <div class="col-md-1">
            <div class="write-sub-title blt-dot">
                기준 일시
            </div>
        </div>
        <div class="col-md-5">
            <div class="grey-600 font-size-13">
                {{formatDate trigger.timer.bas_date_hms 'YYYY-MM-DD HH:mm'}}
            </div>
        </div>
        <div class="col-md-1">
            <div class="write-sub-title blt-dot">
                반복
            </div>
        </div>
        <div class="col-md-5">
            <span class="img-repeat">
                {{#if (eq trigger.timer.repeat_tp 'NO')}}
                    <span class="none">
                        <i></i><span class="grey-600 font-size-13 margin-left-10">반복 없음</span>
                    </span>
                {{/if}}
                {{#if (eq trigger.timer.repeat_tp 'MN')}}
                    <span class="minute">
                        <i></i><span class="grey-600 font-size-13 margin-left-10">분</span>
                    </span>
                {{/if}}
                {{#if (eq trigger.timer.repeat_tp 'HR')}}
                    <span class="hour">
                        <i></i><span class="grey-600 font-size-13 margin-left-10">시간</span>
                    </span>
                {{/if}}
                {{#if (eq trigger.timer.repeat_tp 'DY')}}
                    <span class="day">
                        <i></i><span class="grey-600 font-size-13 margin-left-10">매일</span>
                    </span>
                {{/if}}
                {{#if (eq trigger.timer.repeat_tp 'WK')}}
                    <span class="week">
                        <i></i><span class="grey-600 font-size-13 margin-left-10">매주</span>
                    </span>
                {{/if}}
                {{#if (eq trigger.timer.repeat_tp 'MO')}}
                    <span class="month">
                        <i></i><span class="grey-600 font-size-13 margin-left-10">매월</span>
                    </span>
                {{/if}}
                {{#if (eq trigger.timer.repeat_tp 'YR')}}
                    <span class="year">
                        <i></i><span class="grey-600 font-size-13 margin-left-10">매년</span>
                    </span>
                {{/if}}
            </span>
        </div>
    </div>
    <div class="row padding-left-10">
        <div class="col-md-1">
            <div class="write-sub-title blt-dot">
                반복 설정
            </div>
        </div>
        <div class="col-md-11">
            <div class="grey-600 font-size-13">
                <p>{{trigger.timer.repeat_des}}​</p>
            </div>
        </div>
    </div>
    <div class="row padding-left-10">
        <div class="col-md-1">
            <div class="write-sub-title blt-dot">
                반복 종료일
            </div>
        </div>
        <div class="col-md-11">
            <div class="grey-600 font-size-13">
                {{formatDate trigger.timer.end_date_hms 'YYYY-MM-DD HH:mm'}}
            </div>
        </div>
    </div>
</div> <!-- // panel-body -->