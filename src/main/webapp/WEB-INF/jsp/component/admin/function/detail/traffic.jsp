<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>

<div class="col-md-6">
    <div class="card card-type1">
        <div class="card-header">
            <div class="card-info-area">
                <div class="text-box">
                    <div class="top">
                        <div class="row">
                            <div class="col-xs-12">
                                <div class="title inline-block">
                                    <b>
                                        트래픽 설정​
                                    </b>
                                    <span class="info-icon-tooltip" data-toggle="tooltip" title="" data-placement="right" data-original-title="함수별 트래픽 제한 값을 설정합니다.​">
                                        i
                                    </span>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="card-body">
            <div class="row">
                <div class="col-sm-6 img-content-type2">
                    <div class="img-zone img-burst img-size70">
                        <span class="sr-only">초당 제한 (Burst Limit)​</span>
                    </div>
                    <div class="name">
                        초당 제한 (Burst Limit)​
                    </div>
                    <span class="font-size-20">
                        <b>{{comma function.detail.trf.burst}}건/초​</b>
                    </span>
                </div>
                <div class="col-sm-6 img-content-type2">
                    <div class="img-zone img-quota img-size70">
                        <span class="sr-only">기간별 제한 (Quota Limit)</span>
                    </div>
                    <div class="name">
                        기간별 제한 (Quota Limit)
                    </div>
                    <span class="font-size-20">
                        <b>{{comma function.detail.trf.quota.cnt}}건/{{function.detail.trf.quota.val}}{{function.detail.trf.quota.typenm}}​</b>
                    </span>
                </div>
            </div>
        </div>
    </div>
</div>