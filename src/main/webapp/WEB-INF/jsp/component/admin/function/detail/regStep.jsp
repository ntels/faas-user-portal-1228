<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<script>
</script>
<div class="panel-heading">
	<h3 class="panel-title">
		함수 등록 단계
	</h3>
</div> <!-- // panel-heading -->
<div class="panel-body">

	<div class="info_txt">
		전체 5단계 중
		{{#if (eq function.detail.stat_cd "REG")}}
			<strong class="f-reg">
			{{#with (lookup _steps function.detail.reg_step_cd)}} {{name}} 진행중 {{/with}}
			</strong>
		{{else if (eq function.detail.stat_cd "NORMAL")}}
			<strong class="f-normal">
			{{#with (lookup _steps function.detail.reg_step_cd)}} {{name}} 완료 {{/with}}
			</strong>
		{{else}}
			<strong class="f-error">
			{{#with (lookup _steps function.detail.reg_step_cd)}} {{name}} 실패​ {{/with}}
			</strong>
		{{/if}}
	</div>
	
	<ul class="result_area_2 margin-top-30">

	 {{#each _steps}}
	 	{{#if (eq @key ../function.detail.reg_step_cd)}}
			{{#if (eq ../function.detail.stat_cd 'REG')}}
			<li class="loading_wrap">
				<div class="circle">
					<div class="spinner-border spinner-info" role="status">
						<span class="sr-only">Loading...</span>
					</div>
				</div>
				<span class="txt_wrap">
					<span class="tit">{{this.name}} : {{this.desc}}</span>
					<div class="state">
						<div class="state-round loading"></div>
						진행중
					</div>
				</span>
			</li>
			{{else if (eq ../function.detail.stat_cd 'NORMAL')}}
			<li class="success_wrap">
				<span class="txt_wrap">
					<span class="tit">{{this.name}} : {{this.desc}}</span>
					<div class="state">
						<div class="state-round success"></div>
						완료​
					</div>
				</span>
			</li>
			{{else}}
			<li class="fail_wrap">
				<span class="txt_wrap">
					<span class="tit">{{this.name}} : {{this.desc}}</span>
					<div class="state">
						<div class="state-round fail"></div>
						실패​
					</div>
				</span>
			</li>
			{{/if}}
		{{else if (lessThan (parseInt (substr @key 4 1)) (parseInt (substr ../function.detail.reg_step_cd 4 1)))}}
		<li class="success_wrap">
			<span class="txt_wrap">
				<span class="tit">{{this.name}} : {{this.desc}}</span>
				<div class="state">
					<div class="state-round success"></div>
					완료​
				</div>
			</span>
		</li>
		{{else}}
		<li class="waiting_wrap">
			<span class="txt_wrap">
				<span class="tit">{{this.name}} : {{this.desc}}</span>
				<div class="state">
					<div class="state-round waiting"></div>
					대기
				</div>
			</span>
		</li>
		{{/if}}
	 {{/each}}
	</ul>

</div> <!-- // panel-body -->