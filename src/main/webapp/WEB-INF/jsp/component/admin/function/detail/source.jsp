<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>

<section class="panel panel-bordered">
    <div class="panel-heading">
        <h3 class="panel-title">
            소스 코드
        </h3>
    </div>

    <div class="panel-body">
        <!-- tab -->
        <div class="nav-tabs-wrap full-wrap">
            <ul class="nav nav-tabs">
                {{#each function.source.list}}
                    {{#if (eq @index ../function.source._selectedIndex)}}
                        <li class="active">
                            <a href="javascript:void(0);" idx = {{@index}} name="sourceNavTab">{{this.fileName}}</a>
                        </li>
                    {{else}}
                        <li>
                            <a href="javascript:void(0);" idx = {{@index}} name="sourceNavTab">{{this.fileName}}</a>
                        </li>
                    {{/if}}
                {{/each}}
                <%-- <li class="disabled"><a href="#">Disabled</a></li> --%>
            </ul>
        </div>

        <!-- edit -->
        <div class="pos-r" id="" style="display: block;">
            <div id="editor" class="editorBox"></div>
        </div>
    </div>
</section>

