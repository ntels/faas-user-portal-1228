<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>

<section class="panel panel-bordered">
    <div class="panel-heading">
        <div class="form-inline">
            <a href="#" onclick="javascript:reloadLog('LIST'); return false;" class="panel-back-btn" title="뒤로가기">
                <i class="xi-angle-left" style="line-height: 52px;"></i>
            </a>
            <h3 class="panel-title">
                로그 정보​
            </h3>
        </div>
    </div> <!-- // panel-heading -->
    <div class="panel-body">
        <div class="row">
            <div class="col-md-3">
                <ul class="list-dot">
                    <li>
                        <b>로그 발생 일시 </b> 
                        <span>
                            {{functionLog.detail.reg_date_hms}}
                        </span>
                    </li>
                </ul>
            </div>
            <div class="col-md-2">
                <ul class="list-dot">
                    <li>
                        <b>구분 </b> 
                        <span>
                            {{functionLog.detail.log_cd_nm}}​
                        </span>
                    </li>
                </ul>
            </div>
            {{#if (eq functionLog.detail.log_cd 'MODIFY') }}
            <div class="col-md-3">
                <ul class="list-dot">
                    <li>
                        <b>수정 항목​ </b> 
                        <span>
                            함수 수정​
                        </span>
                    </li>
                </ul>
            </div>
            {{/if}}
        </div>
    </div> <!-- // panel-body -->
</section>

{{#if (eq functionLog.detail.log_cd 'ERROR') }}
<section class="panel panel-bordered">
    <div class="panel-heading">
        <h3 class="panel-title">
            로그 내역​
        </h3>
    </div> <!-- // panel-heading -->
    <div class="panel-body">
        <div id="errorLogEditor" class="editorBox" style="height: ;">
        </div>
    </div> <!-- // panel-body -->
</section>

{{else if (eq functionLog.detail.log_cd 'MODIFY') }}
<section class="panel panel-bordered">
    <div class="panel-heading">
        <h3 class="panel-title">
            함수 수정 내역​
        </h3>
    </div> <!-- // panel-heading -->
    <div class="panel-body">
        <div class="row">
            <div class="col-md-6">
                <h4 class="heading h4">
                    최종 함수​
                </h4>
                <div id="sourceEditor" class="editorBox" style="height: ;">
                </div>
            </div>
            <div class="col-md-6">
                <h4 class="heading h4">
                    변경 함수​
                </h4>
                <div id="diffView" class="editorBox" style="height: ;">
                </div>
            </div>
        </div>
    </div> <!-- // panel-body -->
</section>
{{/if}}