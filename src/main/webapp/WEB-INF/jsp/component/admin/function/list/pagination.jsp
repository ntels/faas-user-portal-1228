<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!-- 페이징 -->
<nav class="text-center">
    <ul class="pagination">
        <li class="page-btn-text first"><a href="#" onclick="navcal(1);"><i class="fas fa-angle-double-left" style="line-height: 17px;"></i></a></li>
        <li class="page-btn-text prev"><a href="#" onclick="navcal({{nav.curpage}}-1);"><i class="fas fa-angle-left" style="line-height: 17px;"></i></a></li>
        {{#for nav.start nav.end}}
            {{#if (eq @index nav.curpage)}}
            <li class="active"><a href="#">{{@index}}</a></li>
            {{else}}
            <li><a href="#" onclick="navcal({{@index}})">{{@index}}</a></li>
            {{/if}}
        {{/for}}
        <li class="page-btn-text next"><a href="#" onclick="navcal({{nav.curpage}}+1);"><i class="fas fa-angle-right" style="line-height: 17px;"></i></a></li>
        <li class="page-btn-text last"><a href="#" onclick="navcal({{nav.lastpage}});"><i class="fas fa-angle-double-right" style="line-height: 17px;"></i></a></li>
    </ul>
</nav>

<script>
function navcal(movepage)
{
    // debugger;
    var nav = NtelsHB.Store.get("nav");
    if(movepage == nav.curpage) return;
    if(movepage <= 0) return
    if(movepage > nav.lastpage) return;

    nav.curpage = movepage;
    nav.start = Math.floor((movepage-1)/10)*10 + 1;
    nav.end = nav.start + 10 - 1;
    if(nav.end > nav.lastpage) nav.end = nav.lastpage;  
    
    if(typeof nav.onclick === 'function')
    {
        nav.onclick(movepage);
    }
    else
    {
        console.log("nav 데이타모델에 onclick가 정의되지 않았습니다.");
    }
    
    NtelsHB.Store.set("nav",nav);				
}
</script>