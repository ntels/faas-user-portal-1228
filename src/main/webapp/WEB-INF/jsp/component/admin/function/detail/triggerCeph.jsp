<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!-- TODO: ceph쪽 테이블 작업된게 없어 작업 불가능. 테이블 설계 이후에 작업예정 -->
<div class="panel-heading">
    <h3 class="panel-title">
        Ceph
    </h3>
</div> <!-- // panel-heading -->
<div class="panel-body">
    <div class="row padding-left-10">
        <div class="col-md-1">
            <div class="write-sub-title blt-dot">
                버킷​
            </div>
        </div>
        <div class="col-md-11">
            <div class="grey-600 font-size-13">
                Bucket_name​
            </div>
        </div>
    </div>
    <div class="row padding-left-10">
        <div class="col-md-1">
            <div class="write-sub-title blt-dot">
                트리거 이벤트 유형​
            </div>
        </div>
        <div class="col-md-11">
            <div class="grey-600 font-size-13">
                파일 생성​
                <span class="info-icon-tooltip" data-toggle="tooltip" title="" data-placement="right" data-original-title="파일이 생성될 때 함수가 실행됩니다.​">
                    i
                </span>
            </div>
        </div>
    </div>
    <div class="row padding-left-10">
        <div class="col-md-1">
            <div class="write-sub-title blt-dot">
                제한 경로
            </div>
        </div>
        <div class="col-md-11">
            <div class="grey-600 font-size-13">
                /image​
            </div>
        </div>
    </div>
    <div class="row padding-left-10">
        <div class="col-md-1">
            <div class="write-sub-title blt-dot">
                제한 확장자​
            </div>
        </div>
        <div class="col-md-11">
            <div class="grey-600 font-size-13">
                .jpg​
            </div>
        </div>
    </div>
</div> <!-- // panel-body -->