<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
    
<section class="panel panel-bordered">
    <div class="panel-heading padding-vertical-10 padding-horizontal-20">
        <div class="row">
            <div class="col-sm-6">
                <div>
                    <b>모니터링​</b>
                </div>
            </div>
            <div class="col-sm-6 text-right">
                <a href="#" target="_blank">
                    <i class="ico-img-grafana-s"></i>
                </a>
                <a href="#" class="btn btn-nostyle btn-sm setting-btn" onclick="javascript: monitoringReload(); return false;">
                    <i class="xi-renew"></i>
                </a>
            </div>
        </div> <!-- // panel-heading -->
    </div>
    <div class="panel-body">
        
        <div class="row">
            <div class="col-md-4">
                <h4 class="heading h4">
                    호출 건 수
                </h4>
                <div id="linechart1" class="chart" style="height: 300px"></div>
            </div>
            
            <div class="col-md-4">
                <h4 class="heading h4">
                    성공률
                </h4>
                <div id="linechart2" class="chart" style="height: 300px"></div>
            </div>

            <div class="col-md-4">
                <h4 class="heading h4">
                    지속 시간 (Duration)​
                </h4>
                <div id="linechart3" class="chart" style="height: 300px"></div>
            </div>
        </div>
        
    </div> <!-- // panel-body -->
</section>
<!-- // inner -->
<script>
	$(document).ready(function () {
		drawGraph();
	});

	function drawGraph() {
		var monitoring = NtelsHB.Store.get('monitoring');
		linechart1(monitoring.c3);
		linechart2(monitoring.c3);
		linechart3(monitoring.c3);
	}

	function linechart1(data) {

		data.time.unshift("time");
		data.cnt.unshift("호출 건수");
		
		var a = c3.generate({
			bindto: "#linechart1",
			data: {
				x: 'time', 
				columns: [
					data.time,data.cnt
				],
				colors: {
					'호출 건수': '#1f77b4'
				}
			},
			axis: {
				x: {
					type: 'category',
					padding: {top: 0, bottom: 0},
				},
				y: {
					min: 0,
					padding: {top: 0, bottom: 0},
				},
			},
		});
	};
	function linechart2(data) {
		
		data.rate.unshift("성공률");
		data.rate1.unshift("실패율");
		
		var a = c3.generate({
			bindto: "#linechart2",
			data: {
				x: 'time',
				columns: [
					data.time,data.rate,data.rate1
				],
				colors: {
					'성공률​': '#1f77b4',
					'실패율​': '#f96197'
				}
			},
			axis: {
				x: {
					type: 'category',
					padding: {top: 0, bottom: 0},
				},
				y: {
					min: 0,
					max: 100,
					padding: {top: 0, bottom: 0},
					tick: {
						count: 6
					}
				}
			},
		});
	};
	function linechart3(data) {
		
		data.p99.unshift("p99");
		data.p90.unshift("p90");
		data.p50.unshift("p50");
		
		var a = c3.generate({
			bindto: "#linechart3",
			data: {
				x: 'time', 
				columns: [
					data.time,data.p99,data.p90,data.p50
				],
				colors: {
					'p99​': '#4674d2',
					'p90​': '#83d0ef',
					'p50​': '#81d54e',
				}
			},
			axis: {
				x: {
					type: 'category',
					padding: {top: 0, bottom: 0},
				},
				y: {
					min: 0,
					padding: {top: 0, bottom: 0},
				},
			},
		});
	}
	function monitoringReload() {
		NtelsHB.Store.get("_reload")("MONITORING");
	}
</script>