<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>

<div class="panel-heading">
    <h3 class="panel-title">
        DB
    </h3>
</div> <!-- // panel-heading -->
<div class="panel-body">
    <div class="row padding-left-10">
        <div class="col-md-1">
            <div class="write-sub-title blt-dot">
                DB 유형
            </div>
        </div>
        <div class="col-md-11">
            <div class="grey-600 font-size-13">
                {{trigger.database.db_type}}
            </div>
        </div>
    </div>
    <div class="row padding-left-10">
        <div class="col-md-1">
            <div class="write-sub-title blt-dot">
                접속 정보​
            </div>
        </div>
        <div class="col-md-11">
            <div class="grey-600 font-size-13">
                <span>
                    접속 URL : 
                    <a href="#" onclick="javascript:copyToClipboard('{{trigger.database.con_host}}:{{trigger.database.con_port}}/{{trigger.database.db_name}}');" title="클립보드에 복사" target="_blank" data-role='btn' data-toggle='modal' data-target='#modalCopy'>
                        {{trigger.database.con_host}}:{{trigger.database.con_port}}/{{trigger.database.db_name}}
                        <span class="feather-icon">
                            <i data-feather="copy"></i>
                        </span>
                    </a>
                </span>
                <span class="margin-left-50">
                    관리자 계정 아이디 : 
                    <a href="#" onclick="javascript:copyToClipboard('{{trigger.database.admin_id}}');" title="클립보드에 복사" target="_blank" data-role='btn' data-toggle='modal' data-target='#modalCopy'>
                        {{trigger.database.admin_id}} 
                        <span class="feather-icon">
                            <i data-feather="copy"></i>
                        </span>
                    </a>
                </span>
            </div>
        </div>
    </div>
    {{#if (eq trigger.database.db_type "POSTGRESQL")}}
        <div class="row padding-left-10">
            <div class="col-md-1">
                <div class="write-sub-title blt-dot">
                    DB 테이블​
                </div>
            </div>
            <div class="col-md-11">
                <div class="grey-600 font-size-13">
                    {{trigger.database.table_name}}
                </div>
            </div>
        </div>
    {{else if (eq trigger.database.db_type "MONGODB")}}
        <div class="row padding-left-10">
            <div class="col-md-1">
                <div class="write-sub-title blt-dot">
                    컬렉션
                </div>
            </div>
            <div class="col-md-11">
                <div class="grey-600 font-size-13">
                    {{trigger.database.collection_name}}
                </div>
            </div>
        </div>
    {{/if}}
    <div class="row padding-left-10">
        <div class="col-md-1">
            <div class="write-sub-title blt-dot">
                트리거 이벤트 유형​
            </div>
        </div>
        <div class="col-md-11">
            {{#if (eq trigger.database.insert_yn 'Y')}}
                <div class="grey-600 font-size-13">
                    INSERT
                    <span class="info-icon-tooltip" data-toggle="tooltip" title="" data-placement="right" data-original-title="DB에 새로운 데이터가 생성되면 함수가 실행됩니다.​">
                        i
                    </span>
                </div>
            {{else if (eq trigger.database.update_yn 'Y')}}
                <div class="grey-600 font-size-13">
                    UPDATE
                    <span class="info-icon-tooltip" data-toggle="tooltip" title="" data-placement="right" data-original-title="DB 데이터의 수정이 발생하면 함수가 실행됩니다.​">
                        i
                    </span>
                </div>
            {{else if (eq trigger.database.delete_yn 'Y')}}
                <div class="grey-600 font-size-13">
                    DELETE
                    <span class="info-icon-tooltip" data-toggle="tooltip" title="" data-placement="right" data-original-title="DB에 적재된 데이터가 삭제되면 함수가 실행됩니다.​">
                        i
                    </span>
                </div>
            {{/if}}
        </div>
    </div>
</div> <!-- // panel-body -->

<script>
    function copyToClipboard(text) {
        /* Get the text field */
        var copyText = document.createElement("input");
        document.body.appendChild(copyText);
        copyText.value = text;

        /* Select the text field */
        copyText.select();
        copyText.setSelectionRange(0, 99999); /*For mobile devices*/

        /* Copy the text inside the text field */
        document.execCommand("copy");
        alert("복사하였습니다.");
    }
</script>