<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
    
{{#if isCard}}
<button type="button" class="btn btn-primary" id="viewCard">
	<div class="feather-icon">
		<i data-feather="grid"></i>
	</div>
	<span class="sr-only">카드형 목록</span>
</button>
<button type="button" class="btn btn-default" id="viewList">
	<div class="feather-icon">
		<i data-feather="list"></i>
	</div>
	<span class="sr-only">테이블형 목록</span>
</button>

{{else}}

<button type="button" class="btn btn-default" id="viewCard">
	<div class="feather-icon">
		<i data-feather="grid"></i>
	</div>
	<span class="sr-only">카드형 목록</span>
</button>
<button type="button" class="btn btn-primary" id="viewList">
	<div class="feather-icon">
		<i data-feather="list"></i>
	</div>
	<span class="sr-only">테이블형 목록</span>
</button>
{{/if}}