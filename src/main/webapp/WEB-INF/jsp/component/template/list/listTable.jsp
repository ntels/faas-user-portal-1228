<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
    
<div class="table-wrap  margin-vertical-20">
	<table class="table table-outline">
		<colgroup>
		{{#each table.widths}}
			{{#if this}}
			<col style="width: {{this}}">
			{{else}}
			<col>
			{{/if}}
		{{/each}}
		</colgroup>
		<thead class="bg-blue-grey-100">
			<tr class="text-center">
			{{#each table.headers}}
				<th scope="col">{{this}}</th>
			{{/each}}
			</tr>
		</thead>
		<tbody>
		{{#each list}}
			{{component "listTableItem"}}
		{{/each}}
		</tbody>
	</table>
</div>