<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
    
<div class="nodata-box">
	<div class="nodata">
		<div class="img-div"></div>
		<p>검색된 데이터가 없습니다. <span class="blue bold">다른 조건으로 검색해 보세요.​</span></p>
	</div>
</div>