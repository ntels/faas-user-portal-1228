<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
    
{{#if (or (eq top_yn 'Y') (eq top_yn 'y'))}}
<tr class="ranking">
{{else}}
<tr>
{{/if}}
	<td class="text-center">
		<div class="radio-group" name="template-list-table" template-id="{{template_id}}">
			<input type="radio" name="template-list-table-btn" class="radio-input">
			<label for="radio01" class="radio-item">
				<div class="radio">
					<div class="checked-icon"></div>
				</div>
			</label>
		</div>
	</td>
	<td>
		{{name}}
	</td>
	<td class="text-center">
		{{runtime}} {{version}}​
	</td>
	<td class="pos-r padding-right-30">
		{{des}}
		{{#if (or (eq top_yn 'Y') (eq top_yn 'y'))}}
		<div class="card-mark">
			<i class="xi-star"></i>
		</div>
		{{/if}}
	</td>
</tr>