<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
    
<div class="col-md-3">
	<div name="template-list-card" class="card card-type2 card-hover-outline-primary" template-id="{{template_id}}">
		<div class="card-header">
			{{#if (or (eq top_yn 'Y') (eq top_yn 'y'))}}
			<div class="card-mark">
				<i class="xi-star"></i>
			</div>
			{{/if}}
			<div class="card-info-area">
				<div class="text-box">
					<div class="top">
						<div class="clearfix">
							<div class="card-title margin-bottom-0">
								<b>
									{{name}}
								</b>
							</div>
						</div>
						<div class="clearfix">
							<span class="lagn">
								{{#if (eq runtime 'nodejs')}}
								<span class="img-nodejs img-size15">
								</span>
								{{runtime}} {{version}}
								
								{{else}}
								
								<span class="img-python img-size15">
								</span>
								{{runtime}} {{version}}
								{{/if}}
							</span>
						</div>
					</div>
				</div>
			</div>
		</div>
		
		<div class="card-body">
			<div class="ellipsis">
				{{des}} 
			</div>
		</div>
	</div>
</div>