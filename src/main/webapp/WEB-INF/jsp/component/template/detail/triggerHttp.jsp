<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>

{{#each info.triggerinfo.HTTP}}
<!-- HTTP 트리거 설정 -->
<div class="panel-body http-trigger" http_trigger_id="{{trigger_id}}" trigger-type="{{trigger_tp}}">
	<h3 class="panel-title title-type2">
		HTTP 트리거 설정​
	</h3>
	<div class="row">
		<div class="col-md-6">
			<div class="write-sub-title">
				<b class="caution-star">*</b> API 이름​
				<span class="info-icon-tooltip" data-toggle="tooltip" title="API 이름을 입력하면, 자동으로 앤드포인트가 생성됩니다. API 이름은 공백 없이 영문 소문자와 언더바(_)만 입력 가능합니다.​" data-placement="right">
					i
				</span>
			</div>
			<div class="form-group">
			{{#if (eq adm_set_yn 'Y')}}
				<input id="http_api_name_{{trigger_id}}" type="text" class="form-control" readonly value='{{api_name}}'>
			{{else}}
				<input id="http_api_name_{{trigger_id}}" type="text" class="form-control" placeholder="API 이름 입력 (예 : api_name)​​" trigger_id="{{trigger_id}}" name="http_api_name">
			{{/if}}
			</div>					
		</div>		
	</div>		
</div>

{{/each}}
<div class="panel-body" style="display:none;">

<script type="text/javascript">
	function triggerHttpEvent()
	{
		$('[name="http_api_name"]').keyup(function(){
			var trigger_id = $(this).attr("trigger_id");
			$('#http_api_name_'+trigger_id).closest('div').removeClass('form-danger-before');
		});
	}
	
	function triggerHttpValidation(trigger_id,data)
	{
		var obj = $('#http_api_name_'+trigger_id);
		if(obj.val() == "")
		{
			data.validation = false;
			obj.closest('div').addClass('form-danger-before');
			return false;
		}
		return true;
	}
	
	function triggerHttpData(trigger_id)
	{
		var data = {};
		data.trigger_id = trigger_id;
		data.api_name = $('#http_api_name_'+trigger_id).val();
		return data;
	}
</script>

</div>
