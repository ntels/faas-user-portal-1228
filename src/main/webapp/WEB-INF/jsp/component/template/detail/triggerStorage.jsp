<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>

{{#each info.triggerinfo.Storage}}
<!-- Ceph 트리거 설정 -->
<div class="panel-body">
	<h3 class="panel-title title-type2">
		Ceph 트리거 설정​
	</h3>
	<div class="row">
		<div class="col-md-6">
			<div class="write-sub-title">
				<b class="caution-star">*</b> 버킷​
				<span class="info-icon-tooltip" data-toggle="tooltip" title="사전에 생성해 둔 버킷을 선택해 주세요.  버킷이 없으면 이용하실 수 없습니다.​​" data-placement="right">
					i
				</span>
			</div>
			<div class="form-group">
				<select name="" id="formName" class="form-control">
					<option value="">버킷 선택</option>
				</select>
			</div>				
		</div>		
	</div>		
	
	<div class="write-sub-title">
		<b class="caution-star">*</b> 트리거 이벤트 유형​
		<span class="info-icon-tooltip" data-toggle="tooltip" title="함수가 실행될 트리거 이벤트를 선택합니다. 선택한 이벤트가 발생할 경우에 한하여 함수가 실행됩니다,​" data-placement="right">
			i
		</span>
	</div>

	<div class="row">
		<div class="col-md-6">
			<label for="card01" class="input-card-type">
				<input type="radio" id="card01" name="card1" class="card-input-element" checked>
				<div class="card-input">
					<div class="card-type4">
						<span class="img-size50 img-file"></span>
						<div class="text-area">
							<div class="title">파일 생성​​</div>
							<p>
								파일이 생성될 때 함수가 실행됩니다.
							</p>
						</div>
					</div>
				</div>
			</label>
		</div>
		<div class="col-md-6">
			<label for="card02" class="input-card-type">
				<input type="radio" id="card02" name="card1" class="card-input-element">
				<div class="card-input">
					<div class="card-type4">
						<span class="img-size50 img-delete"></span>
						<div class="text-area">
							<div class="title">파일 삭제​</div>
							<p>
								파일이 삭제될 때 함수가 실행됩니다.​
							</p>
						</div>
					</div>
				</div>
			</label>
		</div>
	</div>

	<div class="row">
		<div class="col-md-6">
			<div class="write-sub-title">
				제한 경로​
				<span class="info-icon-tooltip" data-toggle="tooltip" title="설정한 경로 내에서 발생한 이벤트만 트리거로 작동합니다.​​​" data-placement="right">
					i
				</span>
			</div>
			<div class="form-group">
				<input type="text" class="form-control" id="" placeholder="예 : images/​​​">
			</div>
		</div>
		<div class="col-md-6">
			<div class="write-sub-title">
				제한 확장자​
				<span class="info-icon-tooltip" data-toggle="tooltip" title="설정한 확장자의 파일 유형만 트리거로 작동합니다.​​​​" data-placement="right">
					i
				</span>
			</div>
			<div class="form-group">
				<input type="text" class="form-control" id="" placeholder="예 : .jpg​​​​">
			</div>
		</div>
	</div>
</div>
{{/each}}

<div class="panel-body" style="display:none;">
<script>
	function triggerStorageEvent()
	{
		
		
	}
	
	function triggerStorageValidation(trigger_id)
	{
		
	}
	
	function triggerStorageData(trigger_id)
	{
		
	}

</script>
</div>
