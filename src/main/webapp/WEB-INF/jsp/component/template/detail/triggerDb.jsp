<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>

{{#each info.triggerinfo.DB}}
<!-- DB 트리거 설정 -->
<div class="panel-body db-trigger" db_trigger_id="{{trigger_id}}" trigger-type="{{trigger_tp}}">
	<h3 class="panel-title title-type2">
		DB 트리거 설정​
	</h3>
	<div class="write-sub-title">
		<b class="caution-star">*</b> DB 유형​
		<span class="info-icon-tooltip" data-toggle="tooltip" title="트리거로 이용할 DB 유형을 선택해 주세요.​​" data-placement="right">
			i
		</span>
	</div>
	
	{{#if (eq adm_set_yn 'Y')}}
	<div class="row">
		<div class="col-md-6">
			<div class="form-group">
				<select id="config_db_type_{{trigger_id}}" name="" data-sel="dbSel" class="form-control" trigger_id="{{trigger_id}}">
					{{#if (eq db_type 'mongodb')}}
					<option value="mongodb" selected>MongoDB</option>
					{{/if}}
					{{#if (eq db_type 'postgre')}}
	                <option value="postgresql" selected>PostgreSQL</option>
	                {{/if}}
					{{#if (eq db_type 'mariadb')}}
	                <option value="mariadb" selected>MariaDB</option>
	                {{/if}}
				</select>
			</div>	
		</div>
	</div>
	
	<!--본문 속 알림문구 (정상) -->
	<div id="mongodb_msg_area_{{trigger_id}}" class="alert alert-primary" role="alert" name="msg_area" {{#if (neq db_type 'mongodb')}}style="display:none;"{{/if}}>
		<i data-feather="file-text"></i>
		<div class="text-area">
			<a href="SC_KS_01_001.html" class="link2">쿠버네티스 서비스</a>를 통해 사전에 설치한 DB가 있을 경우에만 이용할 수 있습니다.​<br>
			쿠버네티스 서비스 > 카탈로그 앱 메뉴에서 <span class="red bold">mongodb-replicaset</span> 앱을 설치한 후 이용해 주세요.​
		</div>
	</div>						
	<div id="postgresql_msg_area_{{trigger_id}}" class="alert alert-primary" role="alert" name="msg_area" {{#if (neq db_type 'postgre')}}style="display:none;"{{/if}}>
		<i data-feather="file-text"></i>
		<div class="text-area">
			<a href="SC_KS_01_001.html" class="link2">쿠버네티스 서비스</a>를 통해 사전에 설치한 DB가 있을 경우에만 이용할 수 있습니다.​<br>
			쿠버네티스 서비스 > 카탈로그 앱 메뉴에서 <span class="red bold">postgresql</span> 앱을 설치한 후 이용해 주세요.​
		</div>
	</div>
	<div id="mariadb_msg_area_{{trigger_id}}" class="alert alert-primary" role="alert" name="msg_area" {{#if (neq db_type 'mariadb')}}style="display:none;"{{/if}}>
		<i data-feather="file-text"></i>
		<div class="text-area">
			<a href="SC_KS_01_001.html" class="link2">쿠버네티스 서비스</a>를 통해 사전에 설치한 DB가 있을 경우에만 이용할 수 있습니다.​<br>
			쿠버네티스 서비스 > 카탈로그 앱 메뉴에서 <span class="red bold">MariaDB</span> 앱을 설치한 후 이용해 주세요.​
		</div>
	</div>
	
	{{else}}
	<div class="row">
		<div class="col-md-6">
			<div class="form-group">
				<select id="config_db_type_{{trigger_id}}" name="" data-sel="dbSel" class="form-control" trigger_id="{{trigger_id}}">
					<option value="mongodb" selected>MongoDB</option>
	                <option value="postgresql">PostgreSQL</option>
	                <option value="mariadb">MariaDB</option>
				</select>
			</div>	
		</div>
	</div>
	
	<!--본문 속 알림문구 (정상) -->
	<div id="mongodb_msg_area_{{trigger_id}}" class="alert alert-primary" role="alert" name="msg_area">
		<i data-feather="file-text"></i>
		<div class="text-area">
			<a href="SC_KS_01_001.html" class="link2">쿠버네티스 서비스</a>를 통해 사전에 설치한 DB가 있을 경우에만 이용할 수 있습니다.​<br>
			쿠버네티스 서비스 > 카탈로그 앱 메뉴에서 <span class="red bold">mongodb-replicaset</span> 앱을 설치한 후 이용해 주세요.​
		</div>
	</div>						
	<div id="postgresql_msg_area_{{trigger_id}}" class="alert alert-primary" role="alert" name="msg_area" style="display:none;">
		<i data-feather="file-text"></i>
		<div class="text-area">
			<a href="SC_KS_01_001.html" class="link2">쿠버네티스 서비스</a>를 통해 사전에 설치한 DB가 있을 경우에만 이용할 수 있습니다.​<br>
			쿠버네티스 서비스 > 카탈로그 앱 메뉴에서 <span class="red bold">postgresql</span> 앱을 설치한 후 이용해 주세요.​
		</div>
	</div>
	<div id="mariadb_msg_area_{{trigger_id}}" class="alert alert-primary" role="alert" name="msg_area" style="display:none;">
		<i data-feather="file-text"></i>
		<div class="text-area">
			<a href="SC_KS_01_001.html" class="link2">쿠버네티스 서비스</a>를 통해 사전에 설치한 DB가 있을 경우에만 이용할 수 있습니다.​<br>
			쿠버네티스 서비스 > 카탈로그 앱 메뉴에서 <span class="red bold">MariaDB</span> 앱을 설치한 후 이용해 주세요.​
		</div>
	</div>	
	{{/if}}
	
	<div class="row margin-top-30">
		<div class="col-md-4">
			<div class="write-sub-title">
				<b class="caution-star">*</b> 접속 URL​
				<span class="info-icon-tooltip" data-toggle="tooltip" title="선택한 DB의 접속 URL을 입력해 주세요.​​​​​" data-placement="right">
					i
				</span>
			</div>
			<div class="form-group">
				<input id="config_con_host_{{trigger_id}}" name="db_config_items" type="text" class="form-control" placeholder="접속 URL을 입력해 주세요.​" trigger_id="{{trigger_id}}">
			</div>
		</div>
		<div class="col-md-4">
			<div class="write-sub-title">
				<b class="caution-star">*</b> 접속 PORT​
				<span class="info-icon-tooltip" data-toggle="tooltip" title="선택한 DB의 접속 PORT를 입력해 주세요.​" data-placement="right">
					i
				</span>
			</div>
			<div class="form-group">
				<input id="config_con_port_{{trigger_id}}" name="db_config_items" type="text" class="form-control" numberOnly placeholder="접속 Port를 입력해 주세요.​" trigger_id="{{trigger_id}}">
			</div>
		</div>
	</div>
	<div class="row margin-top-30">
		<div class="col-md-4">
			<div class="write-sub-title">
				<b class="caution-star">*</b> 데이터베이스​
				<span class="info-icon-tooltip" data-toggle="tooltip" title="선택한 DB의 데이터베이스를 입력해 주세요.​​​​​" data-placement="right">
					i
				</span>
			</div>
			<div class="form-group">
				<input id="config_db_name_{{trigger_id}}" name="db_config_items" type="text" class="form-control" placeholder="데이터베이스를 입력해 주세요.​" trigger_id="{{trigger_id}}">
			</div>
		</div>
	</div>
	<div class="row margin-top-30">
		<div class="col-md-4">
			<div class="write-sub-title">
				<b class="caution-star">*</b> 관리자 계정 아이디​
				<span class="info-icon-tooltip" data-toggle="tooltip" title="반드시 관리자 권한을 가진 아이디를 입력해 주세요.​​​​​​" data-placement="right">
					i
				</span>
			</div>
			<div class="form-group">
				<input id="config_admin_id_{{trigger_id}}" name="db_config_items" type="text" class="form-control" placeholder="관리자 계정 아이디를 입력해 주세요.​​" trigger_id="{{trigger_id}}">
			</div>
		</div>
		<div class="col-md-4">
			<div class="write-sub-title">
				<b class="caution-star">*</b> 관리자 계정 비밀번호​
				<span class="info-icon-tooltip" data-toggle="tooltip" title="입력한 관리자 계정의 비밀번호를 입력해 주세요.​​​​​​" data-placement="right">
					i
				</span>
			</div>
			<div class="form-group">
				<input id="config_admin_pw_{{trigger_id}}" name="db_config_items" type="text" class="form-control" placeholder="관리자 계정 비밀번호를 입력해 주세요.​​" trigger_id="{{trigger_id}}">
			</div>
		</div>
	</div>

	<div class="text-center margin-top-20 form-group">
		<button class="btn btn-primary" trigger_id="{{trigger_id}}" name="btn_con_test" con_test="N">접속 테스트​</button><p/>
		<small class="form-text" id="error_btn_con_test_{{trigger_id}}" style="display:none;">접속 테스트를 먼저 진행해 주세요.</small>
		
	</div>

	<div class="row" id="postgresql_config_area_{{trigger_id}}" style="display: none;">
		<div class="col-md-6">
			<div class="write-sub-title">
				<b class="caution-star">*</b> DB 테이블
				<span class="info-icon-tooltip" data-toggle="tooltip" title="사전에 생성해 둔 DB 테이블을 선택해 주세요.  DB 테이블이 없으면 이용하실 수 없습니다.​" data-placement="right">
					i
				</span>
			</div>
			<div class="form-group">
				<select id="postgresql_table_list_{{trigger_id}}" name="trig_modal_select_box_items" class="form-control">
					<option value="">DB 테이블 선택​</option>
				</select>
				<small id="postgresql_empty_table_err_{{trigger_id}}" class="form-text color-danger" style="display: none;">
					등록된 DB 테이블이 없습니다. 쿠버네티스 서비스의 카탈로그 앱에서 PostgreSQL 테이블을 추가한 후 이용해 주세요.​
				</small>
			</div>
		</div>
	</div>
	
	<div class="row" id="mariadb_config_area_{{trigger_id}}" style="display: none;">
		<div class="col-md-6">
			<div class="write-sub-title">
				<b class="caution-star">*</b> DB 테이블
				<span class="info-icon-tooltip" data-toggle="tooltip" title="사전에 생성해 둔 DB 테이블을 선택해 주세요.  DB 테이블이 없으면 이용하실 수 없습니다.​" data-placement="right">
					i
				</span>
			</div>
			<div class="form-group">
				<select id="mariadb_table_list_{{trigger_id}}" name="trig_modal_select_box_items" class="form-control">
					<option value="">DB 테이블 선택​</option>
				</select>
				<small id="mariadb_empty_table_err_{{trigger_id}}" class="form-text color-danger" style="display: none;">
					등록된 DB 테이블이 없습니다. 쿠버네티스 서비스의 카탈로그 앱에서 PostgreSQL 테이블을 추가한 후 이용해 주세요.​
				</small>
			</div>
		</div>
	</div>

	<div class="row" id="mongodb_config_area_{{trigger_id}}" style="display: none;">
		<div class="col-md-6">
			<div class="write-sub-title">
				<b class="caution-star">*</b> 컬렉션
				<span class="info-icon-tooltip" data-toggle="tooltip" title="MongoDB 대상 컬렉션을 선택해 주세요.​​" data-placement="right">
					i
				</span>
			</div>
			<div class="form-group">
				<select id="mongodb_table_list_{{trigger_id}}" name="trig_modal_select_box_items" class="form-control">
					<option value="">컬렉션 선택​</option>
				</select>
				<small id="mongodb_empty_table_err_{{trigger_id}}" class="form-text color-danger" style="display: none;">
					등록된 DB 테이블이 없습니다. 쿠버네티스 서비스의 카탈로그 앱에서 PostgreSQL 테이블을 추가한 후 이용해 주세요.​
				</small>
			</div>
		</div>
	</div>
	
	<div id="title_event_config_area_{{trigger_id}}" class="write-sub-title" style="display:none;">
		<b class="caution-star">*</b> 트리거 이벤트 유형​
		<span class="info-icon-tooltip" data-toggle="tooltip" title="함수가 실행될 트리거 이벤트를 선택합니다. 선택한 이벤트가 발생할 경우에 한하여 함수가 실행됩니다.​​" data-placement="right">
			i
		</span>
	</div>
	<div id="event_config_area_{{trigger_id}}" class="row" style="display:none;">
		{{#if (eq adm_set_yn 'Y')}}
			{{#if (eq insert_yn 'Y')}}
			<div class="col-md-4">
				<label for="INSERT​" class="input-card-type">
					<input type="radio" id="INSERT​" event_type="insert" name="event_type_{{trigger_id}}" class="card-input-element on" checked>
					<div class="card-input">
						<div class="card card-type-img1">
							<div class="img-content-type2">
								<div class="img-trigger img-insert"></div>
								<div class="name font-size-16">
									<b>INSERT​</b>
								</div>
								<span>DB에 새로운 데이터가 생성되면​<br> 함수가 실행됩니다.​</span>
							</div>
						</div>
					</div>
				</label>
			</div>
			{{/if}}
			{{#if (eq update_yn 'Y')}}
			<div class="col-md-4">
				<label for="UPDATE​" class="input-card-type">
					<input type="radio" id="UPDATE​" event_type="update" name="event_type_{{trigger_id}}" class="card-input-element on">
					<div class="card-input">
						<div class="card card-type-img1">
							<div class="img-content-type2">
								<div class="img-trigger img-update"></div>
								<div class="name font-size-16">
									<b>UPDATE​</b>
								</div>
								<span>DB 데이터의 수정이 발생하면<br>​ 함수가 실행됩니다.​​</span>
							</div>
						</div>
					</div>
				</label>
			</div>
			{{/if}}
			{{#if (eq delete_yn 'Y')}}
			<div class="col-md-4">
				<label for="DELETE​" class="input-card-type">
					<input type="radio" id="DELETE​" event_type="delete" name="event_type_{{trigger_id}}" class="card-input-element on">
					<div class="card-input">
						<div class="card card-type-img1">
							<div class="img-content-type2">
								<div class="img-trigger img-delete"></div>
								<div class="name font-size-16">
									<b>DELETE​</b>
								</div>
								<span>DB에 적재된 데이터가 삭제되면​<br> 함수가 실행됩니다.​</span>
							</div>
						</div>
					</div>
				</label>
			</div>
			{{/if}}
		{{else}}
		<div class="col-md-4 type_area">
			<label for="INSERT​" class="input-card-type">
				<input type="radio" id="INSERT​" event_type="insert" name="event_type_{{trigger_id}}" class="card-input-element on" checked>
				<div class="card-input">
					<div class="card card-type-img1">
						<div class="img-content-type2">
							<div class="img-trigger img-insert"></div>
							<div class="name font-size-16">
								<b>INSERT​</b>
							</div>
							<span>DB에 새로운 데이터가 생성되면​<br> 함수가 실행됩니다.​</span>
						</div>
					</div>
				</div>
			</label>
		</div>
		<div class="col-md-4">
			<label for="UPDATE​" class="input-card-type">
				<input type="radio" id="UPDATE​" event_type="update" name="event_type_{{trigger_id}}" class="card-input-element">
				<div class="card-input">
					<div class="card card-type-img1">
						<div class="img-content-type2">
							<div class="img-trigger img-update"></div>
							<div class="name font-size-16">
								<b>UPDATE​</b>
							</div>
							<span>DB 데이터의 수정이 발생하면<br>​ 함수가 실행됩니다.​​</span>
						</div>
					</div>
				</div>
			</label>
		</div>
		<div class="col-md-4">
			<label for="DELETE​" class="input-card-type">
				<input type="radio" id="DELETE​" event_type="delete" name="event_type_{{trigger_id}}" class="card-input-element">
				<div class="card-input">
					<div class="card card-type-img1">
						<div class="img-content-type2">
							<div class="img-trigger img-delete"></div>
							<div class="name font-size-16">
								<b>DELETE​</b>
							</div>
							<span>DB에 적재된 데이터가 삭제되면​<br> 함수가 실행됩니다.​</span>
						</div>
					</div>
				</div>
			</label>
		</div>
		{{/if}}
	</div>
</div>
{{/each}}

<div class="panel-body" style="display:none;">

<script type="text/javascript">
	function triggerDBEvent()
	{
		function resetValid(trigger_id)
		{
			var thisObj = $('[db_trigger_id="'+trigger_id+'"]');
			thisObj.find('[name="db_config_items"]').each(function(i,item){
	       		$(item).closest('div').removeClass('form-danger-before');
	        });
			thisObj.find('[name="btn_con_test"]').html('접속 테스트');
			thisObj.find('[name="btn_con_test"]').attr('con_test','N');
			thisObj.find('#error_btn_con_test_'+trigger_id).hide();
			thisObj.find('#error_btn_con_test_'+trigger_id).closest('div').removeClass('form-danger-before');
		}
		
		function resetTableInfo(trigger_id)
		{
			var thisObj = $('[db_trigger_id="'+trigger_id+'"]');
			thisObj.find('[name="trig_modal_select_box_items"] option').each(function(i,item){
				if($(item).val() != "") $(item).remove();
			});
	        thisObj.find('[name="table_select"]').hide();
	        thisObj.find('#event_config_area_'+trigger_id).hide();
	        thisObj.find('#title_event_config_area_'+trigger_id).hide();
	        thisObj.find('#error_btn_con_test_'+trigger_id).hide();
	        thisObj.find('#error_btn_con_test_'+trigger_id).closest('div').removeClass('form-danger-before');
		}
		
		function isValidAndGetTableList(trigger_id) 
		{
	        var bvalid = true;
	        var thisObj = $('[db_trigger_id="'+trigger_id+'"]');
	        thisObj.find('[name="db_config_items"]').each(function(i,item)
	        {
	        	if( $(item).val() == "")
	        	{
	        		$(item).closest('div').addClass('form-danger-before');
	        		bvalid = false;
	        		return false;
	        	}
	        	else
	        		$(item).closest('div').removeClass('form-danger-before');
	        });
	        
	        if(bvalid == false) return;

	        var param = {
	            function_id: "${paramMap.function_id}",
	            db_type: $('#config_db_type_'+trigger_id).val(),
	            con_host: $('#config_con_host_'+trigger_id).val(),
	            con_port: $('#config_con_port_'+trigger_id).val(),
	            db_name: $('#config_db_name_'+trigger_id).val(),
	            admin_id: $('#config_admin_id_'+trigger_id).val(),
	            admin_pw: $('#config_admin_pw_'+trigger_id).val()
	        }

	        JsonCall('${pageContext.request.contextPath}/service/isValidAndGetTableList.json', 'param=' + JSON.stringify(param), function(response) {
	            ret = JSON.parse(response.responseText);
	            if(ret.isSuccess == true) {
	            	if(ret.isValid == false)
	            	{
	            		thisObj.find('[name="btn_con_test"]').html('접속 테스트 실패');
	            	}
	            	else
	            	{
		            	var targetDB = thisObj.find("select option:selected").val();
		                var selectObj = thisObj.find("#"+targetDB+"_table_list_"+trigger_id);
		                var emptyTable = thisObj.find("#"+targetDB+"_empty_table_err_"+trigger_id);
		                
		                thisObj.find("#"+targetDB+"_config_area_"+trigger_id).show();
		                thisObj.find("#event_config_area_"+trigger_id).show();
		                thisObj.find('#title_event_config_area_'+trigger_id).show();
		                
		                if(ret.tableList.length > 0)
		                {
			                $(ret.tableList).each(function(i,item){
			                	selectObj.append("<option value='"+item+"'>"+item+"</option>");
			                });
		                }
		                else
		                {
		                	emptyTable.show();
		                }
		                thisObj.find('[name="btn_con_test"]').html('접속 테스트 완료');
		                thisObj.find('[name="btn_con_test"]').attr('con_test','Y');
	            	}
	            } else {
	                fnModalAlert('에러가 발생하였습니다.\n관리자에게 문의 바랍니다.');
	                global.showLoading(false);
	            }
	        }, true);
	    }// end function
		
		$('[event_type]').click( function() {
			
			$(this).closest('.type_area').find('[event_type]').removeClass('on');
			$(this).addClass('on')
			return;
			
			if($(this).hasClass('on')) {
				return false;
			} else {
				$(this).closest('.type_area').find('[event_type]').removeClass('on');
				$(this).addClass('on')
			};
		});
		
		$('[data-sel="dbSel"]').change(function() {
			var trigger_id = $(this).attr("trigger_id");
			var thisObj = $('[db_trigger_id="'+trigger_id+'"]');
			var val = $(this).val();
			thisObj.find('[name="msg_area"]').hide();
			$('#'+val+'_msg_area_'+trigger_id).show();
			resetValid(trigger_id);
			resetTableInfo(trigger_id);
		});
		
		$('[name="btn_con_test"]').click(function(){
			var trigger_id = $(this).attr("trigger_id");
			isValidAndGetTableList(trigger_id);
			resetTableInfo(trigger_id);
		});
		
		$('[name="db_config_items"]').keyup(function(){
			var trigger_id = $(this).attr("trigger_id");
			resetValid(trigger_id);
		});
		
		// 숫자만 입력가능
		$("input:text[numberOnly]").on("keyup", function() {
		    $(this).val($(this).val().replace(/[^0-9]/g,""));
		});
		
	}
	
	function triggerDBValidation(trigger_id,data)
	{
		function isEmpty(selector)
		{
			var obj = thisObj.find(selector);
			obj.each(function(i,item){
				if($(item).val() == "")
				{
					data.validation = false;
					if(data.validation == false) debugger;
					$(item).addClass('false');
					return false;
				}
			});
		}
	    
		var thisObj = $('[db_trigger_id="'+trigger_id+'"]');
		var dbtype = $("#config_db_type_"+trigger_id).val();
		var selectTable = '#'+dbtype+'table_list_'+trigger_id;
		
		if(isEmpty("#config_db_type_"+trigger_id)) return;
		if(isEmpty('[name="db_config_items"]')) return;
		if(isEmpty(selectTable)) return;
		if(thisObj.find('[name="btn_con_test"]').attr('con_test')=='N')
		{
			thisObj.find('#error_btn_con_test_'+trigger_id).show();
			thisObj.find('#error_btn_con_test_'+trigger_id).closest('div').addClass('form-danger-before');
		}
	}
	
	function triggerDBData(trigger_id)
	{
		var data = {};
		data.trigger_id = trigger_id;
		var thisObj = $('[db_trigger_id="'+trigger_id+'"]');
		function getValue(selector)
		{
			if(selector.indexOf('#')==0)
				return thisObj.find(selector+'_'+trigger_id).val();
			else
				return thisObj.find(selector).val();
		}
		function setEvnetType()
		{
			var et = thisObj.find('[name="event_type_'+trigger_id+'"].on').attr('event_type');
			data.insert_yn = data.update_yn = data.delete_yn = "N";
			data[et+'_yn'] = "Y";				
		}
		
		data.db_type = getValue('#config_db_type');
		data.db_name = getValue('#config_db_name');
		data.con_host = getValue('#config_con_host');
		data.con_port = getValue('#config_con_port');
		data.admin_id = getValue('#config_admin_id');
		data.admin_pw = getValue('#config_admin_pw');
		data.table_name = data.db_type!='mongodb' ? getValue("#"+data.db_type+'_table_list') : "";
		data.collection_name = data.db_type=='mongodb' ? getValue("#"+data.db_type+'_table_list') : "";
		setEvnetType();
		
		return data;
	}
	
</script>

</div>


