<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>

{{#each info.triggerinfo.Timer}}
<!-- 타이머 트리거 설정 -->
<div class="panel-body timer-trigger" timer_trigger_id="{{trigger_id}}" trigger-type="{{trigger_tp}}">
	<h3 class="panel-title title-type2">
		타이머 트리거 설정​
	</h3>
	<div class="write-sub-title">
		<b class="caution-star">*</b> 기준 일시​
		<span class="info-icon-tooltip" data-toggle="tooltip" title="트리거 이벤트가 발생할 날짜와 시간을 설정해 주세요." data-placement="right">
			i
		</span>
	</div>
	<div class="form-inline">
		<div class="form-group">
			<div class="input-icon-type2 icon-left">
				<span class="input-icon-addon">
					<i class="xi-calendar"></i>
				</span>
				<input type="text" class="form-control datepicker" id="bas_date_hms_date_{{trigger_id}}">
			</div>
		</div>
		<div class="form-group">
			<select class="form-control" id="bas_date_hms_hour_{{trigger_id}}">
				{{#for 0 23}}
				<option value="{{@index}}">{{@index}}시</option>
				{{/for}}
			</select>
		</div>
		<div class="form-group">
			<select class="form-control" id="bas_date_hms_miniute_{{trigger_id}}">
				{{#for 0 59}}
				<option value="{{@index}}">{{@index}}분</option>
				{{/for}}
			</select>
		</div>
	</div>

	{{#if (eq adm_set_yn 'Y')}}
	<div class="write-sub-title">
		<b class="caution-star">*</b> 반복 설정
	</div>
	<div data-lst="admin" style="width: 800px">
		<ul class="list-inline">
			{{#if (eq repeat_tp 'NO')}}
			<li>
				<label for="triggeradd1" class="input-card-type">
					<input type="radio" id="triggeradd1" name="repeatSet" class="card-input-element on" data-btn="No_repeat" data-repeat-tp="NO" trigger_id="{{trigger_id}}">
					<div class="card-input">
						<div class="card-type4">
							<span class="img-repeat-big">
								<span class="none">
									<i></i>
								</span>
							</span>
						</div>
					</div>
					<p class="text-center">
						{{repeat_des}}
					</p>
				</label>
			</li>
			{{/if}}
			{{#if (eq repeat_tp 'MN')}}
			<li>
				<label for="triggeradd2" class="input-card-type">
					<input type="radio" id="triggeradd2" name="repeatSet" class="card-input-element" data-btn="Minute" data-repeat-tp="MN" trigger_id="{{trigger_id}}">
					<div class="card-input">
						<div class="card-type4">
							<span class="img-repeat-big">
								<span class="minute">
									<i></i>
								</span>
							</span>
						</div>
					</div>
					<p class="text-center">
						{{repeat_des}}
					</p>
				</label>
			</li>
			{{/if}}
			{{#if (eq repeat_tp 'HR')}}
			<li>
				<label for="triggeradd3" class="input-card-type">
					<input type="radio" id="triggeradd3" name="repeatSet" class="card-input-element" data-btn="Hour" data-repeat-tp="HR" trigger_id="{{trigger_id}}">
					<div class="card-input">
						<div class="card-type4">
							<span class="img-repeat-big">
								<span class="hour">
									<i></i>
								</span>
							</span>
						</div>
					</div>
					<p class="text-center">
						{{repeat_des}}
					</p>
				</label>
			</li>
			{{/if}}
			{{#if (eq repeat_tp 'DY')}}
			<li>
				<label for="triggeradd4" class="input-card-type">
					<input type="radio" id="triggeradd4" name="repeatSet" class="card-input-element" data-btn="Day" data-repeat-tp="DY" trigger_id="{{trigger_id}}">
					<div class="card-input">
						<div class="card-type4">
							<span class="img-repeat-big">
								<span class="day">
									<i></i>
								</span>
							</span>
						</div>
					</div>
					<p class="text-center">
						{{repeat_des}}
					</p>
				</label>
			</li>
			{{/if}}
			{{#if (eq repeat_tp 'WK')}}
			<li>
				<label for="triggeradd5" class="input-card-type">
					<input type="radio" id="triggeradd5" name="repeatSet" class="card-input-element" data-btn="Week" data-repeat-tp="WK" trigger_id="{{trigger_id}}">
					<div class="card-input">
						<div class="card-type4">
							<span class="img-repeat-big">
								<span class="week">
									<i></i>
								</span>
							</span>
						</div>
					</div>
					<p class="text-center">
						{{repeat_des}}
					</p>
				</label>
			</li>
			{{/if}}
			{{#if (eq repeat_tp 'MO')}}
			<li>
				<label for="triggeradd6" class="input-card-type">
					<input type="radio" id="triggeradd6" name="repeatSet" class="card-input-element" data-btn="Month" data-repeat-tp="MO" trigger_id="{{trigger_id}}">
					<div class="card-input">
						<div class="card-type4">
							<span class="img-repeat-big">
								<span class="month">
									<i></i>
								</span>
							</span>
						</div>
					</div>
					<p class="text-center">
						{{repeat_des}}
					</p>
				</label>
			</li>
			{{/if}}
			{{#if (eq repeat_tp 'YR')}}
			<li>
				<label for="triggeradd7" class="input-card-type">
					<input type="radio" id="triggeradd7" name="repeatSet" class="card-input-element" data-btn="Year" data-repeat-tp="YR" trigger_id="{{trigger_id}}">
					<div class="card-input">
						<div class="card-type4">
							<span class="img-repeat-big">
								<span class="year">
									<i></i>
								</span>
							</span>
						</div>
					</div>
					<p class="text-center">
						{{repeat_des}}
					</p>
				</label>
			</li>
			{{/if}}
		</ul>
	</div>
		{{#if (neq repeat_tp 'NO')}}
		<div class="write-sub-title">
			<b class="caution-star">*</b> 반복 종료일​
			<a href="#" class="info-icon-tooltip" data-toggle="tooltip" title="트리거 이벤트 반복이 종료되는 날짜를 설정해 주세요.​" data-placement="right">
				i
			</a>
		</div>
		<div class="form-inline">
			<div class="form-group datepicker_wrap">
				<div class="input-icon-type2 icon-left">
					<span class="input-icon-addon">
						<i class="xi-calendar"></i>
					</span>
					<input type="text" class="form-control datepicker ele" id="end_date_hms_date_{{trigger_id}}" name="end_date" disabled>
				</div>
			</div>
			<div class="form-group">
				<select class="form-control" id="end_date_hms_hour_{{trigger_id}}" name="end_date" disabled>
					{{#for 0 23}}
					<option value="{{@index}}">{{@index}}시</option>
					{{/for}}
				</select>
			</div>
			<div class="form-group">
				<select class="form-control" id="end_date_hms_miniute_{{trigger_id}}" name="end_date" disabled>
					{{#for 0 59}}
					<option value="{{@index}}">{{@index}}분</option>
					{{/for}}
				</select>
			</div>
			<div class="checkbox-group margin-left-20">
				<input type="checkbox" class="checkbox-input chk_enddate" id="chk_enddate_{{trigger_id}}" trigger_id="{{trigger_id}}" checked>
				<label for="chk_enddate_{{trigger_id}}" class="checkbox-item">
					<div class="checkbox">
						<div class="checked-icon">
							<i class="xi-check"></i>
						</div>
					</div>
					<span class="text">
						사용 안 함​
					</span>
				</label>
			</div>
		</div>
		{{/if}}
	{{else}}
	<div class="write-sub-title">
		<b class="caution-star">*</b> 반복 설정
		<span class="info-icon-tooltip" data-toggle="tooltip" title="트리거 이벤트 반복을 원할 경우, 반복 주기를 설정해 주세요.​" data-placement="right">
			i
		</span>
	</div>
	<!-- repeat -->
	<div data-lst="admin" style="width: 800px">
		<ul class="list-inline">
			<li>
				<label for="triggeradd1" class="input-card-type">
					<input type="radio" id="triggeradd1" name="repeatSet" class="card-input-element" data-btn="No_repeat" data-repeat-tp="NO" trigger_id="{{trigger_id}}">
					<div class="card-input">
						<div class="card-type4">
							<span class="img-repeat-big">
								<span class="none">
									<i></i>
								</span>
							</span>
						</div>
					</div>
					<p class="text-center">
						없음
					</p>
				</label>
			</li>
			<li>
				<label for="triggeradd2" class="input-card-type">
					<input type="radio" id="triggeradd2" name="repeatSet" class="card-input-element" data-btn="Minute" data-repeat-tp="MN" trigger_id="{{trigger_id}}">
					<div class="card-input">
						<div class="card-type4">
							<span class="img-repeat-big">
								<span class="minute">
									<i></i>
								</span>
							</span>
						</div>
					</div>
					<p class="text-center">
						분
					</p>
				</label>
			</li>
			<li>
				<label for="triggeradd3" class="input-card-type">
					<input type="radio" id="triggeradd3" name="repeatSet" class="card-input-element" data-btn="Hour" data-repeat-tp="HR" trigger_id="{{trigger_id}}">
					<div class="card-input">
						<div class="card-type4">
							<span class="img-repeat-big">
								<span class="hour">
									<i></i>
								</span>
							</span>
						</div>
					</div>
					<p class="text-center">
						시간
					</p>
				</label>
			</li>
			<li>
				<label for="triggeradd4" class="input-card-type">
					<input type="radio" id="triggeradd4" name="repeatSet" class="card-input-element" data-btn="Day" data-repeat-tp="DY" trigger_id="{{trigger_id}}">
					<div class="card-input">
						<div class="card-type4">
							<span class="img-repeat-big">
								<span class="day">
									<i></i>
								</span>
							</span>
						</div>
					</div>
					<p class="text-center">
						매일
					</p>
				</label>
			</li>
			<li>
				<label for="triggeradd5" class="input-card-type">
					<input type="radio" id="triggeradd5" name="repeatSet" class="card-input-element" data-btn="Week" data-repeat-tp="WK" trigger_id="{{trigger_id}}">
					<div class="card-input">
						<div class="card-type4">
							<span class="img-repeat-big">
								<span class="week">
									<i></i>
								</span>
							</span>
						</div>
					</div>
					<p class="text-center">
						주별
					</p>
				</label>
			</li>
			<li>
				<label for="triggeradd6" class="input-card-type">
					<input type="radio" id="triggeradd6" name="repeatSet" class="card-input-element" data-btn="Month" data-repeat-tp="MO" trigger_id="{{trigger_id}}">
					<div class="card-input">
						<div class="card-type4">
							<span class="img-repeat-big">
								<span class="month">
									<i></i>
								</span>
							</span>
						</div>
					</div>
					<p class="text-center">
						월별
					</p>
				</label>
			</li>
			<li>
				<label for="triggeradd7" class="input-card-type">
					<input type="radio" id="triggeradd7" name="repeatSet" class="card-input-element" data-btn="Year" data-repeat-tp="YR" trigger_id="{{trigger_id}}">
					<div class="card-input">
						<div class="card-type4">
							<span class="img-repeat-big">
								<span class="year">
									<i></i>
								</span>
							</span>
						</div>
					</div>
					<p class="text-center">
						연간
					</p>
				</label>
			</li>
		</ul>

		<div class="margin-20" style="display: none;">
			<div class="row" data-lst="repeatWrap" data-tab="Minute" style="display: none;">
				<div class="col-md-12">
					<div class="form-inline">
						<div class="radio-group">
							<input type="radio" name="tabs4_{{trigger_id}}" id="tab4_{{trigger_id}}" class="radio-input" checked>
							<label for="tab4_{{trigger_id}}" class="radio-item">
								<div class="radio">
									<div class="checked-icon"></div>
								</div>
								<span class="text">
									매
								</span>
							</label>
						</div>
						<div class="form-group">
							<select class="form-control mn-value">
								{{#for 1 59}}
								<option value="{{@index}}">{{@index}}분</option>
								{{/for}}
							</select>
							<span class="text">분 마다 반복​​</span>
						</div>
					</div>
				</div>
			</div>

			<div class="row"  data-lst="repeatWrap" data-tab="Hour" style="display: none;">
				<div class="col-md-12">
					<div class="form-inline">
						<div class="radio-group">
							<input type="radio" name="tabs5_{{trigger_id}}" id="tab5_{{trigger_id}}" class="radio-input" checked>
							<label for="tab5_{{trigger_id}}" class="radio-item">
								<div class="radio">
									<div class="checked-icon"></div>
								</div>
								<span class="text">
									매
								</span>
							</label>
						</div>
						<div class="form-group">
							<select class="form-control hr-value">
								{{#for 1 23}}
								<option value="{{@index}}">{{@index}}시간</option>
								{{/for}}
							</select>
							<span class="text">시간 마다 반복​​​</span>
						</div>
					</div>
				</div>
			</div>

			<div class="row" data-lst="repeatWrap" data-tab="Day" style="display: none;">
				<div class="col-md-5">
					<div class="form-inline">
						<div class="radio-group">
							<input type="radio" name="tab1_{{trigger_id}}" id="tab1_1_{{trigger_id}}" class="radio-input" checked>
							<label for="tab1_1_{{trigger_id}}" class="radio-item">
								<div class="radio">
									<div class="checked-icon"></div>
								</div>
								<span class="text">
									매
								</span>
							</label>
						</div>
						<div class="form-group">
							<select class="form-control dy-day-value">
								{{#for 1 31}}
								<option value="{{@index}}">{{@index}}일</option>
								{{/for}}
							</select>
							<span class="text">일 마다 반복​​​</span>
						</div>
					</div>
				</div>
				<div class="col-md-7">
					<div class="form-inline">
						<div class="radio-group padding-top-10">
							<input type="radio" name="tab1_{{trigger_id}}" id="tab1_2_{{trigger_id}}" class="radio-input">
							<label for="tab1_2_{{trigger_id}}" class="radio-item">
								<div class="radio">
									<div class="checked-icon"></div>
								</div>
								<span class="text">
									평일​
								</span>
							</label>
						</div>
					</div>
				</div>
			</div>

			<div class="row" data-lst="repeatWrap" data-tab="Week" style="display: none;">
				<div class="col-md-12">
					<div class="form-inline margin-top-10">
						<div class="checkbox-group margin-right-20">
							<input type="checkbox" class="checkbox-input" id="chk_1_1_{{trigger_id}}" data-code="MON" checked>
							<label for="chk_1_1_{{trigger_id}}" class="checkbox-item">
								<div class="checkbox">
									<div class="checked-icon">
										<i class="xi-check"></i>
									</div>
								</div>
								<span class="text">
									월요일
								</span>
							</label>
						</div>
						<div class="checkbox-group margin-right-20">
							<input type="checkbox" class="checkbox-input" id="chk_1_2_{{trigger_id}}" data-code="TUE"  checked>
							<label for="tuesday" class="checkbox-item">
								<div class="checkbox">
									<div class="checked-icon">
										<i class="xi-check"></i>
									</div>
								</div>
								<span class="text">
									화요일
								</span>
							</label>
						</div>
						<div class="checkbox-group margin-right-20">
							<input type="checkbox" class="checkbox-input" id="chk_1_3_{{trigger_id}}" data-code="WED"  checked>
							<label for="wednesday" class="checkbox-item">
								<div class="checkbox">
									<div class="checked-icon">
										<i class="xi-check"></i>
									</div>
								</div>
								<span class="text">
									수요일
								</span>
							</label>
						</div>
						<div class="checkbox-group margin-right-20">
							<input type="checkbox" class="checkbox-input" id="chk_1_4_{{trigger_id}}" data-code="THU"  checked>
							<label for="thursday" class="checkbox-item">
								<div class="checkbox">
									<div class="checked-icon">
										<i class="xi-check"></i>
									</div>
								</div>
								<span class="text">
									목요일
								</span>
							</label>
						</div>
						<div class="checkbox-group margin-right-20">
							<input type="checkbox" class="checkbox-input" id="chk_1_5_{{trigger_id}}" data-code="FRI"  checked>
							<label for="friday" class="checkbox-item">
								<div class="checkbox">
									<div class="checked-icon">
										<i class="xi-check"></i>
									</div>
								</div>
								<span class="text">
									금요일
								</span>
							</label>
						</div>
						<div class="checkbox-group margin-right-20">
							<input type="checkbox" class="checkbox-input" id="chk_1_6_{{trigger_id}}" data-code="SAT">
							<label for="saturday" class="checkbox-item">
								<div class="checkbox">
									<div class="checked-icon">
										<i class="xi-check"></i>
									</div>
								</div>
								<span class="text">
									토요일
								</span>
							</label>
						</div>
						<div class="checkbox-group margin-right-20">
							<input type="checkbox" class="checkbox-input" id="chk_1_7_{{trigger_id}}" data-code="SUN">
							<label for="sunday" class="checkbox-item">
								<div class="checkbox">
									<div class="checked-icon">
										<i class="xi-check"></i>
									</div>
								</div>
								<span class="text">
									일요일
								</span>
							</label>
						</div>
					</div>
				</div>
			</div>

			<div class="row" data-lst="repeatWrap" data-tab="Month" style="display: none;">
				<div class="col-md-5">
					<div class="form-inline">
						<div class="radio-group">
							<input type="radio" name="tabs2_{{trigger_id}}" id="tab2_1_{{trigger_id}}" class="radio-input" checked>
							<label for="tab2_1_{{trigger_id}}" class="radio-item">
								<div class="radio">
									<div class="checked-icon"></div>
								</div>
								<span class="text">
									매월​
								</span>
							</label>
						</div>
						<div class="form-group">
							<select class="form-control mo-day-value">
								{{#for 1 31}}
								<option value="{{@index}}">{{@index}}일</option>
								{{/for}}
							</select>
							<span class="text">일 마다 반복​</span>
						</div>
					</div>
				</div>
				<div class="col-md-7">
					<div class="form-inline">
						<div class="radio-group">
							<input type="radio"  name="tabs2_{{trigger_id}}" id="tab2_2_{{trigger_id}}" class="radio-input">
							<label for="tab2_2_{{trigger_id}}" class="radio-item">
								<div class="radio">
									<div class="checked-icon"></div>
								</div>
							</label>
						</div>
						<div class="form-group">
							<select class="form-control mo-week-value-th">
								<option value="1" selected>첫 번째</option>
								<option value="2">두 번째</option>
								<option value="3">세 번째</option>
								<option value="4">네 번째</option>
								<option value="5">다섯 번째</option>
							</select>
						</div>
						<div class="form-group">
							<select class="form-control mo-week-value-week">
								<option value="MON" selected>월요일</option>
								<option value="TUE">화요일</option>
								<option value="WED">수요일</option>
								<option value="THU">목요일</option>
								<option value="FRI">금요일</option>
								<option value="SAT">토요일</option>
								<option value="SUN">일요일</option>
							</select>
							<span class="text">마다 반복​</span>
						</div>
					</div>
				</div>
			</div>

			<div class="row" data-lst="repeatWrap" data-tab="Year" style="display: none;">
				<div class="col-md-5">
					<div class="form-inline">
						<div class="radio-group">
							<input type="radio" name="tabs3_{{trigger_id}}" id="tab3_1_{{trigger_id}}" class="radio-input" checked>
							<label for="tab3_1_{{trigger_id}}" class="radio-item">
								<div class="radio">
									<div class="checked-icon"></div>
								</div>
								<span class="text">
									매년​​
								</span>
							</label>
						</div>
						<div class="form-group">
							<select class="form-control yr-month-value">
								{{#for 1 12}}
								<option value="{{@index}}">{{@index}}월</option>
								{{/for}}
							</select>
							<span class="text">월 마다 반복</span>
						</div>
					</div>
				</div>
				<div class="col-md-7">
					<div class="form-inline">
						<div class="radio-group">
							<input type="radio" name="tabs3_{{trigger_id}}" id="tab3_2_{{trigger_id}}" class="radio-input">
							<label for="tab3_2_{{trigger_id}}" class="radio-item">
								<div class="radio">
									<div class="checked-icon"></div>
								</div>
							</label>
						</div>
						<div class="form-group">
							<select class="form-control yr-week-value-th">
								<option value="1" selected>첫 번째</option>
								<option value="2">두 번째</option>
								<option value="3">세 번째</option>
								<option value="4">네 번째</option>
								<option value="5">다섯 번째</option>
							</select>
						</div>
						<div class="form-group">
							<select class="form-control yr-week-value-week">
								<option value="MON" selected>월요일</option>
								<option value="TUE">화요일</option>
								<option value="WED">수요일</option>
								<option value="THU">목요일</option>
								<option value="FRI">금요일</option>
								<option value="SAT">토요일</option>
								<option value="SUN">일요일</option>
							</select>
							<span class="text">마다 반복​</span>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>

	<div class="write-sub-title">
		<b class="caution-star">*</b> 반복 종료일​
		<a href="#" class="info-icon-tooltip" data-toggle="tooltip" title="트리거 이벤트 반복이 종료되는 날짜를 설정해 주세요.​" data-placement="right">
			i
		</a>
	</div>
	<div class="form-inline">
		<div class="form-group datepicker_wrap">
			<div class="input-icon-type2 icon-left">
				<span class="input-icon-addon">
					<i class="xi-calendar"></i>
				</span>
				<input type="text" class="form-control datepicker ele" id="end_date_hms_date_{{trigger_id}}" name="end_date" disabled>
			</div>
		</div>
		<div class="form-group">
			<select class="form-control" id="end_date_hms_hour_{{trigger_id}}" name="end_date" disabled>
				{{#for 0 23}}
				<option value="{{@index}}">{{@index}}시</option>
				{{/for}}
			</select>
		</div>
		<div class="form-group">
			<select class="form-control" id="end_date_hms_miniute_{{trigger_id}}" name="end_date" disabled>
				{{#for 0 59}}
				<option value="{{@index}}">{{@index}}분</option>
				{{/for}}
			</select>
		</div>
		<div class="checkbox-group margin-left-20">
			<input type="checkbox" class="checkbox-input chk_enddate" id="chk_enddate_{{trigger_id}}" trigger_id="{{trigger_id}}" checked>
			<label for="chk_enddate_{{trigger_id}}" class="checkbox-item">
				<div class="checkbox">
					<div class="checked-icon">
						<i class="xi-check"></i>
					</div>
				</div>
				<span class="text">
					사용 안 함​
				</span>
			</label>
		</div>
	</div>
	{{/if}}
</div>
{{/each}}

<div class="panel-body" style="display:none;">
<script>
	function triggerTimerEvent()
	{
		// 반복설정
		$('[data-btn]').click(function() {
			var trigger_id = $(this).attr('trigger_id');
			var thisObj = $('[timer_trigger_id='+trigger_id+']');
			var section = $(this).attr('data-btn');
			
			if($(this).hasClass('on')) {
					return false;
			} else {
				$(this).closest('.repeatClick').find('[data-btn]').removeClass('on');
				$(this).addClass('on')

				thisObj.find('[data-lst="repeatWrap"]').hide();
				thisObj.find('[data-tab='+ section +']').show();
				thisObj.find('.form_wrap2').show();
				if(section == "No_repeat")
					thisObj.find('.form_wrap2').hide();
			};
		});
		
		$('.chk_enddate').click( function() {
			var trigger_id = $(this).attr('trigger_id');
			var thisObj = $('[timer_trigger_id='+trigger_id+']');
			
			if($(this).is(':checked')){
				thisObj.find('[name="end_date"]').prop('disabled', true);
			} else {
				thisObj.find('[name="end_date"]').prop('disabled', false);
			}
		});
		
		$('.datepicker').click(function(){
			$(this).removeClass("false");
		});
		
		$('.datepicker').daterangepicker({
			singleDatePicker: true,
			 locale: {
		            format: 'YYYY-MM-DD'
		         }
		});
		$('.datepicker').attr("readonly", true);
		$('[data-toggle="tooltip"]').tooltip();
	}
	
	function triggerTimerValidation(trigger_id,data)
	{
		var thisObj = $('[timer_trigger_id="'+trigger_id+'"]');
		function isEmpty(selector)
		{
			var obj = thisObj.find(selector);
			obj.each(function(i,item){
				if($(item).val() == "")
				{
					data.validation = false;
					$(item).addClass('false');
					return false;
				}
			});
		}
		
		if(isEmpty("#bas_date_hms_date_"+trigger_id)) return;
		if(thisObj.find('.repeat-tp .on').attr('data-btn') != "No_repeat")
		{
			if( thisObj.find('#chk_enddate_'+trigger_id).is(':checked') == false)
				if(isEmpty("#end_date_hms_date_"+trigger_id)) return;
		}
	}
	
	function triggerTimerData(trigger_id,adm_set_yn)
	{
		var data = {};
		data.trigger_id = trigger_id;
		var thisObj = $('[timer_trigger_id="'+trigger_id+'"]');
		function getValue(selector)
		{
			if(selector.indexOf('#')==0)
				return thisObj.find(selector+'_'+trigger_id).val();
			else
				return thisObj.find(selector).val();
		}
		function dateFormat(date,hour,miniute)
		{
			var val = date.replace(/[.]/g,'-') + ' ' + prefixZero(hour) + ':' + prefixZero(miniute) + ':00';
			return val;
		}
		function prefixZero(val) {
			if(val.length==1) val = '0'+val;
			return val;
		}
		function setRepeatValue(tp)
		{
			var repeat = {};
			var repeat_des;
			switch(tp)
			{
			case 'MN': 
				repeat.value=getValue('.mn-value'); 
				repeat_des = "매 " + repeat.value + "분 마다 반복";
				break;
			case 'HR':
				repeat.value = getValue('.hr-value');
				repeat_des = "매 " + repeat.value + "시간 마다 반복";
				break;
			case 'DY':
				repeat.radio = thisObj.find('.dy-radio:checked').data('code');
				if(repeat.radio == 'day') {
					repeat.value = getValue('.dy-day-value');
					repeat_des = "매 " + repeat.value + "일 마다 반복";
				} else if(repeat.radio == 'week') {
					repeat_des = "평일 마다 반복";
				}
				break;
			case 'WK':
				repeat.value = thisObj.find('.wk-value:checked').map(function(){ return $(this).data('code') }).get().join();
				var wk_value_txt = thisObj.find('.wk-value:checked').map(function(){ return $(this).next().text() }).get().join();
				repeat_des = wk_value_txt + " 마다 반복";
				repeat_des = repeat_des.replace(/,/gi, ", ");
				break;
			case 'MO':
				repeat.radio = thisObj.find('.mo-radio:checked').data('code');
				if(repeat.radio == 'day'){
					repeat.value = thisObj.find('.mo-day-value').val();
					repeat_des = "매월 " + repeat.value + "일 마다 반복";
				}else if(repeat.radio == 'week'){
					repeat.value_th = thisObj.find('.mo-week-value-th').val();
					repeat.value_week = thisObj.find('.mo-week-value-week').val();
					var value_th_txt = thisObj.find(".mo-week-value-th option:checked").text();
					var value_week_txt = thisObj.find(".mo-week-value-week option:checked").text();
					repeat_des = value_th_txt + " " + value_week_txt + " 마다 반복";
				}
				break;
			case 'YR':
				repeat.radio = thisObj.find('.yr-radio:checked').data('code');
				if(repeat.radio == 'month'){
					repeat.value = thisObj.find('.yr-month-value').val();
					repeat_des = "매년 " + repeat.value + "월 마다 반복";
				}else if(repeat.radio == 'week'){
					repeat.value_th = thisObj.find('.yr-week-value-th').val();
					repeat.value_week = thisObj.find('.yr-week-value-week').val();
					var value_th_txt = thisObj.find(".yr-week-value-th option:checked").text();
					var value_week_txt = thisObj.find(".yr-week-value-week option:checked").text();
					repeat_des = value_th_txt + " " + value_week_txt + " 마다 반복";
				}
				break;
			case 'NO':
				repeat_des = "반복 없음";
			}
			data.repeat_val = repeat;
			data.repeat_des = repeat_des;
		}
		
		
		data.repeat_val = "{}";
		data.repeat_des = "";
		data.bas_date_hms = dateFormat(getValue('#bas_date_hms_date'),getValue('#bas_date_hms_hour'),getValue('#bas_date_hms_miniute'));
		data.repeat_tp = thisObj.find('.repeat-tp.on').data('repeat-tp');
		if(data.repeat_tp != "NO" && adm_set_yn != "Y")
		{
			data.end_use_yn = thisObj.find('#chk_enddate_'+trigger_id).is(':checked')==false?"Y":"N";
			data.end_date_hms = data.end_use_yn=="Y"? dateFormat(getValue('#end_date_hms_date'),getValue('#end_date_hms_hour'),getValue('#end_date_hms_miniute')) : "2999-01-01 00:00:00";
			setRepeatValue(data.repeat_tp);
		}
		else
		{
			data.end_use_yn = "N";
			data.end_date_hms = "2999-01-01 00:00:00";
		}
		
		return data;
	}
</script>
</div>
