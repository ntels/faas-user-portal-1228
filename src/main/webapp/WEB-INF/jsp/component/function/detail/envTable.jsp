<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
    
											<table class="table table-outline text-center">
												<colgroup>
													<col span="2">
												</colgroup>
												<thead class="bg-blue-grey-100">
													<tr>
														<th scope="col">키</th>
														<th scope="col">값​</th>
													</tr>
												</thead>
												<tbody>
													{{#each config.env}}
													<tr>
														<td>{{this.name}}</td>
														<td>{{this.value}}</td>
													</tr>
													{{/each}}								
												</tbody>
											</table>
													