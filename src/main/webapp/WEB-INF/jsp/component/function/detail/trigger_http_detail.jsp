<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
    				<!-- HTTP 상세 -->
					<div  data-lst="trigger" data-tab="HTTP" style="display: block;">
						<section class="panel panel-bordered">
							<div class="panel-heading">
								<h3 class="panel-title">
									HTTP
								</h3>
							</div> <!-- // panel-heading -->
							<div class="panel-body">
								<div class="row padding-left-10">
									<div class="col-md-1">
										<div class="write-sub-title  blt-dot">
											API
										</div>
									</div>
									<div class="col-md-11">
										<div class="grey-600 font-size-13">
											{{api_name}}
										</div>
									</div>
								</div>
								<div class="row padding-left-10">
									<div class="col-md-1">
										<div class="write-sub-title  blt-dot">
											앤드포인트
										</div>
									</div>
									<div class="col-md-11">
										<div class="grey-600 font-size-13">
											<span>
												<a href="javascript:void(0)" title="클립보드에 복사" target="_blank" data-role='btn' data-toggle='modal' data-target='#modalCopy' onclick="copyClipBoard('con_url_{{trigger_id}}')">
													<div id="con_url_{{trigger_id}}" style="display:none;">{{con_url}}</div>
													{{con_url}}
													<span class="feather-icon">
														<i data-feather="copy"></i>
													</span>
													
												</a>
											</span>
										</div>
									</div>
								</div>
								<div class="row padding-left-10">
									<div class="col-md-1">
										<div class="write-sub-title  blt-dot">
											API 키
										</div>
									</div>
									<div class="col-md-11">
										<div class="grey-600 font-size-13">
											<span>
												<a href="javascript:void(0)" title="클립보드에 복사" target="_blank" data-role='btn' data-toggle='modal' data-target='#modalCopy' onclick="copyClipBoard('api_key_{{trigger_id}}')">
													<div id="api_key_{{trigger_id}}" style="display:none;">{{api_key}}</div>
													{{api_key}}
													<span class="feather-icon">
														<i data-feather="copy"></i>
													</span>
												</a>
											</span>
										</div>
									</div>
								</div>
							</div> <!-- // panel-body -->
						</section>
					</div>

				