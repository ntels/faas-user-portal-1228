<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
    
				<div class="card card-type2">
					<div class="card-header">
						<div class="card-info-area">
							<div class="text-box">
								<div class="top">
									<div class="row">
										<div class="col-xs-10">
											<b>
												{{funObj.dsp_name}}
											</b>
										</div>
										<div class="col-xs-2">
											<div class="dropdown pull-right">
												<button type="button" class="btn btn-xs btn-nostyle dropdown-toggle" data-toggle="dropdown">
													<div class="feather-icon">
														<i data-feather="more-horizontal"></i>
													</div>
												</button>
												<ul class="dropdown-menu" role="menu">
												{{#if (eq funObj.stat_cd 'DISABLE')}}
													<li>
														<a href="javascript:void(0);" id="enableBtn">
															<i data-feather="play-circle" class="dropdown-icon"></i>
															활성
														</a>
													</li>
													<li class="divider"></li>
												{{/if}}
												{{#if (eq funObj.stat_cd 'NORMAL')}}
													<li>
														<a href="javascript:void(0);" id="disableBtn">
															<i data-feather="stop-circle" class="dropdown-icon"></i>
															비활성​
														</a>
													</li>
													<li class="divider"></li>
												{{/if}}
													<li>
														<a href="javascript:void(0);" id="removeBtn">
															<i data-feather="trash-2" class="dropdown-icon"></i>
															삭제
														</a>
													</li>
												</ul>
											</div>	
										</div>
									</div>	
									<div class="row">
										<div class="col-md-6">
										{{#if (eq funObj.runtime 'nodejs')}}
											<span class="lagn">
												<span class="img-size15 img-nodejs"></span>
												{{funObj.runtime}} {{funObj.version}}
											</span>
										{{else}}
											<span class="lagn">
												<span class="img-size15 img-python"></span>
												{{funObj.runtime}} {{funObj.version}}
											</span>
										{{/if}}
										</div>
										<div class="col-md-6">
											<div class="text-right">
												{{#if (eq funObj.stat_cd 'INIT')}}
												<span class="state"><div class="state-round ready"></div>미배포</span>
												{{else if (eq funObj.stat_cd 'REG')}}
												<span class="state"><div class="state-round reg"></div>등록중</span>
												{{else if (eq funObj.stat_cd 'NORMAL')}}
												<span class="state"><div class="state-round normal"></div>배포 완료</span>
												{{else if (eq funObj.stat_cd 'ERROR')}}
												<span class="state"><div class="state-round error"></div>배포 실패</span>
												{{else if (eq funObj.stat_cd 'SYNC_ERROR')}}
												<span class="state"><div class="state-round error"></div>배포 정보 체크</span>
												{{else if (eq funObj.stat_cd 'DISABLE')}}
												<span class="state"><div class="state-round inactive"></div>비활성</span>
												{{else}}
												<span class="state"><div class="state-round loading"></div>unknown</span>
												{{/if}}
												<span class="left-bar">
													<b>등록 일시</b>
													<span>{{funObj.reg_date_hms}}​</span>
												</span>
												<span class="left-bar">
													<b>등록자</b>
													<span>{{funObj.reg_usr}}​</span>
												</span>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
					<div class="card-body">
						<div class="ellipsis">{{funObj.des}}​</div>
					</div>
				</div>
				
				<script>
function infoAfterRender()
{
	$("#more").on("click",function() {
		if($(this).text() == '더 보기') {
			$(this).prev('.txt').addClass('auto');
			$(this).text('접기');	
		} else {
			$(this).prev('.txt').removeClass('auto');
			$(this).text('더 보기');
		};
	});
	
	//활성
	$("#enableBtn").on("click",function() {
		fnModalConfirm("함수를 활성화 하시겠습니까?" , "알림" , function()
		{
			JsonCall('service/enableFunction.json', 'param=' + JSON.stringify({function_id:"${paramMap.function_id}"}), function(response)
			{
				ret = JSON.parse(response.responseText);
				if(ret.success != undefined)
				{
					if(ret.success == "N")
						fnModalAlert("작업을 실패했습니다.","알림");
					else
						NtelsHB.func.dataLoad();
				}
				else
					fnModalAlert("작업을 정상적으로 처리하지 못했습니다.","에러");					
			});
		});
	});
	
	//비활성
	$("#disableBtn").on("click",function() {
		fnModalConfirm("함수를 비활성화  하시겠습니까?" , "알림" , function()
		{
			JsonCall('service/disableFunction.json', 'param=' + JSON.stringify({function_id:"${paramMap.function_id}"}), function(response)
			{
				ret = JSON.parse(response.responseText);
				if(ret.success != undefined)
				{
					if(ret.success == "N")
						fnModalAlert("작업을 실패했습니다.","알림");
					else
						NtelsHB.func.dataLoad();
				}
				else
					fnModalAlert("작업을 정상적으로 처리하지 못했습니다.","에러");					
			});
		});
	});
	
	//삭제
	$("#removeBtn").on("click",function() {
		fnModalConfirm("삭제하시겠습니까?" , "삭제" , function()
		{
			JsonCall('service/deleteFunction.json', 'param=' + JSON.stringify({function_id:"${paramMap.function_id}"}), function(response)
			{
				ret = JSON.parse(response.responseText);
				if(ret.success != undefined)
				{
					if(ret.success == "N")
						fnModalAlert("삭제를 실패했습니다.","알림");
					else
						loadPage('layout/function/list.do');
				}
				else
					fnModalAlert("작업을 정상적으로 처리하지 못했습니다.","에러");					
			});
		});
	});	
	
}
</script>
