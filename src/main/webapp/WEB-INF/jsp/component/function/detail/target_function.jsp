<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
    
<script>
function actionBackend(function_id){
	
	$('[name=target_result]').on('click',function(){
		$('[name=target_result]').removeClass('on');
		$(this).addClass('on');
		
		var target_result_cd = $(this).attr("target_result_cd");
		
		$('select[name=targetFunctionList]').each(function() {
			if($(this).attr("target_result_cd") == target_result_cd)
				$(this).show();
			else
				$(this).hide();
		});
	});
	
	$('#backend_confirm_btn').on('click',function(e){
		var param = {
			function_id: function_id,
			target_result_cd: '',
			target_function_id: ''
		};
		
		function validate()
		{
			debugger;
			param.target_result_cd = $('[name=target_result].on').attr('target_result_cd');
			
			if(param.target_result_cd)
				param.target_function_id = $('#targetFunctionList_' +  param.target_result_cd).val();
			
			if(param.target_result_cd == undefined || param.target_result_cd == "")
			{
				fnModalAlert("조건을 선택하세요.","알림");
				return false;
			}
			if(param.target_function_id == undefined || param.target_function_id == "")
			{
				fnModalAlert("함수를 선택하세요.","알림");
				return false;
			}
			return true;
		}
		
		if(validate())
		{			
			JsonCall('${pageContext.request.contextPath}/service/regTarget.json', 'param=' + JSON.stringify(param), function(response) {
				var ret = JSON.parse(response.responseText);
				if(ret.success == "Y")
				{
					NtelsHB.func.dataLoad(true);
				}
				else
				{
					fnModalAlert("대상 등록을 실패했습니다.","에러");
					e.stopPropagation();
				}
			});
		}
		else
			e.stopPropagation();
	});
}

</script>

				<div class="modal" id="dialogBackend">
					<div class="modal-dialog">
						<div class="modal-content">
							<div class="modal-header">
								<button type="button" class="close" data-dismiss="modal" aria-label="Close">
									<span aria-hidden="true">×</span>
								</button>
								<h4 class="modal-title">대상 추가​​​</h4>
							</div>
							<div class="modal-body">
								
								<div class="text-right add-txt">
									<span><b class="caution-star">*</b> 는 필수입력 사항입니다.</span>
								</div>
								
								<div class="write-sub-title">
									<b class="caution-star">*</b> 조건
									<span class="info-icon-tooltip" data-toggle="tooltip" title="함수 실행  결과가 조건과 일치 할 때 백엔드 서비스가 동작합니다.​​​​​" data-placement="right">
										i
									</span>
								</div>
								
								<ul class="list-inline margin-bottom-30">
									<li>
										<label for="success" class="input-card-type">
											<input type="radio" id="success" target_result_cd="success" name="target_result" class="card-input-element on" checked>
											<div class="card-input">
												<div class="card-type4">
													<div class="img-state img-success img-size57 margin-10"></div>
												</div>
											</div>
											<p class="text-center">
												성공
											</p>
										</label>
									</li>
									<li>
										<label for="fail" class="input-card-type">
											<input type="radio" id="fail" target_result_cd="fail" name="target_result" class="card-input-element">
											<div class="card-input">
												<div class="card-type4">
													<div class="img-state img-fail img-size57 margin-10"></div>
												</div>
											</div>
											<p class="text-center">
												실패​
											</p>
										</label>
									</li>
								</ul>

								<div class="write-sub-title">
									<b class="caution-star">*</b> 함수
									<span class="info-icon-tooltip" data-toggle="tooltip" title="조건에 만족할 경우, 실행할 함수를 선택해 주세요.​​​" data-placement="right">
										i
									</span>
								</div>
								<div class="form-group">
									<select name="targetFunctionList" id="targetFunctionList_success" target_result_cd="success" class="form-control">
										<option value="">함수선택</option>
										{{#each candidateList.onSuccess}}
										<option value="{{function_id}}">{{dsp_name}}</option>
										{{/each}}
									</select>
									<select name="targetFunctionList" id="targetFunctionList_fail" target_result_cd="fail" class="form-control" style="display: none;">
										<option value="">함수선택</option>
										{{#each candidateList.onFailure}}
										<option value="{{function_id}}">{{dsp_name}}</option>
										{{/each}}
									</select>
								</div>

							</div>
							<div class="modal-footer">
								<button type="button" id="backend_confirm_btn" class="btn btn-primary" data-dismiss="modal">추가​</button>
								<button type="button" class="btn btn-default" data-dismiss="modal">취소​</button>
							</div>
						</div>
					</div>
				</div>