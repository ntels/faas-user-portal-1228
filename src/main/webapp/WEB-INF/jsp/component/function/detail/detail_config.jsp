<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
    
<script>
function actionConfigModal() {
	
	debugger;
	
	var config = NtelsHB.Store.get("config");
	function eventInit() //초기값 설정 , select box , 환경변수 table
	{
		function envAddRow(name,value)
		{
			var temp = $('#env_row_template');
			if(temp.length == 1)
			{
				var tr = temp.clone();
				tr.attr('id','');
				$('#envTable').find('#env_table_tbody').append(tr);
				if(name != undefined && name != "")
				{
					var inputs = tr.find('input');
					if(inputs.length == 2)
					{
						$(inputs[0]).val(name);
						$(inputs[1]).val(value);
					}
				}
			}
		}
		
		//환경변수 row 추가
		$('#env_append_row_btn').on('click',function()
		{
			envAddRow('','');
		});
		
		//환경변수 row 삭제
		$('#dialogEnv').on('click','[item-del=envrow]',function()
		{
			$(this).closest('tr').remove();
		});
		
		// memory, cpu slide처리
		(function(){
			var memory = "#memory";
			var cpu = "#cpu";
			
			$(memory).slider({tooltip: 'always'});
			$(cpu).slider({tooltip: 'always'});
			
			$(memory).on('slide',function(e)
			{
				$('#config_resource_mem').text(e.value)
			});
			
			$(cpu).on('slide',function(e)
			{
				$('#config_resource_cpu').text(e.value)
			});
		})();
		
		$("#config_traffic_quota_types").val(config.trf.quota.type).prop("selected",true);
		if(config.env != undefined && config.env.length == 0)
		{
			envAddRow("","");
		}
		if(config.env != undefined && config.env.length > 0)
		{
			for(var ia=0;ia<config.env.length;ia++)
			{
				envAddRow(config.env[ia].name,config.env[ia].value);				
			}
		}
		
		$("#memory").slider({tooltip: 'always'});
		$("#cpu").slider({tooltip: 'always'});
		
		// 함수 설정 모달 - 설정 버튼 클릭 시
		$('a[name=show_config_modal]').on('click', function() {
			var modal_id = $(this).attr('modal_id');
			$(modal_id).modal({backdrop: 'static',keyboard: true});
		});
		
		// 함수 설정 모달 - 취소 버튼 클릭 시
		$('button[name=config_cancle_btn]').on('click', function() {
			setTimeout(function(){NtelsHB.render();},500);
		});
		
		// 숫자만 입력가능
		$("input:text[numberOnly]").on("keyup", function() {
		    $(this).val($(this).val().replace(/[^0-9]/g,""));
		});
	}
	eventInit();
	
	// 환경변수
	$("button[name=config_confirm_btn][config_type=env]").on('click' , function(e)
	{
		var env = [];
		function showError()
		{
			var obj = $("#config_env_err");
			obj.show();
		}
		
		function validate()
		{
			var trs = $("#envTable").find("tr");
			//$("#envTable").find("input.inp").removeClass("false");
			for(var ib=0;ib<trs.length;ib++)
			{
				var inputs = $(trs[ib]).find("input");
				if(inputs.length == 2)
				{
					var name = $(inputs[0]).val();
					var value = $(inputs[1]).val();
					var item = {};
					
					if(name == undefined || name == "")
					{
						$(inputs[0]).addClass("false");
						showError();
						return false;
					}
					if(value == undefined || value == "")
					{
						$(inputs[1]).addClass("false");
						showError();
						return false;
					}
					
					item.name = name;
					item.value = value;
					env.push(item);
				}
			}
			return true;
		}
		
		if(validate())
		{
			config.env = env;
			config.envLength = env.length;
			setTimeout(function(){NtelsHB.render();},500);
		}
		else
			e.stopPropagation();
		
	});
	
	// 트래픽
	$("button[name=config_confirm_btn][config_type=traffic]").on('click' , function(e)
	{
		function showError(msg)
		{
			var obj = $("#config_traffic_err");
			obj.text(msg);
			obj.show();
		}
		
		function validate()
		{
			var msg = "";
			$("input.inp").removeClass("false");
			if(isNum("#config_traffic_burst_val",1,1000) == false)
			{
				$("#config_traffic_burst_val").addClass('false');
				msg = "1 이상 1,000이하 입력해 주세요.";
				showError(msg);
				return false;
			}
			if(isNum("#config_traffic_quota_val",1,10000000) == false)
			{
				$("#config_traffic_quota_val").addClass('false');
				msg = "1 이상 입력해 주세요.";
				showError(msg);
				return false;
			}
			if(isNum("#config_traffic_quota_cnt",1,100000) == false)
			{
				$("#config_traffic_quota_cnt").addClass('false');
				msg = "1 이상 1,100000이하 입력해 주세요.";
				showError(msg);
				return false;
			}
			
			return true;
		}
		
		if(validate())
		{
			config.trf.burst = parseInt($("#config_traffic_burst_val").val());
			config.trf.quota.type = $("#config_traffic_quota_types option:selected").val();
			config.trf.quota.typenm = $("#config_traffic_quota_types option:selected").attr("typenm");
			config.trf.quota.val = parseInt($("#config_traffic_quota_val").val());
			config.trf.quota.cnt = parseInt($("#config_traffic_quota_cnt").val());
			setTimeout(function(){NtelsHB.render();},500);	
		}
		else
			e.stopPropagation();
	});
	
	// 기본설정
	$("button[name=config_confirm_btn][config_type=resource]").on('click' , function(e)
	{
		function showError(msg)
		{
			var obj = $("#config_resource_err");
			obj.text(msg);
			obj.show();
		}
		
		function validate()
		{
			var msg = "";
			var min = $("#config_resource_timeout_min").val();
			var sec = $("#config_resource_timeout_sec").val();
			
			if(min == "") $("#config_resource_timeout_min").val("0");
			if(sec == "") $("#config_resource_timeout_sec").val("0");
			
			var imin = (min == "") ? 0 : parseInt(min);
			var isec = (sec == "") ? 0 : parseInt(sec);
			var itot = imin*60 + isec;

			$("input.inp").removeClass("false");
			if( itot <= 0)
			{
				$("#config_resource_timeout_sec,#config_resource_timeout_min").addClass('false');
				msg = "1초 이상 입력해 주세요.";
				showError(msg);
				return false;
			}
			
			if( itot > 900)
			{
				$("#config_resource_timeout_sec,#config_resource_timeout_min").addClass('false');
				msg = "15분 이내로 입력해 주세요.";
				showError(msg);
				return false;
			}
			
			if(itot >= 60)
			{
				$("#config_resource_timeout_min").val(Math.floor(itot/60)+"");
				$("#config_resource_timeout_sec").val((itot%60)+"");
			}
			
			return true;
		}
		
		if(validate())
		{
			config.timeout.min = parseInt($("#config_resource_timeout_min").val());
			config.timeout.sec = parseInt($("#config_resource_timeout_sec").val());
			config.mem = parseInt($("#config_resource_mem").text());
			config.cpu = parseInt($("#config_resource_cpu").text());
			setTimeout(function(){NtelsHB.render();},500);	
		}
		else
			e.stopPropagation();
	});
	
	// 동시성
	$("button[name=config_confirm_btn][config_type=concur]").on('click' , function(e)
	{
		function validate()
		{
			if(isNum("#config_concur_concur", 1 , 100) == false)
			{
				$("#config_concur_concur").addClass('false');
				$("#config_concur_err").show();
				return false;
			}
			return true;
		}
		
		if(validate())
		{
			config.concur = parseInt($("#config_concur_concur").val());
			setTimeout(function(){NtelsHB.render();},500);	
		}
		else
			e.stopPropagation();
	});
	
	function isNum(selector,min,max)
	{
		var regex = /^[0-9]*$/g;
		var ival = 0;
		var val = $(selector).val();
		if(val == undefined || val == null || val == "" )
			return false;
		if(regex.test(val) == false) 
			return false
		ival = parseInt(val);
		if(ival >= min && ival <= max)
			return true; 
		else
			return false;				
	}	
}
</script>

<!-- 1. 환경변수 모달 -->
<div class="modal in" id="dialogEnv" style="display: none;">
	<div class="modal-dialog modal-lg">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">×</span>
				</button>
				<h4 class="modal-title">환경 변수​</h4>
			</div>
			<div class="modal-body">
				<div class="row">
					<div class="col-xs-8">
						<div class="write-sub-title">
							<b class="caution-star">*</b>환경변수
							<a href="#" class="info-icon-tooltip" data-toggle="tooltip" title="" data-placement="right" data-original-title="함수 코드에서 액세스할 수 있는 키/값을 설정하면, 함수 코드 변경 없이 간편하게 이용할 수 있습니다.​">
								i
							</a>
						</div>
					</div>
					<div class="col-xs-4 text-right">
						<button id="env_append_row_btn" class="btn btn-sm btn-primary">추가​</button>
					</div>
				</div>

				<!-- table -->
				<div class="table-wrap">
				<table class="table table-outline">
						<colgroup>
							<col style="width: 200px">
							<col>
							<col style="width: 100px">
						</colgroup>
						<thead class="text-center bg-blue-grey-100">
							<tr>
								<th>
									키​
								</th>
								<th>
									값​
								</th>
								<th>
									삭제​
								</th>
							</tr>
						</thead>
				</table>
				</div>
				<div class="scroll-content scroll-line" style="height: 205px">				
					<div class="table-wrap">
						<table class="table table-outline" id="envTable">
							<colgroup>
								<col style="width: 200px">
								<col>
								<col style="width: 100px">
							</colgroup>
							<tbody id="env_table_tbody">
								
							</tbody>
						</table>
					</div>
				</div>
				<!-- 에러 -->
				<div id="config_env_err" class="alert alert-danger" role="alert" style="display:none;">
					<svg xmlns="http://www.w3.org/2000/svg" width="1em" height="1em" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-alert-triangle"><path d="M10.29 3.86L1.82 18a2 2 0 0 0 1.71 3h16.94a2 2 0 0 0 1.71-3L13.71 3.86a2 2 0 0 0-3.42 0z"></path><line x1="12" y1="9" x2="12" y2="13"></line><line x1="12" y1="17" x2="12.01" y2="17"></line></svg>
					<div class="text-area">
						키와 값은 페어로 모두 입력해 주세요.​
					</div>
				</div>

			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-primary" name="config_confirm_btn" config_type="env" data-dismiss="modal">확인​</button>
				<button type="button" class="btn btn-default" name="config_cancle_btn" data-dismiss="modal">취소​</button>
			</div>
		</div>
	</div>
	<!-- 환경설정 table row 템플릿 -->
	<div style="display:none;">
		<table><tbody>
		<tr id="env_row_template">
			<td class="text-center">
				<input type="text" class="form-control" placeholder="" item-name="name">
			</td>
			<td>
				<input type="text" class="form-control" placeholder="" item-name="value">
			</td>
			<td class="text-center">
				<button class="btn btn-nostyle btn-sm" item-del="envrow">
					<div class="feather-icon">
						<svg xmlns="http://www.w3.org/2000/svg" width="1em" height="1em" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-trash-2"><polyline points="3 6 5 6 21 6"></polyline><path d="M19 6v14a2 2 0 0 1-2 2H7a2 2 0 0 1-2-2V6m3 0V4a2 2 0 0 1 2-2h4a2 2 0 0 1 2 2v2"></path><line x1="10" y1="11" x2="10" y2="17"></line><line x1="14" y1="11" x2="14" y2="17"></line></svg>
					</div>
				</button>
			</td>
		</tr>
		</tbody></table>
	</div>
	<!-- 환경설정 table row 템플릿 -->	
</div>


<!-- 2. 트래픽 모달 -->
<div class="modal" id="dialogTraffic">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">×</span>
				</button>
				<h4 class="modal-title">트래픽 설정​​</h4>
			</div>
			<div class="modal-body">
				<div class="text-right add-txt">
					<span><b class="caution-star">*</b> 는 필수입력 사항입니다.</span>
				</div>

				<div class="write-sub-title">
					<b class="caution-star">*</b> 초당 제한 설정(Burst Limit)​
					<a href="#" class="info-icon-tooltip" data-toggle="tooltip" title="초당 제한 값을 설정합니다.​​" data-placement="right">
						i
					</a>
				</div>
				<div class="form-inline">
					<input id="config_traffic_burst_val" type="text" class="form-control" placeholder="100" style="width: 60px;" maxlength=4 numberOnly value="{{config.trf.burst}}">
					<span>
						건/초​
					</span>
				</div>

				<div class="write-sub-title">
					<b class="caution-star">*</b> 기간별 제한 설정(Quota Limit)​
					<a href="#" class="info-icon-tooltip" data-toggle="tooltip" title="기간 별 제한 값을 설정합니다.​" data-placement="right">
						i
					</a>
				</div>
				<div class="form-inline">
					<div class="form-group">
						<input id="config_traffic_quota_val" type="text" class="form-control" placeholder="1" style="width: 60px;" numberOnly value="{{config.trf.quota.val}}" readonly/>
						<select name="" id="config_traffic_quota_types" class="form-control" style="width: 100px;">
							<option value="H" typenm="시간">시간​</option>
							<option value="D" typenm="일">일​</option>
							<option value="M" typenm="개월">개월​</option>
							<option value="Y" typenm="년">년</option>
						</select>
						<span>
							당​
						</span>
					</div>
					<div class="form-group margin-left-30">
						<input id="config_traffic_quota_cnt" type="text" class="form-control" placeholder="1000" style="width: 60px;" maxlength=6 numberOnly value="{{config.trf.quota.cnt}}">
						<span>
							건 제한​
						</span>
					</div>
					
				</div>
				<!-- 에러 -->
				<div id="config_traffic_err" class="alert alert-danger" role="alert" style="display: none;">
					<svg xmlns="http://www.w3.org/2000/svg" width="1em" height="1em" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-alert-triangle"><path d="M10.29 3.86L1.82 18a2 2 0 0 0 1.71 3h16.94a2 2 0 0 0 1.71-3L13.71 3.86a2 2 0 0 0-3.42 0z"></path><line x1="12" y1="9" x2="12" y2="13"></line><line x1="12" y1="17" x2="12.01" y2="17"></line></svg>
					<div class="text-area">
						15분 이내로 입력해 주세요.​
					</div>
				</div>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-primary" name="config_confirm_btn" config_type="traffic" data-dismiss="modal">확인​</button>
				<button type="button" class="btn btn-default" name="config_cancle_btn" data-dismiss="modal">취소​</button>
			</div>
		</div>
	</div>
</div>

<!-- 3. 기본 설정 모달 -->
<div class="modal" id="dialogResource">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">×</span>
				</button>
				<h4 class="modal-title">기본 설정​​​</h4>
			</div>
			<div class="modal-body">
				<div class="text-right add-txt">
					<span><b class="caution-star">*</b> 는 필수입력 사항입니다.</span>
				</div>

				<div class="write-sub-title">
					<b class="caution-star">*</b> 메모리​
					<a href="#" class="info-icon-tooltip" data-toggle="tooltip" title="함수 실행 시 사용할 최대 메모리를 설정합니다.​​​" data-placement="right">
						i
					</a>
				</div>
				<div class="card">
					<div class="card-body padding-top-10 padding-bottom-10">
						<div class="row">
							<div class="col-md-8">
								<input id="memory" data-slider-id='ex1Slider' type="text" data-slider-min="0" data-slider-max="256" data-slider-step="1" data-slider-value="{{config.mem}}"/>
							</div>
							<div class="col-md-4 text-right padding-top-15">
								<span class="color-primary"><b id="config_resource_mem"> {{config.mem}} </b></span>
								<small> / 256 MB </small>
							</div>
						</div>
					</div>
				</div>
				<div class="write-sub-title">
					<b class="caution-star">*</b> CPU
					<a href="#" class="info-icon-tooltip" data-toggle="tooltip" title="함수 실행 시 사용할 최대 CPU를 설정합니다." data-placement="right">
						i
					</a>
				</div>
				<div class="card">
					<div class="card-body padding-top-10 padding-bottom-10">
						<div class="row">
							<div class="col-md-8">
								<input id="cpu" data-slider-id='ex1Slider' type="text" data-slider-min="0" data-slider-max="1000" data-slider-step="1" data-slider-value="{{config.cpu}}"/>
							</div>
							<div class="col-md-4 text-right padding-top-15">
								<span class="color-primary"><b id="config_resource_cpu"> {{config.cpu}} </b></span>
								<small> / 1000 m </small>
							</div>
						</div>
					</div>
				</div>
				<div class="write-sub-title">
					<b class="caution-star">*</b> 제한 시간​
					<a href="#" class="info-icon-tooltip" data-toggle="tooltip" title="설정한 제한 시간 내에 함수가 실행되지 못 할 경우,  함수 실행이 중단됩니다.​​" data-placement="right">
						i
					</a>
				</div>
				<div class="form-inline">
					<div class="form-group">
						<input id="config_resource_timeout_min" type="text" class="form-control" placeholder="1" style="width: 60px;" maxlength=2 numberOnly value="{{config.timeout.min}}">
						<span>
							분​
						</span>
					</div>
					<div class="form-group">
						<input id="config_resource_timeout_sec" type="text" class="form-control" placeholder="1" style="width: 60px;" numberOnly value="{{config.timeout.sec}}">
						<span>
							초​
						</span>
					</div>
				</div>

				<!-- 에러 -->
				<div class="alert alert-danger" role="alert" style="display:none;">
					<i data-feather="alert-triangle"></i>
					<div class="text-area">
						15분 이내로 입력해 주세요.
					</div>
				</div>

			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-primary" name="config_confirm_btn" config_type="resource" data-dismiss="modal">확인​</button>
				<button type="button" class="btn btn-default" name="config_cancle_btn" data-dismiss="modal">취소​</button>
			</div>
		</div>
	</div>
</div>

<!-- 4. 동시성 모달 -->
<div class="modal" id="dialogConcur">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">×</span>
				</button>
				<h4 class="modal-title">함수 스케일 최대값 설정​</h4>
			</div>
			<div class="modal-body">
				<div class="text-right add-txt">
					<span><b class="caution-star">*</b> 는 필수입력 사항입니다.</span>
				</div>

				<div class="write-sub-title">
					<b class="caution-star">*</b> 함수 스케일 최대값​
					<a href="#" class="info-icon-tooltip" data-toggle="tooltip" title="함수가 동시에 실행될 수 있는 건 수를 설정합니다.​" data-placement="right">
						i
					</a>
				</div>
				<div class="form-inline">
					<div class="form-group">
						<input id="config_concur_concur" type="text" class="form-control" placeholder="1~100​" maxlength=5 numberOnly value="{{config.concur}}"/>
					</div>
				</div>

				<!-- 에러 -->
				<div id="config_concur_err" class="alert alert-danger" role="alert" style="display:none;">
					<i data-feather="alert-triangle"></i>
					<div class="text-area">
						1이상 100 이하로 설정해 주세요.​
					</div>
				</div>

			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-primary" name="config_confirm_btn" config_type="concur" data-dismiss="modal">확인​</button>
				<button type="button" class="btn btn-default" name="config_cancle_btn" data-dismiss="modal">취소​</button>
			</div>
		</div>
	</div>
</div>

