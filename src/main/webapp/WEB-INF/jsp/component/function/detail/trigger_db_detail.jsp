<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>

					<!-- DB 상세 -->
					<div data-lst="trigger" data-tab="DB" style="display: block;">
						<section class="panel panel-bordered">
							<div class="panel-heading">
								<h3 class="panel-title">
									DB
								</h3>
							</div> <!-- // panel-heading -->
							<div class="panel-body">
								<div class="row padding-left-10">
									<div class="col-md-1">
										<div class="write-sub-title blt-dot">
											DB 유형
										</div>
									</div>
									<div class="col-md-11">
										<div class="grey-600 font-size-13">
											{{db_type}}​
										</div>
									</div>
								</div>
								<div class="row padding-left-10">
									<div class="col-md-1">
										<div class="write-sub-title blt-dot">
											접속 정보​
										</div>
									</div>
									<div class="col-md-11">
										<div class="grey-600 font-size-13">
											<span>
												접속 URL : 
												<a href="javascript:void(0)" title="클립보드에 복사" target="_blank" data-role='btn' data-toggle='modal' data-target='#modalCopy' onclick="copyClipBoard('con_host_{{trigger_id}}')">
													<div id="con_host_{{trigger_id}}" style="display:none;">{{con_host}}:{{con_port}}</div>
													{{con_host}}:{{con_port}}
													<span class="feather-icon">
														<i data-feather="copy"></i>
													</span>
												</a>
											</span>
											<span class="margin-left-50">
												관리자 계정 아이디 : 
												<a href="javascript:void(0)" title="클립보드에 복사" target="_blank" data-role='btn' data-toggle='modal' data-target='#modalCopy' onclick="copyClipBoard('admin_id_{{trigger_id}}')">
													<div id="admin_id_{{trigger_id}}" style="display:none;">{{admin_id}}​</div>
													{{admin_id}}​ 
													<span class="feather-icon">
														<i data-feather="copy"></i>
													</span>
													
												</a>
											</span>
										</div>
									</div>
								</div>
								{{#if (eq db_type 'mariadb')}}
								<div class="row padding-left-10">
									<div class="col-md-1">
										<div class="write-sub-title blt-dot">
											DB 테이블​
										</div>
									</div>
									<div class="col-md-11">
										<div class="grey-600 font-size-13">
											{{table_name}}
										</div>
									</div>
								</div>
								{{/if}}
								{{#if (eq db_type 'postgre')}}
								<div class="row padding-left-10">
									<div class="col-md-1">
										<div class="write-sub-title blt-dot">
											DB 테이블​
										</div>
									</div>
									<div class="col-md-11">
										<div class="grey-600 font-size-13">
											{{table_name}}
										</div>
									</div>
								</div>
								{{/if}}
								{{#if (eq db_type 'mongodb')}}
								<div class="row padding-left-10">
									<div class="col-md-1">
										<div class="write-sub-title blt-dot">
											컬렉션
										</div>
									</div>
									<div class="col-md-11">
										<div class="grey-600 font-size-13">
											{{collection_name​}}
										</div>
									</div>
								</div>
								{{/if}}
								<div class="row padding-left-10">
									<div class="col-md-1">
										<div class="write-sub-title blt-dot">
											트리거 이벤트 유형​
										</div>
									</div>
									<div class="col-md-11">
										{{#if (eq insert_yn 'Y')}}
										<div class="grey-600 font-size-13">
											INSERT
											<span class="info-icon-tooltip" data-toggle="tooltip" title="" data-placement="right" data-original-title="DB에 새로운 데이터가 생성되면 함수가 실행됩니다.​">
												i
											</span>
										</div>
										{{/if}}
										{{#if (eq update_yn 'Y')}}
										<div class="grey-600 font-size-13">
											UPDATE
											<span class="info-icon-tooltip" data-toggle="tooltip" title="" data-placement="right" data-original-title="DB 데이터의 수정이 발생하면 함수가 실행됩니다.">
												i
											</span>
										</div>
										{{/if}}
										{{#if (eq delete_yn 'Y')}}
										<div class="grey-600 font-size-13">
											DETETE
											<span class="info-icon-tooltip" data-toggle="tooltip" title="" data-placement="right" data-original-title="DB에 적재된 데이터가 삭제되면 함수가 실행됩니다.​">
												i
											</span>
										</div>
										{{/if}}
									</div>
								</div>
							</div> <!-- // panel-body -->
						</section>
					</div>
					