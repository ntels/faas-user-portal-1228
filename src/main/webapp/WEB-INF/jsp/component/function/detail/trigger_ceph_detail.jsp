<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>

					<!-- Ceph 상세 -->
					<div data-lst="trigger" data-tab="Ceph" style="display: none;">
						<section class="panel panel-bordered">
							<div class="panel-heading">
								<h3 class="panel-title">
									Ceph
								</h3>
							</div> <!-- // panel-heading -->
							<div class="panel-body">
								<div class="row padding-left-10">
									<div class="col-md-1">
										<div class="write-sub-title blt-dot">
											버킷​
										</div>
									</div>
									<div class="col-md-11">
										<div class="grey-600 font-size-13">
											Bucket_name​
										</div>
									</div>
								</div>
								<div class="row padding-left-10">
									<div class="col-md-1">
										<div class="write-sub-title blt-dot">
											트리거 이벤트 유형​
										</div>
									</div>
									<div class="col-md-11">
										<div class="grey-600 font-size-13">
											파일 생성​
											<span class="info-icon-tooltip" data-toggle="tooltip" title="" data-placement="right" data-original-title="파일이 생성될 때 함수가 실행됩니다.​">
												i
											</span>
										</div>
									</div>
								</div>
								<div class="row padding-left-10">
									<div class="col-md-1">
										<div class="write-sub-title blt-dot">
											제한 경로
										</div>
									</div>
									<div class="col-md-11">
										<div class="grey-600 font-size-13">
											/image​
										</div>
									</div>
								</div>
								<div class="row padding-left-10">
									<div class="col-md-1">
										<div class="write-sub-title blt-dot">
											제한 확장자​
										</div>
									</div>
									<div class="col-md-11">
										<div class="grey-600 font-size-13">
											.jpg​
										</div>
									</div>
								</div>
							</div> <!-- // panel-body -->
						</section>
					</div>
						
						
					<div class="ncz_panel" data-lst="editWrap" data-tab="Ceph" style="display: block;">
						<div class="panel_head">
							<span class="tit">Ceph</span>
						</div>
						<div class="panel_cnt">
							<ul class="ncz_dot_lst w170">
								<li>
									<strong>버킷</strong>
									<span>
										Bucket_name
									</span>
								</li>
								<li>
									<strong>트리거 이벤트 유형</strong>
									<span>
										파일 생성<em class="ico-info1 ico right" data-toggle="tooltip" data-placement="right" title="" data-original-title="파일이 생성될 때 함수가 실행됩니다."></em>
									</span>
								</li>
								<li>
									<strong>제한 경로</strong>
									<span>/image</span>
								</li>
								<li>
									<strong>제한 확장자</strong>
									<span>.jpg</span>
								</li>
							</ul>
						</div>
					</div>