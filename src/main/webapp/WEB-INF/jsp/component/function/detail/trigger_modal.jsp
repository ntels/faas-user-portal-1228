<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>

<!-- modal -->
<div class="modal" id="regTriggerModal" data-keyboard="false" data-backdrop="static">
	<div class="modal-dialog modal-lg">
		<div id="trigger-modal-mask" style="position:absolute;left:0;top:0;width:100%;height:100%;background:#000000;z-index:999999;opacity:0.5;display:none"></div>
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" name="hide_trig_modal" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">×</span>
				</button>
				<h4 class="modal-title">트리거 추가​​</h4>
			</div>
			<div class="modal-body">
				
				<div class="text-right add-txt">
					<span><b class="caution-star">*</b> 는 필수입력 사항입니다.</span>
				</div>
				
				<div class="write-sub-title">
					<b class="caution-star">*</b> 트리거 유형​
					<span class="info-icon-tooltip" data-toggle="tooltip" title="함수를 실행하기 위한 조건을 설정합니다. 여러 개의 트리거를 한 개의 함수에 연결 등록하실 수 있으며, 한 개씩 설정해 주세요.​​​​" data-placement="right">
						i
					</span>
				</div>

				<ul class="list-inline margin-bottom-30">
					<li>
						<label for="HTTP" class="input-card-type">
							<input type="radio" id="HTTP" name="trigger" class="card-input-element" data-btn='http'>
							<div class="card-input">
								<div class="card-type4">
									<div class="feather-icon font-size-50 color-1">
										<i data-feather="globe"></i>
									</div>
								</div>
							</div>
							<p class="text-center">
								HTTP
							</p>
						</label>
					</li>
					<li>
						<label for="Ceph" class="input-card-type">
							<input type="radio" id="Ceph" name="trigger" class="card-input-element" data-btn='ceph'>
							<div class="card-input">
								<div class="card-type4">
									<div class="img-ceph img-size50"></div>
								</div>
							</div>
							<p class="text-center">
								Ceph
							</p>
						</label>
					</li>
					<li>
						<label for="DB" class="input-card-type">
							<input type="radio" id="DB" name="trigger" class="card-input-element" data-btn='db'>
							<div class="card-input">
								<div class="card-type4">
									<div class="feather-icon font-size-50 color-2">
										<i data-feather="database"></i>
									</div>
								</div>
							</div>
							<p class="text-center">
								DB
							</p>
						</label>
					</li>
					<li>
						<label for="Timer" class="input-card-type">
							<input type="radio" id="Timer" name="trigger" class="card-input-element" data-btn='timer'>
							<div class="card-input">
								<div class="card-type4">
									<div class="feather-icon font-size-50 color-3">
										<i data-feather="clock"></i>
									</div>
								</div>
							</div>
							<p class="text-center">
								타이머
							</p>
						</label>
					</li>
				</ul>

				<!-- HTTP -->
				<div data-lst="registry" data-tab='http'>
					<div class="write-sub-title">
						<b class="caution-star">*</b> API 이름​
						<span class="info-icon-tooltip" data-toggle="tooltip" title="API 이름을 입력하면, 자동으로 앤드포인트가 생성됩니다. API 이름은 공백 없이 영문 소문자와 언더바(_)만 입력 가능합니다.​​​​​" data-placement="right">
							i
						</span>
					</div>
					<div class="form-group">
						<input type="text" id="trig_modal_api_name" class="form-control" placeholder="API 이름 입력 (예 : api_name)">
					</div>
				</div>

				<!-- Ceph -->
				<div data-lst="registry" data-tab='ceph' style="display: none;">
					<div class="write-sub-title">
						<b class="caution-star">*</b> 버킷​
						<span class="info-icon-tooltip" data-toggle="tooltip" title="사전에 생성해 둔 버킷을 선택해 주세요. 버킷이 없으면 이용하실 수 없습니다.​​" data-placement="right">
							i
						</span>
					</div>
					<div class="form-group">
						<select name="" id="formName" class="form-control">
							<option value="">버킷 선택</option>
						</select>
					</div>
					
					<div class="write-sub-title">
						<b class="caution-star">*</b> 트리거 이벤트 유형​
						<span class="info-icon-tooltip" data-toggle="tooltip" title="함수가 실행될 트리거 이벤트를 선택합니다. 선택한 이벤트가 발생할 경우에 한하여 함수가 실행됩니다,​" data-placement="right">
							i
						</span>
					</div>
					<div class="row">
						<div class="col-md-6">
							<label for="card01" class="input-card-type">
								<input type="radio" id="card01" name="card1" class="card-input-element" checked>
								<div class="card-input">
									<div class="card-type4">
										<span class="img-size50 img-file"></span>
										<div class="text-area">
											<div class="title">파일 생성​​</div>
											<p>
												파일이 생성될 때 함수가 실행됩니다.
											</p>
										</div>
									</div>
								</div>
							</label>
						</div>
						<div class="col-md-6">
							<label for="card02" class="input-card-type">
								<input type="radio" id="card02" name="card1" class="card-input-element">
								<div class="card-input">
									<div class="card-type4">
										<span class="img-size50 img-delete"></span>
										<div class="text-area">
											<div class="title">파일 삭제​</div>
											<p>
												파일이 삭제될 때 함수가 실행됩니다.​
											</p>
										</div>
									</div>
								</div>
							</label>
						</div>
					</div>
					
					<div class="write-sub-title">
						제한 경로​
						<span class="info-icon-tooltip" data-toggle="tooltip" title="설정한 경로 내에서 발생한 이벤트만 트리거로 작동합니다.​​​" data-placement="right">
							i
						</span>
					</div>
					<div class="form-group">
						<input type="text" class="form-control" id="" placeholder="예 : images/​​​">
					</div>
					
					<div class="write-sub-title">
						제한 확장자​
						<span class="info-icon-tooltip" data-toggle="tooltip" title="설정한 확장자의 파일 유형만 트리거로 작동합니다.​​​​" data-placement="right">
							i
						</span>
					</div>
					<div class="form-group">
						<input type="text" class="form-control" id="" placeholder="예 : .jpg​​​​">
					</div>
				</div>		
				
				<!-- DB -->
				<div data-lst="registry" data-tab='db' style="display: none;">
					<div class="write-sub-title">
						<b class="caution-star">*</b> DB 유형​
						<span class="info-icon-tooltip" data-toggle="tooltip" title="쿠버네티스 서비스를 통해 사전에 설치한 DB 유형을 선택해 주세요. ​​" data-placement="right">
							i
						</span>
					</div>
					<div class="form-group">
						<select id="config_db_type" class="form-control">
							<option value="mongodb">MongoDB</option>
							<option value="postgresql">PostgreSQL</option>
							<option value="mariadb">MariaDB</option>
						</select>
					</div>

					<div class="alert alert-primary" role="alert" data-lst="db" data-tab="mongodb">
						<i data-feather="file-text"></i>
						<div class="text-area">
							<a href="SC_KS_01_001.html" class="link2">쿠버네티스 서비스</a>를 통해 사전에 설치한 DB가 있을 경우에만 이용할 수 있습니다.​​<br>
							쿠버네티스 서비스 > 카탈로그 앱 메뉴에서 <span class="red bold">mongodb-replicaset</span> 앱을 설치한 후 이용해 주세요.​
						</div>
					</div>				
					<div class="alert alert-primary" role="alert" data-lst="db" data-tab="postgresql">
						<i data-feather="file-text"></i>
						<div class="text-area">
							<a href="SC_KS_01_001.html" class="link2">쿠버네티스 서비스</a>를 통해 사전에 설치한 DB가 있을 경우에만 이용할 수 있습니다.​<br>
							쿠버네티스 서비스 > 카탈로그 앱 메뉴에서 <span class="red bold">postgresql</span> 앱을 설치한 후 이용해 주세요.​
						</div>
					</div>
					<div class="alert alert-primary" role="alert" data-lst="db" data-tab="mariadb">
						<i data-feather="file-text"></i>
						<div class="text-area">
							<a href="SC_KS_01_001.html" class="link2">쿠버네티스 서비스</a>를 통해 사전에 설치한 DB가 있을 경우에만 이용할 수 있습니다.​<br>
							쿠버네티스 서비스 > 카탈로그 앱 메뉴에서 <span class="red bold">mariadb-replicaset</span> 앱을 설치한 후 이용해 주세요.​
						</div>
					</div>
					<div class="row margin-top-30">
						<div class="col-md-6">
							<div class="write-sub-title">
								<b class="caution-star">*</b> 접속 URL​
								<span class="info-icon-tooltip" data-toggle="tooltip" title="선택한 DB의 접속 URL을 입력해 주세요.​​​​​" data-placement="right">
									i
								</span>
							</div>
							<div class="form-group">
								<input type="text" class="form-control" id="config_con_host" name="config_con_host" placeholder="접속 URL을 입력해 주세요.​">
							</div>
						</div>
						<div class="col-md-6">
							<div class="write-sub-title">
								<b class="caution-star">*</b> 접속 PORT​
								<span class="info-icon-tooltip" data-toggle="tooltip" title="선택한 DB의 접속 PORT를 입력해 주세요.​​​​​" data-placement="right">
									i
								</span>
							</div>
							<div class="form-group">
								<input type="text" class="form-control" id="config_con_port" name="config_con_port" placeholder="접속 PORT를 입력해 주세요.​">
							</div>
						</div>
					</div>

					<div class="write-sub-title">
						<b class="caution-star">*</b> 데이터베이스
						<span class="info-icon-tooltip" data-toggle="tooltip" title="선택한 DB의 데이터베이스를 입력해 주세요.​​​​​" data-placement="right">
							i
						</span>
					</div>
					<div class="form-group">
						<input type="text" class="form-control" id="config_db_name" name="config_db_name" placeholder="데이터베이스를 입력해 주세요.​">
					</div>

					<div class="row margin-top-30">
						<div class="col-md-6">
							<div class="write-sub-title">
								<b class="caution-star">*</b> 관리자 계정 아이디​
								<span class="info-icon-tooltip" data-toggle="tooltip" title="반드시 관리자 권한을 가진 아이디를 입력해 주세요.​​​​​​" data-placement="right">
									i
								</span>
							</div>
							<div class="form-group">
								<input type="text" class="form-control" id="config_admin_id" name="config_admin_id" placeholder="관리자 계정 아이디를 입력해 주세요.​​">
							</div>
						</div>
						<div class="col-md-6">
							<div class="write-sub-title">
								<b class="caution-star">*</b> 관리자 계정 비밀번호​
								<span class="info-icon-tooltip" data-toggle="tooltip" title="입력한 관리자 계정의 비밀번호를 입력해 주세요.​​​​​​" data-placement="right">
									i
								</span>
							</div>
							<div class="form-group">
								<input type="password" class="form-control" id="config_admin_pw" name="config_admin_pw" placeholder="관리자 계정 비밀번호를 입력해 주세요.​​">
							</div>
						</div>
					</div>
					
					<div class="text-center margin-top-20">
						<button id="check_db_validation" name="check_db_validation" class="btn btn-primary" >접속 테스트​</button>
					</div>

					<div id="mongodb_config_area" style="display: none;">
						<div class="write-sub-title">
							<b class="caution-star">*</b> 컬렉션
							<span class="info-icon-tooltip" data-toggle="tooltip" title="MongoDB 대상 컬렉션을 선택해 주세요.​​" data-placement="right">
								i
							</span>
						</div>
						<div class="form-group">
							<select name="mongodb_table_list" id="mongodb_table_list" class="form-control">
								<option value="">컬렉션 선택​</option>
							</select>
							<small id="mongodb_empty_table_err" class="form-text color-danger" style="display:none;">
								<i class="xi-info-o"></i>
								등록된 컬렉션이 없습니다. 쿠버네티스 서비스의 카탈로그 앱에서 MongoDB 컬렉션을 추가한 후 이용해 주세요.​
							</small>
						</div>
					</div>
					<div id="postgresql_config_area" style="display: none;">
						<div class="write-sub-title">
							<b class="caution-star">*</b> DB 테이블
							<span class="info-icon-tooltip" data-toggle="tooltip" title="사전에 생성해 둔 DB 테이블을 선택해 주세요.  DB 테이블이 없으면 이용하실 수 없습니다.​" data-placement="right">
								i
							</span>
						</div>
						<div class="form-group">
							<select name="postgresql_table_list" id="postgresql_table_list" class="form-control">
								<option value="">DB 테이블 선택​</option>
							</select>
							<small id="postgresql_empty_table_err" class="form-text color-danger" style="display:none;">
								<i class="xi-info-o"></i>
								등록된 DB 테이블이 없습니다. 쿠버네티스 서비스의 카탈로그 앱에서 PostgreSQL 테이블을 추가한 후 이용해 주세요.​
							</small>
						</div>
					</div>
					<div id="mariadb_config_area" style="display: none;">
						<div class="write-sub-title">
							<b class="caution-star">*</b> DB 테이블
							<span class="info-icon-tooltip" data-toggle="tooltip" title="사전에 생성해 둔 DB 테이블을 선택해 주세요.  DB 테이블이 없으면 이용하실 수 없습니다.​" data-placement="right">
								i
							</span>
						</div>
						<div class="form-group">
							<select name="mariadb_table_list" id="mariadb_table_list" class="form-control">
								<option value="">DB 테이블 선택​</option>
							</select>
							<small id="mariadb_empty_table_err" class="form-text color-danger" style="display:none;">
								<i class="xi-info-o"></i>
								등록된 DB 테이블이 없습니다. 쿠버네티스 서비스의 카탈로그 앱에서 MariaDB 테이블을 추가한 후 이용해 주세요.​
							</small>
						</div>
					</div>

					<div id="event_config_area" class="write-sub-title" style="display: none;">
						<div class="write-sub-title">
							<b class="caution-star">*</b> 트리거 이벤트 유형​
							<span class="info-icon-tooltip" data-toggle="tooltip" title="함수가 실행될 트리거 이벤트를 선택합니다. 선택한 이벤트가 발생할 경우에 한하여 함수가 실행됩니다.​​" data-placement="right">
								i
							</span>
						</div>
						<div class="row">
							<div class="col-md-4">
								<label for="INSERT​" class="input-card-type">
									<input type="radio" id="INSERT​" name="eventType" class="card-input-element" value="insert">
									<div class="card-input">
										<div class="card card-type-img1">
											<div class="img-content-type2 margin-top-0">
												<div class="img-trigger img-insert"></div>
												<div class="name font-size-16">
													<b>INSERT​</b>
												</div>
												<span>DB에 새로운 데이터가 생성되면​<br> 함수가 실행됩니다.​</span>
											</div>
										</div>
									</div>
								</label>
							</div>
							<div class="col-md-4">
								<label for="UPDATE​" class="input-card-type">
									<input type="radio" id="UPDATE​" name="eventType" class="card-input-element" value="update">
									<div class="card-input">
										<div class="card card-type-img1">
											<div class="img-content-type2 margin-top-0">
												<div class="img-trigger img-update"></div>
												<div class="name font-size-16">
													<b>UPDATE​</b>
												</div>
												<span>DB 데이터의 수정이 발생하면<br>​ 함수가 실행됩니다.​​</span>
											</div>
										</div>
									</div>
								</label>
							</div>
							<div class="col-md-4">
								<label for="DELETE​" class="input-card-type">
									<input type="radio" id="DELETE​" name="eventType" class="card-input-element" value="delete">
									<div class="card-input">
										<div class="card card-type-img1">
											<div class="img-content-type2 margin-top-0">
												<div class="img-trigger img-delete"></div>
												<div class="name font-size-16">
													<b>DELETE​</b>
												</div>
												<span>DB에 적재된 데이터가 삭제되면​<br> 함수가 실행됩니다.​</span>
											</div>
										</div>
									</div>
								</label>
							</div>
						</div>
					</div>
				</div>
			
				<!-- Timer -->
				<div data-lst="registry" data-tab='timer' style="display: none;">
					<div class="write-sub-title">
						<b class="caution-star">*</b> 기준 일시​
						<span class="info-icon-tooltip" data-toggle="tooltip" title="트리거 이벤트가 발생할 날짜와 시간을 설정해 주세요." data-placement="right">
							i
						</span>
					</div>
					<div class="form-inline">
						<div class="form-group">
							<div class="input-icon-type2 icon-left">
								<span class="input-icon-addon">
									<i class="xi-calendar"></i>
								</span>
								<input type="text" id="bas_date_hms_date" class="form-control datepicker">
							</div>
						</div>
						<div class="form-group">
							<select id="bas_date_hms_hour" name="trig_modal_select_box_items" class="form-control">
								{{#for 0 23}}
									<option value="{{@index}}">{{@index}}시</option>
								{{/for}}
							</select>
						</div>
						<div class="form-group">
							<select id="bas_date_hms_miniute" name="trig_modal_select_box_items" class="form-control">
								{{#for 0 59}}
									<option value="{{@index}}">{{@index}}분</option>
								{{/for}}
							</select>
						</div>
					</div>

					<div class="write-sub-title">
						<b class="caution-star">*</b> 반복 설정
						<span class="info-icon-tooltip" data-toggle="tooltip" title="트리거 이벤트 반복을 원할 경우, 반복 주기를 설정해 주세요.​" data-placement="right">
							i
						</span>
					</div>

					<!-- repeat -->
					<div data-lst="admin">
						<ul class="list-inline">
							<li>
								<label for="triggeradd1" class="input-card-type">
									<input type="radio" id="triggeradd1" name="repeatSet" class="card-input-element" value="NO" data-btn="none" checked>
									<div class="card-input">
										<div class="card-type4">
											<span class="img-repeat-big">
												<span class="none">
													<i></i>
												</span>
											</span>
										</div>
									</div>
									<p class="text-center">
										없음
									</p>
								</label>
							</li>
							<li>
								<label for="triggeradd2" class="input-card-type">
									<input type="radio" id="triggeradd2" name="repeatSet" class="card-input-element" value="MN" data-btn="minute">
									<div class="card-input">
										<div class="card-type4">
											<span class="img-repeat-big">
												<span class="minute">
													<i></i>
												</span>
											</span>
										</div>
									</div>
									<p class="text-center">
										분
									</p>
								</label>
							</li>
							<li>
								<label for="triggeradd3" class="input-card-type">
									<input type="radio" id="triggeradd3" name="repeatSet" class="card-input-element" value="HR" data-btn="hour">
									<div class="card-input">
										<div class="card-type4">
											<span class="img-repeat-big">
												<span class="hour">
													<i></i>
												</span>
											</span>
										</div>
									</div>
									<p class="text-center">
										시간
									</p>
								</label>
							</li>
							<li>
								<label for="triggeradd4" class="input-card-type">
									<input type="radio" id="triggeradd4" name="repeatSet" class="card-input-element" value="DY" data-btn="day">
									<div class="card-input">
										<div class="card-type4">
											<span class="img-repeat-big">
												<span class="day">
													<i></i>
												</span>
											</span>
										</div>
									</div>
									<p class="text-center">
										매일
									</p>
								</label>
							</li>
							<li>
								<label for="triggeradd5" class="input-card-type">
									<input type="radio" id="triggeradd5" name="repeatSet" class="card-input-element" value="WK" data-btn="week">
									<div class="card-input">
										<div class="card-type4">
											<span class="img-repeat-big">
												<span class="week">
													<i></i>
												</span>
											</span>
										</div>
									</div>
									<p class="text-center">
										주별
									</p>
								</label>
							</li>
							<li>
								<label for="triggeradd6" class="input-card-type">
									<input type="radio" id="triggeradd6" name="repeatSet" class="card-input-element" value="MO" data-btn="month">
									<div class="card-input">
										<div class="card-type4">
											<span class="img-repeat-big">
												<span class="month">
													<i></i>
												</span>
											</span>
										</div>
									</div>
									<p class="text-center">
										월별
									</p>
								</label>
							</li>
							<li>
								<label for="triggeradd7" class="input-card-type">
									<input type="radio" id="triggeradd7" name="repeatSet" class="card-input-element" value="YR" data-btn="year">
									<div class="card-input">
										<div class="card-type4">
											<span class="img-repeat-big">
												<span class="year">
													<i></i>
												</span>
											</span>
										</div>
									</div>
									<p class="text-center">
										연간
									</p>
								</label>
							</li>
						</ul>

						<div class="margin-20">
							<div class="row" data-lst="repeat" data-tab="minute">
								<div class="col-md-12">
									<div class="form-inline">
										<div class="radio-group">
											<input type="radio" name="radio01" id="radio01-1" class="radio-input" checked>
											<label for="radio01-1" class="radio-item">
												<div class="radio">
													<div class="checked-icon"></div>
												</div>
												<span class="text">
													매
												</span>
											</label>
										</div>
										<div class="form-group">
											<select id="mn-value" name="trig_modal_select_box_items" class="form-control">
												{{#for 1 59}}
													<option value="{{@index}}">{{@index}}분</option>
												{{/for}}
											</select>
											<span class="text">분 마다 반복​​</span>
										</div>
									</div>
								</div>
							</div>

							<div class="row" data-lst="repeat" data-tab="hour">
								<div class="col-md-12">
									<div class="form-inline">
										<div class="radio-group">
											<input type="radio" name="radio02" id="radio01-2" class="radio-input" checked>
											<label for="radio01-2" class="radio-item">
												<div class="radio">
													<div class="checked-icon"></div>
												</div>
												<span class="text">
													매
												</span>
											</label>
										</div>
										<div class="form-group">
											<select id="hr-value" name="trig_modal_select_box_items"  class="form-control">
												{{#for 1 23}}
													<option value="{{@index}}">{{@index}}시간</option>
												{{/for}}
											</select>
											<span class="text">시간 마다 반복​​​</span>
										</div>
									</div>
								</div>
							</div>

							<div class="row" data-lst="repeat" data-tab="day">
								<div class="col-md-5">
									<div class="form-inline">
										<div class="radio-group">
											<input type="radio" name="radio03" id="radio03-1" class="radio-input" value="day" checked>
											<label for="radio03-1" class="radio-item">
												<div class="radio">
													<div class="checked-icon"></div>
												</div>
												<span class="text">
													매
												</span>
											</label>
										</div>
										<div class="form-group">
											<select id="dy-day-value" name="trig_modal_select_box_items" class="form-control">
												{{#for 1 30}}
													<option value="{{@index}}">{{@index}}일</option>
												{{/for}}
											</select>
											<span class="text">일 마다 반복​​​</span>
										</div>
									</div>
								</div>
								<div class="col-md-7">
									<div class="form-inline">
										<div class="radio-group padding-top-10">
											<input type="radio" name="radio03" id="radio03-2" class="radio-input" value="week">
											<label for="radio03-2" class="radio-item">
												<div class="radio">
													<div class="checked-icon"></div>
												</div>
												<span class="text">
													평일​
												</span>
											</label>
										</div>
									</div>
								</div>
							</div>

							<div class="row" data-lst="repeat" data-tab="week">
								<div class="col-md-12">
									<div class="form-inline margin-top-10">
										<div class="checkbox-group margin-right-20">
											<input type="checkbox" id="monday" name="wk-value" class="checkbox-input" data-code="MON" value="월요일">
											<label for="monday" class="checkbox-item">
												<div class="checkbox">
													<div class="checked-icon">
														<i class="xi-check"></i>
													</div>
												</div>
												<span class="text">
													월요일
												</span>
											</label>
										</div>
										<div class="checkbox-group margin-right-20">
											<input type="checkbox" id="tuesday" name="wk-value" class="checkbox-input" data-code="TUE" value="화요일">
											<label for="tuesday" class="checkbox-item">
												<div class="checkbox">
													<div class="checked-icon">
														<i class="xi-check"></i>
													</div>
												</div>
												<span class="text">
													화요일
												</span>
											</label>
										</div>
										<div class="checkbox-group margin-right-20">
											<input type="checkbox" id="wednesday" name="wk-value" class="checkbox-input" data-code="WED" value="수요일">
											<label for="wednesday" class="checkbox-item">
												<div class="checkbox">
													<div class="checked-icon">
														<i class="xi-check"></i>
													</div>
												</div>
												<span class="text">
													수요일
												</span>
											</label>
										</div>
										<div class="checkbox-group margin-right-20">
											<input type="checkbox" id="thursday" name="wk-value" class="checkbox-input" data-code="THU" value="목요일">
											<label for="thursday" class="checkbox-item">
												<div class="checkbox">
													<div class="checked-icon">
														<i class="xi-check"></i>
													</div>
												</div>
												<span class="text">
													목요일
												</span>
											</label>
										</div>
										<div class="checkbox-group margin-right-20">
											<input type="checkbox" id="friday" name="wk-value" class="checkbox-input" data-code="FRI" value="금요일">
											<label for="friday" class="checkbox-item">
												<div class="checkbox">
													<div class="checked-icon">
														<i class="xi-check"></i>
													</div>
												</div>
												<span class="text">
													금요일
												</span>
											</label>
										</div>
										<div class="checkbox-group margin-right-20">
											<input type="checkbox" id="saturday" name="wk-value" class="checkbox-input" data-code="SAT" value="토요일">
											<label for="saturday" class="checkbox-item">
												<div class="checkbox">
													<div class="checked-icon">
														<i class="xi-check"></i>
													</div>
												</div>
												<span class="text">
													토요일
												</span>
											</label>
										</div>
										<div class="checkbox-group margin-right-20">
											<input type="checkbox" id="sunday" name="wk-value" class="checkbox-input" data-code="SUN" value="일요일">
											<label for="sunday" class="checkbox-item">
												<div class="checkbox">
													<div class="checked-icon">
														<i class="xi-check"></i>
													</div>
												</div>
												<span class="text">
													일요일
												</span>
											</label>
										</div>
									</div>
								</div>
							</div>

							<div class="row" data-lst="repeat" data-tab="month">
								<div class="col-md-5">
									<div class="form-inline">
										<div class="radio-group">
											<input type="radio" name="radio04" id="radio04-1" class="radio-input" value="day" checked>
											<label for="radio04-1" class="radio-item">
												<div class="radio">
													<div class="checked-icon"></div>
												</div>
												<span class="text">
													매월​
												</span>
											</label>
										</div>
										<div class="form-group">
											<select id="mo-day-value" name="trig_modal_select_box_items" class="form-control">
												{{#for 1 31}}
													<option value="{{@index}}">{{@index}}일</option>
												{{/for}}
											</select>
											<span class="text">일 마다 반복​</span>
										</div>
									</div>
								</div>
								<div class="col-md-7">
									<div class="form-inline">
										<div class="radio-group">
											<input type="radio" name="radio04" id="radio04-2" class="radio-input" value="week">
											<label for="radio04-2" class="radio-item">
												<div class="radio">
													<div class="checked-icon"></div>
												</div>
											</label>
										</div>
										<div class="form-group">
											<select id="mo-week-value-th" name="trig_modal_select_box_items" class="form-control">
												<option value="1" selected="">첫 번째</option>
												<option value="2">두 번째</option>
												<option value="3">세 번째</option>
												<option value="4">네 번째</option>
												<option value="5">다섯 번째</option>
											</select>
										</div>
										<div name="trig_modal_select_box_items" class="form-group">
											<select id="mo-week-value-week" class="form-control">
												<option value="MON">월요일</option>
												<option value="TUE">화요일</option>
												<option value="WED">수요일</option>
												<option value="THU">목요일</option>
												<option value="FRI">금요일</option>
												<option value="SAT">토요일</option>
												<option value="SUN">일요일</option>
											</select>
											<span class="text">마다 반복​</span>
										</div>
									</div>
								</div>
							</div>

							<div class="row" data-lst="repeat" data-tab="year">
								<div class="col-md-5">
									<div class="form-inline">
										<div class="radio-group">
											<input type="radio" name="radio05" id="radio05-1" class="radio-input" value="month" checked>
											<label for="radio05-1" class="radio-item">
												<div class="radio">
													<div class="checked-icon"></div>
												</div>
												<span class="text">
													매년​​
												</span>
											</label>
										</div>
										<div class="form-group">
											<select id="yr-month-value" name="trig_modal_select_box_items" class="form-control">
												{{#for 1 12}}
													<option value="{{@index}}">{{@index}}월</option>
												{{/for}}
											</select>
											<span class="text">월 마다 반복</span>
										</div>
									</div>
								</div>
								<div class="col-md-7">
									<div class="form-inline">
										<div class="radio-group">
											<input type="radio" name="radio05" id="radio05-2" class="radio-input" value="week">
											<label for="radio05-2" class="radio-item">
												<div class="radio">
													<div class="checked-icon"></div>
												</div>
											</label>
										</div>
										<div class="form-group">
											<select id="yr-week-value-th" name="trig_modal_select_box_items" class="form-control">
												<option value="1" selected="">첫 번째</option>
												<option value="2">두 번째</option>
												<option value="3">세 번째</option>
												<option value="4">네 번째</option>
												<option value="5">다섯 번째</option>
											</select>
										</div>
										<div class="form-group">
											<select id="yr-week-value-week" name="trig_modal_select_box_items" class="form-control">
												<option value="MON">월요일</option>
												<option value="TUE">화요일</option>
												<option value="WED">수요일</option>
												<option value="THU">목요일</option>
												<option value="FRI">금요일</option>
												<option value="SAT">토요일</option>
												<option value="SUN">일요일</option>
											</select>
											<span class="text">마다 반복​</span>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>

					<div id="repeatEndDate">
						<div class="write-sub-title">
							<b class="caution-star">*</b> 반복 종료 일​시
							<a href="#" class="info-icon-tooltip" data-toggle="tooltip" title="트리거 이벤트 반복이 종료되는 날짜 및 시간을 설정해 주세요.​​" data-placement="right">
								i
							</a>
						</div>
						<div class="form-inline datepicker_wrap">
							<div class="form-group">
								<div class="input-icon-type2 icon-left">
									<span class="input-icon-addon">
										<i class="xi-calendar"></i>
									</span>
									<input type="text" id="end_date_hms_date" class="form-control datepicker ele">
								</div>
							</div>
							<div class="form-group">
								<select id="end_date_hms_hour" name="trig_modal_select_box_items" class="form-control ele">
									{{#for 0 23}}
										<option value="{{@index}}">{{@index}}시</option>
									{{/for}}
								</select>
							</div>
							<div class="form-group">
								<select id="end_date_hms_minute" name="trig_modal_select_box_items" class="form-control ele">
									{{#for 0 59}}
										<option value="{{@index}}">{{@index}}분</option>
									{{/for}}
								</select>
							</div>
							<div class="checkbox-group margin-left-20">
								<input type="checkbox" id="chk_enddate" class="checkbox-input">
								<label for="chk_enddate" class="checkbox-item">
									<div class="checkbox">
										<div class="checked-icon">
											<i class="xi-check"></i>
										</div>
									</div>
									<span class="text">
										사용 안 함​
									</span>
								</label>
							</div>
						</div>
					</div>
				</div>
			

			</div>
			<div class="modal-footer">
				<button type="button" id="create_trig" class="btn btn-primary">추가​</button>
				<button type="button" name="hide_trig_modal" class="btn btn-default" data-dismiss="modal">취소​</button>
			</div>
		</div>
	</div>
</div>

<link rel="stylesheet" href="${pageContext.request.contextPath}/core/lib/datepicker/daterangepicker.css">
<script src="${pageContext.request.contextPath}/core/lib/datepicker/daterangepicker.js"></script>
<script src="${pageContext.request.contextPath}/core/lib/datepicker/moment.min.js"></script>

<script>
	$(document).ready(function() {
		$('.input-card-type > [name="trigger"]').on('click', function() {		
			if($(this).hasClass('checked')) {
				return false;
			} else {
				var section = $(this).attr('data-btn');
				$('[data-lst="registry"]').hide();
				$('[data-tab='+ section +']').show();
				
				$(this).closest('.input-card-type').find('[name="trigger"]').removeClass('checked');
				initHttpData();
				initDBData();
				initTimerData();
			};
		});

		$('#regTriggerModal').on('hidden.bs.modal', function (e) {
			$('[data-btn="http"]').trigger("click");
		});

		bindHttpEvent();
		bindDBEvent();
		bindTimerEvent();
	});

	function httpTriggerInfo() {
		var info = {};
		info.isSuccess = true;
		info.api_name = $('#trig_modal_api_name').val();

		if(info.api_name == null || info.api_name == "") {
			info.isSuccess = false;
		}
		return info;
	}
	function cephTriggerInfo() {
		var info = {};
		
		info.isSuccess = true;
		
		return info;
	}

	function timerTriggerInfo() {
		var info = {};
		var repeat_des = "";
		
		// TODO: PARAM CHECK
		info.isSuccess = true;
		
		let basdate = $('#bas_date_hms_date').val() || '0000-00-00';
		info.bas_date_hms = basdate + ' ' + $('#bas_date_hms_hour').val().padStart(2, '0') + '\:' + $('#bas_date_hms_miniute').val().padStart(2, '0') + '\:00';
		if(basdate == "0000-00-00") info.isSuccess = false;

		if($('#chk_enddate').prop('checked')) {
			info.end_use_yn = 'N';
			info.end_date_hms = '2999-01-01 00\:00\:00';
		} else {
			info.end_use_yn = 'Y';
			let enddate = $('#end_date_hms_date').val() || '2999-01-01';
			info.end_date_hms = enddate + ' ' + $('#end_date_hms_hour').val().padStart(2, '0') + '\:' + $('#end_date_hms_minute').val().padStart(2, '0') + '\:00';
			if(enddate == "2999-01-01") info.isSuccess = false;
		}

		info.repeat_tp = $(':input:radio[name="repeatSet"]:checked').val();
		let repeat = {};
		switch(info.repeat_tp){
			case 'MN':
				repeat.value = $('#mn-value').val();
				repeat_des = "매 " + repeat.value + "분 마다 반복";
				break;
			case 'HR':
				repeat.value = $('#hr-value').val();
				repeat_des = "매 " + repeat.value + "시간 마다 반복";
				break;
			case 'DY':
				repeat.radio = $(':radio[name="radio03"]:checked').val();
				if(repeat.radio === 'day') {
					repeat.value = $('#dy-day-value').val();
					repeat_des = "매 " + repeat.value + "일 마다 반복";
				} else
					repeat_des = "평일 마다 반복";
				break;
			case 'WK':
				repeat.value = $(':checkbox[name="wk-value"]:checked').map(function(){ return $(this).data('code') }).get().join();
				var wk_value_txt = $(':checkbox[name="wk-value"]:checked').map(function(){ return $(this).val() }).get().join();
				repeat_des = wk_value_txt + " 마다 반복";
				repeat_des = repeat_des.replace(/,/gi, ", ");
				if(repeat.value == "") info.isSuccess = false;
				break;
			case 'MO':
				repeat.radio = $(':radio[name="radio04"]:checked').val();
				if(repeat.radio == 'day'){
					repeat.value = $('#mo-day-value').val();
					repeat_des = "매월 " + repeat.value + "일 마다 반복";
				} else {
					repeat.value_th = $('#mo-week-value-th').val();
					repeat.value_week = $('#mo-week-value-week').val();
					var value_th_txt = $("#mo-week-value-th option:checked").text();
					var value_week_txt = $("#mo-week-value-week option:checked").text();
					repeat_des = value_th_txt + " " + value_week_txt + " 마다 반복";
				}
				break;
			case 'YR':
				repeat.radio = $(':radio[name="radio05"]:checked').val();
				if(repeat.radio == 'month'){
					repeat.value = $('#yr-month-value').val();
					repeat_des = "매년 " + repeat.value + "월 마다 반복";
				} else {
					repeat.value_th = $('#yr-week-value-th').val();
					repeat.value_week = $('#yr-week-value-week').val();
					var value_th_txt = $("#yr-week-value-th option:checked").text();
					var value_week_txt = $("#yr-week-value-week option:checked").text();
					repeat_des = value_th_txt + " " + value_week_txt + " 마다 반복";
				}
				break;
			case 'NO':
				repeat_des = "반복 없음";
			default:
				break;
		}
		
		info.repeat_val = repeat;
		info.repeat_des = repeat_des;
		
		return info;
	}

	function dbTriggerInfo() {
		var info = {};
		
		info.isSuccess = true;
		
		info.db_type = $('#config_db_type').val();
		info.db_name = $('#config_db_name').val();
		info.con_host = $('#config_con_host').val();
		info.con_port = $('#config_con_port').val();
		info.admin_id = $('#config_admin_id').val();
		info.admin_pw = $('#config_admin_pw').val();
		
		if(info.db_type == 'mongodb') {
			info.collection_name = $('#mongodb_table_list').val();
			info.table_name = "";
			
			if(info.collection_name == null || info.collection_name == "") {
				info.isSuccess = false;
			}
		} else if(info.db_type == 'postgresql') {
			info.collection_name = "";
			info.table_name = $('#postgresql_table_list').val();
			
			if(info.table_name == null || info.table_name == "") {
				info.isSuccess = false;
			}
		} else if(info.db_type == 'mariadb') {
			info.collection_name = "";
			info.table_name = $('#mariadb_table_list').val();
			
			if(info.table_name == null || info.table_name == "") {
				info.isSuccess = false;
			}
		}
		
		info.insert_event_yn = "N";
		info.update_event_yn = "N";
		info.delete_event_yn = "N";
		
		switch($(":input:radio[name='eventType']:checked").val()) {
			case 'insert':
				info.insert_event_yn = "Y";
				break;
			case 'update':
				info.update_event_yn = "Y";
				break;
			case 'delete':
				info.delete_event_yn = "Y";
				break;
			default:
		}

		return info;
	}

	function initHttpData() {
		$('#trig_modal_api_name').val("");
	}

	function bindHttpEvent() {

	}

	function initDBData(isChangeDbType) {
		var dbTriggerModal = NtelsHB.Store.get("dbTriggerModal");
		dbTriggerModal.isValidDb = false;
		NtelsHB.Store.set('dbTriggerModal', dbTriggerModal, false);

		if(isChangeDbType !== false)
			$("#config_db_type").val("mongodb").change();

		$("#config_con_host").val("").closest("div.form-group").removeClass("form-danger-before");
		$("#config_con_port").val("").closest("div.form-group").removeClass("form-danger-before");
		$("#config_db_name").val("").closest("div.form-group").removeClass("form-danger-before");
		$("#config_admin_id").val("").closest("div.form-group").removeClass("form-danger-before");
		$("#config_admin_pw").val("").closest("div.form-group").removeClass("form-danger-before");
		$('#check_db_validation').attr('title', '접속 테스트').attr('disabled', false).html('접속 테스트');
		
		var items = ['mongodb', 'postgresql', 'mariadb', 'event'];
		for(var i in items) $('#' + items[i] + '_config_area').hide();

		$(":input:radio[name='eventType'][value='insert']").trigger("click");
	}

	function bindDBEvent() {
		$("#config_db_type").on('change', function() {
			$("[data-lst='db']").hide();
			$("[data-tab='" + $(this).val() + "'").show();
			initDBData(false);
		});
		$("#config_db_type").change();

		$("#config_con_host, #config_con_port, #config_db_name, #config_admin_id, #config_admin_pw").on("keyup", function() {
			var dbTriggerModal = NtelsHB.Store.get("dbTriggerModal");
			if(dbTriggerModal.isValidDb) initDBData();
		});
	}

	function initTimerData() {
		$("#bas_date_hms_date").val(moment(new Date()).format("YYYY-MM-DD"));
		$("#bas_date_hms_hour").val(0);
		$("#bas_date_hms_miniute").val(0);

		$(":input:radio[name='repeatSet'][value='NO']").click();
	}

	function initTimerRepeatData() {
		// 분
		$("#mn-value").val(1);
		// 시간
		$("#hr-value").val(1);
		// 매일
		$("#dy-day-value").val(1);
		$("#radio03-1").prop("checked", true);
		// 주별
		$(":input:checkbox[name='wk-value']").prop("checked", false);
		// 월별
		$("#radio04-1").prop("checked", true);
		$("#mo-day-value").val(1);
		$("#mo-week-value-th").val(1);
		$("#mo-week-value-week").val("MON");
		// 연간
		$("#radio05-1").prop("checked", true);
		$("#yr-month-value").val(1);
		$("#yr-week-value-th").val(1);
		$("#yr-week-value-week").val("MON");

		$("#end_date_hms_date").val(moment(new Date()).format("YYYY-MM-DD"));
		$("#end_date_hms_hour").val(0);
		$("#end_date_hms_minute").val(0);
		$('.datepicker_wrap .ele').attr('disabled', false);
		$("#chk_enddate").prop("checked", false);
	}

	function bindTimerEvent() {
		// 반복설정 제어
		$('[data-lst="admin"] li input').on('click', function() {
			initTimerRepeatData();
			var section = $(this).attr('data-btn');
			if(section === 'none')
				$('#repeatEndDate').hide();
			else
				$('#repeatEndDate').show();

			$('[data-lst="repeat"]').hide();
			$('[data-tab='+ section +']').show();
		});

		$('[data-btn="none"]').click();

		$('#chk_enddate').on('click', function() {
			if($('#chk_enddate').is(':checked')) {
				$('.datepicker_wrap .ele').attr('disabled', true);
			} else {
				$('.datepicker_wrap .ele').attr('disabled', false);
			};
		});

		$('#bas_date_hms_date').daterangepicker({
			singleDatePicker: true,
			locale: {
				format: 'YYYY-MM-DD'
			}
		});

		$('#end_date_hms_date').daterangepicker({
			singleDatePicker: true,
			locale: {
				format: 'YYYY-MM-DD'
			},
			drops: 'up'
		});
	}
</script>