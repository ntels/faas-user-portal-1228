<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
    				<!-- HTTP 상세 -->
					<div  data-lst="target" data-tab="target" style="display: block;">
						<section class="panel panel-bordered">
							<div class="panel-heading">
								<h3 class="panel-title">
									대상
								</h3>
							</div> <!-- // panel-heading -->
							<div class="panel-body">
								<div class="row padding-left-10">
									<div class="col-md-1">
										<div class="write-sub-title  blt-dot">
											조건
										</div>
									</div>
									<div class="col-md-11">
										<div class="grey-600 font-size-13">
											{{#if (eq target_result_cd 'success')}}성공{{/if}}
											{{#if (eq target_result_cd 'fail')}}실패{{/if}}
										</div>
									</div>
								</div>
								<div class="row padding-left-10">
									<div class="col-md-1">
										<div class="write-sub-title  blt-dot">
											함수명
										</div>
									</div>
									<div class="col-md-11">
										<div class="grey-600 font-size-13">
											{{dsp_name}}
										</div>
									</div>
								</div>
								<!--
								<div class="row padding-left-10">
									<div class="col-md-1">
										<div class="write-sub-title  blt-dot">
											호출 URL
										</div>
									</div>
									<div class="col-md-11">
										<div class="grey-600 font-size-13">
											{{target_url}}
										</div>
									</div>
								</div>
								-->
							</div> <!-- // panel-body -->
						</section>
					</div>

				