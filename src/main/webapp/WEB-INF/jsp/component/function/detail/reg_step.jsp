<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
    
							<section class="panel panel-bordered">
								<div class="panel-heading">
									<h3 class="panel-title">
										함수 등록 단계
									</h3>
								</div> <!-- // panel-heading -->
								{{#if (eq step 'STEP1')}}
								<div class="panel-body">
									<div class="info_txt">
										{{#if (eq stat 'ERROR')}}
										전체 5단계 중 <strong class="f-error">1단계 실패</strong>
										{{else}} 
										전체 5단계 중 <strong class="f-reg">1단계 진행중</strong> 
										{{/if}}
									</div>
									<ul class="result_area_2 margin-top-30">
										{{#if (eq stat 'ERROR')}}
										<li class="fail_wrap">
											<span class="txt_wrap">
												<span class="tit">1단계 : 소스 저장</span>
												<div class="state">
													<div class="state-round fail"></div>
													실패​
												</div>
											</span>
										</li>
										{{else}}
										<li class="loading_wrap">
											<div class="circle">
												<div class="spinner-border spinner-info" role="status">
													<span class="sr-only">Loading...</span>
												</div>
											</div>
											<span class="txt_wrap">
												<span class="tit">1단계 : 소스 저장</span>
												<div class="state">
													<div class="state-round loading"></div>
													진행중​
												</div>
											</span>
										</li>
										{{/if}}
										<li class="waiting_wrap">
											<span class="txt_wrap">
												<span class="tit">2단계 : 체크 아웃</span>
												<div class="state">
													<div class="state-round waiting"></div>
													대기​
												</div>
											</span>
										</li>
										<li class="waiting_wrap">
											<span class="txt_wrap">
												<span class="tit">3단계 : 이미지 빌드</span>
												<div class="state">
													<div class="state-round waiting"></div>
													대기​
												</div>
											</span>
										</li>
										<li class="waiting_wrap">
											<span class="txt_wrap">
												<span class="tit">4단계 : 이미지 배포</span>
												<div class="state">
													<div class="state-round waiting"></div>
													대기​
												</div>
											</span>
										</li>
										<li class="waiting_wrap">
											<span class="txt_wrap">
												<span class="tit">5단계 : 서비스 배포</span>
												<div class="state">
													<div class="state-round waiting"></div>
													대기​
												</div>
											</span>
										</li>
								</div> <!-- // panel-body -->
								{{/if}}
								
								{{#if (eq step 'STEP2')}}
								<div class="panel-body">
									<div class="info_txt">
										{{#if (eq stat 'ERROR')}}
										전체 5단계 중 <strong class="f-error">2단계 실패</strong>
										{{else}} 
										전체 5단계 중 <strong class="f-reg">2단계 진행중</strong> 
										{{/if}}
									</div>
									<ul class="result_area_2 margin-top-30">
										<li class="success_wrap">
											<span class="txt_wrap">
												<span class="tit">1단계 : 소스 저장</span>
												<div class="state">
													<div class="state-round success"></div>
													완료​
												</div>
											</span>
										</li>
										{{#if (eq stat 'ERROR')}}
										<li class="fail_wrap">
											<span class="txt_wrap">
												<span class="tit">2단계 : 체크 아웃</span>
												<div class="state">
													<div class="state-round fail"></div>
													실패​
												</div>
											</span>
										</li>
										{{else}}
										<li class="loading_wrap">
											<div class="circle">
												<div class="spinner-border spinner-info" role="status">
													<span class="sr-only">Loading...</span>
												</div>
											</div>
											<span class="txt_wrap">
												<span class="tit">2단계 : 체크 아웃</span>
												<div class="state">
													<div class="state-round loading"></div>
													진행중​
												</div>
											</span>
										</li>
										{{/if}}
										<li class="waiting_wrap">
											<span class="txt_wrap">
												<span class="tit">3단계 : 이미지 빌드</span>
												<div class="state">
													<div class="state-round waiting"></div>
													대기​
												</div>
											</span>
										</li>
										<li class="waiting_wrap">
											<span class="txt_wrap">
												<span class="tit">4단계 : 이미지 배포</span>
												<div class="state">
													<div class="state-round waiting"></div>
													대기​
												</div>
											</span>
										</li>
										<li class="waiting_wrap">
											<span class="txt_wrap">
												<span class="tit">5단계 : 서비스 배포</span>
												<div class="state">
													<div class="state-round waiting"></div>
													대기​
												</div>
											</span>
										</li>
								</div> <!-- // panel-body -->
								{{/if}}
								
								{{#if (eq step 'STEP3')}}
								<div class="panel-body">
									<div class="info_txt">
										{{#if (eq stat 'ERROR')}}
										전체 5단계 중 <strong class="f-error">3단계 실패</strong>
										{{else}} 
										전체 5단계 중 <strong class="f-reg">3단계 진행중</strong> 
										{{/if}}
									</div>
									<ul class="result_area_2 margin-top-30">
										<li class="success_wrap">
											<span class="txt_wrap">
												<span class="tit">1단계 : 소스 저장</span>
												<div class="state">
													<div class="state-round success"></div>
													완료​
												</div>
											</span>
										</li>
										<li class="success_wrap">
											<span class="txt_wrap">
												<span class="tit">2단계 : 체크 아웃</span>
												<div class="state">
													<div class="state-round success"></div>
													완료​
												</div>
											</span>
										</li>
										{{#if (eq stat 'ERROR')}}
										<li class="fail_wrap">
											<span class="txt_wrap">
												<span class="tit">3단계 : 이미지 빌드</span>
												<div class="state">
													<div class="state-round fail"></div>
													실패​
												</div>
											</span>
										</li>
										{{else}}
										<li class="loading_wrap">
											<div class="circle">
												<div class="spinner-border spinner-info" role="status">
													<span class="sr-only">Loading...</span>
												</div>
											</div>
											<span class="txt_wrap">
												<span class="tit">3단계 : 이미지 빌드</span>
												<div class="state">
													<div class="state-round loading"></div>
													진행중​
												</div>
											</span>
										</li>
										{{/if}}
										<li class="waiting_wrap">
											<span class="txt_wrap">
												<span class="tit">4단계 : 이미지 배포</span>
												<div class="state">
													<div class="state-round waiting"></div>
													대기​
												</div>
											</span>
										</li>
										<li class="waiting_wrap">
											<span class="txt_wrap">
												<span class="tit">5단계 : 서비스 배포</span>
												<div class="state">
													<div class="state-round waiting"></div>
													대기​
												</div>
											</span>
										</li>
								</div> <!-- // panel-body -->
								{{/if}}
								
								{{#if (eq step 'STEP4')}}
								<div class="panel-body">
									<div class="info_txt">
										{{#if (eq stat 'ERROR')}}
										전체 5단계 중 <strong class="f-error">4단계 실패</strong>
										{{else}} 
										전체 5단계 중 <strong class="f-reg">4단계 진행중</strong> 
										{{/if}}
									</div>
									<ul class="result_area_2 margin-top-30">
										<li class="success_wrap">
											<span class="txt_wrap">
												<span class="tit">1단계 : 소스 저장</span>
												<div class="state">
													<div class="state-round success"></div>
													완료​
												</div>
											</span>
										</li>
										<li class="success_wrap">
											<span class="txt_wrap">
												<span class="tit">2단계 : 체크 아웃</span>
												<div class="state">
													<div class="state-round success"></div>
													완료​
												</div>
											</span>
										</li>
										<li class="success_wrap">
											<span class="txt_wrap">
												<span class="tit">3단계 : 이미지 빌드</span>
												<div class="state">
													<div class="state-round success"></div>
													완료​
												</div>
											</span>
										</li>
										{{#if (eq stat 'ERROR')}}
										<li class="fail_wrap">
											<span class="txt_wrap">
												<span class="tit">4단계 : 이미지 배포</span>
												<div class="state">
													<div class="state-round fail"></div>
													실패​
												</div>
											</span>
										</li>
										{{else}}
										<li class="loading_wrap">
											<div class="circle">
												<div class="spinner-border spinner-info" role="status">
													<span class="sr-only">Loading...</span>
												</div>
											</div>
											<span class="txt_wrap">
												<span class="tit">4단계 : 이미지 배포</span>
												<div class="state">
													<div class="state-round loading"></div>
													진행중​
												</div>
											</span>
										</li>
										{{/if}}
										<li class="waiting_wrap">
											<span class="txt_wrap">
												<span class="tit">5단계 : 서비스 배포</span>
												<div class="state">
													<div class="state-round waiting"></div>
													대기​
												</div>
											</span>
										</li>
								</div> <!-- // panel-body -->
								{{/if}}
								
								{{#if (eq step 'STEP5')}}
								<div class="panel-body">
									<div class="info_txt">
										{{#if (eq stat 'ERROR')}}
										전체 5단계 중 <strong class="f-error">5단계 실패</strong>
										{{else}} 
										전체 5단계 중 <strong class="f-reg">5단계 진행중</strong> 
										{{/if}}
									</div>
									<ul class="result_area_2 margin-top-30">
										<li class="success_wrap">
											<span class="txt_wrap">
												<span class="tit">1단계 : 소스 저장</span>
												<div class="state">
													<div class="state-round success"></div>
													완료​
												</div>
											</span>
										</li>
										<li class="success_wrap">
											<span class="txt_wrap">
												<span class="tit">2단계 : 체크 아웃</span>
												<div class="state">
													<div class="state-round success"></div>
													완료​
												</div>
											</span>
										</li>
										<li class="success_wrap">
											<span class="txt_wrap">
												<span class="tit">3단계 : 이미지 빌드</span>
												<div class="state">
													<div class="state-round success"></div>
													완료​
												</div>
											</span>
										</li>
										<li class="success_wrap">
											<span class="txt_wrap">
												<span class="tit">4단계 : 이미지 배포</span>
												<div class="state">
													<div class="state-round success"></div>
													완료​
												</div>
											</span>
										</li>
										{{#if (eq stat 'ERROR')}}
										<li class="fail_wrap">
											<span class="txt_wrap">
												<span class="tit">5단계 : 서비스 배포</span>
												<div class="state">
													<div class="state-round fail"></div>
													실패​
												</div>
											</span>
										</li>
										{{else}}
										<li class="loading_wrap">
											<div class="circle">
												<div class="spinner-border spinner-info" role="status">
													<span class="sr-only">Loading...</span>
												</div>
											</div>
											<span class="txt_wrap">
												<span class="tit">5단계 : 서비스 배포</span>
												<div class="state">
													<div class="state-round loading"></div>
													진행중​
												</div>
											</span>
										</li>
										{{/if}}
								</div> <!-- // panel-body -->
								{{/if}}
							</section>