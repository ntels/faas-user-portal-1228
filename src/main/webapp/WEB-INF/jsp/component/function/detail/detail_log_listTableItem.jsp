<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
									<tr>
										<td class="text-center">{{reg_date_hms}}​</td>
										<td class="text-center">{{log_cd_nm}}​</td>
										<td class="ellipsis-2 block">
										{{#if (or (eq log_cd "ERROR") (eq log_dtl_cd "REVISION")) }}
											{{#if (eq log_cd "ERROR")}}
											<a href="javascript:void(0);" onclick="g_func.detailLog({{function_log_id}});return false;">
												<span>{{logtext}}</span>
											</a>
											{{/if}}
											{{#if (eq log_dtl_cd "REVISION")}}
											<a href="javascript:void(0);" onclick="g_func.diffView('{{function_id}}','{{revision_id}}','{{function_log_id}}');return false;">
												<span>{{logtext}}</span>
											</a>
											{{/if}}
										{{else}}
												<span>{{logtext}}</span>
										{{/if}}
										</td>
										{{#if (eq log_cd "ERROR") }}
										<td class="text-center">-</td>
										{{/if}}
										{{#if (eq log_cd "MODIFY") }}
										<td class="text-center">{{reg_usr}}</td>
										{{/if}}
										{{#if (eq log_cd "INFO") }}
										<td class="text-center">{{reg_usr}}</td>
										{{/if}}
									</tr>
