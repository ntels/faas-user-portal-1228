<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>

<input type="hidden" id="repeat_tp" value="{{repeat_tp}}">
<input type="hidden" id="repeat_val" value="{{repeat_val}}">

    				<!-- 타이머 상세 -->
					<div data-lst="trigger" data-tab="Timer" style="display: block;">
						<section class="panel panel-bordered">
							<div class="panel-heading">
								<h3 class="panel-title">
									타이머
								</h3>
							</div> <!-- // panel-heading -->
							<div class="panel-body">
								<div class="row padding-left-10">
									<div class="col-md-1">
										<div class="write-sub-title blt-dot">
											기준 일시
										</div>
									</div>
									<div class="col-md-5">
										<div class="grey-600 font-size-13">
											{{bas_date_hms}}​
										</div>
									</div>
									<div class="col-md-1">
										<div class="write-sub-title blt-dot">
											반복
										</div>
									</div>
									<div class="col-md-5">
										<span class="img-repeat">
											{{#if (eq repeat_tp 'NO')}}
											<span class="none">
												<i></i><span class="grey-600 font-size-13 margin-left-10">반복 없음</span>
											</span>
											{{/if}}
											{{#if (eq repeat_tp 'MN')}}
											<span class="minute">
												<i></i><span class="grey-600 font-size-13 margin-left-10">분</span>
											</span>
											{{/if}}
											{{#if (eq repeat_tp 'HR')}}
											<span class="hour">
												<i></i><span class="grey-600 font-size-13 margin-left-10">시간</span>
											</span>
											{{/if}}
											{{#if (eq repeat_tp 'DY')}}
											<span class="day">
												<i></i><span class="grey-600 font-size-13 margin-left-10">매일</span>
											</span>
											{{/if}}
											{{#if (eq repeat_tp 'WK')}}
											<span class="week">
												<i></i><span class="grey-600 font-size-13 margin-left-10">매주</span>
											</span>
											{{/if}}
											{{#if (eq repeat_tp 'MO')}}
											<span class="month">
												<i></i><span class="grey-600 font-size-13 margin-left-10">매월</span>
											</span>
											{{/if}}
											{{#if (eq repeat_tp 'YR')}}
											<span class="year">
												<i></i><span class="grey-600 font-size-13 margin-left-10">매년</span>
											</span>
											{{/if}}

										</span>
									</div>
								</div>
								<div class="row padding-left-10">
									<div class="col-md-1">
										<div class="write-sub-title blt-dot">
											반복 설정
										</div>
									</div>
									<div class="col-md-11">
										<div class="grey-600 font-size-13">
											<p>{{repeat_des}}​</p>
										</div>
									</div>
								</div>
								{{#if (neq repeat_tp 'NO')}}
								<div class="row padding-left-10">
									<div class="col-md-1">
										<div class="write-sub-title blt-dot">
											반복 종료일
										</div>
									</div>
									<div class="col-md-11">
										<div class="grey-600 font-size-13">
											{{end_date_hms}}​
										</div>
									</div>
								</div>
								{{/if}}
							</div> <!-- // panel-body -->
						</section>
					</div>

<script>
</script>