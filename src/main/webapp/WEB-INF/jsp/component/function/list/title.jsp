<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
    
<div class="page-title">
	<div id="spot">
		<div class="page-info">
			함수 관리 목록
		</div>
		<div class="page-location">
			<span class="home">
				<i class="xi-home-o"></i>
				HUBPoP
			</span>
			<span>함수 관리​</span>
			<span>함수 관리 목록</span>
		</div>
		<a href="#" class="btn-refresh" id="reloadBtn">
			<i class="xi-renew"></i>
			<p class="sr-only">새로고침</p>
		</a>
	</div>
</div>