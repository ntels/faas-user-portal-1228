<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
    
<div class="col-md-4">
	<a href="#" class="color-no" onclick="loadPage('layout/function/detail.do?function_id={{function_id}}&name={{name}}');">
		<div class="card card-type2 card-hover-outline-primary">
			<div class="card-header">
				<div class="card-info-area">
					<div class="text-box">
						<div class="top">
							<div class="clearfix">
								<span class="card-title">
									<b>
										{{dsp_name}}
									</b>
								</span>
							</div>
							<div class="clearfix">
								<span class="lagn">
								
									{{#if (eq runtime 'nodejs')}}
									<span class="img-size15 img-nodejs"></span>
									{{runtime}} {{version}}
									
									{{else}}
									
									<span class="img-size15 img-python"></span>
									{{runtime}} {{version}}
									{{/if}}
									
								</span>
								<span class="state pull-right">
								
									{{#if (eq stat_cd 'INIT')}}
									<div class="state-round ready"></div>
									미배포
									{{else if (eq stat_cd 'REG')}}
									<div class="state-round reg"></div>
									배포 중
									{{else if (eq stat_cd 'NORMAL')}}
									<div class="state-round normal"></div>
									배포 완료
									{{else if (eq stat_cd 'ERROR')}}
									<div class="state-round error"></div>
									배포 실패
									{{else if (eq stat_cd 'SYNC_ERROR')}}
									<div class="state-round error"></div>
									배포 정보 체크
									{{else if (eq stat_cd 'DISABLE')}}
									<div class="state-round ready"></div>
									비활성
									{{else}}
									<div class="state-round"></div>
									알 수 없음
									{{/if}}
									
								</span>
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="card-body">
				<div class="ellipsis">
					{{des}} 
				</div>
			</div>
			<div class="card-body">
				<ul class="list-dot">
					<li>
						<b>트리거 연결  건 수 : </b>
						<span>
							{{trigger_count}}건
						</span>
					</li>
					<li>
						<!-- TODO : 대상 연결 건 수 -->
						<b>대상 연결 건 수 : </b>
						<span>
							{{target_count}}건
						</span>
					</li>
					<li>
						<b>등록 일시 : </b>
						<span>
							{{reg_date_hms}}
						</span>
					</li>
					<li>
						<b>등록자 : </b>
						<span>
							{{reg_usr}}
						</span>
					</li>
				</ul>
			</div>
		</div>
	</a>
</div>