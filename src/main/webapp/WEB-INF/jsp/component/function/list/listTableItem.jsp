<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
    
<tr>
	<td>
		<a href="#" onclick="loadPage('layout/function/detail.do?function_id={{function_id}}&name={{name}}');">{{dsp_name}}</a>
		<div class="ellipsis-2">{{des}}</div>
	</td>
	<td class="text-center">
		<span class="state">
			
			{{#if (eq stat_cd 'INIT')}}
			<div class="state-round ready"></div>
			미배포
			{{else if (eq stat_cd 'REG')}}
			<div class="state-round reg"></div>
			배포 중
			{{else if (eq stat_cd 'NORMAL')}}
			<div class="state-round normal"></div>
			배포 완료
			{{else if (eq stat_cd 'ERROR')}}
			<div class="state-round error"></div>
			배포 실패
			{{else if (eq stat_cd 'SYNC_ERROR')}}
			<div class="state-round error"></div>
			배포 정보 체크
			{{else if (eq stat_cd 'DISABLE')}}
			<div class="state-round ready"></div>
			비활성
			{{else}}
			<div class="state-round"></div>
			알 수 없음
			{{/if}}
			
		</span>
	</td>
	<td class="text-center">{{runtime}} {{version}}</td>
	<td class="text-center">{{trigger_count}}건​</td>
	<!-- TODO : 대상 연결 건 수 -->
	<td class="text-center">{{target_count}}건​</td>
	<td class="text-center">{{reg_date_hms}}​</td>
	<td class="text-center">{{reg_usr}}​</td>
</tr>