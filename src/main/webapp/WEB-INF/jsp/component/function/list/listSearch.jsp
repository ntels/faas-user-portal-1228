<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
    
<div class="card-input-wrap">
	<div class="row">
		<div class="col-md-6">
			<div class="ciw-tit">
				<b class="tit">제목 <span class="sub-tit">({{totalCnt}})</span></b>
			</div>
		</div>
		<div class="col-md-6">
			<div class="text-right">
				<div class="form-group inline-block margin-bottom-0">
					<select id="sch_runtime" class="form-control">
						<option value="">런타임 유형​</option>
						<option value="nodejs">Node.js</option>
						<option value="python">Python​</option>
					</select>
				</div>
				<div class="form-group inline-block margin-bottom-0">
					<select id="order_by" class="form-control">
						<option value="CREATE">등록일 순​</option>
						<option value="MODIFY">수정일 순</option>
						<option value="NAME">이름 순​​</option>
					</select>
				</div>
				
				{{component "viewChanger"}}
				
			</div>
		</div>
	</div>
</div>