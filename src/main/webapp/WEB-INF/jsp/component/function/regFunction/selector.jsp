<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
    
 <div class="write-sub-title">
	<b class="caution-star">*</b> 등록 방식​
</div>

{{#if isViewNew}}
<div class="row">
	<div class="col-md-6">
		<label for="card01" class="input-card-type">
			<input type="radio" id="card01" name="card1" class="card-input-element" checked>
			<div class="card-input">
				<div class="card-type4">
					<span class="img-size50 img-newreg"></span>
					<div class="text-area">
						<div class="title">신규 등록​</div>
						<p>
							개발자가 직접 함수를 등록하여 자유롭게 테스트 해 보실 수 있습니다.​
						</p>
					</div>
				</div>
			</div>
		</label>
	</div>
	<div class="col-md-6" onclick="loadPage('layout/template/list.do');">
		<label for="card02" class="input-card-type">
			<input type="radio" id="card02" name="card1" class="card-input-element">
			<div class="card-input">
				<div class="card-type4">
					<span class="img-size50 img-template"></span>
					<div class="text-area">
						<div class="title">템플릿 사용​</div>
						<p>
							hub-PoP에서 제공하는 다양한 함수 템플릿을 선택하여 간편하게 등록합니다.​
						</p>
					</div>
				</div>
			</div>
		</label>
	</div>
</div>
{{else}}
<div class="row">
	<div class="col-md-6" onclick="loadPage('layout/function/regFunction.do');">
		<label for="card01" class="input-card-type">
			<input type="radio" id="card01" name="card1" class="card-input-element">
			<div class="card-input">
				<div class="card-type4">
					<span class="img-size50 img-newreg"></span>
					<div class="text-area">
						<div class="title">신규 등록​</div>
						<p>
							개발자가 직접 함수를 등록하여 자유롭게 테스트 해 보실 수 있습니다.​
						</p>
					</div>
				</div>
			</div>
		</label>
	</div>
	<div class="col-md-6">
		<label for="card02" class="input-card-type">
			<input type="radio" id="card02" name="card1" class="card-input-element" checked>
			<div class="card-input">
				<div class="card-type4">
					<span class="img-size50 img-template"></span>
					<div class="text-area">
						<div class="title">템플릿 사용​</div>
						<p>
							hub-PoP에서 제공하는 다양한 함수 템플릿을 선택하여 간편하게 등록합니다.​
						</p>
					</div>
				</div>
			</div>
		</label>
	</div>
</div>
{{/if}}