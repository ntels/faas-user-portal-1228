<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<script>

$(document).ready(function(){
	$("#rightContent").show();
	
});

function tableprev(){
	switchContent("${pageContext.request.contextPath}", "/faas/?menu=/layout/resource/tablePrev.do");
}
</script>
		<div class="content">

			<!-- Tab 영역 -->
			<div class="row_wrap">
				<div class="tabWrap type2">		
						<ul class="nav nav-tabs">
							<li><a href="javascript:void(0);" onclick='switchContent("${pageContext.request.contextPath}", "/faas/?menu=/layout/resource/main2.do")'  title="MongoDB">MongoDB</a></li>
							<li class="active"><a href="javascript:void(0);" onclick='switchContent("${pageContext.request.contextPath}", "/faas/?menu=/layout/resource/postgreMain2.do")'  title="Postgre​">Postgre​</a></li>
						</ul>
				</div><!--//tabWrap-->
			</div><!--//row_wrap-->
			<div class="row_wrap">
				<ul class="catalog api drop_wrap side_wrap icon">
					<li>
						<div>
							<span class="img postgre">
							</span>
							<span class="cnt">
								<span>
									<span class="name">Postgre​</span>
								</span>
								<div class="txtlist" style="margin-top: 3px;">
									<dl class="tb_style">
										<dt style="width: 80px;">접속 URL </dt>
										<dd>219.250.188.229:1521/orcl​</dd>
										<dt style="width: 50px;">계정 </dt>
										<dd>
											ID : <span>p89mongodb</span>  &nbsp; &nbsp; &nbsp;  PW : <span>abcd1234#$%​</span>
										</dd>
										<dd>
											<button type="button" class="btn_inner mgL20">패스워드 변경</button>
										</dd>
									</dl>
								</div>
							</span>
						</div>
					</li>
				</ul>
			</div><!--//row_wrap-->
			
			<div class="row_wrap">
				<div class="ncz_panel">
					<div class="panel_head">
						<span class="tit">테이블 정보</span>
					</div>
					<div class="panel_cnt">
						
						<div class="row_wrap panel_add" style="height: 150px;">
							<!-- 버튼 영역 -->
							<button type="button" class="btn btn_white large icon_07" onClick="location.href='SC_FS_04_013.html'">테이블 등록​​</button>

							<!-- 가이드 영역 -->
							<div class="guide_area">
								<span class="btn_guide">설치된 Postgre에 테이블을 등록할 수 있습니다.​​</span>
							</div>
						</div><!--//row_wrap-->
						
						<table class="ncz_tbl default">
							<caption class="hide">테이블 정보 상단제목</caption>
							<colgroup>
								<col style="width:350px;">
								<col style="" />
								<col style="width:270px;">
								<col style="width:120px;">
								<col style="width:100px;">
							</colgroup>
							<thead>
								<tr>
									<th scope="col" class="text-left">테이블 이름</th>
									<th scope="col" class="text-left">설명​</th>
									<th scope="col">등록 일시​</th>
									<th scope="col">등록자​</th>
									<th scope="col">관리​</th>
								</tr>
							</thead>
						</table>
						<div class="scroll-pane y_scroll_5">
							<table class="ncz_tbl default">
								<caption class="hide">테이블 정보 리스트</caption>
								<colgroup>
									<col style="width:350px;">
									<col style="" />
									<col style="width:250px;">
									<col style="width:150px;">
									<col style="width:70px;">
								</colgroup>
								<tbody>							
									<tr>
										<td class="text-left"><a href="javascript:void(0);" onclick="tableprev();" class="underline">user</a></td>
										<td class="text-left">테이블 설명 출력​</td>
										<td>2020-01-02 00:00​</td>
										<td>홍길동​</td>
										<td>
											<div class="dropdown">
												<button id="option" type="button" class="btn only-ico ico-dot" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" title="관리">
													<span class="ico">관리</span>
												</button>
												<ul class="dropdown-menu short" role="option" aria-labelledby="option">
													<li>
														<a href="SC_FS_04_021.html">삭제​</a>
													</li>
													<li>
														<a href="SC_FS_04_014.html">수정</a>
													</li>
												</ul>
											</div>
										</td>
									</tr>	
									<tr>
										<td class="text-left"><a href="javascript:void(0);" onclick="tableprev();" class="underline">password</a></td>
										<td class="text-left">테이블 설명 출력​</td>
										<td>2020-01-02 00:00​</td>
										<td>홍길동​</td>
										<td>
											<div class="dropdown">
												<button id="option" type="button" class="btn only-ico ico-dot" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" title="관리">
													<span class="ico">관리</span>
												</button>
												<ul class="dropdown-menu short" role="option" aria-labelledby="option">
													<li>
														<a href="SC_FS_04_021.html">삭제​</a>
													</li>
													<li>
														<a href="sc_fs_04_014.html">수정</a>
													</li>
												</ul>
											</div>
										</td>
									</tr>								
									<tr>
										<td class="text-left"><a href="javascript:void(0);" onclick="tableprev();" class="underline">image​</a></td>
										<td class="text-left ellipsis">테이블 설명이 길어 한 불에 표시되지 않을 경우 말 줄임테이블 설명이 길어 한 불에 표시되지 않을 경우 말 줄임​</td>
										<td>2020-01-02 00:00​</td>
										<td>홍길동​</td>
										<td>
											<div class="dropdown">
												<button id="option" type="button" class="btn only-ico ico-dot" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" title="관리">
													<span class="ico">관리</span>
												</button>
												<ul class="dropdown-menu short" role="option" aria-labelledby="option">
													<li>
														<a href="SC_FS_04_021.html">삭제​</a>
													</li>
													<li>
														<a href="sc_fs_04_014.html">수정</a>
													</li>
												</ul>
											</div>
										</td>
									</tr>							
									<tr>
										<td class="text-left"><a href="javascript:void(0);" onclick="tableprev();" class="underline">user</a></td>
										<td class="text-left">테이블 설명 출력​</td>
										<td>2020-01-02 00:00​</td>
										<td>홍길동​</td>
										<td>
											<div class="dropdown">
												<button id="option" type="button" class="btn only-ico ico-dot" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" title="관리">
													<span class="ico">관리</span>
												</button>
												<ul class="dropdown-menu short" role="option" aria-labelledby="option">
													<li>
														<a href="SC_FS_04_021.html">삭제​</a>
													</li>
													<li>
														<a href="sc_fs_04_014.html">수정</a>
													</li>
												</ul>
											</div>
										</td>
									</tr>							
									<tr>
										<td class="text-left"><a href="javascript:void(0);" onclick="tableprev();" class="underline">password</a></td>
										<td class="text-left">테이블 설명 출력​</td>
										<td>2020-01-02 00:00​</td>
										<td>홍길동​</td>
										<td>
											<div class="dropdown">
												<button id="option" type="button" class="btn only-ico ico-dot" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" title="관리">
													<span class="ico">관리</span>
												</button>
												<ul class="dropdown-menu short" role="option" aria-labelledby="option">
													<li>
														<a href="SC_FS_04_021.html">삭제​</a>
													</li>
													<li>
														<a href="sc_fs_04_014.html">수정</a>
													</li>
												</ul>
											</div>
										</td>
									</tr>							
								</tbody>
							</table>
						</div>
					</div>
				</div>
			</div><!--//row_wrap-->
		</div>
		<!-- //content -->