<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<script>

$(document).ready(function(){
	$("#rightContent").show();

});

function collectionAddCancel(){
	switchContent("${pageContext.request.contextPath}", "/faas/?menu=/layout/resource/main2.do");
}
</script>
		<div class="content">

				<!-- Tab 영역 -->
				<div class="row_wrap">
					<div class="tabWrap type2">	
						<ul class="nav nav-tabs">
							<li><a href="javascript:void(0);" onclick='switchContent("${pageContext.request.contextPath}", "/faas/?menu=/layout/resource/main2.do")'  title="MongoDB">MongoDB</a></li>
							<li class="active"><a href="javascript:void(0);" onclick='switchContent("${pageContext.request.contextPath}", "/faas/?menu=/layout/resource/postgreMain2.do")'  title="Postgre​">Postgre​</a></li>
						</ul>
					</div><!--//tabWrap-->
				</div><!--//row_wrap-->
 
				<div class="row_wrap">
					<ul class="catalog api drop_wrap side_wrap icon">
						<li>
							<div>
								<span class="img postgre">
								</span>
								<span class="cnt">
									<span>
										<span class="name">Postgre​</span>
									</span>
									<div class="txtlist" style="margin-top: 3px;">
										<dl class="tb_style">
											<dt style="width: 80px;">접속 URL </dt>
											<dd>219.250.188.229:1521/orcl​</dd>
											<dt style="width: 50px;">계정 </dt>
											<dd>
												ID : <span>p89mongodb</span>  &nbsp; &nbsp; &nbsp;  PW : <span>abcd1234#$%​</span>
											</dd>
											<dd>
												<button type="button" class="btn_inner mgL20">패스워드 변경</button>
											</dd>
										</dl>
									</div>
									<div class="side_item">
										<div class="dropdown" style="margin-top: 3px;">
											<button type="button" class="btn only-ico ico-dot" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" title="관리">
												<span class="ico">관리</span>
											</button>
											<ul class="dropdown-menu short md">
												<li>
													<a href="SC_FS_04_021.html">삭제</a>
												</li>
											</ul>
										</div>							
									</div>
								</span>
							</div>
						</li>
					</ul>
				</div><!--//row_wrap-->
				
				<div class="row_wrap">
					<div class="ncz_panel">
						<div class="panel_head poz_r">
							<span class="tit">
								<a href="SC_FS_04_011.html"><i class="icoType1 ico-prev"></i></a>
								<h4>칼럼 정보​​</h4>​
							</span>
							<div class="right_area">
								<div class="dropdown" style="margin-top: 7px;">
									<button type="button" class="btn only-ico ico-dot" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" title="관리">
										<span class="ico">관리</span>
									</button>
									<ul class="dropdown-menu short md">
										<li>
											<a href="SC_FS_04_021.html">삭제</a>
										</li>
										<li>
											<a href="SC_FS_04_014.html">수정​</a>
										</li>
									</ul>
								</div>
							</div>
						</div>
						<div class="panel_cnt">
							<div class="cBox type1">
								<div class="cBox-cnt bullet">
									<p class="insert_txt">테이블 이름​ : <span>user</span></p>
									<p class="insert_txt">설명 : <span>테이블 설명이 출력됩니다. 최대 50byte​</span>​</p>
								</div>
							</div>

							<table class="ncz_tbl default mgT30">
								<caption class="hide">칼럼 정보 리스트 타이틀</caption>
								<colgroup>
									<col style="" />
									<col style="" />
									<col style="" />
									<col style="" />
									<col style="" />
									<col style="" />
									<col style="" />
									<col style="" />
								</colgroup>
								<thead>
									<tr>
										<th scope="col">칼럼 이름 (국문)</th>
										<th scope="col">칼럼 이름 (영문)</th>
										<th scope="col">설명</th>
										<th scope="col">칼럼 유형</th>
										<th scope="col">칼럼 길이</th>
										<th scope="col">PK 여부</th>
										<th scope="col">Null 여부</th>
										<th scope="col">사용 여부</th>
									</tr>
								</thead>
							</table>
							<div class="scroll-pane y_scroll_5">
								<table class="ncz_tbl default">
									<caption class="hide">칼럼 정보 리스트</caption>
									<colgroup>
										<col style="" />
										<col style="" />
										<col style="" />
										<col style="" />
										<col style="" />
										<col style="" />
										<col style="" />
										<col style="" />
									</colgroup>
									<tbody>
										<tr>
											<td>개발자 이름</td>
											<td>Development Name</td>
											<td>개발자 이름</td>
											<td>string</td>
											<td>50</td>
											<td></td>
											<td>N</td>
											<td>Y</td>
										</tr>
										<tr>
											<td>주소</td>
											<td>Location</td>
											<td>수집된 지역 주소</td>
											<td>string</td>
											<td>100</td>
											<td></td>
											<td>Y</td>
											<td>Y</td>
										</tr>
										<tr>
											<td>관리 번호</td>
											<td>Admin Number</td>
											<td>관리 번호</td>
											<td>int</td>
											<td>30</td>
											<td>PK</td>
											<td>N</td>
											<td>Y</td>
										</tr>
										<tr>
											<td>청구 번호</td>
											<td>Bill ID</td>
											<td>청구 번호</td>
											<td>int</td>
											<td>20</td>
											<td></td>
											<td>N</td>
											<td>Y</td>
										</tr>
										<tr>
											<td>청구서 발행일</td>
											<td>Revenue Month</td>
											<td>청구서 발행일</td>
											<td>date</td>
											<td>10</td>
											<td></td>
											<td>Y</td>
											<td>Y</td>
										</tr>
										<tr>
											<td>청구서 발행일</td>
											<td>Revenue Month</td>
											<td>청구서 발행일</td>
											<td>date</td>
											<td>10</td>
											<td></td>
											<td>Y</td>
											<td>Y</td>
										</tr>
										<tr>
											<td>청구서 발행일</td>
											<td>Revenue Month</td>
											<td>청구서 발행일</td>
											<td>date</td>
											<td>10</td>
											<td></td>
											<td>Y</td>
											<td>Y</td>
										</tr>
										<tr>
											<td>청구서 발행일</td>
											<td>Revenue Month</td>
											<td>청구서 발행일</td>
											<td>date</td>
											<td>10</td>
											<td></td>
											<td>Y</td>
											<td>Y</td>
										</tr>
										<tr>
											<td>청구서 발행일</td>
											<td>Revenue Month</td>
											<td>청구서 발행일</td>
											<td>date</td>
											<td>10</td>
											<td></td>
											<td>Y</td>
											<td>Y</td>
										</tr>								
									</tbody>
								</table>
							</div>
						</div>
					</div>
				</div><!--//row_wrap-->

				<div class="row_wrap">
					<div class="ncz_panel">
						<div class="panel_head">
							<span class="tit">데이터 미리보기</span>
							<div class="right_area">
								<select class="inp wL">
									<option>최근 데이터 10건</option>
									<option>최근 데이터 20건</option>
									<option>최근 데이터 30건</option>
									<option>최근 데이터 50건</option>
									<option>최근 데이터 100건</option>
								</select>
								<div class="btnArea">
									<button type="button" class="btn_excel">엑셀 파일 다운로드</button>
								</div>
							</div>
						</div>
						<div class="panel_cnt">
							<div class="scroll_5 scroll-pane">
								<table class="ncz_tbl default">
									<caption class="hide">데이터 미리보기</caption>
									<colgroup>
										<col style="width:400px;"/>
										<col style="width:400px;"/>
										<col style="width:200px;"/>
										<col style="width:200px;"/>
										<col style="width:400px;"/>
									</colgroup>
									<thead>
										<tr>
											<th scope="col">Development Name</th>
											<th scope="col">Location</th>
											<th scope="col">Admin Number</th>
											<th scope="col">Bill ID</th>
											<th scope="col">Revenue Month</th>
										</tr>
									</thead>
									<tbody>
										<tr>
											<td>홍길동</td>
											<td>서울시 강남구 신사동 123</td>
											<td>123</td>
											<td>2283254792</td>
											<td>2018-09-01</td>
										</tr>
										<tr>
											<td>홍길동</td>
											<td>서울시 강남구 신사동 123</td>
											<td>123</td>
											<td>2283254792</td>
											<td>2018-09-01</td>
										</tr>
										<tr>
											<td>홍길동</td>
											<td>서울시 강남구 신사동 123</td>
											<td>123</td>
											<td>2283254792</td>
											<td>2018-09-01</td>
										</tr>
										<tr>
											<td>홍길동</td>
											<td>서울시 강남구 신사동 123</td>
											<td>123</td>
											<td>2283254792</td>
											<td>2018-09-01</td>
										</tr>
										<tr>
											<td>홍길동</td>
											<td>서울시 강남구 신사동 123</td>
											<td>123</td>
											<td>2283254792</td>
											<td>2018-09-01</td>
										</tr>
										<tr>
											<td>홍길동</td>
											<td>서울시 강남구 신사동 123</td>
											<td>123</td>
											<td>2283254792</td>
											<td>2018-09-01</td>
										</tr>
									</tbody>
								</table>
							</div>
						</div>
					</div>
				</div><!--//row_wrap-->

			</div>