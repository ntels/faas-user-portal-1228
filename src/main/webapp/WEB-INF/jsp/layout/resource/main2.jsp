<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<script>

$(document).ready(function(){
	$("#rightContent").show();
	
	tabEvent();
});

function collectionprev(){
	switchContent("${pageContext.request.contextPath}", "/faas/?menu=/layout/resource/collectionPrev.do");
}
function tabEvent(){
	$("#mongoDBTab").on("click",function(){
		switchContent("${pageContext.request.contextPath}", "/faas/?menu=/layout/resource/main2.do");
	});
	$("#postgresTab​").on("click",function(){
		switchContent("${pageContext.request.contextPath}", "/faas/?menu=/layout/resource/postgreMain2.do");
	});
}
</script>
		<div class="content">

			<!-- Tab 영역 -->
			<div class="row_wrap" id="tabArea">
				<div class="tabWrap type2">
					<ul class="nav nav-tabs">
						<li class="active" id="mongoDBTab"><a href="javascript:void(0);" onclick='switchContent("${pageContext.request.contextPath}", "/faas/?menu=/layout/resource/main2.do")'  title="MongoDB">MongoDB</a></li>
						<li id="postgresTab"><a href="javascript:void(0);" onclick='switchContent("${pageContext.request.contextPath}", "/faas/?menu=/layout/resource/postgreMain2.do")'  title="Postgre​">Postgre​</a></li>
					</ul>
				</div><!--//tabWrap-->
			</div><!--//row_wrap-->
			
			<div class="row_wrap" id="installInfo">
					<ul class="catalog api drop_wrap side_wrap icon">
						<li>
							<div>
								<span class="img mongoDB">
								</span>
								<span class="cnt">
									<span>
										<span class="name">MongoDB​</span>
									</span>
									<div class="txtlist" style="margin-top: 3px;">
										<dl class="tb_style">
											<dt style="width: 80px;">접속 URL </dt>
											<dd>219.250.188.229:1521/orcl​</dd>
											<dt style="width: 50px;">계정 </dt>
											<dd>
												ID : <span>p89mongodb</span>  &nbsp; &nbsp; &nbsp;  PW : <span>abcd1234#$%​</span>
											</dd>
											<dd>
												<button type="button" class="btn_inner mgL20">패스워드 변경</button>
											</dd>
										</dl>
									</div>
								</span>
							</div>
						</li>
					</ul>
				</div><!--//row_wrap-->
			<div class="row_wrap" id="install">
				<div class="ncz_panel">
					<div class="panel_head">
						<span class="tit">컬렉션 정보</span>
					</div>
					<div class="panel_cnt">
						
						<div class="row_wrap panel_add" style="height: 150px;">
							<!-- 버튼 영역 -->
							<button type="button" class="btn btn_white large icon_07" id="collectionAdd">컬렉션 등록​​</button>

							<!-- 가이드 영역 -->
							<div class="guide_area" id="collectionGuide">
								<span class="btn_guide">설치된 MongoDB에 컬렉션을 등록할 수 있습니다.​​</span>
							</div>
						</div><!--//row_wrap-->
						
						<table class="ncz_tbl default" id="collectionHeader">
							<caption class="hide">컬렉션 정보 상단제목</caption>
							<colgroup>
								<col style="" />
								<col style="" />
								<col style="width:270px;">
								<col style="width:170px;">
								<col style="width:100px;">
							</colgroup>
							<thead>
								<tr>
									<th scope="col" class="text-left">컬렉션 이름</th>
									<th scope="col" class="text-left">설명​</th>
									<th scope="col">등록 일시​</th>
									<th scope="col">등록자​</th>
									<th scope="col">관리​</th>
								</tr>
							</thead>
						</table>
						<div class="scroll-pane y_scroll_5" id="collectionBody">
							<table class="ncz_tbl default">
								<caption class="hide">컬렉션 정보 리스트</caption>
								<colgroup>
									<col style="" />
									<col style="" />
									<col style="width:250px;">
									<col style="width:200px;">
									<col style="width:70px;">
								</colgroup>
								<tbody>							
									<tr>
										<td class="text-left"><a href="javascript:void(0);" onclick="collectionprev();" class="underline">user</a></td>
										<td class="text-left">컬렉션 설명 출력​</td>
										<td>2020-01-02 00:00​</td>
										<td>홍길동​</td>
										<td>
											<div class="dropdown">
												<button id="option" type="button" class="btn only-ico ico-dot" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" title="관리">
													<span class="ico">관리</span>
												</button>
												<ul class="dropdown-menu short" role="option" aria-labelledby="option">
													<li>
														<a href="SC_FS_04_021.html">삭제​</a>
													</li>
													<li>
														<a href="javascript:void()">수정</a>
													</li>
												</ul>
											</div>
										</td>
									</tr>
									<tr>
										<td class="text-left">
											<input type="text" class="inp" value="user" style="width: 200px;">
										</td>
										<td class="text-left" colspan="4">
											<input type="text" class="inp" value="컬렉션 설명 출력" style="width: 80%;">
											<div class="inB">
												<button type="button" class="btn btn-md btn-color1 btn_s" title="저장" data-toggle="modal" data-target="#save">저장​</button>
												<button type="button" class="btn btn-md btn-color2 btn_s" title="취소">취소</button>
											</div>
										</td>
									</tr>							
									<tr>
										<td class="text-left"><a href="javascript:void(0);" onclick="collectionprev();" class="underline">password</a></td>
										<td class="text-left">컬렉션 설명 출력​</td>
										<td>2020-01-02 00:00​</td>
										<td>홍길동​</td>
										<td>
											<div class="dropdown">
												<button id="option" type="button" class="btn only-ico ico-dot" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" title="관리">
													<span class="ico">관리</span>
												</button>
												<ul class="dropdown-menu short" role="option" aria-labelledby="option">
													<li>
														<a href="SC_FS_04_021.html">삭제​</a>
													</li>
													<li>
														<a href="javascript:void()">수정</a>
													</li>
												</ul>
											</div>
										</td>
									</tr>							
									<tr>
										<td class="text-left"><a href="javascript:void(0);" onclick="collectionprev();" class="underline">image​</a></td>
										<td class="text-left">컬렉션 설명 출력 최대 230byte 출력 말 줄임 없음​</td>
										<td>2020-01-02 00:00​</td>
										<td>홍길동​</td>
										<td>
											<div class="dropdown">
												<button id="option" type="button" class="btn only-ico ico-dot" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" title="관리">
													<span class="ico">관리</span>
												</button>
												<ul class="dropdown-menu short" role="option" aria-labelledby="option">
													<li>
														<a href="SC_FS_04_021.html">삭제​</a>
													</li>
													<li>
														<a href="javascript:void()">수정</a>
													</li>
												</ul>
											</div>
										</td>
									</tr>							
									<tr>
										<td class="text-left"><a href="javascript:void(0);" onclick="collectionprev();" class="underline">user</a></td>
										<td class="text-left">컬렉션 설명 출력​</td>
										<td>2020-01-02 00:00​</td>
										<td>홍길동​</td>
										<td>
											<div class="dropdown">
												<button id="option" type="button" class="btn only-ico ico-dot" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" title="관리">
													<span class="ico">관리</span>
												</button>
												<ul class="dropdown-menu short" role="option" aria-labelledby="option">
													<li>
														<a href="SC_FS_04_021.html">삭제​</a>
													</li>
													<li>
														<a href="javascript:void()">수정</a>
													</li>
												</ul>
											</div>
										</td>
									</tr>							
									<tr>
										<td class="text-left"><a href="javascript:void(0);" onclick="collectionprev();" class="underline">password</a></td>
										<td class="text-left">컬렉션 설명 출력​</td>
										<td>2020-01-02 00:00​</td>
										<td>홍길동​</td>
										<td>
											<div class="dropdown">
												<button id="option" type="button" class="btn only-ico ico-dot" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" title="관리">
													<span class="ico">관리</span>
												</button>
												<ul class="dropdown-menu short" role="option" aria-labelledby="option">
													<li>
														<a href="SC_FS_04_021.html">삭제​</a>
													</li>
													<li>
														<a href="javascript:void()">수정</a>
													</li>
												</ul>
											</div>
										</td>
									</tr>							
								</tbody>
							</table>
						</div>
					</div>
				</div>
			</div><!--//row_wrap-->
		</div>
		<!-- //content -->