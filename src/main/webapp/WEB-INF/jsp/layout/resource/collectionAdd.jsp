<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<script>

$(document).ready(function(){
	$("#rightContent").show();
	file();
	addRow();
	deleteRow();
	bottomTab();
});

function collectionAddCancel(){
	switchContent("${pageContext.request.contextPath}", "/faas/?menu=/layout/resource/main2.do");
}
function file() {
	/* file */
	var multiFile = new Array();
	var fileNum = 0;

	$('.input_file input[type=file]').change(function() {
		var _id = this.getAttribute('id');
		var _name = this.getAttribute('name');
		
		fn_multiFile($(this));
	});

	function fn_multiFile(obj) {
		var chk = true;
		var list = obj[0].files;
		var limit = 1024 * 1024 * 2; //size 예시 2mb

		if(list.length > 5) { // 최대 파일 제한
			alert('파일 첨부는 최대 5개까지 입니다.');
			return;
		};

		if(multiFile.length >= 5) {
			alert('파일 첨부는 최대 5개까지 입니다.');
			return;
		};

		$.each(list, function(i,v) {
			var name = v.name.slice(v.name.lastIndexOf('.') + 1).toLowerCase();

			switch(name) {
				case "jpg":
					chk = true;
					break;
				case "jpeg":
					chk = true;
					break;
				case "gif":
					chk = true;
					break;
				case "ppt":
					chk = true;
					break;
				case "pptx":
					chk = true;
					break;
				case "doc":
					chk = true;
					break;
				case "docx":
					chk = true;
					break;
				case "pdf":
					chk = true;
					break;
				default : 
					alert("첨부 할 수 없는 확장자 입니다.");
					chk = false;
					break;
			};

			if(v.size > limit) {
				alert('첨부할 파일 용량이 2MB 보다 큽니다');
				return;
			};

			if(multiFile.length > 0){
				$.each(multiFile, function(i,f){
					if(f.name == v.name){
						alert('첨부 파일 이름이 같은 것이 있습니다.');
						chk = false;
					}
				});
			}

			if(chk){
				$('.file_info > ul').append('<li id="fileId'+fileNum+'"><span class="file_txt"><a href="javascript:void(0);">'+v.name+'</a> <a href="javascript:deleteMultiFile(\''+fileNum+'\',\''+v.name+'\')" class="delete"></a></span></li>');
				multiFile.push(v);
				fileNum ++;
			}
		});
	};

	function deleteMultiFile(num, fileName){
		for(var i=0; i<multiFile.length; i++){
			if(multiFile[i].name == fileName){
				multiFile.splice(i,1);
			}
		}

		$("#fileId"+num).remove();
	}
	/* //file */

	// 기본 형태 file

	$('.default_file_box .upload_hidden').on('change', function() {
		if(window.FileReader) {
			var filename = $(this)[0].files[0].name; 
		} else {
			var filename = $(this).val().split('/').pop().split('\\').pop(); 
		} 
		$(this).siblings('.upload_name').val(filename);
	});
};

/* table */
function addRow() {
	//추가 영역
	var btn = '[data-btn="row_produce"]';
	$(document).on('click',btn,function(e) {
		e.preventDefault();
		var addArea = $(this).closest('.inp_field').find('table > tbody');
		addArea.append('<tr><td><input type="text" class="inp" placeholder="영문, 숫자, 언더바(_)만 입력 가능​"></td><td><input type="text" class="inp" placeholder="컬렉션에 대한 설명을 입력해 주세요."></td><td><button type="button" name="button" class="btn btn_del only-ico" title="삭제" data-btn="delete"><span class="ico">삭제</span></button></td></tr>');
		$(this).closest('.inp_field').find('.scroll-pane').css('max-height','451px');
		$(this).closest('.inp_field').find('.scroll-pane').jScrollPane({
			autoReinitialise : true
		});
		deleteRow();
	});
};
function deleteRow() {

	$(document).on('click','[data-btn="delete"]', function(e) {
		e.preventDefault();
		var parents = $(this).parents('tr');
		
		$(this).closest('.inp_field').find('.scroll-pane').css('max-height','451px');
		$(this).closest('.inp_field').find('.scroll-pane').jScrollPane({
			autoReinitialise : true
		});
		//add
		if(parents.siblings('tr').length < 0) {
			return false
		} else {
			parents.remove();
		};
	});
};
/* //table */

function bottomTab() {
	$('[data-role="tab_box"] > div').hide();
	$.each($('[data-role="tab"] input'), function() {
		if($(this).is(':checked')) {
			var attr = $(this).attr('id');
			$.each($('[data-role="tab_box"] > div'),function() {
				if($(this).attr('data-tab') == attr) {
					$(this).show();
				};
			});
		};
	});
	$('input[name="tabs"]').change(function() {
		if($(this).is(':checked')) {
			var attr = $(this).attr('id'); 
			$('[data-role="tab_box"]').children('div').hide();
			$('[data-role="tab_box"]').find('[data-tab="'+ attr +'"]').show();
		};
	});
};
</script>
		<div class="content">

				<!-- Tab 영역 -->
				<div class="row_wrap">
					<div class="tabWrap type2">
						<ul class="nav nav-tabs">
							<li class="active" id="mongoDBTab"><a href="javascript:void(0);" onclick='switchContent("${pageContext.request.contextPath}", "/faas/?menu=/layout/resource/main2.do")'  title="MongoDB">MongoDB</a></li>
							<li id="postgresTab"><a href="javascript:void(0);" onclick='switchContent("${pageContext.request.contextPath}", "/faas/?menu=/layout/resource/postgreMain2.do")'  title="Postgre​">Postgre​</a></li>
						</ul>
					</div><!--//tabWrap-->
				</div><!--//row_wrap-->
 
				<div class="row_wrap">
					<ul class="catalog api drop_wrap side_wrap icon">
						<li>
							<div>
								<span class="img mongoDB">
								</span>
								<span class="cnt">
									<span>
										<span class="name">MongoDB​</span>
									</span>
									<div class="txtlist" style="margin-top: 3px;">
										<dl class="tb_style">
											<dt style="width: 80px;">접속 URL </dt>
											<dd>219.250.188.229:1521/orcl​</dd>
											<dt style="width: 50px;">계정 </dt>
											<dd>
												ID : <span>p89mongodb</span>  &nbsp; &nbsp; &nbsp;  PW : <span>abcd1234#$%​</span>
											</dd>
											<dd>
												<button type="button" class="btn_inner mgL20" onclick="location.href='SC_FS_04_022.html'">패스워드 변경</button>
											</dd>
										</dl>
									</div>
									<div class="side_item">
										<div class="dropdown" style="margin-top: 3px;">
											<button type="button" class="btn only-ico ico-dot" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" title="관리">
												<span class="ico">관리</span>
											</button>
											<ul class="dropdown-menu short md">
												<li>
													<a href="SC_FS_04_021.html">삭제</a>
												</li>
											</ul>
										</div>							
									</div>
								</span>
							</div>
						</li>
					</ul>
				</div><!--//row_wrap-->
				
				<div class="row_wrap">
					<div class="ncz_panel">
						<div class="panel_head poz_r">
							<span class="tit">
								<a href="SC_FS_04_001.html"><i class="icoType1 ico-prev"></i></a>
								<h4>컬렉션 등록​​​​</h4>​
							</span>
						</div>
						<div class="panel_cnt">
							<div class="inp_top">
								<span>* 는 필수입력 사항입니다.</span>
							</div>

							<div class="inp_field">
								<div class="top_area">
									<span class="tit imp">컬렉션 등록 방식​</span>
								</div>
								<div class="cnt" data-role="tab">
									<div class="radio radio-inline">
										<input type="radio" name="tabs" id="tab_1" checked="">
										<label for="tab_1" class="label">바로 입력</label>
									</div>
									<div class="radio radio-inline">
										<input type="radio" name="tabs" id="tab_2">
										<label for="tab_2" class="label">엑셀 파일 업로드</label>
									</div>
								</div>
							</div><!--//inp_field-->
							<div data-role="tab_box">
								<div class="inp_field" data-tab="tab_1" style="display: block;"><!--#tab_1-->
									<div class="top_area">
										<span class="tit imp">컬렉션 정보</span>
										<div class="right_area">
											<div class="btnArea">
												<button type="button" class="btn btn-color1 btn_s" data-btn="row_produce">추가</button>
											</div>
										</div>
									</div>
									<div class="cnt">
										<table class="ncz_tbl default pdL">
											<caption class="hide">컬렉션 정보 타이틀</caption>
											<colgroup>
												<col style="width:400px;">
												<col style="">
												<col style="width:117px;">
											</colgroup>
											<thead>
												<tr>
													<th scope="col">
														<span class="tit imp">컬렉션 이름​</span>
														<em class="ico-info1 ico right" data-toggle="tooltip" data-placement="right" title="" data-original-title="입력한 이름으로 컬렉션이 생성됩니다. 영문과 숫자, 
														언더바(_)만 공백 없이 입력 가능합니다.​"></em>
													</th>
													<th scope="col">설명​</th>
													<th scope="col">삭제</th>
												</tr>
											</thead>
										</table>
										<div class="scroll-pane">
											<table class="ncz_tbl default pdL">
												<caption class="hide">컬렉션 정보</caption>
												<colgroup>
													<col style="width:400px;">
													<col style="">
													<col style="width:117px;">
												</colgroup>
												<tbody>
													<tr>
														<td>
															<input type="text" class="inp" placeholder="영문, 숫자, 언더바(_)만 입력 가능​">
														</td>
														<td>
															<input type="text" class="inp" placeholder="컬렉션에 대한 설명을 입력해 주세요.">
														</td>
														<td>
															<button type="button" name="button" class="btn btn_del only-ico" title="삭제" data-btn="delete">
																<span class="ico">삭제</span>
															</button>
														</td>
													</tr>
													<tr>
														<td>
															<input type="text" class="inp" placeholder="영문, 숫자, 언더바(_)만 입력 가능​">
														</td>
														<td>
															<input type="text" class="inp" placeholder="컬렉션에 대한 설명을 입력해 주세요.">
														</td>
														<td>
															<button type="button" name="button" class="btn btn_del only-ico" title="삭제" data-btn="delete">
																<span class="ico">삭제</span>
															</button>
														</td>
													</tr>
													<tr>
														<td>
															<input type="text" class="inp" placeholder="영문, 숫자, 언더바(_)만 입력 가능​">
														</td>
														<td>
															<input type="text" class="inp" placeholder="컬렉션에 대한 설명을 입력해 주세요.">
														</td>
														<td>
															<button type="button" name="button" class="btn btn_del only-ico" title="삭제" data-btn="delete">
																<span class="ico">삭제</span>
															</button>
														</td>
													</tr>
												</tbody>
											</table>
										</div>
									</div>
								</div><!--//inp_field-->
									
								<div class="inp_field" data-tab="tab_2" style="display: none;"><!--#tab_2-->
									<div class="top_area">
										<span class="tit imp">컬렉션 정보 업로드</span>
										<em class="ico-info1 ico right" data-toggle="tooltip" data-placement="right" title="" data-original-title="파일 양식 다운로드 후, 양식에 맞게 컬렉션 정보를 입력해 주세요.​"></em>
									</div>
									<div class="cnt">
										<div class="upload_wrap">
											<button type="button" class="btn_api w200">파일 양식 다운로드</button>
											<div class="default_file_box">
												<input type="text" class="inp upload_name" disabled="">
												<input type="file" id="file" class="inp upload_hidden">
												<label for="file">파일 찾기</label>
											</div>
										</div>
										<div class="upload_wrap">
											<button type="button" class="btn btn-m btn-color1 w200">파일 검증</button>
											<p class="verif">파일 검증 결과 모두 성공하였습니다.</p><!--addClass fail-->
										</div>
										<div class="inner_box">
											<div class="row_list_info">
												<ul>
													<li>성공 : 2건</li>
													<li class="fail">실패 : 2건</li>
												</ul>
												<span class="txt">실패 내역이 있으면 컬렉션 정보가 저장되지 않습니다.</span>
											</div>
											<ul class="row_list">
												<li>Row2 : 입력 양식 오류</li>
												<li>Row3 : 입력 양식 오류</li>
												<li>Row4 : 입력 양식 오류</li>
												<li>Row5 : 입력 양식 오류</li>
												<li>Row6 : 입력 양식 오류</li>
											</ul>
										</div>
									</div>
								</div><!--//inp_field-->
							</div><!--//tab_box-->

							<div class="btnArea confirm">
								<button type="button" class="btn btn-md btn-color1" onclick="collectionAddCancel();" title="등록">등록</button>
								<button type="button" class="btn btn-md btn-color2" onclick="collectionAddCancel();" title="취소">취소</button>
							</div>

						</div>
					</div>
				</div><!--//row_wrap-->
				
		</div>
		<!-- //content -->