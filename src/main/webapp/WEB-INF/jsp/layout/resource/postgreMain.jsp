<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<script>

$(document).ready(function(){
	$("#rightContent").show();
	
	$("#postgresInstall").hide();
	$("#postgresInfo").hide();
	$("#postgresTableArea").hide();
	
	$("#postgresInstallBtn").on("click",function(){
		$("#postgresNoData").hide();
		$("#postgresInstall").show();
		setTimeout(function(){
			$("#postgresInstall").hide();
			$("#postgresTableArea").show();
		},3000);
	});
		
	$("#tableAddBtn").on("click",function(){
		switchContent("${pageContext.request.contextPath}", "/faas/?menu=/layout/resource/tableAdd.do");
	});
});

</script>
		<div class="content">

			<!-- Tab 영역 -->
				<div class="row_wrap">
					<div class="tabWrap type2">		
						<ul class="nav nav-tabs">
							<li><a href="javascript:void(0);" onclick='switchContent("${pageContext.request.contextPath}", "/faas/?menu=/layout/resource/main.do")'  title="MongoDB">MongoDB</a></li>
							<li class="active"><a href="javascript:void(0);" onclick='switchContent("${pageContext.request.contextPath}", "/faas/?menu=/layout/resource/postgreMain.do")'  title="Postgre​">Postgre​</a></li>
						</ul>
					</div><!--//tabWrap-->
				</div><!--//row_wrap-->
 
				<div class="row_wrap panel_add" id="postgresNoData">
					<!-- 버튼 영역 -->
					<button type="button" class="btn btn_white large icon_postgre" id="postgresInstallBtn">Postgre​ 설치​</button>

					<!-- 가이드 영역 -->
					<div class="guide_area type-mongoDB2">
						<span class="btn_guide">MongoDB를 설치하여 트리거 및 백엔드 서비스로 이용할 수 있습니다.​</span>
						<span class="txt">
							Function 서비스를 이용하여 트리거 및 백엔드 서비스로 DB를 이용하고자 할 경우, 사전에 자원을 설치해야 합니다.<br>
							프로젝트별로 필요에 따라 자원을 설치해 주세요. ​<br>
							자원은 종류별로 <strong>1회만 설치 가능</strong>하며, ​ ​<br>
							<strong>설치 시 접속 정보 등 세부 정보를 활용하고 관리</strong>하실 수 있습니다.​ ​
						</span>
					</div>
				</div><!--//row_wrap-->
				<!--// 데이터 없을때 -->
				
				<div class="row_wrap panel_add" id="postgresInstall">
					<div class="ncz_panel pdB80">
						<div class="guide_area type-postgre">
							<span class="txt">
								<span>현재 Postgre를 설치하고 있습니다. </span><br>
								<strong>설치가 완료되면 DB 정보 확인이 가능합니다.​</strong>
							</span>
						</div>
					</div>
				</div><!--//row_wrap-->

				<div class="row_wrap" id="postgresInfo">
					<ul class="catalog api drop_wrap side_wrap icon">
						<li>
							<div>
								<span class="img postgre">
								</span>
								<span class="cnt">
									<span>
										<span class="name">Postgre​</span>
									</span>
									<div class="txtlist" style="margin-top: 3px;">
										<dl class="tb_style">
											<dt style="width: 80px;">접속 URL </dt>
											<dd>219.250.188.229:1521/orcl​</dd>
											<dt style="width: 50px;">계정 </dt>
											<dd>
												ID : <span>p89mongodb</span>  &nbsp; &nbsp; &nbsp;  PW : <span>abcd1234#$%​</span>
											</dd>
											<dd>
												<button type="button" class="btn_inner mgL20">패스워드 변경</button>
											</dd>
										</dl>
									</div>
								</span>
							</div>
						</li>
					</ul>
				</div><!--//row_wrap-->
				
				<div class="row_wrap" id="postgresTableArea">
					<div class="ncz_panel">
						<div class="panel_head">
							<span class="tit">테이블 정보</span>
						</div>
						<div class="panel_cnt">
							
							<div class="row_wrap panel_add" style="height: 150px;">
								<!-- 버튼 영역 -->
								<button type="button" class="btn btn_white large icon_07" id="tableAddBtn">테이블 등록​​</button>

								<!-- 가이드 영역 -->
								<div class="guide_area">
									<span class="btn_guide">설치된 Postgre에 테이블을 등록할 수 있습니다.​​</span>
								</div>
							</div><!--//row_wrap-->
										
							<div class="ncz_msg_empty">
								<span>등록한 테이블이 없습니다. </span>
								<strong>테이블 등록 후 이용해 주세요.​</strong>
							</div>
							

						</div>
					</div>
				</div><!--//row_wrap-->

		</div>
		<!-- //content -->