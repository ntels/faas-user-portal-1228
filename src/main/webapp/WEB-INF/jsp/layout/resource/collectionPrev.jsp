<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<script>

$(document).ready(function(){
	$("#rightContent").show();
	
	tabEvent();
});

function collectionAddCancel(){
	switchContent("${pageContext.request.contextPath}", "/faas/?menu=/layout/resource/main2.do");
}
function tabEvent(){
	$("#mongoDBTab").on("click",function(){
		switchContent("${pageContext.request.contextPath}", "/faas/?menu=/layout/resource/main2.do");
	});
	$("#postgresTab​").on("click",function(){
		switchContent("${pageContext.request.contextPath}", "/faas/?menu=/layout/resource/postgreMain2.do");
	});
}
</script>
		<div class="content">

				<!-- Tab 영역 -->
				<div class="row_wrap">
					<div class="tabWrap type2">
						<ul class="nav nav-tabs">
							<li class="active" id="mongoDBTab"><a href="javascript:void(0);" onclick='switchContent("${pageContext.request.contextPath}", "/faas/?menu=/layout/resource/main2.do")'  title="MongoDB">MongoDB</a></li>
							<li id="postgresTab"><a href="javascript:void(0);" onclick='switchContent("${pageContext.request.contextPath}", "/faas/?menu=/layout/resource/postgreMain2.do")'  title="Postgre​">Postgre​</a></li>
						</ul>
					</div><!--//tabWrap-->
				</div><!--//row_wrap-->
 
				<div class="row_wrap">
					<ul class="catalog api drop_wrap side_wrap icon">
						<li>
							<div>
								<span class="img mongoDB">
								</span>
								<span class="cnt">
									<span>
										<span class="name">MongoDB​</span>
									</span>
									<div class="txtlist" style="margin-top: 3px;">
										<dl class="tb_style">
											<dt style="width: 80px;">접속 URL </dt>
											<dd>219.250.188.229:1521/orcl​</dd>
											<dt style="width: 50px;">계정 </dt>
											<dd>
												ID : <span>p89mongodb</span>  &nbsp; &nbsp; &nbsp;  PW : <span>abcd1234#$%​</span>
											</dd>
											<dd>
												<button type="button" class="btn_inner mgL20" onclick="location.href='SC_FS_04_022.html'">패스워드 변경</button>
											</dd>
										</dl>
									</div>
									<div class="side_item">
										<div class="dropdown" style="margin-top: 3px;">
											<button type="button" class="btn only-ico ico-dot" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" title="관리">
												<span class="ico">관리</span>
											</button>
											<ul class="dropdown-menu short md">
												<li>
													<a href="SC_FS_04_021.html">삭제</a>
												</li>
											</ul>
										</div>							
									</div>
								</span>
							</div>
						</li>
					</ul>
				</div><!--//row_wrap-->
				
				<div class="row_wrap">
					<div class="ncz_panel">
						<div class="panel_head poz_r">
							<span class="tit">
								<a href="SC_FS_04_001.html"><i class="icoType1 ico-prev"></i></a>
								<h4>데이터 미리보기​​​</h4>​
							</span>
							<div class="right_area">
								<select class="inp wL">
									<option>최근 데이터 10건​</option>
									<option>최근 데이터 20건​</option>
									<option>최근 데이터 30건​</option>
									<option>최근 데이터 50건​</option>
									<option>최근 데이터 100건​</option>
								</select>
								<button type="button" class="btn_api" title="데이터 다운로드​">데이터 다운로드​</button>
							</div>
						</div>
						<div class="panel_cnt tabSelectWrap">
							
							<ul class="tab_selecter" style="height: 400px;">
								<li><!--left area--> 
									<ul data-list="main" style="height: 400px;">
										<li class=""><!--active-->
											<a href="javascript:void(0);" data-wrap="list_01">2020-04-01 00:00:00​</a>
										</li>
										<li>
											<a href="javascript:void(0);" data-wrap="list_02">2020-04-01 00:00:00​</a>
										</li>
										<li>
											<a href="javascript:void(0);" data-wrap="list_03">2020-04-01 00:00:00​</a>
										</li>
										<li>
											<a href="javascript:void(0);" data-wrap="list_04">2020-04-01 00:00:00​</a>
										</li>
										<li>
											<a href="javascript:void(0);" data-wrap="list_05">2020-04-01 00:00:00​</a>
										</li>
										<li>
											<a href="javascript:void(0);" data-wrap="list_06">2020-04-01 00:00:00​</a>
										</li>
										<li>
											<a href="javascript:void(0);" data-wrap="list_07">2020-04-01 00:00:00​</a>
										</li>
										<li>
											<a href="javascript:void(0);" data-wrap="list_08">2020-04-01 00:00:00​</a>
										</li>
										<li>
											<a href="javascript:void(0);" data-wrap="list_09">2020-04-01 00:00:00​</a>
										</li>
										<li>
											<a href="javascript:void(0);" data-wrap="list_10">2020-04-01 00:00:00​</a>
										</li>
									</ul>
								</li>
								<li><!--right area-->
									<div data-list="sub" data-num="list_01">
										<div>
											content 1
											content 1
											content 1
											content 1
											content 1
											content 1
											content 1
											content 1
											content 1
											content 1
											content 1
											content 1
											content 1
											content 1
											content 1
											content 1
										</div>
									</div>
									<div data-list="sub" data-num="list_02">
										<div>
											content 2
										</div>
									</div>
									<div data-list="sub" data-num="list_03">
										<div>
											content 3
										</div>
									</div>
									<div data-list="sub" data-num="list_04">
										<div>
											content 4
										</div>
									</div>
									<div data-list="sub" data-num="list_05">
										<div>
											content 5
										</div>
									</div>
									<div data-list="sub" data-num="list_06">
										<div>
											content 6
										</div>
									</div>
									<div data-list="sub" data-num="list_07">
										<div>
											content 7
										</div>
									</div>
									<div data-list="sub" data-num="list_08">
										<div>
											content 8
										</div>
									</div>
									<div data-list="sub" data-num="list_09">
										<div>
											content 9
										</div>
									</div>
									<div data-list="sub" data-num="list_10">
										<div>
											content 10
										</div>
									</div>
								</li>
							</ul>
						</div>
					</div>
				</div><!--//row_wrap-->
				
			</div>