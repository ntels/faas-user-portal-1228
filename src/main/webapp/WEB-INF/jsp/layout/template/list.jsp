<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>

    
<script>
	let debug = true;

$(document).ready(function(){
	debugger;
	
	NtelsHB.rootNodeSelector = "#handlebarRoot";

	let data = {
			isCard: true
			,table:{caption:"" , widths:['80px', '20%', '20%', null] , headers:['선택', '함수', '런타임', '설명']}
			,list:[]
			,listCnt:0
			,nav:{curpage:1,lastpage:1,start:1,end:1,onclick:onNavClick}
			,search_name:''
			,selected_id: ''
		};

	function resetData() {
		data.nav.curpage = 1;
		data.selected_id = '';
	}

	function componentLoad()
	{
		var context = "${pageContext.request.contextPath}";
		var rooturl = context + "/component/template/list/";
		
		NtelsHB.registerComponent("title", rooturl+"title.do");
		NtelsHB.registerComponent("listTable", rooturl+"listTable.do"); 
		NtelsHB.registerComponent("listCardItem", rooturl+"listCardItem.do");
		NtelsHB.registerComponent("listTableItem", rooturl+"listTableItem.do");
		NtelsHB.registerComponent("listEmpty", rooturl+"listEmpty.do");
		NtelsHB.registerComponent("viewChanger", "component/viewChanger.do");
		NtelsHB.registerComponent("selector", "component/function/regFunction/selector.do");		

		NtelsHB.loadComponent();
	}
	
	NtelsHB.onComponentReady = function()
	{
		NtelsHB.bind(data);	
		dataLoad();
	}
	
	NtelsHB.afterRender = function(){

		$("#reloadBtn").on("click",function(){
			dataLoad();
		});
		
		$("#search_btn").on("click", function() {
			data.search_name = $("#search_name").val();
			resetData();
			dataLoad();
		});
		
		$("#viewCard").on("click",function(){
			if(!data.isCard)
			{
				data.isCard = true;
				NtelsHB.render();
				
				selectSelectedTemplate();
			}  
		});
		
		$("#viewList").on("click",function(){
			if(data.isCard)
			{
				data.isCard = false;
				NtelsHB.render();
				
				selectSelectedTemplate();
			}
		});
		
		// 템플릿 선택(카드형)
		$('div[name=template-list-card]').on('click', function() {
			data.selected_id = $(this).attr('template-id');
			
			$('div[name=template-list-card]').each(function() {
				$(this).removeClass('on');
			});
			
			$(this).addClass('on');
		});
		
		// 템플릿 선택(테이블형)
		$('div[name=template-list-table]').on('click', function() {
			data.selected_id = $(this).attr('template-id');
			
			$('div[name=template-list-table]').each(function() {
				$(this).children('input[name=template-list-table-btn]').prop("checked", false);
			});
			
			$(this).children('input[name=template-list-table-btn]').prop("checked", true);
		});

        $("#nextButton").on('click', function() {
        	if(!data.selected_id) {
            	fnModalAlert("템플릿을 선택해 주세요.", "알림");
            	return;
			}
        	
        	let url = 'layout/template/detail.do?template_id=' + data.selected_id;
			loadPage(url);
		});

	}
	
	
	function dataLoad(showProgress)
	{
		let param = {
				curpage: data.nav.curpage,
				search_name: data.search_name,
		}
		
		if(showProgress == undefined) showProgress = true;

		JsonAjaxCall('${pageContext.request.contextPath}/template/loadTemplateList.json', JSON.stringify(param), function(response) {
			if(debug) console.log('loadTemplateList.json', response)
			
			data.list = response.list;
			data.listCnt = data.list.length;
			data.selected_id = '';
			
			callNav(data, response.curpage, response.lastpage);

			NtelsHB.render();
			
		},showProgress);
	}

	function callNav(data, curpage, lastpage)
	{
		data.nav.curpage = curpage;
		data.nav.lastpage = lastpage;

		data.nav.start = Math.floor((curpage-1)/10)*10 + 1;
		data.nav.end = data.nav.start + 10 - 1;
		
		if(data.nav.end > data.nav.lastpage) {
			data.nav.end = data.nav.lastpage;  
		}
	}

	function onNavClick(page)
	{
		data.nav.curpage = page;
		dataLoad();
	}
	
	function selectSelectedTemplate() {
		if(data.isCard) {
			$('div[name=template-list-card]').each(function() {
				if($(this).attr('template-id') == data.selected_id) {
					$(this).addClass('on');
				}
				else {
					$(this).removeClass('on');
				}
			});
		} else {
			$('div[name=template-list-table]').each(function() {
				if($(this).attr('template-id') == data.selected_id) {
					$(this).children('input[name=template-list-table-btn]').prop("checked", true);
				}
				else {
					$(this).children('input[name=template-list-table-btn]').prop("checked", false);
				}
			});
		}
	}

	componentLoad();

});

</script>
<div id="handlebarRoot">
	<main id="contentWrap">
		
		{{component "title"}}
		
		<!-- inner -->
		<div class="page-content">
	
			<!-- 콘텐츠 영역 -->
			<div class="inner-wrap">
				<section class="panel panel-bordered">
					<div class="panel-heading">
						<h3 class="panel-title">
							함수 등록
						</h3>
					</div> <!-- // panel-heading -->
					<div class="panel-body">
						<div class="text-right add-txt">
							<span><b class="caution-star">*</b> 는 필수입력 사항입니다.</span>
						</div>
	
						{{component "selector"}}
	
						<div class="write-sub-title">
							<b class="caution-star">*</b> 템플릿 선택​
							<span class="info-icon-tooltip" data-toggle="tooltip" title="사전에 만들어진 함수를 선택하여 자동으로 생성할 수 있습니다.​​" data-placement="right">
								i
							</span>
						</div>
						<div class="row">
							<div class="col-md-4">
								<div class="form-btn-group">
									<div class="form-group margin-bottom-0">
										<input id="search_name" type="text" class="form-control" placeholder="키워드로 템플릿을 검색해 보세요." value="{{search_name}}">
									</div>
									<button id="search_btn" type="button" class="btn btn-primary">
										<div class="feather-icon">
											<i data-feather="search"></i>
										</div>
										<span class="sr-only">Search</span>
									</button>
								</div>
							</div>
							<div class="col-md-8 text-right">
								
								{{component "viewChanger"}}
								
							</div>
						</div>
						
						{{#if isCard}}
							{{#if (neq listCnt 0)}}
							<!-- 카드형 -->
							<div class="card-list-wrap">
								<div class="row">
								{{#each list}}
									{{component "listCardItem"}}
								{{/each}}
								</div>
							</div>
							{{component "navigation"}}
							
							{{else}}
							
							{{component "listEmpty"}}
							{{/if}}
							
						{{else}}
						
							{{#if (neq listCnt 0)}}
							<!-- 테이블형 -->
							{{component "listTable"}}
							{{component "navigation"}}
							
							{{else}}
							
							{{component "listEmpty"}}
							{{/if}}
						{{/if}}
						
					</div> <!-- // panel-body -->
				</section>
	
				<div class="margin-top-40 text-right">
					<button id="nextButton" class="btn btn-lg btn-primary">다음​​</button>
					<button class="btn btn-lg btn-default" onclick="loadPage('layout/function/list.do');">취소​</button>
				</div>
	
			</div>
		</div>
		<!-- // inner -->
		
	</main>
</div>