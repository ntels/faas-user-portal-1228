<%@ page language="java" contentType="text/html; charset=UTF-8"
		 pageEncoding="UTF-8"%>

<link rel="stylesheet" href="${pageContext.request.contextPath}/core/lib/datepicker/daterangepicker.css">
<script src="${pageContext.request.contextPath}/core/js/ace-builds/src-noconflict/ace.js" type="text/javascript" charset="utf-8"></script>
<script src="${pageContext.request.contextPath}/core/js/ace-builds/src-noconflict/ext-modelist.js"></script>
<script src="${pageContext.request.contextPath}/core/js/ace-builds/src-noconflict/ext-language_tools.js"></script>
<script src="${pageContext.request.contextPath}/core/lib/datepicker/daterangepicker.js"></script>
<script src="${pageContext.request.contextPath}/core/lib/datepicker/daterangepicker_common.js"></script>

<style type="text/css" media="screen">
	.editorBox {background: #ffffff !important}
</style>

<script>

	$(document).ready(function(){
		debugger;

		NtelsHB.rootNodeSelector = "#handlebarRoot";

		var data = {
			templateId: ${paramMap.template_id},
			info: {},
			templateSource: {},
			validation: true,
		};

		function componentLoad()
		{
			let context = "${pageContext.request.contextPath}";
			NtelsHB.registerComponent("title", context + "/component/template/list/title.do");

			let rooturl = context + "/component/template/detail/";
			NtelsHB.registerComponent("triggerHttp", rooturl+"triggerHttp.do");
			NtelsHB.registerComponent("triggerTimer", rooturl+"triggerTimer.do");
			NtelsHB.registerComponent("triggerDb", rooturl+"triggerDb.do");
			NtelsHB.registerComponent("triggerStorage", rooturl+"triggerStorage.do");

			NtelsHB.loadComponent();
		}

		NtelsHB.onComponentReady = function()
		{
			NtelsHB.bind(data);
			dataLoad();
		}

		NtelsHB.afterRender = function()
		{
			setSource();
			
			triggerHttpEvent();
			triggerDBEvent();
			triggerTimerEvent();
			triggerStorageEvent();

			$('#saveBtn').on('click', function(){
			    
				if(triggerValidation() == false){
					fnModalAlert("입력정보에 오류가 있습니다. 화면 오류를 확인하시기 바랍니다.","알림");
					return;
				}
				regFunction();
			});
			
			$('#funcNm').keyup(function(){
				$("#parentFuncNm").removeClass("form-danger-before");
				$("#funcNmNullErr").hide();
				$("#funcNmRegexErr").hide();
				$("#funcNmDupErr").hide();
			});

			$("#reloadBtn").on("click",function(){
				dataLoad();
			});
		}

		function dataLoad(showProgress)
		{
			var param = {
				user_id: USER_ID,
				user_name: USER_NAME,
				project_id: $.cookie('PROJECT-ID'),
				template_id: data.templateId
			}

			console.log(JSON.stringify(param));

			JsonAjaxCall('${pageContext.request.contextPath}/template/loadTemplateInfo.json', JSON.stringify(param), function(response) {

                if(response.result.success){
					data.info = response.info;
					data.templateSource = response.templateSource;
				}else{
                	alert(response.message);
				}

				NtelsHB.render();

			}, showProgress == undefined);

		}

		function setEditor() {
			var editor = ace.edit("editor");
			
			editor.setOptions({
				theme: "ace/theme/textmate",
				fontSize: "16px",
			    readOnly: true,
			});
			editor.session.setOption("useWorker", true);
			editor.setValue("");
			editor.clearSelection();
		}

		function setSource() {
			var modelist = ace.require("ace/ext/modelist");
			var editor = ace.edit("editor");

			console.log('setSource() data.templateSource:' , data.templateSource);
			editor.setValue(data.templateSource.fileContent);
			editor.clearSelection();

			var modeCheckFile = data.templateSource.fileName;
			var modeCheck = modelist.getModeForPath(modeCheckFile);

			editor.session.setMode(modeCheck.mode);
			editor.getOptions().readOnly = true;
		}
		
		function triggerValidation()
		{
			data.validation = true;
			
			if($('#funcNm').val() == "")
		    {
		    	$('#parentFuncNm').addClass('form-danger-before');
		    	$('#funcNmNullErr').show();
		    	return false;
		    }
			
		    $(data.info.triggerinfo.HTTP).each(function(i,item){
		    	triggerHttpValidation(item.trigger_id,data);
		    });
		    $(data.info.triggerinfo.DB).each(function(i,item){
		    	triggerDBValidation(item.trigger_id,data);
		    });
		    $(data.info.triggerinfo.Timer).each(function(i,item){
	    		triggerTimerValidation(item.trigger_id,data);
		    });
		    
		    var funcNmRegex = /^([a-z])[a-z0-9\-]{6,18}([a-z0-9])$/g;
		    if(funcNmRegex.test($("#funcNm").val()) == false) {
		    	$('#parentFuncNm').addClass('form-danger-before');
				$("#funcNmRegexErr").show();
				return false;
			}
		    
		    return data.validation;
		}

		function regFunction(){
			
			function checkPush(list , item)
			{
				var trigger_id = item.trigger_id;
				item['trigger_id'] = 0;
				var json = JSON.stringify(item);
				for(var ii=0;ii<list.length;ii++)
				{
					var trigger_id2 = list[ii].trigger_id;
					list[ii].trigger_id = 0;
					if(JSON.stringify(list[ii]) == json) return false
					list[ii].trigger_id = trigger_id2;
				}
				item['trigger_id'] = trigger_id;
				list.push(item);
				return true;
			}
			
			function triggerData()
			{
				var isSuccess = true;
				
			    $(data.info.triggerinfo.HTTP).each(function(i,item){
			    	if(checkPush(trData.Http , triggerHttpData(item.trigger_id)) == false) isSuccess = false;
			    });
			    $(data.info.triggerinfo.DB).each(function(i,item){
			    	if(checkPush(trData.DB , triggerDBData(item.trigger_id)) == false) isSuccess = false;
			    });
			    $(data.info.triggerinfo.Timer).each(function(i,item){
			    	if(checkPush(trData.Timer , triggerTimerData(item.trigger_id)) == false) isSuccess = false;
			    });
			    
			    return isSuccess;
			}
			
			var trData = {DB:[] , Http:[] , Timer:[] , Ceph: []};
			if(triggerData() == false)
			{
				fnModalAlert("트리거 설정에 동일한 내용이 존재합니다.","알림");
				return;
			}
			
			
			var param = {
				functionDspName: $('#funcNm').val(),
				templateId: ${paramMap.template_id},
				triggerInfo: trData,
			}

			JsonAjaxCall('${pageContext.request.contextPath}/template/existFunction.json', JSON.stringify(param), function(response) {
				if(!response.result.success){
					console.log('saveTemplate.json error', response.result.message);
					return;
				}

				if(response.exist){
					$('#parentFuncNm').addClass('form-danger-before');
					$("#funcNmDupErr").show();
					return;
				}

				JsonAjaxCall('${pageContext.request.contextPath}/template/saveTemplate.json', JSON.stringify(param), function(response) {
					console.log('saveTemplate.json response', response);
					if(response.result.success){
						let functionId = response.functionId;
						loadPage('layout/function/detail.do?function_id=' + functionId);
					}else {
						console.log('saveTemplate.json error', response.result.message);
					}

				}, true);

			}, true);

		}

		setEditor();
		componentLoad();

	});
	
	
</script>


<div id="handlebarRoot">

	<main id="contentWrap">

		{{component "title"}}

		<!-- inner -->
		<div class="page-content">

			<!-- 콘텐츠 영역 -->
			<div class="inner-wrap">
				<section class="panel panel-bordered">
					<div class="panel-heading">
						<h3 class="panel-title">
							함수 등록 <span class="sub-tit"> - {{info.name}}</span>
						</h3>
					</div> <!-- // panel-heading -->
					<div class="panel-body">
						<div class="text-right add-txt">
							<span><b class="caution-star">*</b> 는 필수입력 사항입니다.</span>
						</div>

						<div class="row">
							<div class="col-md-6">
								<div class="write-sub-title">
									<b class="caution-star">*</b> 함수 이름​
									<span class="info-icon-tooltip" data-toggle="tooltip" title="공백 없이 영문 대소문자, 숫자, 하이픈(-), 언더바(_)만 입력해 주세요.​" data-placement="right">
										i
									</span>
								</div>
								<div class="form-group" id="parentFuncNm">
									<input type="text" class="form-control" id="funcNm" placeholder="함수 이름을 입력해 주세요.​">
									<p class="form-text" id="funcNmRegexErr" style="display:none;">함수 이름은 영문 소문자, 숫자, 하이픈(-)만 공백 없이 8 ~ 20자 이내로 입력 가능합니다.</p>
									<p class="form-text" id="funcNmDupErr" style="display:none;">이미 사용중인 이름입니다. 다른 이름으로 입력해 주세요.</p>
									<p class="form-text" id="funcNmNullErr" style="display:none;">함수 이름을 입력해 주세요.</p>
								</div>
							</div>
						</div>
					</div>
					
					{{component "triggerHttp"}}
					{{component "triggerDb"}}
					{{component "triggerTimer"}}
					{{component "triggerStorage"}}

					<div class="panel-body">
						<div class="write-sub-title">
							함수 코드 확인​
							<span class="info-icon-tooltip" data-toggle="tooltip" title="hub-PoP에서 기본 함수 코드를 제공합니다. 함수 생성 후에 함수 코드를 수정하여 사용하실 수 있습니다.​" data-placement="right">
								i
							</span>
						</div>
						<div class="alert alert-secondary">
							<div class="text-area font-size-20">
								{{info.runtime}} - {{info.version}}
							</div>
						</div>
						<div id="editor" class="editorBox" style="height: ;">
							{{data.templateSource.fileContent}}
						</div>
					</div>
				</section>

				<div class="margin-top-40 text-right">
					<button class="btn btn-lg btn-primary" id="saveBtn">함수 생성​</button>
					<button class="btn btn-lg btn-default" onclick="loadPage('layout/template/list.do')">취소​</button>
				</div>

			</div>
			
		</div>
		<!-- // inner -->
	</main>
</div>
