<%@page import="com.fasterxml.jackson.databind.ObjectMapper"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<script src="${pageContext.request.contextPath}/core/js/ace-builds/src-noconflict/ace.js" type="text/javascript" charset="utf-8"></script>
<script src="${pageContext.request.contextPath}/core/js/ace-builds/src-noconflict/ext-modelist.js"></script>
<script src="${pageContext.request.contextPath}/core/js/ace-builds/src-noconflict/ext-language_tools.js"></script>
<!-- diff 관련 -->
<script src="${pageContext.request.contextPath}/core/js/diff2html/diff2html.min.js"></script>
<script src="${pageContext.request.contextPath}/core/js/diff2html/diff2html-ui.min.js"></script>
<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/core/css/diff2html/diff2html.min.css"/>
<!-- diff 관련 -->
<style type="text/css" media="screen">
	.editorBox {background: #ffffff !important}
</style>
<script>
var g_func = {};
$(document).ready(function(){
	debugger;
	
	NtelsHB.rootNodeSelector = "#handlebarRoot";
	NtelsHB["this"] = this;
	
	var data = {
			funObj:{}
			,desBtnShow: false
			,isFirst: true
			,isClickable: false
			,table:{caption:"" , widths:['200px','150px',null,'150px'] , headers:['로그 발생 일시','구분','로그 내역','수정자']}
			,list:[]
			,listCnt:0
			,totalCnt:0
			,nav:{curpage:1,lastpage:1,start:1,end:1,onclick:onNavClick}
			,curView:"list" //list,error,diff
			,curLog: {}
			,curLogId : 0
			,curDiff: {source: "" , diff: ""}
			,sch_log_cd:""
		};

	
	function componentLoad()
	{
		var context = "${pageContext.request.contextPath}";
		var rooturl = context + "/component/function/detail/";
		var commurl = context + "/common/component/list/";
		
		NtelsHB.registerComponent("listEmpty",commurl+"/listEmpty.do");
		NtelsHB.registerComponent("navigation",commurl+"/navigation.do");

		NtelsHB.registerComponent("listTable",rooturl+"/listTable.do");
		NtelsHB.registerComponent("listTableItem",rooturl+"detail_log_listTableItem.do");
		NtelsHB.registerComponent("title",rooturl+"title.do");
		NtelsHB.registerComponent("info",rooturl + "info.do?function_id=" + ${paramMap.function_id} );
		NtelsHB.loadComponent();

	}
	
	NtelsHB.onComponentReady = function()
	{
		NtelsHB.bind(data);	
		dataLoad();
	}
	
	NtelsHB.afterRender = function()
	{
		NtelsHB.func.dataLoad = dataLoad;
		titleAfterRender();
		infoAfterRender();
		
		$("#sch_log_cd").on("change",function(){
			data.sch_log_cd = $(this).val();
			dataLoad();
		});
		
		$("#sch_log_cd").val(data.sch_log_cd).prop("selected",true);
		
		if(data.curView == "error")	createEditor("logEditor",data.curLog.logtext);
		if(data.curView == "diff")
		{
			createEditor("sourceEditor",data.curDiff.source);
			var el = document.getElementById("diffView");
			var configuration = { inputFormat: 'json', drawFileList: false, matching: 'none', highlight: true };
			const diff2htmlUi = new Diff2HtmlUI(el, data.curDiff.diff, configuration);
			diff2htmlUi.draw();
			diff2htmlUi.highlightCode();
		}
	}
	
	function createEditor(id , content) 
	{
		//var modelist = ace.require("ace/ext/modelist");
		var editor = ace.edit(id);
		
		editor.setOptions({
			theme: "ace/theme/textmate",
			fontSize: "16px",
		    //enableBasicAutocompletion: true,
		    //enableSnippets: true,
		    //enableLiveAutocompletion: true,
		    readOnly: true
		});
		editor.session.setOption("useWorker", true);
		
		if(content != undefined)
			editor.setValue(content+"\n");
		editor.clearSelection();
	}
	
	function dataLoad(showProgress)
	{
		var param = {
			function_id: "${paramMap.function_id}",
			curpage: data.nav.curpage,
			sch_log_cd: data.sch_log_cd,
		}
		
		if(showProgress == undefined) showProgress = true;
		
		JsonCall('${pageContext.request.contextPath}/log/getLogList.json', 'param=' + JSON.stringify(param), function(response) 
		{
			ret = JSON.parse(response.responseText);
			if(ret.functionlist.length == 1)
			{
				data.funObj = ret.functionlist[0];
				data.desBtnShow = data.funObj.des.length >= 200;
				data.list = ret.list;
				data.listCnt = data.list.length;
				data.totalCnt = ret.totalCount;
				callNav(data, ret.curpage, ret.lastpage);
				setCurLog(data.curLogId);
			}
			else
			{
				alert("정상적인 데이타 조회가 아닙니다.");
				pageLoad('/layout/function/list.do');
			}
			
			NtelsHB.render();
			
		},showProgress);
	}
	
	function callNav(data, curpage, lastpage)
	{
		data.nav.curpage = curpage;
		data.nav.lastpage = lastpage;

		data.nav.start = Math.floor((curpage-1)/10)*10 + 1;
		data.nav.end = data.nav.start + 10 - 1;
		
		if(data.nav.end > data.nav.lastpage) {
			data.nav.end = data.nav.lastpage;  
		}
	}
	
	function onNavClick(page)
	{
		data.nav.curpage = page;
		dataLoad();
	}
	
	function detailLog(log_id)
	{
		data.curView = "error";
		data.curLogId = log_id;
		setCurLog(log_id)
		NtelsHB.render();
	}
	
	function diffView(function_id , revision_id , log_id)
	{
		var param = {
			function_id: function_id,
			revision_id: revision_id
		};
		
		if(revision_id == undefined || revision_id == "")
		{
			fnModalAlert("리비전 정보가 부족합니다.","경고");
			return;
		}
		
		JsonCall('${pageContext.request.contextPath}/service/sourceCommitDiff.json', 'param=' + JSON.stringify(param), function(response) 
		{
			ret = JSON.parse(response.responseText);
			if(ret.success == "Y")
			{
				var source = "";
				var diff = "";
				if(ret.sourceList.length > 0)
				{
					source = ret.sourceList[0].fileContent
				}
				if(ret.diff.length > 0)
				{
					diff = "--- a/"+ret.diff[0].oldPath+"\n+++ b/"+ret.diff[0].newPath+"\n"+ret.diff[0].diff;
					// diff = "--- a/ \n+++ b/ \n"+ret.diff[0].diff;
				}
				else
				{
					diff = "--- a/ \n+++ b/ \n"+"변경내용이 없습니다.";
				}
				
				data.curView = "diff";			
				data.curDiff.source = source;
				data.curDiff.diff = diff;
				data.curLogId = log_id;
				setCurLog(log_id)
			}
			else
			{
				fnModalAlert("수정정보가 정상적으로 조회되지 않았습니다.","에러");				
			}
			NtelsHB.render();
			
		},true);
	}
	
	function setCurLog(log_id)
	{
		data.curLog = {};
		for(var ic=0;ic<data.list.length;ic++)
		{
			var log = data.list[ic];
			if(log.function_log_id == log_id)
			{
				data.curLog = log;
			}
		}
	}
	
	function showList()
	{
		data.curView = "list";
		NtelsHB.render();
	} 
	
	g_func["detailLog"] = detailLog;
	g_func["diffView"] = diffView;
	g_func["showList"] = showList;	
	
	componentLoad();
});



</script>


		<div id="handlebarRoot">

			{{component "title" }}
				<!-- inner -->
				<div class="page-content">

					<!-- 콘텐츠 영역 -->
					<div class="inner-wrap">
						
						<!-- 콘텐츠가 들어갑니다 -->
						{{component "info"}}

						<!-- tab -->
						<div class="nav-tabs-wrap">
							<ul class="nav nav-tabs nav-tabs-line">
								<li><a href="javascript:void(0)" onclick="loadPage('layout/function/detail.do?function_id=${paramMap.function_id}')">구성 정보​</a></li>
								<li><a href="javascript:void(0)" onclick="loadPage('layout/function/detail_monitoring.do?function_id=${paramMap.function_id}')">모니터링​</a></li>
								<li class="active"><a href="javascript:void(0)">로그 조회​</a></li>
							</ul>
						</div><!-- tab  end-->

						{{#if (eq curView "list") }}
						<section class="panel panel-bordered">
							<div class="panel-heading">
								<div class="row">
									<div class="col-md-7">
										<h3 class="panel-title">
											로그
										</h3>
									</div>
									<div class="col-md-5 text-right">
										<div class="form-inline panel-heading-right margin-top-3">
											<select name="" id="sch_log_cd" class="form-control form-control-sm">
												<option value="">전체</option>
												<option value="MODIFY">수정</option>
												<option value="ERROR">오류</option>
											</select>
										</div>
									</div>
								</div>
							</div> <!-- // panel-heading -->
							<div class="panel-body">
						
								<!-- 테이블형 -->
								<div class="table-wrap">
									{{#if (neq listCnt 0)}}
									{{component "listTable"}}
									{{component "navigation"}}
								{{else}}
									{{component "listEmpty"}}
								{{/if}}
								</div>
							</div>
						</section>
						{{/if}}
						
						
						{{#if (eq curView "diff") }}
						<section class="panel panel-bordered">
							<div class="panel-heading">
								<div class="form-inline clearfix">
									<a href="javascript:void(0);" class="panel-back-btn" title="뒤로가기" onclick="g_func.showList();return false;">
										<i class="xi-angle-left"></i>
									</a>
									<h3 class="panel-title">
										로그 정보
									</h3>
								</div>
							</div> <!-- // panel-heading -->
							<div class="panel-body">
								<ul class="list-dot margin-bottom-20">
									<li class="col-md-4">
										<b>로그 발생 일시 : </b>
										<span>{{curLog.reg_date_hms}}</span>
									</li>
									<li class="col-md-4">
										<b>구분 : </b>
										<span>{{curLog.log_cd_nm}}</span>
									</li>
									<li class="col-md-4">
										<b>수정 항목 : </b>
										<span>{{curLog.log_dtl_cd_nm}}</span>
									</li>
								</ul>
							</div> <!-- // panel-body -->
						</section>

						<section class="panel panel-bordered">
							<div class="panel-heading">
								<h3 class="panel-title">
									함수 수정 내역
								</h3>
							</div> <!-- // panel-heading -->
							<div class="panel-body">
								<div class="row">
									<div class="col-md-6">
										<h4 class="heading h4 border padding-20 margin-bottom-0">
											기존 함수
										</h4>
										<div id="sourceEditor" class="editorBox" style="height: ;">
										</div>
									</div>
									<div class="col-md-6 border-left">
										<h4 class="heading h4 border padding-20 margin-bottom-0">
											변경 함수​
										</h4>
										<div  class="editorBox" style="height:350px;overflow-y:auto;position:relative;">
											<div id="diffView"></div>
										</div>
									</div>
								</div>
							</div> <!-- // panel-body -->
						</section>
						{{/if}}
						
						{{#if (eq curView "error") }}
						<section class="panel panel-bordered">
							<div class="panel-heading">
								<div class="form-inline clearfix">
									<a href="javascript:void(0);" class="panel-back-btn" title="뒤로가기" onclick="g_func.showList();return false;">
										<i class="xi-angle-left"></i>
									</a>
									<h3 class="panel-title">
										로그 정보
									</h3>
								</div>
							</div> <!-- // panel-heading -->
							<div class="panel-body">
								<ul class="list-dot margin-bottom-20">
									<li class="col-md-6">
										<b>로그 발생 일시 : </b>
										<span>{{curLog.reg_date_hms}}​</span>
									</li>
									<li class="col-md-6">
										<b>구분 : </b>
										<span>{{curLog.log_cd_nm}}</span>
									</li>
								</ul>
							</div> <!-- // panel-body -->
						</section>

						<section class="panel panel-bordered">
							<div class="panel-heading">
								<h3 class="panel-title">
									로그 내역
								</h3>
							</div> <!-- // panel-heading -->
							<div class="panel-body">
								<div id="logEditor" class="editorBox" style="height: ;">
								</div>
							</div> <!-- // panel-body -->
						</section>
						{{/if}}
						
					</div>
				</div>
		</div><!-- end handlebarRoot -->

    