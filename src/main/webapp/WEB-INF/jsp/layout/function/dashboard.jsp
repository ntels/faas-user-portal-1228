<%@page import="com.fasterxml.jackson.databind.ObjectMapper"%>
<%@page import="kr.co.ntels.faas.dashboard.service.DashboardServive"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %> 
<spring:eval expression="@environment.getProperty('websocket.base.url')" var="webSocketBaseUrl" />     
    
   
<script>
var chart;
$(document).ready(function(){
	// debugger;
	
	NtelsHB.rootNodeSelector = "#handlebarRoot";
	
	var data = {
			list: {},
			listCnt: 0,
			callList: {},
			callListCnt: 0,
			runtime: {total: 0 , nodejs: 0 , python: 0},
			triggerInfo: {total: "0",http: "0",ceph: "0",mongodb: "0",postgresql: "0",timer: "0" },
			chartData: [{'title' : '미등록','value' : 1}],
			initCahrData: [{'title' : '미등록','value' : 1}],
			sch_month: <%=DashboardServive.getMonth()%>,
			sch_month_list: <%=DashboardServive.getMonthList()%>,
			now: new Date()
		};

	
	function componentLoad()
	{
		var context = "${pageContext.request.contextPath}";
		var rooturl = context + "/component/function/detail/";

		NtelsHB.registerComponent("title",rooturl+"title.do");
		NtelsHB.registerComponent("info",rooturl+"info.do");
		NtelsHB.loadComponent();
	}
	
	NtelsHB.onComponentReady = function()
	{
		NtelsHB.bind(data);	
		dataLoad();
	}
	
	NtelsHB.afterRender = function()
	{
		function reload(progress)
		{
			dataLoad(progress);
		}
		
		$("#sch_month").on("change",function(){
			data.sch_month = $(this).val();
			JsonCall('${pageContext.request.contextPath}/dashboard/callList.json', 'param=' + JSON.stringify({sch_month: data.sch_month}), function(response){
				ret = JSON.parse(response.responseText);
				data.callList = ret.callList;
				data.callListCnt = ret.callList.length || 0;
				NtelsHB.render();
			},false);
		});		
		
		$("#reloadBtn").on("click",function() {reload(true); return false;});
		$("#sch_month").val(data.sch_month).prop("selected",true);
		doughnutChart(data.triggerInfo);
	}
	
	function dataLoad(showProgress)
	{
		var param = {
				sch_month: data.sch_month,
			}
		
		if(showProgress == undefined) showProgress = true;
		
		
		JsonCall('${pageContext.request.contextPath}/dashboard/query.json', 'param=' + JSON.stringify(param), function(response) 
		{
			ret = JSON.parse(response.responseText);
			if(ret.runtime != undefined)
			{
				data.list = ret.list;
				data.listCnt = ret.list.length || 0;
				data.callList = ret.callList;
				data.callListCnt = ret.callList.length || 0;
				data.runtime = ret.runtime;
				data.triggerInfo = ret.triggerInfo;
				data.chartData = ret.chartData;
			}
			else
			{
				alert("정상적인 데이타 조회가 아닙니다.");
				pageLoad('/layout/function/list.do');
			}
			
			NtelsHB.render();
			
		},showProgress);
	}
	
	// Websocket
	(function()
	{
		var webSocketBaseUrl = '${webSocketBaseUrl}';
		if(webSocketBaseUrl != "")
		{
			HBWebSocket.getInstance("ws://<%=request.getServerName()%>:<%=request.getServerPort()%>/faas/service.do", onMessage);
			function onMessage(obj)
			{
				console.log("[DASHBOARD] : " + JSON.stringify(obj));
				if(obj.namespace == "faas-p"+$.cookie('PROJECT-ID'))
				{
					if(obj.source == "KUBE_WATCH")
						dataLoad(false);
				}
			}
		}		
	})();
	
	
	componentLoad();
	
});


function doughnutChart(triggerInfo){
	var ctx = document.getElementById('doughnutChart').getContext('2d');
	var chart = new Chart(ctx, {
		// The type of chart we want to create
		type: 'doughnut',

		// The data for our dataset
		data: {
			labels: ['HTTP', 'Ceph', 'MongoD', 'PostgreSQL', '타이머'],
			datasets: [{
				data: [triggerInfo.http, triggerInfo.ceph, triggerInfo.mongodb, triggerInfo.postgresql, triggerInfo.timer],
				backgroundColor: [
					'#ff6384',
					'#ffce56',
					'#36a2eb',
					'#1f77b4',
					'#00bcd4',
				]
			}]
		},

		// Configuration options go here
		options: {
			maintainAspectRatio: false,
			cutoutPercentage: 75,
			legend: {
				labels: {
					usePointStyle: true,
					padding: 30,
					fontColor: "#000",
					fontSize: 12,
					boxWidth: 15
				},
				position: "none"
			},
		}
	});
};

</script>

<div id="handlebarRoot">
	<main id="contentWrap" class="dashboard-wrap">
		<div class="page-title dashboard-top">
			<div class="page-info">프로젝트명</div>
			<div class="page-desc">
				<span class="date">
				 	{{formatDate now '[Today is] dddd, DD MMMM YYYY'}}
					
				</span>
			</div>
			<div class="dashboard-control-area">
				<a href="" class="dashboard-size btn btn-nostyle btn-xs"></a>
				<a href="#" id="reloadBtn" class="dashboard-refresh btn btn-nostyle btn-xs">
					<i class="xi-renew"></i>
				</a>
			</div>
		</div>

		<!-- inner -->
		<div class="page-content">

			<!-- 콘텐츠 영역 -->
			<div class="inner-wrap">

				<!-- 상단 알림 -->
				<div class="alert alert-type2 alert-info alert-dismissible show" role="alert">
					<i data-feather="alert-circle"></i>
					<div class="text-area">
						Function 서비스는  필요한 기능을 함수 단위로 나누어 구현하고 이를 서비스합니다. 이 때 구현된 함수는 연결된 트리거 이벤트(HTTP  호출, 스케줄, DB 등)가 발생할 때 실행됩니다. ​<br>
						서버는 계속 대기하며 자원을 사용하지 않고, 트리거 이벤트가 발생할 때만 실행되어 자원 절감에 효과적입니다.
						<button type="button" class="close" data-dismiss="alert" aria-label="Close">
							<span aria-hidden="true">&times;</span>
						</button>
					</div>
				</div>

				<div class="row">
					<div class="col-md-6">
						<div class="panel panel-bordered">
							<div class="panel-heading dashboard-t1">
								<h3 class="panel-title">
									<div class="row">
										<div class="col-md-8">
											<h3 class="heading">
												함수
											</h3>
										</div>
										<div class="col-md-4">
											<div class="text-right">
												<a href="#" class="more" onclick="loadPage('layout/function/list.do')">더 보기</a>
											</div>
										</div>
									</div>
								</h3>
							</div>
							<div class="panel-body padding-right-10 padding-top-10" style="height: 300px">
								<button class="btn btn-default" onclick="loadPage('layout/function/regFunction.do')">
									<div class="feather-icon">
										<i data-feather="plus-square"></i>
									</div>
									함수 등록​
								</button>
								{{#if (eq listCnt 0)}}
								<div class="arr-desc-text small">
									직접 코드를 입력하거나 템플릿을 이용하여 함수를 등록합니다.
								</div>
								<div class="nodata-box type3">
									<div class="nodata">
										<div class="img-div"></div>
										<p>현재 등록된 함수가 없습니다.</p>
									</div>
								</div>
								{{else}}
								<div class="scroll-content margin-top-10" style="height: 210px">
									<ul>
									{{#each list}}
										<li>
											<div class="row">
												<div class="col-md-8">
													<a href="#" onclick="loadPage('layout/function/detail.do?function_id={{this.function_id}}')"><b>{{this.dsp_name}}</b></a>
												</div>
												<div class="col-md-4">
													<div class="text-right padding-right-10">
														<span class="state">
															{{#if (eq this.stat_cd 'INIT')}}
															<div class="state-round reg"></div> 미배포
															{{else if (eq this.stat_cd 'REG')}}
															<div class="state-round waiting"></div> 배포 중
															{{else if (eq this.stat_cd 'NORMAL')}}
															<div class="state-round success"></div> 배포 완료
															{{else if (eq this.stat_cd 'ERROR')}}
															<div class="state-round error"></div> 배포 실패
															{{else if (eq this.stat_cd 'SYNC_ERROR')}}
															<div class="state-round error"></div> 배포 정보 체크
															{{else if (eq this.stat_cd 'DISABLE')}}
															<div class="state-round inactive"></div> 비활성
															{{else}}
															<div class="state-round reg"></div> unknown
															{{/if}}
														</span>
													</div>
												</div>
											</div>
											<p class="ellipsis-2 margin-top-5 margin-bottom-15">
												{{this.des}}
											</p>
										</li>
									{{/each}}
									</ul>
								</div>
								{{/if}}
							</div>
						</div>
					</div>
					<div class="col-md-6">
						<div class="panel panel-bordered">
							<div class="panel-heading dashboard-t1">
								<h3 class="panel-title">
									런타임 등록 현황​
								</h3>
							</div>
							<div class="panel-body" style="height: 300px">
								<div class="row list-view-ranking type2 margin-top-40">
									<div class="col-md-4">
										<div class="img-zone img-func-all img-size70">
											<span class="sr-only">전체</span>
										</div>
										<div class="text-box margin-top-20">
											<span class="title">
												전체​
											</span>
											<p class="num">
												{{runtime.total}}
											</p>
										</div>
									</div>
									<div class="col-md-4">
										<div class="img-nodejs img-size70">
											<span class="sr-only">Node</span>
										</div>
										<div class="text-box margin-top-20">
											<span class="title">
												Node.js​
											</span>
											<p class="num">
												{{runtime.nodejs}}
											</p>
										</div>
									</div>
									<div class="col-md-4">
										<div class="img-python img-size70">
											<span class="sr-only">Python​</span>
										</div>
										<div class="text-box margin-top-20">
											<span class="title">
												Python​
											</span>
											<p class="num">
												{{runtime.python}}
											</p>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>

				<div class="row">
					<div class="col-md-6">
						<div class="panel panel-bordered dashboard-panel dashboard-panel-type1" style="height:350px">
							<div class="panel-body">
								<h3 class="heading h3 margin-bottom-25">
									트리거 등록 현황
								</h3>
								<%-- TODO: chart 추가 --%>
								<div class="chart-area margin-top-40 padding-left-60" style="width: calc(100% - 300px); height: 180px;float: left;"><iframe class="chartjs-hidden-iframe" tabindex="-1" style="display: block; overflow: hidden; border: 0px; margin: 0px; top: 0px; left: 0px; bottom: 0px; right: 0px; height: 100%; width: 100%; position: absolute; pointer-events: none; z-index: -1;"></iframe>
									<canvas id="doughnutChart" width="380" height="180" style="display: block; width: 380px; height: 180px;"></canvas>
								</div>
								<ul class="chart-label-list left type1 margin-top-40" style="width:40%;float: left;">
									<li class="clearfix">
										<div class="category pull-left">
											<span class="bul-round" style="border:2px solid #e7e9ed;"></span>
											<span class="text">전체</span>
										</div>
										<div class="pull-right count">
											{{triggerInfo.total}}
										</div>
									</li>
									<li class="clearfix">
										<div class="category pull-left">
											<span class="bul-round" style="background: #ff6384;"></span>
											<span class="text">HTTP</span>
										</div>
										<div class="pull-right count">
											{{triggerInfo.http}}
										</div>
									</li>
									<li class="clearfix">
										<div class="category pull-left">
											<span class="bul-round" style="background: #ffce56;"></span>
											<span class="text">Ceph</span>
										</div>
										<div class="pull-right count">
											{{triggerInfo.ceph}}
										</div>
									</li>
									<li class="clearfix">
										<div class="category pull-left">
											<span class="bul-round" style="background: #36a2eb;"></span>
											<span class="text">MongoDB</span>
										</div>
										<div class="pull-right count">
											{{triggerInfo.mongodb}}
										</div>
									</li>
									<li class="clearfix">
										<div class="category pull-left">
											<span class="bul-round" style="background: #1f77b4;"></span>
											<span class="text">PostgreSQL</span>
										</div>
										<div class="pull-right count">
											{{triggerInfo.postgresql}}
										</div>
									</li>
									<li class="clearfix">
										<div class="category pull-left">
											<span class="bul-round" style="background: #00bcd4;"></span>
											<span class="text">타이머</span>
										</div>
										<div class="pull-right count">
											{{triggerInfo.timer}}
										</div>
									</li>
								</ul>
								
							</div>
						</div>
					</div>
					<div class="col-md-6">
						<div class="panel panel-bordered dashboard-panel dashboard-panel-type1" style="height:350px">
							<div class="panel-body padding-right-10">
								<div class="row">
									<div class="col-md-8">
										<h3 class="heading">
											월간 함수 호출 내역
										</h3>
									</div>
									<div class="col-md-4">
										<div class="text-right">
											<div class="form-group inline-block margin-bottom-0">
												<select id="sch_month" class="form-control">
												{{#each sch_month_list}}
													<option value='{{this}}'>{{this}}​</option>
												{{/each}}
												</select>
											</div>
										</div>
									</div>
								</div>
								<%-- {{#if (eq callListCnt 0)}} --%>
								{{#if (eq callListCnt 0)}}
								<div class="nodata-box type1">
									<div class="nodata">
										<div class="img-div"></div>
										<p>선택한 월에 호출한 내역이 없습니다.​</p>
									</div>
								</div>
								{{else}}
								<div class="all_view_list scroll-content margin-top-30 margin-bottom-10" style="height: 220px">
									<ul>
										{{#each callList}}
										<li>
											<span class="name"><a href="SC_FS_02_010.html" class="ellipsis-2">{{this.dsp_name}}​</a></span>
											<span class="count padding-right-15">{{this.cnt}}건​</span>
										</li>
										{{/each}}
									</ul>
								</div>
								{{/if}}
							</div>
						</div>
					</div>
				</div>
			</div>
			
		</div>
		<!-- // inner -->
	</main>
</div>

    