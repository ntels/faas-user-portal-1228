<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
    
<script>
$(document).ready(function(){
	debugger;
	
	NtelsHB.rootNodeSelector = "#handlebarRoot";
	
	var data = {
			isViewNew: false
			,isCard: true
			,isClickable: true  
			,table:{caption:"" , widths:['80','20%','20%',null] , headers:['선택','함수 이름','런타임','설명']}
			,list:[]
			,listCnt:0
			,totalCnt:0
			,nav:{curpage:1,lastpage:1,start:1,end:1,onclick:onNavClick}
		};
	
	
	function componentLoad()
	{
		var context = "${pageContext.request.contextPath}";
		var rooturl = context + "/component/function/regFunction/";

		NtelsHB.registerComponent("title",rooturl+"title.do");
		NtelsHB.registerComponent("selector",rooturl+"selector.do");
		NtelsHB.registerComponent("listCardItem",rooturl+"listCardItem.do");
		NtelsHB.registerComponent("listTableItem",rooturl+"listTableItem.do");
		NtelsHB.registerComponent("viewChanger","component/viewChanger.do");
		NtelsHB.loadComponent();
	}
	
	NtelsHB.onComponentReady = function()
	{
		NtelsHB.bind(data);	
		dataLoad();
	}
	
	NtelsHB.afterRender = function(){
		$("#reloadBtn").on("click",function()
		{
			dataLoad();
		});
		
		$("#viewCard").on("click",function(){
			if(!data.isCard)
			{
				data.isCard = true;
				NtelsHB.render();
			}  
		});
		
		$("#viewList").on("click",function(){
			if(data.isCard)
			{
				data.isCard = false;
				NtelsHB.render();
			}
		});
		
		$(document).on('click', '.box-click > li > div', function() {
			if($(this).hasClass('on')) {
				return false;	
			} else {
				$(this).closest('.box-click').find('div').removeClass('on');
				$(this).addClass('on')
			};
		});
		
		$(document).on('click', '.row_active tbody tr', function() {
			$(this).parents('tbody').find('tr').removeClass('active')
			$(this).addClass('active');
			$(this).find('input[type="radio"]').prop('checked', true);
		});
		
		
		$("#nextBtn").on("click",function(){ // 다음버튼
			
			var template_id = seletedTemplate();
			if(template_id == undefined)
			{
				fnModalAlert('템플릿을 선택하세요.');
                $('button[name=btnModalAlertClick]').click(function(){
                	$('.modal').modal('hide');
                });
				return;
			}
			
			loadPage('layout/function/regFunctionTemplateStep2.do?template_id='+template_id);
		});
		
		
		
	}
	
	function seletedTemplate()
	{
		var obj = data.isCard ? $(".box-click > li .on") : $(".row_active tbody .active");
		if(obj.length == 1)
		{
			var prop = $(obj).attr("template_id");
			return prop;
		}
		
		return undefined;
		
	}
	
	function dataLoad(showProgress)
	{
		var param = {
				user_id: USER_ID,
				user_name: USER_NAME,
				project_id: $.cookie('PROJECT-ID'),
				sch_template: '',
				curpage: data.nav.curpage,
		}
		
		if(showProgress == undefined) showProgress = true;
		
		JsonCall('${pageContext.request.contextPath}/template/getTemplateList.json', 'param='+JSON.stringify(param) , function(response) {
			
			ret = JSON.parse(response.responseText);
			if(ret.list != undefined)
			{
				data.list = ret.list;
				data.listCnt = data.list.length;
				data.totalCnt = ret.totalCount;
				callNav(data, ret.curpage, ret.lastpage);
				
				//test
				if(data.list.length > 0)
				{
					data.list[0].isTop = true;
					for(var i=0;i<data.list.length;i++)
					{
						data.list[i].temp_function_id = data.list[i].function_id;
					}
				}
				//test
			}
			else
			{
				alert("정상적인 데이타 조회가 아닙니다.");
				loadPage('layout/function/list.do');
			}
			
			NtelsHB.render();
			$("#rightContent").show();
			
		},showProgress);
	}
	
	function callNav(data, curpage, lastpage)
	{
		data.nav.curpage = curpage;
		data.nav.lastpage = lastpage;

		data.nav.start = Math.floor((curpage-1)/10)*10 + 1;
		data.nav.end = data.nav.start + 10 - 1;
		
		if(data.nav.end > data.nav.lastpage) {
			data.nav.end = data.nav.lastpage;  
		}
	}
	
	function onNavClick(page)
	{
		data.nav.curpage = page;
		dataLoad();
	}
	
	
	componentLoad();	
	
});
</script>

		<div id="handlebarRoot">
		
			{{component "title"}}
			
			<!-- content -->
			<div class="content">

				<div class="row_wrap">
					<div class="ncz_panel">
						<div class="panel_head">
							<span class="tit">함수 등록</span>
						</div>
						<div class="panel_cnt">
							<div class="inp_top">
								<span>* 는 필수입력 사항입니다.</span>
							</div>
							
							{{component "selector"}}

							<div class="inp_field">
								<div class="top_area">
									<span class="tit imp">템플릿 선택​</span>
									<em class="ico-info1 ico right" data-toggle="tooltip" data-placement="right" title="" data-original-title="사전에 만들어진 함수를 선택하여 
									자동으로 생성할 수 있습니다."></em>
								</div>

								<div class="ncz_panel">
									<div class="panel_head pd0 pb10">
										<span class="inp_row search">
											<input type="text" class="inp" style="width: 450px;" placeholder="키워드로 템플릿을 검색해 보세요."/><!--// false, true class 추가로 컨트롤-->
											<button type="button" name="button" class="btn search" title="검색">
												<span class="ico">검색</span>
											</button>
										</span>
										<div class="right_area">
											{{component "viewChanger"}}
										</div>
									</div>
								{{#if isCard }}
									<div class="panel_cnt pd0 pt10">
									{{#if (neq listCnt 0)}}
										{{component "listCard"}}
										{{component "navigation"}}
									{{else}}
										{{component "listEmpty"}}
									{{/if}}
										
									</div>
								{{else}}
									<div class="panel_cnt pd0 pt10">
									{{#if (neq listCnt 0)}}
										{{component "listTable"}}
										{{component "navigation"}}
									{{else}}
										{{component "listEmpty"}}
									{{/if}}
									</div>
								
								{{/if}}
								</div>
							</div><!-- //템플릿 선택​ -->

							<!-- button area -->
							<div class="btnArea confirm mt80">
								<button type="button" id="nextBtn" class="btn btn-md btn-color1" title="다음">다음</button>
								<button type="button" class="btn btn-md btn-color2" onclick="loadPage('layout/function/list.do')" title="취소">취소</button>
							</div><!-- //button area -->

						</div>
					</div>
				</div><!--//row_wrap-->

			</div>
			<!-- //content -->
		</div>
