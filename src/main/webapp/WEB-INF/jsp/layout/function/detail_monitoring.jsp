<%@page import="com.fasterxml.jackson.databind.ObjectMapper"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %> 
<spring:eval expression="@environment.getProperty('websocket.base.url')" var="webSocketBaseUrl" />     
    
   
<script>

$(document).ready(function(){
	debugger;
	
	NtelsHB.rootNodeSelector = "#handlebarRoot";
	
	var data = {
			funObj:{},
			desBtnShow: false,
			unitType: 'D'  <% //일간단위로 고정함. hide처리됨. %>
			
		};

	
	function componentLoad()
	{
		var context = "${pageContext.request.contextPath}";
		var rooturl = context + "/component/function/detail/";

		NtelsHB.registerComponent("title",rooturl+"title.do");
		NtelsHB.registerComponent("info",rooturl + "info.do?function_id=" + ${paramMap.function_id} );
		NtelsHB.loadComponent();
	}
	
	NtelsHB.onComponentReady = function()
	{
		NtelsHB.bind(data);	
		dataLoad();
	}
	
	NtelsHB.afterRender = function()
	{
		NtelsHB.func.dataLoad = dataLoad;
		titleAfterRender();
		infoAfterRender();
		
		function reload(progress)
		{
			data.reloadTerm = $("#reloadTerm").val();
			data.reloadType = $("#reloadType").val();
			dataLoad(progress);
		}
		$("#reloadBtn").on("click",function() {reload(true);});
		$("#reloadGraphBtn").on("click",function() {graphLoad();});
		$("#unitType").on("change",function(){
			data.unitType = $(this).val();
			graphLoad();
		});
		$("#unitType").val(data.unitType).prop("selected",true);
		$("#unitType").hide();
		
		graphLoad();
	}
	
	function dataLoad(showProgress)
	{
		var param = {
			function_id: "${paramMap.function_id}",
		}
		
		if(showProgress == undefined) showProgress = true;
		
		JsonCall('${pageContext.request.contextPath}/service/getFunction.json', 'param=' + JSON.stringify(param), function(response) 
		{
			ret = JSON.parse(response.responseText);
			if(ret.list.length == 1)
			{
				data.funObj = ret.list[0];
				data.desBtnShow = data.funObj.des.length >= 200;
			}
			else
			{
				alert("정상적인 데이타 조회가 아닙니다.");
				pageLoad('/layout/function/list.do');
			}
			
			NtelsHB.render();
			
		},showProgress);
	}
	
	function graphLoad()
	{
		var param = {
			function_id: "${paramMap.function_id}",
			stepCnt: 7,
			unitType: data.unitType,
		};
		
		var json = "param="+JSON.stringify(param);
		JsonCall("${pageContext.request.contextPath}/queryGraph.json" , json ,graphDataBind,false);
	}
	
	componentLoad();
	
});


function graphDataBind(data)
{
	if(data.responseText != undefined)
	{
		debugger;
		var obj = JSON.parse(data.responseText);
		if(obj.listResult != undefined && obj.listC3Result instanceof Array)
		{
		}
		if(obj.mapC3Result != undefined && obj.mapC3Result instanceof Object)
		{
			linechart1(obj.mapC3Result);
			linechart2(obj.mapC3Result);
			linechart3(obj.mapC3Result);
		}
	}
}

function linechart1(data) {

	data.time.unshift("time");
	data.cnt.unshift("호출 건수");
	
	var a = c3.generate({
		bindto: "#linechart1",
		data: {
			x: 'time', 
			columns: [
				 data.time,data.cnt
			],
			colors: {
				'호출 건수': '#1f77b4'
			}
		},
		axis: {
			x: {
				type: 'category',
				padding: {top: 0, bottom: 0},
			},
			y: {
				min: 0,
				padding: {top: 0, bottom: 0},
			},
		},
	});
};
function linechart2(data) {
	
	data.rate.unshift("성공률");
	data.rate1.unshift("실패율");
	
	var a = c3.generate({
		bindto: "#linechart2",
		data: {
			x: 'time',
			columns: [
				data.time,data.rate,data.rate1
			],
			colors: {
				'성공률​': '#1f77b4',
				'실패율​': '#f96197'
			}
		},
		axis: {
			x: {
				type: 'category',
				padding: {top: 0, bottom: 0},
			},
			y: {
				min: 0,
				max: 100,
				padding: {top: 0, bottom: 0},
				tick: {
					count: 6
				}
			}
		},
	});
};
function linechart3(data) {
	
	data.p99.unshift("p99");
	data.p90.unshift("p90");
	data.p50.unshift("p50");
	
	var a = c3.generate({
		bindto: "#linechart3",
		data: {
			x: 'time', 
			columns: [
				 data.time,data.p99,data.p90,data.p50
			],
			colors: {
				'p99​': '#4674d2',
				'p90​': '#83d0ef',
				'p50​': '#81d54e',
			}
		},
		axis: {
			x: {
				type: 'category',
				padding: {top: 0, bottom: 0},
			},
			y: {
				min: 0,
				padding: {top: 0, bottom: 0},
			},
		},
	});
}

</script>


		<div id="handlebarRoot">
			
			{{component "title"}}
			
			<!-- content -->
			<div class="content">
				
				{{component "info"}}
				
				<div class="row_wrap">
					<div class="tabWrap type2">
						<ul class="nav nav-tabs">
							<li><a href="#" onclick="loadPage('layout/function/detail.do?function_id=${paramMap.function_id}')" title="구성 정보">구성 정보</a></li>
							<li class="active"><a href="javascript:void(0);" title="모니터링">모니터링</a></li>
							<li><a href="#" onclick="loadPage('layout/function/detail_log.do?function_id=${paramMap.function_id}')" title="로그 조회​">로그 조회​</a></li>
						</ul>
					</div><!--//tabWrap-->
				</div><!--//row_wrap-->


				<section class="panel panel-bordered">
							<div class="panel-heading">
								<div class="row">
									<div class="col-md-7">
										<h3 class="panel-title">
											모니터링​
										</h3>
									</div>
									<div class="col-md-5 text-right">
										<div class="panel-heading-right margin-top-3">
											<a href="${pageContext.request.contextPath}/grafana/main.do?function_id=${paramMap.function_id}" target="_blank"><span class="img-grafana small"></span></a>
											<select class="form-control form-control-sm" style="display: none;width:80px;" id="unitType"> <% //일간단위로 고정함. hide처리됨. %>
												<option value="H">시간</option>
												<option value="D" selected>일</option>
												<option value="W">주</option>
												<option value="M">개월</option>
											</select>
											<button class="btn btn-sm btn-default margin-left-10" id="reloadGraphBtn">
												<i class="xi-renew"></i>
											</button>
										</div>
									</div>
								</div>
								
							</div> <!-- // panel-heading -->
							<div class="panel-body">
								<div class="row">
									<div class="col-md-4">
										<h4 class="heading h4">
											호출 건 수​
										</h4>
										<div id="linechart1" class="chart" style="height: 300px"></div>
									</div>
									<div class="col-md-4">
										<h4 class="heading h4">
											성공률​
										</h4>
										<div id="linechart2" class="chart" style="height: 300px"></div>
									</div>
									<div class="col-md-4">
										<h4 class="heading h4">
											지속 시간 (Duration)​
										</h4>
										<div id="linechart3" class="chart" style="height: 300px"></div>
									</div>
								</div>
							
							</div> <!-- // panel-body -->
						</section>

			</div>
			<!-- //content -->
		</div>

    