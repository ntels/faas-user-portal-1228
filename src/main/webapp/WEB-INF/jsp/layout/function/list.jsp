<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %> 
<spring:eval expression="@environment.getProperty('websocket.base.url')" var="webSocketBaseUrl" />
    
<script>

$(document).ready(function(){
	debugger;
	
	NtelsHB.rootNodeSelector = "#handlebarRoot";
	
	var data = {
			isFirst: true
			,isCard: true
			,isClickable: false
			,table:{caption:"" , widths:[null,150,150,100,100,200,200] , headers:['함수','상태','런타임','트리거','대상','등록 일시','등록자']}
			,list:[]
			,listCnt:0
			,totalCnt:0
			,nav:{curpage:1, lastpage:1, start:1, end:1, limit:10, selectedlimit:10, onclick:onNavClick}
			,sch_runtime:''
			,order_by:'CREATE' 
		};
	
	function componentLoad()
	{
		var context = "${pageContext.request.contextPath}";
		var rooturl = context + "/component/function/list/";
		var commurl = context + "/common/component/list/";
		
		NtelsHB.registerComponent("listCard",commurl+"/listCard.do");
		NtelsHB.registerComponent("listEmpty",commurl+"/listEmpty.do");
		NtelsHB.registerComponent("listTable",commurl+"/listTable.do");
		NtelsHB.registerComponent("navigation",commurl+"/navigation.do");
		
		NtelsHB.registerComponent("title",rooturl+"title.do");
		NtelsHB.registerComponent("listCardItem",rooturl+"listCardItem.do");
		NtelsHB.registerComponent("listTableItem",rooturl+"listTableItem.do");
		NtelsHB.registerComponent("listSearch",rooturl+"listSearch.do");
		NtelsHB.registerComponent("viewChanger","component/viewChanger.do");
		
		NtelsHB.loadComponent();
	}
	
	NtelsHB.onComponentReady = function()
	{
		NtelsHB.bind(data);	
		dataLoad();
	}
	
	NtelsHB.afterRender = function(){

		$("#reloadBtn").on("click",function(){
			dataLoad();
		});
		
		$("#sch_runtime").on("change",function(){
			data.sch_runtime = $(this).val();
			data.nav.curpage = 1;
			
			dataLoad();
		});
		
		$("#order_by").on("change",function(){
			data.order_by = $(this).val();
			data.nav.curpage = 1;
			
			dataLoad();
		});
		
		$("#viewCard").on("click",function(){
			if(!data.isCard)
			{
				data.isCard = true;
				data.nav.limit = 12;
				data.nav.curpage = 1;
				
				dataLoad();
			}  
		});
		
		$("#viewList").on("click",function(){
			if(data.isCard)
			{
				data.isCard = false;
				data.nav.limit = 10;
				data.nav.selectedlimit = 10;
				data.nav.curpage = 1;
				
				dataLoad();
			}
		});
		
		$("#limit-list-per-page").on("change", function() {
			data.nav.limit = $(this).val();
			data.nav.selectedlimit = $(this).val();
			data.nav.curpage = 1;
			
			dataLoad();
		});
		
		$("#sch_runtime").val(data.sch_runtime).prop("selected",true);
		$("#order_by").val(data.order_by).prop("selected",true);
	}
	
	
	function dataLoad(showProgress)
	{
		if(data.isCard)
			data.nav.limit = 12;
		
		var param = {
				user_id: USER_ID,
				user_name: USER_NAME,
				project_id: $.cookie('PROJECT-ID'),
				sch_runtime: data.sch_runtime,
				order_by: data.order_by,
				curpage: data.nav.curpage, 
				limit: data.nav.limit
		}
		
		if(showProgress == undefined) showProgress = true;
		
		JsonCall('${pageContext.request.contextPath}/service/getFunctionList.json', 'param='+JSON.stringify(param), function(response) {
			
			ret = JSON.parse(response.responseText);
			
			data.isFirst = false;
			if(ret.list.length == 0 && data.sch_runtime == "")
				data.isFirst = true;
			else
			{
				data.list = ret.list;
				data.listCnt = data.list.length;
				data.totalCnt = ret.totalCount;
				callNav(data, ret.curpage, ret.lastpage);
			}
			
			NtelsHB.render();
			
			if(!data.isCard) {
				$("#limit-list-per-page").val(data.nav.selectedlimit).attr("selected", "selected");
			}
			
            $("#rightContent").show();

        },showProgress);
	}
	
	function callNav(data, curpage, lastpage)
	{
		data.nav.curpage = curpage;
		data.nav.lastpage = lastpage;

		data.nav.start = Math.floor((curpage-1)/10)*10 + 1;
		data.nav.end = data.nav.start + 10 - 1;
		
		if(data.nav.end > data.nav.lastpage) {
			data.nav.end = data.nav.lastpage;  
		}
	}
	
	function onNavClick(page)
	{
		data.nav.curpage = page;
		dataLoad();
	}

	// Websocket
	(function()
	{
		var webSocketBaseUrl = '${webSocketBaseUrl}';
		if(webSocketBaseUrl != "")
		{
			HBWebSocket.getInstance("${webSocketBaseUrl}/faas/service.do", onMessage);
			function onMessage(obj)
			{
				console.log("[FUNCTION LIST] : " + JSON.stringify(obj));
				if(obj.namespace == "faas-p"+$.cookie('PROJECT-ID'))
				{
					if(obj.source == "KUBE_WATCH")
						dataLoad(false);
				}
			}
		}
	})();
	
	componentLoad();
	
});

</script>

<!-- container -->
<div id="handlebarRoot">
	<main id="contentWrap">

		{{component "title"}}

		<!-- inner -->
		<div class="page-content">

			<!-- 콘텐츠 영역 -->
			<div class="inner-wrap">
				<button class="btn btn-lg btn-primary" onClick="loadPage('layout/function/regFunction.do')">
					<div class="feather-icon">
						<i data-feather="command"></i>
					</div>
					<span>함수 등록</span>
				</button>
				
				{{#if isFirst}}
				<!-- nodata -->
				<div class="arr-desc-text">
					직접 코드를 입력하거나 템플릿을 이용하여 함수를 등록합니다.
				</div>
				<div class="text-center blank-page">
					<div class="img-report img-size100"></div>
					<div class="cont">
						<p>함수를 등록한 후, 트리거 및 백엔드 서비스를 다수 연결하여 자유롭게 서비스를 운영할 수 있습니다.</p>
						<p>함수는 <span class="point">직접 입력하거나 hub-PoP에서 제공하는 템플릿을 이용</span> 하여 간편하게 만들 수 있습니다.<p>
							<p><span class="point">‘함수 등록’</span> 을 통해 필요한 함수를 손쉽게 만들어 보세요.</p>
					</div>
				</div>
				{{else}}
				<!-- title -->
				{{component "listSearch"}}
					{{#if isCard}}
						{{#if (neq listCnt 0)}}
							<!-- 카드형 -->
							{{component "listCard"}}
							<!-- 페이징 -->
							{{component "navigation"}}
						{{else}}
							<!-- 데이터가 없습니다. -->
							{{component "listEmpty"}}
						{{/if}}
					{{else}}
						{{#if (neq listCnt 0)}}
						<section class="panel panel-bordered">
							<div class="panel-body padding-0">
							<!-- 테이블형 -->
							{{component "listTable"}}
							<!-- 페이징 -->
							{{component "navigation"}}
							</div>
						</section>
						{{else}}
							<!-- 데이터가 없습니다. -->
							{{component "listEmpty"}}
						{{/if}}
					{{/if}}
				{{/if}}
			</div>
			
		</div>
		<!-- // inner -->

	</main>
</div> <!-- // #container -->