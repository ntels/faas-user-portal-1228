<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
    
<script>
$(document).ready(function(){
	debugger;
	
	NtelsHB.rootNodeSelector = "#handlebarRoot";
	
	var data = {
			funObj: {},
		};
	
	function componentLoad()
	{
		var context = "${pageContext.request.contextPath}";
		var rooturl = context + "/component/function/create/";

		NtelsHB.loadComponent();
	}
	
	NtelsHB.onComponentReady = function()
	{
		NtelsHB.bind(data);	
		NtelsHB.render();
		$("#rightContent").show();
	}
	
	NtelsHB.afterRender = function()
	{
		$("#reloadBtn").on("click",function()
		{
			loadPage("layout/function/modify.do");
		});
		
		
	}
	
	function dataLoad(showProgress)
	{
		if(showProgress == undefined) showProgress = true;
		
		/*JsonCall('${pageContext.request.contextPath}/service/getFunction.json', 'function_id=${paramMap.function_id}', function(response) {
			
			ret = JSON.parse(response.responseText);
			if(ret.list.length == 1)
			{
				data.funObj = ret.list[0];
			}
			else
			{
				alert("정상적인 데이타 조회가 아닙니다.");
				pageLoad('/layout/function/list.do');
			}
			
			NtelsHB.render();
			$("#rightContent").show();
			
		},showProgress);*/
	}
	
	
	componentLoad();	
	
	
});
</script>

	<div id="handlebarRoot">
		<div class="titWrap">
				<h2 class="h2">함수 관리​</h2>
				<div class="btnArea">
					<div class="tool_wrap">
						<button type="button" name="button" class="btn btn-list only-ico ico-list" data-placement="bottom" data-toggle="tooltip" data-original-title="목록보기" onClick="loadPage('layout/function/list.do')">
							<span class="ico">목록보기</span>
						</button>
					</div>
					<div class="tool_wrap">
						<button type="button" name="button" id="reloadBtn" class="btn btn-list only-ico ico-refresh" data-placement="bottom" data-toggle="tooltip" data-original-title="새로고침">
							<span class="ico">새로고침</span>
						</button>
					</div>
				</div>
			</div>
			
			
			<!-- content -->
			<div class="content">

				<div class="row_wrap">
					<div class="ncz_panel">
						<div class="panel_head">
							<span class="tit">함수 등록 – 템플릿 kinesis-firehose-syslog-to-json 설정</span>
						</div>
						<div class="panel_cnt">
							<div class="inp_top">
								<span>* 는 필수입력 사항입니다.</span>
							</div>
							
							<div class="inp_field">
								<div class="top_area">
									<span class="tit imp">함수 이름​</span>
									<em class="ico-info1 ico right" data-toggle="tooltip" data-placement="right" title="" data-original-title="공백 없이 영문 대소문자, 숫자, 하이픈(-), 언더바(_)만 입력해 주세요."></em>
								</div>
								<div class="cnt">
									<input type="text" class="inp" placeholder="함수 이름을 입력해 주세요.​" style="width:50%;" />
								</div>
							</div><!--// 함수 이름 -->
						</div>

						<!-- HTTP 트리거 설정 -->
						<div class="panel_cnt">
							<div class="inp_field">
								<span class="sub_tit">HTTP 트리거 설정</span>
								<div class="top_area mt20">
									<div class="tit_wrap">
										<span class="tit imp">API 이름</span>
										<em class="ico-info1 ico right" data-toggle="tooltip" data-placement="right" data-html="true" title="" data-original-title="API 이름을 입력하면, 자동으로 엔드포인트가 생성됩니다. 
										API 이름은 공백없이 영문 소문자와 언더바(_)만 입력 가능합니다."></em>
									</div>
								</div>
								<div class="cnt">
									<input type="text" placeholder="API 이름 입력 (예 : api_name)" class="inp" style="width:50%;">
								</div>
							</div><!--//inp_field-->
						</div>

						<!-- Ceph 트리거 설정 -->
						<div class="panel_cnt">
							<div class="inp_field">
								<span class="sub_tit">Ceph 트리거 설정</span>
								<div class="top_area mt20">
									<div class="tit_wrap">
										<span class="tit imp">버킷</span>
										<em class="ico-info1 ico right" data-toggle="tooltip" data-placement="right" data-html="true" title="" data-original-title="사전에 생성해둔 버킷을 선택해 주세요. 버킷이
										없으면 이용하실 수 없습니다."></em>
									</div>
								</div>
								<div class="cnt">
									<select class="inp" style="width:50%;">
										<option value="">버킷 선택</option>
									</select>
								</div>
							</div><!--//inp_field-->
							<div class="inp_field">
								<div class="top_area">
									<div class="tit_wrap">
										<span class="tit imp">트리거 이벤트 유형</span>
										<em class="ico-info1 ico right" data-toggle="tooltip" data-placement="right" data-html="true" title="" data-original-title="함수가 실행될 트리거 이벤트를 선택합니다. 선택한
										이벤트가 발생할 경우에 한하여 함수가 실행됩니다."></em>
									</div>
								</div>
								<ul class="box_lst_etc type2 box-click">
									<li class="sec on">
										<div class="imgIcon type2">
											<div class="icoImg icoImg-file-new">파일 생성</div>
											<div class="info">
												<span class="name">파일 생성</span>
												<span class="txt">파일이 생성될때 함수가 실행됩니다.</span>
											</div>
										</div>
									</li>
									<li class="sec">
										<div class="imgIcon type2">
											<div class="icoImg icoImg-file-del">파일 삭제</div>
											<div class="info">
												<span class="name">파일 삭제</span>
												<span class="txt">파일이 삭제될때 함수가 실행됩니다.</span>
											</div>
										</div>
									</li>
								</ul>
							</div><!--//inp_field-->
							<div class="inp_field">
								<div class="g_column w_2_1">
									<div class="top_area">
										<span class="tit">제한 경로</span>
										<em class="ico-info1 ico right" data-toggle="tooltip" data-placement="right" data-html="true" title="" data-original-title="설정한 경로내에서 발생한 이벤트만 트리거로 작동합니다."></em>
									</div>
									<div class="cnt">
										<input type="text" class="inp" placeholder="예 : images/">
									</div>
								</div>
								<div class="g_column w_2_1">
									<div class="top_area">
										<span class="tit">제한 확장자</span>
										<em class="ico-info1 ico right" data-toggle="tooltip" data-placement="right" data-html="true" title="" data-original-title="설정한 확장자의 파일 유형만 트리거로 작동합니다."></em>
									</div>
									<div class="cnt">
										<input type="text" class="inp" placeholder="예 : .jpg">
									</div>
								</div>
							</div><!--//inp_field-->
						</div>

						<!-- DB 트리거 설정 -->
						<div class="panel_cnt">
							<div class="inp_field">
								<span class="sub_tit">DB 트리거 설정</span>
								<div class="top_area mt20">
									<div class="tit_wrap">
										<span class="tit imp">DB 유형</span>
										<em class="ico-info1 ico right" data-toggle="tooltip" data-placement="right" data-html="true" title="" data-original-title="트리거로 이용할 DB 유형을 선택해 주세요."></em>
									</div>
								</div>
								<div class="cnt">
									<select class="inp" style="width:50%;">
										<option value="MongoDB">DB 유형 선택</option>
									</select>
								</div>
							</div><!--//inp_field-->
							<div class="inp_field">
								<div class="top_area">
									<div class="tit_wrap">
										<span class="tit imp">DB 테이블</span>
										<em class="ico-info1 ico right" data-toggle="tooltip" data-placement="right" data-html="true" title="" data-original-title="사전에 생성해 둔 DB 테이블을 선택해 주세요.
										DB 테이블이 없으면 이용하실 수 없습니다.​"></em>
									</div>
								</div>
								<div class="cnt">
									<select class="inp" style="width:50%;">
										<option value="">DB 테이블 선택</option>
									</select>
								</div>
							</div><!--//inp_field-->
							<div class="inp_field">
								<div class="top_area">
									<div class="tit_wrap">
										<span class="tit imp">컬렉션</span>
										<em class="ico-info1 ico right" data-toggle="tooltip" data-placement="right" data-html="true" title="" data-original-title="MongoDB 대상 컬렉션을 선택해 주세요.​"></em>
									</div>
								</div>
								<div class="cnt">
									<select class="inp" style="width:50%;">
										<option value="">컬렉션 선택</option>
									</select>
								</div>
							</div><!--//inp_field-->
							<div class="inp_field">
								<div class="top_area">
									<div class="tit_wrap">
										<span class="tit imp">트리거 이벤트 유형</span>
										<em class="ico-info1 ico right" data-toggle="tooltip" data-placement="right" data-html="true" title="" data-original-title="함수가 실행될 트리거 이벤트를 선택합니다. 선택한
										이벤트가 발생할 경우에 한하여 함수가 실행됩니다."></em>
									</div>
								</div>
								<div class="cnt">
									<ul class="type_area ver">
										<li>
											<a href="javascript:void(0);" class="on">
												<div class="info">
													<span class="name">INSERT</span>
													<span class="txt">DB에 새로운 데이터가 생성되면
														함수가 실행됩니다.</span>
												</div>
											</a>
										</li>
										<li>
											<a href="javascript:void(0);">
												<div class="info">
													<span class="name">UPDATE</span>
													<span class="txt">DB데이터의 수정이 발생하면
														함수가 실행됩니다.</span>
												</div>
											</a>
										</li>
										<li>
											<a href="javascript:void(0);">
												<div class="info">
													<span class="name">DELETE</span>
													<span class="txt">DB에 적재된 데이터가 삭제되면
														함수가 실행됩니다.</span>
												</div>
											</a>
										</li>
									</ul>
								</div>
							</div><!--//inp_field-->
						</div>

						<!-- 타이머 트리거 설정 -->
						<div class="panel_cnt">
							<div class="inp_field">			
								<span class="sub_tit">타이머 트리거 설정</span>
								<div class="top_area mt20">
									<div class="tit_wrap">
										<span class="tit imp">기준 일시</span>
										<em class="ico-info1 ico right" data-toggle="tooltip" data-placement="right" data-html="true" title="" data-original-title="트리거 이벤트가 발생할 날짜와 시간을 설정해 주세요."></em>
									</div>
								</div>
								<div class="cnt">
									<div class="g_column w_6_2">
										<input type="text" class="inp datepicker" />
									</div>
									<div class="g_column w_6_1">
										<select class="inp">
											<option>13시</option>
										</select>
									</div>
									<div class="g_column w_6_1">
										<select class="inp">
											<option>22분</option>
										</select>
									</div>
								</div>
							</div><!--//inp_field-->
							
							<div class="inp_field">	
								<div class="top_area">
									<div class="tit_wrap">
										<span class="tit imp">반복 설정</span>
										<em class="ico-info1 ico right" data-toggle="tooltip" data-placement="right" data-html="true" title="" data-original-title="트리거 이벤트 반복을 원할 경우, 반복 
										주기를 설정해 주세요."></em>
									</div>
								</div>
								<div class="cnt">
									<ul class="type_area cycle2 repeatClick">
										<li class="none">
											<a href="javascript:void(0);" class="No_repeat on"></a>
											<span>없음</span>
										</li>
										<li class="minute">
											<a href="javascript:void(0);" data-btn="Minute"></a>
											<span>분</span>
										</li>
										<li class="hour">
											<a href="javascript:void(0);" data-btn="Hour"></a>
											<span>시간</span>
										</li>
										<li class="day">
											<a href="javascript:void(0);" data-btn="Day"></a>
											<span>매일</span>
										</li>
										<li class="week">
											<a href="javascript:void(0);" data-btn="Week"></a>
											<span>주별</span>
										</li>
										<li class="month">
											<a href="javascript:void(0);" data-btn="Month"></a>
											<span>월별</span>
										</li>
										<li class="year">
											<a href="javascript:void(0);" data-btn="Year"></a>
											<span>연간</span>
										</li>
									</ul>
									<div class="form_wrap2 mt30" style="display: none;">
										<div class="form-group" data-lst="repeatWrap" data-tab="Minute" style="display: none;">
											<div class="radio radio-inline">
												<input type="radio" name="tabs4" id="tab4" checked>
												<label for="tab4" class="label">매 &nbsp;&nbsp; &nbsp;
													<div class="inB">
														<select class="inp">
															<option>1</option>
															<option>2</option>
														</select>
													</div>
													<span> 분 마다 반복</span>
												</label>
											</div>
										</div>
										<div class="form-group" data-lst="repeatWrap" data-tab="Hour" style="display: none;">
											<div class="radio radio-inline">
												<input type="radio" name="tabs5" id="tab5" checked>
												<label for="tab5" class="label">매 &nbsp;&nbsp; &nbsp;
													<div class="inB">
														<select class="inp">
															<option>1</option>
															<option>2</option>
														</select>
													</div>
													<span> 시간 마다 반복​</span>
												</label>
											</div>
										</div>
										<div class="form-group" data-lst="repeatWrap" data-tab="Day" style="display: none;">
											<div class="radio radio-inline">
												<input type="radio" name="tabs1" id="tab1_1" checked>
												<label for="tab1_1" class="label">매 &nbsp;&nbsp; &nbsp;
													<div class="inB">
														<select class="inp">
															<option>7</option>
															<option>8</option>
														</select>
													</div>
													<span> 일 마다 반복</span>
												</label>
											</div>
											<div class="radio radio-inline">
												<input type="radio" name="tabs1" id="tab1_2">
												<label for="tab1_2" class="label">평일</label>
											</div>
										</div>
										<div class="chk_wrap" data-lst="repeatWrap" data-tab="Week" style="display: none;">
											<div class="checkbox checkbox-inline">
												<input type="checkbox" id="chk_1_1">
												<label for="chk_1_1">월요일</label>
											</div>
											<div class="checkbox checkbox-inline">
												<input type="checkbox" id="chk_1_2">
												<label for="chk_1_2">화요일</label>
											</div>
											<div class="checkbox checkbox-inline">
												<input type="checkbox" id="chk_1_3">
												<label for="chk_1_3">수요일</label>
											</div>
											<div class="checkbox checkbox-inline">
												<input type="checkbox" id="chk_1_4">
												<label for="chk_1_4">목요일</label>
											</div>
											<div class="checkbox checkbox-inline">
												<input type="checkbox" id="chk_1_5">
												<label for="chk_1_5">금요일</label>
											</div>
											<div class="checkbox checkbox-inline">
												<input type="checkbox" id="chk_1_6">
												<label for="chk_1_6">토요일</label>
											</div>
											<div class="checkbox checkbox-inline">
												<input type="checkbox" id="chk_1_7">
												<label for="chk_1_7">일요일</label>
											</div>
										</div>
										<div class="form-group" data-lst="repeatWrap" data-tab="Month" style="display: none;">
											<div class="radio radio-inline">
												<input type="radio" name="tabs2" id="tab2_1" checked>
												<label for="tab2_1" class="label">매월 &nbsp;
													<div class="inB">
														<select class="inp">
															<option>7</option>
															<option>8</option>
														</select>
													</div>
													<span> 일 마다 반복</span>
												</label>
											</div>
											<div class="radio radio-inline">
												<input type="radio" name="tabs2" id="tab2_2">
												<label for="tab2_2" class="label">
													<div class="inB">
														<select class="inp" style="width: 100px;">
															<option selected="">첫 번째</option>
															<option>두 번째</option>
														</select>
													</div>
													<div class="inB">
														<select class="inp">
															<option>월요일</option>
															<option selected="">화요일</option>
														</select>
													</div>
													<span> 마다 반복</span>
												</label>
											</div>
										</div>
										<div class="form-group" data-lst="repeatWrap" data-tab="Year" style="display: none;">
											<div class="radio radio-inline">
												<input type="radio" name="tabs3" id="tab3_1" checked>
												<label for="tab3_1" class="label">매년 &nbsp;
													<div class="inB">
														<select class="inp">
															<option>1</option>
															<option>2</option>
														</select>
													</div>
													<span> 월 마다 반복</span>
												</label>
											</div>
											<div class="radio radio-inline">
												<input type="radio" name="tabs3" id="tab3_2">
												<label for="tab3_2" class="label">
													<div class="inB">
														<select class="inp" style="width: 100px;">
															<option selected="">첫 번째</option>
															<option>두 번째</option>
														</select>
													</div>
													<div class="inB">
														<select class="inp">
															<option>월요일</option>
															<option selected="">화요일</option>
														</select>
													</div>
													<span> 마다 반복</span>
												</label>
											</div>
										</div>

										<div class="inp_field mt10">			
											<div class="top_area">
												<div class="tit_wrap">
													<span class="tit imp">반복 종료일</span>
													<em class="ico-info1 ico right" data-toggle="tooltip" data-placement="right" data-html="true" title="" data-original-title="트리거 이벤트 반복이 종료되는 날짜를 설정해 주세요."></em>
												</div>
											</div>
											<div class="cnt">
												<div class="g_column w_6_2 datepicker_wrap">
													<input type="text" class="inp datepicker" />
												</div>
												<div class="checkbox checkbox-inline mgL10" style="margin-top: 6px;">
													<input type="checkbox" id="chk_enddate">
													<label for="chk_enddate">사용 안 함</label>
												</div>
											</div>
										</div><!--//inp_field-->
									</div>
								</div>
							</div><!--//inp_field-->
							
						</div>

						<!-- DB 백엔드 서비스 설정​ -->
						<div class="panel_cnt">
							<div class="inp_field">
								<span class="sub_tit">DB 백엔드 서비스 설정​</span>
								<div class="top_area mt20">
									<div class="tit_wrap">
										<span class="tit imp">조건</span>
										<em class="ico-info1 ico right" data-toggle="tooltip" data-placement="right" title="" data-original-title="함수 실행  결과가 조건과 일치 할 때 백엔드 서비스가 동작합니다."></em>
									</div>
								</div>
								<div class="cnt">
									<ul class="type_area status v2 none_x">
										<li class="approve">
											<a href="javascript:void(0);" class="on"></a>
											<span>성공</span>
										</li>
										<li class="fail">
											<a href="javascript:void(0);"></a>
											<span>실패</span>
										</li>
									</ul>
								</div>
							</div><!--//inp_field-->
							<div class="inp_field">
								<div class="g_column w_2_1">
									<div class="top_area">
										<span class="tit imp">DB 유형</span>
										<em class="ico-info1 ico right" data-toggle="tooltip" data-placement="right" data-html="true" title="" data-original-title="백엔드 서비스로 이용할 DB 유형을 선택해 주세요.​"></em>
									</div>
									<div class="cnt">
										<select class="inp" data-sel="dbSel">
											<option value="">DB 유형 선택</option>
											<option value="MongoDB">MongoDB</option>
											<option value="Postgre">Postgre</option>
										</select>
									</div>
								</div>
								<div class="g_column w_2_1" data-inp="dbInp-MongoDB">
									<div class="top_area">
										<span class="tit imp">컬렉션</span>
										<em class="ico-info1 ico right" data-toggle="tooltip" data-placement="right" data-html="true" title="" data-original-title="MongoDB 대상 컬렉션을 선택해 주세요.​"></em>
									</div>
									<div class="cnt">
										<select class="inp">
											<option value="">컬렉션 선택</option>
										</select>
									</div>
								</div>
								<div class="g_column w_2_1" data-inp="dbInp-Postgre">
									<div class="top_area">
										<span class="tit imp">DB 테이블</span>
										<em class="ico-info1 ico right" data-toggle="tooltip" data-placement="right" data-html="true" title="" data-original-title="사전에 생성해 둔 DB 테이블을 선택해 주세요.
										DB 테이블이 없으면 이용하실 수 없습니다.​"></em>
									</div>
									<div class="cnt">
										<select class="inp">
											<option value="">DB 테이블 선택</option>
										</select>
									</div>
								</div>
							</div><!--//inp_field-->
							<div class="inp_field">
								<div class="top_area">
									<span class="tit imp">파일명​</span>
									<em class="ico-info1 ico right" data-toggle="tooltip" data-placement="right" data-html="true" title="" data-original-title="영문, 숫자, 언더바(_)만 입력 가능하며, 연월일시분초는 자동으로 저장됩니다.​"></em>
								</div>
								<div class="cnt">
									<div class="g_column w_2_1">
										<input type="text" placeholder="YYYYMMDD_HHMMSSS_파일명​" class="inp">
									</div>
									<div class="g_column w_2_1">
										<select class="inp">
											<option value="">동일 파일명 존재 시 업데이트​</option>
											<option value="">동일 파일 존재 시 새 파일 추가​</option>
										</select>
									</div>
								</div>
							</div><!--//inp_field-->

							<!-- DB 미등록된 경우 -->
							<div class="row_wrap mt50">
								<div class="ncz_msg_alert default">
									<p>자원 관리 메뉴에서 <strong>DB 테이블</strong> 및 <strong>컬렉션 등록</strong> 후 이용 가능합니다. <a href="SC_FS_02_010.html">자원 관리 바로가기</a></p>
								</div>
							</div>
						</div>

						<!-- Ceph 백엔드 서비스 설정​​ -->
						<div class="panel_cnt">
							<div class="inp_field">
								<span class="sub_tit">Ceph 백엔드 서비스 설정​​</span>
								<div class="top_area mt20">
									<div class="tit_wrap">
										<span class="tit imp">조건</span>
										<em class="ico-info1 ico right" data-toggle="tooltip" data-placement="right" title="" data-original-title="함수 실행  결과가 조건과 일치 할 때 백엔드 서비스가 동작합니다.​"></em>
									</div>
								</div>
								<div class="cnt">
									<ul class="type_area status v2 none_x">
										<li class="approve">
											<a href="javascript:void(0);" class="on"></a>
											<span>성공</span>
										</li>
										<li class="fail">
											<a href="javascript:void(0);"></a>
											<span>실패</span>
										</li>
									</ul>
								</div>
							</div><!--//inp_field-->
							<div class="inp_field">
								<div class="g_column w_2_1">
									<div class="top_area">
										<span class="tit imp">버킷</span>
										<em class="ico-info1 ico right" data-toggle="tooltip" data-placement="right" data-html="true" title="" data-original-title="사전에 생성해 둔 버킷을 선택해 주세요.  버킷이 없으면 이용하실 수 없습니다.​"></em>
									</div>
									<div class="cnt">
										<select class="inp">
											<option value="">버킷 선택​</option>
										</select>
									</div>
								</div>
							</div><!--//inp_field-->
							<div class="inp_field">
								<div class="top_area">
									<span class="tit imp">파일명​</span>
									<em class="ico-info1 ico right" data-toggle="tooltip" data-placement="right" data-html="true" title="" data-original-title="영문, 숫자, 언더바(_)만 입력 가능하며, 연월일시분초는 자동으로 저장됩니다.​"></em>
								</div>
								<div class="cnt">
									<div class="g_column w_2_1">
										<input type="text" placeholder="YYYYMMDD_HHMMSSS_파일명​" class="inp">
									</div>
									<div class="g_column w_2_1">
										<select class="inp">
											<option value="">동일 파일명 존재 시 업데이트​</option>
											<option value="">동일 파일 존재 시 새 파일 추가​</option>
										</select>
									</div>
								</div>
							</div><!--//inp_field-->
						</div>

						<!-- 함수 코드 확인 -->
						<div class="panel_cnt">
							<div class="inp_field">
								<div class="top_area mt10">
									<span class="tit">함수 코드 확인 </span>
									<em class="ico-info1 ico right" data-toggle="tooltip" data-placement="right" title="" data-original-title="hub-PoP에서 기본 함수 코드를 제공합니다. 함수 생성 후에 
									함수 코드를 수정하여 사용하실 수 있습니다."></em>
								</div>
								<div class="cnt">
									<div class="ncz_msg_info code mb10">
										<p>
											Node.js 8.10 
										</p>
									</div>
									<div class="edit_box type3" style="padding:10px">
										<div>
											Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.
										</div>
									</div>
								</div>
							</div>

							<!-- button area -->
							<div class="btnArea confirm mt80">
								<button type="button" class="btn btn-md btn-color1" onclick="location.href='SC_FS_02_010.html'" title="함수 생성">함수 생성</button>
								<button type="button" class="btn btn-md btn-color2" onclick="location.href='SC_FS_02_002.html'" title="취소">취소</button>
							</div><!-- //button area -->

						</div>
					</div>
				</div><!--//row_wrap-->

			</div>
			<!-- //content -->
		</div>
		<script type="text/javascript" src="${pageContext.request.contextPath}/core/js/bootstrap.min.js"></script>
		<script type="text/javascript" src="${pageContext.request.contextPath}/core/js/bootstrap-slider.js"></script>    
