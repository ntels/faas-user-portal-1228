<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
    
<script>
var data = {
		isViewNew: true,
		nodejs: {
			value: "nodejs",
			dsp_name: "Node.js",
			version: ["10", "12"]
		},
		python: {
			value: "python",
			dsp_name: "Python",
			version: ["3.6", "3.7", "3.8"]
		},
		runtime: "nodejs",
		version: "10"
	};

$(document).ready(function(){
	debugger;
	
	NtelsHB.rootNodeSelector = "#handlebarRoot";
	
	function componentLoad()
	{
		var context = "${pageContext.request.contextPath}";
		var rooturl = context + "/component/function/regFunction/";

		NtelsHB.registerComponent("title", rooturl + "title.do");
		NtelsHB.registerComponent("selector", rooturl + "selector.do");
		
		NtelsHB.loadComponent();
	}
	
	NtelsHB.onComponentReady = function()
	{
		NtelsHB.bind(data);	
		NtelsHB.render();
		
		setRuntimeArea(data.runtime, data.version);
		
		$("#rightContent").show();
	}
	
	NtelsHB.afterRender = function()
	{
		$("#reloadBtn").on("click", function() {
			loadPage("layout/function/regFunction.do");
		});
		
		// 함수 생성 클릭 시
		$("#regBtn").on("click", function() {
			resetValidationResult();
			
			checkValidation();
		});
		
		// 런타임 유형 클릭 시
		$("li[name=runtime-type-list]").on("click", function() {
			data.runtime = $(this).attr("runtime");
			
			if(data.runtime == "nodejs")
				data.version = data.nodejs.version[0];
			else
				data.version = data.python.version[0];
			
			setRuntimeArea();
		});
		
		// 런타임 버전 클릭 시
		$("li[name=runtime-version-list]").on("click", function() {
			data.version = $(this).attr("version");
			
			setRuntimeArea();
		});
	}
	
	function setRuntimeArea() {
		$("li[name=runtime-type-list]").each(function() {
			if($(this).attr("runtime") == data.runtime) {
				$(this).find("input[name=runtime-type-btn]").prop("checked", true);
			} else {
				$(this).find("input[name=runtime-type-btn]").prop("checked", false);
			}
		});
		
		$("li[name=runtime-version-list]").each(function() {
			if($(this).attr("runtime") == data.runtime)
				$(this).show();
			else
				$(this).hide();
		});
		
		$("li[name=runtime-version-list]").each(function() {
			if($(this).attr("version") == data.version)
				$(this).find("input[name=runtime-version-btn]").prop("checked", true);
			else
				$(this).find("input[name=runtime-version-btn]").prop("checked", false);
		});
	}
	
	componentLoad();
});

function resetValidationResult() {
	$("#parentfuncNm").removeClass("form-danger-before");
	$("#funcNmRegexErr").hide();
	$("#funcNmDupErr").hide();
	$("#funcNmNullErr").hide();
	
	$("#parentfuncDes").removeClass("form-danger-before");
	$("#funcDesRegexErr").hide();
	$("#funcDesNullErr").hide();
}

function checkValidation() {
	if(!checkRequiredParam())
		return;
	if(!checkFuncNmRegex())
		return;
	if(!checkFuncDesRegex())
		return;
	
	regFunction();
}

function checkRequiredParam() {
	if($("#funcNm").val() == null || $("#funcNm").val() == "") {
		showFuncNmNullErr();
		return false
	}
	
	if($("#funcDes").val() == null || $("#funcDes").val() == "") {
		showFuncDesNullErr();
		return false
	}
	
	return true;
}

function checkFuncNmRegex() {
	var funcNmRegex = /^([a-z])[a-z0-9\-]{6,18}([a-z0-9])$/g;
	
	if(funcNmRegex.test($("#funcNm").val())) {
		// $("#funcNm").addClass("true");
		return true;
	} else {
		showFuncNmRegexErr();
		return false;
	}
}

function checkFuncDesRegex() {
	if($("#funcDes").val().length < 1001) {
		// $("#funcDes").addClass("true");
		return true;
	} else {
		showFuncDesRegexErr();
		return false;
	}
}

function regFunction() {
	var param = {
			user_id: USER_ID,
			user_name: USER_NAME,
			user_role: USER_ROLE,
			project_id: $.cookie('PROJECT-ID'),
			function_name: $("#funcNm").val(),
			function_des: $("#funcDes").val(),
			runtime_type: data.runtime,
			runtime_version: data.version
			}
	
	var showProgress = true;
	
	JsonCall('${pageContext.request.contextPath}/service/regFunction.json', 'param=' + JSON.stringify(param), function(response) {
		var ret = JSON.parse(response.responseText);
		
		if(ret.projectExists == "true" || ret.projectExists == true) {
			showFuncNmDupErr();
		} else {
			var function_id = ret.functionId;
			loadPage("layout/function/detail.do?function_id=" + function_id);
		}
	}, showProgress);
}

function showFuncNmRegexErr() {
	$("#parentfuncNm").addClass("form-danger-before");
	$("#funcNmRegexErr").show();
}

function showFuncNmDupErr() {
	$("#parentfuncNm").addClass("form-danger-before");
	$("#funcNmDupErr").show();
}

function showFuncNmNullErr() {
	$("#parentfuncNm").addClass("form-danger-before");
	$("#funcNmNullErr").show();
}

function showFuncDesRegexErr() {
	$("#parentfuncDes").addClass("form-danger-before");
	$("#funcDesRegexErr").show();
}

function showFuncDesNullErr() {
	$("#parentfuncDes").addClass("form-danger-before");
	$("#funcDesNullErr").show();
}
</script>

<div id="handlebarRoot">
	<main id="contentWrap">
	
		{{component "title"}}
		
		<!-- inner -->
		<div class="page-content">

			<!-- 콘텐츠 영역 -->
			<div class="inner-wrap">
				<section class="panel panel-bordered">
					<div class="panel-heading">
						<h3 class="panel-title">
							함수 등록
						</h3>
					</div> <!-- // panel-heading -->
					<div class="panel-body">
						<div class="text-right add-txt">
							<span><b class="caution-star">*</b> 는 필수입력 사항입니다.</span>
						</div>

						{{component "selector"}}

						<div class="row">
							<div class="col-md-6">
								<div class="write-sub-title">
									<b class="caution-star">*</b> 함수 이름​
									<span class="info-icon-tooltip" data-toggle="tooltip" title="공백 없이 영문 대소문자, 숫자, 하이픈(-), 언더바(_)만 입력해 주세요.​" data-placement="right">
										i
									</span>
								</div>
								<div class="form-group" id="parentfuncNm">
									<input type="text" id="funcNm" class="form-control"  placeholder="함수 이름을 입력해 주세요.​">
									<!-- TODO : 아래 경고 메시지에 대한 퍼블리싱이 되어있지 않습니다. -->
									<p class="form-text" id="funcNmRegexErr" style="display:none;">함수 이름은 영문 소문자, 숫자, 하이픈(-)만 공백 없이 8 ~ 20자 이내로 입력 가능합니다.</p>
									<p class="form-text" id="funcNmDupErr" style="display:none;">이미 사용중인 이름입니다. 다른 이름으로 입력해 주세요.</p>
									<p class="form-text" id="funcNmNullErr" style="display:none;">함수 이름을 입력해 주세요.</p>
								</div>
							</div>
						</div>

						<div class="write-sub-title">
							<b class="caution-star">*</b> 함수 설명​
							<span class="info-icon-tooltip" data-toggle="tooltip" title="함수 설명을 자유롭게 입력해 주세요.​" data-placement="right">
								i
							</span>
						</div>
						<div class="form-group" id="parentfuncDes">
							<textarea id="funcDes" class="form-control" placeholder="Node.js 12.x를 활용한 함수" rows="4"></textarea>
							<!-- TODO : 아래 경고 메시지에 대한 퍼블리싱이 되어있지 않습니다. -->
							<p class="form-text" id="funcDesRegexErr" style="display:none;">함수 설명은 1000자 이내로 입력 가능합니다.</p>
							<p class="form-text" id="funcDesNullErr" style="display:none;">함수 설명을 입력해 주세요.</p>
						</div>

						<div class="row margin-top-30">
							<div class="col-md-6">
								<div class="write-sub-title">
									<b class="caution-star">*</b> 런타임
									<span class="info-icon-tooltip" data-toggle="tooltip" title="함수를 작성할 때 사용할 언어를 선택해 주세요.​" data-placement="right">
										i
									</span>
								</div>
								<ul class="list-inline">
									<li name="runtime-type-list" runtime="{{nodejs.value}}">
										<label for="node" class="input-card-type">
											<input type="radio" name="runtime-type-btn" class="card-input-element">
											<div class="card-input">
												<div class="card-type4">
													<div class="img-nodejs img-size70">
													</div>
												</div>
											</div>
											<p class="text-center">
												{{nodejs.dsp_name}}​
											</p>
										</label>
									</li>
									<li name="runtime-type-list" runtime="{{python.value}}">
										<label for="python" class="input-card-type">
											<input type="radio" name="runtime-type-btn" class="card-input-element">
											<div class="card-input">
												<div class="card-type4">
													<div class="img-python img-size70">
													</div>
												</div>
											</div>
											<p class="text-center">
												{{python.dsp_name}}​
											</p>
										</label>
									</li>
								</ul>
							</div>
							<div class="col-md-6">
								<div class="write-sub-title">
									<b class="caution-star">*</b> 런타임 버전
									<span class="info-icon-tooltip" data-toggle="tooltip" title="함수를 작성할 때 사용할 언어의 세부 버전을 선택해 주세요.​" data-placement="right">
										i
									</span>
								</div>
								<ul class="list-inline">
									{{#each nodejs.version}}
									<li name="runtime-version-list" runtime="{{../nodejs.value}}" version="{{this}}" style="display: none;">
										<label for="card21" class="input-card-type">
											<input type="radio" name="runtime-version-btn" class="card-input-element">
											<div class="card-input">
												<div class="card-type4 txt-wrap">
													<div class="text-area">
														{{this}}​
													</div>
												</div>
											</div>
											<p class="text-center">
												{{this}}​
											</p>
										</label>
									</li>
									{{/each}}
									{{#each python.version}}
									<li name="runtime-version-list" runtime="{{../python.value}}" version="{{this}}" style="display: none;">
										<label for="card21" class="input-card-type">
											<input type="radio" name="runtime-version-btn" class="card-input-element">
											<div class="card-input">
												<div class="card-type4 txt-wrap">
													<div class="text-area">
														{{this}}
													</div>
												</div>
											</div>
											<p class="text-center">
												{{this}}​
											</p>
										</label>
									</li>
									{{/each}}
								</ul>
							</div>
						</div>

					
					</div> <!-- // panel-body -->
				</section>

				<div class="margin-top-40 text-right">
					<button id="regBtn" class="btn btn-lg btn-primary">함수 생성​</button>
					<button class="btn btn-lg btn-default" onclick="loadPage('layout/function/list.do');">취소​</button>
				</div>

			</div>
			
		</div>
		<!-- // inner -->
		
	</main>
</div>