<%@page import="com.fasterxml.jackson.databind.ObjectMapper"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %> 
<spring:eval expression="@environment.getProperty('websocket.base.url')" var="webSocketBaseUrl" />    
    
<script src="${pageContext.request.contextPath}/core/js/ace-builds/src-noconflict/ace.js" type="text/javascript" charset="utf-8"></script>
<script src="${pageContext.request.contextPath}/core/js/ace-builds/src-noconflict/ext-modelist.js"></script>
<script src="${pageContext.request.contextPath}/core/js/ace-builds/src-noconflict/ext-language_tools.js"></script>
<!-- script src="${pageContext.request.contextPath}/core/js/dataPortal/amb_datepicker_ko.js" type="text/javascript"></script--> <!-- datepicker 사용 시 필수 // 커스텀 js // 한국어 -->
<script src="${pageContext.request.contextPath}/core/js/dataPortal/custom.js"></script>
<style type="text/css" media="screen">
	.edit_box {background: #ffffff !important}
</style>
<style type="text/css" media="screen">
	.modal {overflow-y: scroll;}
</style>
<script>

$(document).ready(function(){
	debugger;
	
	NtelsHB.rootNodeSelector = "#handlebarRoot";
	
	var data = {
			funObj: {},
			desBtnShow: false,
			sourceList: [],
			attachList: null,
			triggerList: [],
			triggerCnt: 0,
			targetList: [],
			targetCnt: 0,
			selectedItem: {
				kind: 'function',
				id: ''
			},
			selectedSource: {
				idx: 0
			},
			stat: 'INIT',
			step: 'NONE',
			name: '',
			regTriggerType: {
				supported: ['http', 'ceph', 'db', 'timer'],
				supportedDb: ['mongodb', 'postgresql'],
				selected: ''
			},
			regTriggerConfig: {},
			isTriggerModalShow: false,
			dbTriggerModal: {
				isValidDb: false,
				selectedDb: '',
				tableList: []
			},
			config: {
				type: {
					supported: ['env', 'traffic', 'resource', 'concur'],
					selected: ''
				},
				env: [],
				envLength: 0,
				mem: 128,
				cpu: 100,
				timeout: {min:1 , sec:0},
				concur: 100,
				trf: {burst: 100 , quota: {type: 'D' , val: 1 , cnt: 1000 , typenm: '일'}}
			},
			isConfigModalShow: false,
			org: {source: "" , config: ""},
			candidateList: {
				onSuccess: [],
				onFailure: []
			}
		};
	
	function componentLoad()
	{
		var context = "${pageContext.request.contextPath}";
		var rooturl = context + "/component/function/detail/";

		NtelsHB.registerComponent("title",rooturl+"title.do");
		NtelsHB.registerComponent("info",rooturl+"info.do?function_id=" + '${paramMap.function_id}');
		NtelsHB.registerComponent("trigger_modal", rooturl + "trigger_modal.do");
		NtelsHB.registerComponent("trigger_http_detail", rooturl + "trigger_http_detail.do");
		NtelsHB.registerComponent("trigger_ceph_detail", rooturl + "trigger_ceph_detail.do");
		NtelsHB.registerComponent("trigger_db_detail", rooturl + "trigger_db_detail.do");
		NtelsHB.registerComponent("trigger_timer_detail", rooturl + "trigger_timer_detail.do");
		NtelsHB.registerComponent("reg_step",rooturl+"reg_step.do");
		NtelsHB.registerComponent("detail_config",rooturl+"detail_config.do");
		NtelsHB.registerComponent("target_function",rooturl+"target_function.do");
		NtelsHB.registerComponent("envTable",rooturl+"envTable.do");
		NtelsHB.registerComponent("target_detail",rooturl+"target_detail.do");
		
		NtelsHB.loadComponent();
	}
	
	NtelsHB.onComponentReady = function()
	{
		NtelsHB.bind(data);	
		dataLoad();
	}
	
	NtelsHB.afterRender = function() {
		
		NtelsHB.func.dataLoad = dataLoad;
		titleAfterRender();
		infoAfterRender();
		
		modalShow();
		$('[data-toggle="tooltip"]').tooltip();
		if(data.isTriggerModalShow)
			$("#regTriggerModal").modal("show");
		
		function chekcDisable() {
			if(data.stat == "DISABLE")
			{
				fnModalAlert("비활성 상태입니다. 상태 변경후 진행해 주시기 바랍니다.","알림");
				return false;
			}
			return true;
		}
		
		//트리거삭제
		$("[deleteTriggerId]").on("click",function(e) {
			if(chekcDisable() == false) return;
			var trigger_id = $(this).attr("deleteTriggerId");
			
			fnModalConfirm("트리거를 삭제하시겠습니까?" , "삭제" , function()
			{
				JsonCall('trigger/deleteTrigger.json', 'param=' + JSON.stringify({trigger_id: trigger_id}), function(response)
				{
					ret = JSON.parse(response.responseText);
					if(ret.success != undefined)
					{
						if(ret.success == "N")
							fnModalAlert("삭제를 실패했습니다.","알림");
						else
							dataLoad(false);
					}
					else
						fnModalAlert("작업을 정상적으로 처리하지 못했습니다.","에러");					
				});
			});
		});
		
		//대상삭제
		$("[deleteTargetId]").on("click",function(e) {
			if(chekcDisable() == false) return;
			var target_id = $(this).attr("deleteTargetId");
			
			var param = {
				target_id: target_id,
				function_id: '${paramMap.function_id}'
			};
			
			fnModalConfirm("대상을 삭제하시겠습니까?" , "삭제" , function()
			{
				JsonCall('service/deleteTarget.json', 'param=' + JSON.stringify(param), function(response)
				{
					ret = JSON.parse(response.responseText);
					if(ret.success != undefined)
					{
						if(ret.success == "N")
							fnModalAlert("삭제를 실패했습니다.","알림");
						else
							dataLoad(false);
					}
					else
						fnModalAlert("작업을 정상적으로 처리하지 못했습니다.","에러");					
				});
			});
		});
		
		//저장
		$("#saveBtn").on("click",function() {
			if(chekcDisable() == false) return;
			
			saveCurrentSource();
			saveSource();
		});
		
		$('#show_trig_modal').on('click', function() {
			if(chekcDisable() == false) return;
			
			if(data.stat != "REG" && data.stat != "INIT")
			{
				data.isTriggerModalShow = true;
				setTimeout(function(){NtelsHB.render();} , 10);
			} else {
				fnModalAlert('먼저 함수를 배포해주세요.','알림');
			}
		});
		
		// 대상 모달 팝업
		$('#show_backend_modal').on('click', function() {
			if(chekcDisable() == false) return;
			
			var modal_id = $(this).attr('modal_id');
			
			var param = {
				function_id: "${paramMap.function_id}"
			}
			
			JsonCall('service/candidateTargetList.json', 'param=' + JSON.stringify(param), function(response)
			{
				ret = JSON.parse(response.responseText);
				
				if(ret.onSuccess != undefined || ret.onFailure != undefined)
				{
					data.candidateList.onSuccess = ret.onSuccess;
					data.candidateList.onFailure = ret.onFailure;
					
					NtelsHB.render();
					
					$(modal_id).modal({backdrop: 'static',keyboard: true});
				}
				else
					fnModalAlert("작업을 정상적으로 처리하지 못했습니다.", "에러");
			});
		});
 		
		$('button[name="hide_trig_modal"]').on('click', function() {
			data.isTriggerModalShow = false;
			setTimeout(function(){NtelsHB.render();} , 500);
		});
		
		
		// 트리거 추가 버튼 클릭 시
		$('#create_trig').on('click', function() {
			setRegTriggerConfigAndRun();
		});
		
		// DB 트리거 설정 부분
		$('#check_db_validation').on('click', function() {
			isValidAndGetTableList();
		});
		
		$('#config_db_type').on('change', function() {
			data.dbTriggerModal.isValidDb = false;
			setDbMsgArea();
			resetDbConfigArea();
		});
		
		$('input[name=db_config_items]').on('keyup', function() {
			data.dbTriggerModal.isValidDb = false;
			resetDbConfigArea();
		});
		
		// 트리거,함수,대상 클릭시 처리.
		$('.item_wrap li button').on('click' , function() {
			data.selectedItem.kind = $(this).attr("item-kind");
			data.selectedItem.id = $(this).attr("item-id");
			
			NtelsHB.render();
			
			setConfigArea();
		});
		
		$('.source-fileName > li > a').on('click', function() {
			saveCurrentSource();
			
			data.selectedSource.idx = $(this).attr("idx");
			setConfigArea();
			setSource();
		});
		
		if($("#editor").length != undefined || $("#editor").length != 0)
		{
			setConfigArea();
			setEditor();
			setSource();
		}
		
		// 오류창 에디터
		if(data.stat == "ERROR")
			createEditor("errorEditor",data.funObj.log);
		
		// '대상' 모달 액션
		actionBackend(${paramMap.function_id});
		// '트리거 추가' 모달 액션
		actionTriggerModal();
		
		// 설정 모달 액션
		actionConfigModal(); <%//해당 코딩은 detail_config.jsp에서 확인%>
		
		//첨부파일 레이블
		$('.btn-file :file').on('change', function() {
			var input = $(this);
			var	label = input.val().replace(/\\/g, '/').replace(/.*\//, '');
			if(label == "") label = "선택된 파일 없음";
			$('#attach_file_name').text(label);
		  });
		
		//첨부파일 삭제.
		$("#attach_file_delete").on("click",function(){
			data.attachList = null;
			NtelsHB.render();
		});
	}
	
	function createEditor(id , content) 
	{
		//var modelist = ace.require("ace/ext/modelist");
		var editor = ace.edit(id);
		
		editor.setOptions({
			theme: "ace/theme/textmate",
			fontSize: "16px",
		    //enableBasicAutocompletion: true,
		    //enableSnippets: true,
		    //enableLiveAutocompletion: true,
		    readOnly: true
		});
		editor.session.setOption("useWorker", true);
		
		if(content != undefined)
			editor.setValue(content+"\n");
		editor.clearSelection();
	}
	
	function dataLoad(showProgress)
	{
		var param = {
				user_id: USER_ID,
				user_name: USER_NAME,
				user_role: USER_ROLE,
				project_id: $.cookie('PROJECT-ID'),
				function_id: "${paramMap.function_id}"
		}
		
		if(showProgress == undefined) showProgress = true;
		
		JsonCall('${pageContext.request.contextPath}/service/getFunction.json', 'param=' + JSON.stringify(param), function(response) {
			
			ret = JSON.parse(response.responseText);
			if(ret.list.length == 1)
			{
				data.funObj = ret.list[0] || {};
				data.name = data.funObj.name;
				data.stat = data.funObj.stat_cd;
				data.step = data.funObj.reg_step_cd;
				data.desBtnShow = data.funObj.des.length >= 200;
				
				data.sourceList = ret.sourceList || [];
				data.attachList = ret.attachList || null;
				data.triggerList = ret.triggerList || [];
				data.triggerCnt = data.triggerList.length || 0;
				data.targetList = ret.targetList || [];
				data.targetCnt = data.targetList.length || 0; 
				
				data.selectedSource.idx = 0;
				
				data.selectedItem.kind = 'function';
				data.selectedItem.id = data.funObj.function_id;
				
				data.config.env = data.funObj.env || [];
				data.config.envLength = data.config.env.length || 0;
				data.config.mem = data.funObj.mem || 128;
				if(data.funObj.timeout == null) data.funObj.timeout = 60;
				data.config.timeout = {min: parseInt(data.funObj.timeout/60) , sec: parseInt(data.funObj.timeout%60)} || {min:1 , sec:0};
				data.config.concur = data.funObj.concur || 100;
				data.config.trf = data.funObj.trf || {burst: 100 , quota: {type: 'D' , val: 1 , cnt: 1000 , typenm: '일'}};
				
				data.org.source = JSON.stringify(data.sourceList);
				data.org.config = JSON.stringify(data.config);		
			}
			else
			{
				alert("정상적인 데이타 조회가 아닙니다.");
				pageLoad('/layout/function/list.do');
			}
			
			NtelsHB.render();
			
		}, showProgress);
	}
	
	
	// Websocket
	(function()
	{
		var webSocketBaseUrl = '${webSocketBaseUrl}';
		if(webSocketBaseUrl != "")
		{
			HBWebSocket.getInstance(webSocketBaseUrl+"/faas/service.do", onMessage);
			function onMessage(obj)
			{
				console.log("[FUNCTION DETAIL] : " + JSON.stringify(obj));
				if(obj.namespace == undefined || obj.name == undefined) return;
				
				if(obj.namespace == "faas-p"+$.cookie('PROJECT-ID') && obj.name == data.name)
				{
					if(obj.source == "JENKINS" || obj.source == "KUBE_REG")
					{
						if(obj.setp != undefined && data.step == obj.setp) return;
						if(obj.kubeStatus != undefined && data.stat == obj.kubeStatus && data.step == "STEP5") return;
	
						console.log("[FUNCTION DETAIL RELOAD]");
						dataLoad(false);
	
					}
					
					if(obj.source == "KUBE_WATCH" && data.stat == "ERROR" && data.step == "NONE")
					{
						console.log("[FUNCTION DETAIL RELOAD]");
						dataLoad(false);
					}
				}
			}
		}
		else  <% //webSocketBaseUrl 이 없으면 polling 하도록 한다. %>
		{
			var localPageUrl = g_lastLoadPageUrl;
			var param = {
					function_id: "${paramMap.function_id}",
					date: new Date,
			};
			var pollingObj = new HBWebpolling("service/getFunction.json", 'param=' + JSON.stringify(param) , 2000);
			pollingObj.onCallback = function(response)
			{
				var ret = JSON.parse(response.responseText);
				if(ret.list.length == 1)
				{
					var func = ret.list[0];
					if(func.stat_cd != data.stat || func.reg_step_cd != data.step || func.log != data.funObj.log)
						dataLoad(false);
				}
				return g_lastLoadPageUrl == localPageUrl;
			}
			pollingObj.start();
		}
	})();
	
	componentLoad();
	
	function setEditor() {
		var modelist = ace.require("ace/ext/modelist");
		var editor = ace.edit("editor");
		
		editor.setOptions({
			theme: "ace/theme/textmate",
			fontSize: "16px",
		    enableBasicAutocompletion: true,
		    enableSnippets: true,
		    enableLiveAutocompletion: true
		});
		editor.session.setOption("useWorker", true);
		editor.setValue("");
		editor.clearSelection();
	}
	
	function setConfigArea() {
		$(".item_wrap li button").each(function() {
			if($(this).attr("item-kind") == data.selectedItem.kind && $(this).attr("item-id") == data.selectedItem.id) {$(this).addClass("on");}
			else {$(this).removeClass("on");}
		});
		
		$(".source-fileName > li > a").each(function() {
			if($(this).attr("idx") == data.selectedSource.idx) {$(this).closest("li").addClass("active");}
			else {$(this).closest("li").removeClass("active");}
		});
		
		$('[data-lst]').each(function(){
			if($(this).attr('data-lst') == data.selectedItem.kind) $(this).show();
			else $(this).hide();
		});
		
	}
	
	function saveCurrentSource() {
		var modelist = ace.require("ace/ext/modelist");
		var editor = ace.edit("editor");
		
		data.sourceList[data.selectedSource.idx].fileContent = editor.getValue();
	}
	
	function setSource() {
		var modelist = ace.require("ace/ext/modelist");
		var editor = ace.edit("editor");
		
		editor.setValue(data.sourceList[data.selectedSource.idx].fileContent);
		editor.clearSelection();
		
		var modeCheckFile = data.sourceList[data.selectedSource.idx].fileName;
		var modeCheck = modelist.getModeForPath(modeCheckFile);
		
		editor.session.setMode(modeCheck.mode);
	}
	
	function saveSource() {
		var sourceArray = new Array();
		
		for(var i=0; i < data.sourceList.length; i++) {
			var object = new Object();
			
			object.fileName = data.sourceList[i].fileName;
			object.fileContent = data.sourceList[i].fileContent;
			sourceArray[i] = object;
		}
		
		var param = {
				user_id: USER_ID,
				user_name: USER_NAME,
				user_role: USER_ROLE,
				project_id: $.cookie('PROJECT-ID'),
				function_id: "${paramMap.function_id}",
				source_list: JSON.stringify(sourceArray),
				config: data.config,
				isModify: {source: false , config: false }  //<% //변경관련 클라이언트 체크 했다. 서버 모듈 별도 개발필요함. %>
		}
		
		if(JSON.stringify(data.sourceList) != data.org.source)
		{
			param.isModify.source = true;
		}
		if(JSON.stringify(data.config) != data.org.config)
		{
			param.isModify.config = true;
		}
		
		if(data.stat == "NORMAL" && param.isModify.source == false && param.isModify.config == false)
		{
			fnModalAlert("수정된 내용이 없습니다.","알림");
			return;
		}
		
		checkJenkinsExists(param);
	}
	
	function checkJenkinsExists(param) {
		var showProgress = false;
		
		JsonAjaxCall('${pageContext.request.contextPath}/service/checkJenkinsExists.json', JSON.stringify(param), function(response) {
			var jenkinsExists = response.jenkinsExists;
			
			if(jenkinsExists == true || jenkinsExists == "true") {
				uploadFile(function() {
					var showProgress = false;
					
					JsonAjaxCall('${pageContext.request.contextPath}/service/saveSource.json', JSON.stringify(param), function(response) {
					}, showProgress);
					
					data.stat = 'REG';
					data.step = 'STEP1'
					
					NtelsHB.render();
				}, param);
			} else {
				fnModalAlert("HUB-PoP 파이프라인 서비스를 통해 Jenkins를 우선적으로 배포해주세요.", "알림");
				return;
			}
		}, showProgress);
	}
	
	function uploadFile(callback, param)
	{
		var form = $("#uploadForm");
		
		if(form.length <= 0)
		{
			callback()
			return;
		}
			
		var formData = new FormData(form[0]);
		
		$.ajax({
			  url: '${pageContext.request.contextPath}/service/upload.json',
			  type: 'POST',
			  data: formData,
			  enctype: 'multipart/form-data',
			  processData: false,
			  contentType: false,
			  cache: false,
			  success: function(data){
				  callback();
			  },
			  error: function(){
				  fnModalAlert("파일 업로드중 에러가 발생했습니다.","알림");
			  }
		});
	}
	
	// 트리거 추가
	function setRegTriggerConfigAndRun() {
		setRegTriggerType();
		setRegTriggerConfig();
		
		if(data.regTriggerType.selected == "http")
		{
			var regex = RegExp(/[^a-z_]/);
			var apiName = $('#trig_modal_api_name').val();
			if(apiName == null || apiName == "" || regex.test(apiName)) {
				$('#trig_modal_api_name').closest("div").addClass("form-danger-before").focus();
				return false;
			}
			
			var param = {
				function_id: "${paramMap.function_id}",
				apiName: apiName,
			};
			JsonCall('${pageContext.request.contextPath}/trigger/httpAPIExist.json', 'param=' + JSON.stringify(param), function(response) {
				var ret = JSON.parse(response.responseText);
				if(ret.success == "Y" )
				{
					if(ret.isExist == "Y")
					{
						fnModalAlert("'"+ret.api_name+"'은 이미 사용중인 이름입니다.","알림");
						return
					}
					
					if(data.regTriggerConfig.isSuccess == true) {
						runRegTrigger();
					} else {
						return;
					}
				}
			}, false);
		}
		else if(data.regTriggerType.selected == "db")
		{
			if(data.dbTriggerModal.isValidDb == false)
			{
				fnModalAlert("'접속 테스트'를 먼저 진행해 주시기 바랍니다.","알림");
				return;	
			}
			
			if(data.regTriggerConfig.isSuccess == true) {
				runRegTrigger();
			} else {
				var obj = data.regTriggerConfig;
				if(obj.table_name == "" && obj.collection_name == "")
				{
					fnModalAlert("테이블명이나 컬렉션이름을 선택하세요.","알림");
				}
				return;
			}
		}
		else if(data.regTriggerType.selected == "timer")
		{
			if(data.regTriggerConfig.isSuccess == true) {
				runRegTrigger();
			} else {
				var obj = data.regTriggerConfig;
				if(obj.bas_date_hms.indexOf("0000-00-00") == 0)
				{
					$('#bas_date_hms_date').addClass("false");
					fnModalAlert("기준일시를 선택해 주세요.","알림");
					$('#bas_date_hms_date').on("click",function(){$('#bas_date_hms_date').removeClass("false");});
				}
				else if(obj.end_date_hms.indexOf("2999-01-01") == 0 && obj.repeat_tp != "NO" && obj.end_use_yn == "Y")
				{
					$('#end_date_hms_date').addClass("false");
					fnModalAlert("반복 종료 일시를 선택해 주세요.","알림");
					$('#end_date_hms_date').on("click",function(){$('#end_date_hms_date').removeClass("false");});
				}
				else if(obj.repeat_tp == "WK" && obj.repeat_val.value == "")
				{
					fnModalAlert("주별 설정은 요일을 하나이상 선택해야 합니다.","알림");
				}
				return;
			}
		}
	}
	
	function setRegTriggerType() {
		$('.input-card-type > [name="trigger"]').each(function() {
			if($(this).prop('checked'))
				data.regTriggerType.selected = $(this).attr('data-btn');
		});
	}
	
	function setRegTriggerConfig() {
		switch(data.regTriggerType.selected) {
		case 'http':
			getHttpTriggerConfig();
			break;
		case 'timer':
			getTimerTriggerConfig();
			break;
		case 'db':
			getDbTriggerConfig();
			break;
		default:
			data.regTriggerConfig.isSuccess = false;
		}
	}
	
	function runRegTrigger() {
		var param = {
				hubpop_projectId: $.cookie('PROJECT-ID'),
				function_id: "${paramMap.function_id}",
				trigger_type: data.regTriggerType.selected,
				trigger_config: JSON.stringify(data.regTriggerConfig)
		}
		
		var showProgress = true;
		
		$('#regTriggerModal').modal('hide');
		data.isTriggerModalShow = false;
		
		JsonCall('${pageContext.request.contextPath}/service/regTrigger.json', 'param=' + JSON.stringify(param), function(response) {
			ret = JSON.parse(response.responseText);
			console.log('/service/regTrigger.json', ret);
			
			if(ret.isSuccess == true || ret.isSuccess == "true") {
				dataLoad();
			} else {
				errorMessage = "" + ret.errorMessage;
				
				fnModalAlert(errorMessage, "알림");
			}
		}, showProgress);
	}
	
	function isValidAndGetTableList() {
		var db_config_obj = {
				db_type: $('#config_db_type').val(),
				con_host: $('#config_con_host').val(),
				con_port: $('#config_con_port').val(),
				db_name: $('#config_db_name').val(),
				admin_id: $('#config_admin_id').val(),
				admin_pw: $('#config_admin_pw').val()
		}
		
		for(var key in db_config_obj) {
			if(db_config_obj[key] == null || db_config_obj[key] == "") {
				$('#config_' + key).closest("div").addClass("form-danger-before").focus();
				return false;
			} else {
				$('#config_' + key).closest("div").removeClass("form-danger-before");
			}
		}
		
		var param = {
				function_id: "${paramMap.function_id}",
				db_type: db_config_obj.db_type,
				con_host: db_config_obj.con_host,
				con_port: db_config_obj.con_port,
				db_name: db_config_obj.db_name,
				admin_id: db_config_obj.admin_id,
				admin_pw: db_config_obj.admin_pw
		}
		
		data.dbTriggerModal.selectedDb = param.db_type;
		$('#check_db_validation').attr('disabled', true);
		
		var showProgress = false;
		$("#trigger-modal-mask").show();
		
		JsonCall('${pageContext.request.contextPath}/service/isValidAndGetTableList.json', 'param=' + JSON.stringify(param), function(response) {
			ret = JSON.parse(response.responseText);
			
			if(ret.isSuccess == true) {
				data.dbTriggerModal.isValidDb = ret.isValid;
				
				if(ret.tableList == null) {
					data.dbTriggerModal.tableList = [];
				} else {
					data.dbTriggerModal.tableList = ret.tableList;
				}

				if(ret.isValid === false)
					fnModalAlert('접속 URL 및 관리자 계정을 확인해 주세요.','접속 테스트 실패');
			} else {
				if(showProgress)
					global.showLoading(false);
				fnModalAlert('에러가 발생하였습니다.\n관리자에게 문의 바랍니다.','에러');
			}
			setDbConfigArea();
			$("#trigger-modal-mask").hide();
		}, showProgress);
	}
	
	function getHttpTriggerConfig() {
		var ret = httpTriggerInfo();
		data.regTriggerConfig = ret;
	}
	
	function getTimerTriggerConfig() {
		var ret = timerTriggerInfo();
		data.regTriggerConfig = ret;
	}
	
	function getDbTriggerConfig() {
		var ret = dbTriggerInfo();
		data.regTriggerConfig = ret;
	}
	
	function setDbMsgArea() {
		var db_types = ['mongodb', 'postgresql','mariadb'];
		var db_type = $('#config_db_type').val();
		
		for(var i in db_types) {
			if(db_types[i] == db_type) { $('#' + db_types[i] + '_msg_area').show(); }
			else { $('#' + db_types[i] + '_msg_area').hide(); }
		}
	}
	
	function setDbConfigArea() {

		var db_types = ['mongodb', 'postgresql','mariadb'];
		var db_type = $('#config_db_type').val();
		
		if(data.dbTriggerModal.isValidDb == true) {
			$('#check_db_validation').attr('title', '접속 테스트 완료');
			$('#check_db_validation').html('접속 테스트 완료');
			$('#check_db_validation').attr('disabled', true);
			
			for(var i in db_types) {
				if(db_types[i] == db_type) { $('#' + db_types[i] + '_config_area').show(); }
				else { $('#' + db_types[i] + '_config_area').hide(); }
			}
			
			var selected_db = data.dbTriggerModal.selectedDb;
			var table_list_id = selected_db + "_table_list";
			var err_msg_id = selected_db + "_empty_table_err";
			
			$("select#" + table_list_id + " option").remove();
			
			var tableList = data.dbTriggerModal.tableList;
			
			if(tableList.length != null && tableList.length != 0) {
				$("#" + err_msg_id).hide();
				
				if(data.dbTriggerModal.selectedDb == 'mongodb') {
					$("select#" + table_list_id).append("<option value=''>컬렉션 선택</option>");
				} else  if(data.dbTriggerModal.selectedDb == 'postgresql') {
					$("select#" + table_list_id).append("<option value=''>테이블 선택</option>");
				} else  if(data.dbTriggerModal.selectedDb == 'mariadb') {
					$("select#" + table_list_id).append("<option value=''>테이블 선택</option>");
				}
				
				for(var i=0; i<tableList.length; i++) {
					$("select#" + table_list_id).append("<option value='" + tableList[i] + "'>" + tableList[i] + "</option>");
				}
			} else {
				if(data.dbTriggerModal.selectedDb == 'mongodb') {
					$("select#" + table_list_id).append("<option value=''>컬렉션 선택</option>");
				} else  if(data.dbTriggerModal.selectedDb == 'postgresql') {
					$("select#" + table_list_id).append("<option value=''>테이블 선택</option>");
				} else  if(data.dbTriggerModal.selectedDb == 'mariadb') {
					$("select#" + table_list_id).append("<option value=''>테이블 선택</option>");
				}
		
				$("#" + err_msg_id).show();
			}
			
			$('#event_config_area').show();
		} else {
			for(var i in db_types) {
				$('#' + db_types[i] + '_config_area').hide();
			}

			$('#check_db_validation').attr('disabled', false);
			$('#event_config_area').hide();
		}
	}
	
	function resetDbConfigArea() {
		$('#check_db_validation').attr('title', '접속 테스트');
		$('#check_db_validation').html('접속 테스트');
		$('#check_db_validation').attr('disabled', false);
		
		var items = ['mongodb', 'postgresql', 'mariadb','event'];
		
		for(var i in items) {
			$('#' + items[i] + '_config_area').hide();
		}
	}
} );

function copyClipBoard(id)
{
	function copyToClipboard(val) 
	{
   		  var t = document.createElement("textarea");
   		  document.body.appendChild(t);
   		  t.value = val;
   		  t.select();
   		  document.execCommand('copy');
   		  document.body.removeChild(t);
   	}
	
	var tt = $('#'+id).text().trim();
	copyToClipboard(tt);
}

</script>
<div id="handlebarRoot">

			{{component "title" }}
				<!-- inner -->
				<div class="page-content">

					<!-- 콘텐츠 영역 -->
					<div class="inner-wrap">
						
						<!-- 콘텐츠가 들어갑니다 -->
						{{component "info"}}

						<!-- tab -->
						<div class="nav-tabs-wrap">
							<ul class="nav nav-tabs nav-tabs-line">
								<li class="active"><a href="javascript:void(0)">구성 정보​</a></li>
								<li><a href="javascript:void(0)" onclick="loadPage('layout/function/detail_monitoring.do?function_id=${paramMap.function_id}')">모니터링​</a></li>
								<li><a href="javascript:void(0)" onclick="loadPage('layout/function/detail_log.do?function_id=${paramMap.function_id}')">로그 조회​</a></li>
							</ul>
						</div><!-- tab  end-->

						<section class="panel panel-bordered">
							<div class="panel-heading">
								<h3 class="panel-title">
									함수 구성
								</h3>
							</div> <!-- // panel-heading -->
							<div class="panel-body">
								<div class="item_wrap">
									<div>
										<div>
											<p class="tit">트리거</p>
											{{#if (eq triggerCnt 0)}}
											<div class="dot_box" style="display: ;"><p>트리거가 등록되지 않았습니다.​</p></div><!-- 트리거가 추가되면 해당부분은 삭제 또는 display:none -->
											{{else}}
											<ul class="tri_item">
												{{#each triggerList}}
												<li>
													{{#if (eq trigger_tp 'http')}}
													<button class="btn btn-default" data-btn="HTTP" item-kind="trigger" item-id="{{trigger_id}}">
														<div class="feather-icon">
															<i data-feather="globe"></i>
														</div>
														HTTP
														{{#if (eq trigger_stat_cd 'INIT')}}
														<div class="ico_area">
															<span class="unicode xi-spin"></span>
														</div>
														{{else}}
														<div class="ico_area" deleteTriggerId="{{trigger_id}}">
															<div class="feather-icon font-size-13">
																<i data-feather="x"></i>
															</div>
														</div>
														{{/if}}
													</button>
													{{else if (eq trigger_tp 'ceph')}}
													<button class="btn btn-default" data-btn="Ceph" item-kind="trigger" item-id="{{trigger_id}}">
														<!-- <div class="feather-icon">
															<i class="icoTrig-s ico-ceph-s"></i>
														</div> -->
														<div class="feather-icon">
															<i class="img-size16 img-ceph"></i>
														</div>
														Ceph
														{{#if (eq trigger_stat_cd 'INIT')}}
														<div class="ico_area">
															<span class="unicode xi-spin"></span>
														</div>
														{{else}}
														<div class="ico_area" deleteTriggerId="{{trigger_id}}">
															<div class="feather-icon font-size-13" >
																<i data-feather="x"></i>
															</div>
														</div>
														{{/if}}
													</button>
													{{else if (eq trigger_tp 'db')}}
													<button class="btn btn-default" data-btn="DB" item-kind="trigger" item-id="{{trigger_id}}">
														<div class="feather-icon">
															<i data-feather="database"></i>
														</div>
														DB
														{{#if (eq trigger_stat_cd 'INIT')}}
														<div class="ico_area">
															<span class="unicode xi-spin"></span>
														</div>
														{{else}}
														<div class="ico_area" deleteTriggerId="{{trigger_id}}">
															<div class="feather-icon font-size-13">
																<i data-feather="x"></i>
															</div>
														</div>
														{{/if}}
													</button>
													{{else if (eq trigger_tp 'timer')}}
													<button class="btn btn-default" data-btn="Timer" item-kind="trigger" item-id="{{trigger_id}}">
														<div class="feather-icon">
															<i data-feather="clock"></i>
														</div>
														타이머
														{{#if (eq trigger_stat_cd 'INIT')}}
														<div class="ico_area">
															<span class="unicode xi-spin"></span>
														</div>
														{{else}}
														<div class="ico_area" deleteTriggerId="{{trigger_id}}">
															<div class="feather-icon font-size-13">
																<i data-feather="x"></i>
															</div>
														</div>
														{{/if}}
													</button>
													{{/if}}
												</li>
												{{/each}}
											</ul>
											{{/if}}
											<button class="btn btn-default" id="show_trig_modal">
												<div class="feather-icon">
													<i data-feather="plus"></i>
												</div>
												트리거 추가​
											</button>
										</div>
									</div>
									<div>
										<div>
											<p class="tit">함수</p>
											<ul class="tri_item">
												<li>
													<button class="btn btn-default {{#if (eq selectedItem.kind 'function')}}on{{/if}}" data-btn="Func" item-kind="function" item-id="{{funObj.function_id}}">
														<div class="feather-icon">
															<i data-feather="box"></i>
														</div>
														{{funObj.dsp_name}}
													</button>
												</li>
											</ul>
											<!-- <p class="txtLink txtR"><a href="#">레이어 ( <strong>2</strong> )</a></p> -->
										</div>
									</div>
									<div>
										<div>
											<p class="tit">대상​​</p>
											{{#if (eq targetList.length 0)}}
											<div class="dot_box"><p>결과 값을 전송할 대상을 추가해 주세요.​​​</p></div>
											{{else}}
											<ul class="tri_item">
												{{#each targetList}}
												<li>
													<button class="btn btn-default" item-kind="target" item-id="{{target_id}}">
														<div class="feather-icon">
															<i data-feather="file"></i>
														</div>
														{{dsp_name}}​
														<div class="ico_area" deleteTargetId="{{target_id}}">
															<div class="feather-icon font-size-13">
																<i data-feather="x"></i>
															</div>
														</div>
													</button>
												</li>
												{{/each}}
											</ul>
											{{/if}}
											<button class="btn btn-default" id="show_backend_modal" modal_id="#dialogBackend">
												<div class="feather-icon">
													<i data-feather="plus"></i>
												</div>
												대상 추가​
											</button>
										</div>
									</div>
								</div>
							</div> <!-- // panel-body -->
						</section>
						
						<!-- 트리거 상세 정보 -->
						{{#each triggerList}}
							{{#if (and (eq trigger_tp 'http') (eq ../selectedItem.kind 'trigger'))}}
								{{#if (eq trigger_id ../selectedItem.id)}}
									{{component "trigger_http_detail"}}
								{{/if}}
							{{else if (and (eq trigger_tp 'ceph') (eq ../selectedItem.kind 'trigger'))}}
								{{#if (eq trigger_id ../selectedItem.id)}}
									{{component "trigger_ceph_detail"}}
								{{/if}}
							{{else if (and (eq trigger_tp 'db') (eq ../selectedItem.kind 'trigger'))}}
								{{#if (eq trigger_id ../selectedItem.id)}}
									{{component "trigger_db_detail"}}
								{{/if}}
							{{else if (and (eq trigger_tp 'timer') (eq ../selectedItem.kind 'trigger'))}}
								{{#if (eq trigger_id ../selectedItem.id)}}
									{{component "trigger_timer_detail"}}
								{{/if}}
							{{/if}}
						{{/each}}
						
						<!-- 대상(target) 상세 정보 -->
						{{#each targetList}}
							{{#if (and (eq ../selectedItem.kind 'target') (eq target_id ../selectedItem.id))}}
								{{component "target_detail"}}
							{{/if}}
						{{/each}}
						
						
						<!-- 함수 상세 -->
						<div data-lst="function" data-tab="Func" style="display: block;">
						{{#if (or (eq stat 'REG') (neq selectedItem.kind "function"))}}
						<div class="hide">
						{{else}}
						<div>
						{{/if}}
							<section class="panel panel-bordered">
								<div class="panel-heading">
									<h3 class="panel-title">
										함수 편집​
									</h3>
								</div> <!-- // panel-heading -->
								<div class="panel-body">
									<!-- tab -->
									<div class="nav-tabs-wrap full-wrap">
										<ul class="nav nav-tabs source-fileName">
										{{#each sourceList}} <!-- li class에 active를 넣으면 활성 상태 -->
											<li ><a href="#" title="{{fileName}}" idx="{{@key}}">{{fileName}}</a></li>
										{{/each}}
										</ul>
									</div>

									<!-- edit -->
									<div class="pos-r" id="" style="display: block;">
										<div id="editor" class="editorBox"></div>
										<p class="saved" style="display: none;">저장되었습니다.</p>
									</div>

									<!-- button -->
									<div class="text-center margin-top-20">
										<button id="saveBtn" class="btn btn-primary width-100">저장​</button>
									</div>
								</div> <!-- // panel-body -->
							</section>
						
							<section class="panel panel-bordered">
								<div class="panel-heading">
									<h3 class="panel-title">
										외부 함수 라이브러리 선택
										<span class="info-icon-tooltip" data-toggle="tooltip" title="" data-placement="right" data-original-title="함수에 필요한 라이브러리를 zip파일로 묶어서 업로드 하면  라이브러리를 사용할 수 있습니다.​">
										i
										</span>
									</h3>
									
								</div> <!-- // panel-heading -->
								<div class="panel-body">
									<div class="select-file-area">
										<div class="select-file-box">
										{{#if attachList}}
											<ul class="select-file-list">
											{{#each attachList}}
												<li>
													{{file_name}} 
													<a href="#" class="btn btn-nostyle btn-xs" id="attach_file_delete">
														<i class="xi-close"></i>
													</a>
												</li>
											{{/each}}
											</ul>
										{{else}}
											<div class="btn btn-info btn-xs file-input-btn btn-file">
												파일 선택
												<form id="uploadForm" enctype="multipart/form-data" method="POST">
													<input type="file" id="file" name="file"/>
													<input type="hidden" id="text" name="function_id" value="${paramMap.function_id}"/>
												</form>
											</div>
											<small id="attach_file_name">선택된 파일 없음</small>
										{{/if}}
										</div>
									</div>
								</div> <!-- // panel-body -->
							</section>
						</div>
							{{#if (or (eq stat 'REG') (neq selectedItem.kind "function"))}}
							{{else}}
							<!-- 환경변수, 트래픽 설정 -->
							<section class="row">
								<div class="col-md-6">
									<div class="card card-type1">
										<div class="card-header">
											<div class="card-info-area">
												<div class="text-box">
													<div class="top">
														<div class="row">
															<div class="col-xs-10">
																<div class="title inline-block">
																	<b>
																		환경 변수
																	</b>
																	<span class="info-icon-tooltip" data-toggle="tooltip" title="" data-placement="right" data-original-title="함수 코드에서 액세스할 수 있는 키/값을 설정하면, 함수 코드 변경 없이 간편하게 이용할 수 있습니다.​">
																		i
																	</span>
																</div>
															</div>
															<div class="col-xs-2">
																<div class="dropdown pull-right margin-top-5">
																	<button type="button" class="btn btn-xs btn-nostyle dropdown-toggle" data-toggle="dropdown">
																		<div class="feather-icon">
																			<i data-feather="more-horizontal"></i>
																		</div>
																	</button>
																	<ul class="dropdown-menu" role="menu">
																		<li>
																			<a href="#" name="show_config_modal" modal_id="#dialogEnv">
																				<i data-feather="settings" class="dropdown-icon"></i>
																				설정
																			</a>
																		</li>
																	</ul>
																</div>	
															</div>
														</div>
													</div>
												</div>
											</div>
										</div>
										<div class="card-body">
											{{#if (or (eq config.envLength null) (eq config.envLength undefined) (eq config.envLength 0) (eq config.envLength ''))}}
											<div class="nodata-box" style="height: 205px;">
												<div class="img-div">
													<p class="text-center">등록된 데이터가 없습니다. ​</p>
												</div>
											</div>
											{{else}}
											<div class="scroll-content scroll-line" style="height: 205px">
												<div class="table-wrap">
													{{component "envTable"}}
												</div>
											</div>
											{{/if}}
										</div>
									</div>
								</div>
								<div class="col-md-6">
									<div class="card card-type1">
										<div class="card-header">
											<div class="card-info-area">
												<div class="text-box">
													<div class="top">
														<div class="row">
															<div class="col-xs-10">
																<div class="title inline-block">
																	<b>
																		트래픽 설정​
																	</b>
																	<span class="info-icon-tooltip" data-toggle="tooltip" title="" data-placement="right" data-original-title="함수에  추가된  HTTP 트리거의 트래픽 제한 값을 설정합니다.​">
																		i
																	</span>
																</div>
															</div>
															<div class="col-xs-2">
																<div class="dropdown pull-right margin-top-5">
																	<button type="button" class="btn btn-xs btn-nostyle dropdown-toggle" data-toggle="dropdown">
																		<div class="feather-icon">
																			<i data-feather="more-horizontal"></i>
																		</div>
																	</button>
																	<ul class="dropdown-menu" role="menu">
																		<li>
																			<a href="#" name="show_config_modal" modal_id="#dialogTraffic">
																				<i data-feather="settings" class="dropdown-icon"></i>
																				설정
																			</a>
																		</li>
																	</ul>
																</div>	
															</div>
														</div>
													</div>
												</div>
											</div>
										</div>
										<div class="card-body">
											<div class="row">
												<div class="col-md-6 img-content-type2">
													<div class="img-zone img-burst img-size70">
														<span class="sr-only">Burst Limit​</span>
													</div>
													<div class="name">
														초당 제한 (Burst Limit)​
													</div>
													<span class="font-size-20">
														<b>{{config.trf.burst}}건/초​</b>
													</span>
												</div>
												<div class="col-md-6 img-content-type2">
													<div class="img-zone img-quota img-size70">
														<span class="sr-only">Quota Limit​</span>
													</div>
													<div class="name">
														기간별 제한 (Quota Limit)​
													</div>
													<span class="font-size-20">
														<b>{{config.trf.quota.cnt}}건/{{config.trf.quota.val}}{{config.trf.quota.typenm}}​</b>
													</span>
												</div>
											</div>
										</div>
									</div>
								</div>
							</section>

							<!-- 기본 설정, 동시성 -->
							<section class="row">
								<div class="col-md-6">
									<div class="card card-type1">
										<div class="card-header">
											<div class="card-info-area">
												<div class="text-box">
													<div class="top">
														<div class="row">
															<div class="col-xs-10">
																<div class="title inline-block">
																	<b>
																		기본 설정
																	</b>
																	<span class="info-icon-tooltip" data-toggle="tooltip" title="" data-placement="right" data-original-title="함수에 사용할 메모리 및 제한 시간을 설정합니다.​">
																		i
																	</span>
																</div>
															</div>
															<div class="col-xs-2">
																<div class="dropdown pull-right margin-top-5">
																	<button type="button" class="btn btn-xs btn-nostyle dropdown-toggle" data-toggle="dropdown">
																		<div class="feather-icon">
																			<i data-feather="more-horizontal"></i>
																		</div>
																	</button>
																	<ul class="dropdown-menu" role="menu">
																		<li>
																			<a href="#" name="show_config_modal" modal_id="#dialogResource">
																				<i data-feather="settings" class="dropdown-icon"></i>
																				설정
																			</a>
																		</li>
																	</ul>
																</div>	
															</div>
														</div>
													</div>
												</div>
											</div>
										</div>
										<div class="card-body">
											<div class="row">
												<div class="col-md-4 img-content-type2">
													<div class="img-zone img-memory img-size70"></div>
													<div class="name">
														메모리(MB)​
													</div>
													<span class="font-size-20">
														<b>{{config.mem}}​</b>
													</span>
												</div>
												<div class="col-md-4 img-content-type2">
													<div class="img-zone img-cpu img-size70"></div>
													<div class="name">
														CPU(m)
													</div>
													<span class="font-size-20">
														<b>{{config.cpu}}</b>
													</span>
												</div>
												<div class="col-md-4 img-content-type2">
													<div class="img-zone img-limit img-size70"></div>
													<div class="name">
														제한 시간​
													</div>
													<span class="font-size-20">
														<b>{{config.timeout.min}}분 {{config.timeout.sec}}초​​</b>
													</span>
												</div>
											</div>
										</div>
									</div>
								</div>
								<div class="col-md-6">
									<div class="card card-type1">
										<div class="card-header">
											<div class="card-info-area">
												<div class="text-box">
													<div class="top">
														<div class="row">
															<div class="col-xs-10">
																<div class="title inline-block">
																	<b>
																		함수 스케일 최대값 설정
																	</b>
																	<span class="info-icon-tooltip" data-toggle="tooltip" title="" data-placement="right" data-original-title="함수가 동시에 실행될 수 있는 건 수를 설정합니다.​">
																		i
																	</span>
																</div>
															</div>
															<div class="col-xs-2">
																<div class="dropdown pull-right margin-top-5">
																	<button type="button" class="btn btn-xs btn-nostyle dropdown-toggle" data-toggle="dropdown">
																		<div class="feather-icon">
																			<i data-feather="more-horizontal"></i>
																		</div>
																	</button>
																	<ul class="dropdown-menu" role="menu">
																		<li>
																			<a href="#" name="show_config_modal" modal_id="#dialogConcur">
																				<i data-feather="settings" class="dropdown-icon"></i>
																				설정
																			</a>
																		</li>
																	</ul>
																</div>	
															</div>
														</div>
													</div>
												</div>
											</div>
										</div>
										<div class="card-body">
											<div class="row">
												<div class="col-md-12 img-content-type2">
													<div class="img-zone img-concurrency img-size70">
														<span class="sr-only">제한 시간​​</span>
													</div>
													<div class="name">
														함수 스케일 최대값​
													</div>
													<span class="font-size-20">
														<b>{{config.concur}}</b>
													</span>
												</div>
											</div>
										</div>
									</div>
								</div>
							</section>
							{{/if}}
							
							{{#if (neq step 'NONE')}}
								{{component "reg_step"}}
							{{/if}}
							
							{{#if (eq stat 'ERROR')}}
							<section class="panel panel-bordered">
								<div class="panel-heading">
									<h3 class="panel-title">
										오류 내역
									</h3>
								</div> <!-- // panel-heading -->
								<div class="panel-body">
									<div id="errorEditor" class="editorBox" style="height">
										EDITOR 가 들어가는 영역입니다.
									</div>
								</div> <!-- // panel-body -->
							</section>
							{{/if}}
						</div>

					</div>
				</div>
				<!-- // inner -->
				
				<!-- modal -->
				<!-- 팝업(클립보드에 복사) -->
				<div class="modal" id="modalCopy">
					<div class="modal-dialog modal-sm">
						<div class="modal-content">
							<div class="modal-header">
								<button type="button" class="close" data-dismiss="modal" aria-label="Close">
									<span aria-hidden="true">×</span>
								</button>
								<h4 class="modal-title">알림</h4>
							</div>
							<div class="modal-body">
								<p>
									클립보드에 복사되었습니다.
								</p>
							</div>
							<div class="modal-footer">
								<!-- <button type="button" class="btn btn-default" data-dismiss="modal">Close</button> -->
								<button type="button" class="btn btn-primary" data-dismiss="modal">확인</button>
							</div>
						</div>
					</div>
				</div>

{{#if isTriggerModalShow}}
	{{component "trigger_modal"}}
{{/if}}

{{component "detail_config"}}
{{component "target_function"}}

</div><!-- end handlebarRoot -->
<script>
function actionTriggerModal() {
	$('[data-btn="http"]').trigger("click");
	$('[data-lst="admin"]').css("display", "block");
}
</script>
