<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %> 
<spring:eval expression="@environment.getProperty('websocket.base.url')" var="webSocketBaseUrl" />    

<script src="${pageContext.request.contextPath}/core/js/ace-builds/src-noconflict/ace.js" type="text/javascript" charset="utf-8"></script>
<script src="${pageContext.request.contextPath}/core/js/ace-builds/src-noconflict/ext-modelist.js"></script>
<script src="${pageContext.request.contextPath}/core/js/ace-builds/src-noconflict/ext-language_tools.js"></script>

<!-- diff 관련 -->
<script src="${pageContext.request.contextPath}/core/js/diff2html/diff2html.min.js"></script>
<script src="${pageContext.request.contextPath}/core/js/diff2html/diff2html-ui.min.js"></script>

<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/core/css/diff2html/diff2html.min.css"/>
<!-- diff 관련 -->
<style type="text/css" media="screen">
	.edit_box {background: #ffffff !important}
</style>

<script>
    $(document).ready( function() {
        var data = {
            function_id: '${paramMap.function_id}',
            isShowListButton: true,
            function: {
                detail: {},
                source: {
                    list: [],
                    _selectedIndex: 0
                }
            },
            trigger: {
                list: [],
                http: {},
                ceph: {},
                timer: {},
                database: {}
            },
            target: {
                list: []
            },
            monitoring: {
                params: {
                    function_id: '${paramMap.function_id}',
                    unitType: 'D', // H: 시간, D: 일, W: 주, M: 개월
                    stepCnt: 7,
                },
                list: [],
                c3: {}
            },
            functionLog: {
                params: {
                    function_id: '${paramMap.function_id}',
                    limit: 10,
                    curpage: 1,
                    log_cd: '' // MODIFY, ERROR
                },
                list: [],
                detail: {},
                function_log_id: 0,
                totalCount: 0,
                _selectedKind: "LIST" // LIST, DETAIL
            },
            nav: { 
                curpage: 1, 
                lastpage: 1, 
                start: 1, 
                end: 1, 
                onclick: onNavClick 
            },
            _selectedTabKind: 'COMPOSITION', // COMPOSITION, MONITORING, LOG
            _selectedCompositionKind: 'FUNCTION', // 'FUNCTION', 'HTTP', 'CEPH', 'DATABASE', 'TIMER',
            _selectedTriggerId: 0,
            _steps: {
                STEP1: { name: '1단계', desc: '소스 저장'},
                STEP2: { name: '2단계', desc: '체크 아웃'},
                STEP3: { name: '3단계', desc: '이미지 빌드'},
                STEP4: { name: '4단계', desc: '이미지 배포'},
                STEP5: { name: '5단계', desc: '서비스 배포'}
            },
            _reload: reload
        };
        
        NtelsHB.rootNodeSelector = "#handlebarRoot";
        NtelsHB.onComponentReady = function() {
            NtelsHB.bind(data);
            dataLoad(data._selectedTabKind);
        }

        NtelsHB.afterRender = function() {    
            if (data._selectedTabKind === 'COMPOSITION'){
                // source 탭 이벤트 등록
                $("a[name=sourceNavTab]").on('click', function(e) {
                    if(data.function.source._selectedIndex === $(this).attr("idx")) return;
                    data.function.source._selectedIndex = $(this).attr("idx");
                    NtelsHB.render();
                });

                var dataBtn = data._selectedCompositionKind;
                if (data._selectedCompositionKind !== 'FUNCTION') dataBtn += data._selectedTriggerId;
                $('.item_wrap').find('button').removeClass('on');
                $('[data-btn='+ dataBtn +']').addClass('on');
                $('[data-tab='+ data._selectedCompositionKind +']').show();

                $('html,body').animate({scrollTop: $('[data-tab='+ data._selectedCompositionKind +']').offset().top}, 500);

                var fileContent = data.function.source.list[data.function.source._selectedIndex].fileContent;
                if ( fileContent === undefined ) fileContent = "";
                createEditor('editor', fileContent);
                if(data.function.detail.stat_cd === 'ERROR') createEditor('errorEditor', data.function.detail.log);
            } else if(data._selectedTabKind === 'LOG') {
                $("#logCode").on("change", function() {
                    data.functionLog.params.log_cd = $(this).val();
                    data.nav.curpage = 1;
                    dataLoad("LOG");
                });
                $("#logCode").val(data.functionLog.params.log_cd).prop("selected", true);

                if(data.functionLog._selectedKind === 'DETAIL') {
                    if(data.functionLog.detail.log_cd === 'ERROR') {
                        createEditor('errorLogEditor', data.functionLog.detail.logtext);
                    } else if(data.functionLog.detail.log_cd === 'MODIFY' && data.functionLog.detail.log_dtl_cd === 'REVISION') {
                        createEditor("sourceEditor", data.functionLog.detail.diffList[0].content);
                        var el = document.getElementById("diffView");
                        var configuration = { inputFormat: 'json', drawFileList: false, matching: 'none', highlight: true };
                        var diff2htmlUi = new Diff2HtmlUI(el, data.functionLog.detail.diffList[0].diff, configuration);
                        diff2htmlUi.draw();
                        diff2htmlUi.highlightCode();
                    }
                }
            }

            // TAB 클릭 이벤트 등록
            $('a[name=navTab]').bind('click', function() {
                var tabKind = $(this).attr('nav-tab-data');
                if (tabKind !== data._selectedTabKind) reload(tabKind, "LIST");
                return false;
            });
            
            // 선택된 TAB 클래스 추가
            $('[nav-tab-data='+ data._selectedTabKind +']').parent().addClass('active');
        }

        function dataLoad(kind) {
            if(kind === "MONITORING") {
                var url = '${pageContext.request.contextPath}/service/admin/getMonitoring.json';
                prom.get(url, data.monitoring.params)
                .then(function(result) {
                    data.monitoring.list = result.list;
                    data.monitoring.c3 = result.c3;
                    NtelsHB.render();
                })
                .catch(function(reason){ NtelsHB.render(); });
            } else if(kind === "LOG") {
                if(data.functionLog._selectedKind === 'DETAIL') {
                    var params = { 
                        function_id: data.function_id,
                        function_log_id: data.functionLog.function_log_id 
                    };
                    var url = '${pageContext.request.contextPath}/service/admin/getFunctionLog.json';
                    prom.get(url, params)
                    .then(function(result) {
                        data.functionLog.detail = result;
                        data.functionLog.detail.diffList = result.diff_list.map(function(diff) {
                            diff.diff = "--- a/" + diff.oldPath+"\n+++ b/" + diff.newPath+"\n" + diff.diff;
                            return diff;
                        });
                        NtelsHB.render();
                    })
                    .catch(function(reason){ NtelsHB.render(); });
                } else { // LIST
                    var url = '${pageContext.request.contextPath}/service/admin/getFunctionLogList.json'; 
                    data.functionLog.params.curpage = data.nav.curpage;
                    prom.get(url, data.functionLog.params)
                    .then(function(result) {
                        data.functionLog.totalCount = result.totalCount;
                        data.functionLog.list = result.list;
                        callNav(data, result.curpage, result.lastpage);
                        NtelsHB.render();
                    })
                    .catch(function(reason){ NtelsHB.render(); });
                }
            } else {
                var params = {function_id: data.function_id};
                var url = '${pageContext.request.contextPath}/service/admin/getFunction.json';
                prom.get(url, params).then(function(result){
                    var _function = result.function;
                    var _triggers = result.triggers || [];
                    var _targets = result.targets || [];

                    _function.trf = result.function.trf || {burst: 100 , quota: {type: 'D' , val: 1 , cnt: 10000 , typenm: '일'}}; 
                    _function.mem = result.function.mem || 128;
                    _function.cpu = result.function.cpu || 1000;
                    if(_function.timeout == null) result.function.timeout = 60;
                    _function.timeout = {
                        min: parseInt(result.function.timeout / 60),
                        sec: parseInt(result.function.timeout % 60)
                    };
                    _function.concur = result.function.concur || 1000;

                    data.trigger.list = _triggers;
                    data.function.detail = _function;
                    data.target.list = _targets;

                    var params = {function_id: data.function_id};
                    return prom.get('${pageContext.request.contextPath}/service/admin/getFunctionSourceList.json', params);
                })
                .then(function(result) { 
                    data.function.source.list = result;
                    NtelsHB.render();
                })
                .catch(function(response) { NtelsHB.render(); });
            }
        }

        function createEditor(elementId, content) {
            if ($('#' + elementId).length === 0) return;
            var editor = ace.edit(elementId);
            
            editor.setOptions({
                theme: "ace/theme/textmate",
                fontSize: "16px",
                readOnly: true
            });
            editor.session.setOption("useWorker", true);
            editor.setValue(content+"\n");
            editor.clearSelection();
        }

        function componentLoad() {
            var contextPath = "${pageContext.request.contextPath}";
            var rootPath = contextPath + "/component/admin/function/";
            var commonComponentPath = contextPath + "/common/component/";
            
            NtelsHB.registerComponent("title", rootPath + "title.do");
            NtelsHB.registerComponent("info", rootPath + "detail/info.do");
            NtelsHB.registerComponent("composition", rootPath + "detail/composition.do");
            NtelsHB.registerComponent("source", rootPath + "detail/source.do");
            NtelsHB.registerComponent("environment", rootPath + "detail/environment.do");
            NtelsHB.registerComponent("traffic", rootPath + "detail/traffic.do");
            NtelsHB.registerComponent("defaultConfig", rootPath + "detail/defaultConfig.do");
            NtelsHB.registerComponent("concurrency", rootPath + "detail/concurrency.do");
            NtelsHB.registerComponent("regStep", rootPath + "detail/regStep.do");
            NtelsHB.registerComponent("http", rootPath + "detail/triggerHttp.do");
            NtelsHB.registerComponent("ceph", rootPath + "detail/triggerCeph.do");
            NtelsHB.registerComponent("database", rootPath + "detail/triggerDatabase.do");
            NtelsHB.registerComponent("timer", rootPath + "detail/triggerTimer.do");
            NtelsHB.registerComponent("monitoring", rootPath + "detail/monitoring.do");
            NtelsHB.registerComponent("log", rootPath + "detail/log.do");
            NtelsHB.registerComponent("logList", rootPath + "detail/logList.do");
            NtelsHB.registerComponent("logDetail", rootPath + "detail/logDetail.do");
            NtelsHB.registerComponent("pagination", rootPath + "list/pagination.do");

            NtelsHB.loadComponent();
        }

        function reload(tabKind, logKind) {
            data._selectedTabKind = tabKind || "COMPOSITION";
            data._selectedCompositionKind = "FUNCTION";
            data._selectedTriggerId = 0;
            data.function.source._selectedIndex = 0;
            if(logKind !== undefined) data.functionLog._selectedKind = logKind;
            dataLoad(tabKind);
        }

        function callNav(data, curpage, lastpage) {
            data.nav.curpage = curpage;
            data.nav.lastpage = lastpage;

            data.nav.start = Math.floor((curpage-1)/10)*10 + 1;
            data.nav.end = data.nav.start + 10 - 1;
            
            if(data.nav.end > data.nav.lastpage) data.nav.end = data.nav.lastpage;  
        }

        function onNavClick(page) {
            data.nav.curpage = page;
            dataLoad("LOG");
        }

        componentLoad();
    });
</script>

<div id="handlebarRoot">
    <main id="contentWrap" class="dashboard-wrap">
        {{component "title"}}

        <div class="page-content">
            <div class="inner-wrap">

                <!-- 콘텐츠가 들어갑니다 -->
                {{component "info"}}
                
                <ul class="nav nav-tabs nav-tabs-line">
                    <li><a href="#" name="navTab" nav-tab-data="COMPOSITION">구성 정보​</a></li>
                    <li><a href="#" name="navTab" nav-tab-data="MONITORING">모니터링​</a></li>
                    <li><a href="#" name="navTab" nav-tab-data="LOG">로그 조회​</a></li>
                </ul>

                {{#if (eq _selectedTabKind 'COMPOSITION')}}
                    {{ component "composition" }}
                {{else if (eq _selectedTabKind 'MONITORING')}}
                    {{ component "monitoring" }}
                {{else if (eq _selectedTabKind 'LOG')}}
                    {{ component "log" }}
                {{/if}}
            </div>
        </div>
    </main>
</div>