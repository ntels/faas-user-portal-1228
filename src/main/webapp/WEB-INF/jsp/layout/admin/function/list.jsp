<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
    
<script>
    $(document).ready( function() {
        var data = {
            totalCount: 0,
            list: [],
            tableOptions: {
                widths: [250, null, 100, 150, 100, 100, 110, 120], 
                headers: ['프로젝트​', '함수​', '상태​', '런타임​', '트리거', '대상', '등록 일시​', '등록자​']
            },
            nav:{ curpage: 1, lastpage: 1, start: 1, end: 1, onclick: onNavClick },
            projects: [],
            limit: 10,
            projectId: '',
            sch_runtime: '',
            namespace: '',
            keyword: '',
            order_by: 'create',
            _reload: reload
        };

        NtelsHB.rootNodeSelector = "#handlebarRoot";       
        NtelsHB.onComponentReady = function() {
            NtelsHB.bind(data);
            dataLoad();
        }
        NtelsHB.afterRender = function() {
            $("#project").on("change", function() {
                data.projectId = $(this).val();
                data.nav.curpage = 1;
                dataLoad();
            });
            
            $("#sch_runtime").on("change", function() {
                data.sch_runtime = $(this).val();
                data.nav.curpage = 1;
                dataLoad();
            });

            $("#order_by").on("change", function() {
                data.order_by = $(this).val();
                data.nav.curpage = 1;
                dataLoad();
            });
            
            $("#limit").on("change", function() {
                data.limit = $(this).val();
                data.nav.curpage = 1;
                dataLoad();
            });

            $("#btnSearch").on("click", function() {
                data.keyword = $('#keyword').val();
                data.nav.curpage = 1;
                dataLoad();
            });

            $("#keyword").on("keypress", function(e) {
                if (e.keyCode === 13) {
                    data.keyword = $('#keyword').val();
                    data.nav.curpage = 1;
                    dataLoad();
                }
            });

            $("#project").val(data.projectId).prop("selected", true);
            $("#sch_runtime").val(data.sch_runtime).prop("selected", true);
		    $("#order_by").val(data.order_by).prop("selected", true);
		    $("#limit").val(data.limit).prop("selected", true);
            $("#keyword").val(data.keyword);
        }

        function dataLoad() {
            var params = {
                limit: data.limit,
				curpage: data.nav.curpage,
                sch_runtime: data.sch_runtime,
                projectId: data.projectId,
                order_by: data.order_by,
                keyword: data.keyword
            };

            prom.get('${pageContext.request.contextPath}/service/admin/getFunctionList.json', params)
            .then(function(result){
                data.list = result.list;
                data.totalCount = result.totalCount;
                callNav(data, result.curpage, result.lastpage);
                NtelsHB.render();
            })
            .catch(function(reason){
                NtelsHB.render();
            });

            prom.get('${pageContext.request.contextPath}/service/admin/getProjectList.json', {})
            .then(function(result){
                data.projects = result;
                NtelsHB.render();
            })
            .catch(function(reason){
                NtelsHB.render();
            });
        }

        function componentLoad() {
            var contextPath = "${pageContext.request.contextPath}";
            var rootPath = contextPath + "/component/admin/function/";
            var commonComponentPath = contextPath + "/common/admin/component/";
            
            NtelsHB.registerComponent("title", rootPath + "title.do");
            NtelsHB.registerComponent("search", rootPath + "list/search.do");
            NtelsHB.registerComponent("pagination", rootPath + "list/pagination.do");
            NtelsHB.registerComponent("table", rootPath + "list/table.do");
            NtelsHB.registerComponent("tableItem", rootPath + "list/tableItem.do");

            NtelsHB.loadComponent();
        }

        componentLoad();

        function callNav(data, curpage, lastpage) {
            data.nav.curpage = curpage;
            data.nav.lastpage = lastpage;

            data.nav.start = Math.floor((curpage-1)/10)*10 + 1;
            data.nav.end = data.nav.start + 10 - 1;
            
            if(data.nav.end > data.nav.lastpage) {
                data.nav.end = data.nav.lastpage;  
            }
        }

        function onNavClick(page) {
            data.nav.curpage = page;
            dataLoad();
        }

        function reload() {
            data.nav = { curpage: 1, lastpage: 1, start: 1, end: 1, onclick: onNavClick };
            data.sch_runtime = '';
            data.namespace = '';
            data.keyword = '';
            data.order_by = 'create';
            dataLoad();
        }
    });
</script>

<div id="handlebarRoot">
    <main id="contentWrap" class="dashboard-wrap">
        {{component "title"}}
  
        <!-- inner -->
        <div class="page-content">
            <div class="inner-wrap">
                {{#if (and (eq project '') (eq sch_runtime '') (eq namespace '') (eq keyword '') (eq totalCount 0)) }}
                    <div class="nodata-box type2">
                        <div class="img-div">
                            <p class="text-center">등록된 데이터가 없습니다. ​</p>
                        </div>
                    </div>
                {{else}}
                    <!-- 검색 영역 -->
                    {{component "search"}}
                    <!-- 검색 영역 -->
                    {{#if (neq totalCount 0)}}
                    <!-- 목록 영역 -->
                    <section class="panel panel-bordered bt-0">
                        <div class="panel-body padding-0">
                            {{component "table"}}
                            {{component "pagination"}}
                        </div>
                    </section>
                    {{else}}
                    <!-- 검색결과가 없습니다. -->
                    <div class="nodata-box type2">
                        <div class="nodata">
                            <div class="img-div"></div>
                            <p>검색된 데이터가 없습니다. <span class="blue bold">다른 조건으로 검색해 보세요.​</span></p>
                        </div>
                    </div>
                    {{/if}}
                {{/if}}
            </div>
        </div>
    </main>
</div>