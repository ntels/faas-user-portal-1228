<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>  
<script type="text/javascript">
    //Ajax로 첫 화면의 데이터 호출
    $(document).ready(function() {
    	initPaging('pageNavigationDiv2', "${resultList.page}", "${resultList.perPage}", "${resultList.perSize}", "${resultList.totalCount}","\'dashboard/listAction\'");
    });
</script>    
<div class="panel-body padding-0">
	<div class="table-wrap">
		<table class="table" cellspacing="0" width="100%">
			<colgroup>
				<col style="width:250px;">
				<col>
				<col style="width:100px;">
				<col style="width:150px;">
				<col style="width:100px;">
				<col style="width:100px;">
				<col style="width:110px;">
				<col style="width:100px;">
			</colgroup>
			<thead>
				<tr>	
					<th class="text-center">프로젝트​</th>
					<th class="text-center">함수​</th>
					<th class="text-center">상태​</th>
					<th class="text-center">런타임​</th>
					<th class="text-center">트리거</th>
					<th class="text-center">대상​</th>
					<th class="text-center">등록 일시​</th>
					<th class="text-center">등록자​</th>
				</tr>
			</thead>
			<tbody>
				<c:if test="${!empty resultList.lists}">
					<c:forEach items="${resultList.lists}" var="resultModel" varStatus="status">
						<tr>
							<td>${resultModel.namespace}</td>
							<td>
								<a href="javascript:goProject('${resultModel.namespace}');">${resultModel.dsp_name}</a>
								<div class="ellipsis-2">${resultModel.last_sync_des}</div>
							</td>
							<td class="text-center">
								<span class="state">
									<div class="state-round reg"></div>
									${resultModel.stat_cd}
								</span>
							</td>
							<td class="text-center">${resultModel.runtime} ${resultModel.version}</td>
							<td class="text-center">${resultModel.triggerCnt}건​</td>
							<td class="text-center">${resultModel.objCnt}건​</td>
							<td class="text-center">${resultModel.reg_date_hms}​</td>
							<td class="text-center">${resultModel.reg_usr}​</td>
						</tr>
					</c:forEach>
				</c:if>
			</tbody>
		</table>
	</div>
	
	<div class="table-bottom-wrap">
		<div class="row">
			<div class="datatable-paginate text-center">
				<ul class="pagination" id="pageNavigationDiv2"></ul>
			</div>
		</div>
	</div>
	<c:if test="${empty resultList.lists}">
		<!-- 검색결과가 없습니다. -->
		<div class="nodata type1">
			<div class="img-div"></div>
			<p>검색된 데이터가 없습니다. <span class="blue bold">다른 조건으로 검색해 보세요.​</span></p>
		</div>
	</c:if>

</div>
