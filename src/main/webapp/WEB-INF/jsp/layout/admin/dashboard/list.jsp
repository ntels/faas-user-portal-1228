<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>  

<script type="text/javascript" src="${pageContext.request.contextPath}/core/admin/js/paging.js"></script>

<script>
var condition = ${condition};
console.log(condition);
$(document).ready(function() {
	$("#namespace").val(condition.namespace);
	$("#contentWrap").css("display","block");
	goSearch();
	$( "#runtime").change(function() {
		goSearch();	
	}); 
	$( "#orderBy").change(function() {
		goSearch();	
	}); 	
});

function goSearchKeyword() {
/* 	var keyword= $("#keyword").val();
	if(keyword =='') {
		alert("검색어를 입력하세요.");
		return false;
	} */
	goSearch();
}


function goSearch(){
	var param = $("#myForm").serialize();
	//console.log(condition.forward);
	console.log("action :"+JSON.stringify(param));
	formData = param;
	$.ajax({
        url : condition.action,
        data : param,
        type : 'POST',
        success : function(data) {
           $("#dataTable").html(data);
        },
        error: function(request, status, error){	        	
        	alert("code : "+ request.status + "\r\nmessage : "+request.responseText + "\r\error : "+error);
        },
        complete : function() {
        }
	});
}
</script>

<form id="myForm" name="myForm">
<input type="hidden" id="namespace" name="namespace"/>
<!-- <input type="hidden" id="orderBy" name="orderBy"/> -->
<main id="contentWrap" class="dashboard-wrap">

				<div class="page-title">
					<div id="spot">
						<div class="page-info">
							함수 관리
						</div>
						<div class="page-location">
							<span class="home">
								<i class="xi-home-o"></i>
								HUBPoP
							</span>
							<span>함수 관리</span>
						</div>
						<a href="" class="btn-refresh">
							<i class="xi-renew"></i>
							<p class="sr-only">새로고침</p>
						</a>
					</div>
					
				</div>

				<!-- inner -->
				<div class="page-content">

					<div class="inner-wrap">

						<!-- 콘텐츠가 들어갑니다 -->
						<div class="card-input-wrap">
							<div class="row">
								<div class="col-md-6">
									<div class="font-size-18 padding-top-5">
										<b>함수 <span class="color-primary">(22)</span></b>
									</div>
								</div>
								<div class="col-md-6">
									<div class="text-right">
										<div class="form-group inline-block margin-bottom-0">
											<select name="" id="" class="form-control">
												<option value="">프로젝트</option>
								                  <c:forEach items="${comboProjectList}" var="i" varStatus="status">
								                  	  <option value="${i.namespace}" >${i.namespace}</option>
								                  </c:forEach>										
											</select>
										</div>
										<div class="form-group inline-block margin-bottom-0">
											<select name="runtime" id="runtime" class="form-control">
												<option value="">런타임 유형</option>
												<option value="nodejs">Node.js</option>
												<option value="python">Python</option>
											</select>
										</div>
										<div class="form-group inline-block margin-bottom-0">
											<select name="orderBy" id="orderBy" class="form-control">
												<option value="reg_date_hms">등록일 순​</option>
												<option value="mod_date_hms">수정일 순</option>
												<option value="dsp_name">이름 순​​</option>
											</select>
										</div>
										<div class="form-group inline-block margin-bottom-0">
											<input type="text" class="form-control" id="keyword" name="keyword" placeholder="키워드 검색​">
										</div>
										<button type="button" class="btn btn-primary" onClick="javascript:goSearch();">
											<div class="feather-icon">
												<i data-feather="search"></i>
											</div>
											<span class="sr-only">Button</span>
										</button>
									</div>
								</div>
							</div>
						</div>

						<section class="panel panel-bordered bt-0" id="dataTable">
						</section>

<!-- 						<section>
							데이터가 없습니다.
							<div class="nodata-box type2">
								<div class="img-div">
									<p class="text-center">등록된 데이터가 없습니다. ​</p>
								</div>
							</div>
						</section> -->

						<!-- //콘텐츠가 들어갑니다 -->
						
					</div>
				</div>
				<!-- // inner -->
				
				<!-- modal -->
				<!-- //modal -->


			</main>
			
</form>			
