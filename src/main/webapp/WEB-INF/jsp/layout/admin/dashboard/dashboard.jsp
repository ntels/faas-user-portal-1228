<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>  

<link rel="stylesheet" href="${pageContext.request.contextPath}/core/admin/lib/mCustomScrollbar/jquery.mCustomScrollbar.css">

<script type="text/javascript" src="${pageContext.request.contextPath}/core/admin/js/paging.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/core/admin/js/formSubmit.js"></script>	
<script type="text/javascript" src="${pageContext.request.contextPath}/core/admin/lib/mCustomScrollbar/jquery.mCustomScrollbar.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/adminTemplate/adminTemplate.js?ver=1.0"></script>
<script>
var getFunctionTop10Json = ${getFunctionTop10Json};
var getTriggerInfoJson = ${getTriggerInfoJson};
var lineChartRegCnt = ${lineChartRegCnt};
var lineChartRegCnt_cnt = lineChartRegCnt.length; 
var value1 = new Array();
var tot1 = 0;

var regFunctionPerMonthJson = ${regFunctionPerMonthJson};
var title2 = new Array();
var value2 = new Array();

var lineChartJson = ${lineChartJson};
var lineChartJsonCnt = lineChartJson.length;
var xDate = new Array();
var succCnt = new Array();
var failCnt = new Array();
var callCnt = new Array();

var p90 = new Array();
var p99 = new Array();
var p50 = new Array();

var pieValue = new Array();
var regXdate = new Array();
var regCnt = new Array();

//맥스값
var regCntMax = 0; //등록건수 맥스값
var cntMax = 0;  //호출건수 맥스값

var pMax = 0;

$(document).ready(function() {
	$("#contentWrap").css("display","block");
	setTopLink('javascript:dashBoardRefresh();');
	
	
	/* ['x','08.01','08.02','08.03','08.04','08.05','08.06'],
	['성공 건 수​', 30, 20, 90, 30, 55, 70],
	['오류 건 수​', 40, 50, 10, 20, 10, 20]
	 */
 	 xDate.push('x');
	 succCnt.push('성공 건 수​');
	 failCnt.push('오류 건 수​'); 
	 callCnt.push('호출 건 수');
	 
	 p99.push("p99")
	 p90.push("p90")
	 p50.push("p50")
	 
	 regXdate.push('x');
	 regCnt.push('등록 현황');
	for(var i=0;i<lineChartJson.length-1;i++) {
		xDate.push(lineChartJson[i].DATE_HMS);
		succCnt.push(lineChartJson[i].RATE);
		failCnt.push(lineChartJson[i].RATE1);
		callCnt.push(lineChartJson[i].CNT)
		
		p99.push(lineChartJson[i].P99);
		p90.push(lineChartJson[i].P90);
		p50.push(lineChartJson[i].P50);
	}
	
	var pArr = new Array();
	
	pArr.push(lineChartJson[lineChartJson.length-1].P99);
	pArr.push(lineChartJson[lineChartJson.length-1].P90);
	pArr.push(lineChartJson[lineChartJson.length-1].P50);
	
	pMax = Math.max.apply(null,pArr);  //p99,90,50맥스값.
	
	lineChartJsonMax = lineChartJson[lineChartJson.length-1].CNT;
	
	for(var i=0;i<lineChartRegCnt.length-1;i++) {
		regXdate.push(lineChartRegCnt[i].xDate)
		regCnt.push(lineChartRegCnt[i].cnt);
	}
	regCntMax = lineChartRegCnt[lineChartRegCnt.length-1].cnt;
	
	
    var httpCnt = 0;
    var cephCnt = 0;
    var mongo = 0;
    var postgres = 0;
    var timerCnt = 0;
    var mariadbCnt = 0;
    
	for(var i=0;i<getTriggerInfoJson.length;i++) {
		if(getTriggerInfoJson[i].TRIGGER_TP=='http') {
			httpCnt += getTriggerInfoJson[i].CNT;
		}
		
		if(getTriggerInfoJson[i].TRIGGER_TP=='ceph') {
			cephCnt +=getTriggerInfoJson[i].CNT;
		}
		
		if(getTriggerInfoJson[i].TRIGGER_TP=='mongodb') {
			mongo += getTriggerInfoJson[i].CNT;
		}	
		
		if(getTriggerInfoJson[i].TRIGGER_TP=='postgresql') {
			postgres += getTriggerInfoJson[i].CNT;
		}	
		
		if(getTriggerInfoJson[i].TRIGGER_TP=='timer') {
			timerCnt  +=getTriggerInfoJson[i].CNT;
		}
		
		if(getTriggerInfoJson[i].TRIGGER_TP=='mariadb') {
			mariadbCnt  +=getTriggerInfoJson[i].CNT;
		}		
		
		tot1 += getTriggerInfoJson[i].CNT;
	}
	
	$("#httpCnt").text(httpCnt);
	$("#cephCnt").text(cephCnt);
	$("#mongo").text(mongo);
	$("#postgres").text(postgres);
	$("#mariadbCnt").text(mariadbCnt);
	$("#timerCnt").text(timerCnt);
	
	
	$("#tot1").text(tot1);
	pieValue.push(httpCnt);
	pieValue.push(cephCnt);
	pieValue.push(mongo);
	pieValue.push(postgres);
	pieValue.push(mariadbCnt);
	pieValue.push(timerCnt);
	
	console.log(tot1);	
	console.log(getFunctionTop10Json)
	doughnutChart();
	
	title2.push('x');
	value2.push('value');
	for(var i=0;i<regFunctionPerMonthJson.length;i++) {
		title2.push(regFunctionPerMonthJson[i].mon);
		value2.push(regFunctionPerMonthJson[i].cnt);	
	}
	barChart1();
	linechart4();
	linechart1();
	linechart2();
	linechart3();		
	$("#tot1").text(tot1);
	
	afterRootRender();
	
	$('.dashboard-wrap .dashboard-size').on('click', function(){
		alert('dashboard!!');
		$(this).parents().find('#pageWrap').toggleClass('dashboard-full');
		$('.panel').find('.chart').each(function(){
			// resize();
		});
		return false;
	});	
	
});

function linechart1() {
	var chart1 = c3.generate({
		bindto: "#linechart1",
		data: {
			x: 'x',
			columns: [
				xDate,
				callCnt
			],
			colors: {
				'호출 건 수': '#1f77b4'
			}
		},
		axis: {
			y: {
				min: 0,
				max: lineChartJsonMax,
				padding: {top: 0, bottom: 0},
				tick: {
					count: lineChartJsonCnt,
					format: d3.format('d')
				}
			}
		},
	});
};

function linechart2() {
	var chart1 = c3.generate({
		bindto: "#linechart2",
		data: {
			x : 'x',
			columns: [
				xDate,
				succCnt,
				failCnt 
			],
			colors: {
				'성공 건 수​': '#1f77b4',
				'오류 건 수​': '#f96197'
			}
		},
		axis: {
			y: {
				min: 0,
				max: 100,
				padding: {top: 0, bottom: 0},
				tick: {
					format: d3.format('d'),
					count: lineChartJsonCnt
				}
			}
		},
	});
};
function linechart3() {
	var chart1 = c3.generate({
		bindto: "#linechart3",
		data: {
			x : 'x',
			columns: [
				xDate,
				p99,
				p90,
				p50
			],
			colors: {
				'P99​': '#4674d2',
				'P90​': '#83d0ef',
				'P50​': '#81d54e',
			}
		},
		axis: {
			y: {
				min: 0,
				max: pMax,
				padding: {top: 0, bottom: 0},
				tick: {
					format: d3.format('d'),
					count: lineChartJsonCnt
				}
			}
		},
	});
}
function linechart4() {
	var chart1 = c3.generate({
		bindto: "#linechart4",
		data: {
			x: 'x',
			columns: [
			    	regXdate,
			    	regCnt
			],
			colors: {
				'등록 현황': '#1f77b4'
			}
		},
		axis: {
			y: {
				min: 0,
				max: regCntMax,
				padding: {top: 0, bottom: 0},
				tick: {
					format: d3.format('d'),
					count: lineChartRegCnt_cnt
				}
			}
		},
	});
};

function dashBoardRefresh(){
	switchContent("${pageContext.request.contextPath}","/faas/?menu=/admin/dashboard/dashboard");
}

function afterRootRender()
{
	var obj = $("[data-toggle='tooltip']");
	if(obj.length >0) obj.tooltip();
	obj = $("#container");
	if(obj.length >0) obj.show();
	obj = $('.scroll-pane');
	if(obj.length >0) obj.jScrollPane();

	feather.replace({width: '1em', height: '1em'});

	$(".scroll-content").mCustomScrollbar();
	$(".scroll-content-x").mCustomScrollbar({
		axis:"x"
	});
	$(".scroll-content-xy").mCustomScrollbar({
		axis:"yx"
	});
}
function doughnutChart(){
	//(JSON.stringify(pieValue));
	var ctx = document.getElementById('doughnutChart').getContext('2d');
	var chart = new Chart(ctx, {
		// The type of chart we want to create
		type: 'doughnut',
		// The data for our dataset
		data : {
			    datasets: [{
			        data: pieValue ,
	                backgroundColor: [
						'#ff6384',
						'#ffce56',
						'#36a2eb',
						'#1f77b4',
						'#ca6384',
						'#00bcd4',
	                ]
			    }],
			    // These labels appear in the legend and in the tooltips when hovering different arcs
				labels: ['HTTP' , 'Ceph', 'MongoD', 'PostgreSQL','MariaDB	', '타이머']
			},

		// Configuration options go here
		options: {
			maintainAspectRatio: false,
			cutoutPercentage: 75,
			legend: {
				display: false,
				labels: {
					usePointStyle: true,
					padding: 30,
					fontColor: "#000",
					fontSize: 12,
					boxWidth: 15
				},
				position: "right"
			},
		}
	});
};

function barChart1() {
	var chart2 = c3.generate({
		bindto: '#barChart1',
		data: {
			x : 'x',
			columns: [
				/* ['x', '2020.01​', '2020.02​', '2020.03​', '2020.04​', '2020.05​', '2020.06'],
				['value', 30, 90, 20, 40, 15, 70] */
				title2,
				value2
			],
			type: 'bar',
			colors: {
				'title': '#1f77b4',
			}
		},
		legend: {
			show: false
		},
		bar: {
			width: {
				ratio: 0.2
			},
			space: 0.25,
		},
		transition: {
			duration: 0
		},
		axis: {
			x: {
				type: 'category',
				padding: {top: 0, bottom: 0},
			},
			y: {
				min: 0,
				max: 100,
				padding: {top: 0, bottom: 0},
				tick: {
					count: 2
				}
			}
		},
		grid: {
			x: {
				show: true
			},
			y: {
				show: true
			}
		}
	});
};


function goFunctionTop10List() {
	//contentWrap
	$("#contentWrap").hide();
	$("#contentWrap").load("/faas/admin/dashboard/list",{action: "dashboard/listAction"});
}

function goRecentFunctionList() {
	//contentWrap
	$("#contentWrap").hide();
	$("#contentWrap").load("admin/function/list",{action:"admin/function/listAction",orderBy:"reg_date_hms DESC"});
}
function goProject(namespace){
	$("#contentWrap").hide();
	$("#contentWrap").load("admin/function/list",{"action": "admin/function/listAction","namespace":namespace});
}

/* function goFunctionMain(function_id) {
	//switchContent("faas","/faas/?menu=/admin/dashboard/main?action=admin/dashboard/mainStructureAction&function_id="+function_id);
	$("#contentWrap").hide();
	$("#contentWrap").load("admin/dashboard/main",{"action": "admin/dashboard/mainStructureAction","function_id":function_id});
} */

function goTemplateList(){
	switchContent("${pageContext.request.contextPath}","/faas/?menu=/admin/template/list");
}
function goRefresh(){
	switchContent("${pageContext.request.contextPath}","/faas/?menu=/admin/dashboard/dashboard");
}
</script>

<main id="contentWrap" class="dashboard-wrap">
				<div class="page-title dashboard-top">
					<div class="page-info">
						프로젝트명
					</div>
					<div class="page-desc">
						<span class="date">
							Today is Tuesday, 8 February 2018
						</span>
					</div>
					<div class="dashboard-control-area">
						<!-- <a href="" class="dashboard-size btn btn-nostyle btn-xs"></a> -->
						<a href="javascript:goRefresh();" class="dashboard-refresh btn btn-nostyle btn-xs">
							<i class="xi-renew"></i>
						</a>
					</div>
				</div>

				<!-- inner -->
				<div class="page-content">

					<div class="inner-wrap">

						<!-- 콘텐츠가 들어갑니다 -->
						<div class="row">
							<div class="col-md-3">
							<c:if test="${!empty getFunctionTop10List}">
									<div class="panel panel-bordered">
										<div class="panel-heading dashboard-t1">
											<h3 class="panel-title">
												<div class="row">
													<div class="col-md-8">
														<h3 class="heading">
															함수 등록 Top 10​
														</h3>
													</div>
													<div class="col-md-4">
														<div class="text-right">
															<a href="#" onclick='javascript:switchContent("/faas", "/faas/?menu=/layout/admin/function/list.do");' class="more">전체 보기</a>
														</div>
													</div>
												</div>
											</h3>
										</div>
										<div class="panel-body padding-right-10" style="height: 330px">
											<div class="all_view_list vertical scroll-content">
												<ol>
													<c:forEach items="${getFunctionTop10List}" var="resultModel" varStatus="status">
														<li>
															<span class="name"><a href="javascript:goProject('${resultModel.namespace}');" class="ellipsis-2">${resultModel.namespace}</a></span>
															<span class="count padding-right-15">${resultModel.cnt}건​</span>
														</li>
													</c:forEach>
												</ol>
											</div>
										</div>
									</div>
							</c:if>
							<c:if test="${empty getFunctionTop10List}">
								<div class="panel panel-bordered">
									<div class="panel-heading">
										<h3 class="panel-title">
											<div class="row">
												<div class="col-md-8">
													<h3 class="heading">
													최근 등록 함수​
													</h3>
												</div>
												<div class="col-md-4">
													<div class="text-right">
														<a href="#" onclick='javascript:switchContent("/faas", "/faas/?menu=/layout/admin/function/list.do");' class="more">전체 보기</a>
													</div>
												</div>
											</div>
										</h3>
									</div>
									<div class="panel-body" style="height: 330px;">
										<div class="nodata-box type1">
											<div class="nodata">
												<div class="img-div"></div>
												<p>현재 등록된 함수가 없습니다.​</p>
											</div>
										</div>
									</div>
								</div>								
							</c:if>
							</div>
							
							<div class="col-md-3">
								<c:if test="${!empty recentFunctionList}">
									<div class="panel panel-bordered">
										<div class="panel-heading dashboard-t1">
											<h3 class="panel-title">
												<div class="row">
													<div class="col-md-8">
														<h3 class="heading">
														최근 등록 함수​
														</h3>
													</div>
													<div class="col-md-4">
														<div class="text-right">
															<a href="#" onclick='javascript:switchContent("/faas", "/faas/?menu=/layout/admin/function/list.do");' class="more">전체 보기</a>
														</div>
													</div>
												</div>
											</h3>
										</div>
										<div class="panel-body padding-right-10" style="height: 330px">
											<div class="scroll-content" style="height: 280px">
												<ul class="list-dot">
													<c:forEach items="${recentFunctionList}" var="resultModel" varStatus="status">
														<li>
															<b>[${resultModel.namespace}]</b>
															<a href="#"><p class="ellipsis-2">${resultModel.dsp_name}</p></a>
														</li>
													</c:forEach>
												</ul>
											</div>
										</div>
									</div>
									</c:if>
									<c:if test="${empty recentFunctionList}">
										<div class="panel panel-bordered">
										<div class="panel-heading dashboard-t1">
											<h3 class="panel-title">
												<div class="row">
													<div class="col-md-8">
														<h3 class="heading">
														최근 등록 함수​
														</h3>
													</div>
													<div class="col-md-4">
														<div class="text-right">
															<a href="#" onclick='javascript:switchContent("/faas", "/faas/?menu=/layout/admin/function/list.do");' class="more">전체 보기</a>
														</div>
													</div>
												</div>
											</h3>
										</div>
										<div class="panel-body" style="height: 330px;">
											<div class="nodata-box type1">
												<div class="nodata">
													<div class="img-div"></div>
													<p>현재 등록된 함수가 없습니다.​</p>
												</div>
											</div>
										</div>
									</div>								
								 </c:if>
							</div>
							<div class="col-md-3">
								<c:if test="${!empty runtimeFunctionList}">
								<div class="panel panel-bordered">
									<div class="panel-heading dashboard-t1">
										<h3 class="panel-title">
											<div class="row">
												<div class="col-md-8">
													<h3 class="heading">
													런타임별 함수 등록 현황​
													</h3>
												</div>
												<div class="col-md-4">
													<div class="text-right">
														<a href="#" onclick='javascript:switchContent("/faas", "/faas/?menu=/layout/admin/function/list.do");' class="more">전체 보기</a>
													</div>
												</div>
											</div>
										</h3>
									</div>
									<div class="panel-body padding-right-10" style="height: 330px">
										<ul class="list-view-ranking type1">
										<c:forEach items="${runtimeFunctionList}" var="resultModel" varStatus="status">
											<li>
												<div class="clearfix">
													<c:if test="${resultModel.runtime eq 'atot'}">
														<div class="img-zone img-func-all img-size50 margin-top-10">
															<span class="sr-only">전체</span>
														</div>
													</c:if>
													<c:if test="${resultModel.runtime eq 'nodejs'}">
														<div class="img-zone img-nodejs img-size50 margin-top-10">
															<span class="sr-only">Node</span>
														</div>
													</c:if>	
													<c:if test="${resultModel.runtime eq 'python'}">
														<div class="img-zone img-python img-size50 margin-top-10">
															<span class="sr-only">Python</span>
														</div>
													</c:if>																																
													<div class="text-box">
														<span class="title">
															${resultModel.runtime eq 'atot' ? '전체' : resultModel.runtime}​
														</span>
														<span class="num">
															${resultModel.cnt}​
														</span>
													</div>
												</div>
											</li>
											</c:forEach>
										</ul>
									</div>
								</div>
								</c:if>
								<c:if test="${empty runtimeFunctionList}">
								<div class="panel panel-bordered">
									<div class="panel-heading dashboard-t1">
										<h3 class="panel-title">
											<div class="row">
												<div class="col-md-8">
													<h3 class="heading">
													런타임별 함수 등록 현황​
													</h3>
												</div>
												<div class="col-md-4">
													<div class="text-right">
														<a href="#" onclick='javascript:switchContent("/faas", "/faas/?menu=/layout/admin/function/list.do");' class="more">전체 보기</a>
													</div>
												</div>
											</div>
										</h3>
									</div>
									<div class="panel-body">
										<ul class="list-view-ranking type1">
											<li>
												<div class="clearfix">
													<div class="img-zone img-func-all img-size50 margin-top-10">
														<span class="sr-only">전체</span>
													</div>
													<div class="text-box">
														<span class="title">
															전체​
														</span>
														<span class="num">
															0
														</span>
													</div>
												</div>
											</li>
											<li>
												<div class="clearfix">
													<div class="img-zone img-nodejs img-size50 margin-top-10">
														<span class="sr-only">Node</span>
													</div>
													<div class="text-box">
														<span class="title">
															Node.js​
														</span>
														<span class="num">
															0
														</span>
													</div>
												</div>
											</li>
											<li>
												<div class="clearfix">
													<div class="img-zone img-python img-size50 margin-top-10">
														<span class="sr-only">Python​</span>
													</div>
													<div class="text-box">
														<span class="title">
															Python​
														</span>
														<span class="num">
															0
														</span>
													</div>
												</div>
											</li>
										</ul>
									</div>
								</div>								
								</c:if>
							</div>
							<div class="col-md-3">
							<c:if test="${!empty getTemplateList}">
								<div class="panel panel-bordered">
									<div class="panel-heading dashboard-t1">
										<h3 class="panel-title">
											<div class="row">
												<div class="col-md-8">
													<h3 class="heading">
													함수 템플릿 등록 현황​
													</h3>
												</div>
												<div class="col-md-4">
													<div class="text-right">
														<a href="javascript:goTemplateList();" class="more">전체 보기</a>
													</div>
												</div>
											</div>
										</h3>
									</div>
									<div class="panel-body">
										<div style="margin-top: -1px;">
										<div class="scroll-content" style="height: 280px">
											<ul class="list-dot type1">
											<c:forEach items="${getTemplateList}" var="resultModel" varStatus="status">
												<li>
													<a href="javascript:goDetail('${resultModel.template_id}');">${resultModel.name}</a>
												</li>
											</c:forEach>
											</ul>
											</div>
										</div>
									</div>
								</div>
								</c:if>
								<c:if test="${empty getTemplateList}">
								<div class="panel panel-bordered">
									<div class="panel-heading">
										<h3 class="panel-title">
											<div class="row">
												<div class="col-md-8">
													<h3 class="heading">
													함수 템플릿 등록 현황​
													</h3>
												</div>
												<div class="col-md-4">
													<div class="text-right">
														<a href="SC_FS_33_001.html" class="more">전체 보기</a>
													</div>
												</div>
											</div>
										</h3>
									</div>
									<div class="panel-body" style="height: 330px;">
										<div class="nodata-box type1">
											<div class="nodata">
												<div class="img-div"></div>
												<p>현재 등록된 템플릿이 없습니다.​​</p>
												<button class="btn btn-default" onclick="location.href='SC_FS_33_002.html'">
													<div class="feather-icon">
														<i data-feather="plus-square"></i>
													</div>
													함수 템플릿 등록​
												</button>
											</div>
										</div>
									</div>
								</div>
								</c:if>
							</div>
						</div>
						
						
						
						<!-- nodata -->
						<div class="row">
							<div class="col-md-3">
								
							</div>
							<div class="col-md-3">
								
							</div>
						</div>

						<div class="row">
							<div class="col-md-6">
								<div class="panel panel-bordered dashboard-panel dashboard-panel-type1" style="height:355px">
									<div class="panel-body">
										<h3 class="heading h3 margin-bottom-25">
											트리거 등록 현황
										</h3>
										<div class="row">
											<div class="col-md-7">
												<div class="chart-area margin-top-40 padding-left-60" style="height: 180px;">
													<canvas id="doughnutChart"></canvas>
												</div>
											</div>
											<div class="col-md-5">
												<ul class="chart-label-list left type1 margin-top-40">
													<li class="clearfix">
														<div class="category pull-left">
															<span class="bul-round" style="border:2px solid #e7e9ed;"></span>
															<span class="text">전체</span>
														</div>
														<div class="pull-right count" id="tot1">
															0
														</div>
													</li>
													<li class="clearfix">
														<div class="category pull-left">
															<span class="bul-round" style="background: #ff6384;"></span>
															<span class="text">HTTP​</span>
														</div>
														<div class="pull-right count" id="httpCnt">
															0
														</div>
													</li>
													<li class="clearfix">
														<div class="category pull-left">
															<span class="bul-round" style="background: #ffce56;"></span>
															<span class="text">Ceph​</span>
														</div>
														<div class="pull-right count" id="cephCnt">
															0
														</div>
													</li>
													<li class="clearfix">
														<div class="category pull-left">
															<span class="bul-round" style="background: #36a2eb;"></span>
															<span class="text">MongoDB</span>
														</div>
														<div class="pull-right count"  id="mongo">
															0
														</div>
													</li>
													<li class="clearfix">
														<div class="category pull-left">
															<span class="bul-round" style="background: #1f77b4;"></span>
															<span class="text">PostgreSQL​</span>
														</div>
														<div class="pull-right count" id="postgresCnt">
															0
														</div>
													</li>
													<li class="clearfix">
														<div class="category pull-left">
															<span class="bul-round" style="background: #ca6384;"></span>
															<span class="text">MariaDB​</span>
														</div>
														<div class="pull-right count" id="mariadbCnt">
															0
														</div>
													</li>													
													<li class="clearfix">
														<div class="category pull-left">
															<span class="bul-round" style="background: #00bcd4;"></span>
															<span class="text">타이머​</span>
														</div>
														<div class="pull-right count" id="timerCnt">
															0
														</div>
													</li>
												</ul>
												
											</div>
										</div>
									</div>
								</div>
							</div>
							<div class="col-md-6">
								<div class="panel panel-bordered dashboard-panel dashboard-panel-type1" style="height:355px">
									<div class="panel-body">
										<h3 class="heading h3 margin-bottom-25">
											월별 함수 등록 현황​
										</h3>
										<div id="barChart1" class="chart" style="height: 250px;"></div>
									</div>
								</div>
							</div>
						</div>
						
						<!-- nodata -->
<!-- 						<div class="row">
							<div class="col-md-6">
								
							</div>
							<div class="col-md-6">
								<div class="panel panel-bordered dashboard-panel dashboard-panel-type1" style="height:355px">
									<div class="panel-body">
										<h3 class="heading h3 margin-bottom-25">
											월별 함수 등록 현황​
										</h3>
										<div class="nodata-box type1">
											<div class="nodata">
												<div class="img-div"></div>
												<p>현재 등록된 함수가 없습니다.​</p>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div> -->

						<section class="panel panel-bordered">
							<div class="panel-heading">
								<h3 class="panel-title">
									<div class="row">
										<div class="col-sm-6">
											<h3 class="heading">모니터링​</h3>
										</div>
										<div class="col-sm-6 text-right">
											<a href="#" target="_blank">
												<i class="img-grafana small"></i>
											</a>
											<a href="#" style="vertical-align: middle;margin: 0 0 0 10px;display: inline-block;">
												<i class="xi-renew"></i>
											</a>
										</div>
									</div><!-- // panel-heading -->
								</h3>
							</div>
							<div class="panel-body">
								
								<div class="row">
									<div class="col-md-3">
										<h4 class="heading h4">
											등록 현황
										</h4>
										<div id="linechart4" class="chart" style="height: 300px"></div>	
									</div>
									
									<div class="col-md-3">
										<h4 class="heading h4">
											호출 건 수
										</h4>
										<div id="linechart1" class="chart" style="height: 300px"></div>	
									</div>
									
									<div class="col-md-3">
										<h4 class="heading h4">
											성공률
										</h4>
										<div id="linechart2" class="chart" style="height: 300px"></div>
									</div>
		
									<div class="col-md-3">
										<h4 class="heading h4">
											지속 시간 (Duration)​
										</h4>
										<div id="linechart3" class="chart" style="height: 300px"></div>	
									</div>
								</div>
								
							</div> <!-- // panel-body -->
						</section>
						
						<!-- nodata -->
<!-- 						<section class="panel panel-bordered">
							<div class="panel-heading">
								<h3 class="panel-title">
									<div class="row">
										<div class="col-sm-6">
											<h3 class="heading">모니터링​</h3>
										</div>
									</div> 
								</h3>
							</div>// panel-heading
							<div class="panel-body">
								<div class="nodata-box type1">
									<div class="nodata">
										<div class="img-div"></div>
										<p>현재 등록된 함수가 없습니다.​<br>
										등록된 함수가 있을 경우, 전체 등록 현황은 물론  <span class="blue bold">호출 건 수, 성공률, 지속 시간 (Duration)</span> 등 설정한 구간별로 모니터링할 수 있습니다.​</p>
									</div>
								</div>
							</div> // panel-body
						</section> -->
					</div>
				</div>
				<!-- // inner -->

	</main>
	
	
	