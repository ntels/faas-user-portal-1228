<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>  

<script type="text/javascript" src="${pageContext.request.contextPath}/core/admin/js/paging.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/core/admin/js/formSubmit.js"></script>	
<script type="text/javascript" src="${pageContext.request.contextPath}/adminTemplate/adminTemplate.js?ver=1.0"></script>


<script>
var editor1;
var editor2;
	$(document).ready(function() {
		$("#contentWrap").css("display","block");
		goSearch("template/listAction");
		setTopLink('javascript:goList();');
	});

	function goSearch(url){
		var param = $("#myForm").serialize();
		formData = param;
		$(".loading-wrap").show();
		$.ajax({
	        url : url,
	        data : param,
	        type : 'POST',
	        success : function(data) {
	           $("#dataTable").html(data);
	        },
	        error: function(request, status, error){	        	
	        	alert("code : "+ request.status + "\r\nmessage : "+request.responseText + "\r\error : "+error);
	        },
	        complete : function() {
	        	$(".loading-wrap").hide();
	        }
		});
	}	
	
	
    
</script>
<form name="myForm" id="myForm">
	<main id="contentWrap" class="dashboard-wrap">
				<div class="page-title">
					<div id="spot">
						<div class="page-info">
							함수 템플릿 관리
						</div>
						<div class="page-location">
							<span class="home">
								<i class="xi-home-o"></i>
								HUBPoP
							</span>
							<span>함수 템플릿 관리</span>
						</div>
						<a href="javascript:goList();" class="btn-list-all" title="함수 템플릿 관리 목록">
							<i class="xi-bars"></i>
							<p class="sr-only">함수 템플릿 관리 목록</p>
						</a>						
						<a href="" class="btn-refresh">
							<i class="xi-renew"></i>
							<p class="sr-only">새로고침</p>
						</a>
					</div>
					
				</div>

				<!-- inner -->
				<div class="page-content">
					<div class="inner-wrap">
						<!-- 콘텐츠가 들어갑니다 -->
						<a class="btn btn-lg btn-primary" href="javascript:goAdd()">
							<div class="feather-icon">
								<i data-feather="command"></i>
							</div>
							함수 템플릿 등록​
						</a>
						
						<div class="card-input-wrap">
							<div class="row">
								<div class="col-md-6">
									<div class="font-size-18 padding-top-5">
										<b>함수 템플릿 <span class="color-primary">(0)</span></b>
									</div>
								</div>
								<div class="col-md-6">
									<div class="text-right">
										<div class="form-group inline-block margin-bottom-0">
											<select name="runtimeVersion" id="runtimeVersion" class="form-control">
												<option value>런타임 유형</option>
												<option value="nodejs 12">Node.js 12</option>
												<option value="nodejs 10">Node.js 10</option>
												<option value="python 3.8">Python3.8</option>
												<option value="python 3.7">Python3.7</option>
												<option value="python 3.6">Python3.6</option>												
											</select>
										</div>
										<div class="form-group inline-block margin-bottom-0">
											<select name="trigger_tp" id="trigger_tp" class="form-control">
												<option value>트리거 유형</option>
												<option value="http">HTTP</option>
												<option value="storage">Ceph</option>
												<option value="mongodb">MongoDB</option>
												<option value="postgre">Postgre</option>
												<option value="timer">SQL 타이머</option>
											</select>
										</div>
<!-- 										<div class="form-group inline-block margin-bottom-0">
											<select name="" id="" class="form-control">
												<option value="">백엔드 서비스 유형</option>
												<option value="">Ceph</option>
												<option value="">MongoDB</option>
												<option value="">PostgreSQL</option>
											</select>
										</div> -->
										<div class="form-group inline-block margin-bottom-0">
											<select name="orderBy" id="orderBy" class="form-control">
												<option value="reg_date_hms">등록일</option>
												<option value="mod_date_hms">수정일</option>
												<option value="name">템플릿명</option>
											</select>
										</div>
									</div>
								</div>
							</div>
						</div>
						
						<div id="dataTable">
						</div>
						
					</div>
				</div>
				
			<div class="loading-wrap" style="display:none;">
			<div class="loading-box">
				<div class="img">
					<img src="${pageContext.request.contextPath}/core/img/common/loading.png" alt="">
				</div>
				<ul class="dot-pulse">
					<li> <div class="dot"></div> </li>
					<li> <div class="dot"></div> </li>
					<li> <div class="dot"></div> </li>
					<li> <div class="dot"></div> </li>
					<li> <div class="dot"></div> </li>
				</ul>
			</div>
		</div>					
				
			</main>
</form>
