<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>  	

<script type="text/javascript">
	var condition = ${condition};
    $(document).ready(function() {
    	
    });
</script>  		
<div class="bg-border-box trigger-ele" data-tab="HTTP">
	<div class="row">
		<div class="col-md-8">
			<div class="color-primary">
				<b>
					HTTP 트리거
				</b>
			</div>
		</div>
		<div class="col-md-4 text-right">
			<button class="close" data-btn="delete">
				<i class="xi-close"></i>
			</button>
		</div>
	</div>
	<div class="row text-type">
		<div class="col-sm-2">
			<div class="write-sub-title margin-top-10">
				<b class="caution-star">*</b> API 이름
			</div>
		</div>
		<div class="col-sm-10">
			<ul class="list-inline" data-sel="httpSel${paramMap.httpCnt}">
			    <input type="hidden" name="httpIndex" id="httpIndex" value="${paramMap.httpCnt}"/>
				<li>
					<div class="radio-group padding-top-10">
						<input type="radio" name="radioHttp${paramMap.httpCnt}" id="huser${paramMap.httpCnt}" class="radio-input" value="user" checked="">
						<label for="huser${paramMap.httpCnt}" class="radio-item">
							<div class="radio">
								<div class="checked-icon"></div>
							</div>
							<span class="text">
								사용자가 직접 입력
							</span>
						</label>
					</div>
				</li>
				<li>
					<div class="radio-group padding-top-10" style="width: 200px;">
						<input type="radio" name="radioHttp${paramMap.httpCnt}" id="hadmin${paramMap.httpCnt}" class="radio-input" value="admin">
						<label for="hadmin${paramMap.httpCnt}" class="radio-item">
							<div class="radio">
								<div class="checked-icon"></div>
							</div>
							<span class="text">
								관리자 지정
							</span>
							<span class="info-icon-tooltip" data-toggle="tooltip" title="" data-placement="top" data-original-title="관리자가 항목을 지정할 경우, 사용자는 설정을 변경할 수 없습니다.">
								i
							</span>
						</label>
					</div>
				</li>
				<li style="width: calc(100% - 368px); display: none;" data-lst="httpAdmin">
					<div class="form-group margin-bottom-0">
						<input type="text" name="httpApi${paramMap.httpCnt}" id="httpApi${paramMap.httpCnt}" class="form-control" placeholder="API 이름 입력 (예 : api_name)"
																							onKeyup="this.value=this.value.replace(/[^a-zA-Z0-9-]/g,'');">
					</div>
				</li>
			</ul>
		</div>
	</div>
</div><!-- //HTTP 트리거 -->
			
			
			