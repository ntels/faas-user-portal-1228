<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>


<script>
	$("#contentWrap").css("display","block");
	var condition = ${condition};
	
	$(document).ready(function() {
		goSearch("template/"+condition.action);
	});
	
	function goSearch(url){
		//var param = $("#myForm").serialize();
		var param = condition;
		formData = param;
		$(".loading-wrap").show();
		$.ajax({
	        url : url,
	        data : param,
	        type : 'POST',
	        success : function(data) {
	           $(".page-content").html(data);
	           //console.log(data);
	        },
	        error: function(request, status, error){	        	
	        	alert("code : "+ request.status + "\r\nmessage : "+request.responseText + "\r\error : "+error);
	        },
	        complete : function() {
	        	$(".loading-wrap").hide();
	        }
		});
	}
</script>
		<div class="page-title">
			<div id="spot">
				<div class="page-info">
					함수 템플릿 관리
				</div>
				<div class="page-location">
					<span class="home">
						<i class="xi-home-o"></i>
						HUBPoP
					</span>
					<span>함수 템플릿 관리</span>
				</div>
				<a href="javascript:goList();" class="btn-list-all" title="함수 템플릿 관리 목록">
					<i class="xi-bars"></i>
					<p class="sr-only">함수 템플릿 관리 목록</p>
				</a>				
				<a href="" class="btn-refresh">
					<i class="xi-renew"></i>
					<p class="sr-only">새로고침</p>
				</a>
			</div>
		</div>
		<div class="page-content" id="dataTable">

		<div class="loading-wrap" style="display:none;">
			<div class="loading-box">
				<div class="img">
					<img src="${pageContext.request.contextPath}/core/img/common/loading.png" alt="">
				</div>
				<ul class="dot-pulse">
					<li> <div class="dot"></div> </li>
					<li> <div class="dot"></div> </li>
					<li> <div class="dot"></div> </li>
					<li> <div class="dot"></div> </li>
					<li> <div class="dot"></div> </li>
				</ul>
			</div>
		</div>		
		</div>
