<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>  

<script>
var condition = ${condition};
//console.log(condition.templateVO.git_url);
$("#git_url").val(condition.templateVO.git_url);

var editor1 = '';
var editor2 = '';

$(document).ready(function() {
	$("#template_id").val(condition.template_id);
	setTopLink('javascript:goModify();');
	var result = ${sourceList};
	editor1 = setEditor("editor1",result[0],false);
	editor2 = setEditor("editor2",result[1],false);
	//$('[data-lst="admin"]').hide();   	//'각 트리거 > 관리자 지정' 선택시 show
	feather.replace();
	deleteTrigger();
	$("#li_nodejs").bind("click", function(){
		goNodeJs();
	});
	$("#li_python").bind("click", function(){
		goPython();
	});	
	
	if(condition.templateVO.runtime=='nodejs'){
		$("#node").trigger("click");
		if(condition.templateVO.version =='12') $("#nodejsVersion > li:nth-child(1) > label").trigger("click")
		if(condition.templateVO.version =='10') $("#nodejsVersion > li:nth-child(2) > label").trigger("click")
		if(condition.templateVO.version =='8.10') $("#nodejsVersion > li:nth-child(3) > label").trigger("click")
	}else{
		$("#python").trigger("click");
	}
	if(condition.httpTriggerList != '') $("#httpCnt").text(condition.httpTriggerList.length);
	if(condition.cephTriggerList != '') $("#cephCnt").text(condition.cephTriggerList.length);
	if(condition.mongoTriggerList != '') $("#mongoCnt").text(condition.mongoTriggerList.length);
	if(condition.postgreTriggerList != '') $("#postgreCnt").text(condition.postgreTriggerList.length);
	if(condition.postgreTriggerList != '') $("#timerCnt").text(condition.postgreTriggerList.length);
	$("#timerCnt").text(condition.timerCnt);
});

function goUpdate() {
    $("#fileContent1").val(editor1.getValue());
    $("#fileContent2").val(editor2.getValue());

	var param = $("#myForm").serialize();
	param = queryStringToJSON(param);
	param = JSON.stringify(param),
//	console.log(param);
	formData = param;
	$.ajax({
        url : "template/update",
        data : param,
        type : 'POST',
        dataType : 'json',
        contentType:"application/json;charset=utf-8",
        traditional : true,
        success : function(data) {
        	 $("#contentWrap").load("template/main",{action: "modifyAction","template_id":data.templateVO.template_id});
        },
        error: function(request, status, error){	        	
        	alert("code : "+ request.status + "\r\nmessage : "+request.responseText + "\r\error : "+error);
        },
        complete : function() {
        	//alert('comple');
        }
	});	
}

</script>
<input type="hidden" name="template_id" id="template_id"/>
<input type="hidden" name="fileContent1" id="fileContent1"/>
<input type="hidden" name="fileContent2" id="fileContent2"/>
<input type="hidden" name="git_url" id="git_url"/>


<span id="httpCnt" name="httpCnt" style="display:none"></span>
<span id="cephCnt" name="cephCnt" style="display:none"></span>
<span id="mongoCnt" name="mongoCnt" style="display:none"></span>
<span id="postgreCnt" name="postgreCnt" style="display:none"></span>
<span id="timerCnt" name="timerCnt" style="display:none"></span>

<div class="inner-wrap">
	<!-- 콘텐츠가 들어갑니다 -->
	<section class="panel panel-bordered">
		<div class="panel-heading">    
			<h3 class="panel-title">
				함수 템플릿 등록
			</h3>
		</div> <!-- // panel-heading -->
		<div class="panel-body">
			<div class="text-right">
				<span class="color-danger">
					<b class="caution-star">*</b> 는 필수입력 사항입니다
				</span>
			</div>
			
			<div class="write-sub-title">
				<b class="caution-star">*</b> 함수 템플릿 이름
				<span class="info-icon-tooltip" data-toggle="tooltip" title="" data-placement="right" data-original-title="공백 없이 영문 대소문자, 숫자, 하이픈(-), 언더바(_)만 입력해 주세요.">
					i
				</span>
			</div>
			<div class="row">
				<div class="col-sm-9">
					<div class="form-group margin-bottom-0">
						<input type="text" class="form-control" id="name" name="name" value="${paramMap.templateVO.dsp_name}" placeholder="함수 템플릿 이름을 입력해 주세요.">
					</div>
				</div>
				<div class="col-sm-3">
					<div class="checkbox-group margin-top-5">
						<input type="checkbox" id="top_yn" name="top_yn" class="checkbox-input" value="Y" ${paramMap.templateVO.top_yn eq 'Y' ? 'checked' : ''}>
						<label for="top_yn" class="checkbox-item">
							<div class="checkbox">
								<div class="checked-icon">
									<i class="xi-check"></i>
								</div>
							</div>
							<span class="text">상단 노출
								<span class="info-icon-tooltip" data-toggle="tooltip" title="" data-placement="right" data-original-title="체크 시, 사용자 포털 함수 템플릿 목록의 상단에 노출됩니다.">
									i
								</span>
							</span>
						</label>
					</div>
				</div>
			</div>
			<div class="write-sub-title">
				함수 템플릿 설명
				<span class="info-icon-tooltip" data-toggle="tooltip" title="" data-placement="right" data-original-title="함수 템플릿 에서 제공하는 기능을 설명해 주세요.">
					i
				</span>
			</div>
			<div class="form-group">
				<textarea name="des" id="des" class="form-control" value="${paramMap.templateVO.des}" placeholder="함수 템플릿 에서 제공하는 기능을 설명해 주세요." rows="4">${paramMap.templateVO.des}</textarea>
			</div>
		
			<div class="write-sub-title">
				트리거
				<span class="info-icon-tooltip" data-toggle="tooltip" title="" data-placement="right" data-original-title="이 함수 템플릿을 이용하여 함수를 생성할 때 반드시 입력해야 할 트리거가 있다면 모두 추가해 주세요.">
					i
				</span>
			</div>
			<div class="row">	
				<div class="col-sm-2">
					<label for="HTTP" class="input-card-type">
						<input type="radio" id="HTTP" name="triggeradd" class="card-input-element">
						<div class="card-input" onClick="javascript:addTrigger('httpTrigger');">
							<div class="card-type4 type-trigger padding-0">
								<div class="img-size50 feather-26 color-primary padding-top-10">
									<i data-feather="globe"></i>
								</div>
								<div class="text-area">
									<div class="title">HTTP 추가</div>
								</div>
							</div>
						</div>
					</label>
				</div>
				
				<div class="col-sm-2">
					<label for="Ceph" class="input-card-type">
						<input type="radio" id="Ceph" name="triggeradd" class="card-input-element">
						<div class="card-input" onClick="javascript:addTrigger('cephTrigger');">
							<div class="card-type4 type-trigger padding-0">
								<div class="img-size50 feather-26 padding-top-10">
									<i class="img-size26 img-ceph"></i>
								</div>
								<div class="text-area">
									<div class="title">Ceph 추가</div>
								</div>
							</div>
						</div>
					</label>
				</div>
				
				<div class="col-sm-2">
					<label for="MongoDB" class="input-card-type">
						<input type="radio" id="MongoDB" name="triggeradd" class="card-input-element">
						<div class="card-input" onClick="javascript:addTrigger('mongoTrigger');">
							<div class="card-type4 type-trigger padding-0">
								<div class="img-size50 feather-26 color-primary padding-top-10">
									<i data-feather="database"></i>
								</div>
								<div class="text-area">
									<div class="title">MongoDB 추가</div>
								</div>
							</div>
						</div>
					</label>
				</div>
				<div class="col-sm-2">
					<label for="PostgreSQL" class="input-card-type">
						<input type="radio" id="PostgreSQL" name="triggeradd" class="card-input-element">
						<div class="card-input" onClick="javascript:addTrigger('postgreTrigger');">
							<div class="card-type4 type-trigger padding-0">
								<div class="img-size50 feather-26 padding-top-10">
									<i class="img-size26 img-postgresql"></i>
								</div>
								<div class="text-area">
									<div class="title">PostgreSQL 추가</div>
								</div>
							</div>
						</div>
					</label>
				</div>
				<div class="col-sm-2">
					<label for="Timer" class="input-card-type">
						<input type="radio" id="Timer" name="triggeradd" class="card-input-element">
						<div class="card-input" onClick="javascript:addTrigger('timerTrigger');">
							<div class="card-type4 type-trigger padding-0">
								<div class="img-size50 feather-26 color-primary padding-top-10">
										<i data-feather="clock"></i>
								</div>
								<div class="text-area">
									<div class="title">타이머 추가</div>
								</div>
							</div>
						</div>
					</label>
				</div>
			</div>
			<!-- HTTP 트리거 -->
			<div id="httpArea">
				<c:if test="${!empty paramMap.httpTriggerList}">
					<c:forEach items="${paramMap.httpTriggerList}" var="resultModel" varStatus="status">
						<div class="bg-border-box trigger-ele" data-tab="HTTP">
							<div class="row">
								<div class="col-md-8">
									<div class="color-primary">
										<b>
											HTTP 트리거
										</b>
									</div>
								</div>
								<div class="col-md-4 text-right">
									<button class="close" data-btn="delete">
										<i class="xi-close"></i>
									</button>
								</div>
							</div>
							<div class="row text-type">
								<div class="col-sm-2">
									<div class="write-sub-title margin-top-10">
										<b class="caution-star">*</b> API 이름
									</div>
								</div>
								<div class="col-sm-10">
									<ul class="list-inline" data-sel="httpSel${status.index}">
									<input type="hidden" name="httpIndex" id="httpIndex" value="${status.index}"/>
										<li>
											<div class="radio-group padding-top-10">
												<input type="radio" name="radioHttp${status.index}" id="huser${status.index}" class="radio-input" value="user"
																													${resultModel.adm_set_yn eq 'Y' ? '' : 'checked'}>
												<label for="huser${status.index}" class="radio-item">
													<div class="radio">
														<div class="checked-icon"></div>
													</div>
													<span class="text">
														사용자가 직접 입력
													</span>
												</label>
											</div>
										</li>
										<li>
											<div class="radio-group padding-top-10" style="width: 200px;">
												<input type="radio" name="radioHttp${status.index}" id="hadmin${status.index}" class="radio-input" value="admin"
																													${resultModel.adm_set_yn eq 'Y' ? 'checked' : ''}>
												<label for="hadmin${status.index}" class="radio-item">
													<div class="radio">
														<div class="checked-icon"></div>
													</div>
													<span class="text">
														관리자 지정​
													</span>
													<span class="info-icon-tooltip" data-toggle="tooltip" title="관리자가 항목을 지정할 경우, 사용자는 설정을 변경할 수 없습니다.​" data-placement="top">
														i
													</span>
												</label>
											</div>
										</li>
										<li style="width: calc(100% - 368px);${resultModel.adm_set_yn eq 'Y' ? '' : 'display:none;'}" data-lst="admin">
											<div class="form-group margin-bottom-0">
												<input type="text" name="httpApi${status.index}" id="httpApi${status.index}" value="${resultModel.api_name}"
															class="form-control" placeholder="API 이름 입력 (예 : api_name)​">
											</div>
										</li>
									</ul>
								</div>
							</div>
						</div>		
					</c:forEach>
				 </c:if>
			</div>
			<!-- //HTTP 트리거 -->	
			
			<!-- Ceph 트리거 -->
			<div id="cephArea">
				<c:if test="${!empty paramMap.cephTriggerList}">
					<c:forEach items="${paramMap.cephTriggerList}" var="resultModel" varStatus="status">
						<div class="bg-border-box trigger-ele">
							<div class="row">
								<div class="col-md-8">
									<div class="color-primary">
										<b>
											Ceph 트리거
										</b>
									</div>
								</div>
								<div class="col-md-4 text-right">
									<button class="close" data-btn="delete">
										<i class="xi-close"></i>
									</button>
								</div>
							</div>
							<div class="row text-type">
								<div class="col-sm-2">
									<div class="write-sub-title margin-top-10">
										<b class="caution-star">*</b> 이벤트 유형​
									</div>
								</div>
								<div class="col-sm-10">
									<ul class="list-inline" data-sel="cephSel${status.index}">
									<input type="hidden" name="cephIndex" id="cephIndex" value="${status.index}"/>
										<li>
											<div class="radio-group padding-top-10">
												<input type="radio" name="radioCeph${status.index}" id="cuser${status.index}" class="radio-input" value="user"
																													${resultModel.adm_set_yn eq 'Y' ? '' : 'checked'}>
												<label for="cuser${status.index}" class="radio-item">
													<div class="radio">
														<div class="checked-icon"></div>
													</div>
													<span class="text">
														사용자가 직접 입력
													</span>
												</label>
											</div>
										</li>
										<li>
											<div class="radio-group padding-top-10" style="width: 200px;">
												<input type="radio" name="radioCeph${status.index}" id="cadmin${status.index}" class="radio-input" value="admin"
																													${resultModel.adm_set_yn eq 'Y' ? 'checked' : ''}>
												<label for="cadmin${status.index}" class="radio-item">
													<div class="radio">
														<div class="checked-icon"></div>
													</div>
													<span class="text">
														관리자 지정​
													</span>
													<span class="info-icon-tooltip" data-toggle="tooltip" title="관리자가 항목을 지정할 경우, 사용자는 설정을 변경할 수 없습니다.​​​" data-placement="top">
														i
													</span>
												</label>
											</div>
										</li>
										<li data-lst="admin"   style="${resultModel.adm_set_yn eq  'Y' ? '' : 'display:none;'}" >
											<div class="form-inline">
												<div class="radio-group margin-right-20">
													<input type="radio" id="chkFileCreate${status.index}"  name="chkFile${status.index}" value="create" 
																													${resultModel.create_yn eq "Y" ? "checked":""} class="radio-input">
													<label for="chkFileCreate${status.index}" class="radio-item">
														<span class="inline-block" style="vertical-align: bottom;">(</span>
														<div class="radio">
															<div class="checked-icon">
															</div>
														</div>
														<span class="text">
																파일 생성
														</span>
													</label>
												</div>
												<div class="radio-group margin-right-20">
													<input type="radio" id="chkFileDel${status.index}" name="chkFile${status.index}" value="delete" 
																													${resultModel.delete_yn eq "Y" ? "checked":""} class="radio-input">
													<label for="chkFileDel${status.index}" class="radio-item">
														<div class="radio">
															<div class="checked-icon">
															</div>
														</div>
														<span class="text">
															     파일 삭제
														</span>
														<span class="inline-block" style="vertical-align: bottom;">)</span>
													</label>
												</div>
											</div>
										</li>
									</ul>
								</div>
							</div>
						</div>
					</c:forEach>
				</c:if>		
			</div>
			<!-- //Ceph 트리거 -->	
			
			<!-- MongoDB 트리거 -->
			<div id="mongoArea">
			<c:if test="${!empty paramMap.mongoTriggerList}">
				<c:forEach items="${paramMap.mongoTriggerList}" var="resultModel" varStatus="status">
					<div class="bg-border-box trigger-ele">
						<div class="row">
							<div class="col-md-8">
								<div class="color-primary">
									<b>
										MongoDB 트리거​
									</b>
								</div>
							</div>
							<div class="col-md-4 text-right">
								<button class="close" data-btn="delete">
									<i class="xi-close"></i>
								</button>
							</div>
						</div>
						<div class="row text-type">
							<div class="col-sm-2">
								<div class="write-sub-title margin-top-10">
									<b class="caution-star">*</b> 이벤트 유형​
								</div>
							</div>
							<div class="col-sm-10">
								<ul class="list-inline" data-sel="mongoSel${status.index}">
								<input type="hidden" name="mongoIndex" id="mongoIndex" value="${status.index}"/>
									<li>
										<div class="radio-group padding-top-10">
											<input type="radio" name="radioMongo${status.index}" id="muser${status.index}" class="radio-input" value="user"
																																${resultModel.adm_set_yn eq 'Y' ? '' : 'checked'}>
											<label for="muser${status.index}" class="radio-item">
												<div class="radio">
													<div class="checked-icon"></div>
												</div>
												<span class="text">
													사용자가 직접 입력
												</span>
											</label>
										</div>
									</li>
									<li>
										<div class="radio-group padding-top-10" style="width: 200px;">
											<input type="radio" name="radioMongo${status.index}" id="madmin${status.index}" class="radio-input" value="admin"
																																${resultModel.adm_set_yn eq 'Y' ? 'checked' : ''}>
											<label for="madmin${status.index}" class="radio-item">
												<div class="radio">
													<div class="checked-icon"></div>
												</div>
												<span class="text">
													관리자 지정​
												</span>
												<span class="info-icon-tooltip" data-toggle="tooltip" title="관리자가 항목을 지정할 경우, 사용자는 설정을 변경할 수 없습니다.​" data-placement="top">
													i
												</span>
											</label>
										</div>
									</li>
									<li data-lst="admin" ${resultModel.adm_set_yn ne 'Y' ? 'style="display:none;"' : ''}>
										<div class="form-inline">
											<div class="radio-group margin-right-20">
												<input type="radio" id="chkMongoInsert${status.index}" name="chkMongo${status.index}" value="insert" class="radio-input"
																																		${resultModel.insert_yn eq "Y" ? "checked":""}>
												<label for="chkMongoInsert${status.index}" class="radio-item">
													<span class="inline-block" style="vertical-align: bottom;">(</span>
													<div class="radio">
														<div class="checked-icon">
														</div>
													</div>
													<span class="text">
														INSERT
													</span>
												</label>
											</div>
											<div class="radio-group margin-right-20">
												<input type="radio" id="chkMongoUpdate${status.index}" name="chkMongo${status.index}" value="update" class="radio-input"
																																		${resultModel.update_yn eq "Y" ? "checked":""}>
												<label for="chkMongoUpdate${status.index}" class="radio-item">
													<div class="radio">
														<div class="checked-icon">
														</div>
													</div>
													<span class="text">
														UPDATE
													</span>
												</label>
											</div>
											<div class="radio-group margin-right-20">
												<input type="radio" id="chkMongoDelete${status.index}" name="chkMongo${status.index}" value="delete" class="radio-input"
																																		${resultModel.delete_yn eq "Y" ? "checked":""}>
												<label for="chkMongoDelete${status.index}" class="radio-item">
													<div class="radio">
														<div class="checked-icon">
														</div>
													</div>
													<span class="text">
														DELETE
													</span>
													<span class="inline-block" style="vertical-align: bottom;">)</span>
												</label>
											</div>
										</div>
									</li>
								</ul>
							</div>
						</div>
					</div>
				</c:forEach>
			</c:if>
			</div>
			<!-- //MongoDB 트리거​ -->
			<!-- PostgreSQL 트리거 -->
			<div id="postgreArea">
			<c:if test="${!empty paramMap.postgreTriggerList}">
				<c:forEach items="${paramMap.postgreTriggerList}" var="resultModel" varStatus="status">			
					<div class="bg-border-box trigger-ele">
						<div class="row">
							<div class="col-md-8">
								<div class="color-primary">
									<b>
										PostgreSQL 트리거​
									</b>
								</div>
							</div>
							<div class="col-md-4 text-right">
								<button class="close" data-btn="delete">
									<i class="xi-close"></i>
								</button>
							</div>
						</div>
						<div class="row text-type">
							<div class="col-sm-2">
								<div class="write-sub-title margin-top-10">
									<b class="caution-star">*</b> 이벤트 유형​
								</div>
							</div>
							<div class="col-sm-10">
								<ul class="list-inline" data-sel="postgreSel${status.index}">
								<input type="hidden" name="postgreIndex" id="postgreIndex" value="${status.index}"/>
									<li>
										<div class="radio-group padding-top-10">
											<input type="radio" name="radioPostgre${status.index}" id="puser${status.index}" class="radio-input" value="user"
																																${resultModel.adm_set_yn eq 'Y' ? '' : 'checked'}>
											<label for="puser${status.index}" class="radio-item">
												<div class="radio">
													<div class="checked-icon"></div>
												</div>
												<span class="text">
													사용자가 직접 입력
												</span>
											</label>
										</div>
									</li>
									<li>
										<div class="radio-group padding-top-10" style="width: 200px;">
											<input type="radio" name="radioPostgre${status.index}" id="padmin${status.index}" class="radio-input" value="admin"
																																${resultModel.adm_set_yn eq 'Y' ? 'checked' : ''}>
											<label for="padmin${status.index}" class="radio-item">
												<div class="radio">
													<div class="checked-icon"></div>
												</div>
												<span class="text">
													관리자 지정​
												</span>
												<span class="info-icon-tooltip" data-toggle="tooltip" title="관리자가 항목을 지정할 경우, 사용자는 설정을 변경할 수 없습니다.​" data-placement="top">
													i
												</span>
											</label>
										</div>
									</li>
									<li data-lst="admin" ${resultModel.adm_set_yn ne 'Y' ? 'style="display:none;"' : ''}>
										<div class="form-inline">
											<div class="radio-group margin-right-20">
												<input type="radio" id="chkPostgreInsert${status.index}" name="chkPostgre${status.index}" value="insert" class="radio-input"
																																		${resultModel.insert_yn eq "Y" ? "checked":""}>
												<label for="chkPostgreInsert${status.index}" class="radio-item">
													<span class="inline-block" style="vertical-align: bottom;">(</span>
													<div class="radio">
														<div class="checked-icon">
														</div>
													</div>
													<span class="text">
														INSERT
													</span>
												</label>
											</div>
											<div class="radio-group margin-right-20">
												<input type="radio" id="chkPostgreUpdate${status.index}" name="chkPostgre${status.index}" value="update" class="radio-input"
																																		${resultModel.update_yn eq "Y" ? "checked":""}>
												<label for="chkPostgreUpdate${status.index}" class="radio-item">
													<div class="radio">
														<div class="checked-icon">
														</div>
													</div>
													<span class="text">
														UPDATE
													</span>
												</label>
											</div>
											<div class="radio-group margin-right-20">
												<input type="radio" id="chkPostgreDelete${status.index}" name="chkPostgre${status.index}" value="delete" class="radio-input"
																																		${resultModel.delete_yn eq "Y" ? "checked":""}>
												<label for="chkPostgreDelete${status.index}" class="radio-item">
													<div class="radio">
														<div class="checked-icon">
														</div>
													</div>
													<span class="text">
														DELETE
													</span>
													<span class="inline-block" style="vertical-align: bottom;">)</span>
												</label>
											</div>
										</div>
									</li>
								</ul>
							</div>
						</div>
					</div>	
				</c:forEach>
			</c:if>		
			</div>
			<!-- //PostgreSQL 트리거​ -->		
			<!-- 타이머 트리거 -->
			<div id="timerArea">
			<c:if test="${!empty paramMap.timerTriggerList}">
				<c:forEach items="${paramMap.timerTriggerList}" var="resultModel" varStatus="status">					
				<div class="bg-border-box trigger-ele">
				<div class="row">
					<div class="col-md-8">
						<div class="color-primary">
							<b>
								타이머 트리거
							</b>
						</div>
					</div>
					<div class="col-md-4 text-right">
						<a href="javascript:void(0)" class="close" data-btn="delete">
							<i class="xi-close"></i>
						</a>
					</div>
				</div>
				<div class="row text-type">
					<div class="col-sm-2">
						<div class="write-sub-title margin-top-10">
							<b class="caution-star">*</b> 반복 설정
						</div>
					</div>
					<div class="col-sm-10">
						<ul class="list-inline" data-sel="timerSel${status.index}">
						<input type="hidden" name="timerIndex" id="timerIndex" value="${status.index}"/>
							<li style="vertical-align: top;">
								<div class="radio-group padding-top-10">
									<input type="radio" name="radioTimer${status.index}" id="tuser${status.index}" class="radio-input" value="user" 
																																${resultModel.adm_set_yn eq 'Y' ? '' : 'checked'}>
									<label for="tuser${status.index}" class="radio-item">
										<div class="radio">
											<div class="checked-icon"></div>
										</div>
										<span class="text">
											사용자가 직접 입력
										</span>
									</label>
								</div>
							</li>
							<li style="vertical-align: top;">
								<div class="radio-group padding-top-10" style="width: 200px;">
									<input type="radio" name="radioTimer${status.index}" id="tadmin${status.index}" class="radio-input" value="admin"
																																${resultModel.adm_set_yn eq 'Y' ? 'checked' : ''}>
									<label for="tadmin${status.index}" class="radio-item">
										<div class="radio">
											<div class="checked-icon"></div>
										</div>
										<span class="text">
											관리자 지정
										</span>
										<span class="info-icon-tooltip" data-toggle="tooltip" title="" data-placement="top" data-original-title="관리자가 항목을 지정할 경우, 사용자는 설정을 변경할 수 없습니다.">
											i
										</span>
									</label>
								</div>
							</li>
							
							
							<li data-lst="admin"  ${resultModel.adm_set_yn ne 'Y' ? 'style="width: 630px;display:none;"' : 'style="width: 630px;"'}>
								<ul class="list-inline">
								<input type="hidden" name="timerIndex2" id="timerIndex2" value="${status.index}"/>
									<li>
										<label for="timer${status.index}_triggeradd1" class="input-card-type">
											<input type="radio" id="timer${status.index}_triggeradd1" name="repeatSet${status.index}"  class="card-input-element"  ${resultModel.repeat_tp == null  ? 'checked':''}>
																																				
											<div class="card-input">
												<div class="card-type4">
													<span class="img-repeat">
														<span class="none">
															<i></i>
														</span>
													</span>
												</div>
											</div>
											<p class="text-center">
												없음
											</p>
										</label>
									</li>
									<li>
										<label for="timer${status.index}_perMinute" class="input-card-type">
											<input type="radio" id="timer${status.index}_perMinute" name="repeatSet${status.index}" 
																							value="perMinute" class="card-input-element" data-btn="minute${status.index}" 
																																	${resultModel.repeat_tp eq 'MN' ? 'checked':''}>
											<div class="card-input">
												<div class="card-type4">
													<span class="img-repeat">
														<span class="minute">
															<i></i>
														</span>
													</span>
												</div>
											</div>
											<p class="text-center">
												분
											</p>
										</label>
									</li>
									<li>
										<label for="timer${status.index}_perHour" class="input-card-type">
											<input type="radio" id="timer${status.index}_perHour" name="repeatSet${status.index}" 
																							value="perHour" class="card-input-element" data-btn="hour${status.index}"
																																	${resultModel.repeat_tp eq 'HR' ? 'checked':''}>
											<div class="card-input">
												<div class="card-type4">
													<span class="img-repeat">
														<span class="hour">
															<i></i>
														</span>
													</span>
												</div>
											</div>
											<p class="text-center">
												시간
											</p>
										</label>
									</li>
									<li>
										<label for="timer${status.index}_perDay" class="input-card-type">
											<input type="radio" id="timer${status.index}_perDay" name="repeatSet${status.index}" 
																							value="perDay" class="card-input-element" data-btn="day${status.index}"
																																	${resultModel.repeat_tp eq 'DY' ? 'checked':''}>
											<div class="card-input">
												<div class="card-type4">
													<span class="img-repeat">
														<span class="day">
															<i></i>
														</span>
													</span>
												</div>
											</div>
											<p class="text-center">
												매일
											</p>
										</label>
									</li>
									<li>
										<label for="timer${status.index}_perWeek" class="input-card-type">
											<input type="radio" id="timer${status.index}_perWeek" name="repeatSet${status.index}" 
																							value="perWeek" class="card-input-element" data-btn="week${status.index}"
																																	${resultModel.repeat_tp eq 'WK' ? 'checked':''}>
											<div class="card-input">
												<div class="card-type4">
													<span class="img-repeat">
														<span class="week">
															<i></i>
														</span>
													</span>
												</div>
											</div>
											<p class="text-center">
												주별
											</p>
										</label>
									</li>
									<li>
										<label for="timer${status.index}_perMonth" class="input-card-type">
											<input type="radio" id="timer${status.index}_perMonth" name="repeatSet${status.index}" value="perMonth" 
																				class="card-input-element" data-btn="month${status.index}" ${resultModel.repeat_tp eq 'MO' ? 'checked':''}>
											<div class="card-input">
												<div class="card-type4">
													<span class="img-repeat">
														<span class="month">
															<i></i>
														</span>
													</span>
												</div>
											</div>
											<p class="text-center">
												월별
											</p>
										</label>
									</li>
									<li>
										<label for="timer${status.index}_perYear" class="input-card-type">
											<input type="radio" id="timer${status.index}_perYear" name="repeatSet${status.index}" value="perYear" 
																				class="card-input-element" data-btn="year${status.index}" ${resultModel.repeat_tp eq 'YR' ? 'checked':''}>
											<div class="card-input">
												<div class="card-type4">
													<span class="img-repeat">
														<span class="year">
															<i></i>
														</span>
													</span>
												</div>
											</div>
											<p class="text-center">
												연간
											</p>
										</label>
									</li>
								</ul>
								
								<!-- 시간마다반복 -->
								<div class="margin-top-20">
									<div class="row" data-lst="repeat${status.index}" data-tab="minute${status.index}" ${resultModel.repeat_tp eq 'MN' ? '':'style="display: none;"'}>
										<div class="col-sm-12">
											<div class="form-inline">
												<div class="radio-group">
													<input type="radio" id="perMinute${status.index}_per" name="perMinute${status.index}_per" class="radio-input" checked>
													<label for="perMinute${status.index}_per" class="radio-item">
														<div class="radio">
															<div class="checked-icon"></div>
														</div>
														<span class="text">
															매
														</span>
													</label>
												</div>
												<div class="form-group">
													<select name="perMinuteVal${status.index}" id="perMinuteVal${status.index}" class="form-control">
													<c:forEach var="i" begin="1" end="59">
														<option value="${i}" ${resultModel.repeat_tp eq 'MN' && paramMap.timerTriggerResultList[status.index].value eq i ? 'selected' : ''}>${i}</option>
													</c:forEach>
													</select>
													<span class="text">분 마다 반복</span>
												</div>
											</div>
										</div>
									</div>
									<div class="row" data-lst="repeat${status.index}" data-tab="hour${status.index}" ${resultModel.repeat_tp eq 'HR' ? '':'style="display: none;"'}>
										<div class="col-sm-12">
											<div class="form-inline">
												<div class="radio-group">
													<input type="radio" id="perHour${status.index}_per" name="perHour${status.index}_per" value="per"
																																	class="radio-input" checked>
													<label for="perHour${status.index}_per" class="radio-item">
														<div class="radio">
															<div class="checked-icon"></div>
														</div>
														<span class="text">
															매
														</span>
													</label>
												</div>
												<div class="form-group">
													<select id="perHourVal${status.index}" name="perHourVal${status.index}"   class="form-control">
													<c:forEach var="i" begin="1" end="23">
														<option value="${i}" ${resultModel.repeat_tp eq 'HR' && paramMap.timerTriggerResultList[status.index].value eq i ? 'selected' : ''}>${i}</option>
													</c:forEach>
													</select>
													<span class="text">시간 마다 반복</span>
												</div>
											</div>
										</div>
									</div>
									
									<!-- 일마다반복 -->
									<div class="row" data-lst="repeat${status.index}" data-tab="day${status.index}"  ${resultModel.repeat_tp eq 'DY' ? '':'style="display: none;"'} >
										<div class="col-sm-4">
											<div class="form-inline">
												<div class="radio-group">
													<input type="radio" id="perDay${status.index}_per" name="perDay${status.index}" value= "per" class="radio-input"
													  					${paramMap.timerTriggerResultList[status.index].radio.indexOf('day') >= 0 ? 'checked' : ''}>
													<label for="perDay${status.index}_per" class="radio-item">
														<div class="radio">
															<div class="checked-icon"></div>
														</div>
														<span class="text">
															매
														</span>
													</label>
												</div>
												<div class="form-group">
													<select id="perDayVal${status.index}" name="perDayVal${status.index}" class="form-control">
													<c:forEach var="i" begin="1" end="30">
														<option value="${i}" ${resultModel.repeat_tp eq 'DY' && paramMap.timerTriggerResultList[status.index].value eq i ? 'selected' : ''}>${i}</option>
													</c:forEach>
													</select>
													<span class="text">일 마다 반복</span>
												</div>
											</div>
										</div>
										<div class="col-sm-8">
											<div class="form-inline">
												<div class="radio-group padding-top-10">
													<input type="radio" id="perDay${status.index}_noweekend" name="perDay${status.index}" class="radio-input"
													                ${paramMap.timerTriggerResultList[status.index].radio.indexOf('week') >= 0 ? 'checked' : ''}>
													<label for="perDay${status.index}_noweekend" class="radio-item">
														<div class="radio">
															<div class="checked-icon"></div>
														</div>
														<span class="text">
															평일
														</span>
													</label>
												</div>
											</div>
										</div>
									</div>
									
									<!-- 주 마다반복 -->
									<div class="row" data-lst="repeat${status.index}" data-tab="week${status.index}" ${resultModel.repeat_tp eq 'WK' ? '':'style="display: none;"'}>
										<div class="form-inline margin-top-10">
											<div class="checkbox-group margin-right-20">
												<input type="checkbox" id="perWeekVal${status.index}_monday" name="perWeekVal${status.index}" value="MON" class="checkbox-input"
																										${paramMap.timerTriggerResultList[status.index].value.indexOf('MON') >= 0 ? 'checked' : ''}>
												<label for="perWeekVal${status.index}_monday" class="checkbox-item">
													<div class="checkbox">
														<div class="checked-icon">
															<i class="xi-check"></i>
														</div>
													</div>
													<span class="text">
														월요일
													</span>
												</label>
											</div>
											<div class="checkbox-group margin-right-20">
												<input type="checkbox" id="perWeekVal${status.index}_tuesday" name="perWeekVal${status.index}" value="TUE" class="checkbox-input"
																										${paramMap.timerTriggerResultList[status.index].value.indexOf('TUE') >= 0 ? 'checked' : ''}>
												<label for="perWeekVal${status.index}_tuesday" class="checkbox-item">
													<div class="checkbox">
														<div class="checked-icon">
															<i class="xi-check"></i>
														</div>
													</div>
													<span class="text">
														화요일
													</span>
												</label>
											</div>
											<div class="checkbox-group margin-right-20">
												<input type="checkbox" id="perWeekVal${status.index}_wednesday"  name="perWeekVal${status.index}" value="WED" class="checkbox-input"
																										${paramMap.timerTriggerResultList[status.index].value.indexOf('WED') >= 0 ? 'checked' : ''}>
												<label for="perWeekVal${status.index}_wednesday" class="checkbox-item">
													<div class="checkbox">
														<div class="checked-icon">
															<i class="xi-check"></i>
														</div>
													</div>
													<span class="text">
														수요일
													</span>
												</label>
											</div>
											<div class="checkbox-group margin-right-20">
												<input type="checkbox" id="perWeekVal${status.index}_thursday" name="perWeekVal${status.index}" value="THU" class="checkbox-input"
																										${paramMap.timerTriggerResultList[status.index].value.indexOf('THU') >= 0 ? 'checked' : ''}>
												<label for="perWeekVal${status.index}_thursday" class="checkbox-item">
													<div class="checkbox">
														<div class="checked-icon">
															<i class="xi-check"></i>
														</div>
													</div>
													<span class="text">
														목요일
													</span>
												</label>
											</div>
											<div class="checkbox-group margin-right-20">
												<input type="checkbox" id="perWeekVal${status.index}_friday" name="perWeekVal${status.index}" value="FRI" class="checkbox-input"
																										${paramMap.timerTriggerResultList[status.index].value.indexOf('FRI') >= 0 ? 'checked' : ''}>
												<label for="perWeekVal${status.index}_friday" class="checkbox-item">
													<div class="checkbox">
														<div class="checked-icon">
															<i class="xi-check"></i>
														</div>
													</div>
													<span class="text">
														금요일
													</span>
												</label>
											</div>
											<div class="checkbox-group margin-right-20">
												<input type="checkbox" id="perWeekVal${status.index}_saturday" name="perWeekVal${status.index}" value="SAT" class="checkbox-input"
																										${paramMap.timerTriggerResultList[status.index].value.indexOf('SAT') >= 0 ? 'checked' : ''}>
												<label for="perWeekVal${status.index}_saturday" class="checkbox-item">
													<div class="checkbox">
														<div class="checked-icon">
															<i class="xi-check"></i>
														</div>
													</div>
													<span class="text">
														토요일
													</span>
												</label>
											</div>
											<div class="checkbox-group margin-right-20">
												<input type="checkbox" id="perWeekVal${status.index}_sunday" name="perWeekVal${status.index}" value="SUN" class="checkbox-input"
																										${paramMap.timerTriggerResultList[status.index].value.indexOf('SUN') >= 0 ? 'checked' : ''}>												
												<label for="perWeekVal${status.index}_sunday" class="checkbox-item">
													<div class="checkbox">
														<div class="checked-icon">
															<i class="xi-check"></i>
														</div>
													</div>
													<span class="text">
														일요일
													</span>
												</label>
											</div>
										</div>
									</div>
									
									<!-- 월별 -->
									<div class="row" data-lst="repeat${status.index}" data-tab="month${status.index}" ${resultModel.repeat_tp eq 'MO' ? '':'style="display: none;"'}>
										<div class="col-sm-5">
											<div class="form-inline">
												<div class="radio-group">
													<input type="radio" id="type${status.index}_month_eachDay" name="type${status.index}_month" value="eachDay" class="radio-input"
																							${paramMap.timerTriggerResultList[status.index].radio.indexOf('month') >= 0 ? 'checked' : ''}>
													<label for="type${status.index}_month_eachDay" class="radio-item">
														<div class="radio">
															<div class="checked-icon"></div>
														</div>
														<span class="text">
															매월
														</span>
													</label>
												</div>
												<div class="form-group">
													<select id="perMonthVal${status.index}" name="perMonthVal${status.index}" class="form-control">
														<c:forEach var="i" begin="1" end="31">
															<option value="${i}" ${resultModel.repeat_tp eq 'MO' && paramMap.timerTriggerResultList[status.index].value eq i ? 'selected' : ''}>${i}</option>
														</c:forEach>													
													</select>
													<span class="text">일 마다 반복</span>
												</div>
											</div>
										</div>
										<div class="col-sm-7">
											<div class="form-inline">
												<div class="radio-group">
													<input type="radio" id="type${status.index}_month_eachSeq" name="type${status.index}_month" class="radio-input" 
																								${paramMap.timerTriggerResultList[status.index].radio.indexOf('week') >= 0 ? 'checked' : ''}>
													<label for="type${status.index}_month_eachSeq" class="radio-item">
														<div class="radio">
															<div class="checked-icon"></div>
														</div>
													</label>
												</div>
												<div class="form-group">
													<select id="selMonthWeek_1_Val${status.index}" name="selMonthWeek_1_Val${status.index}" class="form-control">
														<option value="1" ${resultModel.repeat_tp eq 'MO' && paramMap.timerTriggerResultList[status.index].value_th eq '1' ? 'selected' : ''}>첫 번째</option>
														<option value="2" ${resultModel.repeat_tp eq 'MO' && paramMap.timerTriggerResultList[status.index].value_th eq '2' ? 'selected' : ''}>두 번째</option>
														<option value="3" ${resultModel.repeat_tp eq 'MO' && paramMap.timerTriggerResultList[status.index].value_th eq '3' ? 'selected' : ''}>세 번째</option>
														<option value="4" ${resultModel.repeat_tp eq 'MO' && paramMap.timerTriggerResultList[status.index].value_th eq '4' ? 'selected' : ''}>네 번째</option>
														<option value="5" ${resultModel.repeat_tp eq 'MO' && paramMap.timerTriggerResultList[status.index].value_th eq '5' ? 'selected' : ''}>다섯번째</option>
													</select>
												</div>
												<div class="form-group">
													<select id="selMonthWeek_2_Val${status.index}" name="selMonthWeek_2_Val${status.index}" class="form-control">
														<option value="MON" ${resultModel.repeat_tp eq 'MO' && paramMap.timerTriggerResultList[status.index].value_week eq 'MON' ? 'selected' : ''}>월요일</option>
														<option value="TUE" ${resultModel.repeat_tp eq 'MO' && paramMap.timerTriggerResultList[status.index].value_week eq 'TUE' ? 'selected' : ''}>화요일</option>
														<option value="WED" ${resultModel.repeat_tp eq 'MO' && paramMap.timerTriggerResultList[status.index].value_week eq 'WED' ? 'selected' : ''}>수요일</option>
														<option value="THU" ${resultModel.repeat_tp eq 'MO' && paramMap.timerTriggerResultList[status.index].value_week eq 'THU' ? 'selected' : ''}>목요일</option>
														<option value="FRI" ${resultModel.repeat_tp eq 'MO' && paramMap.timerTriggerResultList[status.index].value_week eq 'FRI' ? 'selected' : ''}>금요일</option>
														<option value="SAT" ${resultModel.repeat_tp eq 'MO' && paramMap.timerTriggerResultList[status.index].value_week eq 'SAT' ? 'selected' : ''}>토요일</option>
														<option value="SUN" ${resultModel.repeat_tp eq 'MO' && paramMap.timerTriggerResultList[status.index].value_week eq 'SUN' ? 'selected' : ''}>일요일</option>
													</select>
													<span class="text">마다 반복</span>
												</div>
											</div>
										</div>
									</div>
									
									<!-- 년별 -->
									<div class="row" data-lst="repeat${status.index}" data-tab="year${status.index}" ${resultModel.repeat_tp eq 'YR' ? '':'style="display: none;"'}>
										<div class="col-sm-5">
											<div class="form-inline">
												<div class="radio-group">
													<input type="radio" id="type${status.index}_year_eachMonth" name="type${status.index}_year" value="eachMonth" class="radio-input" 
																									${paramMap.timerTriggerResultList[status.index].radio.indexOf('month') >= 0 ? 'checked' : ''}>
													<label for="type${status.index}_year_eachMonth" class="radio-item">
														<div class="radio">
															<div class="checked-icon"></div>
														</div>
														<span class="text">
															매년
														</span>
													</label>
												</div>
												<div class="form-group">
													<select id="perYearVal${status.index}" name="perYearVal${status.index}" class="form-control">
														<c:forEach var="i" begin="1" end="12">
															<option value="${i}"  ${resultModel.repeat_tp eq 'YR' && paramMap.timerTriggerResultList[status.index].value eq i ? 'selected' : ''}>${i}</option>
														</c:forEach>	
													</select>
													<span class="text">월 마다 반복</span>
												</div>
											</div>
										</div>
										<div class="col-sm-7">
											<div class="form-inline">
												<div class="radio-group">
													<input type="radio" id="type${status.index}_year_eachSeq" name="type${status.index}_year" class="radio-input" 
																									${paramMap.timerTriggerResultList[status.index].radio.indexOf('week') >= 0 ? 'checked' : ''}>
													<label for="type${status.index}_year_eachSeq" class="radio-item">
														<div class="radio">
															<div class="checked-icon"></div>
														</div>
													</label>
												</div>
												<div class="form-group">
													<select id="selYearWeek_1_Val${status.index}" name="selYearWeek_1_Val${status.index}" class="form-control">
														<option value="1" ${resultModel.repeat_tp eq 'YR' && paramMap.timerTriggerResultList[status.index].value_th eq '1' ? 'selected' : ''}>첫 번째</option>
														<option value="2" ${resultModel.repeat_tp eq 'YR' && paramMap.timerTriggerResultList[status.index].value_th eq '2' ? 'selected' : ''}>두 번째</option>
														<option value="3" ${resultModel.repeat_tp eq 'YR' && paramMap.timerTriggerResultList[status.index].value_th eq '3' ? 'selected' : ''}>세 번째</option>
														<option value="4" ${resultModel.repeat_tp eq 'YR' && paramMap.timerTriggerResultList[status.index].value_th eq '4' ? 'selected' : ''}>네 번째</option>
														<option value="5" ${resultModel.repeat_tp eq 'YR' && paramMap.timerTriggerResultList[status.index].value_th eq '5' ? 'selected' : ''}>다섯번째</option>
													</select>
												</div>
												<div class="form-group">
													<select id="selYearWeek_2_Val${status.index}" name="selYearWeek_2_Val${status.index}" class="form-control">
														<option value="MON" ${resultModel.repeat_tp eq 'YR' && paramMap.timerTriggerResultList[status.index].value_week eq 'MON' ? 'selected' : ''}>월요일</option>
														<option value="TUE" ${resultModel.repeat_tp eq 'YR' && paramMap.timerTriggerResultList[status.index].value_week eq 'TUE' ? 'selected' : ''}>화요일</option>
														<option value="WED" ${resultModel.repeat_tp eq 'YR' && paramMap.timerTriggerResultList[status.index].value_week eq 'WED' ? 'selected' : ''}>수요일</option>
														<option value="THU" ${resultModel.repeat_tp eq 'YR' && paramMap.timerTriggerResultList[status.index].value_week eq 'THU' ? 'selected' : ''}>목요일</option>
														<option value="FRI" ${resultModel.repeat_tp eq 'YR' && paramMap.timerTriggerResultList[status.index].value_week eq 'FRI' ? 'selected' : ''}>금요일</option>
														<option value="SAT" ${resultModel.repeat_tp eq 'YR' && paramMap.timerTriggerResultList[status.index].value_week eq 'SAT' ? 'selected' : ''}>토요일</option>
														<option value="SUN" ${resultModel.repeat_tp eq 'YR' && paramMap.timerTriggerResultList[status.index].value_week eq 'SUN' ? 'selected' : ''}>일요일</option>
													</select>
													<span class="text">마다 반복</span>
												</div>
											</div>
										</div>
									</div>
								</div>
								<script>
									// 반복설정 제어
	 								$('[data-lst="repeat"]').hide();
									$(document).on('click', '[data-lst="admin"] li input', function() {		
									 	var parents = $(this).parent().parent().parent();

										var divObj = null;
										for (var i=0;i<parents.length;i++) {
											var parent = parents[i];
											console.log($(parent));
											if ($(parent).hasClass("list-inline")) {
												divObj = parent;
												break;
											}
										}
										
										var index = $(divObj).find('input[name="timerIndex2"]').val();
										var section = $(this).attr('data-btn');
									    var repeat = "repeat"+index;
										
										$('[data-lst='+ repeat+']').hide();
										$('[data-tab='+ section +']').show();
									}); 
								</script>
							</li>
						</ul>
					</div>
				</div>
			</div>		
			</c:forEach>
			</c:if>	
			</div>
			
			
			<div class="row margin-top-30">
				<div class="col-md-6">
					<div class="write-sub-title margin-top-10">
						<b class="caution-star">*</b> 런타임
						<span class="info-icon-tooltip" data-toggle="tooltip" title="" data-placement="right" data-original-title="함수를 작성할 때 사용할 언어를 선택해 주세요.">
							i
						</span>
					</div>
					<ul class="list-inline">
						<li id="li_nodejs">
							<label for="node" class="input-card-type">
								<input type="radio" id="node" name="runtime" class="card-input-element" value="nodejs">
								<div class="card-input">
									<div class="card-type4">
										<div class="img-nodejs img-size70">
										</div>
									</div>
								</div>
								<p class="text-center">
									Node.js
								</p>
							</label>
						</li>
						<li id="li_python">
							<label for="python" class="input-card-type">
								<input type="radio" id="python" name="runtime" class="card-input-element" value="python">
								<div class="card-input">
									<div class="card-type4">
										<div class="img-python img-size70">
										</div>
									</div>
								</div>
								<p class="text-center">
									Python
								</p>
							</label>
						</li>
					</ul>
				</div>
				<div class="col-md-6">
					<div class="write-sub-title margin-top-10">
						<b class="caution-star">*</b> 런타임 버전
						<span class="info-icon-tooltip" data-toggle="tooltip" title="" data-placement="right" data-original-title="함수를 작성할 때 사용할 언어의 세부 버전을 선택해 주세요.">
							i
						</span>
					</div>
					<ul class="list-inline" id="nodejsVersion">
						<li>
							<label for="node12" class="input-card-type">
								<input type="radio" id="node12" name="version" value="12" class="card-input-element" ${paramMap.templateVO.version eq '12' ? 'checked':''}>
								<div class="card-input">
									<div class="card-type4 txt-wrap">
										<div class="text-area">
											12.x​
										</div>
									</div>
								</div>
								<p class="text-center">
									12.x​
								</p>
							</label>
						</li>
						<li>
							<label for="node10" class="input-card-type">
								<input type="radio" id="node10" name="version" value="10" class="card-input-element" ${paramMap.templateVO.version eq '10' ? 'checked':''}>
								<div class="card-input">
									<div class="card-type4 txt-wrap">
										<div class="text-area">
											10.x​
										</div>
									</div>
								</div>
								<p class="text-center">
									10.x​
								</p>
							</label>
						</li>
						<li>
							<label for="node8" class="input-card-type">
								<input type="radio" id="node8" name="version" value="8.10" class="card-input-element" ${paramMap.templateVO.version eq '8.10' ? 'checked':''}>
								<div class="card-input">
									<div class="card-type4 txt-wrap">
										<div class="text-area">
											8.10​
										</div>
									</div>
								</div>
								<p class="text-center">
									8.10​
								</p>
							</label>
						</li>
					</ul>
					
					<ul class="list-inline" id="pythonVersion">
						<li>
							<label for="pyVersion_3_7" class="input-card-type">
								<input type="radio" id="pyVersion_3_7" name="version" class="card-input-element" value="3.7" ${paramMap.templateVO.version eq '3.7' ? 'checked':''}>
								<div class="card-input">
									<div class="card-type4 txt-wrap">
										<div class="text-area">
											3.7
										</div>
									</div>
								</div>
								<p class="text-center">
									3.7
								</p>
							</label>
						</li>						
						<li>
							<label for="python3_8" class="input-card-type">
								<input type="radio" id="python3_8" name="version" class="card-input-element" value="3.8" ${paramMap.templateVO.version eq '3.8' ? 'checked':''}>
								<div class="card-input">
									<div class="card-type4 txt-wrap">
										<div class="text-area">
											3.8
										</div>
									</div>
								</div>
								<p class="text-center">
									3.8
								</p>
							</label>
						</li>
					</ul>
										
				</div>
			</div>
			<div class="write-sub-title">
				<b class="caution-star">*</b>함수 코드
			</div>
<!-- 			<div class="row padding-left-10 padding-right-10">
				<div class="editorBox" style="height: 600px;" id="editor">
					EDITOR 가 들어가는 영역입니다.
				</div>
			</div> -->
			
			<div class="row_wrap">
			    <div class="tabWrap type3">
			        <ul class="nav nav-tabs source-fileName">
			            <!-- li class에 active를 넣으면 활성 상태 -->
			            <li id="index" class="active">
			                <a href="javascript:indexActive();" title="index.js">index.js</a>
			            </li>
			
			            <li id="package">
			                <a href="javascript:packageActive();" title="package.json">package.json</a>
			            </li>
			        </ul>
			    </div><!--//tabWrap-->
				<div class="row padding-left-10 padding-right-10">
					<div class="editorBox" style="height: 600px;" id="editor1">
						EDITOR 가 들어가는 영역입니다.
					</div>
					<div class="editorBox" style="height: 600px;display:none;" id="editor2">
						EDITOR 가 들어가는 영역입니다.
					</div>					
				</div>
			</div>	
			
					
		</div> <!-- // panel-body -->
	</section>
	
	<div class="margin-top-40 text-right">
		<button type="button" class="btn btn-lg btn-primary" onclick="javascript:goUpdate();">확인</button>
		<button type="button" class="btn btn-lg btn-default" onclick="javascript:goDetail('${paramMap.templateVO.template_id}');">취소</button>
	</div>
</div>
