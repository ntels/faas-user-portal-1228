<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>  


<script type="text/javascript" src="${pageContext.request.contextPath}/core/admin/js/formSubmit.js"></script>		
<!-- ace editor -web editor- -->
<script type="text/javascript" src="${pageContext.request.contextPath}/core/js/ace-builds/src-noconflict/ace.js" type="text/javascript" charset="utf-8"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/core/js/ace-builds/src-noconflict/ext-modelist.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/core/js/ace-builds/src-noconflict/ext-language_tools.js"></script> 
<script>


var condition = ${condition};
var editor1 ='';
var editor2 ='';





//개발상황에 맞춰 새롭게 구성하세요.
$(document).ready(function(){
	$("#contentWrap").css("display","block");
	$('[data-lst="admin"]').hide();   	//'각 트리거 > 관리자 지정' 선택시 show
	deleteTrigger();	
	feather.replace();
	var initSrc = 'module.exports = function(app)\n'
				+'{\n'
				+'  app.post("/", function(req, res) {\n'
				+'    console.log(req.event_type);\n'
				+'    console.log(req.event_data);\n'
				+'    res.status(200).json(req.event_data);\n'
				+'  });\n'
				+'}\n';
    var result = {fileName:"index.js",fileContent:initSrc};
	editor1 = setEditor("editor1",result,false);
	
    initSrc =   '{\n'
	    +'  "name": "knative-serving",\n'
	    +'  "version": "1.0.0",\n'
	    +'  "description": "knative serving application",\n'
	    +'  "main": "app.js",\n'
	    +'  "scripts": {\n'
	    +'    "start": "node app.js"\n'
	    +'  },\n'
	    +'  "license": "Apache-2.0",\n'
	    +'  "dependencies": {\n'
	    +'    "express": "4.16.0",\n'
	    +'    "body-parser": "1.19.0",\n'
	    +'    "cors": "2.8.5"\n'
	    +'  }\n'
	    +'}\n'
	result = {fileName:"package.json",fileContent:initSrc};    
	editor2 = setEditor("editor2",result,false);
})



function goSave() {
    $("#fileContent1").val(editor1.getValue());
    $("#fileContent2").val(editor2.getValue());

	var param = $("#saveForm").serialize();
	param = queryStringToJSON(param);
	console.log(param);
	if(isEmpty(param.name)) {
		alert("함수 템플릿 이름을 입력하세요.");
		return false;
	}
	if(!isEmpty(param.httpIndex)) {
		var index = new Array();
		index = (param.httpIndex).split(',');
		for(var i=0;i<index.length;i++) {
			var adminYN = "radioHttp"+i;
			if(param[adminYN]=='admin'){
				var httpFunction = "httpApi"+i;
				console.log(JSON.stringify(param));
				if(isEmpty(param[httpFunction])) {
					alert("API 이름을 입력하세요.");
					return false;
				}
			}
		}
	}
	if(isEmpty(param.version)) {
		alert("버젼을 선택하세요")
		return false;
	}
	if(isEmpty(param.httpIndex) && isEmpty(param.cephIndex) && isEmpty(param.mongoIndex) && isEmpty(param.postgreIndex) && isEmpty(param.timerIndex)) {
		alert("트리거를 추가하세요.");
		return false;
	}	

	param = JSON.stringify(param),
	
	formData = param;
	$(".loading-wrap").show();
 	$.ajax({
        url : "template/save",
        data : param,
        type : 'POST',
        dataType : 'json',
        contentType:"application/json;charset=utf-8",
        traditional : true,
        success : function(data) {
          //$("#contentWrap").load("template/main",{action: "modifyAction","template_id":data.templateVO.template_id});
        	switchContent("${pageContext.request.contextPath}","/faas/?menu=/admin/template/list");
        },
        error: function(request, status, error){	        	
        	alert("code : "+ request.status + "\r\nmessage : "+request.responseText + "\r\error : "+error);
        },
        complete : function() {
        	$(".loading-wrap").hide();
        }
	});	 
}


</script>
<form name="saveForm" id="saveForm">
<input type="hidden" name="fileContent1" id="fileContent1"/>
<input type="hidden" name="fileContent2" id="fileContent2"/>

<span id="httpCnt" name="httpCnt" style="display:none"></span>
<span id="cephCnt" name="cephCnt" style="display:none"></span>
<span id="mongoCnt" name="mongoCnt" style="display:none"></span>
<span id="postgreCnt" name="postgreCnt" style="display:none"></span>
<span id="timerCnt" name="timerCnt" style="display:none"></span>


<main id="contentWrap" class="dashboard-wrap">
			<div class="page-title">
					<div id="spot">
						<div class="page-info">
							함수 템플릿 관리​
						</div>
						<div class="page-location">
							<span class="home">
								<i class="xi-home-o"></i>
								HUBPoP
							</span>
							<span>함수 템플릿 등록</span>
						</div>
						<a href="javascript:goList();" class="btn-list-all" title="함수 관리 목록">
							<i class="xi-border-all"></i>
							<p class="sr-only">함수 관리 목록</p>
						</a>
						<a href="javascript:goAdd();" class="btn-refresh" title="새로고침">
							<i class="xi-renew"></i>
							<p class="sr-only">새로고침</p>
						</a>
					</div>
				</div>
<div class="page-content">
<div class="inner-wrap">
	<!-- 콘텐츠가 들어갑니다 -->
	<section class="panel panel-bordered">
		<div class="panel-heading">    
			<h3 class="panel-title">
				함수 템플릿 등록
			</h3>
		</div> <!-- // panel-heading -->
		<div class="panel-body">
			<div class="text-right">
				<span class="color-danger">
					<b class="caution-star">*</b> 는 필수입력 사항입니다
				</span>
			</div>
			
			<div class="write-sub-title">
				<b class="caution-star">*</b> 함수 템플릿 이름
				<span class="info-icon-tooltip" data-toggle="tooltip" title="" data-placement="right" data-original-title="공백 없이 영문 대소문자, 숫자, 하이픈(-), 언더바(_)만 입력해 주세요.">
					i
				</span>
			</div>
			<div class="row">
				<div class="col-sm-9">
					<div class="form-group margin-bottom-0">
						<input type="text" class="form-control" id="name" name="name" placeholder="함수 템플릿 이름을 입력해 주세요."
																							onKeyup="this.value=this.value.replace(/[^a-zA-Z0-9-]/g,'');">
					</div>
				</div>
				<div class="col-sm-3">
					<div class="checkbox-group margin-top-5">
						<input type="checkbox" id="top_yn" name="top_yn" class="checkbox-input" value="y">
						<label for="top_yn" class="checkbox-item">
							<div class="checkbox">
								<div class="checked-icon">
									<i class="xi-check"></i>
								</div>
							</div>
							<span class="text">상단 노출
								<span class="info-icon-tooltip" data-toggle="tooltip" title="" data-placement="right" data-original-title="체크 시, 사용자 포털 함수 템플릿 목록의 상단에 노출됩니다.">
									i
								</span>
							</span>
						</label>
					</div>
				</div>
			</div>
			<div class="write-sub-title">
				함수 템플릿 설명
				<span class="info-icon-tooltip" data-toggle="tooltip" title="" data-placement="right" data-original-title="함수 템플릿 에서 제공하는 기능을 설명해 주세요.">
					i
				</span>
			</div>
			<div class="form-group">
				<textarea name="des" id="des" class="form-control" placeholder="함수 템플릿 에서 제공하는 기능을 설명해 주세요." rows="4"></textarea>
			</div>
		
			<div class="write-sub-title">
				트리거
				<span class="info-icon-tooltip" data-toggle="tooltip" title="" data-placement="right" data-original-title="이 함수 템플릿을 이용하여 함수를 생성할 때 반드시 입력해야 할 트리거가 있다면 모두 추가해 주세요.">
					i
				</span>
			</div>
			<div class="row">	
				<div class="col-sm-2">
					<label for="HTTP" class="input-card-type">
						<input type="radio" id="HTTP" name="triggeradd" class="card-input-element">
						<div class="card-input" onClick="javascript:addTrigger('httpTrigger');">
							<div class="card-type4 type-trigger padding-0">
								<div class="img-size50 feather-26 color-primary padding-top-10">
									<i data-feather="globe"></i>
								</div>
								<div class="text-area">
									<div class="title">HTTP 추가</div>
								</div>
							</div>
						</div>
					</label>
				</div>
				
				<div class="col-sm-2">
					<label for="Ceph" class="input-card-type">
						<input type="radio" id="Ceph" name="triggeradd" class="card-input-element">
						<div class="card-input" onClick="javascript:addTrigger('cephTrigger');">
							<div class="card-type4 type-trigger padding-0">
								<div class="img-size50 feather-26 padding-top-10">
									<i class="img-size26 img-ceph"></i>
								</div>
								<div class="text-area">
									<div class="title">Ceph 추가</div>
								</div>
							</div>
						</div>
					</label>
				</div>
				
				<div class="col-sm-2">
					<label for="MongoDB" class="input-card-type">
						<input type="radio" id="MongoDB" name="triggeradd" class="card-input-element">
						<div class="card-input" onClick="javascript:addTrigger('mongoTrigger');">
							<div class="card-type4 type-trigger padding-0">
								<div class="img-size50 feather-26 color-primary padding-top-10">
									<i data-feather="database"></i>
								</div>
								<div class="text-area">
									<div class="title">MongoDB 추가</div>
								</div>
							</div>
						</div>
					</label>
				</div>
				<div class="col-sm-2">
					<label for="PostgreSQL" class="input-card-type">
						<input type="radio" id="PostgreSQL" name="triggeradd" class="card-input-element">
						<div class="card-input" onClick="javascript:addTrigger('postgreTrigger');">
							<div class="card-type4 type-trigger padding-0">
								<div class="img-size50 feather-26 padding-top-10">
									<i class="img-size26 img-postgresql"></i>
								</div>
								<div class="text-area">
									<div class="title">PostgreSQL 추가</div>
								</div>
							</div>
						</div>
					</label>
				</div>
				<div class="col-sm-2">
					<label for="Timer" class="input-card-type">
						<input type="radio" id="Timer" name="triggeradd" class="card-input-element">
						<div class="card-input" onClick="javascript:addTrigger('timerTrigger');">
							<div class="card-type4 type-trigger padding-0">
								<div class="img-size50 feather-26 color-primary padding-top-10">
										<i data-feather="clock"></i>
								</div>
								<div class="text-area">
									<div class="title">타이머 추가</div>
								</div>
							</div>
						</div>
					</label>
				</div>
			</div>
			<!-- HTTP 트리거 -->
			<div id="httpArea"></div>
			 
			<!-- Ceph 트리거 -->
			<div id="cephArea"></div>
			
			<!-- MongoDB 트리거 -->
			<div id="mongoArea"></div>
			
			<!-- PostgreSQL 트리거 -->
			<div id="postgreArea"></div>
						
			<!-- 타이머 트리거 -->
			<div id="timerArea"></div>
			
			
			<div class="row margin-top-30">
				<div class="col-md-6">
					<div class="write-sub-title margin-top-10">
						<b class="caution-star">*</b> 런타임
						<span class="info-icon-tooltip" data-toggle="tooltip" title="" data-placement="right" data-original-title="함수를 작성할 때 사용할 언어를 선택해 주세요.">
							i
						</span>
					</div>
					<ul class="list-inline">
						<li onclick="javascript:goNodeJs();">
							<label for="node" class="input-card-type">
								<input type="radio" id="node" name="runtime" class="card-input-element" value="nodejs" checked="">
								<div class="card-input">
									<div class="card-type4">
										<div class="img-nodejs img-size70">
										</div>
									</div>
								</div>
								<p class="text-center">
									Node.js
								</p>
							</label>
						</li>
						<li onclick="javascript:goPython();">
							<label for="python" class="input-card-type">
								<input type="radio" id="python" name="runtime" class="card-input-element" value="python">
								<div class="card-input">
									<div class="card-type4">
										<div class="img-python img-size70">
										</div>
									</div>
								</div>
								<p class="text-center">
									Python
								</p>
							</label>
						</li>
					</ul>
				</div>
				<div class="col-md-6">
					<div class="write-sub-title margin-top-10">
						<b class="caution-star">*</b> 런타임 버전
						<span class="info-icon-tooltip" data-toggle="tooltip" title="" data-placement="right" data-original-title="함수를 작성할 때 사용할 언어의 세부 버전을 선택해 주세요.">
							i
						</span>
					</div>
					<ul class="list-inline" id="nodejsVersion" style="display:block;">
						<li>
							<label for="card21" class="input-card-type">
								<input type="radio" id="card21" name="version" value="12" class="card-input-element">
								<div class="card-input">
									<div class="card-type4 txt-wrap">
										<div class="text-area">
											12.x​
										</div>
									</div>
								</div>
								<p class="text-center">
									12.x​
								</p>
							</label>
						</li>
						<li>
							<label for="card22" class="input-card-type">
								<input type="radio" id="card22" name="version" value="10" class="card-input-element">
								<div class="card-input">
									<div class="card-type4 txt-wrap">
										<div class="text-area">
											10.x​
										</div>
									</div>
								</div>
								<p class="text-center">
									10.x​
								</p>
							</label>
						</li>
						<li>
							<label for="card23" class="input-card-type">
								<input type="radio" id="card23" name="version" value="8.10" class="card-input-element">
								<div class="card-input">
									<div class="card-type4 txt-wrap">
										<div class="text-area">
											8.10​
										</div>
									</div>
								</div>
								<p class="text-center">
									8.10​
								</p>
							</label>
						</li>
					</ul>
					
					<ul class="list-inline" id="pythonVersion" style="display:none;">
						<li>
							<label for="pyVersion_3_7" class="input-card-type">
								<input type="radio" id="pyVersion_3_7" name="version" class="card-input-element" value="3.7">
								<div class="card-input">
									<div class="card-type4 txt-wrap">
										<div class="text-area">
											3.7
										</div>
									</div>
								</div>
								<p class="text-center">
									3.7
								</p>
							</label>
						</li>					
						<li>
							<label for="pyVersion_3_8" class="input-card-type">
								<input type="radio" id="pyVersion_3_8" name="version" class="card-input-element" value="3.8">
								<div class="card-input">
									<div class="card-type4 txt-wrap">
										<div class="text-area">
											3.8
										</div>
									</div>
								</div>
								<p class="text-center">
									3.8
								</p>
							</label>
						</li>
					</ul>
										
				</div>
			</div>
			<div class="write-sub-title">
				<b class="caution-star">*</b>함수 코드
			</div>
<!-- 			<div class="row padding-left-10 padding-right-10">
				<div class="editorBox" style="height: 600px;" id="editor">
					EDITOR 가 들어가는 영역입니다.
				</div>
			</div> -->
			
			<div class="row_wrap">
			    <div class="tabWrap type3">
			        <ul class="nav nav-tabs source-fileName">
			            <!-- li class에 active를 넣으면 활성 상태 -->
			            <li id="index" class="active">
			                <a href="javascript:indexActive();" title="index.js">index.js</a>
			            </li>
			
			            <li id="package">
			                <a href="javascript:packageActive();" title="package.json">package.json</a>
			            </li>
			        </ul>
			    </div><!--//tabWrap-->
				<div class="row padding-left-10 padding-right-10">
					<div class="editorBox" style="height: 600px;" id="editor1">
						EDITOR 가 들어가는 영역입니다.
					</div>
					<div class="editorBox" style="height: 600px;display:none;" id="editor2">
						EDITOR 가 들어가는 영역입니다.
					</div>					
				</div>
			</div>	
			
					
		</div> <!-- // panel-body -->
	</section>
	
	<div class="margin-top-40 text-right">
		<a href='javascript:goSave();' class="btn btn-lg btn-primary" >등록</a>
		<button type="button" class="btn btn-lg btn-default" onclick="javascript:goList();">취소</button>
	</div>
</div>
</div>

<div class="loading-wrap" style="display:none;">
			<div class="loading-box">
				<div class="img">
					<img src="${pageContext.request.contextPath}/core/img/common/loading.png" alt="">
				</div>
				<ul class="dot-pulse">
					<li> <div class="dot"></div> </li>
					<li> <div class="dot"></div> </li>
					<li> <div class="dot"></div> </li>
					<li> <div class="dot"></div> </li>
					<li> <div class="dot"></div> </li>
				</ul>
			</div>
		</div>
</main>
</form>

