<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>  

<!-- ace editor -web editor- -->
<script type="text/javascript" src="${pageContext.request.contextPath}/core/js/ace-builds/src-noconflict/ace.js" type="text/javascript" charset="utf-8"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/core/js/ace-builds/src-noconflict/ext-modelist.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/core/js/ace-builds/src-noconflict/ext-language_tools.js"></script>

<script>
var condition = ${condition};
$("#template_id").val(condition.template_id);

$(document).ready(function() {
	var result = ${sourceList};
	editor1 = setEditor("editor1",result[0],true);
	editor2 = setEditor("editor2",result[1],true);
	$("#template_id").val(condition.template_id);
	feather.replace();
	setTopLink('javascript:goDetail("'+condition.template_id+'");');
});

</script>
<input type="hidden" name="template_id" id="template_id">
<div class="inner-wrap">
	<!-- 콘텐츠가 들어갑니다 -->
	<div class="card card-type2">
		<div class="card-header">
			<div class="card-info-area">
				<div class="text-box">
					<div class="top">
						<div class="row">
							<div class="col-xs-10">
								<b>
									${templateVO.dsp_name}
								</b>
									${templateVO.top_yn eq "Y" ? '<span class="label label-info margin-left-10">상단노출</span>' : ''}
							</div>
							<div class="col-xs-2">
								<div class="dropdown pull-right">
									<button type="button" class="btn btn-xs btn-nostyle dropdown-toggle" data-toggle="dropdown">
										<div class="feather-icon">
											<i data-feather="more-horizontal"></i>
										</div>
									</button>
									<ul class="dropdown-menu" role="menu">
										<li>
											<a href="javascript:goModify();">
												<i data-feather="edit-3"  class="dropdown-icon"></i>
												수정
											</a>
										</li>
										<li>
											<a data-toggle="modal" data-target="#modalConfirm">
												<i data-feather="trash-2" class="dropdown-icon"></i>
												삭제
											</a>
										</li>
									</ul>
								</div>	
							</div>
						</div>	
						<div class="row">
							<div class="col-sm-6">
								<span class="lagn">
									${templateVO.runtime eq 'python' ? '<span class="img-size15 img-python"></span>' : '<span class="img-size15 img-nodejs"></span>'}
										${templateVO.runtime} ${templateVO.version}
									<!-- <span class="img-size15 img-nodejs"></span>
									Node -->
								</span>
							</div>
							<div class="col-sm-6">
								<div class="text-right">
									<span>
										<b>등록 일시</b>
										<span>${templateVO.reg_date_hms}</span>
									</span>
									<span class="left-bar">
										<b>등록자</b>
										<span>${templateVO.reg_usr}​</span>
									</span>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>

	<section class="panel panel-bordered">
		<div class="panel-heading">
			<h3 class="panel-title">
				트리거
			</h3>
		</div> <!-- // panel-heading -->
		<div class="panel-body">
			<c:if test="${!empty paramMap.httpTriggerList}">
				<c:forEach items="${paramMap.httpTriggerList}" var="resultModel" varStatus="status">		
					<div class="row padding-left-10">
						<div class="col-md-2">
							<div class="write-sub-title blt-dot">
								HTTP 트리거
							</div>
						</div>
						<div class="col-md-10">
							<div class="grey-600 font-size-13">
								<b>API 이름 : </b>${resultModel.adm_set_yn eq 'Y' ?  resultModel.api_name : '사용자가 직접 입력'}
							</div>
						</div>
					</div>
				</c:forEach>
			</c:if>
			
			<c:if test="${!empty paramMap.cephTriggerList}">
			<c:forEach items="${paramMap.cephTriggerList}" var="resultModel" varStatus="status">
				<div class="row padding-left-10">
					<div class="col-md-2">
						<div class="write-sub-title blt-dot">
							Ceph 트리거
						</div>
					</div>
					<div class="col-md-10">
						<div class="grey-600 font-size-13">
							<b>이벤트 유형 :  </b>
							<c:if test="${resultModel.adm_set_yn eq 'N'}">
								사용자가 직접 설정
							</c:if>
							<c:if test="${resultModel.adm_set_yn eq 'Y'}">
									${resultModel.create_yn eq 'Y' ?  '파일 생성':'파일 삭제'}
							</c:if>							 
						</div>
					</div>
				</div>
			</c:forEach>
			</c:if>
			
			
			<c:if test="${!empty paramMap.mongoTriggerList}">
				<c:forEach items="${paramMap.mongoTriggerList}" var="resultModel" varStatus="status">
					<div class="row padding-left-10">
						<div class="col-md-2">
							<div class="write-sub-title blt-dot">
								MongoDB 트리거
							</div>
						</div>
						<div class="col-md-10">
							<div class="grey-600 font-size-13">
								<b>이벤트 유형 : </b>
									<c:if test="${resultModel.adm_set_yn eq 'N'}">
										사용자가 직접 설정
									</c:if>
									<c:if test="${resultModel.adm_set_yn eq 'Y' && resultModel.db_type eq 'mongodb'}">
											${resultModel.insert_yn eq 'Y' ?  'INSERT':''}
											${resultModel.update_yn eq 'Y' ?  'UPDATE':''}
											${resultModel.delete_yn eq 'Y' ?  'DELETE':''}
									</c:if>								​
							</div>
						</div>
					</div>
				</c:forEach>
			</c:if>
			
			<c:if test="${!empty paramMap.postgreTriggerList}">
				<c:forEach items="${paramMap.postgreTriggerList}" var="resultModel" varStatus="status">
					<div class="row padding-left-10">
						<div class="col-md-2">
							<div class="write-sub-title blt-dot">
								PostgreSQL 트리거
							</div>
						</div>
						<div class="col-md-10">
							<div class="grey-600 font-size-13">
								<b>이벤트 유형 : </b>
									<c:if test="${resultModel.adm_set_yn eq 'N'}">
										사용자가 직접 설정
									</c:if>
									<c:if test="${resultModel.adm_set_yn eq 'Y' && resultModel.db_type eq 'postgre'}">
											${resultModel.insert_yn eq 'Y' ?  'INSERT':''}
											${resultModel.update_yn eq 'Y' ?  'UPDATE':''}
											${resultModel.delete_yn eq 'Y' ?  'DELETE':''}
									</c:if>														​
							</div>
						</div>
					</div>
				</c:forEach>
			</c:if>
			
			<c:if test="${!empty paramMap.timerTriggerList}">
					
					<div class="row padding-left-10">
						<div class="col-md-2">
							<div class="write-sub-title blt-dot">
								타이머 트리거
							</div>
						</div>
						<div class="col-md-10">
							<div class="grey-600 font-size-13">
								<b>반복 : </b>
								<c:forEach items="${paramMap.timerTriggerList}" var="resultModel" varStatus="status">		
									<c:if test="${resultModel.adm_set_yn eq 'N'}">
										<p class="margin-top-10">사용자가 직접 설정</p>
									</c:if>
									<c:if test="${resultModel.adm_set_yn eq 'Y'}">
										<p class="margin-top-10">${resultModel.repeat_des}</p>
									</c:if>
									</c:forEach>
							</div>
						</div>
					</div>
		   		
		   </c:if>
		   			
		</div> <!-- // panel-body -->
	</section>

	<section class="panel panel-bordered">
		<div class="panel-heading">
			<h3 class="panel-title">
				함수 코드
			</h3>
		</div> <!-- // panel-heading -->
		<div class="row_wrap">
		    <div class="tabWrap type3">
		        <ul class="nav nav-tabs source-fileName">
		            <!-- li class에 active를 넣으면 활성 상태 -->
		            <li id="index" class="active">
		                <a href="javascript:indexActive();" title="index.js">index.js</a>
		            </li>
		
		            <li id="package">
		                <a href="javascript:packageActive();" title="package.json">package.json</a>
		            </li>
		        </ul>
		    </div><!--//tabWrap-->
			<div class="row padding-left-10 padding-right-10">
				<div class="editorBox" style="height: 600px;" id="editor1">
					EDITOR 가 들어가는 영역입니다.
				</div>
				<div class="editorBox" style="height: 600px;display:none;" id="editor2">
					EDITOR 가 들어가는 영역입니다.
				</div>					
			</div>
		</div>	
	</section>
	
	
</div>

<!-- modal -->
	<div class="modal" id="modalConfirm">
		<div class="modal-dialog modal-sm">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-label="Close">
						<span aria-hidden="true">×</span>
					</button>
					<h4 class="modal-title">알림</h4>
				</div>
				<div class="modal-body" style="height:90px;">
					<div class="modal-center font-size-16">
						삭제 하시겠습니까?
					</div>
				</div>
				<div class="modal-footer text-center">
					<button type="button" class="btn btn-primary" onClick="javascript:goDelete();">확인</button>
					<button type="button" class="btn btn-default" data-dismiss="modal" id="deleteCancel">취소</button>
				</div>
			</div>
		</div>
	</div>	
	
					
					