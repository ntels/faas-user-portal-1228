<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>  

<div class="bg-border-box trigger-ele">
				<div class="row">
					<div class="col-md-8">
						<div class="color-primary">
							<b>
								타이머 트리거
							</b>
						</div>
					</div>
					<div class="col-md-4 text-right">
						<a href="javascript:void(0)" class="close" data-btn="delete">
							<i class="xi-close"></i>
						</a>
					</div>
				</div>
				<div class="row text-type">
					<div class="col-sm-2">
						<div class="write-sub-title margin-top-10">
							<b class="caution-star">*</b> 반복 설정
						</div>
					</div>
					<div class="col-sm-10">
						<ul class="list-inline" data-sel="timerSel${paramMap.timerCnt}">
						<input type="hidden" name="timerIndex" id="timerIndex" value="${paramMap.timerCnt}"/>
							<li style="vertical-align: top;">
								<div class="radio-group padding-top-10">
									<input type="radio" name="radioTimer${paramMap.timerCnt}" id="tuser${paramMap.timerCnt}" class="radio-input" value="user" checked="">
									<label for="tuser${paramMap.timerCnt}" class="radio-item">
										<div class="radio">
											<div class="checked-icon"></div>
										</div>
										<span class="text">
											사용자가 직접 입력
										</span>
									</label>
								</div>
							</li>
							<li style="vertical-align: top;">
								<div class="radio-group padding-top-10" style="width: 200px;">
									<input type="radio" name="radioTimer${paramMap.timerCnt}" id="tadmin${paramMap.timerCnt}" class="radio-input" value="admin">
									<label for="tadmin${paramMap.timerCnt}" class="radio-item">
										<div class="radio">
											<div class="checked-icon"></div>
										</div>
										<span class="text">
											관리자 지정
										</span>
										<span class="info-icon-tooltip" data-toggle="tooltip" title="" data-placement="top" data-original-title="관리자가 항목을 지정할 경우, 사용자는 설정을 변경할 수 없습니다.">
											i
										</span>
									</label>
								</div>
							</li>
							
							
							<li data-lst="admin" style="width: 630px; display: none;">
								<ul class="list-inline">
								<input type="hidden" name="timerIndex2" id="timerIndex2" value="${paramMap.timerCnt}"/>
									<li>
										<label for="timer${paramMap.timerCnt}_triggeradd1" class="input-card-type">
											<input type="radio" id="timer${paramMap.timerCnt}_triggeradd1" name="repeatSet${paramMap.timerCnt}"  class="card-input-element" checked="">
											<div class="card-input">
												<div class="card-type4">
													<span class="img-repeat">
														<span class="none">
															<i></i>
														</span>
													</span>
												</div>
											</div>
											<p class="text-center">
												없음
											</p>
										</label>
									</li>
									<li>
										<label for="timer${paramMap.timerCnt}_perMinute" class="input-card-type">
											<input type="radio" id="timer${paramMap.timerCnt}_perMinute" name="repeatSet${paramMap.timerCnt}" 
																							value="perMinute" class="card-input-element" data-btn="minute${paramMap.timerCnt}">
											<div class="card-input">
												<div class="card-type4">
													<span class="img-repeat">
														<span class="minute">
															<i></i>
														</span>
													</span>
												</div>
											</div>
											<p class="text-center">
												분
											</p>
										</label>
									</li>
									<li>
										<label for="timer${paramMap.timerCnt}_perHour" class="input-card-type">
											<input type="radio" id="timer${paramMap.timerCnt}_perHour" name="repeatSet${paramMap.timerCnt}" 
																							value="perHour" class="card-input-element" data-btn="hour${paramMap.timerCnt}">
											<div class="card-input">
												<div class="card-type4">
													<span class="img-repeat">
														<span class="hour">
															<i></i>
														</span>
													</span>
												</div>
											</div>
											<p class="text-center">
												시간
											</p>
										</label>
									</li>
									<li>
										<label for="timer${paramMap.timerCnt}_perDay" class="input-card-type">
											<input type="radio" id="timer${paramMap.timerCnt}_perDay" name="repeatSet${paramMap.timerCnt}" 
																							value="perDay" class="card-input-element" data-btn="day${paramMap.timerCnt}">
											<div class="card-input">
												<div class="card-type4">
													<span class="img-repeat">
														<span class="day">
															<i></i>
														</span>
													</span>
												</div>
											</div>
											<p class="text-center">
												매일
											</p>
										</label>
									</li>
									<li>
										<label for="timer${paramMap.timerCnt}_perWeek" class="input-card-type">
											<input type="radio" id="timer${paramMap.timerCnt}_perWeek" name="repeatSet${paramMap.timerCnt}" 
																							value="perWeek" class="card-input-element" data-btn="week${paramMap.timerCnt}">
											<div class="card-input">
												<div class="card-type4">
													<span class="img-repeat">
														<span class="week">
															<i></i>
														</span>
													</span>
												</div>
											</div>
											<p class="text-center">
												주별
											</p>
										</label>
									</li>
									<li>
										<label for="timer${paramMap.timerCnt}_perMonth" class="input-card-type">
											<input type="radio" id="timer${paramMap.timerCnt}_perMonth" name="repeatSet${paramMap.timerCnt}" 
																							value="perMonth" class="card-input-element" data-btn="month${paramMap.timerCnt}">
											<div class="card-input">
												<div class="card-type4">
													<span class="img-repeat">
														<span class="month">
															<i></i>
														</span>
													</span>
												</div>
											</div>
											<p class="text-center">
												월별
											</p>
										</label>
									</li>
									<li>
										<label for="timer${paramMap.timerCnt}_perYear" class="input-card-type">
											<input type="radio" id="timer${paramMap.timerCnt}_perYear" name="repeatSet${paramMap.timerCnt}" 
																							value="perYear" class="card-input-element" data-btn="year${paramMap.timerCnt}">
											<div class="card-input">
												<div class="card-type4">
													<span class="img-repeat">
														<span class="year">
															<i></i>
														</span>
													</span>
												</div>
											</div>
											<p class="text-center">
												연간
											</p>
										</label>
									</li>
								</ul>
								
								<!-- 시간마다반복 -->
								<div class="margin-top-20">
									<div class="row" data-lst="repeat${paramMap.timerCnt}" data-tab="minute${paramMap.timerCnt}" style="display: none;">
										<div class="col-sm-12">
											<div class="form-inline">
												<div class="radio-group">
													<input type="radio" id="perMinute${paramMap.timerCnt}_per" name="perMinute${paramMap.timerCnt}_per" class="radio-input" checked="">
													<label for="perMinute${paramMap.timerCnt}_per" class="radio-item">
														<div class="radio">
															<div class="checked-icon"></div>
														</div>
														<span class="text">
															매
														</span>
													</label>
												</div>
												<div class="form-group">
													<select name="perMinuteVal${paramMap.timerCnt}" id="perMinuteVal${paramMap.timerCnt}" class="form-control">
													<c:forEach var="i" begin="1" end="59">
														<option value="${i}">${i}</option>
													</c:forEach>
													</select>
													<span class="text">분 마다 반복</span>
												</div>
											</div>
										</div>
									</div>
									<div class="row" data-lst="repeat${paramMap.timerCnt}" data-tab="hour${paramMap.timerCnt}" style="display: none;">
										<div class="col-sm-12">
											<div class="form-inline">
												<div class="radio-group">
													<input type="radio" id="perHour${paramMap.timerCnt}_per" name="perHour${paramMap.timerCnt}_per" value="per"
																																	class="radio-input" checked="">
													<label for="perHour${paramMap.timerCnt}_per" class="radio-item">
														<div class="radio">
															<div class="checked-icon"></div>
														</div>
														<span class="text">
															매
														</span>
													</label>
												</div>
												<div class="form-group">
													<select id="perHourVal${paramMap.timerCnt}" name="perHourVal${paramMap.timerCnt}"   class="form-control">
													<c:forEach var="i" begin="1" end="23">
														<option value="${i}">${i}</option>
													</c:forEach>
													</select>
													<span class="text">시간 마다 반복</span>
												</div>
											</div>
										</div>
									</div>
									
									<!-- 일마다반복 -->
									<div class="row" data-lst="repeat${paramMap.timerCnt}" data-tab="day${paramMap.timerCnt}" style="display: none;">
										<div class="col-sm-4">
											<div class="form-inline">
												<div class="radio-group">
													<input type="radio" id="perDay${paramMap.timerCnt}_per" name="perDay${paramMap.timerCnt}" value= "per" 
																																		class="radio-input" checked="">
													<label for="perDay${paramMap.timerCnt}_per" class="radio-item">
														<div class="radio">
															<div class="checked-icon"></div>
														</div>
														<span class="text">
															매
														</span>
													</label>
												</div>
												<div class="form-group">
													<select id="perDayVal${paramMap.timerCnt}" name="perDayVal${paramMap.timerCnt}" class="form-control">
													<c:forEach var="i" begin="1" end="30">
														<option value="${i}">${i}</option>
													</c:forEach>
													</select>
													<span class="text">일 마다 반복</span>
												</div>
											</div>
										</div>
										<div class="col-sm-8">
											<div class="form-inline">
												<div class="radio-group padding-top-10">
													<input type="radio" id="perDay${paramMap.timerCnt}_noweekend" name="perDay${paramMap.timerCnt}" class="radio-input">
													<label for="perDay${paramMap.timerCnt}_noweekend" class="radio-item">
														<div class="radio">
															<div class="checked-icon"></div>
														</div>
														<span class="text">
															평일
														</span>
													</label>
												</div>
											</div>
										</div>
									</div>
									
									<div class="row" data-lst="repeat${paramMap.timerCnt}" data-tab="week${paramMap.timerCnt}" style="display: none;">
										<div class="form-inline margin-top-10">
											<div class="checkbox-group margin-right-20">
												<input type="checkbox" id="perWeekVal${paramMap.timerCnt}_monday" name="perWeekVal${paramMap.timerCnt}" value="MON" class="checkbox-input">
												<label for="perWeekVal${paramMap.timerCnt}_monday" class="checkbox-item">
													<div class="checkbox">
														<div class="checked-icon">
															<i class="xi-check"></i>
														</div>
													</div>
													<span class="text">
														월요일
													</span>
												</label>
											</div>
											<div class="checkbox-group margin-right-20">
												<input type="checkbox" id="perWeekVal${paramMap.timerCnt}_tuesday" name="perWeekVal${paramMap.timerCnt}" value="TUE" class="checkbox-input">
												<label for="perWeekVal${paramMap.timerCnt}_tuesday" class="checkbox-item">
													<div class="checkbox">
														<div class="checked-icon">
															<i class="xi-check"></i>
														</div>
													</div>
													<span class="text">
														화요일
													</span>
												</label>
											</div>
											<div class="checkbox-group margin-right-20">
												<input type="checkbox" id="perWeekVal${paramMap.timerCnt}_wednesday"  name="perWeekVal${paramMap.timerCnt}" value="WED" class="checkbox-input">
												<label for="perWeekVal${paramMap.timerCnt}_wednesday" class="checkbox-item">
													<div class="checkbox">
														<div class="checked-icon">
															<i class="xi-check"></i>
														</div>
													</div>
													<span class="text">
														수요일
													</span>
												</label>
											</div>
											<div class="checkbox-group margin-right-20">
												<input type="checkbox" id="perWeekVal${paramMap.timerCnt}_thursday" name="perWeekVal${paramMap.timerCnt}" value="THU" class="checkbox-input">
												<label for="perWeekVal${paramMap.timerCnt}_thursday" class="checkbox-item">
													<div class="checkbox">
														<div class="checked-icon">
															<i class="xi-check"></i>
														</div>
													</div>
													<span class="text">
														목요일
													</span>
												</label>
											</div>
											<div class="checkbox-group margin-right-20">
												<input type="checkbox" id="perWeekVal${paramMap.timerCnt}_friday" name="perWeekVal${paramMap.timerCnt}" value="FRI" class="checkbox-input">
												<label for="perWeekVal${paramMap.timerCnt}_friday" class="checkbox-item">
													<div class="checkbox">
														<div class="checked-icon">
															<i class="xi-check"></i>
														</div>
													</div>
													<span class="text">
														금요일
													</span>
												</label>
											</div>
											<div class="checkbox-group margin-right-20">
												<input type="checkbox" id="perWeekVal${paramMap.timerCnt}_saturday" name="perWeekVal${paramMap.timerCnt}" value="SAT" class="checkbox-input">
												<label for="perWeekVal${paramMap.timerCnt}_saturday" class="checkbox-item">
													<div class="checkbox">
														<div class="checked-icon">
															<i class="xi-check"></i>
														</div>
													</div>
													<span class="text">
														토요일
													</span>
												</label>
											</div>
											<div class="checkbox-group margin-right-20">
												<input type="checkbox" id="perWeekVal${paramMap.timerCnt}_sunday" name="perWeekVal${paramMap.timerCnt}" value="SUN" class="checkbox-input">
												<label for="perWeekVal${paramMap.timerCnt}_sunday" class="checkbox-item">
													<div class="checkbox">
														<div class="checked-icon">
															<i class="xi-check"></i>
														</div>
													</div>
													<span class="text">
														일요일
													</span>
												</label>
											</div>
										</div>
									</div>
									
									<!-- 월별 -->
									<div class="row" data-lst="repeat${paramMap.timerCnt}" data-tab="month${paramMap.timerCnt}" style="display: none;">
										<div class="col-sm-5">
											<div class="form-inline">
												<div class="radio-group">
													<input type="radio" id="type${paramMap.timerCnt}_month_eachDay" name="type${paramMap.timerCnt}_month"  
																																value="eachDay" class="radio-input" checked="">
													<label for="type${paramMap.timerCnt}_month_eachDay" class="radio-item">
														<div class="radio">
															<div class="checked-icon"></div>
														</div>
														<span class="text">
															매월
														</span>
													</label>
												</div>
												<div class="form-group">
													<select id="perMonthVal${paramMap.timerCnt}" name="perMonthVal${paramMap.timerCnt}" class="form-control">
														<c:forEach var="i" begin="1" end="31">
															<option value="${i}">${i}</option>
														</c:forEach>													
													</select>
													<span class="text">일 마다 반복</span>
												</div>
											</div>
										</div>
										<div class="col-sm-7">
											<div class="form-inline">
												<div class="radio-group">
													<input type="radio" id="type${paramMap.timerCnt}_month_eachSeq" name="type${paramMap.timerCnt}_month" 
																																 class="radio-input">
													<label for="type${paramMap.timerCnt}_month_eachSeq" class="radio-item">
														<div class="radio">
															<div class="checked-icon"></div>
														</div>
													</label>
												</div>
												<div class="form-group">
													<select id="selMonthWeek_1_Val${paramMap.timerCnt}" name="selMonthWeek_1_Val${paramMap.timerCnt}" class="form-control">
														<option value="1">첫 번째</option>
														<option value="2">두 번째</option>
														<option value="3">세 번째</option>
														<option value="4">네 번째</option>
														<option value="5">다섯번째</option>
													</select>
												</div>
												<div class="form-group">
													<select id="selMonthWeek_2_Val${paramMap.timerCnt}" name="selMonthWeek_2_Val${paramMap.timerCnt}" class="form-control">
														<option value="MON">월요일</option>
														<option value="TUE">화요일</option>
														<option value="WED">수요일</option>
														<option value="THU">목요일</option>
														<option value="FRI">금요일</option>
														<option value="SAT">토요일</option>
														<option value="SUN">일요일</option>
													</select>
													<span class="text">마다 반복</span>
												</div>
											</div>
										</div>
									</div>
									
									<!-- 년별 -->
									<div class="row" data-lst="repeat${paramMap.timerCnt}" data-tab="year${paramMap.timerCnt}" style="display: none;">
										<div class="col-sm-5">
											<div class="form-inline">
												<div class="radio-group">
													<input type="radio" id="type${paramMap.timerCnt}_year_eachMonth" name="type${paramMap.timerCnt}_year" 
																																value="eachMonth" class="radio-input" checked="">
													<label for="type${paramMap.timerCnt}_year_eachMonth" class="radio-item">
														<div class="radio">
															<div class="checked-icon"></div>
														</div>
														<span class="text">
															매년
														</span>
													</label>
												</div>
												<div class="form-group">
													<select id="perYearVal${paramMap.timerCnt}" name="perYearVal${paramMap.timerCnt}" class="form-control">
														<c:forEach var="i" begin="1" end="12">
															<option value="${i}">${i}</option>
														</c:forEach>	
													</select>
													<span class="text">월 마다 반복</span>
												</div>
											</div>
										</div>
										<div class="col-sm-7">
											<div class="form-inline">
												<div class="radio-group">
													<input type="radio" id="type${paramMap.timerCnt}_year_eachSeq" name="type${paramMap.timerCnt}_year" 
																																 class="radio-input">
													<label for="type${paramMap.timerCnt}_year_eachSeq" class="radio-item">
														<div class="radio">
															<div class="checked-icon"></div>
														</div>
													</label>
												</div>
												<div class="form-group">
													<select id="selYearWeek_1_Val${paramMap.timerCnt}" name="selYearWeek_1_Val${paramMap.timerCnt}" class="form-control">
														<option value="1">첫 번째</option>
														<option value="2">두 번째</option>
														<option value="3">세 번째</option>
														<option value="4">네 번째</option>
														<option value="5">다섯번째</option>
													</select>
												</div>
												<div class="form-group">
													<select id="selYearWeek_2_Val${paramMap.timerCnt}" name="selYearWeek_2_Val${paramMap.timerCnt}" class="form-control">
														<option value="MON">월요일</option>
														<option value="TUE">화요일</option>
														<option value="WED">수요일</option>
														<option value="THU">목요일</option>
														<option value="FRI">금요일</option>
														<option value="SAT">토요일</option>
														<option value="SUN">일요일</option>
													</select>
													<span class="text">마다 반복</span>
												</div>
											</div>
										</div>
									</div>
								</div>
								<script>
									// 반복설정 제어
	 								$('[data-lst="repeat"]').hide();
									$(document).on('click', '[data-lst="admin"] li input', function() {		
									 	var parents = $(this).parent().parent().parent();

										var divObj = null;
										for (var i=0;i<parents.length;i++) {
											var parent = parents[i];
											console.log($(parent));
											if ($(parent).hasClass("list-inline")) {
												divObj = parent;
												break;
											}
										}
										
										var index = $(divObj).find('input[name="timerIndex2"]').val();
										var section = $(this).attr('data-btn');
									    var repeat = "repeat"+index;
										
										$('[data-lst='+ repeat+']').hide();
										$('[data-tab='+ section +']').show();
									}); 
								</script>
							</li>
						</ul>
					</div>
				</div>
			</div>