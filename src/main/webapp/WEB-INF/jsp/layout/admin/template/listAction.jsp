<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>  

<script>
	$(document).ready(function() {
		$("#pageNavigationDiv2").html("");
		initPaging("pageNavigationDiv2", "${resultList.page}", "${resultList.perPage}", "${resultList.perSize}", "${resultList.totalCount}","\'template/listAction\'");
		
		$("#perPage").change(function() {
			goSearch("template/listAction");
		});
		$("#runtimeVersion").change(function() {
			goSearch("template/listAction");
		});
		$("#trigger_tp").change(function() {
			goSearch("template/listAction");
		});
		$("#orderBy").change(function() {
			goSearch("template/listAction");
		});		
		
		$("#contentWrap > div.page-content > div > div > div > div:nth-child(1) > div > b > span").text("(${resultList.totalCount})")
		
	});
	

</script>
<c:if test="${not empty resultList.lists}">
	<section class="panel panel-bordered bt-0">
		<div class="panel-body padding-0">
			<div class="table-input-wrap">
				<div class="row">
					<div class="col-sm-3">
						<div class="datatable-length">
							<span>페이지당 행 수 : </span>
							<select name="perPage" id="perPage" class="form-control form-control-sm margin-left-5">
								<option value="10" ${paramMap.perPage eq '10' ? 'selected':''}>10</option>
								<option value="25" ${paramMap.perPage eq '25' ? 'selected':''}>25</option>
								<option value="50" ${paramMap.perPage eq '50' ? 'selected':''}>50</option>
								<option value="100" ${paramMap.perPage eq '100' ? 'selected':''}>100</option>
							</select>
						</div>
					</div>
				</div>
			</div>
			<div class="table-wrap">
				<table class="table" cellspacing="0" width="100%">
					<colgroup>
						<col>
						<col style="width:150px;">
						<col style="width:180px;">
						<col style="width:150px;">
						<col style="width:150px;">
					</colgroup>
					<thead>
						<tr>	
							<th class="text-center">함수 템플릿 이름</th>
							<th class="text-center">런타임</th>
							<th class="text-center">트리거</th>
							<th class="text-center">등록 일시</th>
							<th class="text-center">등록자</th>
						</tr>
					</thead>
					<tbody>
					<c:if test="${!empty resultList.lists}">
						<c:forEach items="${resultList.lists}" var="resultModel" varStatus="status">
						<tr>
							<td>
								<a href="javascript:goDetail('${resultModel.template_id}');">${resultModel.dsp_name}</a>
								${resultModel.top_yn eq "Y" ? '<span class="label label-info margin-left-10">상단노출</span>' : ''}
							</td>
							<td class="text-center">${resultModel.runtime} ${resultModel.version}</td>
							<td class="text-center">${resultModel.trigger_tp}</td>
							<td class="text-center">${resultModel.reg_date_hms}</td>
							<td class="text-center">${resultModel.reg_usr}</td>
						</tr>
						</c:forEach>
					</c:if>
					</tbody>
				</table>
			</div>
			
			<div class="table-bottom-wrap">
				<div class="row">
					<div class="datatable-paginate text-center">
						<ul class="pagination" id="pageNavigationDiv2"></ul>
					</div>
				</div>
			</div>
		</div>
	</section>
</c:if>

<c:if test="${empty resultList.lists}">
	<div class="nodata-box type2">
		<div class="img-div">
			<p class="text-center">등록된 데이터가 없습니다. 함수 템플릿을 새로 등록해 주세요.</p>
		</div>
	</div>
</c:if>

<!-- <section>
	데이터가 없습니다.
	<div class="nodata-box type2">
		<div class="img-div">
			<p class="text-center">등록된 데이터가 없습니다. 함수 템플릿을 새로 등록해 주세요.</p>
		</div>
	</div>
</section> -->