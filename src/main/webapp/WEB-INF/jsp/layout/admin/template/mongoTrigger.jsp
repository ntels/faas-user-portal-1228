<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>  

<div class="bg-border-box trigger-ele">
				<div class="row">
					<div class="col-md-8">
						<div class="color-primary">
							<b>
								MongoDB 트리거
							</b>
						</div>
					</div>
					<div class="col-md-4 text-right">
						<button class="close" data-btn="delete">
							<i class="xi-close"></i>
						</button>
					</div>
				</div>
				<div class="row text-type">
					<div class="col-sm-2">
						<div class="write-sub-title margin-top-10">
							<b class="caution-star">*</b> 이벤트 유형
						</div>
					</div>
					<div class="col-sm-10">
						<ul class="list-inline" data-sel="mongoSel${paramMap.mongoCnt}">
						<input type="hidden" name="mongoIndex" id="mongoIndex" value="${paramMap.mongoCnt}"/>
							<li>
								<div class="radio-group padding-top-10">
									<input type="radio" name="radioMongo${paramMap.mongoCnt}" id="muser${paramMap.mongoCnt}" class="radio-input" value="user" checked>
									<label for="muser${paramMap.mongoCnt}" class="radio-item">
										<div class="radio">
											<div class="checked-icon"></div>
										</div>
										<span class="text">
											사용자가 직접 입력
										</span>
									</label>
								</div>
							</li>
							<li>
								<div class="radio-group padding-top-10" style="width: 200px;">
									<input type="radio" name="radioMongo${paramMap.mongoCnt}" id="madmin${paramMap.mongoCnt}" class="radio-input" value="admin">
									<label for="madmin${paramMap.mongoCnt}" class="radio-item">
										<div class="radio">
											<div class="checked-icon"></div>
										</div>
										<span class="text">
											관리자 지정
										</span>
										<span class="info-icon-tooltip" data-toggle="tooltip" title="" data-placement="top" data-original-title="관리자가 항목을 지정할 경우, 사용자는 설정을 변경할 수 없습니다.">
											i
										</span>
									</label>
								</div>
							</li>
							<li data-lst="admin" style="display: none;">
								<div class="form-inline">
									<div class="radio-group margin-right-20">
										<input type="radio" id="chkMongoInsert${paramMap.mongoCnt}"  name="chkMongo${paramMap.mongoCnt}" value="insert" class="radio-input" checked>
										<label for="chkMongoInsert${paramMap.mongoCnt}"  class="radio-item">
											<span class="inline-block" style="vertical-align: bottom;">(</span>
											<div class="radio">
												<div class="checked-icon">
												</div>
											</div>
											<span class="text">
												INSERT
											</span>
										</label>
									</div>
									<div class="radio-group margin-right-20">
										<input type="radio" id="chkMongoUpdate${paramMap.mongoCnt}" name="chkMongo${paramMap.mongoCnt}" value="update" class="radio-input">
										<label for="chkMongoUpdate${paramMap.mongoCnt}" class="radio-item">
											<div class="radio">
												<div class="checked-icon">
												</div>
											</div>
											<span class="text">
												UPDATE
											</span>
										</label>
									</div>
									<div class="radio-group margin-right-20">
										<input type="radio" id="chkMongoDelete${paramMap.mongoCnt}" name="chkMongo${paramMap.mongoCnt}" value="delete" class="radio-input">
										<label for="chkMongoDelete${paramMap.mongoCnt}" class="radio-item">
											<div class="radio">
												<div class="checked-icon">
												</div>
											</div>
											<span class="text">
												DELETE
											</span>
											<span class="inline-block" style="vertical-align: bottom;">)</span>
										</label>
									</div>
								</div>
							</li>
						</ul>
					</div>
				</div>
			</div>