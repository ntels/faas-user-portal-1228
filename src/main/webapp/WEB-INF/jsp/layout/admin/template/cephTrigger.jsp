<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>  	
<script type="text/javascript">
	var condition = ${condition};
    $(document).ready(function() {
    });
</script> 
<div class="bg-border-box trigger-ele">
	<div class="row">
		<div class="col-md-8">
			<div class="color-primary">
				<b>
					Ceph 트리거
				</b>
			</div>
		</div>
		<div class="col-md-4 text-right">
			<button class="close" data-btn="delete">
				<i class="xi-close"></i>
			</button>
		</div>
	</div>
	<div class="row text-type">
		<div class="col-sm-2">
			<div class="write-sub-title margin-top-10">
				<b class="caution-star">*</b> 이벤트 유형
			</div>
		</div>
		<div class="col-sm-10">
			<ul class="list-inline" data-sel="cephSel${paramMap.cephCnt}">
			<input type="hidden" name="cephIndex" id="cephIndex" value="${paramMap.cephCnt}"/>
				<li>
					<div class="radio-group padding-top-10">
						<input type="radio" name="radioCeph${paramMap.cephCnt}" id="cuser${paramMap.cephCnt}" class="radio-input" value="user" checked="">
						<label for="cuser${paramMap.cephCnt}" class="radio-item">
							<div class="radio">
								<div class="checked-icon"></div>
							</div>
							<span class="text">
								사용자가 직접 입력
							</span>
						</label>
					</div>
				</li>
				<li>
					<div class="radio-group padding-top-10" style="width: 200px;">
						<input type="radio" name="radioCeph${paramMap.cephCnt}" id="cadmin${paramMap.cephCnt}" class="radio-input" value="admin">
						<label for="cadmin${paramMap.cephCnt}" class="radio-item">
							<div class="radio">
								<div class="checked-icon"></div>
							</div>
							<span class="text">
								관리자 지정
							</span>
							<span class="info-icon-tooltip" data-toggle="tooltip" title="" data-placement="top" data-original-title="관리자가 항목을 지정할 경우, 사용자는 설정을 변경할 수 없습니다.">
								i
							</span>
						</label>
					</div>
				</li>
				<li data-lst="admin" style="display: none;">
					<div class="form-inline">
						<div class="radio-group padding-top-10">
							<input type="radio" id="chkFileCreate${paramMap.cephCnt}"  name="chkFile${paramMap.cephCnt}"  class="radio-input" value="create" checked>
							<label for="chkFileCreate${paramMap.cephCnt}" class="radio-item">
								<span class="inline-block" style="vertical-align: bottom;">(</span>
								<div class="radio">
									<div class="checked-icon">
									</div>
								</div>
								<span class="text">
									파일 생성
								</span>
							</label>
						</div>
						<div class="radio-group padding-top-10">
							<input type="radio" id="chkFileDel${paramMap.cephCnt}" name="chkFile${paramMap.cephCnt}" value="delete" class="radio-input">
							<label for="chkFileDel${paramMap.cephCnt}" class="radio-item">
								<div class="radio">
									<div class="checked-icon">
									</div>
								</div>
								<span class="text">
									파일 삭제
								</span>
								<span class="inline-block" style="vertical-align: bottom;">)</span>
							</label>
						</div>
					</div>
				</li>
			</ul>
		</div>
	</div>
</div><!-- //Ceph 트리거 -->